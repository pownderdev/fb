<?php
	namespace App\Models;
	
	use Illuminate\Database\Eloquent\Model;
	
	class FacebookAdsAccounts extends Model
	{
		/**
			* The table associated with the model.
			*
			* @var string
		*/
		protected $table = 'facebook_ads_accounts';
		
		/**
			* Indicates if the model should be timestamped.
			*
			* @var bool
		*/
		public $timestamps = true;
		
		
		/**
			* The storage format of the model's date columns.
			*
			* @var string
		*/
		//protected $dateFormat = 'U';
		protected $dateFormat = 'Y-m-d H:i:s';
		
		/**
			* The attributes that are mass assignable.
			*
			* @var array
		*/
		//protected $fillable = ['name', 'email', 'password'];
		
		/**
			* The attributes that should be hidden for arrays.
			*
			* @var array
		*/
		//protected $hidden = ['password', 'remember_token'];
	}		