<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class CheckAndAllowLogins
{
    public function handle($request, Closure $next, ...$role)
    {
        // Perform action
        
        if(in_array(session('user')->category,$role))
        {
         return $next($request);   
        }
        else if(Session::has('user'))
        {
         abort(404);
        }
        else
        {             
             return redirect('logout');
        }
        
    }
}