<?php
	namespace App\Http\Controllers\Panel;
	
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	
	use DB;
	use Redirect;
	use Session;
	use Helper;
	use ManagerHelper;
	use ClientHelper;
	use AdminHelper;
	use VendorHelper;
	use Mail;
	
	class VendorController extends Controller
	{
		public function getVendorDashboard()
		{
			$UserArray = array();
			$ClientArray = array();
			$ManagerArray = array();
			$ShowClient = '';
			$ShowLead = '';
			$WritePermission = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$ShowClient = ManagerHelper::ManagerClientMenuAction();
				$WritePermission = ManagerHelper::ManagerWritePermission('Dashboard Read Write');
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				
				$UserArray[] = Session::get('user_id');
				$ManagerArray[] = Session::get('manager_id');
				
				$Manager = DB::table('managers')
				->leftJoin('users', 'managers.vendor_id', '=', 'users.vendor_id')
				->select('users.id', 'users.vendor_id')
				->where('managers.id', Session::get('manager_id'))
				->first();
				
				$vendor_id = $Manager->vendor_id;
				
				$UserArray[] = $Manager->id;
				
				$FacebookUserId = $Manager->id;
				
				$Users = DB::table('managers')
				->leftJoin('users', 'managers.id', '=', 'users.manager_id')
				->select('users.id', 'users.manager_id')
				->where('managers.vendor_id', $vendor_id)
				->get();
				foreach($Users as $User)
				{
					if($User->id > 0)
					{
						$UserArray[] = $User->id;
					}
					if($User->manager_id > 0)
					{
						$ManagerArray[] = $User->manager_id;
					}
				}
				
				$Clients = DB::table('clients')
				->leftJoin('users', 'clients.id', '=', 'users.client_id')
				->select('users.id', 'users.client_id')
				->where('clients.vendor_id', $vendor_id)
				->get();
				foreach($Clients as $Client)
				{
					$ClientArray[] = $Client->client_id;
					$UserArray[] = $Client->id;
					$Managers = DB::table('managers')
					->leftJoin('users', 'managers.id', '=', 'users.manager_id')
					->select('users.id', 'users.manager_id')
					->where('managers.client_id', $Client->client_id)
					->get();
					foreach($Managers as $Manager)
					{
						if($Manager->id > 0)
						{
							$UserArray[] = $Manager->id;
						}
						if($Manager->manager_id > 0)
						{
							$ManagerArray[] = $Manager->manager_id;
						}
					}
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$FacebookUserId = Session::get('user_id');
				$vendor_id = Session::get('vendor_id');
				
				$Users = DB::table('managers')
				->leftJoin('users', 'managers.id', '=', 'users.manager_id')
				->select('users.id', 'users.manager_id')
				->where('managers.vendor_id', $vendor_id)
				->get();
				foreach($Users as $User)
				{
					if($User->id > 0)
					{
						$UserArray[] = $User->id;
					}
					if($User->manager_id > 0)
					{
						$ManagerArray[] = $User->manager_id;
					}
				}
				
				$Clients = DB::table('clients')
				->leftJoin('users', 'clients.id', '=', 'users.client_id')
				->select('users.id', 'users.client_id')
				->where('clients.vendor_id', $vendor_id)
				->get();
				foreach($Clients as $Client)
				{
					$UserArray[] = $Client->id;
					$ClientArray[] = $Client->client_id;
					$Managers = DB::table('managers')
					->leftJoin('users', 'managers.id', '=', 'users.manager_id')
					->select('users.id', 'users.manager_id')
					->where('managers.client_id', $Client->client_id)
					->get();
					foreach($Managers as $Manager)
					{
						if($Manager->id > 0)
						{
							$UserArray[] = $Manager->id;
						}
						if($Manager->manager_id > 0)
						{
							$ManagerArray[] = $Manager->manager_id;
						}
					}
				}
			}
			
			$date_start = date('Y-m-d');
			$date_stop = date('Y-m-d');
			
			$campaign_s_date = 	date('Y-m').'-01';
			$campaign_e_date = date('Y-m-d');
			
			$VendorColumns = array();
			$Columns = DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Dashboard Client Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$VendorColumns[] = $Column->column_value;
			}
			
			$Reach = 0;
			$Likes = 0;
			$Spend = 0;
			$Click = 0;
			
			$client_id = Session::get('client_id');
			
			$clients = DB::table('clients')->select('id')
			->where(function ($query) use ($ShowClient, $vendor_id) {
				if($ShowClient == 'Allotted Clients')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
				else
				{
					$query->where('vendor_id', '=', $vendor_id);
				}
			})
			->where('is_deleted', 0)
			->count('id');
			
			$QueryClients = DB::table('clients')
			->where(function ($query) use ($ShowClient, $vendor_id) {
				if($ShowClient == 'Allotted Clients')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
				else
				{
					$query->where('vendor_id', '=', $vendor_id);
				}
			})
			->where('is_deleted', 0)
			->orderBy('name', 'ASC')
			->get();
			
			$UnassignLeads = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($UserArray, $ClientArray, $ManagerArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
				else
				{
					$query->whereIn('created_user_id', array_unique($UserArray))
					->whereIn('client_id', array_unique($ClientArray), 'or')
					->whereIn('manager_id', array_unique($ManagerArray), 'or');
				}
			})
			->where('client_id', 0)
			->count('id');
			
			$TotalLead = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($UserArray, $ClientArray, $ManagerArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
				else
				{
					$query->whereIn('created_user_id', array_unique($UserArray))
					->whereIn('client_id', array_unique($ClientArray), 'or')
					->whereIn('manager_id', array_unique($ManagerArray), 'or');
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->count('id');
			
			$TodayLead = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($UserArray, $ClientArray, $ManagerArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
				else
				{
					$query->whereIn('created_user_id', array_unique($UserArray))
					->whereIn('client_id', array_unique($ClientArray), 'or')
					->whereIn('manager_id', array_unique($ManagerArray), 'or');
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($UserArray, $ClientArray, $ManagerArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
				else
				{
					$query->whereIn('created_user_id', array_unique($UserArray))
					->whereIn('client_id', array_unique($ClientArray), 'or')
					->whereIn('manager_id', array_unique($ManagerArray), 'or');
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->where('lead_type', 'Pownder™ Lead')
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$CityLeads = DB::table('facebook_ads_lead_users')->selectRaw('count(id) as Leads, city, latitude, longitude')
			->where(function ($query) use($UserArray, $ClientArray, $ManagerArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
				else
				{
					$query->whereIn('created_user_id', array_unique($UserArray))
					->whereIn('client_id', array_unique($ClientArray), 'or')
					->whereIn('manager_id', array_unique($ManagerArray), 'or');
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->where('city', '!=', '')
			->where('latitude', '!=', '')
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->groupBy('latitude')
			->groupBy('longitude')
			->get();
			
			$CampaignArray = array();
			$client_campaigns = DB::table('facebook_ads_lead_users')->select('campaign_id')
			->where(function ($query) use($UserArray, $ClientArray, $ManagerArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->whereIn('created_user_id', array_unique($UserArray))
					->whereIn('client_id', array_unique($ClientArray), 'or')
					->whereIn('manager_id', array_unique($ManagerArray), 'or');
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->whereDate('created_time', '>=', $campaign_s_date)
			->whereDate('created_time', '<=', $campaign_e_date)
			->where('campaign_id', '!=', 0)
			->groupBy('campaign_id')
			->get();
			foreach($client_campaigns as $client_campaign)
			{
				$CampaignArray[] = $client_campaign->campaign_id;
			}
			
			$GraphClick = array();
			$GraphCPA = array();
			$GraphCTR = array();
			$GraphFreq = array();
			$GraphLead = array();
			$GraphLGR = array();
			$GraphSpent = array();
			
			if($client_id > 0)
			{
				$facebook_campaign_values = DB::table('facebook_campaign_values')
				->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
				->whereIn('campaign_id', array_unique($CampaignArray))
				->whereDate('campaign_date', '>=', $campaign_s_date)
				->whereDate('campaign_date', '<=', $campaign_e_date)
				->groupBy('campaign_date')->get();
				foreach($facebook_campaign_values as $facebook_campaign_value)
				{
					$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					
					$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
					$TotalSpend = DB::table('facebook_campaign_values')->select('spend')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('spend');
					$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
					$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
					if($CampaignLeads > 0)
					{
						$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
						$GraphClick[] = $PownderClick;
						$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $clientCampaignLeads;
						$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $clientCampaignLeads;
						$PownderSpent = ($TotalSpend / $CampaignLeads) * $clientCampaignLeads;
						$GraphSpent[] = $PownderSpent;
						
						if($PownderLeads == 0)
						{
							$GraphCPA[] = 0;
						}
						else
						{
							$GraphCPA[] = $PownderSpent / $PownderLeads;
						}
						if($PownderClick == 0 || $PownderLeads == 0)
						{
							$GraphLGR[] = 0;
						}
						else
						{
							$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
						}
					}
					else
					{
						$GraphClick[] = 0;
						$GraphCPA[] = 0;
						$GraphCTR[] = 0;
						$GraphFreq[] = 0;
						$GraphLGR[] = 0;
						$GraphSpent[] = 0;
					}
				}
				
				$TotalSpend = 0;
				$TotalClicks = 0;
				
				$FacebookAccounts = DB::table('facebook_campaigns')
				->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
				->select('facebook_campaigns.id', 'facebook_accounts.access_token')
				->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
				->groupBy('facebook_campaigns.id')->get();
				foreach($FacebookAccounts as $FacebookAccount)
				{
					$campaign_id = $FacebookAccount->id;
					$access_token = $FacebookAccount->access_token;
					
					//Read facebook clicks, spend
					$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($curl);
					curl_close($curl);
					
					$result = json_decode($response,true);
					
					if(isset($result['data']))
					{
						$data = $result['data'];
						if(isset($data[0]['clicks']))
						{
							$TotalClicks = $TotalClicks + $data[0]['clicks'];
							$TotalSpend = $TotalSpend + $data[0]['spend'];
						}
					}
				}
				
				$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
				$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $date_start, $date_stop);
				if($CampaignLeads > 0)
				{
					$Spend = ($TotalSpend / $CampaignLeads) * $clientCampaignLeads;
					$Click = ceil(($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
				}
			}
			else
			{
				if($ShowLead=='Allotted Leads')
				{
					$facebook_campaign_values = DB::table('facebook_campaign_values')
					->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
					->whereIn('campaign_id', array_unique($CampaignArray))
					->whereDate('campaign_date', '>=', $campaign_s_date)
					->whereDate('campaign_date', '<=', $campaign_e_date)
					->groupBy('campaign_date')->get();
					
					foreach($facebook_campaign_values as $facebook_campaign_value)
					{
						$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', Session::get('manager_id'))->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
						$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', Session::get('manager_id'))->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
						
						$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
						$managerCampaignLeads = ManagerHelper::managerCampaignLeads($client_id, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
						
						$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
						$TotalSpend = DB::table('facebook_campaign_values')->select('spend')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('spend');
						$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
						$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
						if($CampaignLeads > 0)
						{
							$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $managerCampaignLeads);
							$GraphClick[] = $PownderClick;
							$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $managerCampaignLeads;
							$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $managerCampaignLeads;
							$PownderSpent = ($TotalSpend / $CampaignLeads) * $managerCampaignLeads;
							$GraphSpent[] = $PownderSpent;
							
							if($PownderLeads == 0)
							{
								$GraphCPA[] = 0;
							}
							else
							{
								$GraphCPA[] = $PownderSpent / $PownderLeads;
							}
							if($PownderClick == 0 || $PownderLeads == 0)
							{
								$GraphLGR[] = 0;
							}
							else
							{
								$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
							}
						}
						else
						{
							$GraphClick[] = 0;
							$GraphCPA[] = 0;
							$GraphCTR[] = 0;
							$GraphFreq[] = 0;
							$GraphLGR[] = 0;
							$GraphSpent[] = 0;
						}
					}
					
					$TotalSpend = 0;
					$TotalClicks = 0;
					$FacebookAccounts = DB::table('facebook_campaigns')
					->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
					->select('facebook_campaigns.id', 'facebook_accounts.access_token')
					->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
					->groupBy('facebook_campaigns.id')->get();
					foreach($FacebookAccounts as $FacebookAccount)
					{
						$campaign_id = $FacebookAccount->id;
						$access_token = $FacebookAccount->access_token;
						
						//Read facebook clicks, spend
						$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
						$response = curl_exec($curl);
						curl_close($curl);
						
						$result = json_decode($response,true);
						
						if(isset($result['data']))
						{
							$data = $result['data'];
							if(isset($data[0]['clicks']))
							{
								$TotalClicks = $TotalClicks + $data[0]['clicks'];
								$TotalSpend = $TotalSpend + $data[0]['spend'];
							}
						}
					}
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
					$managerCampaignLeads = ManagerHelper::managerCampaignLeads(Session::get('manager_id'), array_unique($CampaignArray), $date_start, $date_stop);
					if($CampaignLeads > 0)
					{
						$Spend = ($TotalSpend / $CampaignLeads) * $managerCampaignLeads;
						$Click = ceil(($TotalClicks / $CampaignLeads) * $managerCampaignLeads);
					}
				}
				else
				{
					$facebook_campaign_values = DB::table('facebook_campaign_values')
					->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
					->whereIn('campaign_id', array_unique($CampaignArray))
					->whereDate('campaign_date', '>=', $campaign_s_date)
					->whereDate('campaign_date', '<=', $campaign_e_date)
					->groupBy('campaign_date')->get();
					
					foreach($facebook_campaign_values as $facebook_campaign_value)
					{
						$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')
						->where(function ($query) use($UserArray, $ClientArray, $ManagerArray){
							$query->whereIn('created_user_id', array_unique($UserArray))
							->whereIn('client_id', array_unique($ClientArray), 'or')
							->whereIn('manager_id', array_unique($ManagerArray), 'or');
						})
						->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)
						->count('id');
						
						$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')
						->where(function ($query) use($UserArray, $ClientArray, $ManagerArray){
							$query->whereIn('created_user_id', array_unique($UserArray))
							->whereIn('client_id', array_unique($ClientArray), 'or')
							->whereIn('manager_id', array_unique($ManagerArray), 'or');
						})
						->where('lead_type', 'Pownder™ Lead')
						->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)
						->count('id');
						
						$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
						$vendorCampaignLeads = VendorHelper::vendorCampaignLeads($UserArray, $ClientArray, $ManagerArray, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
						
						$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
						$TotalSpend = DB::table('facebook_campaign_values')->select('spend')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('spend');
						$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
						$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
						if($CampaignLeads > 0)
						{
							$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $vendorCampaignLeads);
							$GraphClick[] = $PownderClick;
							$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $vendorCampaignLeads;
							$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $vendorCampaignLeads;
							$PownderSpent = ($TotalSpend / $CampaignLeads) * $vendorCampaignLeads;
							$GraphSpent[] = $PownderSpent;
							
							if($PownderLeads == 0)
							{
								$GraphCPA[] = 0;
							}
							else
							{
								$GraphCPA[] = $PownderSpent / $PownderLeads;
							}
							if($PownderClick == 0 || $PownderLeads == 0)
							{
								$GraphLGR[] = 0;
							}
							else
							{
								$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
							}
						}
						else
						{
							$GraphClick[] = 0;
							$GraphCPA[] = 0;
							$GraphCTR[] = 0;
							$GraphFreq[] = 0;
							$GraphLGR[] = 0;
							$GraphSpent[] = 0;
						}
					}
					
					$TotalSpend = 0;
					$TotalClicks = 0;
					$FacebookAccounts = DB::table('facebook_campaigns')
					->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
					->select('facebook_campaigns.id', 'facebook_accounts.access_token')
					->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
					->groupBy('facebook_campaigns.id')->get();
					foreach($FacebookAccounts as $FacebookAccount)
					{
						$campaign_id = $FacebookAccount->id;
						$access_token = $FacebookAccount->access_token;
						
						//Read facebook clicks, spend
						$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
						$response = curl_exec($curl);
						curl_close($curl);
						
						$result = json_decode($response,true);
						
						if(isset($result['data']))
						{
							$data = $result['data'];
							if(isset($data[0]['clicks']))
							{
								$TotalClicks = $TotalClicks + $data[0]['clicks'];
								$TotalSpend = $TotalSpend + $data[0]['spend'];
							}
						}
					}
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
					$vendorCampaignLeads = VendorHelper::vendorCampaignLeads($UserArray, $ClientArray, $ManagerArray, array_unique($CampaignArray), $date_start, $date_stop);
					if($CampaignLeads > 0)
					{
						$Spend = ($TotalSpend / $CampaignLeads) * $vendorCampaignLeads;
						$Click = ceil(($TotalClicks / $CampaignLeads) * $vendorCampaignLeads);
					}
				}
			}
			
			$FacebookAccounts = DB::table('facebook_accounts')
			->leftJoin('facebook_ads_accounts', 'facebook_accounts.id', '=', 'facebook_ads_accounts.facebook_account_id')
			->select('facebook_accounts.id', 'facebook_accounts.access_token', 'facebook_ads_accounts.facebook_id')
			->where('facebook_accounts.updated_user_id', $FacebookUserId)
			->where('facebook_accounts.is_deleted', 0)
			->where('facebook_ads_accounts.is_deleted', 0)
			->groupBy('facebook_ads_accounts.facebook_id')
			->get();
			foreach($FacebookAccounts as $FacebookAccount)
			{
				$facebook_account_id = $FacebookAccount->id;
				$access_token = $FacebookAccount->access_token;
				
				$ads_account = $FacebookAccount->facebook_id;
				
				//Read facebook reach
				$curl1 = curl_init('https://graph.facebook.com/v2.10/'.$ads_account.'/insights?&date_preset=lifetime&fields=reach&access_token='.$access_token.'');
				curl_setopt($curl1, CURLOPT_RETURNTRANSFER, 1);
				$response1 = curl_exec($curl1);
				curl_close($curl1);
				
				$result1 = json_decode($response1,true);
				
				if(isset($result1['data']))
				{
					$data1 = $result1['data'];
					if(isset($data1[0]['reach']))
					{
						$Reach = $Reach+$data1[0]['reach'];
					}
				}
			}
			
			$FacebookAccounts = DB::table('facebook_accounts')->where('updated_user_id', $FacebookUserId)->where('is_deleted', 0)->get();
			foreach($FacebookAccounts as $FacebookAccount)
			{
				$facebook_account_id = $FacebookAccount->id;
				$access_token = $FacebookAccount->access_token;
				
				$facebook_pages = DB::table('facebook_pages')->select('id')->where('facebook_account_id', $facebook_account_id)->where('is_blocked', 0)->where('is_deleted', 0)->get();
				foreach($facebook_pages as $facebook_page)
				{
					$facebook_page_id = $facebook_page->id;
					$curl2 = curl_init('https://graph.facebook.com/v2.10/'.$facebook_page_id.'?fields=fan_count&access_token='.$access_token);
					curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
					$response2 = curl_exec($curl2);
					curl_close($curl2);
					
					$result2 = json_decode($response2,true);
					
					if(isset($result2['fan_count']))
					{
						$Likes = $Likes + $result2['fan_count'];
					}
				}
			}
			
			return view('panel.frontend.vendor_dashboard',['Columns' => $VendorColumns, 'WritePermission' => $WritePermission, 'CityLeads' => $CityLeads, 'QueryClients' => $QueryClients, 'clients' => $clients, 'Reach' => $Reach, 'Click' => $Click, 'Likes' => $Likes, 'Spend' => $Spend, 'TodayLead' => $TodayLead, 'TotalLead' => $TotalLead, 'facebook_ads_lead_users' => $facebook_ads_lead_users, 'facebook_campaign_values' => $facebook_campaign_values, 'GraphCPA' => $GraphCPA, 'GraphCTR' => $GraphCTR, 'GraphClick' => $GraphClick, 'GraphFreq' => $GraphFreq, 'GraphLGR' => $GraphLGR, 'GraphLead' => $GraphLead, 'GraphSpent' => $GraphSpent, 'UnassignLeads' => $UnassignLeads]);
		}
		
		public function getVendor(Request $request)
		{
			$UserArray = array();
			$WritePermission = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				$WritePermission = ManagerHelper::ManagerWritePermission('Vendors Read Write');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			
			$Vendor=array();
			if($request->has('id'))
			{
				$Vendor=DB::table('vendors')->whereIn('created_user_id', array_unique($UserArray))->where('is_deleted', 0)->where('id',$request->id)->first();
				
				if(is_null($Vendor))
				{
					return redirect('vendors')->with('alert_danger','Unauthorized vendor access failed');
				}
			}
			
			$VendorColumns = array();
			$Columns = DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Vendor Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$VendorColumns[] = $Column->column_value;
			}
			
			$VendorPackages = DB::table('vendor_packages')->select('id','name')->whereIn('created_user_id', array_unique($UserArray))->where('is_deleted',0)->orderBy('name', 'ASC')->get();
			
			return view('panel.frontend.vendors',['VendorPackages'=>$VendorPackages, 'VendorColumns'=>$VendorColumns, 'Vendor'=>$Vendor, 'WritePermission'=>$WritePermission]);
		}  
		
		public function getVendorAjax(Request $request)
		{
			$order = isset($request->order)?$request->order:'ASC';
			$sortString = isset($request->sort)?$request->sort:'Vendor Name';
			$search = isset($request->search)?$request->search:'';
			$offset = isset($request->offset)?$request->offset:'0';
			$limit = isset($request->limit)?$request->limit:'9999999999';
			$status = isset($request->status)?$request->status:'1';
			
			if($limit == 'All' )
			{
				$limit = 9999999999;
			}
			
			$start_date = '';
			$end_date = '';
			$dateRange = isset($request->dateRange)?$request->dateRange:'';
			if($dateRange != '')
			{
				$explodeDate = explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			switch($sortString) 
			{
				case 'Date':
				$sort = 'vendors.created_at';
				break;
				case 'Status':
				$sort = 'vendors.status';
				break;
				case 'Vendor Name':
				$sort = 'vendors.name';
				break;
				case 'Package':
				$sort = 'vendor_packages.name';
				break;
				case 'Clients':
				$sort = 'ClientCount';
				break;
				case 'Ads':
				$sort = '';
				break;
				case 'CP\'s':
				$sort = '';
				break;
				case 'LD\'s':
				$sort = '';
				break;
				case 'FB Lists':
				$sort = '';
				break;
				case 'FB Messages':
				$sort = '';
				break;
				default:
				$sort = 'vendors.name';
			}
			
			$data=array();
			$rows=array();
			
			$columns = ['vendor_packages.name', 'vendors.name', 'vendors.created_at', 'vendors.status', 'ClientCount'];
			
			$TotalVendors = DB::table('vendors')
			->leftJoin('vendor_packages', 'vendors.vendor_package_id', '=', 'vendor_packages.id')
			->leftJoin('clients', function ($join) {
				$join->on('vendors.id', '=', 'clients.vendor_id')
				->where('clients.is_deleted', 0);
			})
			->select('vendors.*', 'vendor_packages.name as package_name', DB::raw("count(clients.id) as ClientCount"))
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('vendors.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('vendors.created_at', '=', $start_date);
					}
					else
					{
						$query->whereDate('vendors.created_at', '>=', $start_date)
						->whereDate('vendors.created_at', '<=', $end_date);
					}
				}
			})
			->where('vendors.status', $status)
			->where('vendors.is_deleted', 0)
			->groupBy('vendors.id')
			->get();
			
			$vendors = DB::table('vendors')
			->leftJoin('vendor_packages', 'vendors.vendor_package_id', '=', 'vendor_packages.id')
			->leftJoin('clients', function ($join) {
				$join->on('vendors.id', '=', 'clients.vendor_id')
				->where('clients.is_deleted', 0);
			})
			->select('vendors.*', 'vendor_packages.name as package_name', DB::raw("count(clients.id) as ClientCount"))
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('vendors.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('vendors.created_at', '=', $start_date);
					}
					else
					{
						$query->whereDate('vendors.created_at', '>=', $start_date)
						->whereDate('vendors.created_at', '<=', $end_date);
					}
				}
			})
			->where('vendors.status', $status)
			->where('vendors.is_deleted', 0)
			->groupBy('vendors.id')
			->orderBy($sort, $order)
			->skip($offset)
			->take($limit)
			->get();
			
			$sno=$offset+1;
			foreach($vendors as $vendor)
			{
				$user = array();
				$user['#'] = $sno++;
				$user[''] = '<input type="checkbox" class="vendor_id" value="'.$vendor->id.'" />';
				$user['Date'] = date('m-d-Y',strtotime($vendor->created_at));
				if($vendor->status==1){ 
					$user['Status'] = 'Active';
					}else{
					$user['Status'] = 'Inactive';
				}
				$user['Vendor Name'] = $vendor->name;
				$user['Package'] = $vendor->package_name;
				$user['Clients'] = $vendor->ClientCount;
				$user['Ads'] = 52;
				
				$user['CP\'s'] = 0;
				$user['LD\'s'] = 54;
				
				$user['FB Lists'] = 42;
				$user['FB Messages'] = 45;
				$LoginString = '<a href="vendors?id='.$vendor->id.'"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit Vendor"></i></a>     <a href="#" onClick="VendorDelete('.$vendor->id.')"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete Vendor"></i></a>';
				
				$user['Login'] = $LoginString;
				$rows[] = $user;
			}		
			
			$data['total'] = count($TotalVendors);
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
		
		public function addVendorSubmit(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Vendors Read Write');
			}
			
			$rule=[
			'name'=>'required',
			'address'=>'required',
			'zip'=>'required|integer|zip_code_valid',
			'mobile'=>'required|phone_format|unique:vendors,mobile',
			'email'=>'required|unique:users,email|email',
			'url'=>'required|url',
			'invoice_email'=> 'nullable|email',
			'contact_name'=>'required',
			'vendor_package_id'=>'required',
			'start_date'=>'required',
			'pro_rate'=>'required',
			'photo'=>'image|dimensions:width=180,height=180',
			];
			
			$this->validate($request,$rule);
			
			$photo='';
			if ($request->hasFile('photo')) 
			{
				$image=$request->file('photo');
				$photo=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/vendors/',$photo);
			}
			
			$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($request->zip)."&sensor=false";
			$result_string = file_get_contents($url);
			$result = json_decode($result_string, true);
			$val = isset($result['results'][0]['geometry']['location']) ? $result['results'][0]['geometry']['location'] : array();
			$latitude = isset($val['lat']) ? $val['lat'] : '';
			$longitude = isset($val['lng']) ? $val['lng'] : '';
			
			
			$Address = Helper::get_zip_info($request->zip);
			$city=$Address['city'];
			$state=$Address['state'];
			
			$s_date = explode("-",$request->start_date);
			$start_date = $s_date[2].'-'.$s_date[0].'-'.$s_date[1];
			
			$vendor_id = DB::table('vendors')->insertGetId([
			'name'=>$request->name,
			'address'=>$request->address,
			'city' => $city,
			'state' => $state,
			'zip'=>$request->zip,
			'mobile'=>$request->mobile,
			'email'=>$request->email,
			'invoice_email'=>$request->invoice_email,
			'url'=>$request->url,
			'contact_name'=>$request->contact_name,
			'vendor_package_id'=>$request->vendor_package_id,
			'photo'=>$photo,
			'start_date'=>$start_date,
			'pro_rate'=>$request->pro_rate,
			'latitude'=>$latitude,
			'longitude'=>$longitude,
			'created_user_id'=>Session::get('user_id'),
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_user_id'=>Session::get('user_id'),
			'updated_at'=>date('Y-m-d H:i:s')
			]);
			
			do 
			{
				$user_password = Helper::GenerateRandomPassword(8,true,true,true,true);
			}
			while($user_password=="FALSE");
			
			$password=password_hash($user_password,PASSWORD_BCRYPT); 
			
			DB::table('users')->insert([
			'vendor_id'=>$vendor_id,
			'name'=>$request->name,
			'email'=>$request->email,
			'address'=>$request->address,
			'password'=>$password,
			'category'=>'vendor',
			'created_user_id'=>Session::get('user_id'),
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_user_id'=>Session::get('user_id'),
			'updated_at'=>date('Y-m-d H:i:s')
			]);
			
			$data = array('name' => $request->name, 'email' => $request->email, 'user_password' => $user_password);
			
			Mail::send('emails.login_details', $data, function ($message) use ($request) {
				$message->from('noreply@pownder.com', 'Pownder');
				$message->to($request->email)->subject('Pownder™ Credentials');
			});
			
			return Redirect::back()->with('alert_success','Vendor added successfully');        
		}
		
		public function editVendorSubmit(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Vendors Read Write');
			}
			
			$user=DB::table('users')->where('vendor_id',$request->vendor_id)->first();
			
			$rule=[
			'vendor_name'=>'required',
			'vendor_address'=>'required',
			'vendor_zip'=>'required|integer|zip_code_valid',
			'vendor_mobile'=>'required|phone_format|unique:vendors,mobile,'.$request->vendor_id.'',
			'vendor_email'=>'required|unique:users,email,'.$user->id.'|email',
			'invoice_email'=>'nullable|email',
			'vendor_url'=>'required|url',
			'vendor_contact_name'=>'required',
			'vendor_package'=>'required',
			'vendor_start_date'=>'required',
			'vendor_pro_rate'=>'required',
			'vendor_photo'=>'image|dimensions:width=180,height=180',
			];
			
			$this->validate($request,$rule);
			
			$photo='';
			if ($request->hasFile('vendor_photo')) 
			{
				$image=$request->file('vendor_photo');
				$photo=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/vendors/',$photo);
			}
			else
			{
				$vendor_result=DB::table('vendors')->select('photo')->where('id', $request->vendor_id)->first();
				$photo=$vendor_result->photo;
			}
			
			$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($request->vendor_zip)."&sensor=false";
			$result_string = file_get_contents($url);
			$result = json_decode($result_string, true);
			$val = isset($result['results'][0]['geometry']['location']) ? $result['results'][0]['geometry']['location'] : array();
			$latitude = isset($val['lat']) ? $val['lat'] : '';
			$longitude = isset($val['lng']) ? $val['lng'] : '';
			
			
			$Address = Helper::get_zip_info($request->vendor_zip);
			$city=$Address['city'];
			$state=$Address['state'];
			
			$s_date = explode("-",$request->vendor_start_date);
			$start_date = $s_date[2].'-'.$s_date[0].'-'.$s_date[1];
			
			$vendor_id = DB::table('vendors')->where('id', $request->vendor_id)->where('status', 1)->where('is_deleted', 0)
			->update(['name'=>$request->vendor_name,
			'address'=>$request->vendor_address,
			'city'=>$city,
			'state'=>$state,
			'zip'=>$request->vendor_zip,
			'mobile'=>$request->vendor_mobile,
			'email'=>$request->vendor_email,
			'invoice_email'=>$request->invoice_email,
			'url'=>$request->vendor_url,
			'contact_name'=>$request->vendor_contact_name,
			'vendor_package_id'=>$request->vendor_package,
			'photo'=>$photo,
			'start_date'=>$start_date,
			'pro_rate'=>$request->vendor_pro_rate,
			'latitude'=>$latitude,
			'longitude'=>$longitude,
			'updated_user_id'=>Session::get('user_id'),
			'updated_at'=>date('Y-m-d H:i:s')]);
			
			DB::table('users')->where('vendor_id', $request->vendor_id)
			->update(['name' => $request->vendor_name, 'email' => $request->vendor_email, 'address' => $request->vendor_address, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
			
			return redirect('vendors')->with('alert_success','Vendor updated successfully');        
		}
		
		public function deleteVendor(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Vendors Read Write');
			}
			
			DB::table('vendors')->where('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			
			$user = DB::table('users')->where('vendor_id', $request->id)->first();
			
			DB::table('users')->where('vendor_id', $request->id)
			->update(['email' => $user->email.'(del)', 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			
			return redirect('vendors')->with('alert_success','Vendor deleted successfully');        
		}
		
		public function editVendor()
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Vendors Read Write');
			}
			
			$VendorPackages=DB::table('vendor_packages')->select('id','name')->orderBy('name', 'ASC')->get();
			
			$vendor=DB::table('vendors')
			->leftJoin('vendor_packages', 'vendors.vendor_package_id', '=', 'vendor_packages.id')
			->select('vendors.*', 'vendor_packages.name as package_name')
			->where('vendors.id',Session::get('vendor_id'))->first();
			
			return view('panel.frontend.edit_vendor',['VendorPackages'=>$VendorPackages, 'vendor'=>$vendor]);
		}
		
		public function editVendorProfile(Request $request)
		{
			$rule=[
			'name'=>'required',
			'address'=>'required',
			'zip'=>'required|integer|zip_code_valid',
			'mobile'=>'required|phone_format|unique:vendors,mobile,'.Session::get('vendor_id').'',
			'email'=>'required|email|unique:users,email,'.Session::get('user_id').'',
			'url'=>'required|url',
			'invoice_email'=>'nullable|email',
			'contact_name'=>'required',
			'photo'=>'image|dimensions:width=180,height=180',
			];
			
			$this->validate($request,$rule);
			
			if ($request->hasFile('photo')) 
			{
				$image=$request->file('photo');
				$photo=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/vendors/',$photo);
			}
			else
			{
				$vendor_result=DB::table('vendors')->select('photo')->where('id', Session::get('vendor_id'))->first();
				$photo=$vendor_result->photo;
			}
			
			$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($request->zip)."&sensor=false";
			$result_string = file_get_contents($url);
			$result = json_decode($result_string, true);
			$val = isset($result['results'][0]['geometry']['location']) ? $result['results'][0]['geometry']['location'] : array();
			$latitude = isset($val['lat']) ? $val['lat'] : '';
			$longitude = isset($val['lng']) ? $val['lng'] : '';
			
			
			$Address = Helper::get_zip_info($request->zip);
			$city=$Address['city'];
			$state=$Address['state'];
			
			DB::table('vendors')
			->where('id', Session::get('vendor_id'))
			->update([
			'name'=>$request->name,
			'email'=>$request->email,
			'address'=>$request->address,
			'city'=>$city,
			'state'=>$state,
			'zip'=>$request->zip,
			'mobile'=>$request->mobile,
			'url'=>$request->url,
			'invoice_email'=>$request->invoice_email,
			'contact_name'=>$request->contact_name,
			'photo'=>$photo,
			'latitude'=>$latitude,
			'longitude'=>$longitude,
			'updated_user_id'=>Session::get('user_id'),
			'updated_at'=>date('Y-m-d H:i:s')
			]);
			
			$user=DB::table('users')->where('id',Session::get('user_id'))->first();
			if($request->email==$user->email)
			{
				DB::table('users')->where('id', Session::get('user_id'))
				->update(['name' => $request->name, 'address' => $request->address, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			else
			{
				do 
				{
					$user_password = Helper::GenerateRandomPassword(8,true,true,true,true);
				}
				while($user_password=="FALSE");
				
				$password=password_hash($user_password,PASSWORD_BCRYPT); 
				
				DB::table('users')->where('id', Session::get('user_id'))
				->update(['name' => $request->name, 'email' => $request->email, 'password' => $password, 'address' => $request->address, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
				
				$data = array('name' => $request->name, 'email' => $request->email, 'user_password' => $user_password);
				
				Mail::send('emails.login_details', $data, function ($message) use ($request) {
					$message->from('noreply@pownder.com', 'Pownder');
					$message->to($request->email)->subject('Pownder™ Credentials');
				});
				
				Session::put('EmailChange','Yes');
			}
			
			Session::put('user_name',$request->name);
			Session::put('photo',$photo);
			
			return Redirect::back()->with('alert_success','Profile updated successfully'); 
		}
		
		public function changePassword(Request $request)
		{
			$rule = ['password' => 'required|min:8|confirmed', 'password_confirmation' => 'required'];
			
			$this->validate($request,$rule);
			
			DB::table('users')->where('id', Session::get('user_id'))
			->update(['password' => password_hash($request->password,PASSWORD_BCRYPT), 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
			
			return Redirect::back()->with('alert_success','Password changed successfully'); 
		}
		
		public function getVendorName(Request $request)
		{
			$Vendors = DB::table('users')->where('category', 'vendor')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return response()->json($Vendors);
		}
		
		public function getVendorDashboardAjax(Request $request)
		{
			$UserArray = array();
			$ClientArray = array();
			$ManagerArray = array();
			$ShowClient = '';
			$ShowLead = '';
			$WritePermission = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$ShowClient = ManagerHelper::ManagerClientMenuAction();
				$WritePermission = ManagerHelper::ManagerWritePermission('Dashboard Read Write');
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				
				$UserArray[] = Session::get('user_id');
				$ManagerArray[] = Session::get('manager_id');
				
				$Manager = DB::table('managers')
				->leftJoin('users', 'managers.vendor_id', '=', 'users.vendor_id')
				->select('users.id', 'users.vendor_id')
				->where('managers.id', Session::get('manager_id'))
				->first();
				
				$vendor_id = $Manager->vendor_id;
				
				$UserArray[] = $Manager->id;
				
				$FacebookUserId = $Manager->id;
				
				$Users = DB::table('managers')
				->leftJoin('users', 'managers.id', '=', 'users.manager_id')
				->select('users.id', 'users.manager_id')
				->where('managers.vendor_id', $vendor_id)
				->get();
				foreach($Users as $User)
				{
					if($User->id > 0)
					{
						$UserArray[] = $User->id;
					}
					if($User->manager_id > 0)
					{
						$ManagerArray[] = $User->manager_id;
					}
				}
				
				$Clients = DB::table('clients')
				->leftJoin('users', 'clients.id', '=', 'users.client_id')
				->select('users.id', 'users.client_id')
				->where('clients.vendor_id', $vendor_id)
				->get();
				foreach($Clients as $Client)
				{
					$ClientArray[] = $Client->client_id;
					$UserArray[] = $Client->id;
					$Managers = DB::table('managers')
					->leftJoin('users', 'managers.id', '=', 'users.manager_id')
					->select('users.id', 'users.manager_id')
					->where('managers.client_id', $Client->client_id)
					->get();
					foreach($Managers as $Manager)
					{
						if($Manager->id > 0)
						{
							$UserArray[] = $Manager->id;
						}
						if($Manager->manager_id > 0)
						{
							$ManagerArray[] = $Manager->manager_id;
						}
					}
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$FacebookUserId = Session::get('user_id');
				$vendor_id = Session::get('vendor_id');
				
				$Users = DB::table('managers')
				->leftJoin('users', 'managers.id', '=', 'users.manager_id')
				->select('users.id', 'users.manager_id')
				->where('managers.vendor_id', $vendor_id)
				->get();
				foreach($Users as $User)
				{
					if($User->id > 0)
					{
						$UserArray[] = $User->id;
					}
					if($User->manager_id > 0)
					{
						$ManagerArray[] = $User->manager_id;
					}
				}
				
				$Clients = DB::table('clients')
				->leftJoin('users', 'clients.id', '=', 'users.client_id')
				->select('users.id', 'users.client_id')
				->where('clients.vendor_id', $vendor_id)
				->get();
				foreach($Clients as $Client)
				{
					$UserArray[] = $Client->id;
					$ClientArray[] = $Client->client_id;
					$Managers = DB::table('managers')
					->leftJoin('users', 'managers.id', '=', 'users.manager_id')
					->select('users.id', 'users.manager_id')
					->where('managers.client_id', $Client->client_id)
					->get();
					foreach($Managers as $Manager)
					{
						if($Manager->id > 0)
						{
							$UserArray[] = $Manager->id;
						}
						if($Manager->manager_id > 0)
						{
							$ManagerArray[] = $Manager->manager_id;
						}
					}
				}
			}
			
			$date_start = $request->date_start;
			$date_stop = $request->date_stop;
			$client_id = $request->client_id;
			
			$datediff = floor((strtotime($date_stop) - strtotime($date_start))/(60 * 60 * 24));
			
			if($datediff<=30)
			{
				$campaign_s_date = 	$date_start;
				$campaign_e_date = 	$date_stop;
			}
			else
			{
				$campaign_s_date = 	date('Y-m').'-01';
				$campaign_e_date = date('Y-m-d');
			}
			
			$CPA = 0;
			$Spend = 0;
			$Click = 0;
			
			$CampaignArray = array();
			$client_campaigns = DB::table('facebook_ads_lead_users')->select('campaign_id')
			->where(function ($query) use($UserArray, $ClientArray, $ManagerArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->whereIn('created_user_id', array_unique($UserArray))
					->whereIn('client_id', array_unique($ClientArray), 'or')
					->whereIn('manager_id', array_unique($ManagerArray), 'or');
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->whereDate('created_time', '>=', $campaign_s_date)
			->whereDate('created_time', '<=', $campaign_e_date)
			->where('campaign_id', '!=', 0)
			->groupBy('campaign_id')
			->get();
			foreach($client_campaigns as $client_campaign)
			{
				$CampaignArray[] = $client_campaign->campaign_id;
			}
			
			$CityLeads = DB::table('facebook_ads_lead_users')->selectRaw('count(id) as Leads, city, latitude, longitude')
			->where(function ($query) use($UserArray, $ClientArray, $ManagerArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->whereIn('created_user_id', array_unique($UserArray))
					->whereIn('client_id', array_unique($ClientArray), 'or')
					->whereIn('manager_id', array_unique($ManagerArray), 'or');
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->where('city', '!=', '')
			->where('latitude', '!=', '')
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->groupBy('latitude')
			->groupBy('longitude')
			->get();
			
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($UserArray, $ClientArray, $ManagerArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->whereIn('created_user_id', array_unique($UserArray))
					->whereIn('client_id', array_unique($ClientArray), 'or')
					->whereIn('manager_id', array_unique($ManagerArray), 'or');
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->where('lead_type', 'Pownder™ Lead')
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$Leads = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($UserArray, $ClientArray, $ManagerArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->whereIn('created_user_id', array_unique($UserArray))
					->whereIn('client_id', array_unique($ClientArray), 'or')
					->whereIn('manager_id', array_unique($ManagerArray), 'or');
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$AllLeads = array();
			$FacebookLeads = array();
			$GraphClick = array();
			$GraphCPA = array();
			$GraphCTR = array();
			$GraphFreq = array();
			$GraphLead = array();
			$GraphLGR = array();
			$GraphSpent = array();
			
			if($client_id > 0)
			{
				$facebook_campaign_values = DB::table('facebook_campaign_values')
				->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
				->whereIn('campaign_id', array_unique($CampaignArray))
				->whereDate('campaign_date', '>=', $campaign_s_date)
				->whereDate('campaign_date', '<=', $campaign_e_date)
				->groupBy('campaign_date')->get();
				foreach($facebook_campaign_values as $facebook_campaign_value)
				{
					$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					
					$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
					$TotalSpend = DB::table('facebook_campaign_values')->select('spend')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('spend');
					$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
					$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
					if($CampaignLeads > 0)
					{
						$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
						$GraphClick[] = $PownderClick;
						$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $clientCampaignLeads;
						$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $clientCampaignLeads;
						$PownderSpent = ($TotalSpend / $CampaignLeads) * $clientCampaignLeads;
						$GraphSpent[] = $PownderSpent;
						
						if($PownderLeads == 0)
						{
							$GraphCPA[] = 0;
						}
						else
						{
							$GraphCPA[] = $PownderSpent / $PownderLeads;
						}
						if($PownderClick == 0 || $PownderLeads == 0)
						{
							$GraphLGR[] = 0;
						}
						else
						{
							$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
						}
					}
					else
					{
						$GraphClick[] = 0;
						$GraphCPA[] = 0;
						$GraphCTR[] = 0;
						$GraphFreq[] = 0;
						$GraphLGR[] = 0;
						$GraphSpent[] = 0;
					}
				}
				
				$TotalSpend = 0;
				$TotalClicks = 0;
				$FacebookAccounts = DB::table('facebook_campaigns')
				->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
				->select('facebook_campaigns.id', 'facebook_accounts.access_token')
				->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
				->groupBy('facebook_campaigns.id')->get();
				foreach($FacebookAccounts as $FacebookAccount)
				{
					$campaign_id = $FacebookAccount->id;
					$access_token = $FacebookAccount->access_token;
					
					//Read facebook clicks, spend
					$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($curl);
					curl_close($curl);
					
					$result = json_decode($response,true);
					
					if(isset($result['data']))
					{
						$data = $result['data'];
						if(isset($data[0]['clicks']))
						{
							$TotalClicks = $TotalClicks + $data[0]['clicks'];
							$TotalSpend = $TotalSpend + $data[0]['spend'];
						}
					}
				}
				
				$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
				$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $date_start, $date_stop);
				if($CampaignLeads > 0)
				{
					$Spend = ($TotalSpend / $CampaignLeads) * $clientCampaignLeads;
					$Click = ceil(($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
				}
			}
			else
			{
				if($ShowLead=='Allotted Leads')
				{
					$facebook_campaign_values = DB::table('facebook_campaign_values')
					->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
					->whereIn('campaign_id', array_unique($CampaignArray))
					->whereDate('campaign_date', '>=', $campaign_s_date)
					->whereDate('campaign_date', '<=', $campaign_e_date)
					->groupBy('campaign_date')->get();
					
					foreach($facebook_campaign_values as $facebook_campaign_value)
					{
						$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', Session::get('manager_id'))->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
						$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', Session::get('manager_id'))->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
						
						$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
						$managerCampaignLeads = ManagerHelper::managerCampaignLeads($client_id, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
						
						$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
						$TotalSpend = DB::table('facebook_campaign_values')->select('spend')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('spend');
						$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
						$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
						if($CampaignLeads > 0)
						{
							$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $managerCampaignLeads);
							$GraphClick[] = $PownderClick;
							$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $managerCampaignLeads;
							$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $managerCampaignLeads;
							$PownderSpent = ($TotalSpend / $CampaignLeads) * $managerCampaignLeads;
							$GraphSpent[] = $PownderSpent;
							
							if($PownderLeads == 0)
							{
								$GraphCPA[] = 0;
							}
							else
							{
								$GraphCPA[] = $PownderSpent / $PownderLeads;
							}
							if($PownderClick == 0 || $PownderLeads == 0)
							{
								$GraphLGR[] = 0;
							}
							else
							{
								$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
							}
						}
						else
						{
							$GraphClick[] = 0;
							$GraphCPA[] = 0;
							$GraphCTR[] = 0;
							$GraphFreq[] = 0;
							$GraphLGR[] = 0;
							$GraphSpent[] = 0;
						}
					}
					
					$TotalSpend = 0;
					$TotalClicks = 0;
					$FacebookAccounts = DB::table('facebook_campaigns')
					->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
					->select('facebook_campaigns.id', 'facebook_accounts.access_token')
					->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
					->groupBy('facebook_campaigns.id')->get();
					foreach($FacebookAccounts as $FacebookAccount)
					{
						$campaign_id = $FacebookAccount->id;
						$access_token = $FacebookAccount->access_token;
						
						//Read facebook clicks, spend
						$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
						$response = curl_exec($curl);
						curl_close($curl);
						
						$result = json_decode($response,true);
						
						if(isset($result['data']))
						{
							$data = $result['data'];
							if(isset($data[0]['clicks']))
							{
								$TotalClicks = $TotalClicks + $data[0]['clicks'];
								$TotalSpend = $TotalSpend + $data[0]['spend'];
							}
						}
					}
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
					$managerCampaignLeads = ManagerHelper::managerCampaignLeads(Session::get('manager_id'), array_unique($CampaignArray), $date_start, $date_stop);
					if($CampaignLeads > 0)
					{
						$Spend = ($TotalSpend / $CampaignLeads) * $managerCampaignLeads;
						$Click = ceil(($TotalClicks / $CampaignLeads) * $managerCampaignLeads);
					}
				}
				else
				{
					$facebook_campaign_values = DB::table('facebook_campaign_values')
					->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
					->whereIn('campaign_id', array_unique($CampaignArray))
					->whereDate('campaign_date', '>=', $campaign_s_date)
					->whereDate('campaign_date', '<=', $campaign_e_date)
					->groupBy('campaign_date')->get();
					
					foreach($facebook_campaign_values as $facebook_campaign_value)
					{
						$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')
						->where(function ($query) use($UserArray, $ClientArray, $ManagerArray){
							$query->whereIn('created_user_id', array_unique($UserArray))
							->whereIn('client_id', array_unique($ClientArray), 'or')
							->whereIn('manager_id', array_unique($ManagerArray), 'or');
						})
						->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)
						->count('id');
						
						$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')
						->where(function ($query) use($UserArray, $ClientArray, $ManagerArray){
							$query->whereIn('created_user_id', array_unique($UserArray))
							->whereIn('client_id', array_unique($ClientArray), 'or')
							->whereIn('manager_id', array_unique($ManagerArray), 'or');
						})
						->where('lead_type', 'Pownder™ Lead')
						->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)
						->count('id');
						
						$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
						$vendorCampaignLeads = VendorHelper::vendorCampaignLeads($UserArray, $ClientArray, $ManagerArray, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
						
						$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
						$TotalSpend = DB::table('facebook_campaign_values')->select('spend')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('spend');
						$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
						$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
						if($CampaignLeads > 0)
						{
							$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $vendorCampaignLeads);
							$GraphClick[] = $PownderClick;
							$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $vendorCampaignLeads;
							$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $vendorCampaignLeads;
							$PownderSpent = ($TotalSpend / $CampaignLeads) * $vendorCampaignLeads;
							$GraphSpent[] = $PownderSpent;
							
							if($PownderLeads == 0)
							{
								$GraphCPA[] = 0;
							}
							else
							{
								$GraphCPA[] = $PownderSpent / $PownderLeads;
							}
							if($PownderClick == 0 || $PownderLeads == 0)
							{
								$GraphLGR[] = 0;
							}
							else
							{
								$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
							}
						}
						else
						{
							$GraphClick[] = 0;
							$GraphCPA[] = 0;
							$GraphCTR[] = 0;
							$GraphFreq[] = 0;
							$GraphLGR[] = 0;
							$GraphSpent[] = 0;
						}
					}
					
					$TotalSpend = 0;
					$TotalClicks = 0;
					$FacebookAccounts = DB::table('facebook_campaigns')
					->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
					->select('facebook_campaigns.id', 'facebook_accounts.access_token')
					->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
					->groupBy('facebook_campaigns.id')->get();
					foreach($FacebookAccounts as $FacebookAccount)
					{
						$campaign_id = $FacebookAccount->id;
						$access_token = $FacebookAccount->access_token;
						
						//Read facebook clicks, spend
						$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
						$response = curl_exec($curl);
						curl_close($curl);
						
						$result = json_decode($response,true);
						
						if(isset($result['data']))
						{
							$data = $result['data'];
							if(isset($data[0]['clicks']))
							{
								$TotalClicks = $TotalClicks + $data[0]['clicks'];
								$TotalSpend = $TotalSpend + $data[0]['spend'];
							}
						}
					}
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
					$vendorCampaignLeads = VendorHelper::vendorCampaignLeads($UserArray, $ClientArray, $ManagerArray, array_unique($CampaignArray), $date_start, $date_stop);
					if($CampaignLeads > 0)
					{
						$Spend = ($TotalSpend / $CampaignLeads) * $vendorCampaignLeads;
						$Click = ceil(($TotalClicks / $CampaignLeads) * $vendorCampaignLeads);
					}
				}
			}
			
			$Array = array();
			
			$Array['Spend'] = '$'.Helper::NumberFormat(number_format($Spend,2));
			
			$Array['Leads'] = Helper::NumberFormat(number_format($Leads,2));
			
			$Array['CityLeads'] = $CityLeads;
			$Array['facebook_campaign_values'] = $facebook_campaign_values;
			$Array['GraphCPA'] = $GraphCPA;
			$Array['GraphCTR'] = $GraphCTR;
			$Array['GraphClick'] = $GraphClick;
			$Array['GraphFreq'] = $GraphFreq;
			$Array['GraphLGR'] = $GraphLGR;
			$Array['GraphLead'] = $GraphLead;
			$Array['GraphSpent'] = $GraphSpent;
			
			if($Click == 0 || $facebook_ads_lead_users == 0){
				$Array['LGR'] = '0%';
				}else{
				$Array['LGR'] = Helper::NumberFormat(number_format(($facebook_ads_lead_users/$Click)*100,2)).'%';
			}
			
			if($Spend == 0 || $facebook_ads_lead_users == 0){
				$Array['CPA'] = '$0';
				}else{
				$Array['CPA'] = '$'.Helper::NumberFormat(number_format($Spend/$facebook_ads_lead_users,2));
			}
			
			return response()->json($Array);
		}
		
		public function inactiveBulkVendor(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Vendors Read Write');
			}
			
			DB::table('vendors')->whereIn('id', array_unique($request->id))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 0]);
			
			DB::table('users')->whereIn('vendor_id', array_unique($request->id))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 0]);
		}
		
		public function activeBulkVendor(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Vendors Read Write');
			}
			
			DB::table('vendors')->whereIn('id', array_unique($request->id))
			->update(['updated_user_id'=>Session::get('user_id'), 'updated_at'=>date('Y-m-d H:i:s'), 'status'=>1]);
			
			DB::table('users')->whereIn('vendor_id', array_unique($request->id))
			->update(['updated_user_id'=>Session::get('user_id'), 'updated_at'=>date('Y-m-d H:i:s'), 'status'=>1]);
		}
		
		public function getVendorDashboardClient(Request $request)
		{
			$order = isset($request->order)?$request->order:'DESC';
			$sortString = isset($request->sort)?$request->sort:'Leads';
			$search = isset($request->search)?$request->search:'';
			$offset = isset($request->offset)?$request->offset:'0';
			$client_id = isset($request->client_id)?$request->client_id:'0';
			$limit = isset($request->limit)?$request->limit:'9999999999';
			
			if($limit == 'All' )
			{
				$limit = 9999999999;
			}
			
			$start_date = date('Y-m-d');
			$end_date = date('Y-m-d');
			$dateRange = isset($request->dateRange)?$request->dateRange:'';
			if($dateRange != '')
			{
				$explodeDate = explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			$date1=date_create($start_date);
			$date2=date_create($end_date);
			$diff=date_diff($date1,$date2);
			$days = $diff->format("%a")+1;
			
			switch($sortString) 
			{
				case 'Clients':
				$sort = 'clients.name';
				break;
				case 'Active?':
				$sort = 'clients.status';
				break;
				case 'Leads':
				$sort = 'LDs';
				break;
				default:
				$sort = 'LDs';
			}
			
			$data=array();
			$rows=array();
			
			$ShowClient = '';
			$columns = ['clients.name', 'clients.status'];
			
			if(Session::get('user_category')=='user')
			{
				$Vendor = DB::table('managers')->select('vendor_id')->where('id', Session::get('manager_id'))->first();
				$vendor_id = $Vendor->vendor_id;
				$ShowClient = ManagerHelper::ManagerClientMenuAction();
			}
			else
			{
				$vendor_id = Session::get('vendor_id');
			}
			
			$TotalClients = DB::table('clients')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->selectRAW('clients.*, count(DISTINCT facebook_ads_lead_users.id) as LDs')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('clients.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($ShowClient, $vendor_id){
				if($ShowClient=='Allotted Clients')
				{
					$query->where('clients.manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->where('clients.vendor_id', $vendor_id);
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('clients.id', $client_id);
				}
			})
			->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
			->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date)
			->where('clients.status', 1)
			->where('clients.is_deleted', 0)
			->groupBy('clients.id')
			->get();
			
			
			$Clients = DB::table('clients')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->selectRAW('clients.*, count(DISTINCT facebook_ads_lead_users.id) as LDs')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('clients.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($ShowClient, $vendor_id){
				if($ShowClient=='Allotted Clients')
				{
					$query->where('clients.manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->where('clients.vendor_id', $vendor_id);
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('clients.id', $client_id);
				}
			})
			->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
			->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date)
			->where('clients.status', 1)
			->where('clients.is_deleted', 0)
			->groupBy('clients.id')
			->orderBy($sort, $order)
			->orderBy('clients.name', 'ASC')
			->skip($offset)
			->take($limit)
			->get();
			$sno=$offset+1;
			foreach($Clients as $Client)
			{
				if($Client->id == '' || $Client->id == null )
				{
					$data['total'] = 0;
					$data['rows'] = array();
					
					return \Response::json($data);
				}
				
				$user = array();
				$user['#'] = $sno++;
				if($Client->status==1){ 
					$user['Active?'] = 'Active';
					}else{
					$user['Active?'] = 'Inactive';
				}
				$user['Clients'] = '<a href="'.route("campaigns").'?client_id='.$Client->id.'">'.$Client->name.'</a>';
				$user['Ad Set Name'] = '';
				$user['Leads'] = '<a href="'.route("leads").'?client_id='.$Client->id.'&date_range='.$start_date.' to '.$end_date.'">'.Helper::NumberFormat(number_format(ClientHelper::ClientLDs($Client->id, $start_date, $end_date),2)).'</a>';
				$user['Calls'] = 0;
				$user['Clicks'] = Helper::NumberFormat(number_format(AdminHelper::ClientClicks($Client->id, $start_date, $end_date),2));
				
				if(AdminHelper::ClientClicks($Client->id, $start_date, $end_date) == 0)
				{
					$user['LGR %'] = '0%';
				}
				else
				{
					$user['LGR %'] = Helper::NumberFormat(number_format((ClientHelper::ClientFacebookLeads($Client->id, $start_date, $end_date)/AdminHelper::ClientClicks($Client->id, $start_date, $end_date))*100, 2)).'%';
				}
				
				if(ClientHelper::ClientFacebookLeads($Client->id, $start_date, $end_date) == 0)
				{
					$user['CPA $'] = '$0';
				}
				else
				{
					$user['CPA $'] = '$'.Helper::NumberFormat(number_format((AdminHelper::ClientSpents($Client->id, $start_date, $end_date)/ClientHelper::ClientFacebookLeads($Client->id, $start_date, $end_date)), 2));
				}
				
				$user['Budget'] = '$0';
				$user['Spent $'] = '$'.Helper::NumberFormat(number_format(AdminHelper::ClientSpents($Client->id, $start_date, $end_date), 2));
				$user['CTR %'] = Helper::NumberFormat(number_format(AdminHelper::ClientCTR($Client->id, $start_date, $end_date)/$days, 2)).'%';
				$user['CPM $'] = '$0';
				$user['Freq'] = Helper::NumberFormat(number_format(AdminHelper::ClientFreq($Client->id, $start_date, $end_date)/$days, 2));
				$user['Reach'] = Helper::NumberFormat(number_format(AdminHelper::ClientReach($Client->id, $start_date, $end_date), 2));
				
				$rows[] = $user;
			}		
			
			$data['total'] = count($TotalClients);
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
	}																																																										