<?php
	namespace App\Http\Controllers\Panel;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	
	use DB;
	use Session;
	use Helper;
	use ClientHelper;
	use ManagerHelper;
	
	class ReportController extends Controller
	{
		public function getReport()
		{
			$UserArray = array();
			$WritePermission = 'Yes';
			$ClientArray = array();
			if(ManagerHelper::ManagerCategory() == 'client' || Session::get('user_category') == 'client')
			{
				if(Session::get('user_category') == 'user')
				{
					$manager = DB::table('managers')->select('client_id')->where('id',Session::get('manager_id'))->first();
					$ClientArray[] = $manager->client_id;
				}
				else
				{
					$ClientArray[] = Session::get('client_id');
				}
			}
			
			if(ManagerHelper::ManagerCategory() == 'vendor' || Session::get('user_category') == 'vendor')
			{
				if(Session::get('user_category') == 'user')
				{
					$ShowClient = ManagerHelper::ManagerClientMenuAction();
					if($ShowClient=='Allotted Clients')
					{
						$clients = DB::table('clients')->select('id')->where('manager_id', Session::get('manager_id'))->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
					else
					{
						$manager = DB::table('managers')->select('vendor_id')->where('id',Session::get('manager_id'))->first();
						$clients = DB::table('clients')->select('id')->where('vendor_id', $manager->vendor_id)->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
				}
				else
				{
					$clients = DB::table('clients')->select('id')->where('vendor_id', Session::get('vendor_id'))->get();
					foreach($clients as $client)
					{
						$ClientArray[] = $client->id;
					}
				}
			}
			
			if(ManagerHelper::ManagerCategory() == 'admin' || Session::get('user_category') == 'admin')
			{
				if(Session::get('user_category') == 'user')
				{
					$ShowClient = ManagerHelper::ManagerClientMenuAction();
					if($ShowClient=='Allotted Clients')
					{
						$clients = DB::table('clients')->select('id')->where('manager_id', Session::get('manager_id'))->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
					else
					{
						$clients = DB::table('clients')->select('id')->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
				}
				else
				{
					$clients = DB::table('clients')->select('id')->get();
					foreach($clients as $client)
					{
						$ClientArray[] = $client->id;
					}
				}
			}
			
			$ClientProfitColumns=array();
			
			$Columns = DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Client Profit Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$ClientProfitColumns[]=$Column->column_value;
			}
			
			$ClientROIColumns=array();
			
			$Columns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Client ROI Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$ClientROIColumns[]=$Column->column_value;
			}
			
			$start_date = date('Y-m').'-01';
			$end_date = date('Y-m-d');
			
			if(Session::get('user_category')=='client' || ManagerHelper::ManagerCategory()=='client')
			{
				$SoldTotals = DB::table('sold_leads')->select('id')->where('category', 'Sold')->where('sold_leads.source', 'Pownder™')->whereIn('client_id', $ClientArray)->where('is_deleted', 0)->whereDate('sold_leads.sold_date', '>=', $start_date)->whereDate('sold_leads.sold_date', '<=', $end_date)->count('id');
				
				$SoldTotalProfit = DB::table('sold_leads')->select('total_profit')->where('sold_leads.source', 'Pownder™')->where('category', 'Sold')->whereIn('client_id', $ClientArray)->whereDate('sold_leads.sold_date', '>=', $start_date)->whereDate('sold_leads.sold_date', '<=', $end_date)->where('is_deleted', 0)->sum('total_profit');
			}
			else
			{
				$SoldTotals = DB::table('sold_leads')->leftJoin('clients', 'sold_leads.client_id', '=', 'clients.id')->select('sold_leads.id')->where('sold_leads.category', 'Sold')->where('sold_leads.source', 'Pownder™')->whereIn('client_id', $ClientArray)->whereDate('sold_leads.sold_date', '>=', $start_date)->whereDate('sold_leads.sold_date', '<=', $end_date)->where('clients.is_deleted', 0)->where('sold_leads.is_deleted', 0)->count('sold_leads.id');
				
				$SoldTotalProfit = DB::table('sold_leads')->leftJoin('clients', 'sold_leads.client_id', '=', 'clients.id')->select('sold_leads.total_profit')->where('sold_leads.category', 'Sold')->where('sold_leads.source', 'Pownder™')->whereIn('client_id', $ClientArray)->whereDate('sold_leads.sold_date', '>=', $start_date)->whereDate('sold_leads.sold_date', '<=', $end_date)->where('clients.is_deleted', 0)->where('sold_leads.is_deleted', 0)->sum('sold_leads.total_profit');
			}
			
			$Clients = DB::table('clients')->leftJoin('packages', 'clients.package_id', '=', 'packages.id')->select('clients.*', 'packages.name as package_name', 'packages.package_type', 'packages.amount as package_amount', 'packages.monthly_lead_quantity', 'packages.per_lead_value')
			->whereIn('clients.id', $ClientArray)->where('clients.status', 1)->where('clients.is_deleted', 0)->orderBy('clients.name', 'ASC')->get();
			
			$AutomotiveSubCategories=DB::table('group_sub_categories')->select('id', 'name')->where('group_category_id', 6)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$Client = DB::table('clients')->where('id', Session::get('client_id'))->first();
			
			return view('panel.frontend.reports',['SoldTotals' => $SoldTotals, 'SoldTotalProfit' => $SoldTotalProfit, 'Client' => $Client, 'Clients' => $Clients, 'AutomotiveSubCategories' => $AutomotiveSubCategories, 'ClientProfitColumns' => $ClientProfitColumns, 'ClientROIColumns' => $ClientROIColumns, 'WritePermission' => $WritePermission]);
		}
		
		//-----------------------------------(Get Lead Sold Details)---------------------------------------------------//
		public function getLeadSold(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Reports Read Write');
			}
			
			$LeadSold = DB::table('sold_leads')->where('id', $request->sold_lead_id)->get();
			
			return response()->json($LeadSold);
		}
		
		public function getSoldClientROIStatus(Request $request)
		{	
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Reports Read Write');
			}
			
			$Result = DB::table('clients')->select('roi_automation')->where('id', $request->client_id)->first();
			if(is_null($Result))
			{
				return '';
			}
			else
			{
				return $Result->roi_automation;
			}
		}
		
		public function postLeadSold(Request $request)
		{	
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Reports Read Write');
			}
			
			$sold_lead_id=$request->sold_lead_id;
			$Result = DB::table('sold_leads')
			->leftJoin('clients', 'sold_leads.client_id', '=', 'clients.id')
			->leftJoin('facebook_ads_lead_users', 'sold_leads.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
			->select('packages.amount as package_amount', 'packages.per_lead_value', 'packages.package_type', 'clients.id as client_id', 'clients.package_id', 'clients.per_sold_value', 'clients.discount as client_discount', 'facebook_ads_lead_users.first_name', 'facebook_ads_lead_users.last_name', 'facebook_ads_lead_users.full_name')
			->where('sold_leads.id', $sold_lead_id)
			->first();
			
			$client_id=$Result->client_id;
			$package_id=$Result->package_id;
			$package_type=$Result->package_type;
			$per_lead_value=$Result->per_lead_value;
			$per_sold_value=$Result->per_sold_value;
			$package_amount=$Result->package_amount;
			if($Result->full_name!='')
			{
				$facebook_ads_lead_user_name=$Result->full_name;
			}
			else
			{
				$facebook_ads_lead_user_name=$Result->first_name.' '.$Result->last_name;
			}
			$package_amount=$Result->package_amount;
			$client_discount=$Result->client_discount;
			$sold_date=explode('-',$request->sold_date);
			
			if($request->roi_automation=='No')
			{
				$cost=$package_amount-$client_discount;
				
				if($cost==0)
				{
					$roi_percentage=0;
				}
				else
				{
					$roi_percentage=($request->total_profit/$cost)*100;
				}
				
				DB::table('sold_leads')->where('id', $sold_lead_id)
				->update([ 
				'facebook_ads_lead_user_name' => $facebook_ads_lead_user_name, 
				'real_buyer_name' => $request->real_buyer_name, 
				'sold_date' => $sold_date[2].'-'.$sold_date[0].'-'.$sold_date[1], 
				'stock' => $request->stock,
				'sold_year' => $request->sold_year, 
				'group_sub_category_id' => $request->group_sub_category_id,
				'model' => $request->model,
				'roi_automation' => $request->roi_automation,
				'roi_percentage' => $roi_percentage, 
				'package_id' => $package_id,
				'package_type' => $package_type,
				'per_lead_value' => $per_lead_value,
				'per_sold_value' => $per_sold_value,
				'package_amount' => $package_amount,
				'client_discount' => $client_discount,
				'total_profit' => $request->total_profit,
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			else
			{
				DB::table('sold_leads')->where('id', $sold_lead_id)
				->update([
				'facebook_ads_lead_user_name' => $facebook_ads_lead_user_name, 
				'real_buyer_name' => $request->real_buyer_name, 
				'sold_date' => $sold_date[2].'-'.$sold_date[0].'-'.$sold_date[1], 
				'stock' => $request->stock,
				'sold_year' => $request->sold_year, 
				'group_sub_category_id' => $request->group_sub_category_id,
				'model' => $request->model,
				'roi_automation' => $request->roi_automation, 
				'package_id' => $package_id,
				'package_type' => $package_type,
				'per_lead_value' => $per_lead_value,
				'per_sold_value' => $per_sold_value,
				'package_amount' => $package_amount,
				'client_discount' => $client_discount,
				'total_profit' => $request->profit_total,
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			return redirect('reports')->with('alert_success','Lead sold updated successfully');
		}
		
		public function getAppointments()
		{
			return view('panel.frontend.appointments');
		}
		
		public function getProfitReport(Request $request)
		{
			$WritePermission = 'Yes';
			$ClientArray = array();
			if(ManagerHelper::ManagerCategory() == 'client' || Session::get('user_category') == 'client')
			{
				if(Session::get('user_category') == 'user')
				{
					$manager = DB::table('managers')->select('client_id')->where('id',Session::get('manager_id'))->first();
					$ClientArray[] = $manager->client_id;
				}
				else
				{
					$ClientArray[] = Session::get('client_id');
				}
			}
			
			if(ManagerHelper::ManagerCategory() == 'vendor' || Session::get('user_category') == 'vendor')
			{
				if(Session::get('user_category') == 'user')
				{
					$ShowClient = ManagerHelper::ManagerClientMenuAction();
					if($ShowClient=='Allotted Clients')
					{
						$clients = DB::table('clients')->select('id')->where('manager_id', Session::get('manager_id'))->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
					else
					{
						$manager = DB::table('managers')->select('vendor_id')->where('id',Session::get('manager_id'))->first();
						$clients = DB::table('clients')->select('id')->where('vendor_id', $manager->vendor_id)->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
				}
				else
				{
					$clients = DB::table('clients')->select('id')->where('vendor_id', Session::get('vendor_id'))->get();
					foreach($clients as $client)
					{
						$ClientArray[] = $client->id;
					}
				}
			}
			
			if(ManagerHelper::ManagerCategory() == 'admin' || Session::get('user_category') == 'admin')
			{
				if(Session::get('user_category') == 'user')
				{
					$ShowClient = ManagerHelper::ManagerClientMenuAction();
					if($ShowClient=='Allotted Clients')
					{
						$clients = DB::table('clients')->select('id')->where('manager_id', Session::get('manager_id'))->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
					else
					{
						$clients = DB::table('clients')->select('id')->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
				}
				else
				{
					$clients = DB::table('clients')->select('id')->get();
					foreach($clients as $client)
					{
						$ClientArray[] = $client->id;
					}
				}
			}
			
			$order = isset($request->order)?$request->order:'desc';
			$sortString = isset($request->sort)?$request->sort:'Create Date';
			$search = isset($request->search)?$request->search:'';
			$client_id = isset($request->client_id)?$request->client_id:'';
			$offset = isset($request->offset)?$request->offset:'0';
			$limit = isset($request->limit)?$request->limit:'9999999999';
			
			if($limit == 'All' )
			{
				$limit = 9999999999;
			}
			
			$start_date = date('Y-m').'-01';
			$end_date = date('Y-m-d');
			$dateRange = isset($request->dateRange)?$request->dateRange:'';
			if($dateRange != '')
			{
				$explodeDate = explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			switch($sortString) 
			{
				case 'Create Date':
				$sort = 'sold_leads.created_at';
				break;
				case 'Client':
				$sort = 'clients.name';
				break;
				case 'Sold Date':
				$sort = 'sold_leads.sold_date';
				break;
				case 'Stock #':
				$sort = 'sold_leads.stock';
				break;
				case 'Full Name':
				$sort = 'sold_leads.facebook_ads_lead_user_name';
				break;
				case 'Email':
				$sort = 'sold_leads.email';
				break;
				case 'Phone #':
				$sort = 'sold_leads.phone_number';
				break;
				case 'Vehicle Info':
				$sort = 'sold_leads.sold_year';
				break;
				case 'Total Profit':
				$sort = 'sold_leads.total_profit';
				break;
				default:
				$sort = 'sold_leads.created_at';
			}
			
			$data = array();
			$rows = array();
			
			if(Session::get('user_category')=='client' || ManagerHelper::ManagerCategory()=='client')
			{
				$columns = ['sold_leads.created_at', 'sold_leads.sold_date', 'sold_leads.stock', 'sold_leads.sold_year', 'sold_leads.model', 'sold_leads.email', 'sold_leads.phone_number', 'sold_leads.total_profit', 'sold_leads.facebook_ads_lead_user_name', 'sold_leads.real_buyer_name', 'sold_leads.group_sub_category_id'];
			}
			else
			{
				$columns = ['sold_leads.created_at', 'sold_leads.sold_date', 'sold_leads.stock', 'sold_leads.sold_year', 'sold_leads.model', 'sold_leads.email', 'sold_leads.phone_number', 'sold_leads.total_profit', 'sold_leads.facebook_ads_lead_user_name', 'sold_leads.real_buyer_name', 'sold_leads.group_sub_category_id', 'clients.name'];
			}
			
			$SoldTotals = DB::table('sold_leads')
			->leftJoin('clients', 'sold_leads.client_id', '=', 'clients.id')->select('sold_leads.id')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('sold_leads.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('sold_leads.sold_date', '=', $start_date);
					}
					else
					{
						$query->whereDate('sold_leads.sold_date', '>=', $start_date)
						->whereDate('sold_leads.sold_date', '<=', $end_date);
					}
				}
			})
			->where(function ($query) use($ClientArray, $client_id){
				if($client_id > 0)
				{
					$query->where('clients.id', $client_id);
				}
				else
				{
					$query->whereIn('clients.id', $ClientArray);
				}
			})
			->where('sold_leads.category', 'Sold')
			->where('sold_leads.source', 'Pownder™')
			->where('clients.is_deleted', 0)
			->where('clients.status', 1)
			->where('sold_leads.is_deleted', 0)
			->count('sold_leads.id');
			
			$sold_leads = DB::table('sold_leads')
			->leftJoin('clients', 'sold_leads.client_id', '=', 'clients.id')
			->leftJoin('group_sub_categories', 'sold_leads.group_sub_category_id', '=', 'group_sub_categories.id')
			->select('sold_leads.*', 'group_sub_categories.name as make_name', 'clients.name as client_name')
			->where('sold_leads.category', 'Sold')
			->where('sold_leads.source', 'Pownder™')
			->where(function ($query) use($ClientArray, $client_id){
				if($client_id > 0)
				{
					$query->where('clients.id', $client_id);
				}
				else
				{
					$query->whereIn('clients.id', $ClientArray);
				}
			})
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('sold_leads.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('sold_leads.sold_date', '=', $start_date);
					}
					else
					{
						$query->whereDate('sold_leads.sold_date', '>=', $start_date)
						->whereDate('sold_leads.sold_date', '<=', $end_date);
					}
				}
			})
			->where('clients.is_deleted', 0)
			->where('clients.status', 1)
			->where('sold_leads.is_deleted', 0)
			->orderBy($sort, $order)
			->skip($offset)
			->take($limit)
			->get();
			
			$sno=$offset+1;
			foreach($sold_leads as $sold_lead)
			{
				$user = array();
				$user['#'] = $sno++;
				$user['Client'] = $sold_lead->client_name;
				$user['Create Date'] = date('m/d/Y', strtotime($sold_lead->created_at));
				$user['Sold Date'] = date('m/d/Y', strtotime($sold_lead->sold_date));
				$user['Stock #'] = $sold_lead->stock;
				
				if($sold_lead->phone_number == '' || $sold_lead->phone_number == null)
				{
					$user['Phone #'] = '';
				}
				else
				{
					$user['Phone #'] = $sold_lead->phone_number;
				}
				
				if($sold_lead->email == '' || $sold_lead->email == null)
				{
					$user['Email'] = '';
				}
				else
				{
					$user['Email'] = $sold_lead->email;
				}
				
				$full_name = $sold_lead->facebook_ads_lead_user_name;
				
				if(trim($sold_lead->real_buyer_name) != ''){
					$full_name = $full_name.' ('.$sold_lead->real_buyer_name.')';
				}
				
				$type = '';
				if(($sold_lead->email != '' && $sold_lead->email != null) && ($sold_lead->phone_number != '' && $sold_lead->phone_number != null))
				{
					$type = '<i class="fa fa-random" aria-hidden="true"></i>';
				}
				else if($sold_lead->phone_number != '' && $sold_lead->phone_number != null)
				{
					$type = '<i class="fa fa-phone-square" style="color: #00ccc1;" aria-hidden="true"></i>';
				}
				elseif($sold_lead->email != '' && $sold_lead->email != null)
				{
					$type = '<i class="fa fa-envelope" style="color: #0082a3;" aria-hidden="true"></i>';	
				}
				
				if($sold_lead->sold_type == 'Manual')
				{
					$type = '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>';
				}
				
				$full_name = title_case($full_name).' '.$type;
				$user['Full Name'] = $full_name;
				
				if($sold_lead->make_name == '')
				{
					$user['Vehicle Info'] = $sold_lead->sold_year.' '.$sold_lead->group_sub_category_id.' '.$sold_lead->model;
				}
				else
				{
					$user['Vehicle Info'] = $sold_lead->sold_year.' '.$sold_lead->make_name.' '.$sold_lead->model;
				}
				$user['Total Profit'] = '$'.number_format($sold_lead->total_profit,0);
				
				if($sold_lead->sold_type == 'Manual')
				{
					if(ManagerHelper::ManagerCategory() == 'client' || Session::get('user_category') == 'client')
					{
						$user['Action'] = '<a href="cleads?email='.$sold_lead->email.'&phone_number='.$sold_lead->phone_number.'" title="Lead Details"><i class="fa fa-arrow-circle-right fa-lg" aria-hidden="true"></i></a> <i class="fa fa-pencil-square-o text-primary actions_icon fa-lg" onclick="Sold(\''.$sold_lead->id.'\', \''. $sold_lead->client_id.'\');" title="Edit"></i>';
					}
					else
					{
						$user['Action'] = '<a href="leads?email='.$sold_lead->email.'&phone_number='.$sold_lead->phone_number.'" title="Lead Details"><i class="fa fa-arrow-circle-right fa-lg" aria-hidden="true"></i></a> <i class="fa fa-pencil-square-o text-primary actions_icon fa-lg" onclick="Sold(\''.$sold_lead->id.'\', \''. $sold_lead->client_id.'\');" title="Edit"></i>';
					}
				}
				else
				{
					if(ManagerHelper::ManagerCategory() == 'client' || Session::get('user_category') == 'client')
					{
						$user['Action'] = '<a href="cleads?email='.$sold_lead->email.'&phone_number='.$sold_lead->phone_number.'" title="Lead Details"><i class="fa fa-arrow-circle-right fa-lg" aria-hidden="true"></i></a> <a href="closed-deals?client_id='.$sold_lead->client_id.'&deal_number='.$sold_lead->deal_number.'&stock_number='.$sold_lead->stock.'" title="Close Deal Details"><i class="fa fa-external-link-square fa-lg" aria-hidden="true"></i></a>';
					}
					else
					{
						$user['Action'] = '<a href="leads?email='.$sold_lead->email.'&phone_number='.$sold_lead->phone_number.'" title="Lead Details"><i class="fa fa-arrow-circle-right fa-lg" aria-hidden="true"></i></a> <a href="closed-deals?client_id='.$sold_lead->client_id.'&deal_number='.$sold_lead->deal_number.'&stock_number='.$sold_lead->stock.'" title="Close Deal Details"><i class="fa fa-external-link-square fa-lg" aria-hidden="true"></i></a>';
					}
				}
				
				$rows[] = $user;
			}		
			
			$data['total'] = $SoldTotals;
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
		
		public function getClientROIReport(Request $request)
		{
			$WritePermission = 'Yes';
			$ClientArray = array();
			if(ManagerHelper::ManagerCategory() == 'client' || Session::get('user_category') == 'client')
			{
				if(Session::get('user_category') == 'user')
				{
					$manager = DB::table('managers')->select('client_id')->where('id',Session::get('manager_id'))->first();
					$ClientArray[] = $manager->client_id;
				}
				else
				{
					$ClientArray[] = Session::get('client_id');
				}
			}
			
			if(ManagerHelper::ManagerCategory() == 'vendor' || Session::get('user_category') == 'vendor')
			{
				if(Session::get('user_category') == 'user')
				{
					$ShowClient = ManagerHelper::ManagerClientMenuAction();
					if($ShowClient=='Allotted Clients')
					{
						$clients = DB::table('clients')->select('id')->where('manager_id', Session::get('manager_id'))->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
					else
					{
						$manager = DB::table('managers')->select('vendor_id')->where('id',Session::get('manager_id'))->first();
						$clients = DB::table('clients')->select('id')->where('vendor_id', $manager->vendor_id)->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
				}
				else
				{
					$clients = DB::table('clients')->select('id')->where('vendor_id', Session::get('vendor_id'))->get();
					foreach($clients as $client)
					{
						$ClientArray[] = $client->id;
					}
				}
			}
			
			if(ManagerHelper::ManagerCategory() == 'admin' || Session::get('user_category') == 'admin')
			{
				if(Session::get('user_category') == 'user')
				{
					$ShowClient = ManagerHelper::ManagerClientMenuAction();
					if($ShowClient=='Allotted Clients')
					{
						$clients = DB::table('clients')->select('id')->where('manager_id', Session::get('manager_id'))->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
					else
					{
						$clients = DB::table('clients')->select('id')->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
				}
				else
				{
					$clients = DB::table('clients')->select('id')->get();
					foreach($clients as $client)
					{
						$ClientArray[] = $client->id;
					}
				}
			}
			
			$order = isset($request->order)?$request->order:'desc';
			$sortString = isset($request->sort)?$request->sort:'Date';
			$search = isset($request->search)?$request->search:'';
			$client_id = isset($request->client_id)?$request->client_id:'';
			$offset = isset($request->offset)?$request->offset:'0';
			$limit = isset($request->limit)?$request->limit:'9999999999';
			
			if($limit == 'All' )
			{
				$limit = 9999999999;
			}
			
			$start_date = date('Y-m').'-01';
			$end_date = date('Y-m-d');
			$dateRange = isset($request->dateRange)?$request->dateRange:'';
			if($dateRange != '')
			{
				$explodeDate = explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			switch($sortString) 
			{
				case 'Date':
				$sort = 'clients.created_at';
				break;
				case 'Client':
				$sort = 'clients.name';
				break;
				case 'Package':
				$sort = 'packages.name';
				break;
				default:
				$sort = 'clients.created_at';
			}
			
			$data = array();
			$rows = array();
			
			$columns = ['clients.created_at', 'clients.name', 'packages.name', 'packages.amount', 'packages.per_lead_value', 'packages.monthly_lead_quantity',  'packages.package_type'];
			
			$TotalClients = DB::table('clients')->leftJoin('packages', 'clients.package_id', '=', 'packages.id')->select('clients.*', 'packages.name as package_name', 'packages.package_type', 'packages.amount as package_amount', 'packages.monthly_lead_quantity', 'packages.per_lead_value')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('clients.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($ClientArray, $client_id){
				if($client_id > 0)
				{
					$query->where('clients.id', $client_id);
				}
				else
				{
					$query->whereIn('clients.id', $ClientArray);
				}
			})
			->where('clients.status', 1)->where('clients.is_deleted', 0)->count('clients.id');
			
			$clients = DB::table('clients')->leftJoin('packages', 'clients.package_id', '=', 'packages.id')->select('clients.*', 'packages.name as package_name', 'packages.package_type', 'packages.amount as package_amount', 'packages.monthly_lead_quantity', 'packages.per_lead_value')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('clients.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($ClientArray, $client_id){
				if($client_id > 0)
				{
					$query->where('clients.id', $client_id);
				}
				else
				{
					$query->whereIn('clients.id', $ClientArray);
				}
			})
			->where('clients.status', 1)->where('clients.is_deleted', 0)
			->orderBy($sort, $order)
			->skip($offset)
			->take($limit)
			->get();
			
			$sno = $offset+1;
			foreach($clients as $client)
			{
				$user = array();
				$user['Date'] = date('m/d/Y', strtotime($client->created_at));
				$user['Client'] = $client->name;
				$user['Package'] = $client->package_name;
				$user['LD\'s'] = number_format(ClientHelper::ClientLDs($client->id, $start_date, $end_date),0);
				$user['+/-'] = number_format(ClientHelper::ClientLDs($client->id, $start_date, $end_date)-$client->monthly_lead_quantity,0);
				
				if($client->package_type=='Monthly')
				{
					$Cost = $client->package_amount-$client->discount;
				}
				elseif($client->package_type=='Lead')
				{
					$Cost=(ClientHelper::ClientLDs($client->id, $start_date, $end_date)*$client->per_lead_value)-$client->discount;
				}
				elseif($client->package_type=='Sold')
				{
					$Cost=ClientHelper::ClientSold($client->id, $start_date, $end_date)*$client->per_sold_value;
				}
				else
				{
					$Cost=0;
				}
				
				$user['Cost'] = '$'.number_format($Cost, 0);
				
				$Profit = ClientHelper::ClientProfit($client->id, $start_date, $end_date);
				$user['Profit'] = '$'.number_format($Profit,2);
				if($Cost==0)
				{
					$user['ROI'] = '0.00%';		
				}
				else
				{
					$user['ROI'] = number_format(($Profit/$Cost)*100,2).'%';
				}
				
				$rows[] = $user;
			}		
			
			$data['total'] = $TotalClients;
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
		
		public function getReportFilter(Request $request)
		{
			$WritePermission = 'Yes';
			$ClientArray = array();
			if(ManagerHelper::ManagerCategory() == 'client' || Session::get('user_category') == 'client')
			{
				if(Session::get('user_category') == 'user')
				{
					$manager = DB::table('managers')->select('client_id')->where('id',Session::get('manager_id'))->first();
					$ClientArray[] = $manager->client_id;
				}
				else
				{
					$ClientArray[] = Session::get('client_id');
				}
			}
			
			if(ManagerHelper::ManagerCategory() == 'vendor' || Session::get('user_category') == 'vendor')
			{
				if(Session::get('user_category') == 'user')
				{
					$ShowClient = ManagerHelper::ManagerClientMenuAction();
					if($ShowClient=='Allotted Clients')
					{
						$clients = DB::table('clients')->select('id')->where('manager_id', Session::get('manager_id'))->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
					else
					{
						$manager = DB::table('managers')->select('vendor_id')->where('id',Session::get('manager_id'))->first();
						$clients = DB::table('clients')->select('id')->where('vendor_id', $manager->vendor_id)->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
				}
				else
				{
					$clients = DB::table('clients')->select('id')->where('vendor_id', Session::get('vendor_id'))->get();
					foreach($clients as $client)
					{
						$ClientArray[] = $client->id;
					}
				}
			}
			
			if(ManagerHelper::ManagerCategory() == 'admin' || Session::get('user_category') == 'admin')
			{
				if(Session::get('user_category') == 'user')
				{
					$ShowClient = ManagerHelper::ManagerClientMenuAction();
					if($ShowClient=='Allotted Clients')
					{
						$clients = DB::table('clients')->select('id')->where('manager_id', Session::get('manager_id'))->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
					else
					{
						$clients = DB::table('clients')->select('id')->get();
						foreach($clients as $client)
						{
							$ClientArray[] = $client->id;
						}
					}
				}
				else
				{
					$clients = DB::table('clients')->select('id')->get();
					foreach($clients as $client)
					{
						$ClientArray[] = $client->id;
					}
				}
			}
			
			$search = isset($request->search)?$request->search:'';
			$search1 = isset($request->search1)?$request->search1:'';
			$client_id = isset($request->client_id)?$request->client_id:'';
			
			$start_date = date('Y-m').'-01';
			$end_date = date('Y-m-d');
			$dateRange = isset($request->dateRange)?$request->dateRange:'';
			if($dateRange != '')
			{
				$explodeDate = explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			$data = array();
			$rows = array();
			
			$columns = ['clients.created_at', 'clients.name', 'packages.name', 'packages.amount', 'packages.per_lead_value', 'packages.monthly_lead_quantity',  'packages.package_type'];
			
			$clients = DB::table('clients')->leftJoin('packages', 'clients.package_id', '=', 'packages.id')->select('clients.*', 'packages.name as package_name', 'packages.package_type', 'packages.amount as package_amount', 'packages.monthly_lead_quantity', 'packages.per_lead_value')
			->where(function ($query) use($search1, $columns){
				if($search1!='')
				{
					$query->where('clients.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search1.'%');
					}
				}
			})
			->where(function ($query) use($ClientArray, $client_id){
				if($client_id > 0)
				{
					$query->where('clients.id', $client_id);
				}
				else
				{
					$query->whereIn('clients.id', $ClientArray);
				}
			})
			->where('clients.status', 1)->where('clients.is_deleted', 0)
			->get();
			
			$TotalLDs = 0;
			$LeadPlusMinus = 0;
			$TotalCost = 0;
			$TotalProfit = 0;
			$TotalROI = 0;
			foreach($clients as $client)
			{
				$TotalLDs = $TotalLDs + (ClientHelper::ClientLDs($client->id, $start_date, $end_date)); 
				$LeadPlusMinus = $LeadPlusMinus + (ClientHelper::ClientLDs($client->id, $start_date, $end_date) - $client->monthly_lead_quantity);
				
				if($client->package_type == 'Monthly')
				{
					$Cost = $client->package_amount - $client->discount;
				}
				elseif($client->package_type == 'Lead')
				{
					$Cost = (ClientHelper::ClientLDs($client->id, $start_date, $end_date) * $client->per_lead_value) - $client->discount; 
				}
				elseif($client->package_type == 'Sold')
				{
					$Cost = ClientHelper::ClientSold($client->id, $start_date, $end_date) * $client->per_sold_value;
				}
				else
				{
					$Cost = 0; 
				}
				
				$TotalCost = $TotalCost + ($Cost);
				
				$TotalProfit = $TotalProfit + (ClientHelper::ClientProfit($client->id, $start_date, $end_date));
			}	
			
			if($TotalCost == 0 || $TotalProfit == 0)
			{
				$TotalROI = 0;
			}
			else
			{
				$TotalROI = ($TotalProfit / $TotalCost) * 100;
			}
			
			if(Session::get('user_category')=='client' || ManagerHelper::ManagerCategory()=='client')
			{
				$columns = ['sold_leads.created_at', 'sold_leads.sold_date', 'sold_leads.stock', 'sold_leads.sold_year', 'sold_leads.model', 'sold_leads.email', 'sold_leads.phone_number', 'sold_leads.total_profit', 'sold_leads.facebook_ads_lead_user_name', 'sold_leads.real_buyer_name', 'sold_leads.group_sub_category_id'];
			}
			else
			{
				$columns = ['sold_leads.created_at', 'sold_leads.sold_date', 'sold_leads.stock', 'sold_leads.sold_year', 'sold_leads.model', 'sold_leads.email', 'sold_leads.phone_number', 'sold_leads.total_profit', 'sold_leads.facebook_ads_lead_user_name', 'sold_leads.real_buyer_name', 'sold_leads.group_sub_category_id', 'clients.name'];
			}
			
			$SoldTotals = DB::table('sold_leads')
			->leftJoin('clients', 'sold_leads.client_id', '=', 'clients.id')->select('sold_leads.id')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('sold_leads.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('sold_leads.sold_date', '=', $start_date);
					}
					else
					{
						$query->whereDate('sold_leads.sold_date', '>=', $start_date)
						->whereDate('sold_leads.sold_date', '<=', $end_date);
					}
				}
			})
			->where(function ($query) use($ClientArray, $client_id){
				if($client_id > 0)
				{
					$query->where('clients.id', $client_id);
				}
				else
				{
					$query->whereIn('clients.id', $ClientArray);
				}
			})
			->where('sold_leads.category', 'Sold')
			->where('sold_leads.source', 'Pownder™')
			->where('clients.is_deleted', 0)
			->where('clients.status', 1)
			->where('sold_leads.is_deleted', 0)
			->count('sold_leads.id');
			
			$SoldTotalProfits = DB::table('sold_leads')
			->leftJoin('clients', 'sold_leads.client_id', '=', 'clients.id')->select('sold_leads.total_profit')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('sold_leads.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('sold_leads.sold_date', '=', $start_date);
					}
					else
					{
						$query->whereDate('sold_leads.sold_date', '>=', $start_date)
						->whereDate('sold_leads.sold_date', '<=', $end_date);
					}
				}
			})
			->where(function ($query) use($ClientArray, $client_id){
				if($client_id > 0)
				{
					$query->where('clients.id', $client_id);
				}
				else
				{
					$query->whereIn('clients.id', $ClientArray);
				}
			})
			->where('sold_leads.category', 'Sold')
			->where('sold_leads.source', 'Pownder™')
			->where('clients.is_deleted', 0)
			->where('clients.status', 1)
			->where('sold_leads.is_deleted', 0)
			->sum('sold_leads.total_profit');
			
			$data['TotalLDs'] = number_format($TotalLDs,0);
			$data['LeadPlusMinus'] = number_format($LeadPlusMinus,0);
			$data['TotalCost'] = '$'.number_format($TotalCost,2);
			$data['TotalProfit'] = '$'.number_format($TotalProfit,2);
			$data['TotalROI'] = number_format($TotalROI,2).'%';
			$data['SoldTotals'] = number_format($SoldTotals,0);
			$data['SoldTotalProfits'] = '$'.number_format($SoldTotalProfits,2);			
			return \Response::json($data);
		}
	}																																					