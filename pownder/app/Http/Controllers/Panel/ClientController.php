<?php
	namespace App\Http\Controllers\Panel;
	
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	
	use DB;
	use Redirect;
	use Session;
	use Helper;
	use ClientHelper;
	use ConsoleHelper;
	use ManagerHelper;
	use Validator;
	use Mail;
	
	class ClientController extends Controller
	{
		public function getClientDashboard()
		{
			$date_start = date('Y-m-d');
			$date_stop = date('Y-m-d');
			
			$campaign_s_date = 	date('Y-m').'-01';
			$campaign_e_date = date('Y-m-d');
			
			$GraphClick = array();
			$GraphCTR = array();
			$GraphFreq = array();
			$GraphLead = array();
			$GraphLGR = array();
			$Click = 0;
			$ShowLead = '';
			$WritePermission = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$WritePermission = ManagerHelper::ManagerWritePermission('Dashboard Read Write');
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				
				$manager = DB::table('managers')->select('client_id')->where('id', Session::get('manager_id'))->first();
				$client_id = $manager->client_id;
			}
			else
			{
				$client_id = Session::get('client_id');
			}			
			
			$CityLeads = DB::table('facebook_ads_lead_users')->selectRaw('count(id) as Leads, city, latitude, longitude')
			->where(function ($query) use($ShowLead, $client_id){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
			})
			->where('client_id', $client_id)
			->where('city', '!=', '')
			->where('latitude', '!=', '')
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->groupBy('latitude')
			->groupBy('longitude')
			->get();
			
			$TodayLead = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($ShowLead, $client_id){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
			})
			->where('client_id', $client_id)
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($ShowLead, $client_id){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
			})
			->where('client_id', $client_id)
			->where('lead_type', 'Pownder™ Lead')
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$Sold = DB::table('facebook_ads_lead_users')
			->join('sold_leads', 'facebook_ads_lead_users.id', '=', 'sold_leads.facebook_ads_lead_user_id')
			->select('sold_leads.id')->where('sold_leads.category', 'Sold')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
			})
			->where('sold_leads.client_id', $client_id)
			->whereDate('sold_leads.sold_date', '>=', $date_start)
			->whereDate('sold_leads.sold_date', '<=', $date_stop)
			->count('sold_leads.id');
			
			$Appointment = DB::table('facebook_ads_lead_users')->select('id')
			->where('setting', 'Appointment')
			->where(function ($query) use($ShowLead, $client_id){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where('client_id', $client_id)
			->whereDate('appointment_date', '>=', $date_start)
			->whereDate('appointment_date', '<=', $date_stop)
			->count('id');
			
			$CampaignArray = array();
			$client_campaigns = DB::table('facebook_ads_lead_users')->select('campaign_id')
			->where(function ($query) use($ShowLead, $client_id){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where('client_id', $client_id)
			->whereDate('created_time', '>=', $campaign_s_date)
			->whereDate('created_time', '<=', $campaign_e_date)
			->where('campaign_id', '!=', 0)
			->groupBy('campaign_id')
			->get();
			foreach($client_campaigns as $client_campaign)
			{
				$CampaignArray[] = $client_campaign->campaign_id;
			}
			//print_r($CampaignArray);die;
			
			if($ShowLead=='Allotted Leads')
			{
				$facebook_campaign_values = DB::table('facebook_campaign_values')
				->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
				->whereIn('campaign_id', array_unique($CampaignArray))
				->whereDate('campaign_date', '>=', $campaign_s_date)
				->whereDate('campaign_date', '<=', $campaign_e_date)
				->groupBy('campaign_date')->get();
				
				foreach($facebook_campaign_values as $facebook_campaign_value)
				{
					$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', Session::get('manager_id'))->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', Session::get('manager_id'))->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					$managerCampaignLeads = ManagerHelper::managerCampaignLeads($client_id, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					
					$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
					$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
					$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
					if($CampaignLeads > 0)
					{
						$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $managerCampaignLeads);
						$GraphClick[] = $PownderClick;
						$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $managerCampaignLeads;
						$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $managerCampaignLeads;
						
						if($PownderClick == 0 || $PownderLeads == 0)
						{
							$GraphLGR[] = 0;
						}
						else
						{
							$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
						}
					}
					else
					{
						$GraphClick[] = 0;
						$GraphCTR[] = 0;
						$GraphFreq[] = 0;
						$GraphLGR[] = 0;
					}
				}
				
				$TotalClicks = 0;
				$FacebookAccounts = DB::table('facebook_campaigns')
				->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
				->select('facebook_campaigns.id', 'facebook_accounts.access_token')
				->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
				->groupBy('facebook_campaigns.id')->get();
				foreach($FacebookAccounts as $FacebookAccount)
				{
					$campaign_id = $FacebookAccount->id;
					$access_token = $FacebookAccount->access_token;
					
					//Read facebook clicks, spend
					$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($curl);
					curl_close($curl);
					
					$result = json_decode($response,true);
					
					if(isset($result['data']))
					{
						$data = $result['data'];
						if(isset($data[0]['clicks']))
						{
							$TotalClicks = $TotalClicks + $data[0]['clicks'];
						}
					}
				}
				
				$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
				$managerCampaignLeads = ManagerHelper::managerCampaignLeads(Session::get('manager_id'), array_unique($CampaignArray), $date_start, $date_stop);
				if($CampaignLeads > 0)
				{
					$Click = ceil(($TotalClicks / $CampaignLeads) * $managerCampaignLeads);
				}
			}
			else
			{
				$facebook_campaign_values = DB::table('facebook_campaign_values')
				->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
				->whereIn('campaign_id', array_unique($CampaignArray))
				->whereDate('campaign_date', '>=', $campaign_s_date)
				->whereDate('campaign_date', '<=', $campaign_e_date)
				->groupBy('campaign_date')->get();
				foreach($facebook_campaign_values as $facebook_campaign_value)
				{
					$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					
					$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
					$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
					$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
					if($CampaignLeads > 0)
					{
						$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
						$GraphClick[] = $PownderClick;
						$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $clientCampaignLeads;
						$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $clientCampaignLeads;
						
						if($PownderClick == 0 || $PownderLeads == 0)
						{
							$GraphLGR[] = 0;
						}
						else
						{
							$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
						}
					}
					else
					{
						$GraphClick[] = 0;
						$GraphCTR[] = 0;
						$GraphFreq[] = 0;
						$GraphLGR[] = 0;
					}
				}
				
				$TotalClicks = 0;
				
				$FacebookAccounts = DB::table('facebook_campaigns')
				->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
				->select('facebook_campaigns.id', 'facebook_accounts.access_token')
				->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
				->groupBy('facebook_campaigns.id')->get();
				foreach($FacebookAccounts as $FacebookAccount)
				{
					$campaign_id = $FacebookAccount->id;
					$access_token = $FacebookAccount->access_token;
					
					//Read facebook clicks, spend
					$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($curl);
					curl_close($curl);
					
					$result = json_decode($response,true);
					
					if(isset($result['data']))
					{
						$data = $result['data'];
						if(isset($data[0]['clicks']))
						{
							$TotalClicks = $TotalClicks + $data[0]['clicks'];
						}
					}
				}
				
				$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
				$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $date_start, $date_stop);
				if($CampaignLeads > 0)
				{
					$Click = ceil(($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
				}	
			}
			
			return view('panel.frontend.client_dashboard',['WritePermission' => $WritePermission, 'Click' => $Click, 'CityLeads' => $CityLeads, 'TodayLead' => $TodayLead, 'Sold' => $Sold, 'Appointment' => $Appointment, 'facebook_ads_lead_users' => $facebook_ads_lead_users, 'facebook_campaign_values' => $facebook_campaign_values, 'GraphCTR' => $GraphCTR, 'GraphClick' => $GraphClick, 'GraphFreq' => $GraphFreq, 'GraphLGR' => $GraphLGR, 'GraphLead' => $GraphLead]);
		}
		
		public function getClientList(Request $request)
		{
			$UserArray = array();
			$WritePermission = 'Yes';
			$vendor_id = 0;
			$ShowClient = '';
			$LeadList = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				$ShowClient = ManagerHelper::ManagerClientMenuAction();
				$WritePermission = ManagerHelper::ManagerWritePermission('Clients Read Write');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
				{
					$LeadList = 'Yes';
				}
				else
				{
					$LeadList = 'No';
				}
				if(ManagerHelper::ManagerCategory()=='vendor')
				{
					$Vendor = DB::table('users')->where('id',$manager->created_user_id)->first();
					$vendor_id = $Vendor->vendor_id;
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				$vendor_id = Session::get('vendor_id');
			}
			
			$Vendors = DB::table('vendors')->select('id', 'name')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->orderBy('name', 'asc')->get();
			
			$editdata = array();
			$ClientColumns = array();
			$GroupSubCategories = array();
			$EditFacebookAdsLeads = array();
			
			$Columns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Client Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$ClientColumns[]=$Column->column_value;
			}
			
			$GroupFormArray = array();
			$LeadFormQuery = DB::table('group_lead_forms')
			->leftJoin('groups', 'group_lead_forms.group_id', '=', 'groups.id')
			->select('group_lead_forms.facebook_ads_lead_id')
			->where('group_lead_forms.is_deleted',0)
			->where('groups.is_deleted',0)
			->groupBy('group_lead_forms.facebook_ads_lead_id')
			->get();
			foreach($LeadFormQuery as $row)
			{
				$GroupFormArray[] = $row->facebook_ads_lead_id;
			}
			
			$edit_facebook_pages = array();
			if($request->has('id'))
			{
				$editdata = DB::table('clients')->where('is_deleted', 0)->where('id',$request->id)->first();
				
				if(is_null($editdata))
				{
					return redirect('clients')->with('alert_danger','Unauthorized client access failed');
				}
				else
				{
					$GroupSubCategories = DB::table('group_sub_categories')->where('group_category_id',$editdata->group_category_id)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
					
					$EditLeadFormArray=array();
					$EditLeadFormQuery = DB::table('client_lead_forms')->leftJoin('clients', 'client_lead_forms.client_id', '=', 'clients.id')->select('client_lead_forms.facebook_ads_lead_id')->where('client_lead_forms.client_id', '!=', $request->id)->where('client_lead_forms.is_deleted',0)->where('clients.is_deleted',0)->groupBy('client_lead_forms.facebook_ads_lead_id')->get();
					foreach($EditLeadFormQuery as $row)
					{
						$EditLeadFormArray[] = $row->facebook_ads_lead_id;
					}
					
					$EditFacebookAdsLeads=DB::table('facebook_ads_leads')->select('id', 'name')->whereNotIn('id', $GroupFormArray)->whereNotIn('id', $EditLeadFormArray)->where('status', 'ACTIVE')->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
				}
				
				$PageArray=array();
				$GroupPages = DB::table('group_page_engagements')
				->leftJoin('groups', 'group_page_engagements.group_id', '=', 'groups.id')
				->select('group_page_engagements.facebook_page_id')
				->where('group_page_engagements.is_deleted',0)
				->where('groups.is_deleted',0)
				->groupBy('group_page_engagements.facebook_page_id')
				->get();
				foreach($GroupPages as $GroupPage)
				{
					$PageArray[] = $GroupPage->facebook_page_id;
				}
				
				$ClientPages = DB::table('page_engagements')
				->leftJoin('clients', 'page_engagements.client_id', '=', 'clients.id')
				->select('page_engagements.facebook_page_id')
				->where('page_engagements.client_id', '!=', $request->id)
				->where('page_engagements.is_deleted', 0)
				->where('clients.is_deleted', 0)
				->groupBy('page_engagements.facebook_page_id')
				->get();
				foreach($ClientPages as $ClientPage)
				{
					$PageArray[] = $ClientPage->facebook_page_id;
				}
				
				$edit_facebook_pages = DB::table('facebook_pages')->select('id', 'name')->whereNotIn('id', $PageArray)->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			}
			
			$PageArray=array();
			$GroupPages = DB::table('group_page_engagements')
			->leftJoin('groups', 'group_page_engagements.group_id', '=', 'groups.id')
			->select('group_page_engagements.facebook_page_id')
			->where('group_page_engagements.is_deleted',0)
			->where('groups.is_deleted',0)
			->groupBy('group_page_engagements.facebook_page_id')
			->get();
			foreach($GroupPages as $GroupPage)
			{
				$PageArray[] = $GroupPage->facebook_page_id;
			}
			
			$ClientPages = DB::table('page_engagements')
			->leftJoin('clients', 'page_engagements.client_id', '=', 'clients.id')
			->select('page_engagements.facebook_page_id')
			->where('page_engagements.is_deleted', 0)
			->where('clients.is_deleted', 0)
			->groupBy('page_engagements.facebook_page_id')
			->get();
			foreach($ClientPages as $ClientPage)
			{
				$PageArray[] = $ClientPage->facebook_page_id;
			}
			
			$facebook_pages = DB::table('facebook_pages')->select('id', 'name')->whereNotIn('id', $PageArray)->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$LeadFormArray=array();
			$LeadFormQuery = DB::table('client_lead_forms')->leftJoin('clients', 'client_lead_forms.client_id', '=', 'clients.id')->select('client_lead_forms.facebook_ads_lead_id')->where('client_lead_forms.is_deleted',0)->where('clients.is_deleted',0)->groupBy('client_lead_forms.facebook_ads_lead_id')->get();
			foreach($LeadFormQuery as $row)
			{
				$LeadFormArray[] = $row->facebook_ads_lead_id;
			}
			
			$FacebookAdsLeads=DB::table('facebook_ads_leads')->select('id', 'name')->whereNotIn('id', $GroupFormArray)->whereNotIn('id', $LeadFormArray)->where('status', 'ACTIVE')->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$packages = DB::table('packages')->select('id','name')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->orderBy('name', 'asc')->get();
			
			$GroupCategories = DB::table('group_categories')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$DefaultGroupSubCategories = DB::table('group_sub_categories')->where('group_category_id', 6)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$Managers = array();
			if(Session::get('user_category') == 'admin')
			{
				$Managers=DB::table('managers')->select('id', 'first_name', 'last_name')->where('is_deleted', 0)->orderBy('full_name', 'ASC')->get();
			}
			else
			{
				$Managers=DB::table('managers')->select('id', 'first_name', 'last_name')->where('vendor_id',Session::get('vendor_id'))->where('is_deleted', 0)->orderBy('full_name', 'ASC')->get();
			}
			
			return view('panel.frontend.clients',['FacebookAdsLeads'=>$FacebookAdsLeads, 'Vendors'=>$Vendors, 'packages'=>$packages, 'editdata'=>$editdata, 'ClientColumns'=>$ClientColumns, 'GroupCategories'=>$GroupCategories, 'GroupSubCategories'=>$GroupSubCategories, 'EditFacebookAdsLeads'=>$EditFacebookAdsLeads, 'Managers' => $Managers, 'LeadList' => $LeadList, 'WritePermission' => $WritePermission, 'ShowClient' => $ShowClient, 'UserArray' => $UserArray, 'vendor_id' => $vendor_id, 'facebook_pages' => $facebook_pages, 'edit_facebook_pages' => $edit_facebook_pages, 'DefaultGroupSubCategories' => $DefaultGroupSubCategories]);
		}
		
		public function getClientSearch(Request $request)
		{
			$vendor_id = 0;
			$ShowClient = '';
			$LeadList = 'Yes';
			$WritePermission = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$ShowClient = ManagerHelper::ManagerClientMenuAction();
				$WritePermission = ManagerHelper::ManagerWritePermission('Clients Read Write');
				if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
				{
					$LeadList = 'Yes';
				}
				else
				{
					$LeadList = 'No';
				}
				if(ManagerHelper::ManagerCategory()=='vendor')
				{
					$manager = DB::table('managers')->where('id', Session::get('manager_id'))->first();
					$vendor_id = $manager->vendor_id;
				}
			}
			else
			{
				$vendor_id = Session::get('vendor_id');
			}
			
			$start_date='';
			$end_date='';
			$dateRange = isset($request->dateRange)?$request->dateRange:'';
			if($dateRange!='')
			{
				$explodeDate=explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			$client_id = isset($request->client_id)?$request->client_id:'';
			$offset = isset($request->offset)?$request->offset:'0';
			$limit = isset($request->limit)?$request->limit:'9999999999';
			
			if($limit=='All' )
			{
				$limit=9999999999;
			}
			
			$order = isset($request->order)?$request->order:'ASC';
			$status = isset($request->status)?$request->status:'1';
			
			$sortString=isset($request->sort)?$request->sort:'Client';
			
			$search=isset($request->search)?$request->search:'';
			
			switch($sortString) 
			{
				case 'ID':
				$sort = 'clients.client_no';
				break;
				case 'Date':
				$sort = 'clients.created_at';
				break;
				case 'Status':
				$sort = 'clients.status';
				break;
				case 'Client':
				$sort = 'clients.name';
				break;
				case 'Owner':
				$sort = 'vendors.name';
				break;
				case 'Package':
				$sort = 'packages.name';
				break;
				case 'Ads':
				$sort = 'Ads';
				break;
				case 'CPs':
				$sort = 'CPs';
				break;
				case 'LDs':
				$sort = 'LDs';
				break;
				case 'CRM':
				$sort = 'Campaigns';
				break;
				case 'User':
				$sort = 'managers.full_name';
				break;
				default:
				$sort = 'facebook_ads_lead_users.created_time';
			}
			
			$data = array();
			$rows = array();
			
			$columns = ['clients.client_no', 'packages.name', 'clients.name', 'clients.created_at', 'managers.full_name', 'vendors.name'];
			
			$TotalClients = DB::table('clients')
			->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
			->leftJoin('managers', 'clients.manager_id', '=', 'managers.id')
			->leftJoin('vendors', 'clients.vendor_id', '=', 'vendors.id')
			->leftJoin('campaigns', 'clients.id', '=', 'campaigns.client_id')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->selectRAW('clients.client_no, clients.id, clients.name, clients.manager_id, clients.vendor_id, clients.status, clients.status, clients.created_at, packages.name as package_name, managers.full_name as manager_name, vendors.name as vendor_name, count(DISTINCT campaigns.id) as Campaigns, count(DISTINCT facebook_ads_lead_users.id) as LDs, count(DISTINCT facebook_ads_lead_users.campaign_id) as CPs, count(DISTINCT facebook_ads_lead_users.ad_id) as Ads')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('clients.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($ShowClient, $vendor_id){
				if($ShowClient=='Allotted Clients')
				{
					$query->where('clients.manager_id', Session::get('manager_id'));
				}
				else
				{
					if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
					{
						$query->where('clients.id','>', 0);
					}
					else
					{
						$query->where('clients.vendor_id', $vendor_id);
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					$query->whereDate('clients.created_at', '>=', $start_date)
					->whereDate('clients.created_at', '<=', $end_date);
				}
			})
			->where('clients.status', $status)
			->where('clients.is_deleted', 0)
			->orderBy('clients.name', 'ASC')
			->groupBy('clients.id')
			->get();
			
			$clients = DB::table('clients')
			->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
			->leftJoin('managers', 'clients.manager_id', '=', 'managers.id')
			->leftJoin('vendors', 'clients.vendor_id', '=', 'vendors.id')
			->leftJoin('campaigns', 'clients.id', '=', 'campaigns.client_id')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->selectRAW('clients.client_no, clients.id, clients.name, clients.manager_id, clients.vendor_id, clients.status, clients.created_at, packages.name as package_name, managers.full_name as manager_name, vendors.name as vendor_name, count(DISTINCT campaigns.id) as Campaigns, count(DISTINCT facebook_ads_lead_users.id) as LDs, count(DISTINCT facebook_ads_lead_users.campaign_id) as CPs, count(DISTINCT facebook_ads_lead_users.ad_id) as Ads')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('clients.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($ShowClient, $vendor_id){
				if($ShowClient=='Allotted Clients')
				{
					$query->where('clients.manager_id', Session::get('manager_id'));
				}
				else
				{
					if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
					{
						$query->where('clients.id','>', 0);
					}
					else
					{
						$query->where('clients.vendor_id', $vendor_id);
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					$query->whereDate('clients.created_at', '>=', $start_date)
					->whereDate('clients.created_at', '<=', $end_date);
				}
			})
			->where('clients.status', $status)
			->where('clients.is_deleted', 0)
			->groupBy('clients.id')
			->orderBy($sort, $order)
			->skip($offset)
			->take($limit)
			->get();
			
			$sno = $offset+1;
			
			foreach($clients as $client)
			{
				$user = array();
				$user[''] = '<input type="checkbox" class="client_id" value="'.$client->id.'" />';
				$user['ID'] = $client->client_no;
				$user['Date'] = date('m-d-Y',strtotime($client->created_at));
				
				if($client->status==1) {
					$user['Status'] = 'Active'; 
					}else{ 
					$user['Status'] = 'Inactive'; 
				}
				
				$user['Client'] = $client->name;
				
				if($client->vendor_name==null || $client->vendor_name=='') {
					$user['Owner'] = 'Admin'; 
					}else{ 
					$user['Owner'] = $client->vendor_name; 
				}
				
				$user['Package'] = $client->package_name;
				$user['Ads'] = $client->Ads;
				$user['CPs'] = $client->CPs;
				if($LeadList=='Yes')
				{
					$user['LDs'] = '<a href="leads?client_id='.$client->id.'">'.$client->LDs.'</a>';
					}else{ 
					$user['LDs'] = $client->LDs; 
				}
				
				$CRM = '';
				
				if($client->Campaigns > 0)
				{
					
					if($WritePermission == 'Yes')
					{
						$CRM = $CRM.'<i class="fa fa-pencil-square-o actions_icon" aria-hidden="true" title="Create" onClick="CreateCRM('.$client->id.')"></i>';
					}
					$CRM = $CRM.'<a href="crm_account/'.$client->id.'" title="View"><i class="fa fa-file-text-o" aria-hidden="true"></i></a>';
				}
				
				$user['CRM'] = $CRM;
				
				if($client->manager_name == null || $client->manager_name == '')
				{
					$user['User'] = '';
				}
				else
				{
					$user['User'] = $client->manager_name;
				}
				
				$Action = '';
				
				if($WritePermission=='Yes')
				{
					if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
					{
						if($client->vendor_id  == 0)
						{
							$Action = $Action.'<a href="clients?id='.$client->id.'"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit Client"></i></a> ';
						}
						else
						{
							$Action = $Action.'<a href="clients?v=edit&id='.$client->id.'"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit Client"></i></a> ';
						}
					}
					else
					{
						$Action = $Action.'<a href="clients?id='.$client->id.'"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit Client"></i></a> ';
					}
					
					if(Session::get('user_category')=='vendor' || Session::get('user_category')=='admin')
					{
						if($client->manager_id==0)
						{
							$Action = $Action.'<i class="fa fa-user-plus text-primary actions_icon" onclick="UserClientAllot('.$client->id.');" title="Allot Client to User"></i> ';
						}
						else
						{
							$Action = $Action.'<i class="fa fa-user-plus text-success actions_icon" onclick="UserClientReAllot('.$client->id.','.$client->manager_id.');" title="Re-Allot Client to another User"></i> ';
						}
					}
					
					$Action = $Action.'<i class="fa fa-fw fa-times text-danger actions_icon" title="Delete Client" onClick="ClientDelete('.$client->id.')"></i> ';
					
					if($client->status == 1)
					{
						$Action = $Action.'<i class="fa fa-fw fa-star text-danger actions_icon" title="Inactive Client" onClick="ClientInactive('.$client->id.')"></i>';
					}
					else
					{
						$Action = $Action.'<i class="fa fa-fw fa-star text-success actions_icon" title="Active Client" onClick="ClientActive('.$client->id.')"></i>';
					}
				}
				
				$user['Action'] = $Action;
				
				$rows[] = $user;
			}		
			
			$data['total'] = count($TotalClients);
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
		
		public function addClientSubmit(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Clients Read Write');
			}
			
			$rule=[
			'name'=>'required',
			'address'=>'required',
			'zip'=>'required|integer|zip_code_valid',
			'mobile'=>'required|phone_format|unique:clients,mobile',
			'email'=>'required|unique:users,email|email',
			'url'=>'required|url',
			'invoice_email'=>'nullable|email',
			'contact_name'=>'required',
			'package_id'=>'required',
			'group_category_id'=>'required',
			'group_sub_category_id'=>'required',
			'roi_automation'=>'required',
			'start_date'=>'required',
			'pro_rate'=>'required',
			'photo'=>'image|dimensions:width=180,height=180',
			'page_engagements'=>'nullable|in:On,Off'
			];
			
			$this->validate($request,$rule);
			
			$facebook_ads_lead_id = '';
			if(count($request->lead_form)>0)			
			{				
				$facebook_ads_lead_id = implode(',', $request->lead_form);				
			}			
			
			$facebook_page_id = '';
			if(count($request->facebook_pages) > 0)			
			{				
				$facebook_page_id = implode(',', $request->facebook_pages);				
			}			
			
			$photo='';
			if ($request->hasFile('photo')) 
			{
				$image=$request->file('photo');
				$photo=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/clients/',$photo);
			}
			
			$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($request->zip)."&sensor=false";
			$result_string = file_get_contents($url);
			$result = json_decode($result_string, true);
			$val = isset($result['results'][0]['geometry']['location']) ? $result['results'][0]['geometry']['location'] : array();
			$latitude = isset($val['lat']) ? $val['lat'] : '';
			$longitude = isset($val['lng']) ? $val['lng'] : '';
			
			$Address = Helper::get_zip_info($request->zip);
			$city=$Address['city'];
			$state=$Address['state'];
			
			$s_date = explode("-",$request->start_date);
			$start_date = $s_date[2].'-'.$s_date[0].'-'.$s_date[1];
			
			$page_engagement_status=($request->has('page_engagements')) ? $request->page_engagements : '';
			
			$client_id = DB::table('clients')->insertGetId([
			'vendor_id' => $request->vendor_id,
			'name' => $request->name,
			'address' => $request->address,
			'city' => $city,
			'state' => $state,
			'zip' => $request->zip,
			'mobile' => $request->mobile,
			'email' => $request->email,
			'url' => $request->url,
			'group_category_id' => $request->group_category_id,
			'group_sub_category_id' => $request->group_sub_category_id,
			'contact_name' => $request->contact_name,
			'package_id' => $request->package_id,
			'per_sold_value' => $request->per_sold_value,
			'discount' => $request->discount,
			'is_override' => $request->is_override,
			'crm_email' => $request->crm_email,
			'invoice_email'=>$request->invoice_email,
			'roi_automation' => $request->roi_automation,
			'roi_id' => $request->roi_id,
			'photo' => $photo,
			'start_date' => $start_date,
			'pro_rate' => $request->pro_rate,
			'latitude' => $latitude,
			'longitude' => $longitude,
			'facebook_ads_lead_id' => $facebook_ads_lead_id,
			'facebook_page_id' => $facebook_page_id,
			'page_engagements' => $page_engagement_status,
			'created_user_id' => Session::get('user_id'),
			'created_user_ip' => $request->ip(),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_user_id' => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			if($client_id > 999){
				$client_no = 'POW'.$client_id;
				}elseif($client_id > 99){
				$client_no = 'POW0'.$client_id;
				}elseif($client_id > 9){
				$client_no = 'POW00'.$client_id;
				}else{
				$client_no = 'POW000'.$client_id;
			}
			
			DB::table('clients')->where('id', $client_id)->update(['client_no' => $client_no]);
			
			if($request->page_engagements=="On")
			{
				$facebook_pages = $request->facebook_pages;
				for($i = 0; $i < count($facebook_pages); $i++)
				{
					if($facebook_pages[$i] != '')
					{
						DB::table('page_engagements')->insert([
						"client_id" => $client_id,
						"facebook_page_id" => $facebook_pages[$i],
						"created_user_id" => Session::get('user_id'),
						"created_at" => date('Y-m-d H:i:s'),
						"updated_user_id" => Session::get('user_id'),
						"updated_at" => date('Y-m-d H:i:s')
						]);
					}
				}
			}           
			
			$lead_form = $request->lead_form;
			for($i = 0; $i < count($lead_form); $i++)
			{
				if($lead_form[$i] != '')
				{
					DB::table('client_lead_forms')->insert([
					"client_id" => $client_id,
					"facebook_ads_lead_id" => $lead_form[$i],
					"created_user_id" => Session::get('user_id'),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
			}
			
			do 
			{
				$user_password = Helper::GenerateRandomPassword(8,true,true,true,true);
			}
			while($user_password=="FALSE");
			
			$password=password_hash($user_password,PASSWORD_BCRYPT); 
			
			DB::table('users')->insert([
			'client_id' => $client_id,
			'name' => $request->name,
			'email' => $request->email,
			'address' => $request->address,
			'password' => $password,
			'category' => 'client',
			'created_user_id' => Session::get('user_id'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_user_id' => Session::get('user_id'),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			$data = array('name' => $request->name, 'email' => $request->email, 'user_password' => $user_password);
			
			Mail::send('emails.login_details', $data, function ($message) use ($request) {
				$message->from('noreply@pownder.com', 'Pownder');
				$message->to($request->email)->subject('Pownder™ Credentials');
			});
			
			return Redirect::back()->with('alert_success','Client added successfully');        
		}
		
		public function editClientSubmit(Request $request)
		{
			if(Session::get('user_category') == 'user')
			{
				ManagerHelper::ManagerMenuAction('Clients Read Write');
			}
			
			$user = DB::table('users')->where('client_id', $request->client_id)->first();
			
			$rule = [
			'client_name' => 'required',
			'client_address' => 'required',
			'client_zip' => 'required|integer|zip_code_valid',
			'client_mobile' => 'required|phone_format|unique:clients,mobile,'.$request->client_id.'',
			'client_email' => 'required|unique:users,email,'.$user->id.'|email',
			'client_url' => 'required|url',
			'invoice_email' => 'nullable|email',
			'client_contact_name' => 'required',
			'client_package_id' => 'required',
			'client_group_category_id' => 'required',
			'client_group_sub_category_id' => 'required',
			'client_roi_automation' => 'required',
			'client_start_date' => 'required',
			'client_pro_rate' => 'required',
			'client_photo' => 'image|dimensions:width=180,height=180',
			'client_page_engagements' => 'nullable|in:On,Off'
			];
			
			$this->validate($request,$rule);
			
			$client_id = $request->client_id;
			
			$facebook_ads_lead_id = '';
			if(count($request->client_lead_form) > 0)			
			{				
				$facebook_ads_lead_id = implode(',', $request->client_lead_form);				
			}	
			
			$facebook_page_id = '';
			if(count($request->client_facebook_pages) > 0)			
			{				
				$facebook_page_id = implode(',', $request->client_facebook_pages);				
			}	
			
			$client = DB::table('clients')->where('id', $request->client_id)->first();
			
			if ($request->hasFile('client_photo')) 
			{
				$image = $request->file('client_photo');
				$photo = date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/clients/', $photo);
			}
			else
			{
				$photo = $client->photo;
			}
			
			$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($request->client_zip)."&sensor=false";
			$result_string = file_get_contents($url);
			$result = json_decode($result_string, true);
			$val = isset($result['results'][0]['geometry']['location']) ? $result['results'][0]['geometry']['location'] : array();
			$latitude = isset($val['lat']) ? $val['lat'] : '';
			$longitude = isset($val['lng']) ? $val['lng'] : '';
			
			$Address = Helper::get_zip_info($request->client_zip);
			$city = $Address['city'];
			$state = $Address['state'];
			
			$s_date = explode("-",$request->client_start_date);
			$start_date = $s_date[2].'-'.$s_date[0].'-'.$s_date[1];
			
			$page_engagement_status = ($request->has('client_page_engagements')) ? $request->client_page_engagements : '';
			
			DB::table('clients')->where('id', $request->client_id)->where('is_deleted', 0)
			->update([
			'vendor_id' => $request->client_vendor_id,
			'name' => $request->client_name,
			'address' => $request->client_address,
			'city' => $city,
			'state' => $state,
			'zip' => $request->client_zip,
			'mobile' => $request->client_mobile,
			'email' => $request->client_email,
			'url' => $request->client_url,
			'group_category_id' => $request->client_group_category_id,
			'group_sub_category_id' => $request->client_group_sub_category_id,
			'contact_name' => $request->client_contact_name,
			'package_id' => $request->client_package_id,
			'per_sold_value' => $request->client_per_sold_value,
			'discount' => $request->client_discount,
			'is_override' => $request->client_is_override,
			'crm_email' => $request->client_crm_email,
			'invoice_email' => $request->invoice_email,
			'roi_automation' => $request->client_roi_automation,
			'roi_id' => $request->client_roi_id,
			'photo' => $photo,
			'start_date' => $start_date,
			'pro_rate' => $request->client_pro_rate,
			'latitude' => $latitude,
			'longitude' => $longitude,
			'facebook_ads_lead_id' => $facebook_ads_lead_id,
			'facebook_page_id' => $facebook_page_id,
			'page_engagements' => $page_engagement_status,
			'updated_user_id' => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			DB::table('client_lead_forms')->where('client_id', $request->client_id)
			->update(['is_deleted' => 1]);
			
			DB::table('page_engagements')->where('client_id', $request->client_id)
			->update(['is_deleted' => 1]);
			
			if($request->client_page_engagements == "On")
			{
				$facebook_pages = $request->client_facebook_pages;
				for($i = 0; $i < count($facebook_pages); $i++)
				{
					if($facebook_pages[$i] != '')
					{
						$checkExists = DB::table('page_engagements')->where('client_id', $request->client_id)->where('facebook_page_id', $facebook_pages[$i])->where('created_user_id', Session::get('user_id'))->first();
						if(is_null($checkExists))
						{
							DB::table('page_engagements')->insert([
							"client_id" => $request->client_id,
							"facebook_page_id" => $facebook_pages[$i],
							"created_user_id" => Session::get('user_id'),
							"created_at" => date('Y-m-d H:i:s'),
							"updated_user_id" => Session::get('user_id'),
							"updated_at" => date('Y-m-d H:i:s')
							]);
						}
						else
						{
							DB::table('page_engagements')->where('client_id', $request->client_id)->where('facebook_page_id', $facebook_pages[$i])
							->update(["updated_user_id" => Session::get('user_id'), "updated_at" => date('Y-m-d H:i:s'), "is_deleted" => 0]);
						}
					}
				}
			}             
			
			$lead_form = $request->client_lead_form;
			for($i=0; $i<count($lead_form); $i++)
			{
				if($lead_form[$i] != '')
				{
					$checkExists = DB::table('client_lead_forms')->where('client_id', $request->client_id)->where('facebook_ads_lead_id', $lead_form[$i])->where('created_user_id', Session::get('user_id'))->first();
					if(is_null($checkExists))
					{
						DB::table('client_lead_forms')->insert([
						"client_id" => $request->client_id,
						"facebook_ads_lead_id" => $lead_form[$i],
						"created_user_id" => Session::get('user_id'),
						"created_at" => date('Y-m-d H:i:s'),
						"updated_user_id" => Session::get('user_id'),
						"updated_at" => date('Y-m-d H:i:s')
						]);
					}
					else
					{
						DB::table('client_lead_forms')->where('client_id', $request->client_id)->where('facebook_ads_lead_id', $lead_form[$i])
						->update(["updated_user_id" => Session::get('user_id'), "updated_at" => date('Y-m-d H:i:s'), "is_deleted" => 0]);
					}
				}
			}
			
			if($request->client_email == $user->email)
			{
				DB::table('users')->where('client_id', $request->client_id)->where('is_deleted', 0)
				->update(['name' => $request->client_name, 'address' => $request->client_address, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			else
			{
				do 
				{
					$user_password = Helper::GenerateRandomPassword(8,true,true,true,true);
				}
				while($user_password=="FALSE");
				
				$password=password_hash($user_password,PASSWORD_BCRYPT); 
				
				DB::table('users')->where('client_id', $request->client_id)->where('is_deleted', 0)
				->update(['name'=>$request->client_name, 'email'=>$request->client_email, 'address'=>$request->client_address, 'password'=>$password, 'updated_user_id'=>Session::get('user_id'), 'updated_at'=>date('Y-m-d H:i:s')]);
				
				$data = array('name' => $request->client_name, 'email' => $request->client_email, 'user_password' => $user_password);
				
				Mail::send('emails.login_details', $data, function ($message) use ($request) {
					$message->from('noreply@pownder.com', 'Pownder');
					$message->to($request->client_email)->subject('Pownder™ Credentials');
				});
			}
			
			if(count($request->client_lead_form) > 0)
			{
				$lead_form = $request->client_lead_form;
			}
			else
			{
				$lead_form = array();
			}
			
			$OldClientForm = explode(",", $client->facebook_ads_lead_id);
			$campaignForm = array();
			$campaignPage = array();
			$campaign = DB::table('campaigns')->where('client_id', $request->client_id)->where('is_deleted', 0)->orderBy('id', 'ASC')->first();
			if(!is_null($campaign))
			{
				$campaign_id = $campaign->id;
				$client_id = $request->client_id;
				$campaign_form = explode(",", $campaign->facebook_ads_lead_id);
				$GroupForm = array_diff($campaign_form, $OldClientForm);
				$campaignForm = array_merge($GroupForm, $lead_form);
				
				$facebook_ads_leads = DB::table('facebook_ads_leads')->select('facebook_page_id')->whereIn('id', $campaignForm)->groupBy('facebook_page_id')->where('is_blocked', 0)->where('is_deleted', 0)->get();
				foreach($facebook_ads_leads as $facebook_ads_lead)
				{
					$campaignPage[] = $facebook_ads_lead->facebook_page_id;
				}
				
				$facebook_ads_lead_id = implode(',', $campaignForm);
				$facebook_page_id = implode(',', $campaignPage);
				
				DB::table('campaigns')->where('id', $campaign_id)
				->update(['facebook_page_id' => $facebook_page_id, 'facebook_ads_lead_id' => $facebook_ads_lead_id, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
				
				DB::table('campaign_logs')->insert([
				'client_id' => $client_id,
				'campaign_id' => $campaign_id,
				'facebook_page_id' => $facebook_page_id,
				'facebook_ads_lead_id' => $facebook_ads_lead_id,
				'created_user_id' => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_user_id' => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			
			return redirect('clients')->with('alert_success','Client updated successfully');        
		}
		
		public function deleteClient(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Clients Read Write');
			}
			
			$client_id=$request->id;
			
			DB::table('clients')->where('id', $request->id)
			->update(['updated_user_id'=>Session::get('user_id'), 'updated_at'=>date('Y-m-d H:i:s'), 'is_deleted'=>1]);
			
			$user = DB::table('users')->where('client_id', $request->id)->first();
			
			DB::table('users')->where('client_id', $request->id)
			->update(['email' => $user->email.'(del)', 'updated_user_id'=>Session::get('user_id'), 'updated_at'=>date('Y-m-d H:i:s'), 'is_deleted'=>1]);
			
			return redirect('clients')->with('alert_success','Client deleted successfully');        
		}
		
		public function inactiveClient(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Clients Read Write');
			}
			
			DB::table('clients')->where('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 0]);
			
			DB::table('users')->where('client_id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 0]);
			
			return redirect('clients')->with('alert_success','Client inactive successfully');        
		}
		
		public function inactiveBulkClient(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Clients Read Write');
			}
			
			DB::table('clients')->whereIn('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 0]);
			
			DB::table('users')->whereIn('client_id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 0]);
		}
		
		public function activeClient(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Clients Read Write');
			}
			
			DB::table('clients')->where('id', $request->id)
			->update(['updated_user_id'=>Session::get('user_id'), 'updated_at'=>date('Y-m-d H:i:s'), 'status'=>1]);
			
			DB::table('users')->where('client_id', $request->id)
			->update(['updated_user_id'=>Session::get('user_id'), 'updated_at'=>date('Y-m-d H:i:s'), 'status'=>1]);
			
			return redirect('clients')->with('alert_success','Client active successfully');        
		}
		
		public function activeBulkClient(Request $request)
		{
			if(Session::get('user_category')=='user')
			{    
				ManagerHelper::ManagerMenuAction('Clients Read Write');
			}    
			
			DB::table('clients')->whereIn('id', $request->id)
			->update(['updated_user_id'=>Session::get('user_id'), 'updated_at'=>date('Y-m-d H:i:s'), 'status'=>1]);
			
			DB::table('users')->whereIn('client_id', $request->id)
			->update(['updated_user_id'=>Session::get('user_id'), 'updated_at'=>date('Y-m-d H:i:s'), 'status'=>1]);
		}
		
		public function editClient()
		{
			$Packages = DB::table('packages')->select('id','name')->orderBy('name', 'ASC')->get();
			
			$client = DB::table('clients')
			->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
			->select('clients.*', 'packages.name as package_name')
			->where('clients.id',Session::get('client_id'))
			->first();
			
			return view('panel.frontend.edit_client',['Packages'=>$Packages, 'client'=>$client]);
		}
		
		public function editClientProfile(Request $request)
		{
			$rule=[
			'name'=>'required',
			'address'=>'required',
			'zip'=>'required|integer|zip_code_valid',
			'mobile'=>'required|phone_format|unique:clients,mobile,'.Session::get('client_id').'',
			'email'=>'required|email|unique:users,email,'.Session::get('user_id').'',
			'url'=>'required|url',
			'contact_name'=>'required',
			'invoice_email'=>'nullable|email',
			'crm_email'=>'required|email',
			'photo'=>'image|dimensions:width=180,height=180',
			];
			
			$this->validate($request,$rule);
			
			if ($request->hasFile('photo')) 
			{
				$image=$request->file('photo');
				$photo=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/clients/',$photo);
			}
			else
			{
				$client_result=DB::table('clients')->select('photo')->where('id', Session::get('client_id'))->first();
				$photo=$client_result->photo;
			}
			
			$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($request->zip)."&sensor=false";
			$result_string = file_get_contents($url);
			$result = json_decode($result_string, true);
			$val = isset($result['results'][0]['geometry']['location']) ? $result['results'][0]['geometry']['location'] : array();
			$latitude = isset($val['lat']) ? $val['lat'] : '';
			$longitude = isset($val['lng']) ? $val['lng'] : '';
			
			
			$Address = Helper::get_zip_info($request->zip);
			$city=$Address['city'];
			$state=$Address['state'];
			
			DB::table('clients')
			->where('id', Session::get('client_id'))
			->update([
			'name'=>$request->name,
			'email'=>$request->email,
			'address'=>$request->address,
			'city'=>$city,
			'state'=>$state,
			'zip'=>$request->zip,
			'mobile'=>$request->mobile,
			'url'=>$request->url,
			'contact_name'=>$request->contact_name,
			"invoice_email" => $request->invoice_email,
			'crm_email'=>$request->crm_email,
			'photo'=>$photo,
			'latitude'=>$latitude,
			'longitude'=>$longitude,
			'updated_user_id'=>Session::get('user_id'),
			'updated_at'=>date('Y-m-d H:i:s')
			]);
			
			$user=DB::table('users')->where('id',Session::get('user_id'))->first();
			if($request->email==$user->email)
			{
				DB::table('users')->where('id', Session::get('user_id'))
				->update(['name'=>$request->name, 'address'=>$request->address, 'updated_user_id'=>Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at'=>date('Y-m-d H:i:s')]);
			}
			else
			{
				do 
				{
					$user_password = Helper::GenerateRandomPassword(8,true,true,true,true);
				}
				while($user_password=="FALSE");
				
				$password=password_hash($user_password,PASSWORD_BCRYPT); 
				
				DB::table('users')->where('id', Session::get('user_id'))
				->update([
				'name'=>$request->name,
				'email'=>$request->email,
				'password'=>$password,
				'address'=>$request->address,
				'updated_user_id'=>Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				'updated_at'=>date('Y-m-d H:i:s')
				]);
				
				$data = array('name' => $request->name, 'email' => $request->email, 'user_password' => $user_password);
				
				Mail::send('emails.login_details', $data, function ($message) use ($request) {
					$message->from('noreply@pownder.com', 'Pownder');
					$message->to($request->email)->subject('Pownder™ Credentials');
				});
				
				Session::put('EmailChange','Yes');
			}
			
			Session::put('user_name',$request->name);
			Session::put('photo',$photo);
			
			return Redirect::back()->with('alert_success','Profile updated successfully'); 
		}
		
		public function changePassword(Request $request)
		{
			$rule = ['password' => 'required|min:8|confirmed', 'password_confirmation' => 'required'];
			
			$this->validate($request, $rule);
			
			DB::table('users')->where('id', Session::get('user_id'))
			->update(['password'=>password_hash($request->password,PASSWORD_BCRYPT), 'updated_user_id'=>Session::get('user_id'), 'updated_at'=>date('Y-m-d H:i:s')]);
			
			return Redirect::back()->with('alert_success','Password changed successfully'); 
		}
		
		public function addCRMAccount(Request $request)
		{	
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Clients Read Write');
			}
			
			if($request->crm_account!='Agile' && $request->crm_account!='Zoho')
			{
				$rule = ['campaign' => 'required', 'crm_account' => 'required',];
				
				$this->validate($request,$rule);
				
				DB::table('campaigns')->where('id', $request->campaign)
				->update(['crm_account' => $request->crm_account, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
				
				$check=DB::table('crm_accounts')->where('account',$request->crm_account)->where('campaign_id',$request->campaign)->where('client_id',$request->crm_client_id)->first();
				if(is_null($check))
				{
					DB::table('crm_accounts')->insert([
					'campaign_id'=>$request->campaign,
					'client_id'=>$request->crm_client_id,
					'account'=>$request->crm_account,
					'created_user_id'=>Session::get('user_id'),
					'created_at'=>date('Y-m-d H:i:s'),
					'updated_user_id'=>Session::get('user_id'),
					'updated_at'=>date('Y-m-d H:i:s')
					]);
					
					return "success";
				}
				else
				{
					DB::table('crm_accounts')->where('account',$request->crm_account)->where('campaign_id',$request->campaign)->where('client_id',$request->crm_client_id)
					->update(['updated_user_id'=>Session::get('user_id'), 'updated_at'=>date('Y-m-d H:i:s')]);
					return "success";
				}
			}
			else
			{
				if($request->crm_account=='Agile')
				{
					$rule = ['campaign' => 'required', 'crm_account' => 'required', 'crm_domain' => 'required', 'crm_username' => 'required|email', 'crm_password' => 'required'];
					
					$this->validate($request,$rule);
					
					$status=ConsoleHelper::CheckValidAgileAccount($request->crm_domain, $request->crm_username, $request->crm_password);
					
					if($status=='200')
					{
						$check=DB::table('crm_accounts')->where('account',$request->crm_account)->where('domain',$request->crm_domain)->where('username',$request->crm_username)->where('password',$request->crm_password)->first();
						if(is_null($check))
						{
							DB::table('crm_accounts')->insert([
							'campaign_id'=>$request->campaign,
							'client_id'=>$request->crm_client_id,
							'account'=>$request->crm_account,
							'domain'=>$request->crm_domain,
							'username'=>$request->crm_username,
							'password'=>$request->crm_password,
							'created_user_id'=>Session::get('user_id'),
							'created_at'=>date('Y-m-d H:i:s'),
							'updated_user_id'=>Session::get('user_id'),
							'updated_at'=>date('Y-m-d H:i:s')
							]);
							
							return "success";
						}
						else
						{
							return "CRM Account Already Exists";
						}
					}
					else
					{
						return "CRM Account Invalid";
					}
				}
				elseif($request->crm_account=='Zoho')
				{
					$rule = ['campaign' => 'required', 'crm_account' => 'required', 'crm_username' => 'required|email', 'crm_password' => 'required'];
					
					$this->validate($request,$rule);
					
					$access_token=ConsoleHelper::ZohoOauthToken($request->crm_username, $request->crm_password);
					
					if($access_token!='')
					{
						$check=DB::table('crm_accounts')->where('account',$request->crm_account)->where('username',$request->crm_username)->where('password',$request->crm_password)->first();
						if(is_null($check)){
							
							DB::table('crm_accounts')->insert([
							'campaign_id'=>$request->campaign,
							'client_id'=>$request->crm_client_id,
							'account'=>$request->crm_account,
							'username'=>$request->crm_username,
							'password'=>$request->crm_password,
							'access_token'=>$access_token,
							'created_user_id'=>Session::get('user_id'),
							'created_at'=>date('Y-m-d H:i:s'),
							'updated_user_id'=>Session::get('user_id'),
							'updated_at'=>date('Y-m-d H:i:s')
							]);
							
							return "success";
						}
						else
						{
							return "CRM Account Already Exists";
						}
					}
					else
					{
						return "CRM Account Invalid";
					}
				}
				
				return "CRM Account Invalid";
			}
		}
		
		public function getCRMAccount($client_id)
		{
			$WritePermission = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$WritePermission = ManagerHelper::ManagerWritePermission('Clients Read Write');
			}
			
			$CRMAccountColumns=array();
			
			$Columns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Client CRM Account Table')->where('created_user_id', Session::get('user_id'))	->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$CRMAccountColumns[]=$Column->column_value;
			}
			
			$CRMAccounts=DB::table('crm_accounts')
			->leftJoin('clients', 'crm_accounts.client_id', '=', 'clients.id')
			->select('crm_accounts.id', 'crm_accounts.client_id', 'clients.name', 'crm_accounts.created_at', 'crm_accounts.is_deleted', 'crm_accounts.account')
			->where('crm_accounts.client_id',$client_id)
			->where('clients.is_deleted',0)
			->get();
			
			return view('panel.frontend.client_crm_account',['CRMAccounts'=>$CRMAccounts, 'CRMAccountColumns'=>$CRMAccountColumns, 'WritePermission' => $WritePermission]);
		}
		
		public function changeCRMAccountStatus(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Clients Read Write');
			}
			
			DB::table('crm_accounts')->where('id', $request->id)
			->update(['updated_user_id'=>Session::get('user_id'), 'updated_at'=>date('Y-m-d H:i:s'), 'is_deleted'=>$request->is_deleted]);
		}
		
		public function postUserClientAllot(Request $request)
		{			
			$client_id = $request->AllotUserClientId;
			$manager_id = $request->manager_id;
			
			DB::table('clients')->where('id', $client_id)
			->update(['manager_id' => $manager_id, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			
			DB::table('user_allotted_clients')->insert([
			"manager_id" => $manager_id,
			"client_id" => $client_id,
			"created_user_id" => Session::get('user_id'),
			'created_user_ip' => $request->ip(),
			"created_at" => date('Y-m-d H:i:s'),
			"updated_user_id" => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			"updated_at" => date('Y-m-d H:i:s')
			]);
			
			if($request->Action=='Allot')
			{
				return redirect('clients')->with('alert_success','Client allotted to user successfully');
			}
			else
			{
				return redirect('clients')->with('alert_success','Client re-allotted to another user successfully');
			}
		}
		
		public function getClientDashboardAjax(Request $request)
		{
			$GraphClick = array();
			$GraphCTR = array();
			$GraphFreq = array();
			$GraphLead = array();
			$GraphLGR = array();
			$Click = 0;
			$ShowLead = '';
			$WritePermission = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$WritePermission = ManagerHelper::ManagerWritePermission('Dashboard Read Write');
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				
				$manager = DB::table('managers')->select('client_id')->where('id', Session::get('manager_id'))->first();
				$client_id = $manager->client_id;
			}
			else
			{
				$client_id = Session::get('client_id');
			}			
			
			$date_start = $request->date_start;
			$date_stop = $request->date_stop;
			
			$datediff = floor((strtotime($date_stop) - strtotime($date_start))/(60 * 60 * 24));
			
			if($datediff<=30)
			{
				$campaign_s_date = 	$date_start;
				$campaign_e_date = 	$date_stop;
			}
			else
			{
				$campaign_s_date = date('Y-m').'-01';
				$campaign_e_date = date('Y-m-d');
			}
			
			$AllLeads = array();
			$FacebookLeads = array();
			
			$Leads = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
			})
			->where('client_id', $client_id)
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$CityLeads = DB::table('facebook_ads_lead_users')->selectRaw('count(id) as Leads, city, latitude, longitude')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
			})
			->where('client_id', $client_id)
			->where('city', '!=', '')
			->where('latitude', '!=', '')
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->groupBy('latitude')
			->groupBy('longitude')
			->get();
			
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->select('id')
			->where('lead_type', 'Pownder™ Lead')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', '=', Session::get('manager_id'));
				}
			})
			->where('client_id', $client_id)
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$Sold = DB::table('facebook_ads_lead_users')
			->join('sold_leads', 'facebook_ads_lead_users.id', '=', 'sold_leads.facebook_ads_lead_user_id')
			->select('sold_leads.id')
			->where('sold_leads.category', 'Sold')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
			})
			->where('sold_leads.client_id', $client_id)
			->whereDate('sold_leads.sold_date', '>=', $date_start)
			->whereDate('sold_leads.sold_date', '<=', $date_stop)
			->count('sold_leads.id');
			
			$Appointment = DB::table('facebook_ads_lead_users')->select('id')
			->where('setting', 'Appointment')
			->where(function ($query) use($ShowLead, $client_id){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where('client_id', $client_id)
			->whereDate('appointment_date', '>=', $date_start)
			->whereDate('appointment_date', '<=', $date_stop)
			->count('id');
			
			$CampaignArray = array();
			$client_campaigns = DB::table('facebook_ads_lead_users')->select('campaign_id')
			->where(function ($query) use($ShowLead, $client_id){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where('client_id', $client_id)
			->whereDate('created_time', '>=', $campaign_s_date)
			->whereDate('created_time', '<=', $campaign_e_date)
			->where('campaign_id', '!=', 0)
			->groupBy('campaign_id')
			->get();
			foreach($client_campaigns as $client_campaign)
			{
				$CampaignArray[] = $client_campaign->campaign_id;
			}
			
			if($ShowLead=='Allotted Leads')
			{
				$facebook_campaign_values = DB::table('facebook_campaign_values')
				->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
				->whereIn('campaign_id', array_unique($CampaignArray))
				->whereDate('campaign_date', '>=', $campaign_s_date)
				->whereDate('campaign_date', '<=', $campaign_e_date)
				->groupBy('campaign_date')->get();
				
				foreach($facebook_campaign_values as $facebook_campaign_value)
				{
					$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', Session::get('manager_id'))->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', Session::get('manager_id'))->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					$managerCampaignLeads = ManagerHelper::managerCampaignLeads($client_id, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					
					$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
					$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
					$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
					if($CampaignLeads > 0)
					{
						$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $managerCampaignLeads);
						$GraphClick[] = $PownderClick;
						$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $managerCampaignLeads;
						$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $managerCampaignLeads;
						
						if($PownderClick == 0 || $PownderLeads == 0)
						{
							$GraphLGR[] = 0;
						}
						else
						{
							$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
						}
					}
					else
					{
						$GraphClick[] = 0;
						$GraphCTR[] = 0;
						$GraphFreq[] = 0;
						$GraphLGR[] = 0;
					}
				}
				
				$TotalClicks = 0;
				$FacebookAccounts = DB::table('facebook_campaigns')
				->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
				->select('facebook_campaigns.id', 'facebook_accounts.access_token')
				->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
				->groupBy('facebook_campaigns.id')->get();
				foreach($FacebookAccounts as $FacebookAccount)
				{
					$campaign_id = $FacebookAccount->id;
					$access_token = $FacebookAccount->access_token;
					
					//Read facebook clicks, spend
					$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($curl);
					curl_close($curl);
					
					$result = json_decode($response,true);
					
					if(isset($result['data']))
					{
						$data = $result['data'];
						if(isset($data[0]['clicks']))
						{
							$TotalClicks = $TotalClicks + $data[0]['clicks'];
						}
					}
				}
				
				$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
				$managerCampaignLeads = ManagerHelper::managerCampaignLeads(Session::get('manager_id'), array_unique($CampaignArray), $date_start, $date_stop);
				if($CampaignLeads > 0)
				{
					$Click = ceil(($TotalClicks / $CampaignLeads) * $managerCampaignLeads);
				}
			}
			else
			{
				$facebook_campaign_values = DB::table('facebook_campaign_values')
				->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
				->whereIn('campaign_id', array_unique($CampaignArray))
				->whereDate('campaign_date', '>=', $campaign_s_date)
				->whereDate('campaign_date', '<=', $campaign_e_date)
				->groupBy('campaign_date')->get();
				foreach($facebook_campaign_values as $facebook_campaign_value)
				{
					$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					
					$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
					$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
					$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
					if($CampaignLeads > 0)
					{
						$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
						$GraphClick[] = $PownderClick;
						$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $clientCampaignLeads;
						$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $clientCampaignLeads;
						
						if($PownderClick == 0 || $PownderLeads == 0)
						{
							$GraphLGR[] = 0;
						}
						else
						{
							$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
						}
					}
					else
					{
						$GraphClick[] = 0;
						$GraphCTR[] = 0;
						$GraphFreq[] = 0;
						$GraphLGR[] = 0;
					}
				}
				
				$TotalClicks = 0;
				
				$FacebookAccounts = DB::table('facebook_campaigns')
				->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
				->select('facebook_campaigns.id', 'facebook_accounts.access_token')
				->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
				->groupBy('facebook_campaigns.id')->get();
				foreach($FacebookAccounts as $FacebookAccount)
				{
					$campaign_id = $FacebookAccount->id;
					$access_token = $FacebookAccount->access_token;
					
					//Read facebook clicks, spend
					$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($curl);
					curl_close($curl);
					
					$result = json_decode($response,true);
					
					if(isset($result['data']))
					{
						$data = $result['data'];
						if(isset($data[0]['clicks']))
						{
							$TotalClicks = $TotalClicks + $data[0]['clicks'];
						}
					}
				}
				
				$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
				$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $date_start, $date_stop);
				if($CampaignLeads > 0)
				{
					$Click = ceil(($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
				}
			}
			
			$Array = array();
			
			$Array['Leads'] = Helper::NumberFormat(number_format($Leads,2));
			
			$Array['Sold'] = Helper::NumberFormat(number_format($Sold,2));
			$Array['Appointment'] = Helper::NumberFormat(number_format($Appointment,2));
			
			$Array['CityLeads'] = $CityLeads;
			$Array['facebook_campaign_values'] = $facebook_campaign_values;
			$Array['GraphCTR'] = $GraphCTR;
			$Array['GraphClick'] = $GraphClick;
			$Array['GraphFreq'] = $GraphFreq;
			$Array['GraphLGR'] = $GraphLGR;
			$Array['GraphLead'] = $GraphLead;
			
			if($Click == 0 || $facebook_ads_lead_users == 0){
				$Array['LGR'] = '0%';
				}else{
				$Array['LGR'] = Helper::NumberFormat(number_format(($facebook_ads_lead_users/$Click)*100,2)).'%';
			}
			
			return response()->json($Array);
		}
	}																											