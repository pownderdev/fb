<?php
	namespace App\Http\Controllers\Panel;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	use View;
	use DB;
	use Session;
    use Redirect;
    
	class QuestionsController extends Controller
	{
	    var $now='';        
        
        public function __construct()
        {
            $this->setTimeZone('America/Los_Angeles');
            $this->now=$this->getCurrentDateTime();
		}
        public function setTimeZone($tz)
        {
			return  date_default_timezone_set($tz);
		}
        public function getCurrentDateTime()
        {
            date_default_timezone_set('America/Los_Angeles');
            return date('Y-m-d H:i:s');
		}
        //Money Format
        public function money_format1($number)
        {
			$number=str_replace(",","",$number);
			settype($number,"float");          
			setlocale(LC_MONETARY, 'en_US'); 
			$f_number=money_format('%!.2i',$number); 
			$f_number=($number<0) ? '-$'.($f_number* -1) : '$'.$f_number;
			return $f_number;
		}
        
        /** Bot Questions */
        
        //Show Manage Questions Page
        public function ManageQuestions(Request $request)
        {          
			$questions=DB::table('questions')->where([['is_deleted',0],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->orderBy('id','asc')->get();
            
            $active_questions=DB::table('questions')->where([['is_deleted',0],['status',1],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->count('id');
                        
            $edit_question='';
            
            if($request->has('edit'))
            {
              $edit_question=DB::table('questions')->where([['is_deleted',0],['id',$request->edit],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->first();
              
              if(is_null($edit_question))
              {
                return redirect()->route('questions')->with('alert_danger','Oops! Question not found');
              }  
            }            
            return view('panel.frontend.manage_questions',['questions'=>$questions,'edit_question'=>$edit_question , "active_questions"=>$active_questions]);   
		}
        //Add Question 
        public function AddQuestion(Request $request)
        {
            $rules=[
            "question_en"=>"required",
            "question_sp"=>"required",
            "input_format"=>"required"        
            ];
            
            $this->validate($request,$rules);
            
            $sequence=DB::table('questions')->where([['is_deleted',0],['status',1],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->max('sequence');
            $sequence++;
            
            DB::table('questions')->insert(["question_en"=>$request->question_en,"question_sp"=>$request->question_sp,"input_format"=>$request->input_format ,"required"=>$request->has('required') , "created_user_id"=>session('user')->id , 'sequence'=>$sequence , "created_user_category" => session('user')->category , "created_at" =>$this->now , "updated_at" =>$this->now ]);
            
            return Redirect::route('questions')->with('alert_success','Question added successfully.');
        }
        //Edit Question 
        public function EditQuestion(Request $request)
        {
            $rules=[
            "question_en"=>"required",
            "question_sp"=>"required",
            "input_format"=>"required"        
            ];
            
            $this->validate($request,$rules);
            
            DB::table('questions')->where([['is_deleted',0],['id',$request->question_id],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->update(["question_en"=>$request->question_en,"question_sp"=>$request->question_sp,"input_format"=>$request->input_format ,"required"=>$request->has('required') ,  "updated_at" =>$this->now ]);
            
            return Redirect::route('questions')->with('alert_success','Question updated successfully.');
        }
        //Delete Question
        public function DeleteQuestion(Request $request)
        {            
            $sequence=DB::table('questions')->where([['id',$request->question_id],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->select('sequence')->first();
            $sequence=$sequence->sequence;
            
            DB::table('questions')->where([['id',$request->question_id],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->update(['is_deleted'=>1,'sequence'=>0,"updated_at" =>$this->now]);
            
            if($sequence)
            {            
            DB::table('questions')->where([['is_deleted',0],['status',1],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category],['sequence','>',$sequence]])->update(['sequence'=>DB::raw('sequence-1'),"updated_at" =>$this->now]);
            }           
            Session::flash('alert_success','Question deleted successfully');
          
            return response()->make('{"code":200,"msg":"Question deleted successfully"}',200,["content-type"=>'application/json']);
         
        }
         //Enable Question
        public function EnableQuestion(Request $request)
        {
            $new_status=($request->current_status) ? 0 : 1;
            
            if($request->current_status)
            {
            $sequence=DB::table('questions')->where([['id',$request->question_id],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->select('sequence')->first();
            $sequence=$sequence->sequence;
            $new_sequence=0;            
            }
            else
            {
            $sequence=DB::table('questions')->where([['is_deleted',0],['status',1],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->max('sequence');
            $sequence++;
            $new_sequence=$sequence;
            }
            
            DB::table('questions')->where([['id',$request->question_id],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->update(['status'=>$new_status,'sequence'=>$new_sequence,"updated_at" =>$this->now]);
            
            if($request->current_status)
            {
            DB::table('questions')->where([['is_deleted',0],['status',1],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category],['sequence','>',$sequence]])->update(['sequence'=>DB::raw('sequence-1'),"updated_at" =>$this->now]);
            }                       
            return response()->make('{"code":200,"msg":"Question status updated successfully"}',200,["content-type"=>'application/json']);         
        }
        //Update Question Sequence
        public function ChangeQuestionSequence(Request $request)
        {            
            $arr=explode('***',$request->sequence,2); 
            $question_id=$arr[0]; $new_sequence=$arr[1];
            
            $sequence=DB::table('questions')->where([['id',$question_id],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->select('sequence')->first();
            
            if(is_null($sequence))
            {
             return response()->make('{"code":404,"msg":"Question not found."}',404,["content-type"=>'application/json']);             
            }
            $sequence=$sequence->sequence;
            
            DB::table('questions')->where([['is_deleted',0],['status',1],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category],['sequence',$new_sequence]])->update(['sequence'=>$sequence,"updated_at" =>$this->now]);            
            
            DB::table('questions')->where([['id',$question_id],['is_deleted',0],['status',1],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->update(['sequence'=>$new_sequence,"updated_at" =>$this->now]);
                              
            Session::flash('alert_success','Question sequence changed successfully');
          
            return response()->make('{"code":200,"msg":"Question sequence changed successfully"}',200,["content-type"=>'application/json']);
         
        }
        
        /** Bot Answers */
        
        //Show Manage Answers Page
        public function ManageAnswers(Request $request)
        {          
			$questions=DB::table('bot_answers')->where([["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->orderBy('id','asc')->get();
           
            $edit_question='';$unanswered_queries='';
            
            if($request->has('edit'))
            {
              $edit_question=DB::table('bot_answers')->where([['id',$request->edit],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->first();
              
              if(is_null($edit_question))
              {
                return redirect()->route('answers')->with('alert_danger','Oops! Answer not found');
              }  
            }  
            
            if($request->has('unanswered'))
            {
              $query_ids=explode(',',$request->unanswered);
              $queries=DB::table('bot_unanswered_query')->whereIn('id',$query_ids)->select('text')->get();
              $queries=json_decode($queries,true); $queries_text=array_column($queries,'text'); 
       	      $unanswered_queries=implode(',',$queries_text);                
            }          
            return view('panel.frontend.manage_answers',['questions'=>$questions,'edit_question'=>$edit_question,'unanswered_queries'=>$unanswered_queries]);   
		      
        }
        //Add Question 
        public function AddAnswer(Request $request)
        {
            $rules=[
            "question"=>"required",
            "answer.*"=>"required"       
            ];
            
            $this->validate($request,$rules);
            
            DB::table('bot_unanswered_query')->whereIn('text',explode(',',$request->question))->update(['is_deleted'=>1]);
                        
            DB::table('bot_answers')->insert(["question"=>trim(str_replace([", "," ,"," , "],",",$request->question)),"answer"=>implode('$%^',$request->answer) , "created_user_id"=>session('user')->id ,  "created_user_category" => session('user')->category , "created_at" =>$this->now , "updated_at" =>$this->now ]);
            
            return Redirect::route('answers')->with('alert_success','Answer added successfully.');
        }
        //Edit Answer 
        public function EditAnswer(Request $request)
        {
            $rules=[
            "question"=>"required",
            "answer.*"=>"required"       
            ];
            
            $this->validate($request,$rules);
            
            DB::table('bot_answers')->where([['id',$request->question_id],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->update(["question"=>trim(str_replace([", "," ,"," , "],",",$request->question)),"answer"=>implode('$%^',$request->answer) ,  "updated_at" =>$this->now ]);
            
            return Redirect::route('answers')->with('alert_success','Answer updated successfully.');
        }
        //Delete Answer
        public function DeleteAnswer(Request $request)
        {            
           
            DB::table('bot_answers')->where([['id',$request->question_id],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->delete();
            
            Session::flash('alert_success','Answer deleted successfully');
          
            return response()->make('{"code":200,"msg":"Answer deleted successfully"}',200,["content-type"=>'application/json']);
         
        }
         //Enable Answer
        public function EnableAnswer(Request $request)
        {
            $new_status=($request->current_status) ? 0 : 1;
                        
            DB::table('bot_answers')->where([['id',$request->question_id],["created_user_id",session('user')->id] , [ "created_user_category" , session('user')->category]])->update(['status'=>$new_status,"updated_at" =>$this->now]);
                                               
            return response()->make('{"code":200,"msg":"Answer status updated successfully"}',200,["content-type"=>'application/json']);         
        }
        
        //Check if answer already added
        public function CheckQuestionDuplicate(Request $request)
        {
          $match=DB::select('select count(id) as count_1 from bot_answers where FIND_IN_SET(LOWER("'.$request->question.'"),LOWER(question))>0');
          if($match[0]->count_1>0)
          {
            return '{"code":1}';
          }
          return '{"code":0}';
        }
        
	}           