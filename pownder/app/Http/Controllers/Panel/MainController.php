<?php
	namespace App\Http\Controllers\Panel;
	
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	
	use App\Models\Clients;
	use App\Models\Groups;
	use App\Models\Packages;
	
	use DB;
	use Redirect;
	use Session;
	use ManagerHelper;
	use ClientHelper;
	use AdminHelper;
	use Helper;
	use Mail;
	use Cookie;
	
	class MainController extends Controller
	{
		public function getcurrenttime()
		{
			return date('Y-m-d H:i:s');
		}  
		
		public function ShowLoginPage()
		{
			if (Session::has('user_category'))
			{
				if (Session::get('user_category')=='admin')
				{
					return Redirect::route('dashboard');
				}
				
				if (Session::get('user_category')=='vendor')
				{
					return Redirect::route('vdash');
				}
				
				if (Session::get('user_category')=='client')
				{
					return Redirect::route('cdash');
				}
				
				if (Session::get('user_category')=='user')
				{
					if(ManagerHelper::ManagerCategory() == 'admin')
					{
						if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
						{
							return Redirect::route('dashboard');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Campaigns')=='Yes')
						{
							return Redirect::route('campaigns');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
						{
							return Redirect::route('leads');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
						{
							return Redirect::route('calltrack');
						}
						
						if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
						{
							return Redirect::route('dashboard');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
						{
							return Redirect::route('reports');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Clients')=='Yes')
						{
							return Redirect::route('clients');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Vendors')=='Yes')
						{
							return Redirect::route('vendors');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Invoice')=='Yes')
						{
							return Redirect::route('invoice');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Setting')=='Yes')
						{
							return Redirect::route('settings');
						}
					}
					elseif(ManagerHelper::ManagerCategory() == 'vendor')
					{
						if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
						{
							return Redirect::route('vdash');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Campaigns')=='Yes')
						{
							return Redirect::route('campaigns');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
						{
							return Redirect::route('leads');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
						{
							return Redirect::route('calltrack');
						}
						
						if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
						{
							return Redirect::route('vdash');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
						{
							return Redirect::route('reports');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Clients')=='Yes')
						{
							return Redirect::route('clients');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Invoice')=='Yes')
						{
							return Redirect::route('invoice');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Setting')=='Yes')
						{
							return Redirect::route('settings');
						}
					}
					elseif(ManagerHelper::ManagerCategory() == 'client')
					{
						if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
						{
							return Redirect::route('cdash');
						}
						if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
						{
							return Redirect::route('cleads');
						}
						if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
						{
							return Redirect::route('calltrack');
						}
						if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
						{
							return Redirect::route('cdash');
						}
						if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
						{
							return Redirect::route('reports');
						}
					}
				}
			}
			return view('panel.Auth.login');  
		}
		
		public function checkLoginValidate(Request $request)
		{
			$rule=['email' => 'required|email', 'password' => 'required'];
			
			$this->validate($request,$rule);
		}
		
		public function checkLogin(Request $request)
		{
			$validate=$this->validate($request,['username'=>'required|email','password'=>'required']);      
			if($validate && $validate->fail())
			{
				return Redirect()->back()->withInput($request->all())->withErrors('errors',$validate);
			}
			
			$user=DB::table('users')->where('email', $request->username)->first();
			
			if(is_null($user))
			{
				$errors=array('loginfailed'=>'Invalid Credentials');
				return Redirect()->back()->withInput($request->all())->withErrors($errors);
			}
			else
			{	
				if(password_verify($request->password,$user->password))
				{
					if($request->has('remember_me'))
					{
						Cookie::queue("username", $request->username, time()+ (10 * 365 * 24 * 60 * 60));
						Cookie::queue("password", $request->password, time()+ (10 * 365 * 24 * 60 * 60));
					}
					else
					{
						Cookie::queue("username", '', time()+ (10 * 365 * 24 * 60 * 60));
						Cookie::queue("password", '', time()+ (10 * 365 * 24 * 60 * 60));
					}
					
					if($user->category=='user' && $user->is_confirm==1)
					{
						$token=password_hash(date('U'),PASSWORD_BCRYPT);
						DB::table('users')->where('email',$request->username)->update(['password_reset' => $token, "updated_at" => date('Y-m-d H:i:s')]);
						return Redirect('http://big.pownder.com/password_reset?action=password_reset&token='.$token.'&username='.$request->username);
					}
					
					if($user->is_deleted==0 && $user->status==1)
					{
						$user->name = $user->name;
						$user->user_id = $user->id;
						$user->email = $user->email;
						$user->category = $user->category;  
						Session::put('user', $user);
                        $table_r='';
                        if(session('user')->category=="admin")
                        {
							$table_r = "admins";
							$id_r = session('user')->admin_id;
						}
                        else if(session('user')->category == "vendor")
                        {
							$table_r = "vendors";
							$id_r = session('user')->vendor_id;
						} 
                        if($table_r)
                        {                        
							$data_r = DB::table($table_r)->where('id',$id_r)->select('mobile','city','state','zip','url')->first();
							$user_r = session('user');
							$user_r->mobile = $data_r->mobile;
							$user_r->city = $data_r->city;
							$user_r->state = $data_r->state;
							$user_r->zip = $data_r->zip;
							$user_r->url = $data_r->url;
							Session::put('user', $user_r);
						}
						Session::put('user_id', $user->id);
						Session::put('user_name', $user->name);
						Session::put('user_category', $user->category);
						Session::put('admin_id', $user->admin_id);
						Session::put('vendor_id', $user->vendor_id);
						Session::put('client_id', $user->client_id);
						Session::put('manager_id', $user->manager_id);
						
						if($user->category == 'admin')
						{
							$admin=DB::table('admins')->where('id', $user->admin_id)->first();
							Session::put('photo',$admin->photo);
							return Redirect::route('dashboard');
						}
						elseif($user->category=='vendor')
						{
							$vendor=DB::table('vendors')->where('id', $user->vendor_id)->first();
							Session::put('photo',$vendor->photo);
							return Redirect::route('vdash');
						}
						elseif($user->category=='client')
						{
							$client=DB::table('clients')->where('id', $user->client_id)->first();
							Session::put('photo',$client->photo);
							return Redirect::route('cdash');
						}
						elseif($user->category=='user')
						{
							$manager=DB::table('managers')->where('id', $user->manager_id)->first();
							Session::put('photo',$manager->picture);
							
							$data = DB::table('users')->where('id', $user->created_user_id)->first();
							
							if($data->category=='admin')
							{
								if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
								{
									return Redirect::route('dashboard');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Campaigns')=='Yes')
								{
									return Redirect::route('campaigns');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
								{
									return Redirect::route('leads');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
								{
									return Redirect::route('calltrack');
								}
								
								if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
								{
									return Redirect::route('dashboard');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
								{
									return Redirect::route('reports');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Clients')=='Yes')
								{
									return Redirect::route('clients');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Vendors')=='Yes')
								{
									return Redirect::route('vendors');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Invoice')=='Yes')
								{
									return Redirect::route('invoice');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Expense')=='Yes')
								{
									return Redirect::route('expenses');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Setting')=='Yes')
								{
									return Redirect::route('settings');
								}
							}
							elseif($data->category=='vendor')
							{
								if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
								{
									return Redirect::route('vdash');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Campaigns')=='Yes')
								{
									return Redirect::route('campaigns');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
								{
									return Redirect::route('leads');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
								{
									return Redirect::route('calltrack');
								}
								
								if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
								{
									return Redirect::route('vdash');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
								{
									return Redirect::route('reports');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Clients')=='Yes')
								{
									return Redirect::route('clients');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Invoice')=='Yes')
								{
									return Redirect::route('invoice');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Expense')=='Yes')
								{
									return Redirect::route('expenses');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Setting')=='Yes')
								{
									return Redirect::route('settings');
								}
							}
							elseif($data->category=='client')
							{
								if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
								{
									return Redirect::route('cdash');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
								{
									return Redirect::route('cleads');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
								{
									return Redirect::route('calltrack');
								}
								
								if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
								{
									return Redirect::route('cdash');
								}
								
								if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
								{
									return Redirect::route('reports');
								}
							}
							
							return Redirect::route('Error401');
						}
					}
					else
					{
						$errors=array('loginfailed'=>'Suspend User');
						return Redirect()->back()->withInput($request->all())->withErrors($errors);
					}  
				}
				else
				{
					$errors=array('loginfailed'=>'Invalid Credentials');
					return Redirect()->back()->withInput($request->all())->withErrors($errors);
				} 
			}
		}
		
		//Logout
		public function logout(Request $request)
		{
			Session::forget('user');
			Session::flush();
			$errors=array('alert_success'=>'You have successfully logged out!');
			return redirect('/')->withErrors($errors);
		}
		
		//Password Recovery
		public function forgotPassword(Request $request)
		{
			if(!isset($request->action) && !isset($request->token) && !isset($request->username))
			{
				return view('panel.Auth.forgot_password');
			}
			else if($request->action=="password_reset")
			{
				return view('panel.Auth.reset_password',['email'=>$request->username,'token'=>$request->token]);
			}
			else
			{
				$errors=array('loginfailed'=>'Oops! Password could not be recovered. Pls try later.');
				return Redirect()->route('forgot-password')->withInput($request->all())->withErrors($errors); 
			}
		}
		
		//Forgot Password
		public function forgotPasswordSubmit(Request $request)
		{
			$this->validate($request,['email'=>'required|email']);
			$data=DB::table('users')->where('email',$request->email)->first();
			if(count($data)==1)
			{
				$token=password_hash(date('U'),PASSWORD_BCRYPT);
				
				$data1 = array(
				'name' => $data->name,
				'token' => $token,
				'to' => $data->email
				);
				
				Mail::send('emails.forgot_password', $data1, function ($message) use ($data) {
					$message->from('noreply@pownder.com', 'Pownder');
					$message->to($data->email)->subject('Pownder™ Password Recovery');
				});
				
				DB::table('users')->where('email',$request->email)->update(['password_reset'=>$token,"updated_at"=>$this->getcurrenttime()]);
				$errors=array('alert_success'=>'Check your email for Reset link');
				return Redirect()->back()->withErrors($errors);
				
			}
			else
			{
				$errors=array('loginfailed'=>'Email doesn\'t exist.');
				return Redirect()->back()->withInput($request->all())->withErrors($errors);
			}
		}
		
		//Password Reset
		public function resetPasswordSubmit(Request $request)
		{
			$this->validate($request,['password'=>'required|confirmed']);
			$data= DB::table('users')->where([['email',$request->email]])->first();
			if(count($data)==1)
			{
				//echo $request->token.'<br/>'.$data->password_reset;exit();
				if($request->token==$data->password_reset)
				{            
					DB::table('users')->where('email',$request->email)->update(['password'=>password_hash($request->password,PASSWORD_BCRYPT),'password_reset'=>'',"updated_at"=>$this->getcurrenttime()]);
					$errors=['alert_success'=>'Your password resetted successfully.Login Above.'];
					return redirect()->route('/')->withErrors($errors); 
				}
				$errors=['loginfailed'=>'Invalid Link.Try again'];
				return redirect()->back()->withInput($request->all())->withErrors($errors);  
			}
			else
			{
				$errors=array('loginfailed'=>'Username doesn\'t exist.');
				return redirect()->back()->withInput($request->all())->withErrors($errors);
			}
		}
		
		//User First Time Login Password Change 
		public function getResetUserPassword(Request $request)
		{
			return view('panel.Auth.password_reset',['email'=>$request->username, 'token'=>$request->token]);
		}
		
		//User First Time Login Password Change
		public function postResetUserPassword(Request $request)
		{
			$this->validate($request,['password'=>'required|confirmed']);
			
			$user = DB::table('users')->where('email',$request->email)->first();
			
			if($request->token==$user->password_reset)
			{            
				DB::table('users')->where('email',$request->email)->update(['password'=>password_hash($request->password,PASSWORD_BCRYPT), 'password_reset'=>'', 'is_confirm' => 0 ,"updated_at"=>date('Y-m-d H:i:s')]);
				
				if($user->is_deleted==0 && $user->status==1)
				{
					$user->name = $user->name;
					$user->user_id = $user->id;
					$user->email = $user->email;
					$user->category = $user->category;  
					Session::put('user', $user);
                    $table_r='';
                    if(session('user')->category=="admin")
                    {
						$table_r = "admins";
						$id_r = session('user')->admin_id;
					}
                    else if(session('user')->category == "vendor")
                    {
						$table_r = "vendors";
						$id_r = session('user')->vendor_id;
					} 
                    if($table_r)
                    {                        
						$data_r = DB::table($table_r)->where('id',$id_r)->select('mobile','city','state','zip','url')->first();
						$user_r = session('user');
						$user_r->mobile = $data_r->mobile;
						$user_r->city = $data_r->city;
						$user_r->state = $data_r->state;
						$user_r->zip = $data_r->zip;
						$user_r->url = $data_r->url;
						Session::put('user', $user_r);
					}
					Session::put('user_id', $user->id);
					Session::put('user_name', $user->name);
					Session::put('user_category', $user->category);
					Session::put('admin_id',$user->admin_id);
					Session::put('vendor_id',$user->vendor_id);
					Session::put('client_id',$user->client_id);
					Session::put('manager_id',$user->manager_id);
					
					$manager=DB::table('managers')->where('id', $user->manager_id)->first();
					Session::put('photo',$manager->picture);
					
					$data = DB::table('users')->where('id', $user->created_user_id)->first();
					
					if($data->category=='admin')
					{
						if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
						{
							return Redirect::route('dashboard');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Campaigns')=='Yes')
						{
							return Redirect::route('campaigns');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
						{
							return Redirect::route('leads');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
						{
							return Redirect::route('calltrack');
						}
						
						if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
						{
							return Redirect::route('dashboard');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
						{
							return Redirect::route('reports');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Clients')=='Yes')
						{
							return Redirect::route('clients');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Vendors')=='Yes')
						{
							return Redirect::route('vendors');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Invoice')=='Yes')
						{
							return Redirect::route('invoice');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Expense')=='Yes')
						{
							return Redirect::route('expenses');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Setting')=='Yes')
						{
							return Redirect::route('settings');
						}
					}
					elseif($data->category=='vendor')
					{
						if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
						{
							return Redirect::route('vdash');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Campaigns')=='Yes')
						{
							return Redirect::route('campaigns');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
						{
							return Redirect::route('leads');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
						{
							return Redirect::route('calltrack');
						}
						
						if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
						{
							return Redirect::route('vdash');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
						{
							return Redirect::route('reports');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Clients')=='Yes')
						{
							return Redirect::route('clients');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Invoice')=='Yes')
						{
							return Redirect::route('invoice');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Expense')=='Yes')
						{
							return Redirect::route('expenses');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Setting')=='Yes')
						{
							return Redirect::route('settings');
						}
					}
					elseif($data->category=='client')
					{
						if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
						{
							return Redirect::route('cdash');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
						{
							return Redirect::route('cleads');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
						{
							return Redirect::route('calltrack');
						}
						
						if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
						{
							return Redirect::route('cdash');
						}
						
						if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
						{
							return Redirect::route('reports');
						}
					}
					
					return Redirect::route('Error401');
				}
			}
			else
			{
				$errors=array('loginfailed'=>'Suspend User');
				return Redirect('/')->withInput($request->all())->withErrors($errors);
			}	
		}
		
		public function dashboard()
		{
			$ShowLead = '';
			$ShowClient = '';
			$WritePermission = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				$ShowClient = ManagerHelper::ManagerClientMenuAction();
				$WritePermission = ManagerHelper::ManagerWritePermission('Dashboard Read Write');
				
				$Manager = DB::table('managers')
				->leftJoin('users', 'managers.admin_id', '=', 'users.admin_id')
				->select('users.id')
				->where('managers.id', Session::get('manager_id'))
				->first();
				$FacebookUserId = $Manager->id;
			}
			else
			{
				$FacebookUserId = Session::get('user_id');
			}
			
			$date_start = date('Y-m-d');
			$date_stop = date('Y-m-d');
			
			$campaign_s_date = 	date('Y-m').'-01';
			$campaign_e_date = date('Y-m-d');
			
			$AdminColumns = array();
			$Columns = DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Dashboard Client Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$AdminColumns[] = $Column->column_value;
			}
			
			$Reach = 0;
			$Likes = 0;
			$Spend = 0;
			$Click = 0;
			
			$GraphClick = array();
			$GraphCPA = array();
			$GraphCTR = array();
			$GraphFreq = array();
			$GraphLead = array();
			$GraphLGR = array();
			$GraphSpent = array();
			
			$client_id = Session::get('client_id');
			
			$vendors = DB::table('vendors')->select('id')->where('is_deleted', 0)->count('id');			
			
			$UnassignLeads = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where('client_id', 0)
			->count('id');
			
			$Leads = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->where('lead_type', 'Pownder™ Lead')
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$CityLeads = DB::table('facebook_ads_lead_users')->selectRaw('count(id) as Leads, city, latitude, longitude')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->where('city', '!=', '')
			->where('latitude', '!=', '')
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->groupBy('latitude')
			->groupBy('longitude')
			->get();
			
			$TotalLead = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->count('id');
			
			$TodayLead = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$CampaignArray = array();
			$client_campaigns = DB::table('facebook_ads_lead_users')->select('campaign_id')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->whereDate('created_time', '>=', $campaign_s_date)
			->whereDate('created_time', '<=', $campaign_e_date)
			->where('campaign_id', '!=', 0)
			->groupBy('campaign_id')
			->get();
			foreach($client_campaigns as $client_campaign)
			{
				$CampaignArray[] = $client_campaign->campaign_id;
			}
			
			$clients = DB::table('clients')->select('id')
			->where(function ($query) use($ShowClient){
				if($ShowClient=='Allotted Clients')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where('is_deleted', 0)
			->count('id');
			
			if(Session::get('client_id') > 0)
			{
				$facebook_campaign_values = DB::table('facebook_campaign_values')
				->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
				->whereIn('campaign_id', array_unique($CampaignArray))
				->whereDate('campaign_date', '>=', $campaign_s_date)
				->whereDate('campaign_date', '<=', $campaign_e_date)
				->groupBy('campaign_date')->get();
				foreach($facebook_campaign_values as $facebook_campaign_value)
				{
					$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					
					$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
					$TotalSpend = DB::table('facebook_campaign_values')->select('spend')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('spend');
					$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
					$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
					if($CampaignLeads > 0)
					{
						$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
						$GraphClick[] = $PownderClick;
						$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $clientCampaignLeads;
						$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $clientCampaignLeads;
						$PownderSpent = ($TotalSpend / $CampaignLeads) * $clientCampaignLeads;
						$GraphSpent[] = $PownderSpent;
						
						if($PownderLeads == 0)
						{
							$GraphCPA[] = 0;
						}
						else
						{
							$GraphCPA[] = $PownderSpent / $PownderLeads;
						}
						if($PownderClick == 0 || $PownderLeads == 0)
						{
							$GraphLGR[] = 0;
						}
						else
						{
							$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
						}
					}
					else
					{
						$GraphClick[] = 0;
						$GraphCPA[] = 0;
						$GraphCTR[] = 0;
						$GraphFreq[] = 0;
						$GraphLGR[] = 0;
						$GraphSpent[] = 0;
					}
				}
				
				$TotalSpend = 0;
				$TotalClicks = 0;
				
				$FacebookAccounts = DB::table('facebook_campaigns')
				->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
				->select('facebook_campaigns.id', 'facebook_accounts.access_token')
				->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
				->groupBy('facebook_campaigns.id')->get();
				foreach($FacebookAccounts as $FacebookAccount)
				{
					$campaign_id = $FacebookAccount->id;
					$access_token = $FacebookAccount->access_token;
					
					//Read facebook clicks, spend
					$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
					//echo 'https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token;die;
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($curl);
					curl_close($curl);
					
					$result = json_decode($response,true);
					
					if(isset($result['data']))
					{
						$data = $result['data'];
						if(isset($data[0]['clicks']))
						{
							$TotalClicks = $TotalClicks + $data[0]['clicks'];
							$TotalSpend = $TotalSpend + $data[0]['spend'];
						}
					}
				}
				
				$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
				$clientCampaignLeads = ClientHelper::clientCampaignLeads(Session::get('client_id'), array_unique($CampaignArray), $date_start, $date_stop);
				if($CampaignLeads > 0)
				{
					$Spend = ($TotalSpend / $CampaignLeads) * $clientCampaignLeads;
					$Click = ceil(($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
				}
			}
			else
			{
				if($ShowLead=='Allotted Leads')
				{
					$facebook_campaign_values = DB::table('facebook_campaign_values')
					->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
					->whereIn('campaign_id', array_unique($CampaignArray))
					->whereDate('campaign_date', '>=', $campaign_s_date)
					->whereDate('campaign_date', '<=', $campaign_e_date)
					->groupBy('campaign_date')->get();
					
					foreach($facebook_campaign_values as $facebook_campaign_value)
					{
						$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', Session::get('manager_id'))->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
						$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', Session::get('manager_id'))->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
						
						$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
						$managerCampaignLeads = ManagerHelper::managerCampaignLeads($client_id, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
						
						$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
						$TotalSpend = DB::table('facebook_campaign_values')->select('spend')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('spend');
						$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
						$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
						if($CampaignLeads > 0)
						{
							$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $managerCampaignLeads);
							$GraphClick[] = $PownderClick;
							$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $managerCampaignLeads;
							$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $managerCampaignLeads;
							$PownderSpent = ($TotalSpend / $CampaignLeads) * $managerCampaignLeads;
							$GraphSpent[] = $PownderSpent;
							
							if($PownderLeads == 0)
							{
								$GraphCPA[] = 0;
							}
							else
							{
								$GraphCPA[] = $PownderSpent / $PownderLeads;
							}
							if($PownderClick == 0 || $PownderLeads == 0)
							{
								$GraphLGR[] = 0;
							}
							else
							{
								$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
							}
						}
						else
						{
							$GraphClick[] = 0;
							$GraphCPA[] = 0;
							$GraphCTR[] = 0;
							$GraphFreq[] = 0;
							$GraphLGR[] = 0;
							$GraphSpent[] = 0;
						}
					}
					
					$TotalSpend = 0;
					$TotalClicks = 0;
					$FacebookAccounts = DB::table('facebook_campaigns')
					->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
					->select('facebook_campaigns.id', 'facebook_accounts.access_token')
					->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
					->groupBy('facebook_campaigns.id')->get();
					foreach($FacebookAccounts as $FacebookAccount)
					{
						$campaign_id = $FacebookAccount->id;
						$access_token = $FacebookAccount->access_token;
						
						//Read facebook clicks, spend
						$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
						$response = curl_exec($curl);
						curl_close($curl);
						
						$result = json_decode($response,true);
						
						if(isset($result['data']))
						{
							$data = $result['data'];
							if(isset($data[0]['clicks']))
							{
								$TotalClicks = $TotalClicks + $data[0]['clicks'];
								$TotalSpend = $TotalSpend + $data[0]['spend'];
							}
						}
					}
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
					$managerCampaignLeads = ManagerHelper::managerCampaignLeads(Session::get('manager_id'), array_unique($CampaignArray), $date_start, $date_stop);
					if($CampaignLeads > 0)
					{
						$Spend = ($TotalSpend / $CampaignLeads) * $managerCampaignLeads;
						$Click = ceil(($TotalClicks / $CampaignLeads) * $managerCampaignLeads);
					}
				}
				else
				{
					$facebook_campaign_values = DB::table('facebook_accounts')
					->leftJoin('facebook_campaigns', 'facebook_accounts.id', '=', 'facebook_campaigns.facebook_account_id')
					->leftJoin('facebook_campaign_values', 'facebook_campaigns.id', '=', 'facebook_campaign_values.campaign_id')
					->selectRaw("facebook_campaign_values.campaign_date, sum(facebook_campaign_values.clicks) as clicks, sum(facebook_campaign_values.cpa) as cpa, sum(facebook_campaign_values.ctr) as ctr, sum(facebook_campaign_values.frequency) as frequency, sum(facebook_campaign_values.impressions) as impressions, sum(facebook_campaign_values.leads) as leads, sum(facebook_campaign_values.spend) as spend")
					->where('facebook_accounts.updated_user_id', $FacebookUserId)
					->whereDate('facebook_campaign_values.campaign_date', '>=', $campaign_s_date)
					->whereDate('facebook_campaign_values.campaign_date', '<=', $campaign_e_date)
					->groupBy('facebook_campaign_values.campaign_date')->get();
					
					foreach($facebook_campaign_values as $facebook_campaign_value)
					{
						$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
						
						$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
						
						$PownderClick = $facebook_campaign_value->clicks;
						$GraphClick[] = $PownderClick;
						$GraphCTR[] = $facebook_campaign_value->ctr;
						$GraphFreq[] = $facebook_campaign_value->frequency;
						$PownderSpent = $facebook_campaign_value->spend;
						$GraphSpent[] = $PownderSpent;
						
						if($PownderLeads == 0)
						{
							$GraphCPA[] = 0;
						}
						else
						{
							$GraphCPA[] = $PownderSpent / $PownderLeads;
						}
						if($PownderClick == 0 || $PownderLeads == 0)
						{
							$GraphLGR[] = 0;
						}
						else
						{
							$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
						}
					}
					
					$FacebookAccounts = DB::table('facebook_accounts')
					->leftJoin('facebook_ads_accounts', 'facebook_accounts.id', '=', 'facebook_ads_accounts.facebook_account_id')
					->select('facebook_accounts.id', 'facebook_accounts.access_token', 'facebook_ads_accounts.facebook_id')
					//->where('facebook_accounts.updated_user_id', $FacebookUserId)
					->where('facebook_accounts.is_deleted', 0)
					->where('facebook_ads_accounts.is_deleted', 0)
					->groupBy('facebook_ads_accounts.facebook_id')
					->get();
					foreach($FacebookAccounts as $FacebookAccount)
					{
						$facebook_account_id = $FacebookAccount->id;
						$access_token = $FacebookAccount->access_token;
						
						$ads_account = $FacebookAccount->facebook_id;
						
						//Read facebook clicks, spend
						$curl = curl_init('https://graph.facebook.com/v2.10/'.$ads_account.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
						$response = curl_exec($curl);
						curl_close($curl);
						
						$result = json_decode($response,true);
						
						if(isset($result['data']))
						{
							$data = $result['data'];
							if(isset($data[0]['clicks']))
							{
								$Click = $Click + $data[0]['clicks'];
								$Spend = $Spend + $data[0]['spend'];
							}
						}
					}
				}
			}
			
			$FacebookAccounts = DB::table('facebook_accounts')
			->leftJoin('facebook_ads_accounts', 'facebook_accounts.id', '=', 'facebook_ads_accounts.facebook_account_id')
			->select('facebook_accounts.id', 'facebook_accounts.access_token', 'facebook_ads_accounts.facebook_id')
			//->where('facebook_accounts.updated_user_id', $FacebookUserId)
			->where('facebook_accounts.is_deleted', 0)
			->where('facebook_ads_accounts.is_deleted', 0)
			->groupBy('facebook_ads_accounts.facebook_id')
			->get();
			foreach($FacebookAccounts as $FacebookAccount)
			{
				$facebook_account_id = $FacebookAccount->id;
				$access_token = $FacebookAccount->access_token;
				
				$ads_account = $FacebookAccount->facebook_id;
				
				//Read facebook reach
				$curl1 = curl_init('https://graph.facebook.com/v2.10/'.$ads_account.'/insights?&date_preset=lifetime&fields=reach&access_token='.$access_token.'');
				
				curl_setopt($curl1, CURLOPT_RETURNTRANSFER, 1);
				
				$response1 = curl_exec($curl1);
				$info1 = curl_getinfo($curl1);
				
				curl_close($curl1);
				
				$result1 = json_decode($response1,true);
				
				if(isset($result1['data']))
				{
					$data1 = $result1['data'];
					if(isset($data1[0]['reach']))
					{
						$Reach = $Reach+$data1[0]['reach'];
					}
				}
			}
			
			$FacebookAccounts = DB::table('facebook_accounts')->where('updated_user_id', $FacebookUserId)->where('is_deleted', 0)->get();
			foreach($FacebookAccounts as $FacebookAccount)
			{
				$facebook_account_id = $FacebookAccount->id;
				$access_token = $FacebookAccount->access_token;
				$facebook_pages = DB::table('facebook_pages')->select('id')->where('facebook_account_id', $facebook_account_id)->where('is_blocked', 0)->where('is_deleted', 0)->get();
				foreach($facebook_pages as $facebook_page)
				{
					$facebook_page_id = $facebook_page->id;
					$curl2 = curl_init('https://graph.facebook.com/v2.10/'.$facebook_page_id.'?fields=fan_count&access_token='.$access_token);
					curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
					$response2 = curl_exec($curl2);
					$info2 = curl_getinfo($curl2);
					curl_close($curl2);
					
					$result2 = json_decode($response2,true);
					
					if(isset($result2['fan_count']))
					{
						$Likes = $Likes + $result2['fan_count'];
					}
				}
			}
			
			return view('panel.frontend.dashboard',['Columns' => $AdminColumns, 'clients' => $clients, 'vendors' => $vendors, 'Likes' => $Likes, 'CityLeads' => $CityLeads, 'Reach' => $Reach, 'Click' => $Click, 'TotalLead' => $TotalLead, 'TodayLead' => $TodayLead, 'Spend' => $Spend, 'facebook_ads_lead_users' => $facebook_ads_lead_users, 'facebook_campaign_values' => $facebook_campaign_values, 'WritePermission' => $WritePermission, 'GraphCPA' => $GraphCPA, 'GraphCTR' => $GraphCTR, 'GraphClick' => $GraphClick, 'GraphFreq' => $GraphFreq, 'GraphLGR' => $GraphLGR, 'GraphLead' => $GraphLead, 'GraphSpent' => $GraphSpent, 'UnassignLeads' => $UnassignLeads]);
		}
		
		public function getAdminDashboardAjax(Request $request)
		{
			$ShowLead = '';
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				
				$Manager = DB::table('managers')
				->leftJoin('users', 'managers.admin_id', '=', 'users.admin_id')
				->select('users.id')
				->where('managers.id', Session::get('manager_id'))
				->first();
				$FacebookUserId = $Manager->id;
			}
			else
			{
				$FacebookUserId = Session::get('user_id');
			}
			
			$date_start = $request->date_start;
			$date_stop = $request->date_stop;
			$client_id = $request->client_id;
			
			$datediff = floor((strtotime($date_stop) - strtotime($date_start))/(60 * 60 * 24));
			
			if($datediff <= 30)
			{
				$campaign_s_date = 	$date_start;
				$campaign_e_date = 	$date_stop;
			}
			else
			{
				$campaign_s_date = 	date('Y-m').'-01';
				$campaign_e_date = date('Y-m-d');
			}
			
			$CPA = 0;
			$Spend = 0;
			$Click = 0;
			
			$GraphClick = array();
			$GraphCPA = array();
			$GraphCTR = array();
			$GraphFreq = array();
			$GraphLead = array();
			$GraphLGR = array();
			$GraphSpent = array();
			
			$Leads = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->where('lead_type', 'Pownder™ Lead')
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->count('id');
			
			$CityLeads = DB::table('facebook_ads_lead_users')->selectRaw('count(id) as Leads, city, latitude, longitude')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->where('city', '!=', '')
			->where('latitude', '!=', '')
			->whereDate('created_time', '>=', $date_start)
			->whereDate('created_time', '<=', $date_stop)
			->groupBy('latitude')
			->groupBy('longitude')
			->get();
			
			$CampaignArray = array();
			$client_campaigns = DB::table('facebook_ads_lead_users')->select('campaign_id')
			->where(function ($query) use($ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
			})
			->whereDate('created_time', '>=', $campaign_s_date)
			->whereDate('created_time', '<=', $campaign_e_date)
			->where('campaign_id', '!=', 0)
			->groupBy('campaign_id')
			->get();
			foreach($client_campaigns as $client_campaign)
			{
				$CampaignArray[] = $client_campaign->campaign_id;
			}
			
			if($client_id > 0)
			{
				$facebook_campaign_values = DB::table('facebook_campaign_values')
				->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
				->whereIn('campaign_id', array_unique($CampaignArray))
				->whereDate('campaign_date', '>=', $campaign_s_date)
				->whereDate('campaign_date', '<=', $campaign_e_date)
				->groupBy('campaign_date')->get();
				foreach($facebook_campaign_values as $facebook_campaign_value)
				{
					$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
					
					$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
					$TotalSpend = DB::table('facebook_campaign_values')->select('spend')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('spend');
					$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
					$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
					if($CampaignLeads > 0)
					{
						$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
						$GraphClick[] = $PownderClick;
						$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $clientCampaignLeads;
						$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $clientCampaignLeads;
						$PownderSpent = ($TotalSpend / $CampaignLeads) * $clientCampaignLeads;
						$GraphSpent[] = $PownderSpent;
						
						if($PownderLeads == 0)
						{
							$GraphCPA[] = 0;
						}
						else
						{
							$GraphCPA[] = $PownderSpent / $PownderLeads;
						}
						if($PownderClick == 0 || $PownderLeads == 0)
						{
							$GraphLGR[] = 0;
						}
						else
						{
							$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
						}
					}
					else
					{
						$GraphClick[] = 0;
						$GraphCPA[] = 0;
						$GraphCTR[] = 0;
						$GraphFreq[] = 0;
						$GraphLGR[] = 0;
						$GraphSpent[] = 0;
					}
				}
				
				//print_r($GraphClick);die;
				$TotalSpend = 0;
				$TotalClicks = 0;
				$FacebookAccounts = DB::table('facebook_campaigns')
				->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
				->select('facebook_campaigns.id', 'facebook_accounts.access_token')
				->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
				->groupBy('facebook_campaigns.id')->get();
				foreach($FacebookAccounts as $FacebookAccount)
				{
					$campaign_id = $FacebookAccount->id;
					$access_token = $FacebookAccount->access_token;
					
					//Read facebook clicks, spend
					$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($curl);
					curl_close($curl);
					
					$result = json_decode($response,true);
					
					if(isset($result['data']))
					{
						$data = $result['data'];
						if(isset($data[0]['clicks']))
						{
							$TotalClicks = $TotalClicks + $data[0]['clicks'];
							$TotalSpend = $TotalSpend + $data[0]['spend'];
						}
					}
				}
				
				$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
				$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $date_start, $date_stop);
				if($CampaignLeads > 0)
				{
					$Spend = ($TotalSpend / $CampaignLeads) * $clientCampaignLeads;
					$Click = ceil(($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
				}
			}
			else
			{
				if($ShowLead=='Allotted Leads')
				{
					$facebook_campaign_values = DB::table('facebook_campaign_values')
					->selectRaw("campaign_date, sum(clicks) as clicks, sum(cpa) as cpa, sum(ctr) as ctr, sum(frequency) as frequency, sum(impressions) as impressions, sum(leads) as leads, sum(spend) as spend")
					->whereIn('campaign_id', array_unique($CampaignArray))
					->whereDate('campaign_date', '>=', $campaign_s_date)
					->whereDate('campaign_date', '<=', $campaign_e_date)
					->groupBy('campaign_date')->get();
					
					foreach($facebook_campaign_values as $facebook_campaign_value)
					{
						$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', Session::get('manager_id'))->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
						$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', Session::get('manager_id'))->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
						
						$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
						$managerCampaignLeads = ManagerHelper::managerCampaignLeads($client_id, array_unique($CampaignArray), $facebook_campaign_value->campaign_date, $facebook_campaign_value->campaign_date);
						
						$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('clicks');
						$TotalSpend = DB::table('facebook_campaign_values')->select('spend')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('spend');
						$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('ctr');
						$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $facebook_campaign_value->campaign_date)->whereDate('campaign_date', '<=', $facebook_campaign_value->campaign_date)->sum('frequency');
						if($CampaignLeads > 0)
						{
							$PownderClick = ceil(($TotalClicks / $CampaignLeads) * $managerCampaignLeads);
							$GraphClick[] = $PownderClick;
							$GraphCTR[] = ($TotalCtr / $CampaignLeads) * $managerCampaignLeads;
							$GraphFreq[] = ($TotalFreq / $CampaignLeads) * $managerCampaignLeads;
							$PownderSpent = ($TotalSpend / $CampaignLeads) * $managerCampaignLeads;
							$GraphSpent[] = $PownderSpent;
							
							if($PownderLeads == 0)
							{
								$GraphCPA[] = 0;
							}
							else
							{
								$GraphCPA[] = $PownderSpent / $PownderLeads;
							}
							if($PownderClick == 0 || $PownderLeads == 0)
							{
								$GraphLGR[] = 0;
							}
							else
							{
								$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
							}
						}
						else
						{
							$GraphClick[] = 0;
							$GraphCPA[] = 0;
							$GraphCTR[] = 0;
							$GraphFreq[] = 0;
							$GraphLGR[] = 0;
							$GraphSpent[] = 0;
						}
					}
					
					$TotalSpend = 0;
					$TotalClicks = 0;
					$FacebookAccounts = DB::table('facebook_campaigns')
					->join('facebook_accounts', 'facebook_campaigns.facebook_account_id', '=', 'facebook_accounts.id')
					->select('facebook_campaigns.id', 'facebook_accounts.access_token')
					->whereIn('facebook_campaigns.id', array_unique($CampaignArray))
					->groupBy('facebook_campaigns.id')->get();
					foreach($FacebookAccounts as $FacebookAccount)
					{
						$campaign_id = $FacebookAccount->id;
						$access_token = $FacebookAccount->access_token;
						
						//Read facebook clicks, spend
						$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
						$response = curl_exec($curl);
						curl_close($curl);
						
						$result = json_decode($response,true);
						
						if(isset($result['data']))
						{
							$data = $result['data'];
							if(isset($data[0]['clicks']))
							{
								$TotalClicks = $TotalClicks + $data[0]['clicks'];
								$TotalSpend = $TotalSpend + $data[0]['spend'];
							}
						}
					}
					
					$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $date_start, $date_stop);
					$managerCampaignLeads = ManagerHelper::managerCampaignLeads(Session::get('manager_id'), array_unique($CampaignArray), $date_start, $date_stop);
					if($CampaignLeads > 0)
					{
						$Spend = ($TotalSpend / $CampaignLeads) * $managerCampaignLeads;
						$Click = ceil(($TotalClicks / $CampaignLeads) * $managerCampaignLeads);
					}
				}
				else
				{
					
					$facebook_campaign_values = DB::table('facebook_accounts')
					->leftJoin('facebook_campaigns', 'facebook_accounts.id', '=', 'facebook_campaigns.facebook_account_id')
					->leftJoin('facebook_campaign_values', 'facebook_campaigns.id', '=', 'facebook_campaign_values.campaign_id')
					->selectRaw("facebook_campaign_values.campaign_date, sum(facebook_campaign_values.clicks) as clicks, sum(facebook_campaign_values.cpa) as cpa, sum(facebook_campaign_values.ctr) as ctr, sum(facebook_campaign_values.frequency) as frequency, sum(facebook_campaign_values.impressions) as impressions, sum(facebook_campaign_values.leads) as leads, sum(facebook_campaign_values.spend) as spend")
					->where('facebook_accounts.updated_user_id', $FacebookUserId)
					->whereDate('facebook_campaign_values.campaign_date', '>=', $campaign_s_date)
					->whereDate('facebook_campaign_values.campaign_date', '<=', $campaign_e_date)
					->groupBy('facebook_campaign_values.campaign_date')->get();
					
					foreach($facebook_campaign_values as $facebook_campaign_value)
					{
						$GraphLead[] = DB::table('facebook_ads_lead_users')->select('id')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
						
						$PownderLeads = DB::table('facebook_ads_lead_users')->select('id')->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $facebook_campaign_value->campaign_date)->count('id');
						
						$PownderClick = $facebook_campaign_value->clicks;
						$GraphClick[] = $PownderClick;
						$GraphCTR[] = $facebook_campaign_value->ctr;
						$GraphFreq[] = $facebook_campaign_value->frequency;
						$PownderSpent = $facebook_campaign_value->spend;
						$GraphSpent[] = $PownderSpent;
						
						if($PownderLeads == 0)
						{
							$GraphCPA[] = 0;
						}
						else
						{
							$GraphCPA[] = $PownderSpent / $PownderLeads;
						}
						if($PownderClick == 0 || $PownderLeads == 0)
						{
							$GraphLGR[] = 0;
						}
						else
						{
							$GraphLGR[] = ($PownderLeads / $PownderClick) * 100;
						}
					}
					
					$FacebookAccounts = DB::table('facebook_accounts')
					->leftJoin('facebook_ads_accounts', 'facebook_accounts.id', '=', 'facebook_ads_accounts.facebook_account_id')
					->select('facebook_accounts.id', 'facebook_accounts.access_token', 'facebook_ads_accounts.facebook_id')
					//->where('facebook_accounts.updated_user_id', $FacebookUserId)
					->where('facebook_accounts.is_deleted', 0)
					->where('facebook_ads_accounts.is_deleted', 0)
					->groupBy('facebook_ads_accounts.facebook_id')
					->get();
					foreach($FacebookAccounts as $FacebookAccount)
					{
						$facebook_account_id = $FacebookAccount->id;
						$access_token = $FacebookAccount->access_token;
						
						$ads_account = $FacebookAccount->facebook_id;
						
						//Read facebook clicks, spend
						$curl = curl_init('https://graph.facebook.com/v2.10/'.$ads_account.'/insights?fields=clicks,spend&time_range[since]='.$date_start.'&time_range[until]='.$date_stop.'&access_token='.$access_token);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
						$response = curl_exec($curl);
						curl_close($curl);
						
						$result = json_decode($response,true);
						
						if(isset($result['data']))
						{
							$data = $result['data'];
							if(isset($data[0]['clicks']))
							{
								$Click = $Click + $data[0]['clicks'];
								$Spend = $Spend + $data[0]['spend'];
							}
						}
					}
				}	
			}
			
			$Array = array();
			
			$Array['Spend'] = '$'.Helper::NumberFormat(number_format($Spend,2));
			
			$Array['Leads'] = Helper::NumberFormat(number_format($Leads,2));
			
			$Array['facebook_campaign_values'] = $facebook_campaign_values;
			$Array['GraphCPA'] = $GraphCPA;
			$Array['GraphCTR'] = $GraphCTR;
			$Array['GraphClick'] = $GraphClick;
			$Array['GraphFreq'] = $GraphFreq;
			$Array['GraphLGR'] = $GraphLGR;
			$Array['GraphLead'] = $GraphLead;
			$Array['GraphSpent'] = $GraphSpent;
			$Array['CityLeads'] = $CityLeads;
			
			if($Click == 0 || $facebook_ads_lead_users == 0){
				$Array['LGR'] = '0%';
				}else{
				$Array['LGR'] = Helper::NumberFormat(number_format(($facebook_ads_lead_users/$Click)*100,2)).'%';
			}
			
			if($Spend == 0 || $facebook_ads_lead_users == 0){
				$Array['CPA'] = '$0';
				}else{
				$Array['CPA'] = '$'.Helper::NumberFormat(number_format($Spend/$facebook_ads_lead_users,2));
			}
			
			return response()->json($Array);
		}
		
		public function addUser()
		{
			return view('panel.frontend.addnew_user');
		}
		
		public function editUser()
		{
			return view('panel.frontend.edit_user');
		}
		
		public function userProfile()
		{
			return view('panel.frontend.user_profile');
		}
		
		public function lockscreen()
		{
			return view('panel.frontend.lockscreen');
		}
		
		public function fbMessenger()
		{
			return view('panel.frontend.fbMessenger');
		}
		
		public function delete(Request $request)
		{
			DB::table($request->table)->where('id',$request->id)->delete();
		}
		
		public function blankPage()
		{
			return view('panel.frontend.blank');  
		}
		
		public function Calltrack()
		{
			$WritePermission = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$WritePermission = ManagerHelper::ManagerWritePermission('Call Track Read Write');
			}
			
			return view('panel.frontend.calltrack',['WritePermission' => $WritePermission]);  
		}
		
		public function manageNumbers()
		{
			return view('panel.frontend.manageNumbers');  
		}	
		
		public function editAdmin()
		{
			$admin = DB::table('admins')->where('id',Session::get('admin_id'))->first();
			
			return view('panel.frontend.edit_admin',['admin'=>$admin]);
		}
		
		public function editAdminProfile(Request $request)
		{
			$rule = [
			'name' => 'required',
			'address' => 'required',
			'appointment_email' => 'required|email',
			'invoice_email' => 'required|email',
			'new_lead_email' => 'required|email',
			'close_deal_email' => 'required|email',
			'other_email' => 'required|email',
			'zip' => 'required|integer|zip_code_valid|digits:5',
			'mobile' => 'required|phone_format|unique:admins,mobile,'.Session::get('admin_id'),
			'email' => 'required|email|unique:users,email,'.Session::get('user_id'),
			'photo' => 'image|dimensions:width=180,height=180',
			];
			
			$this->validate($request,$rule);
			
			if($request->hasFile('photo')) 
			{
				$image=$request->file('photo');
				$photo=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/admins/',$photo);
			}
			else
			{
				$admin_result=DB::table('admins')->select('photo')->where('id', Session::get('admin_id'))->first();
				$photo=$admin_result->photo;
			}
			
			$Address = Helper::get_zip_info($request->zip);
			$city = $Address['city'];
			$state = $Address['state'];
			
			DB::table('admins')->where('id', Session::get('admin_id'))
			->update([
			'name' => $request->name,
			'email' => $request->email,
			'appointment_option' => $request->appointment_option,
			'appointment_email' => $request->appointment_email,
			'invoice_option' => $request->invoice_option,
			'invoice_email' => $request->invoice_email,
			'new_lead_option' => $request->new_lead_option,
			'new_lead_email' => $request->new_lead_email,
			'close_deal_option' => $request->close_deal_option,
			'close_deal_email' => $request->close_deal_email,
			'other_option' => $request->other_option,
			'other_email' => $request->other_email,
			'receive_notification' => $request->receive_notification,
			'address' => $request->address,
			'city' => $city,
			'state' => $state,
			'zip' => $request->zip,
			'mobile' => $request->mobile,
			'photo' => $photo,
			'updated_user_id' => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			$user = DB::table('users')->where('id', Session::get('user_id'))->first();
			if($request->email == $user->email)
			{
				DB::table('users')->where('id', Session::get('user_id'))
				->update(['name' => $request->name, 'address' => $request->address, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			else
			{
				do 
				{
					$user_password = Helper::GenerateRandomPassword(8, true, true, true, true);
				}
				while($user_password == "FALSE");
				
				$password = password_hash($user_password, PASSWORD_BCRYPT); 
				
				DB::table('users')->where('id', Session::get('user_id'))
				->update(['name' => $request->name, 'email' => $request->email, 'password' => $password, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
				
				$data = array('name' => $request->name, 'email' => $request->email, 'user_password' => $user_password);
				
				Mail::send('emails.login_details', $data, function ($message) use ($request) {
					$message->from('noreply@pownder.com', 'Pownder');
					$message->to($request->email)->subject('Pownder™ Credentials');
				});
				
				$data = array('name' => $user->name, 'dateTime' => date('l').", ".date('d F Y')." at ".date('H:i'), 'ip' => $request->ip());
				
				Mail::send('emails.admin_login_details', $data, function ($message) use ($user) {
					$message->from('noreply@pownder.com', 'Pownder');
					$message->to($user->email)->subject('Pownder™ Email Change');
				});
				Session::put('EmailChange', 'Yes');
			}
			
			Session::put('user_name', $request->name);
			Session::put('photo', $photo);
			
			return Redirect::back()->with('alert_success', 'Profile updated successfully'); 
		}
		
		public function changePassword(Request $request)
		{
			$rule = ['password' => 'required|min:8|confirmed', 'password_confirmation'=>'required'];
			
			$this->validate($request, $rule);
			
			DB::table('users')->where('id', Session::get('user_id'))
			->update(['password' => password_hash($request->password,PASSWORD_BCRYPT), 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
			
			return Redirect::back()->with('alert_success','Password changed successfully'); 
		}
		
		public function getAdminDashboardClient(Request $request)
		{
			$order = isset($request->order) ? $request->order : 'DESC';
			$sortString = isset($request->sort) ? $request->sort : '';
			$search = isset($request->search) ? $request->search : '';
			$offset = isset($request->offset) ? $request->offset : '0';
			$client_id = isset($request->client_id) ? $request->client_id : '0';
			$limit = isset($request->limit) ? $request->limit : '9999999999';
			
			if($limit == 'All' )
			{
				$limit = 9999999999;
			}
			
			$start_date = date('Y-m-d');
			$end_date = date('Y-m-d');
			$dateRange = isset($request->dateRange)?$request->dateRange:'';
			if($dateRange != '')
			{
				$explodeDate = explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			$date1=date_create($start_date);
			$date2=date_create($end_date);
			$diff=date_diff($date1,$date2);
			$days = $diff->format("%a")+1;
			
			switch($sortString) 
			{
				case 'Clients':
				$sort = 'clients.name';
				break;
				case 'Active?':
				$sort = 'clients.status';
				break;
				case 'Leads':
				$sort = 'LDs';
				break;
				default:
				$sort = 'LDs';
			}
			
			$data=array();
			$rows=array();
			
			$ShowClient = '';
			
			$columns = ['clients.name', 'clients.status'];
			
			if(Session::get('user_category')=='user')
			{
				$ShowClient = ManagerHelper::ManagerClientMenuAction();
			}
			
			$TotalClients = DB::table('clients')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->selectRAW('clients.*, count(DISTINCT facebook_ads_lead_users.id) as LDs')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('clients.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($ShowClient){
				if($ShowClient=='Allotted Clients')
				{
					$query->where('clients.manager_id', Session::get('manager_id'));
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('clients.id', $client_id);
				}
			})
			->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
			->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date)
			->where('clients.status', 1)
			->where('clients.is_deleted', 0)
			->groupBy('clients.id')
			->get();
			
			$Clients = DB::table('clients')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->selectRAW('clients.*, count(DISTINCT facebook_ads_lead_users.id) as LDs')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('clients.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($ShowClient){
				if($ShowClient=='Allotted Clients')
				{
					$query->where('clients.manager_id', Session::get('manager_id'));
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('clients.id', $client_id);
				}
			})
			->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
			->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date)
			->where('clients.status', 1)
			->where('clients.is_deleted', 0)
			->groupBy('clients.id')
			->orderBy($sort, $order)
			->orderBy('clients.name', 'ASC')
			->skip($offset)
			->take($limit)
			->get();
			
			$sno=$offset+1;
			foreach($Clients as $Client)
			{
				if($Client->id == '' || $Client->id == null )
				{
					$data['total'] = 0;
					$data['rows'] = array();
					
					return \Response::json($data);
				}
				
				$user = array();
				$user['#'] = $sno++;
				if($Client->status==1){ 
					$user['Active?'] = 'Active';
					}else{
					$user['Active?'] = 'Inactive';
				}
				$user['Clients'] = '<a href="'.route("campaigns").'?client_id='.$Client->id.'">'.$Client->name.'</a>';
				$user['Ad Set Name'] = '';
				$user['Leads'] = '<a href="'.route("leads").'?client_id='.$Client->id.'&date_range='.$start_date.' to '.$end_date.'">'.Helper::NumberFormat(number_format(ClientHelper::ClientLDs($Client->id, $start_date, $end_date),2)).'</a>';
				$user['Calls'] = 0;
				$user['Clicks'] = Helper::NumberFormat(number_format(AdminHelper::ClientClicks($Client->id, $start_date, $end_date),2));
				
				if(AdminHelper::ClientClicks($Client->id, $start_date, $end_date) == 0)
				{
					$user['LGR %'] = '0%';
				}
				else
				{
					$user['LGR %'] = Helper::NumberFormat(number_format((ClientHelper::ClientFacebookLeads($Client->id, $start_date, $end_date)/AdminHelper::ClientClicks($Client->id, $start_date, $end_date))*100, 2)).'%';
				}
				
				if(ClientHelper::ClientFacebookLeads($Client->id, $start_date, $end_date) == 0)
				{
					$user['CPA $'] = '$0';
				}
				else
				{
					$user['CPA $'] = '$'.Helper::NumberFormat(number_format((AdminHelper::ClientSpents($Client->id, $start_date, $end_date)/ClientHelper::ClientFacebookLeads($Client->id, $start_date, $end_date)), 2));
				}
				
				$user['Budget'] = '$0';
				$user['Spent $'] = '$'.Helper::NumberFormat(number_format(AdminHelper::ClientSpents($Client->id, $start_date, $end_date), 2));
				$user['CTR %'] = Helper::NumberFormat(number_format(AdminHelper::ClientCTR($Client->id, $start_date, $end_date)/$days, 2)).'%';
				$user['CPM $'] = '$0';
				$user['Freq'] = Helper::NumberFormat(number_format(AdminHelper::ClientFreq($Client->id, $start_date, $end_date)/$days, 2));
				$user['Reach'] = Helper::NumberFormat(number_format(AdminHelper::ClientReach($Client->id, $start_date, $end_date), 2));
				
				$rows[] = $user;
			}		
			
			$data['total'] = count($TotalClients);
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
	}													