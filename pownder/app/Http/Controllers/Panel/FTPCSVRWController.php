<?php
	namespace App\Http\Controllers\Panel;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	use View;
	use DB;
	use Session;
    use Redirect;
    use App\Http\Controllers\Panel\InvoiceController;
    
    
	class FTPCSVRWController extends Controller
	{
	    var $now='';        
        
        public function __construct()
        {
            $this->setTimeZone('America/Los_Angeles');
            $this->now=$this->getCurrentDateTime();
		}
        public function setTimeZone($tz)
        {
			return  date_default_timezone_set($tz);
		}
        public function getCurrentDateTime()
        {
            date_default_timezone_set('America/Los_Angeles');
            return date('Y-m-d H:i:s');
		}
        //Money Format
        public function money_format1($number)
        { 
          
          $number=str_replace(",","",$number);
          settype($number,"float"); 
          setlocale(LC_MONETARY, 'en_US'); 
          $f_number=money_format('%!.2i',abs( $number ));     
          $f_number=($number<0) ? '-$'.$f_number : '$'.$f_number;
          return $f_number;
        }
        
        //Show Closed Deals Page
        public function showClosedDeals(Request $request)
        {          
			return view('panel.frontend.closed_deals');   
		}       
        
        //Get department wise data
        
        public function getDepartmentWiseData(Request $request)
        {
			$table1="ftp_csv_data_1";
			$table2="ftp_csv_data_2";
			$table3="ftp_csv_data_3";
			
			$limit=($request->has('limit')) ?   $request->limit : 10;
			$offset=($request->has('offset')) ? $request->offset : 0;
			$search=($request->has('search')) ? $request->search : '';
			$range= ($request->has('range')) ? $request->range  : date('Y-m-d',strtotime('Yesterday')).' to '.date('Y-m-d',strtotime('Yesterday'));
			$client_id=($request->has('client_id')) ? $request->client_id : '';
			$deal_number=($request->has('deal_number')) ? $request->deal_number : '';
			$stock_number=($request->has('stock_number')) ? $request->stock_number : '';
			$from=$to=$range_condition=$client_id_condition=$deal_number_condition=$stock_number_condition='';
			$deal_status_condition=' and B.col149 NOT IN("S")';
			
			if($deal_number && $stock_number)
			{
				$range='';
			}
			
			$order=  'desc';
			$sort= 'department';
			
			if($range)
			{
				$range=explode('to',urldecode($range));
				$from=trim($range[0]);
				$to=trim($range[1]);
				$range_condition=' and STR_TO_DATE(B.col153,"%m/%d/%Y")>="'.$from.'" and STR_TO_DATE(B.col153,"%m/%d/%Y")<="'.$to.'"';
			}
			
			if($deal_number)
			{
				$deal_number_condition=" and A.col5='".$deal_number."'";
			}
			if($stock_number)
			{
				$stock_number_condition=" and A.col70='".$stock_number."'";
			}
			
			if($client_id)
			{
				//$client_data=DB::table('clients')->where('id',$client_id)->select('client_no','roi_id')->first();
				//            if($client_data->client_no && $client_data->roi_id)
				//            {
				//            $client_id_condition=' and A.col2 IN("'.$client_data->client_no.'","'.$client_data->roi_id.'")';
				//            }
				//            else if($client_data->client_no)
				//            {
				//            $client_id_condition=' and A.col2 ="'.$client_data->client_no.'" and A.from_ftp=1';
				//            }
				//            else if($client_data->roi_id)
				//            {
				//            $client_id_condition=' and A.col2 ="'.$client_data->roi_id.'" and A.from_ftp=2';
				//            }
				$client_id_condition=' and A.client_id ="'.$client_id.'" ';
			}
			else
			{
				if(session('user')->category=="client")
				{
					//$client_data=DB::table('clients')->where('id',session('user')->client_id)->select('client_no','roi_id')->first();
					//            if($client_data->client_no && $client_data->roi_id)
					//            {
					//            $client_id_condition=' and A.col2 IN("'.$client_data->client_no.'","'.$client_data->roi_id.'")';
					//            }
					//            else if($client_data->client_no)
					//            {
					//            $client_id_condition=' and A.col2 ="'.$client_data->client_no.'" and A.from_ftp=1';
					//            }
					//            else if($client_data->roi_id)
					//            {
					//            $client_id_condition=' and A.col2 ="'.$client_data->roi_id.'" and A.from_ftp=2';
					//            }
					$client_id_condition=' and A.client_id ="'.session('user')->client_id.'"';
				}
				else if(session('user')->category=="vendor" || session('user')->category=="admin")
				{
					$request1=new Request();
					$request1->type="client";
					$clients_list=InvoiceController::getClientsInvoice($request1); 
					$clients_list=json_decode($clients_list,true);
					$clients_list=array_column($clients_list,'id');
					
					//$client_data=DB::table('clients')->whereIn('id',$clients_list)->select('client_no','roi_id')->get();
					//            $client_data=json_decode($client_data,true);
					//            $client_no=array_column($client_data,'client_no');
					//            $roi_id=array_column($client_data,'roi_id');
					//            $client_no='"'.implode('","',$client_no).'"';
					//            $roi_id='"'.implode('","',$roi_id).'"';
					
					//if($client_no && $roi_id)
					//            {
					//            $client_id_condition=' and A.col2 IN('.$client_no.','.$roi_id.')';
					//            }
					//            else if($client_no)
					//            {
					//            $client_id_condition=' and A.col2 IN ('.$client_no.') and A.from_ftp=1';
					//            }
					//            else if($roi_id)
					//            {
					//            $client_id_condition=' and A.col2 IN('.$roi_id.') and A.from_ftp=2';
					//            } 
					
					$client_id_condition=' and A.client_id IN('.implode(',',$clients_list).')';  
					
				}
				else if(session('user')->category=="user")
				{
					$manager_detail=DB::table('managers')->where('id',session('user')->manager_id)->select('category',DB::raw('(case when(category="admin") then admin_id when(category="client") then client_id when(category="vendor") then vendor_id END ) As owner_id'))->first();
					
					if($manager_detail->category=="client")
					{   
						//$client_data=DB::table('clients')->where('id',$manager_detail->owner_id)->select('client_no','roi_id')->first();
						//              
						//                if($client_data->client_no && $client_data->roi_id)
						//                {
						//                $client_id_condition=' and A.col2 IN("'.$client_data->client_no.'","'.$client_data->roi_id.'")';
						//                }
						//                else if($client_data->client_no)
						//                {
						//                $client_id_condition=' and A.col2 ="'.$client_data->client_no.'" and A.from_ftp=1';
						//                }
						//                else if($client_data->roi_id)
						//                {
						//                $client_id_condition=' and A.col2 ="'.$client_data->roi_id.'" and A.from_ftp=2';
						//                }
						$client_id_condition=' and A.client_id ="'.$manager_detail->owner_id.'" ';                
					}
					else if($manager_detail->category=="vendor" || $manager_detail->category=="admin")
					{
						$request1=new Request();
						$request1->type="client";
						$request1->login_user_type=$manager_detail->category;
						$request1->login_id=$manager_detail->owner_id;
						
						$clients_list=InvoiceController::getClientsInvoice($request1); 
						$clients_list=json_decode($clients_list,true);
						$clients_list=array_column($clients_list,'id');
						
						//$client_data=DB::table('clients')->whereIn('id',$clients_list)->select('client_no','roi_id')->get();
						//            $client_data=json_decode($client_data,true);
						//            $client_no=array_column($client_data,'client_no');$roi_id=array_column($client_data,'roi_id');
						//            $client_no='"'.implode('","',$client_no).'"';   $roi_id='"'.implode('","',$roi_id).'"';
						//           
						//            if($client_no && $roi_id)
						//            {
						//            $client_id_condition=' and A.col2 IN('.$client_no.','.$roi_id.')';
						//            }
						//            else if($client_no)
						//            {
						//            $client_id_condition=' and A.col2 IN ('.$client_no.') and A.from_ftp=1';
						//            }
						//            else if($roi_id)
						//            {
						//            $client_id_condition=' and A.col2 IN('.$roi_id.') and A.from_ftp=2';
						//            }  
						$client_id_condition=' and A.client_id IN('.$clients_list.') ';  
					}          
				}  
				
			}
			
			
			$department_wise_data=DB::table($table1." As A")
			->join($table2." As B","A.id",'=','B.record_id')
			->join($table3." As C","A.id",'=','C.record_id')
			->leftjoin('departments As D', 'D.id','=','A.department')
			->select(DB::raw('CAST( sum(replace(A.col135,",","")) AS DECIMAL(65,2) ) as front_gross , CAST( sum(replace(B.col174,",","")) AS DECIMAL(65,2) ) as hold_back,CAST( sum(replace(B.col145,",","")) AS DECIMAL(65,2) ) as back_gross,CAST( sum(replace(B.col146,",","")) AS DECIMAL(65,2) ) as total_profit , CAST( sum(replace(col407,",","")) AS DECIMAL(65,2) ) as hold_check,
			count(A.id) as total_sold,
			sum( case when(LOWER(A.col69)=LOWER("NEW") OR LOWER(A.col69)=LOWER("N") ) then 1 else 0 end ) as total_new,
			sum( case when(LOWER(A.col69)=LOWER("USED") OR LOWER(A.col69)=LOWER("U") ) then 1 else 0 end ) as total_used,
			D.name As department         
			'))
			->whereRaw('LOWER(A.col69) IN("new","used","n","u") '.$client_id_condition.' '.$range_condition.' '.$deal_status_condition.' '.$deal_number_condition.' '.$stock_number_condition.' ')  
			->groupBy('A.department')
			->orderBy($sort,$order)         
			->limit($limit)
			->offset($offset)
			->get();
			
			
			$totals=DB::table($table1." As A")
			->join($table2." As B","A.id",'=','B.record_id')
			->join($table3." As C","A.id",'=','C.record_id')
			->select(DB::raw('CAST(sum(replace(A.col135,",",""))  AS DECIMAL(65,2) ) as front_gross , CAST( sum(replace(B.col174,",","")) AS DECIMAL(65,2) ) as hold_back,CAST( sum(replace(B.col145,",","")) AS DECIMAL(65,2) ) as back_gross, CAST(sum(replace(B.col146,",","")) AS DECIMAL(65,2) ) as total_profit , CAST( sum(replace(col407,",","")) AS DECIMAL(65,2) ) as hold_check,
			count(A.id) as total_sold,
			sum( case when(LOWER(A.col69)=LOWER("NEW") OR LOWER(A.col69)=LOWER("N") ) then 1 else 0 end ) as total_new,
			sum( case when(LOWER(A.col69)=LOWER("USED") OR LOWER(A.col69)=LOWER("U") ) then 1 else 0 end ) as total_used,
			"Totals" AS department         
			'))
			->whereRaw('LOWER(A.col69) IN("new","used","n","u") '.$client_id_condition.' '.$range_condition.' '.$deal_status_condition.' '.$deal_number_condition.' '.$stock_number_condition.' ')
			->first(); 
			
			if(count($department_wise_data)>0)
			{
				$department_wise_data=json_decode($department_wise_data,true);
				$department_wise_data=array_map(function($object)
				{
					if(!$object['department']) 
					{
						$object['department']="Other"; 
					}
					$object['front_gross']=$this->money_format1($object['front_gross']);
					$object['hold_back']=$this->money_format1($object['hold_back']);
					$object['back_gross']=$this->money_format1($object['back_gross']);
					$object['total_profit']=$this->money_format1($object['total_profit']);
					$object['hold_check']=$this->money_format1($object['hold_check']);
					
					return $object; 
				},$department_wise_data);
				
				$totals->front_gross=$this->money_format1($totals->front_gross);
				$totals->hold_back=$this->money_format1($totals->hold_back);
				$totals->back_gross=$this->money_format1($totals->back_gross);
				$totals->total_profit=$this->money_format1($totals->total_profit);
				$totals->hold_check=$this->money_format1($totals->hold_check);
				
				array_push($department_wise_data,$totals);
			}
			
			return \Response::json($department_wise_data);
		}
        
        //Get All Deals
        
        public function getClosedDeals(Request $request,$type)
        {
			$table1="ftp_csv_data_1";
			$table2="ftp_csv_data_2";
			$table3="ftp_csv_data_3";
			
			$limit=($request->has('limit')) ?   $request->limit : 10;
			$offset=($request->has('offset')) ? $request->offset : 0;
			$search=($request->has('search')) ? $request->search : '';
			$range= ($request->has('range')) ? $request->range  : date('Y-m-d',strtotime('Yesterday')).' to '.date('Y-m-d',strtotime('Yesterday'));
			$client_id=($request->has('client_id')) ? $request->client_id : '';
			$deal_number=($request->has('deal_number')) ? $request->deal_number : '';
			$stock_number=($request->has('stock_number')) ? $request->stock_number : '';
			$from=$to=$range_condition=$client_id_condition=$deal_number_condition=$stock_number_condition='';
			$deal_status_condition=' and B.col149 NOT IN("S")';         
			
			if($deal_number)
			{
				$deal_number_condition=" and A.col5='".$deal_number."'";
			}
			if($stock_number)
			{
				$stock_number_condition=" and A.col70='".$stock_number."'";
			}
			
			if($deal_number && $stock_number)
			{
				$range='';
			}
			
			if($range)
			{
				$range=explode('to',urldecode($range));
				$from=trim($range[0]);
				$to=trim($range[1]);
				$range_condition=' and STR_TO_DATE(B.col153,"%m/%d/%Y")>="'.$from.'" and STR_TO_DATE(B.col153,"%m/%d/%Y")<="'.$to.'"';
			}
			
			if($client_id)
			{
				//$client_data=DB::table('clients')->where('id',$client_id)->select('client_no','roi_id')->first();
				//            if($client_data->client_no && $client_data->roi_id)
				//            {
				//            $client_id_condition=' and A.col2 IN("'.$client_data->client_no.'","'.$client_data->roi_id.'")';
				//            }
				//            else if($client_data->client_no)
				//            {
				//            $client_id_condition=' and A.col2 ="'.$client_data->client_no.'" and A.from_ftp=1';
				//            }
				//            else if($client_data->roi_id)
				//            {
				//            $client_id_condition=' and A.col2 ="'.$client_data->roi_id.'" and A.from_ftp=2';
				//            }
				$client_id_condition=' and A.client_id ="'.$client_id.'" ';
			}
			else
			{
				if(session('user')->category=="client")
				{
					//$client_data=DB::table('clients')->where('id',session('user')->client_id)->select('client_no','roi_id')->first();
					//            if($client_data->client_no && $client_data->roi_id)
					//            {
					//            $client_id_condition=' and A.col2 IN("'.$client_data->client_no.'","'.$client_data->roi_id.'")';
					//            }
					//            else if($client_data->client_no)
					//            {
					//            $client_id_condition=' and A.col2 ="'.$client_data->client_no.'" and A.from_ftp=1';
					//            }
					//            else if($client_data->roi_id)
					//            {
					//            $client_id_condition=' and A.col2 ="'.$client_data->roi_id.'" and A.from_ftp=2';
					//            }
					$client_id_condition=' and A.client_id ="'.session('user')->client_id.'"';
				}
				else if(session('user')->category=="vendor" || session('user')->category=="admin")
				{
					$request1=new Request();
					$request1->type="client";
					$clients_list=InvoiceController::getClientsInvoice($request1); 
					$clients_list=json_decode($clients_list,true);
					$clients_list=array_column($clients_list,'id');
					
					//$client_data=DB::table('clients')->whereIn('id',$clients_list)->select('client_no','roi_id')->get();
					//            $client_data=json_decode($client_data,true);
					//            $client_no=array_column($client_data,'client_no');
					//            $roi_id=array_column($client_data,'roi_id');
					//            $client_no='"'.implode('","',$client_no).'"';
					//            $roi_id='"'.implode('","',$roi_id).'"';
					
					//if($client_no && $roi_id)
					//            {
					//            $client_id_condition=' and A.col2 IN('.$client_no.','.$roi_id.')';
					//            }
					//            else if($client_no)
					//            {
					//            $client_id_condition=' and A.col2 IN ('.$client_no.') and A.from_ftp=1';
					//            }
					//            else if($roi_id)
					//            {
					//            $client_id_condition=' and A.col2 IN('.$roi_id.') and A.from_ftp=2';
					//            } 
					
					$client_id_condition=' and A.client_id IN('.implode(',',$clients_list).')';  
					
				}
				else if(session('user')->category=="user")
				{
					$manager_detail=DB::table('managers')->where('id',session('user')->manager_id)->select('category',DB::raw('(case when(category="admin") then admin_id when(category="client") then client_id when(category="vendor") then vendor_id END ) As owner_id'))->first();
					
					if($manager_detail->category=="client")
					{   
						//$client_data=DB::table('clients')->where('id',$manager_detail->owner_id)->select('client_no','roi_id')->first();
						//              
						//                if($client_data->client_no && $client_data->roi_id)
						//                {
						//                $client_id_condition=' and A.col2 IN("'.$client_data->client_no.'","'.$client_data->roi_id.'")';
						//                }
						//                else if($client_data->client_no)
						//                {
						//                $client_id_condition=' and A.col2 ="'.$client_data->client_no.'" and A.from_ftp=1';
						//                }
						//                else if($client_data->roi_id)
						//                {
						//                $client_id_condition=' and A.col2 ="'.$client_data->roi_id.'" and A.from_ftp=2';
						//                }
						$client_id_condition=' and A.client_id ="'.$manager_detail->owner_id.'" ';                
					}
					else if($manager_detail->category=="vendor" || $manager_detail->category=="admin")
					{
						$request1=new Request();
						$request1->type="client";
						$request1->login_user_type=$manager_detail->category;
						$request1->login_id=$manager_detail->owner_id;
						
						$clients_list=InvoiceController::getClientsInvoice($request1); 
						$clients_list=json_decode($clients_list,true);
						$clients_list=array_column($clients_list,'id');
						
						//$client_data=DB::table('clients')->whereIn('id',$clients_list)->select('client_no','roi_id')->get();
						//            $client_data=json_decode($client_data,true);
						//            $client_no=array_column($client_data,'client_no');$roi_id=array_column($client_data,'roi_id');
						//            $client_no='"'.implode('","',$client_no).'"';   $roi_id='"'.implode('","',$roi_id).'"';
						//           
						//            if($client_no && $roi_id)
						//            {
						//            $client_id_condition=' and A.col2 IN('.$client_no.','.$roi_id.')';
						//            }
						//            else if($client_no)
						//            {
						//            $client_id_condition=' and A.col2 IN ('.$client_no.') and A.from_ftp=1';
						//            }
						//            else if($roi_id)
						//            {
						//            $client_id_condition=' and A.col2 IN('.$roi_id.') and A.from_ftp=2';
						//            }  
						$client_id_condition=' and A.client_id IN('.$clients_list.') ';  
					}          
				}  
				
			}
			
			$output=[];
			
			$totals=DB::table($table1." As A")
			->join($table2." As B","A.id",'=','B.record_id')
			->join($table3." As C","A.id",'=','C.record_id')
			->select(DB::raw('count(A.id) as total_sold'))
			->whereRaw('LOWER(A.col69) IN("new","used","n","u") '.$client_id_condition.' '.$range_condition.' '.$deal_status_condition.' '.$deal_number_condition.' '.$stock_number_condition)
			->get();
			
			$totals=json_decode($totals,true);
			
			$overall_total_sold=array_sum(array_column($totals,'total_sold'));
			
			if($type=="group_by_city")
			{
				$order= ($request->has('order')) ? $request->order : 'desc';
				$sort= ($request->has('sort')) ? $request->sort : 'total_sold';
				
				
				$count=DB::table($table1." As A")
				->join($table2." As B","A.id",'=','B.record_id')
				->join($table3." As C","A.id",'=','C.record_id')
				->select('A.col15')
				->whereRaw('LOWER(A.col69) IN("new","used","n","u") '.$client_id_condition.' '.$range_condition.' '.$deal_status_condition.' '.$deal_number_condition.' '.$stock_number_condition)         
				->groupBy('A.col15')         
				->havingRaw('count(A.id) like "%'.$search.'%" OR A.col15 like "%'.$search.'%" OR CAST( sum(replace(B.col146,",","")) AS DECIMAL(65,2) ) like "%'.$search.'%" OR cast(((count(A.id)/'.$overall_total_sold.')*100) as DECIMAL(65,2)) like "%'.$search.'%"  ') 
				->get(); 
				
				$count=count($count);
				
				$data=DB::table($table1." As A")
				->join($table2." As B","A.id",'=','B.record_id')
				->join($table3." As C","A.id",'=','C.record_id')
				->select(DB::raw('A.col15 as city, count(A.id) as total_sold, "0.00" as avg_gross, SUM(CAST(REPLACE(B.col146,",","") AS DECIMAL(65,2))) as total_profit, cast(((count(A.id)/'.$overall_total_sold.')*100) as DECIMAL(65,2)) as percentage'))
				->whereRaw('LOWER(A.col69) IN("new","used","n","u") '.$client_id_condition.' '.$range_condition.' '.$deal_status_condition.' '.$deal_number_condition.' '.$stock_number_condition)    
				->groupBy('city')
				->havingRaw('count(A.id) like "%'.$search.'%" OR A.col15 like "%'.$search.'%" OR CAST( sum(replace(B.col146,",","")) AS DECIMAL(65,2) ) like "%'.$search.'%" OR cast(((count(A.id)/'.$overall_total_sold.')*100) as DECIMAL(65,2)) like "%'.$search.'%"  ') 
				->orderBy($sort,$order)         
				->limit($limit)
				->offset($offset)
				->get(); 
				
				$data=json_decode($data,true);       
				
				
				$data=array_map(function($object)
				{               
					if($object['total_sold'])
					{
						$object['avg_gross']=$this->money_format1($object['total_profit']/$object['total_sold']);
					}
					$object['total_profit']=$this->money_format1($object['total_profit']);
					$object['percentage']=$object['percentage'].'%'; 
					if(!$object['city']) 
					{
						$object['city']="Other"; 
					}                   
					return $object; 
				},$data);
				
				$output['total']=$count;
				$output['rows']=$data;
				
				return \Response::json($output);  
				
				
			}
			else if($type=="group_by_city_total")
			{
				
				$data=DB::table($table1." As A")
				->join($table2." As B","A.id",'=','B.record_id')
				->join($table3." As C","A.id",'=','C.record_id')
				->select(DB::raw('A.col15 as city,count(A.id) as total_sold,CAST( ( sum(replace(B.col146,",","")) / count(A.id) ) AS DECIMAL(65,2) ) as avg_gross, SUM(CAST(REPLACE(B.col146,",","") AS DECIMAL(65,2))) as total_profit, ((count(A.id)/'.$overall_total_sold.')*100) as percentage'))
				->whereRaw('LOWER(A.col69) IN("new","used","n","u") '.$client_id_condition.' '.$range_condition.' '.$deal_status_condition.' '.$deal_number_condition.' '.$stock_number_condition)  
				->havingRaw('count(A.id) like "%'.$search.'%" OR A.col15 like "%'.$search.'%" OR CAST( sum(replace(B.col146,",","")) AS DECIMAL(65,2) ) like "%'.$search.'%" OR cast(((count(A.id)/'.$overall_total_sold.')*100) as DECIMAL(65)) like "%'.$search.'%"  ') 
				->groupBy('city')
				->get(); 
				
				if(count($data)==0)
				{
					$object=new \stdClass();
					$object->total_sold=0;
					$object->total_profit=0;
					$object->avg_gross=0;
					$object->percentage=0;
					$data=[$object];
				}
				else
				{
					$data=json_decode($data,true); 
					
					$total_sold=array_sum(array_column($data,'total_sold')) ; 
					$avg_gross=$this->money_format1(array_sum(array_column($data,'avg_gross'))) ; 
					$total_profit=$this->money_format1(array_sum(array_column($data,'total_profit'))) ;
					$percentage=number_format(array_sum(array_column($data,'percentage'))).'%' ;  
					$data=["city"=>"","total_sold"=>$total_sold,"avg_gross"=>$avg_gross,"total_profit"=>$total_profit,"percentage"=>$percentage] ;
					$data=[$data];
				} 
				return \Response::json($data);  
			}
			else
			{
				$order= ($request->has('order')) ? $request->order : 'desc';
				$sort= ($request->has('sort')) ? $request->sort : 'contract_date';
				$filter_new=($request->has('filter_new')) ? $request->filter_new : 'default' ;
				$filter_used=($request->has('filter_used')) ? $request->filter_used : 'default' ; 
				$filter_condition=""; 
				$types=[];
				
				if($type=="NEW")
				{
					$filter_dcategory=$filter_new;
					$types=["new","n"]; 
					
				}
				else
				{
					$filter_dcategory=$filter_used;
					$types=["used","u"];   
				}
				
				switch($filter_dcategory)
				{
					case "FR": $filter_condition=" and B.col158 IN('F','R')";break;
					case "L": $filter_condition=" and B.col158 IN('L')";break;
					case "W": $filter_condition=" and B.col158 IN('W')";break;
					case "all": break;
					default: $filter_condition=" and B.col158 NOT IN('W')";break;
				}
				
				
				$columns=['B.col153','A.col5','A.col7','A.col15','A.col135','B.col174','B.col145','B.col146','C.col407','A.col106','D.name','A.source'];  
				
				$count=DB::table($table1." As A")
				->join($table2." As B","A.id",'=','B.record_id')
				->join($table3." As C","A.id",'=','C.record_id')
				->leftjoin('departments As D', 'D.id','=','A.department')    
				->where(function($query) use($types,$search,$columns,$range_condition,$client_id_condition,$filter_condition,$deal_status_condition,$deal_number_condition,$stock_number_condition){
					if($search)
					{
						$query->where('A.id','0'); 
						foreach($columns as $column)
						{
							$query->orWhereRaw('LOWER(A.col69) IN("'.implode('","',$types).'") '.$client_id_condition.' '.$range_condition.' '.$filter_condition.' '.$deal_status_condition.' '.$deal_number_condition.' '.$stock_number_condition.'  and '.$column.' like "%'.$search.'%" '); 
						}   
					}
					else
					{
						$query->whereRaw('LOWER(A.col69) IN("'.implode('","',$types).'") '.$client_id_condition.' '.$range_condition.' '.$filter_condition.' '.$deal_status_condition.' '.$deal_number_condition.' '.$stock_number_condition.' '); 
					}
				}) 
				->count('A.id'); 
				
				$data=DB::table($table1." As A")
				->join($table2." As B","A.id",'=','B.record_id')
				->join($table3." As C","A.id",'=','C.record_id')
				->leftjoin('departments As D', 'D.id','=','A.department')
				->where(function($query) use($types,$search,$columns,$range_condition,$client_id_condition,$filter_condition,$deal_status_condition,$deal_number_condition,$stock_number_condition){
					if($search)
					{
						$query->where('A.id','0'); 
						foreach($columns as $column)
						{
							$query->orWhereRaw('LOWER(A.col69) IN("'.implode('","',$types).'") '.$client_id_condition.' '.$range_condition.' '.$filter_condition.'  '.$deal_status_condition.' '.$deal_number_condition.' '.$stock_number_condition.'  and '.$column.' like "%'.$search.'%" '); 
						}   
					}
					else
					{
						$query->whereRaw('LOWER(A.col69) IN("'.implode('","',$types).'") '.$client_id_condition.' '.$range_condition.'  '.$filter_condition.' '.$deal_status_condition.' '.$deal_number_condition.' '.$stock_number_condition.' '); 
					}
				})          
				->select(DB::raw('A.id,STR_TO_DATE(B.col153,"%m/%d/%Y") as contract_date,CAST(A.col5 as unsigned) as deal_number'),'A.col7 as full_name','A.col15 as city',DB::raw('CAST(REPLACE(A.col135,",","") AS DECIMAL(65,2)) as front_gross, CAST(REPLACE(B.col174,",","") AS DECIMAL(65,2)) as hold_back, CAST(REPLACE(B.col145,",","") AS DECIMAL(65,2)) as back_gross, CAST(REPLACE(B.col146,",","") AS DECIMAL(65,2)) as total_profit, CAST(REPLACE(C.col407,",","") AS DECIMAL(65,2)) as hold_check'),'A.col106 as salesman1' , 'D.name as department' , 'A.source' ,'A.from_ftp')
				->orderBy($sort,$order) 
				->limit($limit)
				->offset($offset)
				->get();
				
				$data=json_decode($data,true);
				$data=array_map(function($object)
				{               
					if(!$object['department']) 
					{
						$object['department']="Other"; 
					}
					$object['contract_date']=date('m/d/Y',strtotime($object['contract_date']));
					$object['front_gross']=$this->money_format1($object['front_gross']);
					$object['hold_back']=$this->money_format1($object['hold_back']);
					$object['back_gross']=$this->money_format1($object['back_gross']);
					$object['total_profit']=$this->money_format1($object['total_profit']);
					$object['hold_check']=$this->money_format1($object['hold_check']);
					$object['action']='<a target="_blank" href="'.route('view-closed-deal',['id'=>$object['id']]).'" title="View Deal Details"><i class="fa fa-car"></i></a>';
					return $object; 
				},$data); 
				
				$output['total']=$count;
				$output['rows']=$data;
				
				return \Response::json($output);                                           
			} 
		}
        
        //Show View Closed Deal Page
        public function viewClosedDeal($id)
        {
            $table1="ftp_csv_data_1";
            $table2="ftp_csv_data_2";
            $table3="ftp_csv_data_3";
            
            $data=DB::table($table1." As A")
            ->join($table2." As B","A.id",'=','B.record_id')
            ->join($table3." As C","A.id",'=','C.record_id')
            ->leftjoin('departments As D', 'D.id','=','A.department')
            ->leftjoin('clients As E', function($join){
				$join->on([['E.client_no','=','A.col2'],['A.from_ftp','=',DB::raw("1")]])->orOn([['E.roi_id','=','A.col2'],['A.from_ftp','=',DB::raw("2")]]);   
			})
            ->leftjoin('group_categories As F','F.id','=','E.group_category_id')
            ->where('A.id',$id)         
            ->select('A.*','B.col163','B.col157','B.col141','B.col148','B.col171','B.col145','B.col185','B.col146','B.col165','B.col162','B.col150','B.col153','B.col152','B.col158','B.col164','B.col181','B.col160','B.col211','E.name As client_name','F.name As group_category','B.col167')
            ->first();
			
			return view('panel.frontend.view_closed_deal',['data'=>$data]);   
		}
	}           