<?php
	namespace App\Http\Controllers\Panel;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	use View;
	use DB;
	use Session;
    use Redirect;
    
	class BotController extends Controller
	{
	    var $now='';  
        const DIALOGFLOW_DEVELOPER_TOKEN='6659c1d3b5d64c259a2c473eee850af4';
        const DIALOGFLOW_CLIENT_TOKEN='0fe08036c1814096b5ac7dfff0a02078';
        const DIALOGFLOW_INTENT_ID='8dcf5c1c-8b6b-407a-bc8d-758ddc767b19';
		
        
        public function __construct()
        {
            $this->setTimeZone('America/Los_Angeles');
            $this->now=$this->getCurrentDateTime();
		}
        public function setTimeZone($tz)
        {
			return  date_default_timezone_set($tz);
		}
        public function getCurrentDateTime()
        {
            date_default_timezone_set('America/Los_Angeles');
            return date('Y-m-d H:i:s');
		}
        //Money Format
        public function money_format1($number)
        { 
			
			$number=str_replace(",","",$number);
			settype($number,"float"); 
			setlocale(LC_MONETARY, 'en_US'); 
			$f_number=money_format('%!.2i',abs( $number ));     
			$f_number=($number<0) ? '-$'.$f_number : '$'.$f_number;
			return $f_number;
		}
        
        //Show Manage Bot Page
        public function ManageBot()
        {
            $fb_pages=DB::table('facebook_pages As A')->leftJoin('bot_activation As B','A.id','=','B.fb_page_id')->where([['A.is_deleted',0],['A.is_blocked',0],["A.updated_user_id",session('user')->id]])->select('A.*','B.status As BotStatus','B.fb_page_token As FbPageToken','B.verify_token As VerifyToken','B.dialogflow_callback_url As DialogflowCallbackUrl','B.dialogflow_client_token As DialogflowClientToken','B.dialogflow_developer_token As DialogflowDeveloperToken','B.id As BotId')->get();
            return view('panel.frontend.manage_bot',['fb_pages'=>$fb_pages]);   
		}
        //Get List Of Intents
        public function GetIntents($developer_token)
        {
            $curl=curl_init('https://api.dialogflow.com/v1/intents?v=20170712');
            curl_setopt($curl,CURLOPT_HTTPHEADER, ['Authorization: Bearer '.$developer_token,'Content-Type: application/json']);
            curl_setopt($curl,CURLOPT_HEADER,false);
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
            $response=curl_exec($curl);
            $http_code=curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            return json_decode($response);
		}
        //Create An Internt
        public function MakeIntent($developer_token)
        {
            $data='{                  
			"name": "PownderBotIntent_1",
			"auto": false,
			"contexts": [],
			"responses": [
			{
			"resetContexts": false,
			"action": "welcome",
			"affectedContexts": [],
			"parameters": [
			{
			"id": "433743d9-22ff-41a7-a634-e399bfac6425",
			"required": false,
			"dataType": "@sys.phone-number",
			"name": "phone_number",
			"value": "$phone_number",
			"prompts": [
			"Enter mobile number"
			],
			"isList": false
			},
			{
			"id": "38bf9406-c7de-4002-8e02-e7427e634fa4",
			"required": false,
			"dataType": "@sys.email",
			"name": "email",
			"value": "$email",
			"prompts": [
			"Enter email id"
			],
			"isList": false
			},
			{
			"id": "1abe32fd-e940-4449-939c-13eea7a94fc0",
			"dataType": "@sys.number",
			"name": "digits",
			"value": "$digits",
			"isList": false
			},
			{
			"id": "1babb240-080e-4890-abea-0ed2b0630a9e",
			"dataType": "@sys.any",
			"name": "any",
			"value": "$any",
			"isList": true
			},
			{
			"id": "5fdb897f-0d4b-42c9-83ac-0986db52b698",
			"dataType": "@sys.given-name",
			"name": "given-name",
			"value": "$given-name",
			"isList": false
			},
			{
			"id": "8fa5f360-684d-473b-8887-5a0f668b3c31",
			"dataType": "@sys.address",
			"name": "address",
			"value": "$address",
			"isList": false
			},
			{
			"id": "2eddc838-75cb-450a-aec0-b884a27e772c",
			"dataType": "@sys.zip-code",
			"name": "zip-code",
			"value": "$zip-code",
			"isList": false
			}
			],
			"messages": [
			{
			"type": 0,
			"speech": "Hello $given-name :o"
			}
			],
			"defaultResponsePlatforms": {},
			"speech": []
			}
			],
			"priority": 1000000,
			"cortanaCommand": {
			"navigateOrService": "NAVIGATE",
			"target": ""
			},
			"webhookUsed": true,
			"webhookForSlotFilling": true,
			"lastUpdate": 1509622434954,
			"fallbackIntent": false,
			"events": [
			{
			"name": "FACEBOOK_WELCOME"
			}
			],
			"userSays": [
			{
			"id": "192ec385-afd1-4ea9-9b6e-af8f293f5318",
			"data": [
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "zipcode",
			"alias": "zip-code",
			"meta": "@sys.zip-code",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"isAuto": false
			},
			{
			"id": "a9b7e27d-1d15-42cc-8680-ef6076cb1606",
			"data": [
			{
			"text": "zipcode",
			"alias": "zip-code",
			"meta": "@sys.zip-code",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"isAuto": false
			},
			{
			"id": "a5e61658-138f-4f43-901d-ab717698a31e",
			"data": [
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "zipcode",
			"alias": "zip-code",
			"meta": "@sys.zip-code",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"isAuto": false
			},
			{
			"id": "665458ae-9396-4859-9d99-20f21384e920",
			"data": [
			{
			"text": "address",
			"alias": "address",
			"meta": "@sys.address",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"isAuto": false
			},
			{
			"id": "7c059ab3-80fa-42fb-8cce-2d8e802cae59",
			"data": [
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "address",
			"alias": "address",
			"meta": "@sys.address",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"isAuto": false
			},
			{
			"id": "3223da32-6f5d-49c3-8fed-e39c93efef68",
			"data": [
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "address",
			"alias": "address",
			"meta": "@sys.address",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"isAuto": false
			},
			{
			"id": "fecb4389-2a98-4e16-bcdf-19fd94bcf03d",
			"data": [
			{
			"text": "zipcode",
			"alias": "zip-code",
			"meta": "@sys.zip-code",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "4f20e9b1-0adc-4033-8d89-d6bbc1c87256",
			"data": [
			{
			"text": "address",
			"alias": "address",
			"meta": "@sys.address",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "0b990927-0158-49e8-b5a3-9b97bc113e03",
			"data": [
			{
			"text": "my name is "
			},
			{
			"text": "GivenName",
			"alias": "given-name",
			"meta": "@sys.given-name",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "3a75b746-ad3d-419a-bc1e-bd65be9af69d",
			"data": [
			{
			"text": "digits",
			"alias": "digits",
			"meta": "@sys.number",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "7d9d2156-f5eb-440f-b3e8-7383c7dd3d58",
			"data": [
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "digits",
			"alias": "digits",
			"meta": "@sys.number",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "a3c82961-0d03-46e4-9c61-81952bd8e0c0",
			"data": [
			{
			"text": "digits",
			"alias": "digits",
			"meta": "@sys.number",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "db7560ea-f13a-47da-b078-a4c8d5d1dc28",
			"data": [
			{
			"text": "phone_number",
			"alias": "phone_number",
			"meta": "@sys.phone-number",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "b7889de7-a6ea-45a4-bfa1-b3798975b4d8",
			"data": [
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "phone_number",
			"alias": "phone_number",
			"meta": "@sys.phone-number",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "978722be-6bee-41a1-9e62-9d5c2ec80876",
			"data": [
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "email",
			"alias": "email",
			"meta": "@sys.email",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "26c0996e-6180-4dff-930c-3832f317bfe9",
			"data": [
			{
			"text": "email",
			"alias": "email",
			"meta": "@sys.email",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "f9db76c4-6dba-4e82-8a93-caeac8e99941",
			"data": [
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "digits",
			"alias": "digits",
			"meta": "@sys.number",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "7fb06c1a-6e7b-4c8c-b435-9d316c7d58cb",
			"data": [
			{
			"text": "phone_number",
			"alias": "phone_number",
			"meta": "@sys.phone-number",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "4dfe5c01-ff81-459d-9d6a-05d3bd62b3a1",
			"data": [
			{
			"text": "email",
			"alias": "email",
			"meta": "@sys.email",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "1a41a3a3-8b9a-4c0e-aaad-85445e5d6f4c",
			"data": [
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "email",
			"alias": "email",
			"meta": "@sys.email",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "3941e55a-54d9-4b42-9bfe-38c4edd42c62",
			"data": [
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "phone_number",
			"alias": "phone_number",
			"meta": "@sys.phone-number",
			"userDefined": true
			},
			{
			"text": " "
			},
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 0,
			"updated": 1509622434,
			"isAuto": false
			},
			{
			"id": "2b6ef37b-b6cd-4538-b551-fa0f1845c816",
			"data": [
			{
			"text": "any",
			"alias": "any",
			"meta": "@sys.any",
			"userDefined": true
			}
			],
			"isTemplate": false,
			"count": 1,
			"updated": 1509622434,
			"isAuto": false
			}
			],
			"followUpIntents": [],
			"templates": []
			}';                            
            
            $curl=curl_init('https://api.dialogflow.com/v1/intents?v=20170712');
            curl_setopt($curl,CURLOPT_HTTPHEADER, ['Authorization: Bearer '.$developer_token,'Content-Type: application/json']);
            curl_setopt($curl,CURLOPT_HEADER,false);
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl,CURLOPT_POST,true);
            curl_setopt($curl,CURLOPT_POSTFIELDS,$data);
            $response=curl_exec($curl);
            $http_code=curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            return json_decode($response);
		}
        //Set Get Started Button
        public function SetWelcomeEvent($accessToken)
        {
			$data=array('setting_type'=>'call_to_actions',"thread_state"=>'new_thread','call_to_actions'=>[['payload'=>'FACEBOOK_WELCOME']]);
			$data=json_encode($data);
			
			$options=array(
			CURLOPT_URL=>'https://graph.facebook.com/v2.6/me/thread_settings?access_token='.$accessToken,
			CURLOPT_RETURNTRANSFER=>true,
			CURLOPT_HEADER=>false,
			CURLOPT_POST=>true,
			CURLOPT_POSTFIELDS=>$data,
			CURLOPT_HTTPHEADER=>array("Content-Type:application/json; charset=UTF-8")        
			);
			
			$curl=curl_init();
			curl_setopt_array($curl,$options);
			$response=curl_exec($curl);
			$http_code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
			curl_close($curl);
			return $http_code;
		}
        //Start Bot
        public function StartBot(Request $request)
        {
            $rule=
            [
			"fb_page_id"=>"required",
			"fb_page_token"=>"required",
            // "verify_token"=>"required",
			//"dialogflow_callback_url"=>"required|active_url",
			//"dialogflow_client_token"=>"required",
            // "dialogflow_developer_token"=>"required"
            ];
            $validations=$this->validate($request,$rule);
            
            //Check if already activated
            $page_exists=DB::table('facebook_pages')->where([['id',$request->fb_page_id],['is_deleted',0],['is_blocked',0],["updated_user_id",session('user')->id]])->count('id');
            
            if($page_exists==0)
            {
				$error_msg='Facebook Page Not Found.';
				return response()->make('{"code":401,"msg":"'.$error_msg.'"}',401,["content-type"=>"application/json"]);   
			}    
            
            //Check if already activated
            $bot_exists=DB::table('bot_activation')->where('fb_page_id',$request->fb_page_id)->count('id');
            
            if($bot_exists==0)
            {
				//Check if duplicate token
				$duplicate_token=DB::table('bot_activation')->where('fb_page_token',$request->fb_page_token)->count('id');
				
				if($duplicate_token==0)
				{                
					//Make A New Intent
					//$response=$this->MakeIntent($request->dialogflow_developer_token);
					
					//if($response->status->code==200)
					//{
					$http_code=$this->SetWelcomeEvent($request->fb_page_token);
					if($http_code==200)
					{
						DB::table('bot_activation')->insert(['fb_page_id'=>$request->fb_page_id,'fb_page_token'=>$request->fb_page_token,'created_user_id'=>session('user')->id,'created_at'=>$this->now,'created_ip'=>$request->ip(),'updated_user_id'=>session('user')->id,'updated_at'=>$this->now,'updated_ip'=>$request->ip()]);
						Session::flash('alert_success','Bot activated successfully.');
						return response()->make('{"code":200,"msg":"Bot activated successfully."}',200,["content-type"=>"application/json"]);
					}
					else
					{
						$error_msg='Invalid Page Access Token';
						return response()->make('{"code":401,"msg":"'.$error_msg.'"}',401,["content-type"=>"application/json"]);
					}
					//}
					//            else
					//            {
					//                $error_msg=$response->status->code.' : '.$response->status->errorType;
					//                if(isset($response->status->errorDetails))
					//                {
					//                    $error_msg.="<br/>".$response->status->errorDetails;
					//                }
					//                return response()->make('{"code":401,"msg":"'.$error_msg.'"}',401,["content-type"=>"application/json"]);
					//            }
					
				}            
				else
				{
					$error_msg='Invalid Page Access Token';
					return response()->make('{"code":401,"msg":"'.$error_msg.'"}',401,["content-type"=>"application/json"]);
				} 
				
			}
            else
            {
                $error_msg='Bot is already acivated on this page';
                return response()->make('{"code":401,"msg":"'.$error_msg.'"}',401,["content-type"=>"application/json"]);
			}            
            
		}
        
        
        //Stop Bot
        public function StopBot($bot_id,$new_status)
        {            
            if($new_status==0)
            {                
                $msg="Bot stopped successfully.";
			}
            else
            {                
                $msg="Bot enabled successfully.";
			}
            DB::table('bot_activation')->where([['id',$bot_id],['created_user_id',session('user')->id]])->update(['status'=>$new_status]);
            
            return Redirect::back()->with('alert_success',$msg);
            
		}
        
        //Show Unanswered Queries
        public function UnansweredQueries()
        {
            $queries=DB::table('bot_unanswered_query As A')->join('messenger_answers As B',[['A.senderId','=','B.senderId'],['A.recipientId','=','B.recipientId']])->join('facebook_pages As C','A.recipientId','=','C.id')->where('A.is_deleted',0)->select('A.text As query','A.lang As query_lang','A.created_at As query_created','A.id As query_id','B.*','C.name As page_name')->groupBy('A.id')->orderByDesc('A.id')->get();
            return view('panel.frontend.unanswered_queries',['queries'=>$queries]);   
		}
                
        //Delete Query
        public function DeleteQueries(Request $request)
        {            
           
            DB::table('bot_unanswered_query')->where([['id',$request->question_id]])->update(['is_deleted'=>1]);
            
            Session::flash('alert_success','Query deleted successfully');
          
            return response()->make('{"code":200,"msg":"Query deleted successfully"}',200,["content-type"=>'application/json']);
         
        }
        
        //Bot Report
        public function BotReport()
        {         
           
           $total_msg=DB::table('bot_chat_records')->count('id');
           $total_user_msg=DB::table('bot_chat_records')->where('msg_by','user')->count('id');
           $total_bot_msg=DB::table('bot_chat_records')->where('msg_by','bot')->count('id');
           $total_get_started_msg=DB::table('bot_chat_records')->where([['msg_by','user'],['text','Get Started'],['lang','en']])->count('id');
           $total_anwered_by_bot=DB::table('bot_chat_records')->where([['msg_by','bot'],['answer_id','>','0']])->count('id');
           $total_unanwered_queries=DB::table('bot_chat_records')->where([['msg_by','bot'],['answer_id','=','-1']])->count('id');
           $total_bot_question_asked=DB::table('bot_chat_records')->where([['msg_by','bot'],['question_id','>','0']])->count('id');           
           
           $user_says_report=DB::table('bot_chat_records As A')->where([['A.msg_by','bot'],['A.answer_id','>','0']])->select('A.matched_keyword As user_says',DB::raw('count(A.id) As no_of_time_asked, cast(((count(A.id)/'.$total_anwered_by_bot.')*100) AS DECIMAL(65,2)) As Percentage '))->groupBy('user_says')->get();//group_concat(DISTINCT A.text separator "*@*") As bot_reply
          
           $data=['total_user_msg'=>$total_user_msg,'total_bot_msg'=>$total_bot_msg,'total_msg'=>$total_msg,'total_get_started_msg'=>$total_get_started_msg,'total_anwered_by_bot'=>$total_anwered_by_bot,'total_unanwered_queries'=>$total_unanwered_queries,'total_bot_question_asked'=>$total_bot_question_asked];
           $data['user_says_report']=$user_says_report;
           
           return view('panel.frontend.bot_report',$data);  
        }
        //Filter Bot Report
        public function FilterBotReport(Request $request)
        {  
           $date_condition=$report='';
           if($request->has('date_range'))
           {
           $date=explode(' to ',$request->date_range);
           $from_date=$date[0];$to_date=$date[1];
           $date_condition=' and DATE(created_at)>="'.$from_date.'" and DATE(created_at)<="'.$to_date.'"';
           }
           $total_anwered_by_bot=DB::table('bot_chat_records')->whereRaw('msg_by="bot" and answer_id>0 '.$date_condition.'')->count('id');
            
           if($request->type=="total_chat_report")
           {
           $total_msg=DB::table('bot_chat_records')->whereRaw('1 '.$date_condition)->count('id');
           $total_user_msg=DB::table('bot_chat_records')->whereRaw('msg_by="user" '.$date_condition.'')->count('id');
           $total_bot_msg=DB::table('bot_chat_records')->whereRaw('msg_by="bot" '.$date_condition.'')->count('id');
           $total_get_started_msg=DB::table('bot_chat_records')->whereRaw('msg_by="user" and text="Get Started" and lang="en" '.$date_condition.'')->count('id');
           $total_unanwered_queries=DB::table('bot_chat_records')->whereRaw('msg_by="bot" and answer_id="-1" '.$date_condition.'')->count('id');
           $total_bot_question_asked=DB::table('bot_chat_records')->whereRaw('msg_by="bot" and question_id>0 '.$date_condition.'')->count('id');           
           
             $report='<tr>                                          
                                          <td>'.$total_msg.'</td>
                                          <td>'.$total_user_msg.'</td>
                                          <td>'.$total_bot_msg.'</td>
                                          <td>'.$total_get_started_msg.'</td>
                                          <td>'.$total_anwered_by_bot.'</td>
                                          <td>'.$total_unanwered_queries.'</td>
                                          <td>'.$total_bot_question_asked.'</td>
           </tr>';
           
           if(!$report)
                {
                $report='<tr><td class="text-center" colspan="7">No matching records found</td></tr>';
                }   
           
           }
           else if($request->type=="user_says_report")
           {
             $user_says_report=DB::table('bot_chat_records As A')->whereRaw('A.msg_by="bot" and A.answer_id>0 '.$date_condition.'')->select('A.matched_keyword As user_says',DB::raw('count(A.id) As no_of_time_asked, cast(((count(A.id)/'.$total_anwered_by_bot.')*100) AS DECIMAL(65,2)) As Percentage '))->groupBy('user_says')->get();
        
              
               foreach($user_says_report as $keyword)
               {
                                            $report.='<tr>
                                                  <td>'.$keyword->user_says.'</td>
                                                  <td>'.$keyword->no_of_time_asked.'</td>
                                                  <td>'.$keyword->Percentage.' %</td>
                                                </tr>';
                } 
                if(!$report)
                {
                $report='<tr><td class="text-center" colspan="3">No matching records found</td></tr>';
                }     
           }
           return $report;
        }                
        
	}           	