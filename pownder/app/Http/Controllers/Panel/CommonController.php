<?php
	namespace App\Http\Controllers\Panel;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	
	use DB;
	use Session;
	use ManagerHelper;
	use Helper;
	
	class CommonController extends Controller
	{
		public function showError401()
		{
			return view('panel.frontend.401');
		}
		
		public function setTableVisibleColumn(Request $request)
		{			
			$checkExists = DB::table('table_visible_columns')->where('table_name', $request->table_name)->where('column_value', $request->column_value)->where('created_user_id', Session::get('user_id'))->first();
			if(is_null($checkExists))
			{
				DB::table('table_visible_columns')->insert(['table_name' => $request->table_name, 'column_value' => $request->column_value, 'created_user_id' => Session::get('user_id'), 'created_at' => date('Y-m-d H:i:s'), 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			}
			else
			{
				if($checkExists->is_deleted == 1)
				{
					$is_deleted = 0;
				}
				else
				{
					$is_deleted = 1;
				}
				
				DB::table('table_visible_columns')->where('table_name', $request->table_name)->where('column_value', $request->column_value)->where('created_user_id', Session::get('user_id'))
				->update(['updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => $is_deleted]);			
			}
		}
		
		public function checkPackageType(Request $request)
		{			
			$Package = DB::table('packages')->where('id', $request->package_id)->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->first();
			
			if(is_null($Package))
			{
				return NULL;
			}
			else
			{
				return $Package->package_type;
			}
		}
		
		public function checkClientDiscount(Request $request)
		{			
			$Package = DB::table('packages')->where('id', $request->package_id)->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->first();
			
			if(is_null($Package))
			{
				return 'No';
			}
			else
			{
				if($Package->package_type=='Sold' || $Package->package_type=='')
				{
					return 'No';
				}
				elseif($Package->package_type=='Monthly')
				{
					if($Package->amount < $request->discount_amount)
					{
						return 'discount amount $'.$request->discount_amount.' is greater than package value $'.$Package->amount;
					}
					else
					{
						return 'No';
					}
				}
				elseif($Package->package_type=='Lead')
				{
					if($Package->per_lead_value < $request->discount_amount)
					{
						return 'discount amount $'.$request->discount_amount.' is greater than per lead value $'.$Package->per_lead_value;
					}
					else
					{
						return 'No';
					}
				}
			}
		}
		
		//-----------------------------------(Get Group Sub Category)---------------------------------------------------//
		public function getGroupSubCategory(Request $request)
		{
			$GroupSubCategories=DB::table('group_sub_categories')->select('id', 'name')->where('group_category_id', $request->group_category_id)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return response()->json($GroupSubCategories);
		}
		
		public function getClientUserList(Request $request)
		{
			$Managers = DB::table('managers')->where('client_id', $request->client_id)->where('status', 1)->where('is_deleted', 0)->orderBy('full_name', 'ASC')->get();
			
			return response()->json($Managers);
		}
		
		public function getfacebookAdsForm(Request $request)
		{
			if(count($request->facebook_page_id) > 0)
			{
				$facebookAdsForms=DB::table('facebook_ads_leads')->select('id', 'name')->whereIn('facebook_page_id', $request->facebook_page_id)->where('status', 'ACTIVE')->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
				
				return response()->json($facebookAdsForms);
			}
			else
			{
				$facebookAdsForms = array();
				
				return response()->json($facebookAdsForms);
			}
		}
		
		public function getClientManagers(Request $request)
		{
			//$managers = DB::table('managers')->select('id', 'full_name')->where('client_id', $request->client_id)->where('email', '!=', '')->where('status', '1')->where('is_deleted', 0)->orderBy('full_name', 'ASC')->get();
			
			$managers = DB::table('managers')->select('id', 'full_name')->where('client_id', $request->client_id)->where('status', '1')->where('is_deleted', 0)->orderBy('full_name', 'ASC')->get();
			
			return response()->json($managers);
		}
		
		//-----------------------(Get Get Client Campaign CRM Account)----------------------------------------//
		public function ClientCRMAccount(Request $request)
		{
			$CRMAccounts = DB::table('campaigns')->select('crm_account')->where('id', $request->campaign_id)->where('is_deleted', 0)->groupBy('crm_account')->orderBy('crm_account', 'ASC')->get();
			
			return response()->json($CRMAccounts);
		}
		
		//-----------------------------------(Get Client Campaign)---------------------------------------------------//
		public function ClientCampaign(Request $request)
		{
			$CRMAccounts=DB::table('campaigns')->select('id','custom_campaign_name as campaign_name')->where('client_id', $request->client_id)->where('is_active', 0)->where('is_deleted', 0)->groupBy('crm_account')->orderBy('crm_account', 'ASC')->get();
			
			return response()->json($CRMAccounts);
		}
		
		//-----------------------------------(Get Group Sub Category)---------------------------------------------------//
		public function getClientInfo(Request $request)
		{
			$Client=DB::table('clients')->where('id', $request->client_id)->orderBy('name', 'ASC')->get();
			
			return response()->json($Client);
		}
		
		//-----------------------------------(Remove Facebook Account)---------------------------------------------------//
		public function removeFacebookAccount(Request $request)
		{
			$facebook_account_id = $request->facebook_account_id;
			
			DB::table('facebook_accounts')->where('id', $facebook_account_id)->where('updated_user_id', Session::get('user_id'))
            ->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			
			DB::table('facebook_ads_accounts')->where('facebook_account_id', $facebook_account_id)->where('updated_user_id', Session::get('user_id'))
            ->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			
			DB::table('facebook_pages')->where('facebook_account_id', $facebook_account_id)->where('updated_user_id', Session::get('user_id'))
            ->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			
			DB::table('facebook_ads_leads')->where('facebook_account_id', $facebook_account_id)->where('updated_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
		}
		
		public function createDepartment(Request $request)
		{
			$rule = ['department_name' => 'required|unique:departments,name'];
			
			$this->validate($request,$rule);
			
			DB::table('departments')->insert(['name' => $request->department_name, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s'), 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			
			$Departments = DB::table('departments')->select('id', 'name')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return response()->json($Departments);
		}
		
		public function createJobTitle(Request $request)
		{
			$rule = ['job_title_name' => 'required|unique:job_titles,name'];
			
			$this->validate($request,$rule);
			
			DB::table('job_titles')->insert(['name' => $request->job_title_name, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s'), 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			
			$JobTitles = DB::table('job_titles')->select('id', 'name')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return response()->json($JobTitles);
		}
		
		public function createExpenseCategory(Request $request)
		{
			$rule = ['category_name' => 'required|unique:expense_categories,name'];
			
			$this->validate($request,$rule);
			
			DB::table('expense_categories')->insert(['name' => $request->category_name, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s'), 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			
			$expense_categories = DB::table('expense_categories')->select('id', 'name')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return response()->json($expense_categories);
		}
		
		public function createTeam(Request $request)
		{
			$rule = ['team_name' => 'required|unique:teams,name'];
			
			$this->validate($request,$rule);
			
			DB::table('teams')->insert(['name' => $request->team_name, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s'), 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			
			$Teams = DB::table('teams')->select('id', 'name')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return response()->json($Teams);
		}
		
		public function getJobTitleMenu(Request $request)
		{
			$Results = DB::table('job_title_menus')->select('menu')->where('job_title_id', $request->job_title_id)->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->get();
			
			return response()->json($Results);
		}
		
		public function getJobTitlePermission(Request $request)
		{
			$Results = DB::table('job_title_permissions')->select('permission')->where('job_title_id', $request->job_title_id)->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->get();
			
			return response()->json($Results);
		}
		
		public function getAdminClient(Request $request)
        {   
			$Results = DB::table('clients')->where('vendor_id', '=', 0)->where('is_deleted', 0)->where('status', 1)->get();
			
			return response()->json($Results);
		}
		
		public function GetClientCurrentMonthLeads(Request $request)
        {   
			$StartDate = date('Y-m').'-01';
			$EndDate = date('Y-m-d');
			
			$UserArray = array();
			
			$Client = DB::table('users')->select('id')->where('client_id', $request->client_id)->first();
			$UserArray[] = $Client->id;
			
			$users = DB::table('managers')->leftJoin('users', 'managers.id', '=', 'users.manager_id')->select('users.id')->where('managers.client_id', $request->client_id)->get();
			foreach($users as $user)
			{
				$UserArray[] = $user->id;
			}
			
			$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use ($UserArray, $request) {
				$query->whereIn('created_user_id', $UserArray)
				->orWhere('client_id', '=', $request->client_id);
			})
			->whereDate('created_time', '>=', $StartDate)->whereDate('created_time', '<=', $EndDate)->count('id');
			
			return $FacebookAdsLeadUsers;
		}
		
		public function GetClientTotalLeads(Request $request)
        {   
			$UserArray = array();
			
			$Client = DB::table('users')->select('id')->where('client_id', $request->client_id)->first();
			$UserArray[] = $Client->id;
			
			$users = DB::table('managers')->leftJoin('users', 'managers.id', '=', 'users.manager_id')->select('users.id')->where('managers.client_id', $request->client_id)->get();
			foreach($users as $user)
			{
				$UserArray[] = $user->id;
			}
			
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				
				$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->select('id')
				->where(function ($query) use($ShowLead, $UserArray, $request){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', Session::get('manager_id'));
					}
					else
					{
						$query->whereIn('created_user_id', $UserArray)
						->orWhere('client_id', '=', $request->client_id);
					}
				})->count('id');
			}
			else
			{
				$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->select('id')
				->where(function ($query) use ($UserArray, $request) {
					$query->whereIn('created_user_id', $UserArray)
					->orWhere('client_id', '=', $request->client_id);
				})->count('id');
			}
			
			return Helper::NumberFormat(number_format($facebook_ads_lead_users,2));
		}
		
		public function setClientHeaderFilter(Request $request)
		{
			$client_id = isset($request->client_id) ? $request->client_id : 0;
			
			Session::put('client_id', $client_id);
		}
		
		public function setDateHeaderFilter(Request $request)
		{
			$dateRange = isset($request->dateRange) ? $request->dateRange : '';
			
			Session::put('dateRange', $dateRange);
		}
	}																																				