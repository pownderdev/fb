<?php
	namespace App\Http\Controllers\Panel;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	
	use DB;
	use Session;
	use ManagerHelper;
	
	class ExpenseController extends Controller
	{
		public function getExpense(Request $request)
		{	
			if(Session::get('user_category')=='admin' || Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='admin' || ManagerHelper::ManagerCategory()=='vendor')
			{
				$vendor_id = 0;
				$UserArray = array();
				$WritePermission = 'Yes';
				$ShowClient = '';
				if(Session::get('user_category')=='user')
				{
					$UserArray[] = Session::get('user_id');
					$WritePermission = ManagerHelper::ManagerWritePermission('Expense Read Write');
					$ShowClient = ManagerHelper::ManagerClientMenuAction();
					$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
					$UserArray[] = $manager->created_user_id;
					$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
					}
					
					if(ManagerHelper::ManagerCategory()=='vendor')
					{
						$Vendor = DB::table('users')->where('id',$manager->created_user_id)->first();
						$vendor_id = $Vendor->vendor_id;
					}
				}
				else
				{
					$UserArray[] = Session::get('user_id');
					$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
					}
					
					$vendor_id = Session::get('vendor_id');
				}
				
				$TableColumn=array();
				$Columns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Expense List Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
				foreach($Columns as $Column)
				{
					$TableColumn[]=$Column->column_value;
				}
				
				if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
				{
					$expenses = DB::table('expenses')
					->leftJoin('expense_categories', 'expenses.expense_category_id', '=', 'expense_categories.id')
					->leftJoin('clients', 'expenses.client_id', '=', 'clients.id')
					->select('expenses.*', 'expense_categories.name as expense_category_name', 'clients.name as client_name')
					->where('expenses.is_deleted', 0)
					->orderBy('created_at', 'DESC')
					->get();
				}
				else
				{
					$expenses = DB::table('expenses')
					->leftJoin('expense_categories', 'expenses.expense_category_id', '=', 'expense_categories.id')
					->leftJoin('clients', 'expenses.client_id', '=', 'clients.id')
					->select('expenses.*', 'expense_categories.name as expense_category_name', 'clients.name as client_name')
					->where('expenses.is_deleted', 0)
					->whereIn('expenses.created_user_id', $UserArray)
					->orderBy('created_at', 'DESC')
					->get();
				}
				
				$expense_categories = DB::table('expense_categories')->where('is_deleted', 0)->get();
				$Vendors = DB::table('users')->where('category', 'vendor')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
				
				$Clients=DB::table('clients')->select('id', 'name', 'email')->where('status', 1)
				->where(function ($query) use($UserArray, $ShowClient, $vendor_id){
					if($ShowClient=='Allotted Clients')
					{
						$query->where('clients.manager_id', Session::get('manager_id'));
					}
					else
					{
						if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
						{
							$query->whereIn('created_user_id', $UserArray)
							->where('vendor_id','<=', 0);
						}
						else
						{
							$query->whereIn('created_user_id', $UserArray)
							->orWhere('vendor_id', $vendor_id);
						}
					}
				})
				->where('is_deleted', 0)->orderBy('name', 'ASC')->orderBy('email', 'ASC')->get();
				
				return view('panel.frontend.expenses',['expenses' => $expenses, 'expense_categories' => $expense_categories, 'Vendors' => $Vendors, 'Clients' => $Clients, 'WritePermission' => $WritePermission, 'TableColumn' => $TableColumn]);
			}
			else
			{
				return redirect('Error401');
			}
		}
		
		public function validateAddExpense(Request $request)
		{
			$rule = ['date' => 'required', 'total_amount' => 'required|numeric', 'category' => 'required|numeric', 'company_name' => 'required'];
			
			$this->validate($request,$rule);
		}
		
		public function postAddExpense(Request $request)
		{
			$date = explode("-",$request->date);
			$expense_date = $date[2].'-'.$date[0].'-'.$date[1];
			
			DB::table('expenses')->insert([
			'expense_date' => $expense_date,
			'company_name' => $request->company_name,
			'total_amount' => $request->total_amount,
			'expense_category_id' => $request->category,
			'description' => $request->description,
			'client_id' => $request->client,
			'created_user_id' => Session::get('user_id'),
			'created_user_ip' => $request->ip(),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_user_id' => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			return redirect('expenses')->with('alert_success','Expense added successfully');
		}
		
		public function getEditExpense(Request $request)
		{
			$data = array();
			$expense = DB::table('expenses')->where('id', $request->id)->first();
			if(!is_null($expense))
			{
				$data['company_name'] = $expense->company_name;
				$data['expense_date'] = date('m-d-Y',strtotime($expense->expense_date));
				$data['total_amount'] = $expense->total_amount;
				$data['expense_category_id'] = $expense->expense_category_id;
				$data['description'] = $expense->description;
				$data['client_id'] = $expense->client_id;
			}
			return response()->json($data);
		}
		
		public function postEditExpense(Request $request)
		{
			$date = explode("-",$request->date);
			$expense_date = $date[2].'-'.$date[0].'-'.$date[1];
			
			DB::table('expenses')->where('id', $request->id)
			->update([
			'expense_date' => $expense_date,
			'company_name' => $request->company_name,
			'total_amount' => $request->total_amount,
			'expense_category_id' => $request->category,
			'description' => $request->description,
			'client_id' => $request->client,
			'updated_user_id' => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			return redirect('expenses')->with('alert_success','Expenses updated successfully');
		}
		
		public function deleteExpense(Request $request)
		{
			if(Session::get('user_category')=='admin' || Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='admin' || ManagerHelper::ManagerCategory()=='vendor')
			{	
				DB::table('expenses')->where('id', $request->id)
				->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			}
			else
			{
				return redirect('Error401');
			}
		}
	}																	