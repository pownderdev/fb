<?php
	namespace App\Http\Controllers\Panel;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	use View;
	use DB;
	use Session;
	use PDF;
    use Redirect;
	class InvoiceController extends Controller
	{
		var $user='';
        var $now='';
        var $stripe_secret_key_test;
        var $stripe_secret_key_live;
        var $plaid_client_id;
        var $plaid_secret_key;
        const SENDGRID_USER='help@constacloud.in'; //Sendgrid user name to send emails
        const SENDGRID_PASS='main1234';            //Sendgrid password to send emails
        var $INVOICEEMAILID='invoice@pownder.com'; //Default- Send all Emails From this email id
        var $INVOICESUBJECT='Invoice [invoice_num]'; //Default Invoice email subject
        var $INVOICEMSG='Dear [name],<br/><br/>Please find the invoice details in attached pdf file.<br/><br/>'; //Default Invoice email msg
        var $REMINDERSUBJECT='Reminder: Payment is due'; //Default Reminder Invoice email subject
        var $REMINDERMSG='Your payment is due for invoice [invoice_num]'; //Default Invoice email msg
        var $INVOICETNC='';
        
        public function __construct()
        {
            $this->setTimeZone('America/Los_Angeles');
            $this->user=session('user');
            $this->now=$this->getCurrentDateTime();
            $this->stripe_secret_key_test='sk_test_gi4rxjXLkbqZ4zyNk2ZvS0gj';/*Test sk_test_o487qfpPDQxrZC1bUpJqC02t */ /**New sk_test_gi4rxjXLkbqZ4zyNk2ZvS0gj pk_test_asEFIgfe18JoKmkee0MfXowh*/
            $this->stripe_secret_key_live='sk_live_4ElwDqAsJ3pILhX3E6XatNAL'; // Stripe Secret Key //publishing key : pk_live_qEcIKnKuT6JES4DyxZYujDl7   
            $this->plaid_client_id='59703cc3bdc6a41992933876'; //Plaid Account client Id  //Test 58c3d055bdc6a4775dcafc59
            $this->plaid_secret_key='e6609fe2436fd008888ae5664b1729'; // Plaid secret key //Test b93854ff56d369bcc81b17d000db63
     
        	$invoice_settings=DB::table('invoice_settings')->first();
            
            if(count($invoice_settings)>0)
            {
              if($invoice_settings->invoice_email)
              {   
              $this->INVOICEEMAILID=$invoice_settings->invoice_email;
              }
              if($invoice_settings->invoice_subject)
              {   
              $this->INVOICESUBJECT=$invoice_settings->invoice_subject;
              }
              if($invoice_settings->invoice_msg)
              {   
              $this->INVOICEMSG=$invoice_settings->invoice_msg;
              }
              if($invoice_settings->invoice_tnc)
              {
                $this->INVOICETNC=$invoice_settings->invoice_tnc;
              }
              if($invoice_settings->reminder_subject)
              {
                $this->REMINDERSUBJECT=$invoice_settings->reminder_subject;
              } 
              if($invoice_settings->reminder_msg)
              {
                $this->REMINDERMSG=$invoice_settings->reminder_msg;
              }      
            }
		          
        }
        
        
        
        public function setTimeZone($tz)
        {
          return  date_default_timezone_set($tz);
        }
        public function getCurrentDateTime()
        {
            return date('Y-m-d H:i:s');
        }
        public function getInvoiceNum()
        {            
            $invoice_id=200;$prefix='unknown';
            $invoice_num=DB::table('invoice')->select(DB::raw('max(cast( SUBSTRING_INDEX( invoice_num  ,"-" ,-1) as unsigned )) as invoice_num'))->first();	
            if(count($invoice_num)>0 && $invoice_num->invoice_num)
            {
            $invoice_id=$invoice_num->invoice_num+1;
            }
            if(session('user')->category=="admin")
            {
                $prefix="PNAD";
            }
            else if(session('user')->category=="vendor")
            {
                $prefix="PNVA";
            }  
            $invoice_num=$prefix.'-'.sprintf('%03d',$invoice_id);
            
            return $invoice_num;
        }
        public function checkifBothValuesAreSame($arr1,$arr2)
        {
            if(count($arr1)!=count($arr2))
            {
                return false;
            }
            for($i=0;$i<count($arr1);$i++)
            {
                if($arr1[$i]!=$arr2[$i])
                {
                    return false;
                }
            }
            return true;
        }
        //Money Format
        public function money_format1($number)
        {
          setlocale(LC_MONETARY, 'en_US'); 
          return money_format('%!.2i',$number); 
        }
        //Money Format
        public function money_format_view($number)
        {
          
          $number=str_replace(",","",$number);
          settype($number,"float"); 
          setlocale(LC_MONETARY, 'en_US'); 
          $f_number=money_format('%!.2i',abs( $number ));     
          $f_number=($number<0) ? '-$'.$f_number : '$'.$f_number;
          return $f_number;
        }
        //Replace Tags/Keywords in String
        public function replaceKeywords($text,$values)
        {
            foreach($values as $key=>$value)
            {
            $text=str_ireplace($key,$value,$text);
            }
            return $text;
        }
        //Map Date and Year
        public function MapDateYear($month,$year)
        {            
            return("01-$month-$year");
        }
        //Round up float to 2 digit after decimal
        public function roundUpFloatNumber2D($number)
        {
                return round($number,2);
        }
         //Round up float to 0 digit after decimal
        public function roundUpFloatNumber($number)
        {
                return round($number);
        }
        //Get Items
        public function getSavedItems()
        {
            $items_array=[];
            $items=DB::table('invoice_saved_items')->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->select('item','description','id')->get();     
            if(count($items)>0)
            {
               $items_array=$items; 
            }
            return json_encode($items_array);
        }  
        //validate_comma_sepa_emails
        public function validate_comma_sepa_emails($emails)
        {
            $arr=explode(',',$emails);
            $code=200;$invalid_emails=[];
            foreach($arr as $email)
            {
                if(!filter_var($email , FILTER_VALIDATE_EMAIL))
                {
                    $code=400;
                    $invalid_emails[]=$email;
                }
            }
            if($code==400)
            {
                return '{"code":400,"emails":"'.implode(',',$invalid_emails).'"}';
            }
            return '{"code":200}';
        }              
        //Stripe APIs 
        //Card Payment APIs
        public function get_pay($key,$card_number,$exp_month,$exp_year,$cvc,$amount,$email,$credit_card_processing_fee=3.5)
        {        
            $token_request_body = array('card' => array(
            'number' => $card_number, 
            'exp_month' => $exp_month,
            'exp_year' => $exp_year,
            'cvc' => $cvc,
            'currency' => 'USD'
          ));
          /* step 1 create token by card details */
          $curl = curl_init('https://api.stripe.com/v1/tokens');
          curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($curl, CURLOPT_USERPWD, "$key:");
          curl_setopt($curl, CURLOPT_POST, true );
          curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($token_request_body));          
          $response = json_decode(curl_exec($curl), true);
          $http_code = curl_getinfo($curl,CURLINFO_HTTP_CODE);  
           
          if($http_code!=200)
          {
            return '{"code":402}';
          }
          $token=$response['id'];
          $brand=$response['card']['brand'];
          $cvc_last4=$response['card']['last4'];
          $card_type=$response['card']['funding'];//Debit card or credtit card
                      
       	 return '{"code":200,"token":"'.$token.'","card_type":"'.$card_type.'","card_brand":"'.$brand.'","cvc_last4":"'.$cvc_last4.'"}';
        }
        public function charge($key,$amount,$token,$description)
        {
        	/* step 3 create charge or pay */
        	$curl = curl_init();
        	$post = [
        		'amount' => $amount*100,/* it should be in cent eg; $10=1000 cent $1=100cent */
        		'currency' => 'usd',
        		'source'   => $token, 
        		'description'   => $description
        	];
        	curl_setopt($curl, CURLOPT_URL,"https://api.stripe.com/v1/charges");
        	curl_setopt($curl, CURLOPT_POST, 1);
        	curl_setopt($curl, CURLOPT_POSTFIELDS,http_build_query($post));
        	curl_setopt($curl, CURLOPT_USERPWD, "$key:");  /* use secret key */
        	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        	$response = json_decode(curl_exec($curl));
        	$http_code =curl_getinfo($curl,CURLINFO_HTTP_CODE);
            if($http_code!=200)
             {
                return '{"code":404}';
             } 
             $response->code=200;
             return json_encode($response);
        }
        //Bank Account Payment APIs
        public function get_exchange($client_id,$secret,$token)
	    {
		/* step1:- exchange public token */

		$token_request_body = array(
			'client_id' => $client_id,
			'secret' => $secret,
			'public_token' => $token
				);
		$url='https://sandbox.plaid.com/item/public_token/exchange';
		$curl = curl_init();
	  
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
			) );
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true ); 
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($token_request_body));
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); 
        $response=curl_exec($curl);
		$http_code = curl_getinfo($curl,CURLINFO_HTTP_CODE);
		if($http_code!=200)
        {
            return '{"code":400}';
        }
		$response = json_decode($response, true);
		$access_token=$response['access_token'];
		return '{"code":200,"access_token":"'.$access_token.'"}';
	   }
	   public function get_stripe_tok($client_id,$secret,$access_token,$account) /* get stripe bank account token */
	   {
		/* step2:- generate stripe token */
		$token_request_body = array(
		'client_id' => $client_id,
		'secret' => $secret,
		'access_token' => $access_token,
		'account_id' => $account,
		);
		$url='https://sandbox.plaid.com/processor/stripe/bank_account_token/create';
		$curl = curl_init();
	  
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
			) );
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true ); 
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($token_request_body));
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); 
        $response=curl_exec($curl);
		$http_code = curl_getinfo($curl,CURLINFO_HTTP_CODE);
        if($http_code!=200)
        {
            return '{"code":400}';
        }
        $response = json_decode($response, true);
		$stripe_bank_account_token=$response['stripe_bank_account_token'];
		return '{"code":200,"stripe_bank_account_token":"'.$stripe_bank_account_token.'"}';
	   }
	   public function create_cust($stripe_bank_account_token,$description,$email,$stripe_key)
	   {
		/* step 3:-  create external account */
			
		$token_request_body = array('source' => $stripe_bank_account_token,'description' => $description,'email' => $email);
		$curl = curl_init('https://api.stripe.com/v1/customers');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_USERPWD, $stripe_key.":");
		curl_setopt($curl, CURLOPT_POST, true );
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type: application/x-www-form-urlencoded"]);
        $response = json_decode(curl_exec($curl), true);
		$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE); 
		if($http_code!=200)
        {
            return '{"code":400}';
        }        
        $customer_id=$response['id']; $bank_id=$response['sources']['data'][0]['id'];
		return '{"code":200,"customer_id":"'.$customer_id.'","bank_id":"'.$bank_id.'"}';
	   }
       
       //Verify token
       public function Verify_token($customet_id,$bank_id,$secret_key)
       {    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/customers/$customet_id/sources/$bank_id/verify");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "amounts[]=32&amounts[]=45");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $secret_key . ":" . "");
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/x-www-form-urlencoded"]);
        $response = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);        
		if($http_code!=200)
        {
            return '{"code":400}';
        }        
        $status=$response['status']; 
		return '{"code":200,"status":"'.$status.'"}';
       }
	
	   public function get_pay_ach($amount,$cust_id,$stripe_key)
	   {
		/* step4:- Make payment */	
		$token_request_body = array('amount' => $amount * 100,
						'currency' => 'usd',
						'customer' => $cust_id);
    	$curl = curl_init('https://api.stripe.com/v1/charges');
    	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($curl, CURLOPT_USERPWD, $stripe_key.":");
    	curl_setopt($curl, CURLOPT_POST, true );
        curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type: application/x-www-form-urlencoded"]);
    	curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
        $response = json_decode(curl_exec($curl));
    	$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if($http_code!=200)
        {
            return '{"code":400}';
        }
        $response->code=200;
        return json_encode($response);
	   }


        //Show Create Invoice Page
        public function getInvoice()
		{	
            $invoice_id=$this->getInvoiceNum();
            $items_array=json_decode($this->getSavedItems());
            $tax='NA';
            $tax_detail=DB::table('invoice_prepopulate_fields')->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->select('tax')->first();     
            if(count($tax_detail)>0)
            {
                $tax=$tax_detail->tax;
            }
                      
			return view('panel.frontend.invoices',['invoice_id'=>$invoice_id,'saved_items'=>$items_array,'tax'=>$tax,'INVOICEEMAILID'=>$this->INVOICEEMAILID,'INVOICETNC'=>$this->INVOICETNC]);
		}
        //Get clients or vendors
        public static function getClientsInvoice(Request $request)
        {           
         
           $type=$request->type;$data=[]; $login_user_type="";
           
           if($request->login_user_type)
           {
            $login_user_type=$request->login_user_type;
            $login_id=$request->login_id;
           }
           else
           {
            $login_user_type=session('user')->category;
            $login_id=session('user')->id;
           }
                   
           if($login_user_type=="admin")
           {
             if($type=="vendor")
             {
              $table='vendors';
              $data=DB::table($table)->where([['created_user_id',$login_id],['is_deleted',0],['status',1]])->select('name','id','id as vendor_id',DB::raw('"" as category'),'created_user_id',DB::raw(''.$login_id.' as login_id, "" as created_user_name , "single" as type'))->orderBy('name','ASC')->get();                        
             }
             else 
             {
              $table='clients';
              $vendors_client1="A.created_user_id in (".DB::raw('select id from users where created_user_id = "'.$login_id.'" and is_deleted=0 and category="vendor"').") and A.vendor_id=0 and A.status=1 and A.is_deleted=0";  // // Created By Vendor Login
              $vendors_client2="A.vendor_id in (".DB::raw('select vendor_id from users where created_user_id="'.$login_id.'" and is_deleted=0 and category="vendor"').") and A.status=1 and A.is_deleted=0"; //// Created By Super Admin Or super admin's manager Login
              $vendors_client3="A.created_user_id in (".DB::raw('select id from users where created_user_id in (select id from users where created_user_id = "'.$login_id.'" and is_deleted=0 and category="vendor") and is_deleted=0 and category="user"').") and A.vendor_id=0 and A.status=1 and A.is_deleted=0";  // // Created By Vendor's Manager Login (manager created by himeself )
              $vendors_client4="A.created_user_id in (".DB::raw('select id from users where manager_id in (select id from managers where vendor_id IN (select vendor_id from users where created_user_id = "'.$login_id.'" and is_deleted=0 and category="vendor")  )and is_deleted=0 and category="user"').") and A.vendor_id=0 and A.status=1 and A.is_deleted=0";  // // Created By Vendor's Manager Login (manager created by other )
                           
              $admin_own=DB::table($table.' as A')
              ->join('users as B','A.created_user_id','=','B.id')
              ->where([['A.created_user_id',$login_id],['A.vendor_id',0],['A.is_deleted',0],['A.status',1],["B.category","admin"]])  // Admin's own clients created by him or his manager
              ->orWhereRaw('A.created_user_id IN  (select id from users where created_user_id="'.$login_id.'" and is_deleted=0 and category="user") and A.vendor_id=0 and A.is_deleted=0 and A.status=1 and B.category="user"')
              ->select('A.name','A.id','A.vendor_id','B.category',DB::raw(''.$login_id.' as created_user_id,'.$login_id.' as login_id, B.name as created_user_name , "double" as type'))->orderBy('A.name','ASC');
             
              $vendor_clients1=DB::table($table.' as A')
              ->join('users as B','A.created_user_id','=','B.id')
              ->where(function($query) use ($vendors_client1) {
                $query->whereRaw($vendors_client1);
              })->select('A.name','A.id','A.vendor_id','B.category','A.created_user_id',DB::raw(''.$login_id.' as login_id, B.name as  created_user_name, "double" as type'))->orderBy('created_user_name','ASC')->orderBy('A.name','ASC');
             
             $vendor_clients2=DB::table($table.' as A')
              ->join('users as B','A.created_user_id','=','B.id')
              ->join('users as D','A.vendor_id','=','D.vendor_id')
              ->where(function($query) use ($vendors_client2) {
                $query->whereRaw($vendors_client2);
              })->select('A.name','A.id','A.vendor_id','B.category','A.created_user_id',DB::raw(''.$login_id.' as login_id, D.name as created_user_name , "double" as type'))->orderBy('created_user_name','ASC')->orderBy('A.name','ASC');
             
              $vendor_clients3=DB::table($table.' as A')
              ->join('users as B','A.created_user_id','=','B.id')
               ->join('users as D','B.created_user_id','=','D.id')
              ->where(function($query) use ($vendors_client3) {
                $query->whereRaw($vendors_client3);
              })->select('A.name','A.id','A.vendor_id','B.category','A.created_user_id',DB::raw(''.$login_id.' as login_id, D.name as  created_user_name, "double" as type'))->orderBy('created_user_name','ASC')->orderBy('A.name','ASC');
            
              $vendor_clients4=DB::table($table.' as A')
              ->join('users as B','A.created_user_id','=','B.id')
               ->join('managers as D','B.manager_id','=','D.id')
               ->join('users As E','D.vendor_id','=','E.vendor_id')
              ->where(function($query) use ($vendors_client4) {
                $query->whereRaw($vendors_client4);
              })->select('A.name','A.id','A.vendor_id','B.category','A.created_user_id',DB::raw(''.$login_id.' as login_id, E.name as  created_user_name, "double" as type'))->orderBy('created_user_name','ASC')->orderBy('A.name','ASC');
            
            
             $data=$admin_own->union($vendor_clients1)->union($vendor_clients2)->union($vendor_clients3)->union($vendor_clients4)->orderBy('created_user_name','ASC')->orderBy('name','ASC')->get();
             }          
             
           }
           else if($login_user_type=="vendor")
           { 
              $table='clients';
              $data=DB::table($table)
              ->where([['created_user_id',$login_id],['is_deleted',0],['vendor_id',0],['status',1]])           //Created by vendor
              ->orWhereRaw("vendor_id=(select vendor_id from users where id='".$login_id."' and is_deleted=0 and category='vendor') and status=1 and is_deleted=0")  //created by superadmin or superadmin's manager
              ->orWhereRaw('created_user_id IN(select id from users where created_user_id="'.$login_id.'" and is_deleted=0 and category="user" ) and vendor_id=0 and status=1 and is_deleted=0') // created by vendors'manager (manager created by himeself )
              ->orWhereRaw('created_user_id IN(select id from users where manager_id IN ( select id from managers where vendor_id = (select vendor_id from users where id="'.$login_id.'" and is_deleted=0 and category="vendor") ) )  and vendor_id=0 and status=1 and is_deleted=0') // created by vendors'manager (manager created by superadmin/other  )            
              ->select('name','id','vendor_id',DB::raw('"" as category'),'created_user_id',DB::raw(''.$login_id.' as login_id, "" as created_user_name , "single" as type'))->orderBy('name','ASC')->get();                 
             
           }                    
          return json_encode($data);          
        }
        //Download Invoice As PDF before emailing to client
        public function downloadInvoiceAsPdf(Request $request)
        {
            $rule=[
            'recurring_on'=>'nullable',
			'client_type' => 'required|in:vendor,client',
            "client"=>"required|numeric",
            "client_email"=>"nullable",
            "cc_email"=>"nullable",
            "company_logo"=>"nullable|image",
            "company_name"=>"required",
            "company_address"=>"required",
            "bill_to"=>"required",
			'due_date' => 'required|numeric|in:0,3,7,14,21,30,45,60,180',
            "invoice_date"=>"required|date_format:m/d/Y", 
            "recurring_date"=>"nullable|date_format:m/d/Y",            
            "item.*"=>"required",
			'description.*' => 'nullable',
			'qty.*' => 'required|numeric',
			'rate.*' => 'required|numeric',
			'amount.*' => 'required|numeric',
            "sub_total"=>"required|numeric",            
            "tax_perc"=>"required|numeric",
            "tax"=>"nullable|numeric",      
            "tax_value"=>"required|numeric",
            "total"=>"required|numeric",
            "paid"=>"required|numeric",
            "balance"=>"required|numeric"						
			];
            // validate data            
            $this->validate($request,$rule);
            
            if($request->has('client_email'))
            {
                $validate_email=$this->validate_comma_sepa_emails($request->client_email);
                $validate_email=json_decode($validate_email);
                if($validate_email->code==400)
                {
                    return response()->make('{"code":404}',200,['content-type'=>'application/json']);  //Invalid Emails
                }
            }
            
            if($request->has('cc_email'))
            {
                $validate_email=$this->validate_comma_sepa_emails($request->cc_email);
                $validate_email=json_decode($validate_email);
                if($validate_email->code==400)
                {
                    return response()->make('{"code":404}',200,['content-type'=>'application/json']);  //Invalid Emails
                }
            }
            
            if($request->client_type=="vendor")
            {
                $colname="vendor_id";
            }
            else
            {
                $colname="client_id";
            }
            $email=DB::table('users')->where([[$colname,$request->client],['category',$request->client_type]])->select('email','name')->first();
            if(count($email)==0)
            {
                return response()->make('{"code":404}',200,['content-type'=>'application/json']);  //Invalid Client
            }
            if($request->hasFile('company_logo'))
            {
            $file=$request->file('company_logo');
            $extension=$file->getClientOriginalExtension();
            $company_logo= date('dmYHis').rand(1,100).'.'.$extension;            
            $fil_res=$file->move('pownder/storage/app/uploads/invoice/company_logo/',$company_logo);
            }
            else
            {
               $company_logo='pownder_logo.png'; 
            }
                      
            $invoice_num=$this->getInvoiceNum(); 
                    
            $items=$request->item;
            $description=$request->description;
            $qty=$request->qty;
            $rate=$request->rate;
            $amount=$request->amount;
            
            //Generate PDF
            $html=View::make('invoice_payment.invoice_pdf_download',['request'=>$request,"email"=>$email,'company_logo'=>$company_logo,'invoice_num'=>$invoice_num,'items'=>$items,'description'=>$description,'qty'=>$qty,'rate'=>$rate,'amount'=>$amount,'INVOICETNC'=>$this->INVOICETNC])->render();
            $pdffilename= 'invoice_preview'.date('dmYHis').rand(1,100).'.pdf';            
            PDF::load($html)->filename('pownder/storage/app/uploads/invoice/invoice_pdf/'.$pdffilename)->output(); 
            return response()->make('{"code":200,"tmpfileurl":"pownder/storage/app/uploads/invoice/invoice_pdf/'.$pdffilename.'"}',200,['content-type'=>'application/json']);
        }
        //delete tmp file
        public function deleteTmpFile(Request $request)
        {
            unlink($request->file);
        }
        //Save Invoice As draft
        public function saveInvoiceAsDraft(Request $request)
        {
          $rule=[
            'recurring_on'=>'nullable',
			'client_type' => 'nullable|in:vendor,client',
            "client"=>"nullable|numeric",
            "client_email"=>"nullable",
            "cc_email"=>"nullable",
            "company_logo"=>"nullable|image",
            "company_name"=>"nullable",
            "company_address"=>"nullable",
            "bill_to"=>"nullable",
			'due_date' => 'required|numeric|in:0,3,7,14,21,30,45,60,180',
            "invoice_date"=>"nullable|date_format:m/d/Y", 
            "recurring_date"=>"nullable|date_format:m/d/Y",            
            "item.*"=>"nullable",
			'description.*' => 'nullable',
			'qty.*' => 'nullable|numeric',
			'rate.*' => 'nullable|numeric',
			'amount.*' => 'nullable|numeric',
            "sub_total"=>"nullable|numeric",            
            "tax_perc"=>"nullable|numeric",
            "tax"=>"nullable|numeric",      
            "tax_value"=>"nullable|numeric",
            "total"=>"nullable|numeric",
            "paid"=>"nullable|numeric",
            "balance"=>"nullable|numeric"						
			];
            $validate=$this->validate($request,$rule);
            if($validate && $validate->fails())
            {
               return '{"code":404,"msg":"'.print_r($validate->All(),true).'"}'; 
            }
            if($request->has('client_email'))
            {
                $validate_email=$this->validate_comma_sepa_emails($request->client_email);
                $validate_email=json_decode($validate_email);
                if($validate_email->code==400)
                {
                    return '{"code":404,"msg":"Invalid client email"}';   //Invalid Emails
                }
            }
            if($request->has('cc_email'))
            {
                $validate_email=$this->validate_comma_sepa_emails($request->cc_email);
                $validate_email=json_decode($validate_email);
                if($validate_email->code==400)
                {
                    return '{"code":404,"msg":"Invalid cc email"}';   //Invalid Emails
                }
            }              
          if($request->has('client_type') && $request->has('client'))
          {  
            if($request->client_type=="vendor")
            {
                $colname="vendor_id";
            }
            else
            {
                $colname="client_id";
            }
            
            $email=DB::table('users')->where([[$colname,$request->client],['category',$request->client_type]])->select('email','name')->first();
            if(count($email)==0)
            {
                return '{"code":404,"msg":"Oops! Invalid client."}';
            }
           } 
            if($request->hasFile('company_logo'))
            {
            $file=$request->file('company_logo');
            $extension=$file->getClientOriginalExtension();
            $company_logo= date('dmYHis').rand(1,100).'.'.$extension;            
            $fil_res=$file->move('pownder/storage/app/uploads/invoice/company_logo/',$company_logo);
            }
            else
            {
               if($request->has('invoice_id'))
               {
               $company_logo=$request->company_logo_org;
               }
               else
               {
                $company_logo='pownder_logo.png';
               } 
               
            }
            $due_date= ($request->has('due_date') && $request->has('invoice_date')) ? date('Y-m-d',strtotime('+'.$request->due_date.' days',strtotime($request->invoice_date))) :'';       
            $recurr_date=$request->has('recurring_date') ? date('Y-m-d H:i:s',strtotime($request->recurring_date)) :'';            
            
            //Save Records
            $draft_id=DB::table('draft_invoices')->insertGetId([
            "client_id"=>$request->client,
            "client_type"=>$request->client_type,
            "client_email"=>$request->client_email,
            "cc_email"=>$request->cc_email,
            "company_logo"=>$company_logo,
            "company_name"=>$request->company_name,
            "company_address"=>$request->company_address,
            "bill_to"=>$request->bill_to,
            "invoice_date"=>date('Y-m-d',strtotime($request->invoice_date)),
            "due_date"=>$due_date,
            'due_on_receipt'=>$request->due_date,
            "recurring_date"=>$recurr_date,
            "sub_total"=>$request->sub_total,
            "tax_perc"=>$request->tax_perc,
            "tax"=>$request->tax_value,
            "total"=>$request->total,
            "paid"=>$request->paid,
            "balance"=>$request->balance,
            "recurring_on"=>$request->has('recurring_on'),            
            "created_user_id"=>session('user')->id,
            "updated_user_id"=>session('user')->id,
            "created_at"=>$this->now,
            "updated_at"=>$this->now            
            ]);
            
            //Save Tax for future use
            if($request->has('tax_perc') && $request->tax_perc )
            {
                $tax_detail=DB::table('invoice_prepopulate_fields')->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->select('id')->first();     
                if(count($tax_detail)==0)
                {
                   DB::table('invoice_prepopulate_fields')->insert(['tax'=>$request->tax_perc,'created_user_id'=>session('user')->id,'created_user_category'=>session('user')->category,'created_at'=>$this->now]); 
                }
                else
                {
                    DB::table('invoice_prepopulate_fields')->where([['id',$tax_detail->id],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->update(['tax'=>$request->tax_perc,'updated_at'=>$this->now]);
                }
            }
            if($request->has('item') && $request->has('description') && $request->has('qty') && $request->has('rate') && $request->has('amount'))
            {
            $items=$request->item;
            $description=$request->description;
            $qty=$request->qty;
            $rate=$request->rate;
            $amount=$request->amount;
            
            $values=[];
            for($i=0;$i<count($items);$i++)
            { 
                $values[]=["draft_id"=>$draft_id,"item"=>$items[$i],"description"=>$description[$i],"qty"=>$qty[$i],"rate"=>$rate[$i],"amount"=>$amount[$i]];
                //Save item name and description for future use
                $item_detail=DB::table('invoice_saved_items')->where([['item',$items[$i]],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->select('id')->first();     
                if(count($item_detail)==0)
                {
                   DB::table('invoice_saved_items')->insert(['item'=>$items[$i],'description'=>$description[$i],'created_user_id'=>session('user')->id,'created_user_category'=>session('user')->category,'created_at'=>$this->now]); 
                }
                else
                {
                    DB::table('invoice_saved_items')->where([['id',$item_detail->id],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->update(['description'=>$description[$i],'updated_at'=>$this->now]);
                }
            }
            
            DB::table('draft_description')->insert($values);            
            }
            Session::flash('alert_success','Invoice has been saved as Draft.');
            return '{"code":200,"msg":"Invoice has been saved as Draft."}';            
              
        }
        //Submit New/Update Invoice and email to  Client                        
		public function postInvoice(Request $request)
		{
            $invoice_id_old=$invoice_num_old='';  $neworold='new';
            
            $rule=[
            'recurring_on'=>'nullable',
            'mark_paid'=>'required|in:on,off',
			'client_type' => 'required|in:vendor,client',
            'cheque.*'=>'nullable|image',            
            "client"=>"required|numeric",
            "client_email"=>"required",
            "cc_email"=>"nullable",
            "company_logo"=>"nullable|image",
            "company_name"=>"required",
            "company_address"=>"required",
            "bill_to"=>"required",
			'due_date' => 'required|numeric|in:0,3,7,14,21,30,45,60,180',
            "invoice_date"=>"required|date_format:m/d/Y", 
            "recurring_date"=>"nullable|date_format:m/d/Y",            
            "item.*"=>"required",
			'description.*' => 'nullable',
			'qty.*' => 'required|numeric',
			'rate.*' => 'required|numeric',
			'amount.*' => 'required|numeric',
            "sub_total"=>"required|numeric",            
            "tax_perc"=>"nullable|numeric",
            "tax"=>"nullable|numeric",      
            "tax_value"=>"required|numeric",
            "total"=>"required|numeric",
            "paid"=>"required|numeric",
            "balance"=>"required|numeric"						
			];
            // validate data            
            $this->validate($request,$rule);
            
            if($request->has('recurring_on') && !$request->has('recurring_date'))
            {
                    $errors=["recurring_date"=>"The recurring date is required."];
                    return Redirect::back()->withInputs($request->all())->withErrors($errors); //Invalid Emails
            }
            
            if($request->has('client_email'))
            {
                $validate_email=$this->validate_comma_sepa_emails($request->client_email);
                $validate_email=json_decode($validate_email);
                if($validate_email->code==400)
                {
                    $errors=["client_email"=>"Invalid client email"];
                    return Redirect::back()->withInputs($request->all())->withErrors($errors); //Invalid Emails
                }
            }
            
            if($request->has('cc_email'))
            {
                $validate_email=$this->validate_comma_sepa_emails($request->cc_email);
                $validate_email=json_decode($validate_email);
                if($validate_email->code==400)
                {
                    $errors=["cc_email"=>"Invalid cc email"];
                    return Redirect::back()->withInputs($request->all())->withErrors($errors); //Invalid Emails
                }
            }
            
            ini_set('max_execution_time',0);
            ini_set('memory_limit',-1);
                      
            $company_name=$request->company_name;          
            if($request->client_type=="vendor")
            {
                $colname="vendor_id";
                $table='vendors';
            }
            else
            {
                $colname="client_id";
                $table='clients';
            }
            
            $email=DB::table('users')->where([[$colname,$request->client],['category',$request->client_type],['status',1],['is_deleted',0]])->select('email','name')->first();
            if(count($email)==0)
            {
                return redirect()->back()->with('alert_danger','Oops! Invalid client.');
            }
            $client_name=$email->name;
            if($request->has('draft_id'))
            {
            $count_draft=DB::table('draft_invoices')->where([['id',$request->draft_id],['created_user_id',session('user')->id]])->count('id');                     
            if($count_draft==0)
            {
              return redirect()->back()->with('alert_danger','Oops! Invalid draft data.');
            } 
            $draft_id=$request->draft_id;
            }
            if($request->has('invoice_id') && $request->has('invoice_num'))
            {
                
            $getmy_clients='""';
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
                $getmy_clients='""';
             }
             }           
            $invoice_id_old=$request->invoice_id;
            $invoice_num_old=$request->invoice_num;
                
            $invoice_data=DB::table('invoice')
            ->where([['invoice_id',$invoice_id_old],['invoice_num',$invoice_num_old],['created_user_id',session('user')->id],['status',1]])
            ->orWhereRaw('invoice_id="'.$invoice_id_old.'" and invoice_num="'.$invoice_num_old.'" and created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and client_id IN ('.$getmy_clients.') and client_type="client" and status=1')
            ->first();
            
            if(count($invoice_data)==0)
            {
                return redirect()->back()->with('alert_danger','Oops! Invalid invoice data');
            }
            
            }
            if($request->hasFile('company_logo'))
            {
            $file=$request->file('company_logo');
            $extension=$file->getClientOriginalExtension();
            $company_logo= date('dmYHis').rand(1,100).'.'.$extension;            
            $fil_res=$file->move('pownder/storage/app/uploads/invoice/company_logo/',$company_logo);
            
            if($request->has('draft_id') && $request->company_logo_org!="pownder_logo.png" || $request->has('invoice_id') && $request->company_logo_org!="pownder_logo.png")
            {
                unlink('pownder/storage/app/uploads/invoice/company_logo/'.$request->company_logo_org);
            }
            }
            else
            {
               if($request->has('draft_id') || $request->has('invoice_id'))
               {
               $company_logo=$request->company_logo_org;
               }
               else
               {
                $company_logo='pownder_logo.png';
               } 
            }
            
            $invoice_num=($request->has('invoice_id') && $request->has('invoice_num') && $request->invoice_edit) ? $request->invoice_num : $this->getInvoiceNum();
           // $recurr_date= $request->has('recurring_date') ? date('Y-m-d H:i:s',strtotime($request->recurring_date)) :'';
           /** 
            * Caculating Recurring Date
            */
            
            $client_data=DB::table($table)->where([['id',$request->client],['is_deleted',0],['status',1]])->select('start_date','pro_rate')->first();                            
            
            if(!count($client_data))            
            {
               return redirect()->back()->with('alert_danger','Oops! Invalid client'); 
            } 
            
            $pro_rate=$client_data->pro_rate;
            $start_date=$client_data->start_date;
            
            if(!$start_date || !$pro_rate)
            {
                return redirect()->back()->with('alert_danger','Oops! Start Date or Pro Rate not defined');  
            }
            /*if($pro_rate=="Yes")
            {
                $next_recurr_date=date('Y-m-d H:i:s',strtotime('+30 days',strtotime('first day of next month')));                           
            } 
            else if($pro_rate=="No")
            {
                $sDay=date('d',strtotime($start_date));
                $sDate=$sDay.'-'.date('m-Y');
                $next_recurr_date=date('Y-m-d H:i:s',strtotime('+30 days',strtotime($sDate)));                      
            }
            else
            {
                return redirect()->back()->with('alert_danger','Oops! Start Date or Pro Rate not defined');
            }                
            */            
            $rec_date=$request->recurring_date;
            $recurr_date=$request->has('recurring_on') ? date('Y-m-d H:i:s',strtotime($rec_date.' '.date('H:i:s'))) :'';
            
            $invoice_date=date('Y-m-d',strtotime($request->invoice_date));
                                    
            if(!$request->has('invoice_id') && !$request->has('invoice_num') && !$request->invoice_edit)
            {
            //Save Records
            $invoice_id=DB::table('invoice')->insertGetId([
            "invoice_num"=>$invoice_num,
            "client_id"=>$request->client,
            "client_type"=>$request->client_type,
            "client_email"=>$request->client_email,
            "cc_email"=>$request->cc_email,
            "company_logo"=>$company_logo,         
            "company_name"=>$request->company_name,
            "company_address"=>$request->company_address,
            "bill_to"=>$request->bill_to,
            "invoice_date"=>date('Y-m-d',strtotime($request->invoice_date)),
            "due_date"=>date('Y-m-d',strtotime('+'.$request->due_date.' days',strtotime($request->invoice_date))),
            "due_on_receipt"=>$request->due_date,
            "recurring_date"=>$recurr_date,
            "sub_total"=>$request->sub_total,
            "tax_perc"=>$request->tax_perc,
            "tax"=>$request->tax_value,
            "total"=>$request->total,
            "paid"=>$request->paid,
            "balance"=>$request->balance,
            "total_amount"=>$request->balance,
            "recurring_on"=>$request->has('recurring_on'),            
            "created_user_id"=>session('user')->id,
            "created_at"=>$this->now            
            ]);
            
            
            }
            else
            {
            //Update Records
            DB::table('invoice')
            ->where([['invoice_id',$invoice_id_old],['invoice_num',$invoice_num_old],['created_user_id',session('user')->id],['status',1]])
            ->orWhereRaw('invoice_id="'.$invoice_id_old.'" and invoice_num="'.$invoice_num_old.'" and created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and client_id IN ('.$getmy_clients.') and client_type="client" and status=1')
            ->update([
            //"invoice_num"=>$invoice_num,
            "client_email"=>$request->client_email,
            "cc_email"=>$request->cc_email,
            "company_logo"=>$company_logo,         
            "company_name"=>$request->company_name,
            "company_address"=>$request->company_address,
            "bill_to"=>$request->bill_to,
            "invoice_date"=>date('Y-m-d',strtotime($request->invoice_date)),
            "due_date"=>date('Y-m-d',strtotime('+'.$request->due_date.' days',strtotime($request->invoice_date))),
            "due_on_receipt"=>$request->due_date,
            "recurring_date"=>$recurr_date,
            "sub_total"=>$request->sub_total,
            "tax_perc"=>$request->tax_perc,
            "tax"=>$request->tax_value,
            "total"=>$request->total,
            "paid"=>$request->paid,
            "balance"=>$request->balance,
            "total_amount"=>$request->balance,
            "recurring_on"=>$request->has('recurring_on'),
            "updated_at"=>$this->now            
            ]);
            
            $invoice_id=$invoice_id_old;
            $neworold='old';
                  
            }
            //Save Tax for future use
            if($request->has('tax_perc') && $request->tax_perc )
            {
                $tax_detail=DB::table('invoice_prepopulate_fields')->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->select('id')->first();     
                if(count($tax_detail)==0)
                {
                   DB::table('invoice_prepopulate_fields')->insert(['tax'=>$request->tax_perc,'created_user_id'=>session('user')->id,'created_user_category'=>session('user')->category,'created_at'=>$this->now]); 
                }
                else
                {
                    DB::table('invoice_prepopulate_fields')->where([['id',$tax_detail->id],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->update(['tax'=>$request->tax_perc,'updated_at'=>$this->now]);
                }
            }
            
            $items=$request->item;
            $description=$request->description;
            $qty=$request->qty;
            $rate=$request->rate;
            $amount=$request->amount;
            
            $values=[];
            for($i=0;$i<count($items);$i++)
            { 
                $values[]=["invoice_id"=>$invoice_id,"item"=>$items[$i],"description"=>$description[$i],"qty"=>$qty[$i],"rate"=>$rate[$i],"amount"=>$amount[$i]];
                //Save item name and description for future use
                $item_detail=DB::table('invoice_saved_items')->where([['item',$items[$i]],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->select('id')->first();     
                if(count($item_detail)==0)
                {
                   DB::table('invoice_saved_items')->insert(['item'=>$items[$i],'description'=>$description[$i],'created_user_id'=>session('user')->id,'created_user_category'=>session('user')->category,'created_at'=>$this->now]); 
                }
                else
                {
                    DB::table('invoice_saved_items')->where([['id',$item_detail->id],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->update(['description'=>$description[$i],'updated_at'=>$this->now]);
                }
            }
            if($request->has('invoice_id') && $request->has('invoice_num') && $request->invoice_edit)
            {
                DB::table('invoice_description')->where('invoice_id',$invoice_id_old)->delete(); 
                
                $recurr_id=DB::table('recurring_invoice')
                ->where([['invoice_id',$invoice_id_old],['invoice_num',$invoice_num_old]])
                ->select('id')->first();
                if(count($recurr_id)>0)
                {
                 $recurring_id=$recurr_id->id;
                 DB::table('recurring_invoice')->where('id',$recurring_id)->delete();
                 DB::table('recurring_invoice_description')->where('recurring_id',$recurring_id)->delete();              
                }          
            }
            DB::table('invoice_description')->insert($values);            
            
            if($request->has('draft_id'))
            {
                DB::table('draft_invoices')->where([['id',$draft_id],['created_user_id',session('user')->id]])->delete();
                DB::table('draft_description')->where('draft_id',$draft_id)->delete();
            }
            
            
            //Generate PDF
            $html=View::make('invoice_payment.invoice_pdf_generate',['request'=>$request,"recurr_date"=>$recurr_date,'company_logo'=>$company_logo,'invoice_num'=>$invoice_num,'items'=>$items,'description'=>$description,'qty'=>$qty,'rate'=>$rate,'amount'=>$amount,"invoice_date"=>$invoice_date,'INVOICETNC'=>$this->INVOICETNC])->render();
                      
            $pdffilename= $invoice_num.'.pdf';
            
            PDF::load($html)->filename('pownder/storage/app/uploads/invoice/invoice_pdf/'.$pdffilename)->output(); 
            
            if($request->mark_paid=="off")
            {           		    
            
            //email to client                        
           
            $from=$this->INVOICEEMAILID;
            if($neworold=="new")
            {
            $subject=str_ireplace('[name]',$client_name,str_ireplace('[invoice_num]',$invoice_num,$this->INVOICESUBJECT));
            }
            else
            {
             $subject="Your Invoice ".$invoice_num." Has Been Updated";   
            }
           // $msg=View::make('invoice_payment.invoice_email_template1',['request'=>$request,"email"=>$email,'company_logo'=>$company_logo,'invoice_num'=>$invoice_num,'items'=>$items,'description'=>$description,'qty'=>$qty,'rate'=>$rate,'amount'=>$amount])->render();
            $msg=View::make('invoice_payment.invoice_email_template',['company_name'=>$company_name,"email"=>$email,'invoice_num'=>$invoice_num,'msg'=>$this->INVOICEMSG , 'neworold'=>$neworold])->render();
                                 
            $filepath=file_get_contents(__DIR__.'/../../../../storage/app/uploads/invoice/invoice_pdf/'.$pdffilename,true); 
            $urls = 'https://api.sendgrid.com/';
            $users = self::SENDGRID_USER;
            $passs = self::SENDGRID_PASS;
            $paramss = array(
            'api_user' => $users,
            'api_key' => $passs,            
            'subject' => $subject,
            'html' => $msg,
            'from' => $from,
            'fromname' => "Pownder�",
            'files['.$pdffilename.']'=>'@'.$filepath.'/'.$pdffilename
            );
            $paramss=http_build_query($paramss);
            $emails=explode(',',$request->client_email);//     priya.constacloud2@gmail.com
            foreach($emails as $email)
            {
              $paramss.='&to[]='.$email;  
            }
            $emails=explode(',',$request->cc_email);//     priya.constacloud2@gmail.com
            foreach($emails as $email)
            {
              $paramss.='&cc[]='.$email; 
            }
            
            $requests = $urls.'api/mail.send.json';
            $sessions = curl_init($requests);
            curl_setopt ($sessions, CURLOPT_POST, true);
            curl_setopt ($sessions, CURLOPT_POSTFIELDS, $paramss);
            curl_setopt($sessions, CURLOPT_HEADER, false);
            curl_setopt($sessions, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($sessions, CURLOPT_RETURNTRANSFER, true);
            $responses = curl_exec($sessions);
            $info=curl_getinfo($sessions);
            curl_close($sessions);  
            
            //Notify admin/invoice creator when updated          
             if($neworold=="old")
             {
             $subject="The Invoice ".$invoice_num." Has Been Updated For ".$client_name;   
            
             $msg="The invoice ".$invoice_num." has been updated. <br/><br/>Please find updated invoice pdf in attachment.";    
                  
             $admin_email=DB::table('users')->where('id',$invoice_data->created_user_id)->select('email','name','category','admin_id')->first(); 
                
             /** Find SuperAdmin's Invoice Email */ 
             if($admin_email->category=="admin")
             {
              $admin_email_data=DB::table('admins')->where([['id',$admin_email->admin_id],['is_deleted',0],['status',1]])->select('invoice_email','receive_notification','invoice_option')->first();
              
              if(is_null($admin_email_data))
              {
                $admin_email->email=''; // Admin Deleted Or Inactive
              }
              else if($admin_email_data->invoice_option=='No')
              {
                $admin_email->email=''; // Admin Don't want notification
              }
              else if($admin_email_data->receive_notification=="Everytime")
              {               
                $admin_email->email=$admin_email_data->invoice_email;  // Send Instant Notifcation
              }
              else
              {
                DB::table('admin_notifications')->insert([
                'to'=>$admin_email_data->invoice_email,
                'toname'=>$admin_email->name,
                'from'=>$from,
                'subject'=>$subject,
                'email_body'=>$msg,
                'attachment_path'=>'storage/app/uploads/invoice/invoice_pdf/',
                'attachment_file'=>$pdffilename,
                "created_user_id"=>session('user')->id,
                "created_user_ip"=>$request->ip(),
                "created_at"=>$this->now,
                'for_person_id'=>$admin_email->admin_id    
                ]);  //Send Later
                $admin_email->email='';
              }
             }
             /** Find SuperAdmin's Invoice Email */ 
              
             if($admin_email->email)
             {  
           
            $paramss = array(
            'api_user' => $users,
            'api_key' => $passs,            
            'subject' => $subject,
            'html' => $msg,
            'from' => $from,
            'fromname' => "Pownder�",
            'to'=>$admin_email->email,
            'toname'=>$admin_email->name,
            'files['.$pdffilename.']'=>'@'.$filepath.'/'.$pdffilename
            );
            $sessions = curl_init($requests);
            curl_setopt ($sessions, CURLOPT_POST, true);
            curl_setopt ($sessions, CURLOPT_POSTFIELDS, $paramss);
            curl_setopt($sessions, CURLOPT_HEADER, false);
            curl_setopt($sessions, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($sessions, CURLOPT_RETURNTRANSFER, true);
            $responses = curl_exec($sessions);
            $info=curl_getinfo($sessions);
            curl_close($sessions);  
             }
             
             }                          
            }              
                        
            //Save/Update Pdf File Name For Future Use
            DB::table('invoice')->where([['invoice_id',$invoice_id],['invoice_num',$invoice_num]])->update(['pdffilename'=>$pdffilename]);
            
            if($request->mark_paid=="on")
            {
                //$request2=new Request();
//                $request2->invoice_num=$invoice_num;
//                $request2->invoice_id=$invoice_id;
//                
//                $result=$this->markInvoicePaid($request2); 
                
                /** Mark As Paid */                
          
             $getmy_clients='""';
             if(session('user')->category=="vendor")
             {
             $request1_1=new  Request();
             $request1_1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1_1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
                $getmy_clients='""';
             }
             }
                         
            $invoice_details=DB::table('invoice as A')->join('invoice_description as B','A.invoice_id','=','B.invoice_id')
            ->where([['A.invoice_id',$invoice_id],['A.invoice_num',$invoice_num],['A.status',1],['A.is_deleted',0],['A.created_user_id',session('user')->id]])
            ->orWhereRaw('A.invoice_id="'.$invoice_id.'" and A.invoice_num="'.$invoice_num.'" and A.status=1 and A.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client"')
            ->select('A.*','B.item','B.description','B.qty','B.rate','B.amount')->orderBy('B.id','asc')->get();
            
            $cheques_images=[];
            if($request->hasFile('cheque'))
            {
                $files=$request->file('cheque');
               
                foreach($files as $file)
                {                                  
                    $filename=$file->getClientOriginalName();
                    $filename=date('U').$filename;
                    $file->move('pownder/storage/app/uploads/invoice/cheque_images/',$filename);
                    $cheques_images[]=$filename;                                   
                }
            }
            $cheques_images=implode(',',$cheques_images);
            
             //Save Records
            $pay_id=DB::table('paid_invoices')->insertGetId([
            "invoice_id"=>$invoice_details[0]->invoice_id,
            "invoice_num"=>$invoice_details[0]->invoice_num,
            "client_id"=>$invoice_details[0]->client_id,
            "client_type"=>$invoice_details[0]->client_type,
            "client_email"=>$invoice_details[0]->client_email,
            "cc_email"=>$invoice_details[0]->cc_email,
            "company_logo"=>$invoice_details[0]->company_logo,
            "company_name"=>$invoice_details[0]->company_name,
            "company_address"=>$invoice_details[0]->company_address,
            "bill_to"=>$invoice_details[0]->bill_to,
            "invoice_date"=>$invoice_details[0]->invoice_date,
            "due_date"=>$invoice_details[0]->due_date,
            "due_on_receipt"=>$invoice_details[0]->due_on_receipt,
            "recurring_date"=>$invoice_details[0]->recurring_date,
            "payment_date"=>date('Y-m-d'),
            "sub_total"=>$invoice_details[0]->sub_total,
            "tax_perc"=>$invoice_details[0]->tax_perc,
            "tax"=>$invoice_details[0]->tax,
            "total"=>$invoice_details[0]->total,
            "paid"=>$invoice_details[0]->paid,
            "balance"=>$invoice_details[0]->balance,
            "total_amount"=>$invoice_details[0]->total_amount,
            "pdffilename"=>$invoice_details[0]->pdffilename,
            "cheque_images"=>$cheques_images,
            "pay_method_type"=>'by_admin',
            "created_at"=>$this->now,
            "updated_at"=>$this->now            
            ]);
            
            $values=[];
            for($i=0;$i<count($invoice_details);$i++)
            {
                $item=$invoice_details[$i]->item;
                $description=$invoice_details[$i]->description;
                $qty=$invoice_details[$i]->qty;
                $rate=$invoice_details[$i]->rate;
                $amount=$invoice_details[$i]->amount;
                $values[]=["pay_id"=>$pay_id,"item"=>$item,"description"=>$description,"qty"=>$qty,"rate"=>$rate,"amount"=>$amount];
            }
            
            DB::table('paid_invoice_description')->insert($values);            
            
            DB::table('invoice')
            ->where([['invoice_num',$invoice_num],['invoice_id',$invoice_id],['status',1],['created_user_id',session('user')->id]])
            ->orWhereRaw('invoice_num="'.$invoice_num.'" and invoice_id="'.$invoice_id.'" and status=1 and created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and client_id IN ('.$getmy_clients.') and client_type="client"')
            ->update(['status'=>0,"payment_date"=>$this->now]);
              /** Mark As :paid */            
            }
          
            if($invoice_num_old && $request->mark_paid=="off")
            {
                return redirect()->back()->with('alert_success','Invoice updated and emailed to client successfully.');
            }
            if($invoice_num_old && $request->mark_paid=="on")
            {
                return redirect()->back()->with('alert_success','Invoice updated and marked as paid successfully.');
            }
            else if($request->mark_paid=="on")
            {
                return redirect()->back()->with('alert_success','Invoice added and marked as paid successfully.');
            }
            else
            {                
                return redirect()->back()->with('alert_success','Invoice added and emailed to client successfully.');
            }    	
		}
        //Get All Invoices
        public function getAllInvoices($filter='all',$filter_type="all")
        {
            $condition=$condition_paid=''; 
            
            if($filter!='all' && $filter_type!="all")
            {
               $filter=urldecode($filter); 
               $type=explode('-',$filter);
               $type_range=explode(' to ',$filter);
               $quarter_type=(strpos('KL'.$filter,'q1_')  || strpos('KL'.$filter,'q2_')  || strpos('KL'.$filter,'q3_') || strpos('KL'.$filter,'q4_') ) ?true :false;
               
               if($quarter_type && $filter_type=="quarter")
               {
                  $condition1=$condition2=$condition3=$condition4='';
                  
                  $quarter_arr=explode('_',$filter,2);
                  $quarter=$quarter_arr[0];$year_quarter=$quarter_arr[1];
         
                 switch($quarter)
                 {
                    case "q1": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (1,2,3)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (1,2,3)';break;
                    case "q2": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (4,5,6)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (4,5,6)';break;
                    case "q3": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (7,8,9)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (7,8,9)';break;
                    case "q4": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (10,11,12)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (10,11,12)';break;
                    default:   $condition="";$condition_paid="";break;
                 }
               }               
                
               else if(count($type_range)==2 && $filter_type=="daterange")
               {
                  $condition=' and A.invoice_date>="'.date('Y-m-d',strtotime($type_range[0])).'" and A.invoice_date<="'.date('Y-m-d',strtotime($type_range[1])).'"';
                  $condition_paid=' and A.payment_date>="'.date('Y-m-d',strtotime($type_range[0])).'" and A.payment_date<="'.date('Y-m-d',strtotime($type_range[1])).'"';          
              
               }
               else if(count($type)==3 && $filter_type=="day")
               {
                  $condition=' and A.invoice_date="'.date('Y-m-d',strtotime($filter)).'"';
                  $condition_paid=' and A.payment_date="'.date('Y-m-d',strtotime($filter)).'"';          
               }
               else if(count($type)==2 && $filter_type=="month")
               {
                  $condition=' and A.invoice_date>="'.date('Y-m-d',strtotime($filter)).'" and A.invoice_date<="'.date('Y-m-t',strtotime($filter)).'"';
                  $condition_paid=' and A.payment_date>="'.date('Y-m-d',strtotime($filter)).'" and A.payment_date<="'.date('Y-m-t',strtotime($filter)).'"';
               }
               else if(count($type)==1 && is_numeric($filter) && $filter_type=="year")
               {
                  $condition=' and YEAR(A.invoice_date)="'.$filter.'"';
                  $condition_paid=' and YEAR(A.payment_date)="'.$filter.'"';
               }
               else if($filter_type=="customer")
               {
                $client_arr=explode(',',$filter,2);
                $client_id=$client_arr[1];
                $client_type=$client_arr[0]; 
                
                $condition=$condition_paid=' and A.client_id="'.$client_id.'" and A.client_type="'.$client_type.'"';
                            
               }
                                     
            }
               
                 $getmy_clients='""';
                 if(session('user')->category=="vendor")
                 {
                 $request1=new  Request();
                 $request1->type="client";                     
                 $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
                 $getmy_clients=array_column($data,'id');
                 if(count($getmy_clients)>0)
                 {
                 $getmy_clients=implode(',',$getmy_clients);
                 }
                 else
                 {
                    $getmy_clients='""';
                 }
                 }
                 
               if($filter_type=="item")
               {                     
                 
                 $condition=$condition_paid=($filter!="other") ? ' and C.id="'.$filter.'"' : ' and C.id IS NULL';
                 
                 $drafts=DB::table('draft_invoices As A')
                 ->join('draft_description As B','A.id','=','B.draft_id')
                 ->leftJoin('invoice_saved_items As C',[['B.item','=','C.item'],['A.created_user_id','C.created_user_id']])
                 ->leftJoin('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
                ->whereRaw('A.created_user_id='.session('user')->id.' and U.status=1 and U.is_deleted=0  and A.is_deleted=0 '.$condition.'')
                ->orWhereRaw('A.created_user_id='.session('user')->id.' and U.status IS NULL and U.is_deleted IS NULL and A.is_deleted=0 '.$condition.'')
                ->select('U.name as client_name','A.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.balance','A.id','A.created_at','A.id As invoice_num',DB::raw("'draft' as invoice_type, 'na' as recurring_on, 'na' as has_recurred"),"A.id as invoice_id")
                ->groupBY('A.id');
                
                $sent=DB::table('invoice As A')
                ->join('invoice_description As B','A.invoice_id','=','B.invoice_id')
                ->leftJoin('invoice_saved_items As C',[['B.item','=','C.item'],['A.created_user_id','C.created_user_id']])
                ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
                ->whereRaw('A.created_user_id="'.session('user')->id.'" and U.status=1 and U.is_deleted=0 and A.status=1 and A.due_date>="'.date('Y-m-d').'" and A.is_deleted=0  '.$condition.' OR A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.due_date>="'.date('Y-m-d').'" and A.is_deleted=0  '.$condition.'')
                ->select('U.name as client_name','A.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.total_amount as balance','A.invoice_id as id','A.created_at','A.invoice_num',DB::raw("'sent' as invoice_type"),"A.recurring_on","A.has_recurred","A.invoice_id")
                ->groupBY('A.invoice_id');
                
                $overdue=DB::table('invoice As A')
                ->join('invoice_description As B','A.invoice_id','=','B.invoice_id')
                 ->leftJoin('invoice_saved_items As C',[['B.item','=','C.item'],['A.created_user_id','C.created_user_id']])
                ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
                ->whereRaw('A.created_user_id="'.session('user')->id.'" and U.status=1 and U.is_deleted=0 and A.status=1 and A.due_date<"'.date('Y-m-d').'" and A.is_deleted=0  '.$condition.' OR A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.due_date<"'.date('Y-m-d').'" and A.is_deleted=0  '.$condition.'')
                ->select('U.name as client_name','A.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.total_amount as balance','A.invoice_id as id','A.created_at','A.invoice_num',DB::raw("'overdue' as invoice_type"),"A.recurring_on","A.has_recurred","A.invoice_id")
                ->groupBY('A.invoice_id');
                
                
                $paid=DB::table('paid_invoices As A')
                ->join('invoice As D',[['A.invoice_id','=','D.invoice_id'],['A.invoice_num','=','D.invoice_num']])
                ->join('paid_invoice_description As B','A.id','=','B.pay_id')
                ->leftJoin('invoice_saved_items As C',[['B.item','=','C.item'],['D.created_user_id','C.created_user_id']])
                ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
                ->whereRaw('D.created_user_id="'.session('user')->id.'" and U.status=1 and U.is_deleted=0 and D.status=0 and A.is_deleted=0  '.$condition_paid.' OR D.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and D.client_id IN ('.$getmy_clients.') and D.client_type="client" and D.status=0  and A.is_deleted=0 '.$condition_paid.'')
                ->select('U.name as client_name','D.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.total_amount as balance','A.id','A.created_at','A.invoice_num',DB::raw("'paid' as invoice_type"),"D.recurring_on","D.has_recurred","A.invoice_id")
                ->groupBY('A.invoice_id'); 
                
                          
              }
              else
              {  
                 $drafts=DB::table('draft_invoices As A')
                ->leftJoin('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                }) 
                ->whereRaw('A.created_user_id='.session('user')->id.' and U.status=1 and U.is_deleted=0 and A.is_deleted=0'.$condition.'')
                ->orWhereRaw('A.created_user_id='.session('user')->id.' and U.status IS NULL  and U.is_deleted IS NULL and A.is_deleted=0'.$condition.'')
                ->select('U.name as client_name','A.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.balance','A.id','A.created_at','A.id As invoice_num',DB::raw("'draft' as invoice_type, 'na' as recurring_on, 'na' as has_recurred"),"A.id as invoice_id");
                
                $sent=DB::table('invoice As A')
                ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })                
                ->whereRaw('A.created_user_id="'.session('user')->id.'" and U.status=1 and U.is_deleted=0 and A.status=1 and A.due_date>="'.date('Y-m-d').'" and A.is_deleted=0 '.$condition.' OR A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.due_date>="'.date('Y-m-d').'" and A.is_deleted=0 '.$condition.'')
                ->select('U.name as client_name','A.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.balance','A.invoice_id as id','A.created_at','A.invoice_num',DB::raw("'sent' as invoice_type"),"A.recurring_on","A.has_recurred","A.invoice_id");
                
                $overdue=DB::table('invoice As A')
               ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
                 ->whereRaw('A.created_user_id="'.session('user')->id.'" and U.status=1 and U.is_deleted=0 and A.status=1 and A.due_date<"'.date('Y-m-d').'" and A.is_deleted=0 '.$condition.' OR A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.due_date<"'.date('Y-m-d').'" and A.is_deleted=0 '.$condition.'')
                ->select('U.name as client_name','A.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.balance','A.invoice_id as id','A.created_at','A.invoice_num',DB::raw("'overdue' as invoice_type"),"A.recurring_on","A.has_recurred","A.invoice_id");            
                
                $paid=DB::table('paid_invoices As A')
                ->join('invoice As D',[['A.invoice_id','=','D.invoice_id'],['A.invoice_num','=','D.invoice_num']])
               ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
                ->whereRaw('D.created_user_id="'.session('user')->id.'" and U.status=1 and U.is_deleted=0 and D.status=0 and A.is_deleted=0 '.$condition_paid.' OR D.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and D.client_id IN ('.$getmy_clients.') and D.client_type="client" and D.status=0 and A.is_deleted=0 '.$condition_paid.'')
                ->select('U.name as client_name','D.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.total_amount as balance','A.id','A.created_at','A.invoice_num',DB::raw("'paid' as invoice_type"),"D.recurring_on","D.has_recurred","A.invoice_id");
                            
              }
            $invoices=$drafts->union($sent)->union($overdue)->union($paid)->orderByDesc('created_at')->get();
          
            return $invoices;
        }
        //Get Sent Invoices
        public function getSentInvoices($filter='all',$filter_type="all")
        {
            $condition=$condition_paid=''; 
            
            if($filter!='all' && $filter_type!="all")
            {
               $filter=urldecode($filter); 
               $type=explode('-',$filter);
               $type_range=explode(' to ',$filter);
               $quarter_type=(strpos('KL'.$filter,'q1_')  || strpos('KL'.$filter,'q2_')  || strpos('KL'.$filter,'q3_') || strpos('KL'.$filter,'q4_') ) ?true :false;
               
               if($quarter_type && $filter_type=="quarter")
               {
                  $condition1=$condition2=$condition3=$condition4='';
                  
                  $quarter_arr=explode('_',$filter,2);
                  $quarter=$quarter_arr[0];$year_quarter=$quarter_arr[1];
         
                 switch($quarter)
                 {
                    case "q1": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (1,2,3)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (1,2,3)';break;
                    case "q2": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (4,5,6)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (4,5,6)';break;
                    case "q3": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (7,8,9)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (7,8,9)';break;
                    case "q4": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (10,11,12)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (10,11,12)';break;
                    default:   $condition="";$condition_paid="";break;
                 }
               }               
                
               else if(count($type_range)==2 && $filter_type=="daterange")
               {
                  $condition=' and A.invoice_date>="'.date('Y-m-d',strtotime($type_range[0])).'" and A.invoice_date<="'.date('Y-m-d',strtotime($type_range[1])).'"';
                  $condition_paid=' and A.payment_date>="'.date('Y-m-d',strtotime($type_range[0])).'" and A.payment_date<="'.date('Y-m-d',strtotime($type_range[1])).'"';          
              
               }
               else if(count($type)==3 && $filter_type=="day")
               {
                  $condition=' and A.invoice_date="'.date('Y-m-d',strtotime($filter)).'"';
                  $condition_paid=' and A.payment_date="'.date('Y-m-d',strtotime($filter)).'"';          
               }
               else if(count($type)==2 && $filter_type=="month")
               {
                  $condition=' and A.invoice_date>="'.date('Y-m-d',strtotime($filter)).'" and A.invoice_date<="'.date('Y-m-t',strtotime($filter)).'"';
                  $condition_paid=' and A.payment_date>="'.date('Y-m-d',strtotime($filter)).'" and A.payment_date<="'.date('Y-m-t',strtotime($filter)).'"';
               }
               else if(count($type)==1 && is_numeric($filter) && $filter_type=="year")
               {
                  $condition=' and YEAR(A.invoice_date)="'.$filter.'"';
                  $condition_paid=' and YEAR(A.payment_date)="'.$filter.'"';
               }
               else if($filter_type=="customer")
               {
                $client_arr=explode(',',$filter,2);
                $client_id=$client_arr[1];
                $client_type=$client_arr[0]; 
                
                $condition=$condition_paid=' and A.client_id="'.$client_id.'" and A.client_type="'.$client_type.'"';
                            
               }
                                     
            }
               
                 $getmy_clients='""';
                 if(session('user')->category=="vendor")
                 {
                 $request1=new  Request();
                 $request1->type="client";                     
                 $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
                 $getmy_clients=array_column($data,'id');
                 if(count($getmy_clients)>0)
                 {
                 $getmy_clients=implode(',',$getmy_clients);
                 }
                 else
                 {
                    $getmy_clients='""';
                 }
                 }
                 
               if($filter_type=="item")
               {                     
                 
                 $condition=$condition_paid=($filter!="other") ? ' and C.id="'.$filter.'"' : ' and C.id IS NULL';
                 
               
                $sent=DB::table('invoice As A')
                ->join('invoice_description As B','A.invoice_id','=','B.invoice_id')
                ->leftJoin('invoice_saved_items As C',[['B.item','=','C.item'],['A.created_user_id','C.created_user_id']])
                ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
                ->whereRaw('A.created_user_id="'.session('user')->id.'" and U.status=1 and U.is_deleted=0 and A.status=1 and A.due_date>="'.date('Y-m-d').'" and A.is_deleted=0  '.$condition.' OR A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.due_date>="'.date('Y-m-d').'" and A.is_deleted=0  '.$condition.'')
                ->select('U.name as client_name','A.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.total_amount as balance','A.invoice_id as id','A.created_at','A.invoice_num',DB::raw("'sent' as invoice_type"),"A.recurring_on","A.has_recurred","A.invoice_id")
                ->groupBY('A.invoice_id');
              }
              else
              {  
                  
                $sent=DB::table('invoice As A')
                ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })                
                ->whereRaw('A.created_user_id="'.session('user')->id.'" and U.status=1 and U.is_deleted=0 and A.status=1 and A.due_date>="'.date('Y-m-d').'" and A.is_deleted=0 '.$condition.' OR A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.due_date>="'.date('Y-m-d').'" and A.is_deleted=0 '.$condition.'')
                ->select('U.name as client_name','A.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.balance','A.invoice_id as id','A.created_at','A.invoice_num',DB::raw("'sent' as invoice_type"),"A.recurring_on","A.has_recurred","A.invoice_id");
                   
              }
            $invoices=$sent->orderByDesc('created_at')->get();
          
            return $invoices;
        }
        //get Over due invoices
        public function getOverdueInvoices($filter="all",$filter_type="all")
        {
            $condition=$condition_paid=''; 
            
            if($filter!='all' && $filter_type!="all")
            {
               $filter=urldecode($filter); 
               $type=explode('-',$filter);
               $type_range=explode(' to ',$filter);
               $quarter_type=(strpos('KL'.$filter,'q1_')  || strpos('KL'.$filter,'q2_')  || strpos('KL'.$filter,'q3_') || strpos('KL'.$filter,'q4_') ) ?true :false;
               
               if($quarter_type && $filter_type=="quarter")
               {
                  $condition1=$condition2=$condition3=$condition4='';
                  
                  $quarter_arr=explode('_',$filter,2);
                  $quarter=$quarter_arr[0];$year_quarter=$quarter_arr[1];
         
                 switch($quarter)
                 {
                    case "q1": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (1,2,3)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (1,2,3)';break;
                    case "q2": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (4,5,6)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (4,5,6)';break;
                    case "q3": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (7,8,9)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (7,8,9)';break;
                    case "q4": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (10,11,12)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (10,11,12)';break;
                    default:   $condition="";$condition_paid="";break;
                 }
               }               
                
               else if(count($type_range)==2 && $filter_type=="daterange")
               {
                  $condition=' and A.invoice_date>="'.date('Y-m-d',strtotime($type_range[0])).'" and A.invoice_date<="'.date('Y-m-d',strtotime($type_range[1])).'"';
                  $condition_paid=' and A.payment_date>="'.date('Y-m-d',strtotime($type_range[0])).'" and A.payment_date<="'.date('Y-m-d',strtotime($type_range[1])).'"';          
              
               }
               else if(count($type)==3 && $filter_type=="day")
               {
                  $condition=' and A.invoice_date="'.date('Y-m-d',strtotime($filter)).'"';
                  $condition_paid=' and A.payment_date="'.date('Y-m-d',strtotime($filter)).'"';          
               }
               else if(count($type)==2 && $filter_type=="month")
               {
                  $condition=' and A.invoice_date>="'.date('Y-m-d',strtotime($filter)).'" and A.invoice_date<="'.date('Y-m-t',strtotime($filter)).'"';
                  $condition_paid=' and A.payment_date>="'.date('Y-m-d',strtotime($filter)).'" and A.payment_date<="'.date('Y-m-t',strtotime($filter)).'"';
               }
               else if(count($type)==1 && is_numeric($filter) && $filter_type=="year")
               {
                  $condition=' and YEAR(A.invoice_date)="'.$filter.'"';
                  $condition_paid=' and YEAR(A.payment_date)="'.$filter.'"';
               }
               else if($filter_type=="customer")
               {
                $client_arr=explode(',',$filter,2);
                $client_id=$client_arr[1];
                $client_type=$client_arr[0]; 
                
                $condition=$condition_paid=' and A.client_id="'.$client_id.'" and A.client_type="'.$client_type.'"';
                            
               }
                                     
            }
               
                 $getmy_clients='""';
                 if(session('user')->category=="vendor")
                 {
                 $request1=new  Request();
                 $request1->type="client";                     
                 $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
                 $getmy_clients=array_column($data,'id');
                 if(count($getmy_clients)>0)
                 {
                 $getmy_clients=implode(',',$getmy_clients);
                 }
                 else
                 {
                    $getmy_clients='""';
                 }
                 }
                 
               if($filter_type=="item")
               {                     
                 
                 $condition=$condition_paid=($filter!="other") ? ' and C.id="'.$filter.'"' : ' and C.id IS NULL';
                                  
                $overdue=DB::table('invoice As A')
                ->join('invoice_description As B','A.invoice_id','=','B.invoice_id')
                 ->leftJoin('invoice_saved_items As C',[['B.item','=','C.item'],['A.created_user_id','C.created_user_id']])
                ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
                ->whereRaw('A.created_user_id="'.session('user')->id.'" and U.status=1 and U.is_deleted=0 and A.status=1 and A.due_date<"'.date('Y-m-d').'" and A.is_deleted=0  '.$condition.' OR A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.due_date<"'.date('Y-m-d').'" and A.is_deleted=0  '.$condition.'')
                ->select('U.name as client_name','A.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.total_amount as balance','A.invoice_id as id','A.created_at','A.invoice_num',DB::raw("'overdue' as invoice_type"),"A.recurring_on","A.has_recurred","A.invoice_id")
                ->groupBY('A.invoice_id');
              }
              else
              {  
                
                $overdue=DB::table('invoice As A')
               ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
                 ->whereRaw('A.created_user_id="'.session('user')->id.'" and U.status=1 and U.is_deleted=0 and A.status=1 and A.due_date<"'.date('Y-m-d').'" and A.is_deleted=0 '.$condition.' OR A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.due_date<"'.date('Y-m-d').'" and A.is_deleted=0 '.$condition.'')
                ->select('U.name as client_name','A.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.balance','A.invoice_id as id','A.created_at','A.invoice_num',DB::raw("'overdue' as invoice_type"),"A.recurring_on","A.has_recurred","A.invoice_id");            
                           
              }
            $invoices=$overdue->orderByDesc('created_at')->get();
          
            return $invoices;
        }
        //Get draft invoices
        public function getDraftInvoices($filter="all",$filter_type="all")
        {
            $condition=$condition_paid=''; 
            
            if($filter!='all' && $filter_type!="all")
            {
               $filter=urldecode($filter); 
               $type=explode('-',$filter);
               $type_range=explode(' to ',$filter);
               $quarter_type=(strpos('KL'.$filter,'q1_')  || strpos('KL'.$filter,'q2_')  || strpos('KL'.$filter,'q3_') || strpos('KL'.$filter,'q4_') ) ?true :false;
               
               if($quarter_type && $filter_type=="quarter")
               {
                  $condition1=$condition2=$condition3=$condition4='';
                  
                  $quarter_arr=explode('_',$filter,2);
                  $quarter=$quarter_arr[0];$year_quarter=$quarter_arr[1];
         
                 switch($quarter)
                 {
                    case "q1": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (1,2,3)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (1,2,3)';break;
                    case "q2": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (4,5,6)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (4,5,6)';break;
                    case "q3": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (7,8,9)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (7,8,9)';break;
                    case "q4": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (10,11,12)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (10,11,12)';break;
                    default:   $condition="";$condition_paid="";break;
                 }
               }               
                
               else if(count($type_range)==2 && $filter_type=="daterange")
               {
                  $condition=' and A.invoice_date>="'.date('Y-m-d',strtotime($type_range[0])).'" and A.invoice_date<="'.date('Y-m-d',strtotime($type_range[1])).'"';
                  $condition_paid=' and A.payment_date>="'.date('Y-m-d',strtotime($type_range[0])).'" and A.payment_date<="'.date('Y-m-d',strtotime($type_range[1])).'"';          
              
               }
               else if(count($type)==3 && $filter_type=="day")
               {
                  $condition=' and A.invoice_date="'.date('Y-m-d',strtotime($filter)).'"';
                  $condition_paid=' and A.payment_date="'.date('Y-m-d',strtotime($filter)).'"';          
               }
               else if(count($type)==2 && $filter_type=="month")
               {
                  $condition=' and A.invoice_date>="'.date('Y-m-d',strtotime($filter)).'" and A.invoice_date<="'.date('Y-m-t',strtotime($filter)).'"';
                  $condition_paid=' and A.payment_date>="'.date('Y-m-d',strtotime($filter)).'" and A.payment_date<="'.date('Y-m-t',strtotime($filter)).'"';
               }
               else if(count($type)==1 && is_numeric($filter) && $filter_type=="year")
               {
                  $condition=' and YEAR(A.invoice_date)="'.$filter.'"';
                  $condition_paid=' and YEAR(A.payment_date)="'.$filter.'"';
               }
               else if($filter_type=="customer")
               {
                $client_arr=explode(',',$filter,2);
                $client_id=$client_arr[1];
                $client_type=$client_arr[0]; 
                
                $condition=$condition_paid=' and A.client_id="'.$client_id.'" and A.client_type="'.$client_type.'"';
                            
               }
                                     
            }
               
                 $getmy_clients='""';
                 if(session('user')->category=="vendor")
                 {
                 $request1=new  Request();
                 $request1->type="client";                     
                 $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
                 $getmy_clients=array_column($data,'id');
                 if(count($getmy_clients)>0)
                 {
                 $getmy_clients=implode(',',$getmy_clients);
                 }
                 else
                 {
                    $getmy_clients='""';
                 }
                 }
                 
               if($filter_type=="item")
               {                     
                 
                 $condition=$condition_paid=($filter!="other") ? ' and C.id="'.$filter.'"' : ' and C.id IS NULL';
                 
                 $drafts=DB::table('draft_invoices As A')
                 ->join('draft_description As B','A.id','=','B.draft_id')
                 ->leftJoin('invoice_saved_items As C',[['B.item','=','C.item'],['A.created_user_id','C.created_user_id']])
                 ->leftJoin('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
                ->whereRaw('A.created_user_id='.session('user')->id.' and U.status=1 and U.is_deleted=0  and A.is_deleted=0 '.$condition.'')
                ->orWhereRaw('A.created_user_id='.session('user')->id.' and U.status IS NULL and U.is_deleted IS NULL and A.is_deleted=0 '.$condition.'')
                ->select('U.name as client_name','A.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.balance','A.id','A.created_at','A.id As invoice_num',DB::raw("'draft' as invoice_type, 'na' as recurring_on, 'na' as has_recurred"),"A.id as invoice_id")
                ->groupBY('A.id');
              }
              else
              {  
                 $drafts=DB::table('draft_invoices As A')
                ->leftJoin('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                }) 
                ->whereRaw('A.created_user_id='.session('user')->id.' and U.status=1 and U.is_deleted=0 and A.is_deleted=0'.$condition.'')
                ->orWhereRaw('A.created_user_id='.session('user')->id.' and U.status IS NULL  and U.is_deleted IS NULL and A.is_deleted=0'.$condition.'')
                ->select('U.name as client_name','A.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.balance','A.id','A.created_at','A.id As invoice_num',DB::raw("'draft' as invoice_type, 'na' as recurring_on, 'na' as has_recurred"),"A.id as invoice_id");
                           
              }
            $invoices=$drafts->orderByDesc('created_at')->get();
          
            return $invoices;
        }
        //Get paid invoices
        public function getPaidInvoices($filter="all",$filter_type="all",$payment_mode='all')
        {
            $condition=$condition_paid=$payment_mode_condition=''; 
            
            if($payment_mode=="by_admin" || $payment_mode=="bank")
            {
                $payment_mode_condition=' and A.pay_method_type="'.$payment_mode.'"';
            }
            else if($payment_mode=="dcard")
            {
                $payment_mode_condition=' and A.pay_method_type="card" and A.creditcardfee_perc IS NULL and A.creditcardfee_amount IS NULL ';
            }
             else if($payment_mode=="ccard")
            {
                $payment_mode_condition=' and A.pay_method_type="card" and A.creditcardfee_perc IS NOT NULL and A.creditcardfee_amount IS NOT NULL ';
            }
            
            
            if($filter!='all' && $filter_type!="all")
            {
               $filter=urldecode($filter); 
               $type=explode('-',$filter);
               $type_range=explode(' to ',$filter);
               $quarter_type=(strpos('KL'.$filter,'q1_')  || strpos('KL'.$filter,'q2_')  || strpos('KL'.$filter,'q3_') || strpos('KL'.$filter,'q4_') ) ?true :false;
               
               if($quarter_type && $filter_type=="quarter")
               {
                  $condition1=$condition2=$condition3=$condition4='';
                  
                  $quarter_arr=explode('_',$filter,2);
                  $quarter=$quarter_arr[0];$year_quarter=$quarter_arr[1];
         
                 switch($quarter)
                 {
                    case "q1": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (1,2,3)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (1,2,3)';break;
                    case "q2": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (4,5,6)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (4,5,6)';break;
                    case "q3": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (7,8,9)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (7,8,9)';break;
                    case "q4": $condition=' and YEAR(A.invoice_date)="'.$year_quarter.'" and MONTH(A.invoice_date) IN (10,11,12)';$condition_paid=' and YEAR(A.payment_date)="'.$year_quarter.'" and MONTH(A.payment_date) IN (10,11,12)';break;
                    default:   $condition="";$condition_paid="";break;
                 }
               }               
                
               else if(count($type_range)==2 && $filter_type=="daterange")
               {
                  $condition=' and A.invoice_date>="'.date('Y-m-d',strtotime($type_range[0])).'" and A.invoice_date<="'.date('Y-m-d',strtotime($type_range[1])).'"';
                  $condition_paid=' and A.payment_date>="'.date('Y-m-d',strtotime($type_range[0])).'" and A.payment_date<="'.date('Y-m-d',strtotime($type_range[1])).'"';          
              
               }
               else if(count($type)==3 && $filter_type=="day")
               {
                  $condition=' and A.invoice_date="'.date('Y-m-d',strtotime($filter)).'"';
                  $condition_paid=' and A.payment_date="'.date('Y-m-d',strtotime($filter)).'"';          
               }
               else if(count($type)==2 && $filter_type=="month")
               {
                  $condition=' and A.invoice_date>="'.date('Y-m-d',strtotime($filter)).'" and A.invoice_date<="'.date('Y-m-t',strtotime($filter)).'"';
                  $condition_paid=' and A.payment_date>="'.date('Y-m-d',strtotime($filter)).'" and A.payment_date<="'.date('Y-m-t',strtotime($filter)).'"';
               }
               else if(count($type)==1 && is_numeric($filter) && $filter_type=="year")
               {
                  $condition=' and YEAR(A.invoice_date)="'.$filter.'"';
                  $condition_paid=' and YEAR(A.payment_date)="'.$filter.'"';
               }
               else if($filter_type=="customer")
               {
                $client_arr=explode(',',$filter,2);
                $client_id=$client_arr[1];
                $client_type=$client_arr[0]; 
                
                $condition=$condition_paid=' and A.client_id="'.$client_id.'" and A.client_type="'.$client_type.'"';
                            
               }
                                     
            }
               
                 $getmy_clients='""';
                 if(session('user')->category=="vendor")
                 {
                 $request1=new  Request();
                 $request1->type="client";                     
                 $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
                 $getmy_clients=array_column($data,'id');
                 if(count($getmy_clients)>0)
                 {
                 $getmy_clients=implode(',',$getmy_clients);
                 }
                 else
                 {
                    $getmy_clients='""';
                 }
                 }
                 
               if($filter_type=="item")
               {                     
                 
                 $condition=$condition_paid=($filter!="other") ? ' and C.id="'.$filter.'"' : ' and C.id IS NULL';                 
                
                $paid=DB::table('paid_invoices As A')
                ->join('invoice As D',[['A.invoice_id','=','D.invoice_id'],['A.invoice_num','=','D.invoice_num']])
                ->join('paid_invoice_description As B','A.id','=','B.pay_id')
                ->leftJoin('invoice_saved_items As C',[['B.item','=','C.item'],['D.created_user_id','C.created_user_id']])
                ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
                ->whereRaw('D.created_user_id="'.session('user')->id.'" and U.status=1 and U.is_deleted=0 and D.status=0 and A.is_deleted=0  '.$condition_paid.' '.$payment_mode_condition.' OR D.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and D.client_id IN ('.$getmy_clients.') and D.client_type="client" and D.status=0  and A.is_deleted=0 '.$condition_paid.' '.$payment_mode_condition.' ')
                ->select('U.name as client_name','D.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.total_amount as balance','A.id','A.created_at','A.invoice_num',DB::raw("'paid' as invoice_type"),"D.recurring_on","D.has_recurred","A.invoice_id")
                ->groupBY('A.invoice_id'); 
                
                          
              }
              else
              { 
                
                $paid=DB::table('paid_invoices As A')
                ->join('invoice As D',[['A.invoice_id','=','D.invoice_id'],['A.invoice_num','=','D.invoice_num']])
               ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
                ->whereRaw('D.created_user_id="'.session('user')->id.'" and U.status=1 and U.is_deleted=0 and D.status=0 and A.is_deleted=0 '.$condition_paid.' '.$payment_mode_condition.' OR D.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and D.client_id IN ('.$getmy_clients.') and D.client_type="client" and D.status=0 and A.is_deleted=0 '.$condition_paid.' '.$payment_mode_condition.' ')
                ->select('U.name as client_name','D.created_user_id','A.due_date','A.client_email','A.cc_email','A.invoice_date','A.bill_to','A.total_amount as balance','A.id','A.created_at','A.invoice_num',DB::raw("'paid' as invoice_type"),"D.recurring_on","D.has_recurred","A.invoice_id");
                            
              }
            $invoices=$paid->orderByDesc('created_at')->get();
          
            return $invoices;
        } 
        //Show Manage Invoice Page
        public function manageInvoice(Request $request)
        {            
            if(!$request->has('filter'))
            {
            $invoices=$this->getAllInvoices();
            }
            else if($request->filter=="outstanding")
            {
             $invoices_sent=json_decode(json_encode($this->getSentInvoices()));
             $invoices_overdue=json_decode(json_encode($this->getOverdueInvoices()));
             $invoices=array_merge($invoices_sent,$invoices_overdue);
            }
            else if($request->filter=="overdue")
            {
             $invoices=$this->getOverdueInvoices();
            }  
            else
            {
             $type=($request->has('type')) ? $request->type : "";
             $invoices=$this->getAllInvoices($request->filter,$type);
            }            
            return view('panel.frontend.manage_invoice',['invoices'=>$invoices]);
        }
        //Draft Invoice Continue
        public function draftInvoiceContinue($id)
        {
           $draft_data=DB::table('draft_invoices')->where([['id',$id],['created_user_id',session('user')->id],['is_deleted',0]])->first();
                     
           if(count($draft_data)>0)
           {
            $client_name='';$clients_list=[];
            
            $draft_items=DB::table('draft_description')->where('draft_id',$draft_data->id)->orderBy('id','asc')->get();
            if(!is_null($draft_data->client_type))
            {
            $request1=new Request();
            $request1->type=$draft_data->client_type;
            $clients_list=json_decode($this->getClientsInvoice($request1));            
            }
            $draft_data->client_name=$client_name;
            Session::flash('draft_items',$draft_items);
            Session::flash('draft_data',$draft_data);
            Session::flash('draft_continue',1);
            Session::flash('clients_list',$clients_list); 
            //Session::put(['draft_items'=>$draft_items,'draft_data'=>$draft_data,'draft_continue'=>1,'clients_list'=>$clients_list]);                       
            return redirect()->route('invoice');
           }
           else
           {
            return redirect()->route('manage-invoice')->with('alert_danger','Oops! Invalid draft data');
           }
        }
        //Update draft invoice
        public function updateInvoiceDraft(Request $request)
        {
          $rule=[
            'recurring_on'=>'nullable',          
			'client_type' => 'nullable|in:vendor,client',
            "client"=>"nullable|numeric",
            "client_email"=>"nullable",
            "cc_email"=>"nullable",
            "company_logo"=>"nullable|image",
            "company_logo_org"=>"required",
            "draft_id"=>"required|numeric",
            "company_name"=>"nullable",
            "company_address"=>"nullable",
            "bill_to"=>"nullable",
			'due_date' => 'required|numeric|in:0,3,7,14,21,30,45,60,180',
            "invoice_date"=>"nullable|date_format:m/d/Y", 
            "recurring_date"=>"nullable|date_format:m/d/Y",            
            "item.*"=>"nullable",
			'description.*' => 'nullable',
			'qty.*' => 'nullable|numeric',
			'rate.*' => 'nullable|numeric',
			'amount.*' => 'nullable|numeric',
            "sub_total"=>"nullable|numeric",            
            "tax_perc"=>"nullable|numeric",
            "tax"=>"nullable|numeric",      
            "tax_value"=>"nullable|numeric",
            "total"=>"nullable|numeric",
            "paid"=>"nullable|numeric",
            "balance"=>"nullable|numeric"						
			];
            $validate=$this->validate($request,$rule);
            if($validate && $validate->fails())
            {
               return '{"code":404,"msg":"'.print_r($validate->All(),true).'"}'; 
            }
            if($request->has('client_email'))
            {
                $validate_email=$this->validate_comma_sepa_emails($request->client_email);
                $validate_email=json_decode($validate_email);
                if($validate_email->code==400)
                {
                    return '{"code":404,"msg":"Oops! Invalid client email"}';  //Invalid Emails
                }
            }
            if($request->has('cc_email'))
            {
                $validate_email=$this->validate_comma_sepa_emails($request->cc_email);
                $validate_email=json_decode($validate_email);
                if($validate_email->code==400)
                {
                    return '{"code":404,"msg":"Oops! Invalid cc email."}';  //Invalid Emails
                }
            }
          if($request->has('client_type') && $request->has('client'))
          {  
            if($request->client_type=="vendor")
            {
                $colname="vendor_id";
            }
            else
            {
                $colname="client_id";
            }
            
            $email=DB::table('users')->where([[$colname,$request->client],['category',$request->client_type]])->select('email','name')->first();
            
            if(count($email)==0)
            {
                return '{"code":404,"msg":"Oops! Invalid client."}';
            }
           } 
           $count_draft=DB::table('draft_invoices')->where([['id',$request->draft_id],['created_user_id',session('user')->id]])->count('id');
                     
           if($count_draft==0)
           {
            return '{"code":404,"msg":"Oops! Invalid draft data."}';
           } 
            $draft_id=$request->draft_id;
            if($request->hasFile('company_logo'))
            {
            $file=$request->file('company_logo');
            $extension=$file->getClientOriginalExtension();
            $company_logo= date('dmYHis').rand(1,100).'.'.$extension;            
            $fil_res=$file->move('pownder/storage/app/uploads/invoice/company_logo/',$company_logo);
            if($request->company_logo_org!="pownder_logo.png")
            {
            unlink('pownder/storage/app/uploads/invoice/company_logo/'.$request->company_logo_org);
            }
            }
            else
            {
               $company_logo=$request->company_logo_org; 
            }
            $due_date= $request->has('invoice_date') ? date('Y-m-d',strtotime('+'.$request->due_date.' days',strtotime($request->invoice_date))) :'';       
            $recurr_date= $request->has('recurring_date') ? date('Y-m-d H:i:s',strtotime($request->recurring_date)) :'';            
            
            //Update Records
            DB::table('draft_invoices')->where([['id',$draft_id],['created_user_id',session('user')->id],['is_deleted',0]])
            ->update([
            "client_id"=>$request->client,
            "client_type"=>$request->client_type,
            "client_email"=>$request->client_email,
            "cc_email"=>$request->cc_email,
            "company_logo"=>$company_logo,
            "company_name"=>$request->company_name,
            "company_address"=>$request->company_address,
            "bill_to"=>$request->bill_to,
            "invoice_date"=>date('Y-m-d',strtotime($request->invoice_date)),
            "due_date"=>$due_date,
            'due_on_receipt'=>$request->due_date,
            "recurring_date"=>$recurr_date,
            "sub_total"=>$request->sub_total,
            "tax_perc"=>$request->tax_perc,
            "tax"=>$request->tax_value,
            "total"=>$request->total,
            "paid"=>$request->paid,
            "balance"=>$request->balance,
            "recurring_on"=>$request->has('recurring_on'),
            "updated_user_id"=>session('user')->id,
            "updated_at"=>$this->now            
            ]);
            
            //Save Tax for future use
            if($request->has('tax_perc') && $request->tax_perc )
            {
                $tax_detail=DB::table('invoice_prepopulate_fields')->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->select('id')->first();     
                if(count($tax_detail)==0)
                {
                   DB::table('invoice_prepopulate_fields')->insert(['tax'=>$request->tax_perc,'created_user_id'=>session('user')->id,'created_user_category'=>session('user')->category,'created_at'=>$this->now]); 
                }
                else
                {
                    DB::table('invoice_prepopulate_fields')->where([['id',$tax_detail->id],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->update(['tax'=>$request->tax_perc,'updated_at'=>$this->now]);
                }
            }
            if($request->has('item') && $request->has('description') && $request->has('qty') && $request->has('rate') && $request->has('amount'))
            {
            $items=$request->item;
            $description=$request->description;
            $qty=$request->qty;
            $rate=$request->rate;
            $amount=$request->amount;
            
            DB::table('draft_description')->where('draft_id',$draft_id)->delete();
            
            $values=[];
            for($i=0;$i<count($items);$i++)
            { 
                $values[]=["draft_id"=>$draft_id,"item"=>$items[$i],"description"=>$description[$i],"qty"=>$qty[$i],"rate"=>$rate[$i],"amount"=>$amount[$i]];
                //Save item name and description for future use
                $item_detail=DB::table('invoice_saved_items')->where([['item',$items[$i]],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->select('id')->first();     
                if(count($item_detail)==0)
                {
                   DB::table('invoice_saved_items')->insert(['item'=>$items[$i],'description'=>$description[$i],'created_user_id'=>session('user')->id,'created_user_category'=>session('user')->category,'created_at'=>$this->now]); 
                }
                else
                {
                    DB::table('invoice_saved_items')->where([['id',$item_detail->id],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->update(['description'=>$description[$i],'updated_at'=>$this->now]);
                }
            }
            
            DB::table('draft_description')->insert($values);            
            }
            Session::flash('alert_success','Draft invoice updated successfully.');
            return '{"code":200,"msg":"Draft invoice updated successfully."}';            
              
        }
        //Delete Saved Item
        public function deleteSavedItem($id)
        {
            DB::table('invoice_saved_items')->where([['id',$id],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])
            ->delete();
            return response()->make('{"code":200,"msg":"Item removed successfully"}',200,['content-type'=>'application/json']);
        }
        //Show Invoice
        public function ViewInvoice($type,$id)
        {
             $getmy_clients='""';
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
                $getmy_clients='""';
             }
             }       
        
            $table=($type=="paid") ? 'paid_invoices' :'invoice' ;
            $table1=($type=="paid") ? 'paid_invoice_description' :'invoice_description' ;
            $conditions=($type=="paid") ? [['A.id',$id],['C.created_user_id',session('user')->id],['C.status','0'],['U.is_deleted',0],['U.status',1],['A.is_deleted',0]] :[['A.invoice_id',$id],['A.status','1'],['A.created_user_id',session('user')->id],['U.is_deleted',0],['U.status',1],['A.is_deleted',0]] ;
            $ORconditions=($type=="paid") ? 'A.id="'.$id.'" and C.created_user_id="'.session('user')->id.'" and C.status=0 and U.is_deleted=0 and U.status=1 and A.is_deleted=0 OR A.id="'.$id.'" and C.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and C.client_id IN ('.$getmy_clients.') and C.client_type="client" and C.status=0 and U.is_deleted=0 and U.status=1 and A.is_deleted=0'  :'A.invoice_id="'.$id.'" and A.status=1 and A.created_user_id="'.session('user')->id.'" and U.is_deleted=0 and U.status=1 and A.is_deleted=0 OR A.invoice_id="'.$id.'" and A.status=1 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and U.is_deleted=0 and U.status=1 and A.is_deleted=0' ;
            
            $view= ($type=="paid") ? 'panel.frontend.view_invoice_paid' : 'panel.frontend.view_invoice_sent';

            $invoice_data=[];
            if($type=="paid")
            {
            $invoice_data=DB::table($table.' as A')
            ->join('invoice As C',[['A.invoice_id','=','C.invoice_id'],['A.invoice_num','=','C.invoice_num']])
            ->join($table1.' as B','A.id','=','B.pay_id')
            ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
            ->where($conditions)
            ->orWhereRaw($ORconditions)            
            ->select('A.*','B.item','B.description','B.qty','B.rate','B.amount','B.id As item_id','C.has_recurred','C.recurring_on')->orderBy('B.id','asc')->get(); 
            }
            else
            {
            $invoice_data=DB::table($table.' as A')->join($table1.' as B','A.invoice_id','=','B.invoice_id')
            ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
            ->where($conditions)
            ->orWhereRaw($ORconditions)   
            ->select('A.*','B.item','B.description','B.qty','B.rate','B.amount','B.id As item_id')->get();
            //print_r($invoice_data);die;
            }
            if(count($invoice_data)==0)
            {
                Session::flash('alert_danger','Oops! Invoice not found');
            } 
            $items_array=json_decode($this->getSavedItems());
            return view($view,['invoice_data'=>$invoice_data,'saved_items'=>$items_array]);
        }
        //Get client Info
        public function getClientInfo(Request $request)
        {
            if($request->client_type=="vendor")
            {
                $table='vendors';
            }
            else
            {
                $table='clients';
            }
            $data=DB::table($table)->where([['id',$request->client],['is_deleted',0],['status',1]])->select('name','mobile','invoice_email','address','city','state','zip','url','pro_rate','start_date')->first();
            if(count($data)==0)
            {
                return response()->make('{"code":404,"msg":"Not found"}',404,['content-type'=>'application/json']);
            }
            
            /** 
            * Caculating Recurring Date
            */            
           
            $pro_rate=$data->pro_rate;
            $start_date=$data->start_date;
            
            if(!$start_date || !$pro_rate)
            {
                return response()->make('{"code":900,"msg":"Oops! Start Date or Pro Rate not defined"}',200,['content-type'=>'application/json']);                
            }
            
            if(!$request->recurring_date_only)
            {            
            $data->invoice_date=date('m/d/Y');            
            }
            else
            {            
            $data->invoice_date=$request->invoice_date;            
            }
            
            if($pro_rate=="Yes")
            {
                    $next_recurr_date=date('m/d/Y',strtotime('first day of next month',strtotime($data->invoice_date)));                           
            } 
            else if($pro_rate=="No")
            {                
                $next_recurr_date=date('m/d/Y',strtotime('+1 month',strtotime($data->invoice_date)));                      
            }
            else
            {
                return response()->make('{"code":900,"msg":"Oops! Start Date or Pro Rate not defined"}',200,['content-type'=>'application/json']);
            } 
           
            
            $check_invoice_exists=DB::table('invoice')->where([['client_id',$request->client],['client_type',$request->client_type]])->count('invoice_id');
            
            
            
            if($check_invoice_exists==0)
            {
              if(!$request->recurring_date_only)
              {
              $data->invoice_date=date('m/d/Y',strtotime($start_date)); 
              $invoice_date=date('m/d/Y');
              }
              else
              {            
              $data->invoice_date=$request->invoice_date; 
              $invoice_date=$request->invoice_date;
              $start_date=$request->invoice_date;           
              }
                if($pro_rate=="Yes")
                {
                $next_recurr_date=date('m/d/Y',strtotime('first day of next month',strtotime($invoice_date)));                           
                } 
                else if($pro_rate=="No")
                {                    
                    $start_date_ob=date_create($start_date);
                    $today_obj=date_create($this->now);
                    
                    $difference=date_diff($start_date_ob,$today_obj);
                    $years=$difference->format('%r%y');$months=$years*12;
                    $months1=$difference->format('%r%m');$months1=$months1+$months;
                    if(!$request->recurring_date_only)
                    {
                    $months1=($months1) ? $months1+1 : 1;
                    }
                    else
                    {
                      $months1=1;  
                    }
                    
                    $next_recurr_date=date('m/d/Y',strtotime('+'.$months1.' months',strtotime($start_date)));                      
                }
            }
            $recurr_date=$next_recurr_date;
            $data->recurring_date=$recurr_date;
            
            $data->code=200;
            return response()->make(json_encode($data),200,['content-type'=>'application/json']);
        }
		// Show confirm invoice page
		public function confirmAddInvoice($id)
		{
          $invoice_data=[];
          $invoice_data=DB::table('invoice as A')->join('invoice_description as B','A.invoice_id','=','B.invoice_id')
            ->where([['A.invoice_num',$id],['A.status',1],['A.is_deleted',0]])
            ->select('A.*','B.item','B.description','B.qty','B.rate','B.amount','B.id As item_id')->orderBy('B.id','asc')->get();
            if(count($invoice_data)==0)
            {
                Session::flash('alert_danger','Oops! Invoice not found or has already been paid.');
            }            
            else
            {
                if($invoice_data[0]->client_type=="vendor")
                {
                   $table='vendors';
                }
                else
                {
                    $table='clients';
                }
                $client_data=DB::table($table)->where([['id',$invoice_data[0]->client_id]])->select('mobile','name',DB::raw('invoice_email as email'),'address','zip','city','state')->first(); //,['status',1],['is_deleted',0]
                $client_data->created_user_id=$invoice_data[0]->created_user_id;
                Session::put('invoice_data',$client_data);   
            }     
            return view('invoice_payment.confirm_invoice',['invoice_data'=>$invoice_data,'INVOICETNC'=>$this->INVOICETNC]);            
		}
        //Show Additional Information Page
        public function invoiceDetails($id)
        {
            if(!Session::has('invoice_data'))
            {
              return redirect('/')->with('alert_success','Thank you. You have successfully submitted payment.');
            } 
            $countries=DB::table('countries')->orderBy('name','ASC')->get();
            
            return view('invoice_payment.invoice_details',['countries'=>$countries,"invoiceno"=>$id]);
        }
        //Submit Invoice details
        public function SubmitInvoiceDetails(Request $request,$id)
        {
            if(!Session::has('invoice_data'))
            {
              return redirect('/')->with('alert_success','Thank you. You have successfully submitted payment.');
            }  
            $rule=
            [
             "company_name"=>"required|max:100",
             "telephone"=>"required|regex:/^\([0-9]{3}\)\s[0-9]{3}\-[0-9]{4}$/",
             "telephone_type"=>"required|in:mobile,work,personal",
             "first_name"=>"required|max:50",
             "email"=>"required|email",
             "middle_name"=>"nullable|max:50",
             "last_name"=>"required|max:50",
             "country"=>"required",
             "address"=>"required|max:200",
             "address2"=>"nullable|max:100",
             "city"=>"required|max:100",
             "state"=>"required|max:100",
             "zip"=>"required|regex:/^[0-9]{5}$/"
            ];
            $this->validate($request,$rule);
            Session::put('data',$request->All());
            return Redirect::route('payment-method',['id'=>$id]);
        }
        //Show Payment Method Page
        public function paymentMethod($id)
        {
            if(!Session::has('invoice_data') || !session()->has('data'))
            {
              return redirect('/')->with('alert_success','Thank you. You have successfully submitted payment.');
            }    
            $countries=DB::table('countries')->orderBy('name','ASC')->get();
            $states=DB::table('states')->orderBy('name','ASC')->get();
            return view('invoice_payment.payment_method',['countries'=>$countries,"states"=>$states,"invoiceno"=>$id]);
            
        }
        //Submit Payment Method
        public function SubmitPaymentMethod(Request $request,$id)
        {
           if(!Session::has('invoice_data') || !session()->has('data'))
            {
              return redirect('/')->with('alert_success','Thank you. You have successfully submitted payment.');
            } 
           $current_year=date('Y');$to_year=date('Y',strtotime('+10 years'));
           $rule=
            [
             "name_on_card"=>"required|max:100",
             "card_number"=>"required|regex:/^[0-9]{4}\-[0-9]{4}\-[0-9]{4}\-[0-9]{4}$/",
             "cvv"=>"required|regex:/^[0-9]{3,4}$/",
             "month"=>"required|date_format:m",
             "year"=>"required|date_format:Y|after_or_equal:".$current_year."|before_or_equal:".$to_year,             
             "country"=>"required",
             "address"=>"required|max:200",
             "address2"=>"nullable|max:100",
             "city"=>"required|max:100",
             "state"=>"required|max:100",
             "zip"=>"required|regex:/^[0-9]{5}$/",
             "terms"=>"required|in:on,off"
            ];
            $this->validate($request,$rule);            
            
            $invoice_details=DB::table('invoice as A')
            ->where([['A.invoice_num',$id],['A.status',1],['A.is_deleted',0]])->select('A.*')->first();
            
            if(is_null($invoice_details))
            {               
                return Redirect::back()->with('alert_danger','Oops! Invoice not found or has already been paid.');
            }  
            
            if($invoice_details->client_type=="vendor")
            {
                $colname="vendor_id";
            }
            else
            {
                $colname="client_id";
            }
                        
            $client=DB::table('invoice as A')
            ->join('users as B',[['B.'.$colname,'=','A.client_id'],['B.category','=','A.client_type']])
            ->where([['A.invoice_num',$id],['A.status',1],['A.is_deleted',0]])->select('B.email','B.name')->first();
            
            if(count($client)==0 || count($invoice_details)==0)
            {
              return Redirect::back()->with('alert_danger','Oops! invalid request.')->withInput($request->All()); 
            }                        
            
            $expiry_year=substr($request->year,-2);
            $key=$this->stripe_secret_key_live; //Stripe Scret Key 
            $card_number=$request->card_number;//str_replace('-','',$request->card_number); 
            $exp_month=sprintf('%02d',$request->month);
            $exp_year=$expiry_year;
            $cvv=$request->cvv;
            $amount=$invoice_details->balance; 
            $email=session('data')['email'];
            
            $invoice_settings=DB::table('invoice_settings')->select('cc_charge')->first();
            $credit_card_processing_fee=3.5; 
            if(count($invoice_settings)>0)
            {
                $credit_card_processing_fee=$invoice_settings->cc_charge; 
            }          
          
            $card_data=json_decode($this->get_pay($key,$card_number,$exp_month,$exp_year,$cvv,$amount,$email,$credit_card_processing_fee)); /* call for create token using card details */
            
            if($card_data->code!=200)
            {
                $erros=['card_number'=>'The card details is invalid','cvv'=>'The card details is invalid','month'=>'The card details is invalid','year'=>'The card details is invalid'];
                return redirect()->back()->withErrors($erros)->withInput($request->All());
            }
            $token=$card_data->token; 
            sleep(5);
            $formdata=$request->All();
            $formdata['token']=$token;
            $formdata['card_brand']=$card_data->card_brand;
            $formdata['card_type']=$card_data->card_type;
            $formdata['credit_card_processing_fee']=$credit_card_processing_fee;
            $formdata['cvc_last4']=$card_data->cvc_last4;
            $formdata['client_name']=$client->name;
            $formdata['pay_method_type']='card';
            $formdata['invoice_details']=json_decode(json_encode($invoice_details),true);
            Session::put('data2',$formdata); 
            
            return Redirect::route('payment-verification',['id'=>$id]);//->with('alert_success','card verified successfully'); 
        }
        //Submit Payment Method
        public function SubmitPaymentMethodACH(Request $request,$id)
        {
          
           if(!Session::has('invoice_data') || !session()->has('data'))
            {
              return response()->make('{"code":401,"msg":"Oops! session has expired.Please start process again."}',400,['content-type'=>'application/json']);
            }
           $rule=
            [
             "pt"=>"required",
             "bank_name"=>"required",
             "cvc_last4"=>"required"
             //"ach_id"=>"required"
            ];
            $validate=$this->validate($request,$rule);
            if($validate && $validate->fails())
            {
              return response()->make('{"code":300,"msg":"'.json_encode($erros->all()).'"}',200,['content-type'=>'application/json']);            
            }            
            
            $invoice_details=DB::table('invoice as A')
            ->where([['A.invoice_num',$id],['A.status',1],['A.is_deleted',0]])->select('A.*')->first();
            
            if(is_null($invoice_details))
            {
              return response()->make('{"code":401,"msg":"Oops! Invoice not found or has already been paid."}',400,['content-type'=>'application/json']);            
            } 
            
            if($invoice_details->client_type=="vendor")
            {
                $colname="vendor_id";
            }
            else
            {
                $colname="client_id";
            }
                        
            $client=DB::table('invoice as A')
            ->join('users as B',[['B.'.$colname,'=','A.client_id'],['B.category','=','A.client_type']])
            ->where([['A.invoice_num',$id],['A.status',1],['A.is_deleted',0]])->select('B.email','B.name')->first();
            
            if(count($client)==0 || count($invoice_details)==0)
            {
              return response()->make('{"code":300,"msg":"Oops! invalid request"}',400,['content-type'=>'application/json']);            
            };
            
            $formdata=$request->All();
            $formdata['client_name']=$client->name;
            $formdata['invoice_details']=json_decode(json_encode($invoice_details),true);
            $formdata['pay_method_type']='bank';
            Session::put('data2',$formdata); 
            
            return response()->make('{"code":200,"msg":""}',200,['content-type'=>'application/json']);  
        }
        //Show Verification Page
        public function paymentVerification($id)
        {
          if(!Session::has('invoice_data') || !session()->has('data') || !session()->has('data2'))
            {
              return redirect('/')->with('alert_success','Thank you. You have successfully submitted payment.');
            } 
         $payment_terms=DB::table('invoice_prepopulate_fields')->where('created_user_id',session('invoice_data')->created_user_id)->select('payment_tnc')->first();
              
          return view('invoice_payment.payment_verification',["invoiceno"=>$id,"payment_terms"=>$payment_terms->payment_tnc]);   
        }
        //Submit Verification Page
        public function SubmitPaymentVerification(Request $request,$id)
        {            
             
            if(!Session::has('invoice_data') || !session()->has('data') || !session()->has('data2'))
            {
              return redirect('/')->with('alert_success','Thank you. You have successfully submitted payment.');
            } 
            
            $invoice_details=DB::table('invoice as A')->join('invoice_description as B','A.invoice_id','=','B.invoice_id')
            ->where([['A.invoice_num',$id],['A.status',1],['A.is_deleted',0]])
            ->select('A.*','B.item','B.description','B.qty','B.rate','B.amount')->orderBy('B.id','asc')->get();
            
            if($invoice_details && count($invoice_details)==0)
            {
              return Redirect::back()->with('alert_danger','Oops! invalid request.'); 
            }
            $invoice_num=$invoice_details[0]->invoice_num;  
            
            if($invoice_details[0]->client_type=="vendor")
            {
                $colname="vendor_id";
                $table='vendors';
            }
            else
            {
                $colname="client_id";
                $table='clients';
            }
            $client_email=DB::table($table)->where('id',$invoice_details[0]->client_id)->select('invoice_email','name')->first();
                                    
            $description= 'Invoice Payment Against '.$invoice_num; 
            if(session('data2')['pay_method_type']=="card")
            {
            $key=$this->stripe_secret_key_live; //Stripe Secret Key
            $amount=$invoice_details[0]->balance;
            $token=session('data2')['token'];
            
            if(session('data2')['card_type']=="credit")
            {
              $credit_charge=round($amount*session('data2')["credit_card_processing_fee"]/100,2);
              $amount_pay=$amount+$credit_charge;   
            }
            else
            {
              $amount_pay=$amount;
            }
            $response=json_decode($this->charge($key,$amount_pay,$token,$description)); /* create charge using source */
            
            if($response->code!=200)
            {
                return redirect()->back()->with('alert_danger','Oops! payment couldn\'t be made.');
            }
            }
            else if(session('data2')['pay_method_type']=="bank")
            {              
                //$client_id=$this->plaid_client_id; /* plaid account client id */
            	//$secret=$this->plaid_secret_key; /* plaid account secret */
            	$stripe_key=$this->stripe_secret_key_live; /* stripe account secret key */
            	$token=session('data2')['pt']; /* public token */
            	//$account=session('data2')['ach_id'];/* account id  */
            	$email=session('data')['email'];
            	$amount=$invoice_details[0]->balance;
           	     

                /* step1:- exchange public token */
               // $step1=json_decode($this->get_exchange($client_id,$secret,$token));
//                if($step1->code!=200)
//                {
//                  return redirect()->back()->with('alert_danger','Oops! payment couldn\'t be made.Token has been expired.Please select <strong><a href="'.route('payment-method',['id'=>$id]).'">payment method</a></strong> again.');  
//                }
//                $access_token=$step1->access_token;
//                if(!isset($access_token))
//            	{
//                  return redirect()->back()->with('alert_danger','Oops! payment couldn\'t be made.Token has been expired.Please select <strong><a href="'.route('payment-method',['id'=>$id]).'">payment method</a></strong> again.');  
//                }    
//        		/* step2:- generate stripe token */
        	//	$step2=json_decode($this->get_stripe_tok($client_id,$secret,$access_token,$account));
//                if($step2->code!=200)
//                {
//                  return redirect()->back()->with('alert_danger','Oops! payment couldn\'t be made.Token has been expired.Please select <strong><a href="'.route('payment-method',['id'=>$id]).'">payment method</a></strong> again.');  
//                }
//        	    $stripe_bank_account_token=$step2->stripe_bank_account_token;
//                if(!isset($stripe_bank_account_token))
//            	{
//                  return redirect()->back()->with('alert_danger','Oops! payment couldn\'t be made.Token has been expired.Please select <strong><a href="'.route('payment-method',['id'=>$id]).'">payment method</a></strong> again.');  
//                }                        	
   			  /* step 1:-  create external account */
    			$step1=json_decode($this->create_cust($token,$description,$email,$stripe_key));
                
                if($step1->code!=200)
                {
                  return redirect()->back()->with('alert_danger','Oops! payment couldn\'t be made.Token has been expired.Please select <strong><a href="'.route('payment-method',['id'=>$id]).'">payment method</a></strong> again.');  
                }
    			$customer_id=$step1->customer_id; $bank_id=$step1->bank_id;
    			if(!isset($customer_id) || !isset($bank_id))
            	{
                  return redirect()->back()->with('alert_danger','Oops! payment couldn\'t be made.Token has been expired.Please select <strong><a href="'.route('payment-method',['id'=>$id]).'">payment method</a></strong> again.');  
                } 
                /* step 2:-  Verify Token */
    			$step2=json_decode($this->Verify_token($customer_id,$bank_id,$stripe_key));
                if($step2->code!=200)
                {
                  return redirect()->back()->with('alert_danger','Oops! Token couldn\'t not be verified.Please select <strong><a href="'.route('payment-method',['id'=>$id]).'">payment method</a></strong> again.');  
                }
                if($step2->status!='verified')
                {
                  return redirect()->back()->with('alert_danger','Oops! Token couldn\'t not be verified.Please select <strong><a href="'.route('payment-method',['id'=>$id]).'">payment method</a></strong> again.');  
                }  			
                
				/* step3:- Make payment */	
				$step3=json_decode($this->get_pay_ach($amount,$customer_id,$stripe_key));
    			if($step3->code!=200)
                {
                  return redirect()->back()->with('alert_danger','Oops! payment couldn\'t be made.Token has been expired.Please select <strong><a href="'.route('payment-method',['id'=>$id]).'">payment method</a></strong> again.');  
                }
               
                $response=new \stdClass(); 
                if($step3->captured==1)
                {
                $response->paid=1; 
                $response->status="succeeded";                
                }
                else
                {
                $response->paid=0; 
                $response->status="failed";
                }                
            }
            else
            {
                return Redirect::back()->with('alert_danger','Oops! Invalid payment method.');
            }
            if($response->paid==1 && $response->status=="succeeded")
            {  
            if(session('data2')['pay_method_type']=="card" &&  session('data2')['card_type']=="credit")
            {
                DB::table('invoice')->where([["invoice_id",$invoice_details[0]->invoice_id],["invoice_num",$invoice_details[0]->invoice_num],['status',1],['is_deleted',0]])
                ->update(["creditcardfee_perc"=>session('data2')["credit_card_processing_fee"],"creditcardfee_amount"=>$credit_charge,"total_amount"=>$amount_pay]);
                
                $invoice_details=DB::table('invoice as A')->join('invoice_description as B','A.invoice_id','=','B.invoice_id')
                ->where([['A.invoice_num',$id],['A.status',1],['A.is_deleted',0]])
                ->select('A.*','B.item','B.description','B.qty','B.rate','B.amount')->orderBy('B.id','asc')->get();
                
                if(count($invoice_details)==0)
                {
                  return Redirect::back()->with('alert_danger','Oops! invalid request.'); 
                }
            }
            
            $payment_via=(session('data2')['pay_method_type']=="bank") ?session('data2')['bank_name'] :session('data2')['card_brand'];
                
             //Save Records
            $pay_id=DB::table('paid_invoices')->insertGetId([
            "invoice_id"=>$invoice_details[0]->invoice_id,
            "invoice_num"=>$invoice_details[0]->invoice_num,
            "client_id"=>$invoice_details[0]->client_id,
            "client_type"=>$invoice_details[0]->client_type,
            "client_email"=>$invoice_details[0]->client_email,
            "cc_email"=>$invoice_details[0]->cc_email,
            "company_logo"=>$invoice_details[0]->company_logo,
            "company_name"=>$invoice_details[0]->company_name,
            "company_address"=>$invoice_details[0]->company_address,
            "bill_to"=>$invoice_details[0]->bill_to,
            "invoice_date"=>$invoice_details[0]->invoice_date,
            "due_date"=>$invoice_details[0]->due_date,
            "due_on_receipt"=>$invoice_details[0]->due_on_receipt,
            "recurring_date"=>$invoice_details[0]->recurring_date,
            "payment_date"=>date('Y-m-d'),
            "sub_total"=>$invoice_details[0]->sub_total,
            "tax_perc"=>$invoice_details[0]->tax_perc,
            "tax"=>$invoice_details[0]->tax,
            "total"=>$invoice_details[0]->total,
            "paid"=>$invoice_details[0]->paid,
            "balance"=>$invoice_details[0]->balance,
            "creditcardfee_perc"=>$invoice_details[0]->creditcardfee_perc,
            "creditcardfee_amount"=>$invoice_details[0]->creditcardfee_amount,
            "total_amount"=>$invoice_details[0]->total_amount,
            "pay_method_type"=>session('data2')['pay_method_type'],
            "payment_via"=>$payment_via,
            "pdffilename"=>$invoice_details[0]->pdffilename,
            "created_at"=>$this->now,
            "updated_at"=>$this->now            
            ]);
            
            $values=$items=$description=$qty=$rate=$amount=[];
            
            for($i=0;$i<count($invoice_details);$i++)
            {
                $items[$i]=$invoice_details[$i]->item;
                $description[$i]=$invoice_details[$i]->description;
                $qty[$i]=$invoice_details[$i]->qty;
                $rate[$i]=$invoice_details[$i]->rate;
                $amount[$i]=$invoice_details[$i]->amount;
                $values[]=["pay_id"=>$pay_id,"item"=>$items[$i],"description"=>$description[$i],"qty"=>$qty[$i],"rate"=>$rate[$i],"amount"=>$amount[$i]];
            }
            
            DB::table('paid_invoice_description')->insert($values);
            
            DB::table('paid_invoice_addinfo')->insert([
            'pay_id'=>$pay_id,
            "company_name"=>session('data')['company_name'],
            "first_name"=>session('data')['first_name'],
            "middle_name"=>session('data')['middle_name'],
            "last_name"=>session('data')['last_name'],
            "mobile"=>session('data')['telephone'],
            "email"=>session('data')['email'],
            "country"=>session('data')['country'],
            "address"=>session('data')['address'],
            "address2"=>session('data')['address2'],
            "city"=>session('data')['city'],
            "state"=>session('data')['state'],
            "zip"=>session('data')['zip'],
            "created_at"=>$this->now,
            "updated_at"=>$this->now 
            ]);     
            DB::table('invoice')->where([['invoice_num',$id],['invoice_id',$invoice_details[0]->invoice_id],['status',1],['is_deleted',0]])
            ->update(['status'=>0,"payment_date"=>$this->now]); 
            $data=session('data2');
            $data['confirmation_no']=$pay_id;
            Session::put('data2',$data); 
            
            
            //Update invoice pdf with paid tag
            
            $html=View::make('invoice_payment.invoice_paid_notif_pdf_generate',['request'=>$invoice_details[0],"recurr_date"=>$invoice_details[0]->recurring_date,'company_logo'=>$invoice_details[0]->company_logo,'invoice_num'=>$invoice_num,'items'=>$items,'description'=>$description,'qty'=>$qty,'rate'=>$rate,'amount'=>$amount,'INVOICETNC'=>$this->INVOICETNC])->render();
            $pdffilename= $invoice_details[0]->pdffilename;
            
            unlink('pownder/storage/app/uploads/invoice/invoice_pdf/'.$pdffilename); // delete old file
            
            PDF::load($html)->filename('pownder/storage/app/uploads/invoice/invoice_pdf/'.$pdffilename)->output(); 
            
            
            //Notify Admin
            
            /** Find Email */
                        
            $admin_email=DB::table('users')->where('id',$invoice_details[0]->created_user_id)->select('email','name','category','id','admin_id')->first();            
            
            $from=$this->INVOICEEMAILID;
            $subject='Payment Done : '.$invoice_num;
            $msg=View::make('invoice_payment.invoice_paid_notif_template',['invoice_num'=>$invoice_num])->render();
            $msg_client=View::make('invoice_payment.invoice_paid_notif_template_client',["client_name"=>$client_email->name,"company_name"=>$invoice_details[0]->company_name,'invoice_num'=>$invoice_num])->render();
                                
            $filepath=file_get_contents(__DIR__.'/../../../../storage/app/uploads/invoice/invoice_pdf/'.$pdffilename,true);   
              
                
             /** Find SuperAdmin's Invoice Email */ 
             if($admin_email->category=="admin")
             {
              $admin_email_data=DB::table('admins')->where([['id',$admin_email->admin_id],['is_deleted',0],['status',1]])->select('invoice_email','receive_notification','invoice_option')->first();
              
              if(is_null($admin_email_data))
              {
                $admin_email->email=''; // Admin Deleted Or Inactive
              }
              else if($admin_email_data->invoice_option=='No')
              {
                $admin_email->email=''; // Admin Don't want notification
              }
              else if($admin_email_data->receive_notification=="Everytime")
              {               
                $admin_email->email=$admin_email_data->invoice_email;  // Send Instant Notifcation
              }
              else
              {
                DB::table('admin_notifications')->insert([
                'to'=>$admin_email_data->invoice_email,
                'toname'=>$admin_email->name,
                'from'=>$from,
                'subject'=>$subject,
                'email_body'=>$msg,
                'attachment_path'=>'storage/app/uploads/invoice/invoice_pdf/',
                'attachment_file'=>$pdffilename,
                "created_user_id"=>session('user')->id,
                "created_user_ip"=>$request->ip(),
                "created_at"=>$this->now,
                'for_person_id'=>$admin_email->admin_id    
                ]);  //Send Later
                $admin_email->email='';
              }
             }
             /** Find SuperAdmin's Invoice Email */               
            
            
            if($admin_email->category=="admin" && $invoice_details[0]->client_type=="client" )
            {             
            $request_new=new Request();
            $request_new->type="client";
            $request_new->login_user_type="admin";
            $request_new->login_id=$invoice_details[0]->created_user_id;
            $admin_clients=$this->getClientsInvoice($request_new);
            $clients_list=json_decode($admin_clients); 
            
            $own_clients=$vendor_clients=[];  
            
            if(count($clients_list)>0)
            {
                foreach($clients_list as $client) 
                {
                 if($client->login_id==$client->created_user_id && $client->vendor_id==0 && $client->type=="double")
                 {
                      $own_clients[]=$client->id;
                 }                
                 else
                 {
                      $vendor_clients[]=$client->id;
                 }     
                }
            }
            
            if(in_array($invoice_details[0]->client_id,$own_clients))
            {
              $admin_email=$admin_email->email;
            }
            else
            { 
             $client_detail=DB::table('clients')->where([['id',$invoice_details[0]->client_id]])->select('vendor_id','created_user_id')->first();
             
             if(count($client_detail)>0 && $client_detail->vendor_id)
             {
               $vendor_email=DB::table('users')->where([['vendor_id',$client_detail->vendor_id],['category','vendor']])->select('email','name')->first();
              
               $vendor_email=$vendor_email->email;
             
               $admin_email=$admin_email->email;
               
               $admin_email=$admin_email.','.$vendor_email;           
           
             }
             else if(count($client_detail)>0)
             {
             $user_category=DB::table('users')->where([['id',$client_detail->created_user_id]])->select('id','email','name','category','created_user_id')->first();
                 
             if($user_category->category=="vendor")
             {   
             $vendor_email=$user_category->email;
             $admin_email=$admin_email->email;
             $admin_email=$admin_email.','.$vendor_email;          
             }
             else
             {
              $client_by_manager_self=DB::select("select id from clients A where A.created_user_id in (".DB::raw('select id from users where created_user_id in (select id from users where created_user_id = "'.$admin_email->id.'" and is_deleted=0 and category="vendor") and is_deleted=0 and category="user"').") and A.vendor_id=0 and A.status=1 and A.is_deleted=0");  // // Created By Vendor's Manager Login (manager created by himeself )
              $client_by_manager_self=json_decode($client_by_manager_self,true);
              $client_by_manager_self=array_column($client_by_manager_self,'id');
              if(in_array($invoice_details[0]->client_id,$client_by_manager_self))
              {
                $vendor_email=DB::table('users')->where('id',$user_category->created_user_id)->select('email','name')->first();
                $vendor_email=$vendor_email->email;
                $admin_email=$admin_email->email;
                $admin_email=$admin_email.','.$vendor_email;
            
              }
              else
              {           
                $manager_detail=DB::select('select vendor_id from managers where vendor_id IN (select vendor_id from users where created_user_id = "'.$admin_email->id.'" and is_deleted=0 and category="vendor") and id='.$user_category->id.'');
                $vendor_email=DB::table('users')->where([['vendor_id',$manager_detail[0]->vendor_id],['category','vendor']])->select('email','name')->first();
                $vendor_email=$vendor_email->email;
                $admin_email=$admin_email->email;
                $admin_email=$admin_email.','.$vendor_email;
            
              }                       

             }          
             }
              
            }  
            }
            else 
            {
            $admin_email=$admin_email->email;
            }
            $admin_email=trim($admin_email,', ');
            /** Find Email */
            
            if($admin_email)
            {
           
            $urls = 'https://api.sendgrid.com/';
            $users = self::SENDGRID_USER;
            $passs = self::SENDGRID_PASS;
                        
            //Notify Admin
            $paramss = array(
            'api_user' => $users,
            'api_key' => $passs,            
            'subject' => $subject,
            'html' => $msg,
            'from' => $from,
            'fromname' => "Pownder�",
            'files['.$pdffilename.']'=>'@'.$filepath.'/'.$pdffilename
            );
            $paramss=http_build_query($paramss);
            
            $emails=explode(',',$admin_email);
            
            foreach($emails as $email)
            {
                $paramss.='&to[]='.$email;//"priya.constacloud2@gmail.com"
            }
            
            $requests = $urls.'api/mail.send.json';
            $sessions = curl_init($requests);
            curl_setopt ($sessions, CURLOPT_POST, true);
            curl_setopt ($sessions, CURLOPT_POSTFIELDS, $paramss);
            curl_setopt($sessions, CURLOPT_HEADER, false);
            curl_setopt($sessions, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($sessions, CURLOPT_RETURNTRANSFER, true);
            $responses = curl_exec($sessions);
            $info=curl_getinfo($sessions);
            curl_close($sessions); 
            
            } 

            
             //Notify Client
            $paramss1 = array(
            'api_user' => $users,
            'api_key' => $passs,            
            'subject' => $subject,
            'html' => $msg_client,
            'from' => $from,
            'fromname' => "Pownder�",
         //   "to"   => $client_email->invoice_email, //"priya.constacloud2@gmail.com"
         //   "toname" =>$client_email->name,            
            'files['.$pdffilename.']'=>'@'.$filepath.'/'.$pdffilename
            );
            $paramss1=http_build_query($paramss1);
            
            $emails=explode(',',$invoice_details[0]->client_email);//     priya.constacloud2@gmail.com
            foreach($emails as $email)
            {
              $paramss1.='&to[]='.$email;  
            }
            $emails=explode(',',$invoice_details[0]->cc_email);//     priya.constacloud2@gmail.com
            foreach($emails as $email)
            {
              $paramss1.='&cc[]='.$email; 
            }
            
            $requests = $urls.'api/mail.send.json';
            $sessions = curl_init($requests);
            curl_setopt ($sessions, CURLOPT_POST, true);
            curl_setopt ($sessions, CURLOPT_POSTFIELDS, $paramss1);
            curl_setopt($sessions, CURLOPT_HEADER, false);
            curl_setopt($sessions, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($sessions, CURLOPT_RETURNTRANSFER, true);
            $responses = curl_exec($sessions);
            $info=curl_getinfo($sessions);
            curl_close($sessions);  
            
            return Redirect::route('payment-confirmation',['id'=>$id]);//->with('alert_success','Payment submitted successfully'); 
            }
            else
            {
              return redirect()->back()->with('alert_danger','Oops! payment couldn\'t be made.');
            }   
          
        }
        //Payment Confirmation Page
        public function paymentConfirmation($id)
        {
            if(Session::has('invoice_data') && session()->has('data') && session()->has('data2'))
            {
            return view('invoice_payment.payment_confirmation',["invoiceno"=>$id]);
            }
            else
            {
                return redirect('/')->with('alert_success','Thank you. You have successfully submitted payment.');
            }   
        }
        // Send Auto Invoice To Clients
        public function send_invoice_auto(Request $request)
        {
            
           ini_set('max_execution_time',0);
           ini_set('memory_limit',-1);
                      
            $invoice=DB::table('invoice')->where([['invoice_id',$request->invoice_id],['invoice_num',$request->invoice_num],['recurring_on',1],['has_recurred',0],['is_deleted',0],['recurring_date','!=','0000-00-00 00:00:00'],[DB::raw('DATE(recurring_date)'),'<=',DB::raw('DATE_ADD(CURDATE(), INTERVAL ( case when ( due_on_receipt<21) then due_on_receipt else 3 end )  DAY)')]])->first();
            
            if(is_null($invoice))
            {
               	return response()->make('{"code":500,"msg":"Invoice not found"}',200,['content-type'=>'application/json']);		   
            }
            
            if($invoice->client_type=="vendor")
            {
                $colname="vendor_id";
                $table="vendors";
            }
            else
            {
                $colname="client_id";
                $table="clients";
            }
            
            //check if client/vendor is active or not
            
            $data=DB::table($table)->where([['id',$invoice->client_id],['is_deleted',0],['status',1]])->first();
            
            if(count($data)==0)
            {
                DB::table('invoice')->where([['invoice_id',$request->invoice_id],['invoice_num',$request->invoice_num]])
                ->update(['recurring_on'=>0]);
                
		      	return response()->make('{"code":404,"msg":"Invoice could not be sent. Client is inactive."}',404,['content-type'=>'application/json']);
		
            }
            /** Get Invoice Num
            */
            $invoice_id=200;$prefix='unknown';
            $invoice_num=DB::table('invoice')->select(DB::raw('max(cast( SUBSTRING_INDEX( invoice_num  ,"-" ,-1) as unsigned )) as invoice_num'))->first();	
            if(count($invoice_num)>0 && $invoice_num->invoice_num)
            {
            $invoice_id=$invoice_num->invoice_num+1;
            }
            $prefix1=explode('-',$invoice->invoice_num,2)[0];
            if($prefix1=="PNAD")
            {
                $prefix="PNRA";
            }
            else if($prefix1=="PNVA")
            {
                $prefix="PNRV";
            }
            else
            {
                $prefix=$prefix1;
            }
            $invoice_num=$prefix.'-'.sprintf('%03d',$invoice_id);
            /** Get Invoice Num
            */
            
            
            /** 
            * Caculating Recurring Date
            */
            
            /*

            $client_data=DB::table($table)->where([['id',$invoice->client_id],['is_deleted',0]])->select('start_date','pro_rate')->first();                            
            
            if(!count($client_data))            
            {
               return response()->make('{"code":404,"msg":"Oops! Invalid client"}',404,['content-type'=>'application/json']);		
            } 
            $pro_rate=$client_data->pro_rate;
            $start_date=$client_data->start_date;
            
            if(!$start_date || !$pro_rate)
            {
                return response()->make('{"code":500,"msg":"Oops! Start Date or Pro Rate not defined"}',500,['content-type'=>'application/json']);  
            }
            if($pro_rate=="Yes")
            {
                $next_recurr_date=date('Y-m-d H:i:s',strtotime('+30 days',strtotime('first day of next month')));                           
            } 
            else if($pro_rate=="No")
            {
                $sDay=date('d',strtotime($start_date));
                $sDate=$sDay.'-'.date('m-Y');
                $next_recurr_date=date('Y-m-d H:i:s',strtotime('+30 days',strtotime($sDate)));                      
            }
            else
            {
                return response()->make('{"code":500,"msg":"Oops! Start Date or Pro Rate not defined"}',500,['content-type'=>'application/json']);  
            }      
            
            $recurr_date=$invoice->recurring_on ? $next_recurr_date :'';
            */
            $next_recurr_date=date('Y-m-d H:i:s',strtotime('+1 month',strtotime($invoice->recurring_date)));
            $recurr_date=$invoice->recurring_on ? $next_recurr_date :'';
            
            $due_on_receipt=$invoice->due_on_receipt; 
            
            //Check if recurring invoice has updated
            
            $recurr_update=DB::table('recurring_invoice')->where([['invoice_id',$request->invoice_id],['invoice_num',$request->invoice_num]])->first(); 
            
            if(count($recurr_update)>0)
            {              
                $invoice->sub_total=$recurr_update->sub_total;
                $invoice->tax_perc=$recurr_update->tax_perc;
                $invoice->tax=$recurr_update->tax;
                $invoice->total=$recurr_update->total;
                $invoice->paid=$recurr_update->paid;
                $invoice->balance=$recurr_update->balance;
            }         
            
            //Save Records
            $invoice_id=DB::table('invoice')->insertGetId([
            "invoice_num"=>$invoice_num,
            "client_id"=>$invoice->client_id,
            "client_type"=>$invoice->client_type,
            "client_email"=>$invoice->client_email,
            "cc_email"=>$invoice->cc_email,
            "company_logo"=>$invoice->company_logo,
            "company_name"=>$invoice->company_name,
            "company_address"=>$invoice->company_address,
            "bill_to"=>$invoice->bill_to,
            "invoice_date"=>date('Y-m-d',strtotime($invoice->recurring_date)),
            "due_date"=>date('Y-m-d',strtotime('+'.$due_on_receipt.' days',strtotime($invoice->recurring_date))),
            "due_on_receipt"=>$due_on_receipt,            
            "recurring_date"=>$recurr_date,
            "sub_total"=>$invoice->sub_total,
            "tax_perc"=>$invoice->tax_perc,
            "tax"=>$invoice->tax,
            "total"=>$invoice->total,
            "paid"=>$invoice->paid,
            "balance"=>$invoice->balance,
            "total_amount"=>$invoice->balance,
            "recurring_on"=>$invoice->recurring_on,            
            "created_user_id"=>$invoice->created_user_id,
            "created_at"=>$this->now            
            ]);            
             //Check if recurring invoice has updated
            if(count($recurr_update)>0)
            {
            $invoices_desc=DB::table('recurring_invoice_description')->where('recurring_id',$recurr_update->id)->orderBy('id','asc')->get(); 
            }
            else
            { 
            $invoices_desc=DB::table('invoice_description')->where('invoice_id',$invoice->invoice_id)->orderBy('id','asc')->get();
            }
            $items=$description=$qty=$rate=$amount=[];
            foreach($invoices_desc as $invoice_desc)
            {
            $items[]=$invoice_desc->item;
            $description[]=$invoice_desc->description;
            $qty[]=$invoice_desc->qty;
            $rate[]=$invoice_desc->rate;
            $amount[]=$invoice_desc->amount;
            }
            
            $values=[];
            for($i=0;$i<count($items);$i++)
            { 
                $values[]=["invoice_id"=>$invoice_id,"item"=>$items[$i],"description"=>$description[$i],"qty"=>$qty[$i],"rate"=>$rate[$i],"amount"=>$amount[$i]];                
            }
            
            DB::table('invoice_description')->insert($values);                        
                        
            $invoice->due_on_receipt=$due_on_receipt;
            //Generate PDF
            $html=View::make('invoice_payment.recurring_invoice_pdf_generate',['request'=>$invoice,"recurr_date"=>$recurr_date,'company_logo'=>$invoice->company_logo,'invoice_num'=>$invoice_num,'items'=>$items,'description'=>$description,'qty'=>$qty,'rate'=>$rate,'amount'=>$amount,'INVOICETNC'=>$this->INVOICETNC])->render();
            $pdffilename= $invoice_num.'.pdf';
            
            PDF::load($html)->filename('pownder/storage/app/uploads/invoice/invoice_pdf/'.$pdffilename)->output(); 
                        
            //email to client
                        
            $from=$this->INVOICEEMAILID;
            $subject=str_ireplace('[name]',$data->name,str_ireplace('[invoice_num]',$invoice_num,$this->INVOICESUBJECT));         
           // $msg=View::make('invoice_payment.invoice_email_template1',['request'=>$request,"email"=>$email,'company_logo'=>$company_logo,'invoice_num'=>$invoice_num,'items'=>$items,'description'=>$description,'qty'=>$qty,'rate'=>$rate,'amount'=>$amount])->render();
             $msg=View::make('invoice_payment.invoice_email_template',['company_name'=>$invoice->company_name,"email"=>$data,'invoice_num'=>$invoice_num,'msg'=>$this->INVOICEMSG, 'neworold'=>'new'])->render();
            
            $filepath=file_get_contents(__DIR__.'/../../../../storage/app/uploads/invoice/invoice_pdf/'.$pdffilename,true); 
            $urls = 'https://api.sendgrid.com/';
            $users = self::SENDGRID_USER;
            $passs = self::SENDGRID_PASS;
            $paramss = array(
            'api_user' => $users,
            'api_key' => $passs,            
            'subject' => $subject,
            'html' => $msg,
            'from' => $from,
            'fromname' => "Pownder�",
            'files['.$pdffilename.']'=>'@'.$filepath.'/'.$pdffilename
            );
            $paramss=http_build_query($paramss);
            $emails=explode(',',$invoice->client_email);
            foreach($emails as $email)
            {
              $paramss.='&to[]='.$email;  
            }
            $emails=explode(',',$invoice->cc_email);//     priya.constacloud2@gmail.com
            foreach($emails as $email)
            {
              $paramss.='&cc[]='.$email; 
            }
            $requests = $urls.'api/mail.send.json';
            $sessions = curl_init($requests);
            curl_setopt ($sessions, CURLOPT_POST, true);
            curl_setopt ($sessions, CURLOPT_POSTFIELDS, $paramss);
            curl_setopt($sessions, CURLOPT_HEADER, false);
            curl_setopt($sessions, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($sessions, CURLOPT_RETURNTRANSFER, true);
            $responses = curl_exec($sessions);
            $info=curl_getinfo($sessions);
            curl_close($sessions);
            
            //Notify Invoice creator
            $admin_email=DB::table('users')->where('id',$invoice->created_user_id)->select('email','name','category','id','admin_id')->first();           
            
            $subject='Recurring Invoice Sent : '.$invoice_num;
            $msg=View::make('invoice_payment.recurring_invoice_sent_notif_template',['invoice_num'=>$invoice_num])->render();                                
               
             /** Find SuperAdmin's Invoice Email */ 
             if($admin_email->category=="admin")
             {
              $admin_email_data=DB::table('admins')->where([['id',$admin_email->admin_id],['is_deleted',0],['status',1]])->select('invoice_email','receive_notification','invoice_option')->first();
              
              if(is_null($admin_email_data))
              {
                $admin_email->email=''; // Admin Deleted Or Inactive
              }
              else if($admin_email_data->invoice_option=='No')
              {
                $admin_email->email=''; // Admin Don't want notification
              }
              else if($admin_email_data->receive_notification=="Everytime")
              {               
                $admin_email->email=$admin_email_data->invoice_email;  // Send Instant Notifcation
              }
              else
              {
                DB::table('admin_notifications')->insert([
                'to'=>$admin_email_data->invoice_email,
                'toname'=>$admin_email->name,
                'from'=>$from,
                'subject'=>$subject,
                'email_body'=>$msg,
                'attachment_path'=>'storage/app/uploads/invoice/invoice_pdf/',
                'attachment_file'=>$pdffilename,
                "created_user_id"=>0,
                "created_user_ip"=>$request->ip(),
                "created_at"=>$this->now,
                'for_person_id'=>$admin_email->admin_id    
                ]);  //Send Later
                $admin_email->email='';
              }
             }
             /** Find SuperAdmin's Invoice Email */ 
              
             if($admin_email->email)
             {
                
            $paramss = array(
            'api_user' => $users,
            'api_key' => $passs,            
            'subject' => $subject,
            'html' => $msg,
            'from' => $from,
            'fromname' => "Pownder�",
            'files['.$pdffilename.']'=>'@'.$filepath.'/'.$pdffilename
            );
            $paramss=http_build_query($paramss);
            
            $emails=explode(',',$admin_email->email);
            
            foreach($emails as $email)
            {
                $paramss.='&to[]='.$email;//"priya.constacloud2@gmail.com"
            }
            
            $requests = $urls.'api/mail.send.json';
            $sessions = curl_init($requests);
            curl_setopt ($sessions, CURLOPT_POST, true);
            curl_setopt ($sessions, CURLOPT_POSTFIELDS, $paramss);
            curl_setopt($sessions, CURLOPT_HEADER, false);
            curl_setopt($sessions, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($sessions, CURLOPT_RETURNTRANSFER, true);
            $responses = curl_exec($sessions);
            $info=curl_getinfo($sessions);
            curl_close($sessions);
            
            }
            //Update Old Recurring Invoice That it has been recurred
            DB::table('invoice')->where([['invoice_id',$invoice->invoice_id],['invoice_num',$invoice->invoice_num],['recurring_on',1],['has_recurred',0],['is_deleted',0]])->update(['has_recurred'=>1,'updated_at'=>$this->now]);
                      
            //Save Pdf File Name For Future Use
            DB::table('invoice')->where([['invoice_id',$invoice_id],['invoice_num',$invoice_num]])->update(['pdffilename'=>$pdffilename,'recurring_notific'=>1]);
            
			return response()->make('{"code":200,"msg":"Invoice added and emailed to client successfully."}',200,['content-type'=>'application/json']);
		
                                            
        }
        public function tmp1()
        {
           
           $invoices=DB::table('invoice')->where([['recurring_on',1],['has_recurred',0],['recurring_date','!=','0000-00-00 00:00:00'],['recurring_date','<=',$this->now]])->get();
           
           foreach($invoices as $invoice)
           {
            $options=[
            CURLOPT_URL=>'http://big.pownder.com/send-invoice-auto?invoice_num='.$invoice->invoice_num.'&invoice_id='.$invoice->invoice_id,
            CURLOPT_HEADER=>false,
            CURLOPT_RETURNTRANSFER=>true
            ]; 
            $curl=curl_init();
            curl_setopt_array($curl,$options);
            $response=curl_exec($curl);
            $http_code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
            print_r($response)                        ;
            print_r($http_code);
           //echo 'Invoice added and emailed to client successfully.';
           }                                 
        }
        //Send Reminder 
        public function sendReminder(Request $request)
        {            
             $getmy_clients='""';
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
                $getmy_clients='""';
             }
             }  

            $invoice_id=$request->invoice_id;
            $invoice_num=$request->invoice_num;
            
            $invoice=DB::table('invoice')
            ->where([['invoice_id',$invoice_id],['invoice_num',$invoice_num],['status',1],['is_deleted',0],['created_user_id',session('user')->id]])
            ->orWhereRaw('invoice_id="'.$invoice_id.'" and invoice_num="'.$invoice_num.'" and status=1 and is_deleted=0 and created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and client_id IN ('.$getmy_clients.') and client_type="client"')
            ->first();
            
            if(count($invoice)!=1)
            {
                return response()->make('{"code":404,"msg":"Invoice not found"}',404,["content-type"=>"application/json"]);
            }            
            
            if($invoice->client_type=="vendor")
            {
                $colname="vendor_id";
                $table="vendors";
            }
            else
            {
                $colname="client_id";
                $table="clients";
            }
            
             
            $email=DB::table($table)->where([['id',$invoice->client_id],["is_deleted",0]])->select('invoice_email','name')->first();            
              
                        
            if(count($email)!=1)
            {
                return response()->make('{"code":404,"msg":"Client not found"}',404,["content-type"=>"application/json"]);
            } 
            
            if($request->has('checkIfAutoSent'))
            {
             /** First check if automated reminder sent today or yesterday */
               $check_auto_send=DB::table('invoice_reminder')->where([["invoice_id",$invoice_id],["invoice_num",$invoice_num],["created_user_id",0],["created_user_category",'cron'],[DB::raw('DATE(updated_at)'),'>=',date('Y-m-d',strtotime('yesterday'))]])->count('id');   
               
                if($check_auto_send)
                {
                return response()->make('{"code":200,"msg":"Already sent through Auto Reminder. Do you want to send anyway?"}',200,["content-type"=>"application/json"]);
                }
                else
                {
                return response()->make('{"code":201,"msg":""}',200,["content-type"=>"application/json"]);
                }
            }
            
            $pdffilename=$invoice->pdffilename;
            
            /** Calculate Due By Days */
            $due_days=0;
            $date1=date_create(date('Y-m-d'));
            $date2=date_create($invoice->due_date);    
            $diff=date_diff($date1,$date2); 
            $due_days=$diff->format('%a');
            /** Calculate Due By Days */
                                
            //email to client
            
            $key_values=['[name]'=>$email->name,'[invoice_num]'=>$invoice_num,'[due_date]'=>date('m/d/Y',strtotime($invoice->due_date)),'[Amount]'=>$this->money_format_view($invoice->balance),'[Company_Name]'=>$invoice->company_name,'[Company_Address]'=>nl2br($invoice->company_address),'[Due_Days]'=>$due_days];
                        
           $reminder_msg=$this->replaceKeywords($this->REMINDERMSG,$key_values);            
    
            $from=$this->INVOICEEMAILID;
       //     $subject='Reminder:'.$request->send_reminder_subject; 
            $subject=$this->replaceKeywords($this->REMINDERSUBJECT,$key_values);            
            $msg=View::make('invoice_payment.reminder_email_template',['company_name'=>$invoice->company_name,"email"=>$email,'invoice_num'=>$invoice_num,"msg"=>$reminder_msg])->render();//$request->send_reminder_msg
            
            $filepath=file_get_contents(__DIR__.'/../../../../storage/app/uploads/invoice/invoice_pdf/'.$pdffilename,true); 
            $urls = 'https://api.sendgrid.com/';
            $users = self::SENDGRID_USER;
            $passs = self::SENDGRID_PASS;
            $paramss = array(
            'api_user' => $users,
            'api_key' => $passs,            
            'subject' => $subject,
            'html' => $msg,
            'from' => $from,
            'fromname' => "Pownder�",
            'files['.$pdffilename.']'=>'@'.$filepath.'/'.$pdffilename
            );
            $paramss=http_build_query($paramss);
            $emails=explode(',',$invoice->client_email);//     priya.constacloud2@gmail.com
            foreach($emails as $email)
            {
              $paramss.='&to[]='.$email;  
            }
            $emails=explode(',',$invoice->cc_email);//     priya.constacloud2@gmail.com
            foreach($emails as $email)
            {
              $paramss.='&cc[]='.$email; 
            }
            $requests = $urls.'api/mail.send.json';
            $sessions = curl_init($requests);
            curl_setopt ($sessions, CURLOPT_POST, true);
            curl_setopt ($sessions, CURLOPT_POSTFIELDS, $paramss);
            curl_setopt($sessions, CURLOPT_HEADER, false);
            curl_setopt($sessions, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($sessions, CURLOPT_RETURNTRANSFER, true);
            $responses = curl_exec($sessions);
            $info=curl_getinfo($sessions);
            curl_close($sessions);
            
            //Save Reminder Log
            DB::table('invoice_reminder')->insert(["invoice_id"=>$invoice_id,"invoice_num"=>$invoice_num,"client_name"=>$request->billto,/*"subject"=>$subject,"msg"=>$reminder_msg,*/"created_user_id"=>session('user')->id,"created_user_category"=>session('user')->category,"created_at"=>$this->now]);
			
            return response()->make('{"code":200,"msg":"Invoice added and emailed to client successfully."}',200,['content-type'=>'application/json']);
		
                                            
        }
        //Update recurring on off
        public function updateRecurringStatus(Request $request)
        {
             $getmy_clients='""';
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
                $getmy_clients='""';
             }
             }       
             
            //Get Pro rate and Start date for clients and vendors
            $invoice_data=DB::table('invoice')->where([
            ['invoice_id',$request->invoice_id],
            ['invoice_num',$request->invoice_num],
            ['is_deleted',0],
            ['created_user_id',session('user')->id]
            ])
            ->orWhereRaw('invoice_id="'.$request->invoice_id.'" and invoice_num="'.$request->invoice_num.'" and is_deleted=0 and created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and client_id IN ('.$getmy_clients.') and client_type="client"')
            ->select('client_id','client_type')            
            ->first();
            
            if(!count($invoice_data))
            {
              return response()->make('{"code":404,"msg":"Oops! Invalid invoice data"}',404,['content-type'=>'application/json']);  
            }
            if($invoice_data->client_type=="vendor")
            {
                $table='vendors';
            }
            else
            {
                  $table="clients";
            }
             /** 
            * Caculating Recurring Date
            */
            
            $client_data=DB::table($table)->where([['id',$invoice_data->client_id],['is_deleted',0],['status',1]])->select('start_date','pro_rate')->first();                            
            
            if(!count($client_data))            
            {
               return response()->make('{"code":404,"msg":"Oops! Invalid client"}',404,['content-type'=>'application/json']);		
            } 
            $pro_rate=$client_data->pro_rate;
            $start_date=$client_data->start_date;
            
            if(!$start_date || !$pro_rate)
            {
                return response()->make('{"code":500,"msg":"Oops! Start Date or Pro Rate not defined"}',500,['content-type'=>'application/json']);  
            }
            if($pro_rate=="Yes")
            {
                $next_recurr_date=date('Y-m-d H:i:s',strtotime('first day of next month'));                           
            } 
            else if($pro_rate=="No")
            {               
                $next_recurr_date=date('Y-m-d H:i:s',strtotime('+1 month'));                      
            }
            else
            {
                return response()->make('{"code":500,"msg":"Oops! Start Date or Pro Rate not defined"}',500,['content-type'=>'application/json']);  
            }              
                        
            $recurr_date=$request->newstatus ?  $next_recurr_date :'';
            
            $old_status=$request->newstatus ? 0 : 1;
            
            DB::table('invoice')->where([
            ['invoice_id',$request->invoice_id],
            ['invoice_num',$request->invoice_num],
            ['created_user_id',session('user')->id],
            ['is_deleted',0],
            ['recurring_on',$old_status]
            ])
            ->orWhereRaw('invoice_id="'.$request->invoice_id.'" and invoice_num="'.$request->invoice_num.'" and created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and client_id IN ('.$getmy_clients.') and client_type="client" and recurring_on="'.$old_status.'"')
            ->update([
              'recurring_on'=>$request->newstatus,
              'recurring_date'=>$recurr_date,
              'updated_at'=>$this->now
            ]);
            
            return response()->make('{"code":200,"msg":"Invoice recurring status updated successfully"}',200,['content-type'=>'application/json']);
        }
        //Update Recurring invoice
        public function updateRecurringInvoice(Request $request)
        {
         $rule=[               
            "item.*"=>"required",
			'description.*' => 'nullable',
			'qty.*' => 'required|numeric',
			'rate.*' => 'required|numeric',
			'amount.*' => 'required|numeric',
            "sub_total"=>"required|numeric",            
            "tax_perc"=>"nullable|numeric",              
            "tax_value"=>"required|numeric",
            "total"=>"required|numeric",
            "paid"=>"required|numeric",
            "balance"=>"required|numeric"						
			];
            // validate data            
            $this->validate($request,$rule);
            
            $getmy_clients='""';
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
                $getmy_clients='""';
             }
             }     
            
           $invoice_data=DB::table('invoice')
           ->where([['invoice_id',$request->invoice_id],['invoice_num',$request->invoice_num],['is_deleted',0],['created_user_id',session('user')->id],['has_recurred','!=',1],['recurring_on',1]])
           ->orWhereRaw('invoice_id="'.$request->invoice_id.'" and invoice_num="'.$request->invoice_num.'" and is_deleted=0 and created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and client_id IN ('.$getmy_clients.') and client_type="client" and has_recurred!=1 and recurring_on=1')         
           ->first();
            
            if(count($invoice_data)==0)
            {
                return redirect()->back()->with('alert_danger','Oops! Invalid invoice data');
            }
            $invoice_id=$request->invoice_id;
            $invoice_num=$request->invoice_num;
            
            //Check Already Exists
            $recurring_invoice=DB::table('recurring_invoice')->where([['invoice_id',$invoice_id],['invoice_num',$invoice_num]])->first();
            
            if(count($recurring_invoice)==0)
            {
            //Save Records For Next Recurring Date
            $recurring_id=DB::table('recurring_invoice')->insertGetId([            
            "invoice_id"=>$invoice_id,
            "invoice_num"=>$invoice_num,            
            "sub_total"=>$request->sub_total,
            "tax_perc"=>$request->tax_perc,
            "tax"=>$request->tax_value,
            "total"=>$request->total,
            "paid"=>$request->paid,
            "balance"=>$request->balance,
            "total_amount"=>$request->balance,        
            "created_user_id"=>session('user')->id,
            "created_at"=>$this->now            
            ]);            
            
            
            $items=$request->item;
            $description=$request->description;
            $qty=$request->qty;
            $rate=$request->rate;
            $amount=$request->amount;
            
            $values=[];
            for($i=0;$i<count($items);$i++)
            { 
                $values[]=["recurring_id"=>$recurring_id,"item"=>$items[$i],"description"=>$description[$i],"qty"=>$qty[$i],"rate"=>$rate[$i],"amount"=>$amount[$i]];
                //Save item name and description for future use
                $item_detail=DB::table('invoice_saved_items')->where([['item',$items[$i]],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->select('id')->first();     
                if(count($item_detail)==0)
                {
                   DB::table('invoice_saved_items')->insert(['item'=>$items[$i],'description'=>$description[$i],'created_user_id'=>session('user')->id,'created_user_category'=>session('user')->category,'created_at'=>$this->now]); 
                }
                else
                {
                    DB::table('invoice_saved_items')->where([['id',$item_detail->id],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->update(['description'=>$description[$i],'updated_at'=>$this->now]);
                }
            }
            
            DB::table('recurring_invoice_description')->insert($values);           
           
           }
           else
           {
            //Update Records For Next Recurring Date
            $recurring_id=$recurring_invoice->id;
            DB::table('recurring_invoice')->where([['id',$recurring_id],['invoice_id',$invoice_id],['invoice_num',$invoice_num]])
            ->update([            
            "sub_total"=>$request->sub_total,
            "tax_perc"=>$request->tax_perc,
            "tax"=>$request->tax_value,
            "total"=>$request->total,
            "paid"=>$request->paid,
            "balance"=>$request->balance,
            "total_amount"=>$request->balance,
            "updated_at"=>$this->now            
            ]);            
            
            
            $items=$request->item;
            $description=$request->description;
            $qty=$request->qty;
            $rate=$request->rate;
            $amount=$request->amount;
            
            $values=[];
            for($i=0;$i<count($items);$i++)
            { 
                $values[]=["recurring_id"=>$recurring_id,"item"=>$items[$i],"description"=>$description[$i],"qty"=>$qty[$i],"rate"=>$rate[$i],"amount"=>$amount[$i]];
                //Save item name and description for future use
                $item_detail=DB::table('invoice_saved_items')->where([['item',$items[$i]],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->select('id')->first();     
                if(count($item_detail)==0)
                {
                   DB::table('invoice_saved_items')->insert(['item'=>$items[$i],'description'=>$description[$i],'created_user_id'=>session('user')->id,'created_user_category'=>session('user')->category,'created_at'=>$this->now]); 
                }
                else
                {
                    DB::table('invoice_saved_items')->where([['id',$item_detail->id],['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->update(['description'=>$description[$i],'updated_at'=>$this->now]);
                }
            }
            DB::table('recurring_invoice_description')->where('recurring_id',$recurring_id)->delete();
            DB::table('recurring_invoice_description')->insert($values);          
           
           }
           return redirect()->route('manage-invoice')->with('alert_success','Recurring invoice updated successfully.');   
        }
        //Filter Invoices Along With Date Range in Manage Invoice
        public function filterInvoices(Request $request)
        {
         
           $type=$request->type;          
           $filter_type=$request->filter_type;
           $filter=$request->filter; 
           
            if($type=="all")
            {                       
            $invoices=$this->getAllInvoices($filter,$filter_type);           
            }
            else if($type=="sent")
            {
               $invoices=$this->getSentInvoices($filter,$filter_type);
            }
            else if($type=="overdue")
            {
              $invoices=$this->getOverdueInvoices($filter,$filter_type);           
            }
            else if($type=="draft")
            {
              $invoices=$this->getDraftInvoices($filter,$filter_type); 
            }
            else if($type=="paid")
            {
             $invoices=$this->getPaidInvoices($filter,$filter_type); 
            } 
            else if($type=="paid_by_cheque")
            {
             $payment_mode="by_admin";
             $invoices=$this->getPaidInvoices($filter,$filter_type,$payment_mode); 
            } 
            else if($type=="paid_by_bank")
            {
             $payment_mode="bank";
             $invoices=$this->getPaidInvoices($filter,$filter_type,$payment_mode); 
            }
            else if($type=="paid_by_ccard")
            {
             $payment_mode="ccard";
             $invoices=$this->getPaidInvoices($filter,$filter_type,$payment_mode); 
            } 
            else if($type=="paid_by_dcard")
            {
             $payment_mode="dcard";
             $invoices=$this->getPaidInvoices($filter,$filter_type,$payment_mode); 
            }                   
            else
            {
              return '<tr><td class="text-center" colspan="7">Oops! Invalid invoice type</td></tr>';  
            }
            $msg='';$k=0;
            foreach($invoices as $invoice)
            {
                            $duein='-';
                            if($invoice->due_date!="0000-00-00")
                            {
                            $date1=date_create(date('Y-m-d'));
                            $date2=date_create($invoice->due_date);    
                            $diff=date_diff($date1,$date2); 
                            $duein=$diff->format('%r%a');
                            }
                            
                            $msg.='<tr>
                                <td>';
                                  $balance_color='';$invoice_num='';$due_status='';
                                 if($invoice->invoice_type=="draft")
                                 { 
                            $msg.='<button class="btn btn-info bg-white text-info  small"><small>Draft</small></button>';
                                    $balance_color='text-info';$invoice_num='DRFT-'.sprintf('%03d',$invoice->invoice_num);$due_status=($duein>=0) ? ' - Due in '.abs($duein).' days' : ' - overdue by '.abs($duein).' days';
                                 }
                                 elseif($invoice->invoice_type=="sent") 
                                 {  
                                 $msg.='<button class="btn btn-success bg-white text-success small"><small>Sent</small></button>';
                                 $balance_color='text-success';$invoice_num=$invoice->invoice_num;$due_status=' - Due in '.abs($duein).' days';
                                 }
                                 elseif($invoice->invoice_type=="paid") 
                                 {  
                                 $msg.='<button class="btn btn-primary bg-white text-primary  small"><small>Paid</small></button>'; 
                                 $balance_color='text-primary';$invoice_num=$invoice->invoice_num;$due_status=' - '.sprintf('CNF%05d',$invoice->id);
                                 }
                                 elseif($invoice->invoice_type=="overdue")
                                 {   
                                 $msg.='<button class="btn btn-danger bg-white text-danger  small"><small>Over<br/>due</small></button>'; 
                                 $balance_color='text-danger';$invoice_num=$invoice->invoice_num;$due_status=' - overdue by '.abs($duein).' days';
                                 }   
                                $msg.='</td>
                                <td><strong class="bill_to lead text-black">'.$invoice->client_name.'</strong>
                                <br />
                                <span class="text-muted small">'.date('F d, Y',strtotime($invoice->created_at)).$due_status.'</span>
                                </td>
                                <td>'.$invoice->client_email.'</td>
                                <td>
                                <span><strong class="lead '.$balance_color.'">$'.$this->money_format1($invoice->balance).'</strong></span>
                                <br /><span class="text-muted small">'.$invoice_num.'</span>
                                </td>
                                <td>';
                                
                                   if($invoice->recurring_on!=="na" and $invoice->has_recurred!==1)
                                   {
                                   $msg.='<label class="switch ib switch-custom text-center">
                					<input type="checkbox" class="recurring-status" id="include_tax'.++$k.'"';if($invoice->recurring_on){$msg.='  checked="" ';}$msg.=' />
                					<label for="include_tax'.$k.'" data-on="ON" data-off="OFF" data-num="'.$invoice->invoice_num.'" data-id="'.$invoice->invoice_id.'"></label>
                					<span class="switch_label">Recurring</span>
    					           </label>';                                     
                                   }
                                $msg.='</td><td>';
                                
                                if(session('user')->category!="admin" and $invoice->created_user_id!=session('user')->id)
                                 {
                                 $msg.='<label class="badge">By Admin</label>';
                                 }
                                
                                $msg.='</td><td class="text-center">';                                
                                 if($invoice->invoice_type=="draft")
                                 {
                                 $msg.='
                                 <a href="#" data-id="'.$invoice->id.'" data-invoice-type="draft" class="delete-invoice" title="Delete Invoice"><i class="fa fa-trash-o text-info"></i></a>
                                 <a href="'.route('draft-invoice',['id'=>$invoice->id]).'"><i class="fa fa-repeat text-info" title="Continue Invoice"></i></a>';
                                 }
                                 elseif($invoice->invoice_type=="sent")
                                 {
                                 $msg.='<a href="'.route('edit-sent-invoice',['id'=>$invoice->invoice_id]).'"><i class="fa fa-edit text-success" title="Edit Invoice"></i></a>';
                                
                                 $msg.='<a href="#" data-num="'.$invoice->invoice_num.'" data-id="'.$invoice->invoice_id.'" data-email="'.$invoice->client_email.'" data-ccemail="'.$invoice->cc_email.'" class="send-payment-reminder-direct" title="Send Payment Reminder"><i class="fa fa-envelope text-success"></i></a>
                                        <a href="#" data-num="'.$invoice->invoice_num.'" data-id="'.$invoice->invoice_id.'" class="mark-as-paid" title="Mark Invoice As Paid"><i class="fa fa-check-square-o text-success"></i></a>
                                        <a href="#" data-id="'.$invoice->invoice_id.'" data-invoice-type="sent" class="delete-invoice" title="Delete Invoice"><i class="fa fa-trash-o text-success"></i></a>
                                        <a href="'.route('view-invoice',['id'=>$invoice->id,'type'=>$invoice->invoice_type]).'"><i class="fa fa-file-pdf-o text-success" title="View Invoice"></i></a>';
                                 }
                                 elseif($invoice->invoice_type=="paid")
                                 {
                                 $msg.='<a href="#" data-num="'.$invoice->invoice_num.'" data-id="'.$invoice->invoice_id.'" data-invoice-type="paid" class="clone-invoice" title="Clone Invoice"><i class="fa fa-clone text-primary"></i></a>
                                        <a href="#" data-id="'.$invoice->invoice_id.'" data-invoice-type="paid" class="delete-invoice" title="Delete Invoice"><i class="fa fa-trash-o text-primary"></i></a>
                                        <a href="'.route('view-invoice',['id'=>$invoice->id,'type'=>$invoice->invoice_type]).'"><i class="fa fa-file-pdf-o text-primary" title="View Invoice"></i></a>';
                                 }
                                 elseif($invoice->invoice_type=="overdue")
                                 {                                    
                               
                                 $msg.='<a href="'.route('edit-sent-invoice',['id'=>$invoice->invoice_id]).'"><i class="fa fa-edit text-danger" title="Edit Invoice"></i></a>';
                                
                                 $msg.='<a href="#" data-num="'.$invoice->invoice_num.'" data-id="'.$invoice->invoice_id.'" data-email="'.$invoice->client_email.'" data-ccemail="'.$invoice->cc_email.'" class="send-payment-reminder-direct" title="Send Payment Reminder"><i class="fa fa-envelope text-danger"></i></a>
                                         <a href="#" data-id="'.$invoice->invoice_id.'" data-invoice-type="sent" class="delete-invoice" title="Delete Invoice"><i class="fa fa-trash-o text-danger"></i></a>
                                         <a href="'.route('view-invoice',['id'=>$invoice->id,'type'=>$invoice->invoice_type]).'"><i class="fa fa-file-pdf-o text-danger" title="View Invoice"></i></a>';  
                                 }                                 
                              $msg.='</td>
                            </tr>';
            }
            if(!$msg)
            {
                $msg='<tr><td class="text-center" colspan="7">No matching records found</td></tr>';
            }         
            
            return $msg;
           
        }       
        
        //Mark Invoice As Paid
        public function markInvoicePaid(Request $request)
        { 
           
             $rule=["invoice_num"=>"required","invoice_id"=>"required|numeric","cheque.*"=>"nullable|image"];
             $validation=$this->validate($request,$rule , ["image"=>"Cheque files must be of image type only."]); 
             
             
             $getmy_clients='""';
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
                $getmy_clients='""';
             }
             }
             
            $invoice_num=$request->invoice_num;
            $invoice_id=$request->invoice_id;
            
            $invoice_details=DB::table('invoice as A')->join('invoice_description as B','A.invoice_id','=','B.invoice_id')
            ->where([['A.invoice_id',$invoice_id],['A.invoice_num',$invoice_num],['A.status',1],['A.is_deleted',0],['A.created_user_id',session('user')->id]])
            ->orWhereRaw('A.invoice_id="'.$invoice_id.'" and A.invoice_num="'.$invoice_num.'" and A.status=1 and A.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client"')
            ->select('A.*','B.item','B.description','B.qty','B.rate','B.amount')->orderBy('B.id','asc')->get();
            
            if(count($invoice_details)==0)
            {
              return response()->make('{"code":500,"msg":"Oops! invalid request."}',500,["content-type"=>'application/json']);
            }
            $cheques_images=[];
            if($request->hasFile('cheque'))
            {
                $files=$request->file('cheque');
               
                foreach($files as $file)
                {                                  
                    $filename=$file->getClientOriginalName();
                    $filename=date('U').$filename;
                    $file->move('pownder/storage/app/uploads/invoice/cheque_images/',$filename);
                    $cheques_images[]=$filename;                                   
                }
            }
            $cheques_images=implode(',',$cheques_images);
            
             //Save Records
            $pay_id=DB::table('paid_invoices')->insertGetId([
            "invoice_id"=>$invoice_details[0]->invoice_id,
            "invoice_num"=>$invoice_details[0]->invoice_num,
            "client_id"=>$invoice_details[0]->client_id,
            "client_type"=>$invoice_details[0]->client_type,
            "client_email"=>$invoice_details[0]->client_email,
            "cc_email"=>$invoice_details[0]->cc_email,
            "company_logo"=>$invoice_details[0]->company_logo,
            "company_name"=>$invoice_details[0]->company_name,
            "company_address"=>$invoice_details[0]->company_address,
            "bill_to"=>$invoice_details[0]->bill_to,
            "invoice_date"=>$invoice_details[0]->invoice_date,
            "due_date"=>$invoice_details[0]->due_date,
            "due_on_receipt"=>$invoice_details[0]->due_on_receipt,
            "recurring_date"=>$invoice_details[0]->recurring_date,
            "payment_date"=>date('Y-m-d'),
            "sub_total"=>$invoice_details[0]->sub_total,
            "tax_perc"=>$invoice_details[0]->tax_perc,
            "tax"=>$invoice_details[0]->tax,
            "total"=>$invoice_details[0]->total,
            "paid"=>$invoice_details[0]->paid,
            "balance"=>$invoice_details[0]->balance,
            "total_amount"=>$invoice_details[0]->total_amount,
            "pdffilename"=>$invoice_details[0]->pdffilename,
            "cheque_images"=>$cheques_images,
            "pay_method_type"=>'by_admin',
            "created_at"=>$this->now,
            "updated_at"=>$this->now            
            ]);
            
            $values=[];
            for($i=0;$i<count($invoice_details);$i++)
            {
                $item=$invoice_details[$i]->item;
                $description=$invoice_details[$i]->description;
                $qty=$invoice_details[$i]->qty;
                $rate=$invoice_details[$i]->rate;
                $amount=$invoice_details[$i]->amount;
                $values[]=["pay_id"=>$pay_id,"item"=>$item,"description"=>$description,"qty"=>$qty,"rate"=>$rate,"amount"=>$amount];
            }
            
            DB::table('paid_invoice_description')->insert($values);            
            
            DB::table('invoice')
            ->where([['invoice_num',$invoice_num],['invoice_id',$invoice_id],['status',1],['created_user_id',session('user')->id]])
            ->orWhereRaw('invoice_num="'.$invoice_num.'" and invoice_id="'.$invoice_id.'" and status=1 and created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and client_id IN ('.$getmy_clients.') and client_type="client"')
            ->update(['status'=>0,"payment_date"=>$this->now]);
            
            Session::flash('alert_success','Invoice marked as paid.');
           return response()->make('{"code":200,"msg":"Invoice marked as paid."}',200,["content-type"=>'application/json']);
            
          
        }
        /** 
         * Invoice Reports 
         */
          
        //Sales Report By Day
        public function sales_by_day($report_for_month_f,$report_for_month,$report_for_year,$num_of_days_in_month)
        {        
         $getmy_clients='""';
         
         if(session('user')->category=="vendor")
         {
         $request1=new  Request();
         $request1->type="client";                     
         $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
         $getmy_clients=array_column($data,'id');
         if(count($getmy_clients)>0)
         {
         $getmy_clients=implode(',',$getmy_clients);
         }
         else
         {
            $getmy_clients='""';
         }         
         }
         
         $sales_by_day_sent=DB::table('invoice As A')
          ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
          ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.'  and MONTH(A.invoice_date)='.$report_for_month.' and YEAR(A.invoice_date)='.$report_for_year.' and A.status=1 and A.is_deleted=0')
          ->orWhereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and MONTH(A.invoice_date)='.$report_for_month.' and YEAR(A.invoice_date)='.$report_for_year.' and A.status=1 and A.is_deleted=0')
          ->select('A.invoice_date',DB::raw('sum(A.total_amount) as total,sum(A.tax) as tax,count(A.invoice_id) as count_invoices'))
          ->groupBy('A.invoice_date')
          ->orderBy('A.invoice_date','asc')
          ->get();
          
          $sales_by_day_onlypaid=DB::table('invoice As A')
           ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })         
          ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and MONTH(A.payment_date)='.$report_for_month.' and YEAR(A.payment_date)='.$report_for_year.' and A.status=0 and A.is_deleted=0')
          ->orWhereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and MONTH(A.payment_date)='.$report_for_month.' and YEAR(A.payment_date)='.$report_for_year.' and A.status=0 and A.is_deleted=0')
          
          ->select(DB::raw('DATE(A.payment_date) as payment_date1,sum(A.total_amount) as total,sum(A.tax) as tax'))
          ->groupBy(DB::raw('DATE(A.payment_date)'))
          ->orderBy('payment_date1','asc')
          ->get(); 
          
          $sales_by_day_sent=json_decode(json_encode($sales_by_day_sent),true);
          $sales_by_day_onlypaid=json_decode(json_encode($sales_by_day_onlypaid),true);
          
          $sales_by_day_sale=array_column($sales_by_day_sent,'total','invoice_date');
          $sales_by_day_paid=array_column($sales_by_day_onlypaid,'total','payment_date1');
          $sales_by_day_tax=array_column($sales_by_day_sent,'tax','invoice_date');
          $sales_by_day_tax_paid=array_column($sales_by_day_onlypaid,'tax','payment_date1');
          $sales_by_day=[];
           
         $num_of_days_in_month=$num_of_days_in_month;
          for($i=0;$i<$num_of_days_in_month;$i++)
          {
             $date_sale=$report_for_year.'-'.$report_for_month_f.'-'.sprintf('%02d',$i+1);
             $sales_by_day[$date_sale]=(!array_key_exists($date_sale,$sales_by_day_sale)) ? ["unpaid"=>0] :["unpaid"=>$sales_by_day_sale[$date_sale]];
             $sales_by_day[$date_sale]["paid"]=(!array_key_exists($date_sale,$sales_by_day_paid)) ? 0 :$sales_by_day_paid[$date_sale];
             $sales_by_day[$date_sale]["tax"]=(!array_key_exists($date_sale,$sales_by_day_tax)) ? 0 :$sales_by_day_tax[$date_sale];
             $sales_by_day[$date_sale]["tax"]=(!array_key_exists($date_sale,$sales_by_day_tax_paid)) ? $sales_by_day[$date_sale]["tax"] :$sales_by_day[$date_sale]["tax"]+$sales_by_day_tax_paid[$date_sale];
          }
           ksort($sales_by_day);
           
           return $sales_by_day;
        }
        //Sales Report By Month
        public function sales_by_month($report_for_year,$number_of_passed_months)
        {        
         $getmy_clients='""';
         
         if(session('user')->category=="vendor")
         {
         $request1=new  Request();
         $request1->type="client";                     
         $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
         $getmy_clients=array_column($data,'id');
         if(count($getmy_clients)>0)
         {
         $getmy_clients=implode(',',$getmy_clients);
         }
         else
         {
           $getmy_clients='""';
         }        
         }
         
         $sales_by_day_unpaid=DB::table('invoice As A')
          ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })          
          ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and YEAR(A.invoice_date)='.$report_for_year.' and A.status=1 and A.is_deleted=0 OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and YEAR(A.invoice_date)='.$report_for_year.' and A.status=1 and A.is_deleted=0')
          ->select(DB::raw('MONTH(A.invoice_date) as month,sum(A.total_amount) as total,sum(A.tax) as tax,count(A.invoice_id) as count_invoices'))
          ->groupBy(DB::raw('MONTH(A.invoice_date)'))
          ->orderBy('month','asc')
          ->get();
          
          $sales_by_day_onlypaid=DB::table('invoice As A')
          ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                }) 
          ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and YEAR(A.payment_date)='.$report_for_year.' and A.status=0 and A.is_deleted=0 OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and YEAR(A.payment_date)='.$report_for_year.' and A.status=0 and A.is_deleted=0')
          ->select(DB::raw('MONTH(A.payment_date) as month,sum(A.total_amount) as total,sum(A.tax) as tax'))
          ->groupBy(DB::raw('MONTH(A.payment_date)'))
          ->orderBy('month','asc')
          ->get();
          
          $sales_by_day_unpaid=json_decode(json_encode($sales_by_day_unpaid),true);
          $sales_by_day_onlypaid=json_decode(json_encode($sales_by_day_onlypaid),true);
          
          $sales_by_day_sale_unpaid=array_column($sales_by_day_unpaid,'total','month');
          $sales_by_day_paid=array_column($sales_by_day_onlypaid,'total','month');
          $sales_by_day_tax_unpaid=array_column($sales_by_day_unpaid,'tax','month');
          $sales_by_day_tax_paid=array_column($sales_by_day_onlypaid,'tax','month');
          $sales_by_day=[];
           
          $number_of_passed_months=$number_of_passed_months; 
          for($i=0;$i<$number_of_passed_months;$i++)
          {
            
             $month_sale=$i+1;
             $sales_by_day[$month_sale]=(!array_key_exists($month_sale,$sales_by_day_sale_unpaid)) ? ["overall"=>0] :["overall"=>$sales_by_day_sale_unpaid[$month_sale]];
             $sales_by_day[$month_sale]["overall"]=(!array_key_exists($month_sale,$sales_by_day_paid)) ? $sales_by_day[$month_sale]["overall"] :$sales_by_day[$month_sale]["overall"]+$sales_by_day_paid[$month_sale];            
             $sales_by_day[$month_sale]["paid"]=(!array_key_exists($month_sale,$sales_by_day_paid)) ? 0 :$sales_by_day_paid[$month_sale];
             $sales_by_day[$month_sale]["tax"]=(!array_key_exists($month_sale,$sales_by_day_tax_unpaid)) ? 0 :$sales_by_day_tax_unpaid[$month_sale];
             $sales_by_day[$month_sale]["tax"]=(!array_key_exists($month_sale,$sales_by_day_tax_paid)) ? $sales_by_day[$month_sale]["tax"] :$sales_by_day[$month_sale]["tax"]+$sales_by_day_tax_paid[$month_sale];
           
          }
           ksort($sales_by_day);
           $year=array_pad([$report_for_year],count($sales_by_day),$report_for_year);
          
           $keys=array_map([$this,"MapDateYear"],array_keys($sales_by_day),$year);
           $sales_by_day=array_combine($keys,array_values($sales_by_day));
           
           return $sales_by_day;
        }
        //Sales Report By 
        public function sales_by_quarter($report_for_year,$number_of_passed_months)
        {        
         if($number_of_passed_months%3==0)
         {
         $number_of_quarters=$number_of_passed_months/3;
         }
         else if($number_of_passed_months<3)
         {
            $number_of_quarters=1;
         }
         else if($number_of_passed_months<6)
         {
            $number_of_quarters=2;
         }
         else if($number_of_passed_months<9)
         {
            $number_of_quarters=3;
         }
         else if($number_of_passed_months<12)
         {
            $number_of_quarters=4;
         }
         $condition1=$condition2=$condition3=$condition4='';
         
          $getmy_clients='""';
         
         if(session('user')->category=="vendor")
         {
         $request1=new  Request();
         $request1->type="client";                     
         $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
         $getmy_clients=array_column($data,'id');
         if(count($getmy_clients)>0)
         {
         $getmy_clients=implode(',',$getmy_clients);
         }
         else
         {
            $getmy_clients='""';
         }                  
         }
         
         switch($number_of_quarters)
         {
            case "1": $condition1=" and MONTH(A.invoice_date) IN (1,2,3)";$condition1A=" and MONTH(A.payment_date) IN (1,2,3)";break;
            case "2": $condition1=" and MONTH(A.invoice_date) IN (1,2,3)";$condition1A=" and MONTH(A.payment_date) IN (1,2,3)";$condition2=" and MONTH(A.invoice_date) IN (4,5,6)";$condition2A=" and MONTH(A.payment_date) IN (4,5,6)";break;
            case "3": $condition1=" and MONTH(A.invoice_date) IN (1,2,3)";$condition1A=" and MONTH(A.payment_date) IN (1,2,3)";$condition2=" and MONTH(A.invoice_date) IN (4,5,6)";$condition2A=" and MONTH(A.payment_date) IN (4,5,6)";$condition3=" and MONTH(A.invoice_date) IN (7,8,9)";$condition3A=" and MONTH(A.payment_date) IN (7,8,9)";break;
            case "4": $condition1=" and MONTH(A.invoice_date) IN (1,2,3)";$condition1A=" and MONTH(A.payment_date) IN (1,2,3)";$condition2=" and MONTH(A.invoice_date) IN (4,5,6)";$condition2A=" and MONTH(A.payment_date) IN (4,5,6)";$condition3=" and MONTH(A.invoice_date) IN (7,8,9)";$condition3A=" and MONTH(A.payment_date) IN (7,8,9)";$condition4=" and MONTH(A.invoice_date) IN (10,11,12)";$condition4A=" and MONTH(A.payment_date) IN (10,11,12)";break;
            default:  $condition1=" and MONTH(A.invoice_date) IN (1,2,3)";$condition1A=" and MONTH(A.payment_date) IN (1,2,3)";$condition2=" and MONTH(A.invoice_date) IN (4,5,6)";$condition2A=" and MONTH(A.payment_date) IN (4,5,6)";$condition3=" and MONTH(A.invoice_date) IN (7,8,9)";$condition3A=" and MONTH(A.payment_date) IN (7,8,9)";$condition4=" and MONTH(A.invoice_date) IN (10,11,12)";$condition4A=" and MONTH(A.payment_date) IN (10,11,12)";break;
        
         }
         $data=[];
         if($condition1 || $condition2 || $condition3 || $condition4)
         {
         $sales_by_day_unpaid=DB::table('invoice As A')
           ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
         ->whereRaw('A.created_user_id='.session('user')->id.' and YEAR(A.invoice_date)='.$report_for_year.' '.$condition1.' and A.status=1 and A.is_deleted=0 OR A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and YEAR(A.invoice_date)='.$report_for_year.' '.$condition1.' and A.status=1 and A.is_deleted=0')
          ->select(DB::raw('sum(A.total_amount) as total,sum(A.tax) as tax,count(A.invoice_id) as count_invoices'))
          ->first();
          
          $sales_by_day_onlypaid=DB::table('invoice As A')
            ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
          ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and YEAR(A.payment_date)='.$report_for_year.' '.$condition1A.' and A.status=0 and A.is_deleted=0 OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and YEAR(A.payment_date)='.$report_for_year.' '.$condition1A.' and A.status=0 and A.is_deleted=0')
          ->select(DB::raw('sum(A.total_amount) as total,sum(A.tax) as tax'))
          ->first();          
          
          $sales_by_day_sale=$sales_by_day_unpaid->total+$sales_by_day_onlypaid->total;
          $sales_by_day_paid=$sales_by_day_onlypaid->total;
          $sales_by_day_tax=$sales_by_day_unpaid->tax+$sales_by_day_onlypaid->tax;
          $data["q1_".$report_for_year]=["overall"=>$sales_by_day_sale,"paid"=>$sales_by_day_paid,"tax"=>$sales_by_day_tax];
         } 
         if($condition2 || $condition3 || $condition4)
         {
         $sales_by_day_unpaid=DB::table('invoice As A')
          ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })  
         ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and YEAR(A.invoice_date)='.$report_for_year.' '.$condition2.' and A.status=1 and A.is_deleted=0 OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and YEAR(A.invoice_date)='.$report_for_year.' '.$condition2.' and A.status=1 and A.is_deleted=0')
          ->select(DB::raw('sum(A.total_amount) as total,sum(A.tax) as tax,count(A.invoice_id) as count_invoices'))
          ->first();
          
          $sales_by_day_onlypaid=DB::table('invoice As A')
           ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })  
          ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and YEAR(A.payment_date)='.$report_for_year.' '.$condition2A.' and A.status=0 and A.is_deleted=0 OR U.status=1 and U.is_deleted=0 and  A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and YEAR(A.payment_date)='.$report_for_year.' '.$condition2A.' and A.status=0 and A.is_deleted=0')
          ->select(DB::raw('sum(A.total_amount) as total,sum(A.tax) as tax'))
          ->first();          
          
          $sales_by_day_sale=$sales_by_day_unpaid->total+$sales_by_day_onlypaid->total;
          $sales_by_day_paid=$sales_by_day_onlypaid->total;
          $sales_by_day_tax=$sales_by_day_unpaid->tax+$sales_by_day_onlypaid->tax;
          $data["q2_".$report_for_year]=["overall"=>$sales_by_day_sale,"paid"=>$sales_by_day_paid,"tax"=>$sales_by_day_tax];
         }
         if($condition3 || $condition4)
         {
         $sales_by_day_unpaid=DB::table('invoice As A')
          ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })  
           ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and YEAR(A.invoice_date)='.$report_for_year.' '.$condition3.' and A.status=1 and A.is_deleted=0 OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and YEAR(A.invoice_date)='.$report_for_year.' '.$condition3.' and A.status=1 and A.is_deleted=0')
          ->select(DB::raw('sum(A.total_amount) as total,sum(A.tax) as tax,count(A.invoice_id) as count_invoices'))
          ->first();
          
          $sales_by_day_onlypaid=DB::table('invoice As A')
           ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })  
          ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and YEAR(A.payment_date)='.$report_for_year.' '.$condition3A.' and A.status=0 and A.is_deleted=0 OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and YEAR(A.payment_date)='.$report_for_year.' '.$condition3A.' and A.status=0 and A.is_deleted=0')
          ->select(DB::raw('sum(A.total_amount) as total,sum(A.tax) as tax'))
          ->first();          
          
          $sales_by_day_sale=$sales_by_day_unpaid->total+$sales_by_day_onlypaid->total;
          $sales_by_day_paid=$sales_by_day_onlypaid->total;
          $sales_by_day_tax=$sales_by_day_unpaid->tax+$sales_by_day_onlypaid->tax;
          $data["q3_".$report_for_year]=["overall"=>$sales_by_day_sale,"paid"=>$sales_by_day_paid,"tax"=>$sales_by_day_tax];
         }
         if($condition4)
         {
         $sales_by_day_unpaid=DB::table('invoice As A')
          ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })  
          ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and YEAR(A.invoice_date)='.$report_for_year.' '.$condition4.' and A.status=1 and A.is_deleted=0 OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and YEAR(A.invoice_date)='.$report_for_year.' '.$condition4.' and A.status=1 and A.is_deleted=0')
          ->select(DB::raw('sum(A.total_amount) as total,sum(A.tax) as tax,count(A.invoice_id) as count_invoices'))
          ->first();
          
          $sales_by_day_onlypaid=DB::table('invoice As A')
           ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })  
          ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and YEAR(A.payment_date)='.$report_for_year.' '.$condition4A.' and A.status=0 and A.is_deleted=0 OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and YEAR(A.payment_date)='.$report_for_year.' '.$condition4A.' and A.status=0 and A.is_deleted=0')
          ->select(DB::raw('sum(A.total_amount) as total,sum(A.tax) as tax'))
          ->first();          
          
          $sales_by_day_sale=$sales_by_day_unpaid->total+$sales_by_day_onlypaid->total;
          $sales_by_day_paid=$sales_by_day_onlypaid->total;
          $sales_by_day_tax=$sales_by_day_unpaid->tax+$sales_by_day_onlypaid->tax;
          $data["q4_".$report_for_year]=["overall"=>$sales_by_day_sale,"paid"=>$sales_by_day_paid,"tax"=>$sales_by_day_tax];
         }   
         
          
         return $data;
        }
        //Sales report by year
        public function sales_by_year($report_for_year)
        {      
          
          
          $getmy_clients='""';
         
         if(session('user')->category=="vendor")
         {
         $request1=new  Request();
         $request1->type="client";                     
         $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
         $getmy_clients=array_column($data,'id');
         if(count($getmy_clients)>0)
         {
         $getmy_clients=implode(',',$getmy_clients);
         }
         else
         {
            $getmy_clients='""';
         }
         }         
          $data=[];       
          $sales_by_day_unpaid=DB::table('invoice As A')
           ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                }) 
          ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and YEAR(A.invoice_date)='.$report_for_year.' and A.status=1 and A.is_deleted=0 OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and YEAR(A.invoice_date)='.$report_for_year.' and A.status=1 and A.is_deleted=0')
          ->select(DB::raw('sum(A.total_amount) as total,sum(A.tax) as tax,count(A.invoice_id) as count_invoices'))
          ->first();
          
          $sales_by_day_onlypaid=DB::table('invoice As A')
           ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                }) 
          ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and YEAR(A.payment_date)='.$report_for_year.' and A.status=0 and A.is_deleted=0 OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and YEAR(A.payment_date)='.$report_for_year.' and A.status=0 and A.is_deleted=0')
          ->select(DB::raw('sum(A.total_amount) as total,sum(A.tax) as tax'))
          ->first();         
          
          $sales_by_day_sale=$sales_by_day_unpaid->total+$sales_by_day_onlypaid->total;
          $sales_by_day_paid=$sales_by_day_onlypaid->total;
          $sales_by_day_tax=$sales_by_day_unpaid->tax+$sales_by_day_onlypaid->tax;
          $data[$report_for_year]=["overall"=>$sales_by_day_sale,"paid"=>$sales_by_day_paid,"tax"=>$sales_by_day_tax];
         
          return $data;
        }
        //Sales Report by customer
        public function sales_by_customer($client_id='all',$client_type='all')
        {
             $condition1=(($client_id=="all" && $client_type=="all") ? ' and A.client_type="client"' : (($client_type=="client") ? ' and A.client_type="'.$client_type.'" and A.client_id="'.$client_id.'"' : ''));
             $condition2=(($client_id=="all" && $client_type=="all") ? ' and A.client_type="vendor"' : (($client_type=="vendor") ? ' and A.client_type="'.$client_type.'" and A.client_id="'.$client_id.'"' : ''));
            
             $getmy_clients='""';
         
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
             $getmy_clients='""';
             }
             }
             
            $data=[];
              
              //Clients
              if($condition1)
              {              
              $sales_by_day_overall=DB::table('invoice As A')
              ->join('users As B',[['B.category','=','A.client_type'],['A.client_id','=','B.client_id']])
              ->whereRaw('B.status=1 and B.is_deleted=0 and A.created_user_id='.session('user')->id.' and A.is_deleted=0 '.$condition1.' OR B.status=1 and B.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.is_deleted=0 '.$condition1.'')
              ->select(DB::raw('B.name as client_name,A.client_id,A.client_type,sum(A.total_amount) as total,sum(A.tax) as tax,count(A.invoice_id) as count_invoices'))
              ->groupBy(DB::raw('A.client_id,A.client_type'))
              ->orderBy('client_name','asc')
              ->get();
              
              for($i=0;$i<count($sales_by_day_overall);$i++)
              {
              $sales_by_day_onlypaid=DB::table('invoice As A')
              ->whereRaw('A.created_user_id='.session('user')->id.' and A.client_type="'.$sales_by_day_overall[$i]->client_type.'" and A.client_id="'.$sales_by_day_overall[$i]->client_id.'" and A.status=0 and A.is_deleted=0')
              ->select(DB::raw('sum(A.total_amount) as total'))
              ->first();
              
              $sales_by_day_sale=$sales_by_day_overall[$i]->total; 
              $sales_by_day_tax=$sales_by_day_overall[$i]->tax;
              if(count($sales_by_day_onlypaid)>0)
              {
                $sales_by_day_paid=$sales_by_day_onlypaid->total;  
              }
              else
              {
                $sales_by_day_paid=0;
              }
              
              
              $data[$sales_by_day_overall[$i]->client_type.','.$sales_by_day_overall[$i]->client_id]=["overall"=>$sales_by_day_sale,"paid"=>$sales_by_day_paid,"tax"=>$sales_by_day_tax,"name"=>$sales_by_day_overall[$i]->client_name,"client_type"=>$sales_by_day_overall[$i]->client_type];
              }
              }
              //Vendors
              if($condition2)
              {
              $sales_by_day_overall=DB::table('invoice As A')
              ->join('users As B',[['B.category','=','A.client_type'],['A.client_id','=','B.vendor_id']])
              ->whereRaw('B.status=1 and B.is_deleted=0 and A.created_user_id='.session('user')->id.' and A.is_deleted=0 '.$condition2.' OR B.status=1 and B.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.is_deleted=0 '.$condition2.'')
              ->select(DB::raw('B.name as client_name,A.client_id,A.client_type,sum(A.total_amount) as total,sum(A.tax) as tax,count(A.invoice_id) as count_invoices'))
              ->groupBy(DB::raw('A.client_id,A.client_type'))
              ->orderBy('client_name','asc')
              ->get();
              
              for($i=0;$i<count($sales_by_day_overall);$i++)
              {
              $sales_by_day_onlypaid=DB::table('invoice As A')
              ->whereRaw('A.created_user_id='.session('user')->id.' and A.client_type="'.$sales_by_day_overall[$i]->client_type.'" and A.client_id="'.$sales_by_day_overall[$i]->client_id.'" and A.status=0 and A.is_deleted=0')
              ->select(DB::raw('sum(A.total_amount) as total'))
              ->first();
              
              $sales_by_day_sale=$sales_by_day_overall[$i]->total; 
              $sales_by_day_tax=$sales_by_day_overall[$i]->tax;
              if(count($sales_by_day_onlypaid)>0)
              {
                $sales_by_day_paid=$sales_by_day_onlypaid->total;  
              }
              else
              {
                $sales_by_day_paid=0;
              }
              $data[$sales_by_day_overall[$i]->client_type.','.$sales_by_day_overall[$i]->client_id]=["overall"=>$sales_by_day_sale,"paid"=>$sales_by_day_paid,"tax"=>$sales_by_day_tax,"name"=>$sales_by_day_overall[$i]->client_name,"client_type"=>$sales_by_day_overall[$i]->client_type];
              }
              }
              return $data;
            
        }
        //Sales report by item 
        public function sales_by_item($item_id="all")
        {
             $condition=($item_id=="all") ? '' : ' and C.id="'.$item_id.'"' ;
             
             $getmy_clients='""';
         
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
               $getmy_clients='""';
             }
             }
             
             $data=[];
                          
              $sales_by_day_overall=DB::table('invoice As A')
              ->join('invoice_description As B',[['B.invoice_id','=','A.invoice_id']])
              ->leftJoin('invoice_saved_items As C',[['B.item','C.item'],['C.created_user_id','A.created_user_id']])
               ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                }) 
              ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and A.is_deleted=0 '.$condition.' OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.is_deleted=0 '.$condition.'')
              ->select(DB::raw('C.item as item_name,C.id as item_id,group_concat(B.item separator "*@*") as item_name1,
              sum(B.amount+B.amount*A.tax_perc/100) as total,sum((B.amount+B.amount*A.tax_perc/100)*A.creditcardfee_perc/100) as creditcardfees,              
              sum(B.qty) as qty,sum(B.amount*A.tax_perc/100) as tax'))
              ->groupBy('C.item')
              ->orderBy('C.item','asc')
              ->get();
              
              for($i=0;$i<count($sales_by_day_overall);$i++)
              {
              
              $sales_by_day_sale=$sales_by_day_overall[$i]->total+$sales_by_day_overall[$i]->creditcardfees; 
              $sales_by_day_tax=$sales_by_day_overall[$i]->tax;
              $sales_by_day_qty=$sales_by_day_overall[$i]->qty;
              
              $data[$sales_by_day_overall[$i]->item_id]=["overall"=>$sales_by_day_sale,"qty"=>$sales_by_day_qty,"tax"=>$sales_by_day_tax,"item_name"=>$sales_by_day_overall[$i]->item_name,"item_name1"=>$sales_by_day_overall[$i]->item_name1];
              }
              return $data;
            
        }
        //Load Bar chart and Pie Chart
        public function loadCharts($type="overall",$year="current")
        {
           $getmy_clients='""';
         
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
               $getmy_clients='""';
             }
             }
             
           $arr2=[];
           if(!$year || $year=="current")
           {
            $year_paid="  and YEAR(A.payment_date)=YEAR(CURDATE()) and MONTH(A.payment_date)=MONTH(CURDATE())";
            $year_sent="  and YEAR(A.invoice_date)=YEAR(CURDATE()) and MONTH(A.invoice_date)=MONTH(CURDATE())";
           }
           else
           {
             $arr=explode('/',$year,2);
             
             $year_paid='  and DATE(A.payment_date)>="'.$arr[0].'" and DATE(A.payment_date)<="'.$arr[1].'"';
             $year_sent='  and A.invoice_date>="'.$arr[0].'" and A.invoice_date<="'.$arr[1].'"';
           }  
           if($type=="paid")
           {
           $total_paid_amount_month_wise=DB::table('invoice As A')
             ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                }) 
            ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and A.status=0 and A.is_deleted=0 '.$year_paid.' OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=0 and A.is_deleted=0 '.$year_paid.'')
            ->select(DB::raw('MONTH(A.payment_date) as month,SUM(A.total_amount) as total'))->groupBy('month')->orderBy('month')->get();
            
            $highlypaid_condition=$overallOther_condition='';
           
           }
           else if($type=="sent")
           {
           $total_paid_amount_month_wise=DB::table('invoice As A')
             ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                }) 
            ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and A.status=1 and A.is_deleted=0 '.$year_sent.' OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.is_deleted=0 '.$year_sent.'')
            ->select(DB::raw('MONTH(A.invoice_date) as month,SUM(A.total_amount) as total'))->groupBy('month')->orderBy('month')->get();
           
           $highlypaid_condition=$overallOther_condition=' and A.status=1';
           }
           else if($type=="overdue")
           {
           $total_paid_amount_month_wise=DB::table('invoice As A')
             ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                }) 
            ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and A.status=1 and A.is_deleted=0 and A.due_date<CURDATE() '.$year_sent.' OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.is_deleted=0 and A.due_date<CURDATE() '.$year_sent.'')
            ->select(DB::raw('MONTH(A.invoice_date) as month,SUM(A.total_amount) as total'))->groupBy('month')->orderBy('month')->get();
           
           $highlypaid_condition=$overallOther_condition=' and A.status=1 and A.due_date<CURDATE()';
           }
           else if($type=="overall")
           {
           $total_paid_amount_month_wise=DB::table('invoice As A')
             ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                }) 
            ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and A.status=0 and A.is_deleted=0 '.$year_paid.' OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=0 and A.is_deleted=0 '.$year_paid.'')
            ->select(DB::raw('MONTH(A.payment_date) as month,SUM(A.total_amount) as total'))->groupBy('month')->orderBy('month')->get();
            
           $total_unpaid_amount_month_wise=DB::table('invoice As A')
             ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                }) 
            ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id='.session('user')->id.' and A.status=1 and A.is_deleted=0 '.$year_sent.' OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.is_deleted=0 '.$year_sent.'')
            ->select(DB::raw('MONTH(A.invoice_date) as month,SUM(A.total_amount) as total'))->groupBy('month')->orderBy('month')->get();
            
            $total_unpaid_amount_month_wise=json_decode(json_encode($total_unpaid_amount_month_wise),true);
            
           
            $arr2= array_column($total_unpaid_amount_month_wise,'total','month');    
              
               
           //  print_r($arr);die;
            $highlypaid_condition=$overallOther_condition='';           
           
           }   
            
            $total_paid_amount_month_wise=json_decode(json_encode($total_paid_amount_month_wise),true);
            
            $total_paid_amount_month_wise=array_column($total_paid_amount_month_wise,'total','month');            
            
            $total_paid_amount_month_wise=array_map([$this,"roundUpFloatNumber"],$total_paid_amount_month_wise);

            for($i=0;$i<12;$i++)
            {
               if(!array_key_exists($i+1,$total_paid_amount_month_wise))
               {
                $total_paid_amount_month_wise[$i+1]=0;
               }
               if(array_key_exists($i+1,$arr2))
               {
                $total_paid_amount_month_wise[$i+1]=$total_paid_amount_month_wise[$i+1]+$arr2[$i+1];
               }
                              
            }
            ksort($total_paid_amount_month_wise);
            
           $highlypaid=DB::select('select sum(A.total_amount) as balance,A.client_id,A.client_type from invoice As A inner join users U ON A.client_id=U.client_id and A.client_type=U.category OR A.client_id=U.vendor_id and A.client_type=U.category where U.status=1 and U.is_deleted=0 and A.is_deleted=0 and A.created_user_id=? '.$highlypaid_condition.' OR U.status=1 and U.is_deleted=0 and A.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" '.$highlypaid_condition.' group by A.client_id,A.client_type  order by balance desc limit 2',[session('user')->id]); //'.$year_sent.'
           foreach($highlypaid as $client)
           {
            $table=($client->client_type=="vendor") ?"vendors" :"clients";
            $name=DB::table($table)->where('id',$client->client_id)->select('name')->first();
            $client->client_name=$name->name;
            $client->balance=$this->roundUpFloatNumber($client->balance);
           }
           
            $overallamount=DB::table('invoice As A')
            ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                }) 
            ->whereRaw('U.status=1 and U.is_deleted=0 and A.is_deleted=0 and A.created_user_id="'.session('user')->id.'"   '.$overallOther_condition.' OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.is_deleted=0   '.$overallOther_condition.'')->sum('total_amount');     //'.$year_sent.'       
            $overallamount=$this->roundUpFloatNumber($overallamount);
            if(count($highlypaid)==1)
            {
              $overallamount=$overallamount-$highlypaid[0]->balance;  
            }
            else if(count($highlypaid)==2)
            {
              $overallamount=$overallamount-$highlypaid[0]->balance-$highlypaid[1]->balance;  
            }
            $overallamount=$this->roundUpFloatNumber($overallamount);                                     
           return ["total_paid_amount_month_wise"=>$total_paid_amount_month_wise,"highlypaid"=>$highlypaid,"overallamount"=>$overallamount];
        }
        //Show Sales/Invoice Report
        public function invoiceReport()
        {
             $getmy_clients='""';
         
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
               $getmy_clients='""';
             }
             }
            
            $count_sent_invoices=DB::table('invoice As A')
            ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
            ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id="'.session('user')->id.'" and A.status=1 and A.is_deleted=0 and A.due_date>="'.date('Y-m-d').'" and YEAR(A.invoice_date)=YEAR(CURDATE()) OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.is_deleted=0 and A.due_date>="'.date('Y-m-d').'" and YEAR(A.invoice_date)=YEAR(CURDATE())')->count('invoice_id');
            
            $count_overdue_invoices=DB::table('invoice As A')
            ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
            ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id="'.session('user')->id.'" and A.status=1 and A.is_deleted=0 and A.due_date<"'.date('Y-m-d').'" and YEAR(A.invoice_date)=YEAR(CURDATE()) OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.is_deleted=0 and A.due_date<"'.date('Y-m-d').'" and YEAR(A.invoice_date)=YEAR(CURDATE())')->count('invoice_id');           
            
            $count_total_sent_invoices=$count_sent_invoices+$count_overdue_invoices;
            
            $amount_sent_invoices=DB::table('invoice As A')
            ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
            ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id="'.session('user')->id.'" and A.status=1 and A.is_deleted=0 and A.due_date>="'.date('Y-m-d').'" and YEAR(A.invoice_date)=YEAR(CURDATE()) OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.is_deleted=0 and A.due_date>="'.date('Y-m-d').'" and YEAR(A.invoice_date)=YEAR(CURDATE())')->sum('total_amount');
            
            
            $amount_overdue_invoices=DB::table('invoice As A')
            ->join('users As U',function($join){
                    $join->on([['A.client_type','=','U.category'],['A.client_id','=','U.client_id']])->orOn([['A.client_type','=','U.category'],['A.client_id','=','U.vendor_id']]);
                })
            ->whereRaw('U.status=1 and U.is_deleted=0 and A.created_user_id="'.session('user')->id.'" and A.status=1 and A.is_deleted=0 and A.due_date<"'.date('Y-m-d').'" and YEAR(A.invoice_date)=YEAR(CURDATE()) OR U.status=1 and U.is_deleted=0 and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.status=1 and A.is_deleted=0 and A.due_date<"'.date('Y-m-d').'" and YEAR(A.invoice_date)=YEAR(CURDATE())')->sum('total_amount');
                        
            $amount_total_sent_invoices=$amount_sent_invoices+$amount_overdue_invoices;            
            
            $amount_total_sent_invoices=$this->money_format1($amount_total_sent_invoices);
            $amount_overdue_invoices=$this->money_format1($amount_overdue_invoices);            
           
            $charts=$this->loadCharts();
            
           $total_paid_amount_month_wise=$charts["total_paid_amount_month_wise"];            
           $highlypaid=$charts["highlypaid"];
           $overallamount=$charts["overallamount"];
           
           //Sales by day           
          $report_for_month_f=date('m'); $report_for_month=date('n'); $report_for_year=date('Y');
          $sales_by_day=$this->sales_by_day($report_for_month_f,$report_for_month,$report_for_year,date('t'));
          
          //Sales by month 
          $sales_by_month=$this->sales_by_month(date('Y'),12);
          
          //Sales by quarter 
          $sales_by_quarter=$this->sales_by_quarter(date('Y'),12);         
          
          //Sales by year 
          $sales_by_year=$this->sales_by_year(date('Y')); 
          
          //Sales by customer 
          $sales_by_customer=$this->sales_by_customer();
          
          //Sales by items 
          $sales_by_item=$this->sales_by_item();  
             
           return view('panel.frontend.invoice-report',['count_total_sent_invoices'=>$count_total_sent_invoices,'count_overdue_invoices'=>$count_overdue_invoices,'amount_total_sent_invoices'=>$amount_total_sent_invoices,'amount_overdue_invoices'=>$amount_overdue_invoices,'total_paid_amount_month_wise'=>$total_paid_amount_month_wise,'top2highlypaid'=>$highlypaid,"overallamount"=>$overallamount,'sales_by_day'=>$sales_by_day,"sales_by_month"=>$sales_by_month,"sales_by_quarter"=>$sales_by_quarter,"sales_by_year"=>$sales_by_year,'sales_by_customer'=>$sales_by_customer,"sales_by_item"=>$sales_by_item]);
        }       

        //Filter sales by day report
        public function filterSalesByDay(Request $request)
        {
            //Sales by day
          $month=explode('/',$request->filterKey)[0];  $year=explode('/',$request->filterKey)[1]; 
          $report_for_month_f=$month;  $report_for_month=sprintf('%01d',$month); $report_for_year=$year;
          $sales_by_day=$this->sales_by_day($report_for_month_f,$report_for_month,$report_for_year,date('t',strtotime('01-'.$report_for_month.'-'.$report_for_year)));
         
           $msg='';
           
           foreach($sales_by_day as $key=>$value)
           {
				            $msg.='<tr>
										<td><a target="_blank" title="View Invoices" href="manage-invoice?filter='.$key.'&type=day">'.date('m/d/Y',strtotime($key)).'</a></td>
										<td>$'.$this->money_format1($value["unpaid"]+$value["paid"]).'</td>
										<td>$'.$this->money_format1($value["paid"]).'</td>
										<td>$'.$this->money_format1($value["unpaid"]).'</td>
										<td>$'.$this->money_format1($value["tax"]).'</td>
									</tr>';    
            }
            if(!$msg)
            {
                $msg='<tr><td class="text-center" colspan="6">No matching records found</td></tr>';
            }         
            
            return $msg;
            
        }
        //Filter sales by month report
        public function filterSalesByMonth(Request $request)
        {
          //Sales by month
          $year=$request->filterKey; 
          $sales_by_month=$this->sales_by_month($year,12);
          $msg='';
           
           foreach($sales_by_month as $key=>$value)
           {
				            $msg.='<tr>
										<td><a target="_blank" title="View Invoices" href="manage-invoice?filter='.date('Y-m',strtotime($key)).'&type=month">'.date('F Y',strtotime($key)).'</a></td>
										<td>$'.$this->money_format1($value["overall"]).'</td>
										<td>$'.$this->money_format1($value["paid"]).'</td>
										<td>$'.$this->money_format1($value["overall"]-$value["paid"]).'</td>
										<td>$'.$this->money_format1($value["tax"]).'</td>
									</tr>';    
            }
            if(!$msg)
            {
                $msg='<tr><td class="text-center" colspan="6">No matching records found</td></tr>';
            }         
            
            return $msg;
        }
        //Filter sales by quarter report
        public function filterSalesByQuarter(Request $request)
        {
          //Sales by quarter
          $year=$request->filterKey; 
          $number_of_passed_months=12;
          $sales_by_quarter=$this->sales_by_quarter($year,$number_of_passed_months);  
          $msg='';             
          
           foreach($sales_by_quarter as $key=>$value)
           {
				            $msg.='<tr>
										<td><a target="_blank" title="View Invoices" href="manage-invoice?filter='.$key.'&type=quarter">'.ucfirst(str_replace("_"," ",$key)).'</a></td>
										<td>$'.$this->money_format1($value["overall"]).'</td>
										<td>$'.$this->money_format1($value["paid"]).'</td>
										<td>$'.$this->money_format1($value["overall"]-$value["paid"]).'</td>
										<td>$'.$this->money_format1($value["tax"]).'</td>
									</tr>';    
            }
            if(!$msg)
            {
                $msg='<tr><td class="text-center" colspan="6">No matching records found</td></tr>';
            }         
            
            return $msg;
        }
        //Filter sales report by year
        public function filterSalesByYear(Request $request)
        {
          //Sales by year
          $year=$request->filterKey;
          $sales_by_year=$this->sales_by_year($year);  
          $msg='';             
          
           foreach($sales_by_year as $key=>$value)
           {
				            $msg.='<tr>
										<td><a target="_blank" title="View Invoices" href="manage-invoice?filter='.$key.'&type=year">'.$key.'</a></td>
										<td>$'.$this->money_format1($value["overall"]).'</td>
										<td>$'.$this->money_format1($value["paid"]).'</td>
										<td>$'.$this->money_format1($value["overall"]-$value["paid"]).'</td>
										<td>$'.$this->money_format1($value["tax"]).'</td>
									</tr>';    
            }
            if(!$msg)
            {
                $msg='<tr><td class="text-center" colspan="6">No matching records found</td></tr>';
            }         
            
            return $msg;
        }
        //Filter sales report by customer
        public function filterSalesByCustomer(Request $request)
        {
          //Sales by customer
          $client_id=$request->filterKey;
          $client_type=$request->client_type;
          $sales_by_customer=$this->sales_by_customer($client_id,$client_type); 
          
          $msg='';             
          
           foreach($sales_by_customer as $key=>$value)
           {
				            $msg.='<tr class="';if($value['client_type']!='client'){$msg.='active';}$msg.='">
										<td><a target="_blank" title="View Invoices" href="manage-invoice?filter='.urlencode($key).'&type=customer">'.$value["name"].'</a></td>
										<td>$'.$this->money_format1($value["overall"]).'</td>
										<td>$'.$this->money_format1($value["paid"]).'</td>
										<td>$'.$this->money_format1($value["overall"]-$value["paid"]).'</td>
										<td>$'.$this->money_format1($value["tax"]).'</td>
									</tr>';    
            }
            if(!$msg)
            {
                $msg='<tr><td class="text-center" colspan="6">No matching records found</td></tr>';
            }         
            
            return $msg;
        }
        //Filter sales report by item
        public function filterSalesByItem(Request $request)
        {
          //Sales by item
          $item_id=$request->filterKey;
          $sales_by_item=$this->sales_by_item($item_id); 
           
          $msg='';             
          
           foreach($sales_by_item as $key=>$value)
           {
				            $item_name=($value["item_name"] && $key) ?$value["item_name"] :'Others(Deleted Items)'; 
                            $item_name_key=($value["item_name"] && $key) ? $key :'other';
                            $msg.='<tr">
										<td><a target="_blank" title="View Invoices" href="manage-invoice?filter='.urlencode($item_name_key).'&type=item">'.$value["item_name"].'</a></td>
										<td>$'.$this->money_format1($value["overall"]).'</td>
										<td>'.$this->money_format1($value["qty"]).'</td>
										<td>$'.$this->money_format1($value["overall"]/$value["qty"]).'</td>
										<td>$'.$this->money_format1($value["tax"]).'</td>
									</tr>';    
            }
            if(!$msg)
            {
                $msg='<tr><td class="text-center" colspan="6">No matching records found</td></tr>';
            }         
            
            return $msg;
        }
        //Load Charts with  data
        public function loadChartsType(Request $request)
        {  
           $charts=json_encode($this->loadCharts($request->type,$request->range));
           
           return $charts;
        }
        //edit Sent Invoice
        public function editSentInvoice($invoice_id)
        {          
             $getmy_clients='""';
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
                $getmy_clients='""';
             }
             }    
             
           $invoice_data=DB::table('invoice')
           ->where([['invoice_id',$invoice_id],['created_user_id',session('user')->id],['status',1],['is_deleted',0]])
           ->orWhereRaw('invoice_id="'.$invoice_id.'" and created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and client_id IN ('.$getmy_clients.') and client_type="client" and status=1 and is_deleted=0')
           ->first();
                     
           if(count($invoice_data)>0)
           {
            $client_name='';$clients_list=[];
            
            $invoice_items=DB::table('invoice_description')->where('invoice_id',$invoice_id)->orderBy('id','asc')->get();
            
            $request1=new Request();
            $request1->type=$invoice_data->client_type;
            $clients_list=json_decode($this->getClientsInvoice($request1));            
            
            $invoice_data->client_name=$client_name;
            Session::flash('invoice_items',$invoice_items);
            Session::flash('invoice_data',$invoice_data);
            Session::flash('invoice_edit',1);
            Session::flash('clients_list',$clients_list);                   
            return redirect()->route('invoice');
           }
           else
           {
            return redirect()->route('manage-invoice')->with('alert_danger','Oops! Invalid invoice data');
           }
        }
        
        //Delete Invoices
        public function deleteInvoice(Request $request)
        {
          switch($request->invoice_type)
          {
            case "draft" : $table='draft_invoices';$col='id'; break;
            case "sent" : $table='invoice';$col='invoice_id';break;
            case "paid" : $table='invoice';$col='invoice_id';break;
          }
          
          $getmy_clients='""';
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
                $getmy_clients='""';
             }
             }
             
            
            $invoice_id=$request->invoice_id;
            
            $invoice_details=DB::table($table)
            ->where([[$col,$invoice_id],['is_deleted',0],['created_user_id',session('user')->id]])
            ->orWhereRaw($col.'="'.$invoice_id.'" and is_deleted=0 and created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and client_id IN ('.$getmy_clients.') and client_type="client"')
            ->count($col);
            
            if($invoice_details==0)
            {
              return response()->make('{"code":500,"msg":"Oops! invalid request."}',500,["content-type"=>'application/json']);
            }
            
          
          DB::table($table)->where($col,$invoice_id)->update(['is_deleted'=>1 , 'updated_at'=>$this->now]);
          
          if($request->invoice_type=="paid")
          {
           DB::table('paid_invoices')->where($col,$invoice_id)->update(['is_deleted'=>1 , 'updated_at'=>$this->now]); 
          }
          
          Session::flash('alert_success','Invoice deleted successfully');
          
          return response()->make('{"code":200,"msg":"Invoice deleted successfully"}',200,["content-type"=>'application/json']);
            
        }
        
        //Copy invoice to drafts
        public function cloneInvoice(Request $request)
        {          
          switch($request->invoice_type)
          {
           // case "draft" : $table='draft_invoices';$col='id'; break;
           //  case "sent" : $table='invoice';$col='invoice_id';break;
            case "paid" : $table='invoice'; $table1='invoice_description'; $col='invoice_id';break;
          }
          
          $getmy_clients='""';
             if(session('user')->category=="vendor")
             {
             $request1=new  Request();
             $request1->type="client";                     
             $data=json_decode($this->getClientsInvoice($request1),true); // For vendors only
             $getmy_clients=array_column($data,'id');
             if(count($getmy_clients)>0)
             {
             $getmy_clients=implode(',',$getmy_clients);
             }
             else
             {
                $getmy_clients='""';
             }
             }
             
            $invoice_num=$request->invoice_num;
            $invoice_id=$request->invoice_id;
            
            $invoice_details=DB::table($table.' As A')->join($table1.' As B','A.invoice_id','=','B.invoice_id')
            ->where([['A.invoice_id',$invoice_id],['A.invoice_num',$invoice_num],['A.is_deleted',0],['A.created_user_id',session('user')->id]])
            ->orWhereRaw('A.invoice_id="'.$invoice_id.'" and A.invoice_num="'.$invoice_num.'" and A.created_user_id=(select created_user_id from users where id="'.session('user')->id.'") and A.client_id IN ('.$getmy_clients.') and A.client_type="client" and A.is_deleted=0')
            ->select('A.*','B.item','B.description','B.qty','B.rate','B.amount')->orderBy('B.id','asc')->get();
            
            if(count($invoice_details)==0)
            {
              return response()->make('{"code":500,"msg":"Oops! invalid request."}',500,["content-type"=>'application/json']);
            } 
            
            $new_logo=$invoice_details[0]->company_logo;
            //Copy Files
            if($invoice_details[0]->company_logo!='pownder_logo.png')
            {
            $org_logo=explode('.',$invoice_details[0]->company_logo);    
            $extension=end($org_logo);    
            $new_logo=date('dmYHis').rand(1,100).'.'.$extension;      
            copy('pownder/storage/app/uploads/invoice/company_logo/'.$invoice_details[0]->company_logo,'pownder/storage/app/uploads/invoice/company_logo/'.$new_logo);
            }
            
            //Save Records
            $draft_id=DB::table('draft_invoices')->insertGetId([
            "client_id"=>$invoice_details[0]->client_id,
            "client_type"=>$invoice_details[0]->client_type,
            "client_email"=>$invoice_details[0]->client_email,
            "cc_email"=>$invoice_details[0]->cc_email,
            "company_logo"=>$new_logo,
            "company_name"=>$invoice_details[0]->company_name,
            "company_address"=>$invoice_details[0]->company_address,
            "bill_to"=>$invoice_details[0]->bill_to,
            "invoice_date"=>$invoice_details[0]->invoice_date,
            "due_date"=>$invoice_details[0]->due_date,
            'due_on_receipt'=>$invoice_details[0]->due_on_receipt,
            "recurring_date"=>$invoice_details[0]->recurring_date,
            "sub_total"=>$invoice_details[0]->sub_total,
            "tax_perc"=>$invoice_details[0]->tax_perc,
            "tax"=>$invoice_details[0]->tax,
            "total"=>$invoice_details[0]->total,
            "paid"=>$invoice_details[0]->paid,
            "balance"=>$invoice_details[0]->balance,
            "recurring_on"=>$invoice_details[0]->recurring_on,            
            "created_user_id"=>session('user')->id,
            "updated_user_id"=>session('user')->id,
            "created_at"=>$this->now,
            "updated_at"=>$this->now            
            ]);
            
            $values=[];
            for($i=0;$i<count($invoice_details);$i++)
            { 
                $values[]=["draft_id"=>$draft_id,"item"=>$invoice_details[$i]->item,"description"=>$invoice_details[$i]->description,"qty"=>$invoice_details[$i]->qty,"rate"=>$invoice_details[$i]->rate,"amount"=>$invoice_details[$i]->amount];
            }
            
            DB::table('draft_description')->insert($values);            
            
            Session::flash('alert_success','Invoice has been saved as Draft.');
            
            return response()->make('{"code":200,"msg":"Invoice deleted successfully"}',200,["content-type"=>'application/json']);
                        
        }
        //Mark notifications as read
        public function MarkReadNotification($type,$id)
        {           
                if($type=="recurring_invoice_sent")
                {
                DB::table('invoice')->where([['invoice_id',$id],['created_user_id',session('user')->id],['recurring_notific',1]])->update(['recurring_notific'=>0]);
                return redirect()->route('view-invoice',['type'=>'sent','id'=>$id]);
                }
            
        }
        public function tmp()
        {
		  	ini_set('max_execution_time',0);
			ini_set('memory_limit',-1);
            
        //    $already_sending= DB::table('cron_reminder')->first();
//            if(is_null($already_sending))
//            {
//            DB::table('cron_reminder')->insert(['created_at'=>$this->now,'status'=>1]);
//            }
//            else if($already_sending->status==0)
//            {
//                DB::table('cron_reminder')->update(['created_at'=>$this->now,'status'=>1]);
//            }
//            else if($already_sending->status==1)
//            {
//               exit; 
//               //Already running a cron job 
//            }
		  $overdue_invoices=DB::table('invoice As A')->leftJoin('invoice_reminder As B',[['A.invoice_id','=','B.invoice_id'],['A.invoice_num','=','B.invoice_num']])
           ->where([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 3 DAY)'),'=',date('Y-m-d')]])->whereNull('B.auto_reminder_3')
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 3 DAY)'),'=',date('Y-m-d')],['B.auto_reminder_3',0]])          
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 7 DAY)'),'=',date('Y-m-d')],["B.created_user_id",0],["B.created_user_category",'cron'],['B.auto_reminder_7',0]])
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 7 DAY)'),'=',date('Y-m-d')]])->whereNull('B.auto_reminder_3')
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 7 DAY)'),'=',date('Y-m-d')],['B.auto_reminder_3',0]])           
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 14 DAY)'),'=',date('Y-m-d')],["B.created_user_id",0],["B.created_user_category",'cron'],['B.auto_reminder_14',0]])
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 14 DAY)'),'=',date('Y-m-d')]])->whereNull('B.auto_reminder_3')
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 14 DAY)'),'=',date('Y-m-d')],['B.auto_reminder_3',0]])     
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 21 DAY)'),'=',date('Y-m-d')],["B.created_user_id",0],["B.created_user_category",'cron'],['B.auto_reminder_21',0]])
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 21 DAY)'),'=',date('Y-m-d')]])->whereNull('B.auto_reminder_3')
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 21 DAY)'),'=',date('Y-m-d')],['B.auto_reminder_3',0]])          
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 30 DAY)'),'=',date('Y-m-d')],["B.created_user_id",0],["B.created_user_category",'cron'],['B.auto_reminder_30',0]])          
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 30 DAY)'),'=',date('Y-m-d')]])->whereNull('B.auto_reminder_3')          
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 30 DAY)'),'=',date('Y-m-d')],['B.auto_reminder_3',0]])
           ->select('A.*','B.id As reminder_id','B.auto_reminder_3','B.auto_reminder_7','B.auto_reminder_14','B.auto_reminder_21','B.auto_reminder_30')->get();
           print_r($overdue_invoices);die;
           foreach($overdue_invoices as $invoice)
           {
            if($invoice->invoice_num=="PNAD-443")
            {  
            if($invoice->client_type=="vendor")
            {
                $colname="vendor_id";
                $table="vendors";
            }
            else
            {
                $colname="client_id";
                $table="clients";
            }            
             
            $email=DB::table($table)->where([['id',$invoice->client_id],["is_deleted",0]])->select('invoice_email','name')->first();
            print_r($email);   
            if(!is_null($email))
            {
            $pdffilename=$invoice->pdffilename;
            $invoice_id=$invoice->invoice_id;
            $invoice_num=$invoice->invoice_num;
            $client_name=$email->name;
             
            $due_diff=date_diff(date_create($invoice->due_date),date_create(date('Y-m-d')));
            $due_in_days=$due_diff->format('%a');
            
            /** First check if manual reminder sent today or yesterday */
           $check_manually_send=DB::table('invoice_reminder')->where([["invoice_id",$invoice_id],["invoice_num",$invoice_num],["created_user_id",'!=',0],["created_user_category",'!=','cron'],[DB::raw('DATE(created_at)'),'>=',date('Y-m-d',strtotime('yesterday'))]])->count('id');   
           
            if($check_manually_send)
            {
            if(is_null($invoice->auto_reminder_3) || $invoice->auto_reminder_3==0)
            {    
            $check_already_inserted=DB::table('invoice_reminder')->where([["invoice_id",$invoice_id],["invoice_num",$invoice_num],["created_user_id",0],["created_user_category",'cron']])->count('id');   
           
            if(!$check_already_inserted)
            {               
            DB::table('invoice_reminder')->insert(["invoice_id"=>$invoice_id,"invoice_num"=>$invoice_num /*, "client_name"=>$client_name,"subject"=>$subject,"msg"=>$reminder_msg */ ,"created_user_id"=>0,"created_user_category"=>'cron',"created_at"=>$this->now,"updated_at"=>date('Y-m-d H:i:s',strtotime('-4 days'))]);
		    }
            }
            }
            else
            {
            $reminder_col='';
            
            if($due_in_days==3)
            {
                $reminder_col='auto_reminder_3';
            }
            else if($due_in_days==7)
            {
                $reminder_col='auto_reminder_7';
            }
            else if($due_in_days==14)
            {
                $reminder_col='auto_reminder_14';
            }
            else if($due_in_days==21)
            {
                $reminder_col='auto_reminder_21';
            }
            else if($due_in_days==30)
            {
                $reminder_col='auto_reminder_30';
            }
            $check_already_send=DB::table('invoice_reminder')->where([["invoice_id",$invoice_id],["invoice_num",$invoice_num],["created_user_id",0],["created_user_category",'cron'],[$reminder_col,1]])->count('id');   
            if($check_already_send)
            {
               exit; 
            }   
            DB::table('test')->insert(['name'=>$reminder_col]);
            if($reminder_col)
            {
            /** Calculate Due By Days */
            $due_days=0;
            $date1=date_create(date('Y-m-d'));
            $date2=date_create($invoice->due_date);    
            $diff=date_diff($date1,$date2); 
            $due_days=$diff->format('%a');
            /** Calculate Due By Days */
                                
            //email to client
            
            $key_values=['[name]'=>$email->name,'[invoice_num]'=>$invoice_num,'[due_date]'=>date('m/d/Y',strtotime($invoice->due_date)),'[Amount]'=>$this->money_format_view($invoice->balance),'[Company_Name]'=>$invoice->company_name,'[Company_Address]'=>nl2br($invoice->company_address),'[Due_Days]'=>$due_days];
                        
           $reminder_msg=$this->replaceKeywords($this->REMINDERMSG,$key_values);            
    
            $from=$this->INVOICEEMAILID;
       //     $subject='Reminder:'.$request->send_reminder_subject; 
            $subject=$this->replaceKeywords($this->REMINDERSUBJECT,$key_values);            
            $msg=View::make('invoice_payment.reminder_email_template',['company_name'=>$invoice->company_name,"email"=>$email,'invoice_num'=>$invoice_num,"msg"=>$reminder_msg])->render();//$request->send_reminder_msg
            
            $filepath=file_get_contents(__DIR__.'/../../../storage/app/uploads/invoice/invoice_pdf/'.$pdffilename,true); 
            $urls = 'https://api.sendgrid.com/';
            $users = self::SENDGRID_USER;
            $passs = self::SENDGRID_PASS;
            $paramss = array(
            'api_user' => $users,
            'api_key' => $passs,            
            'subject' => $subject,
            'html' => $msg,
            'from' => $from,
            'fromname' => "Pownder�",
            'files['.$pdffilename.']'=>'@'.$filepath.'/'.$pdffilename
            );
            $paramss=http_build_query($paramss);
            $emails=explode(',',$invoice->client_email);//     priya.constacloud2@gmail.com
            foreach($emails as $email)
            {
              $paramss.='&to[]='.$email;  
            }
            $emails=explode(',',$invoice->cc_email);//     priya.constacloud2@gmail.com
            foreach($emails as $email)
            {
              $paramss.='&cc[]='.$email; 
            }
            $requests = $urls.'api/mail.send.json';
            $sessions = curl_init($requests);
            curl_setopt ($sessions, CURLOPT_POST, true);
            curl_setopt ($sessions, CURLOPT_POSTFIELDS, $paramss);
            curl_setopt($sessions, CURLOPT_HEADER, false);
            curl_setopt($sessions, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($sessions, CURLOPT_RETURNTRANSFER, true);
            $responses = curl_exec($sessions);
            $info=curl_getinfo($sessions);
            curl_close($sessions);
            
            //Save Reminder Log
             if(is_null($invoice->auto_reminder_3) || $invoice->auto_reminder_3==0)
            {
            DB::table('invoice_reminder')->insert(["invoice_id"=>$invoice_id,"invoice_num"=>$invoice_num,"client_name"=>$client_name/*,"subject"=>$subject,"msg"=>$reminder_msg*/,"created_user_id"=>0,"created_user_category"=>'cron',"created_at"=>$this->now,"updated_at"=>$this->now,$reminder_col=>1]);
		    }
            else 
            {
            DB::table('invoice_reminder')->where("id",$invoice->reminder_id)->update([$reminder_col=>1,"updated_at"=>$this->now]);
		    }
            
            }	
             
            }
            
            }
           }
           }
           //Close cron job
         //  DB::table('cron_reminder')->update(['updated_at'=>$this->now,'status'=>0,'remark'=>count($overdue_invoices).' reminders sent']);
            
		}
        	
}            