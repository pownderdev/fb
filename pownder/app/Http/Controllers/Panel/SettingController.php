<?php
	namespace App\Http\Controllers\Panel;
	
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	
	use App\Models\Clients;
	use App\Models\Groups;
	use App\Models\Packages;
	
	use DB;
	use Redirect;
	use Session;
	use Helper;
	use ManagerHelper;
	use Mail;
	
	class SettingController extends Controller
	{
		public function getcurrenttime()
		{
			date_default_timezone_set(config('app.timezone'));
			$now = date('Y-m-d H:i:s');
			return $now;
		}  
		
		//Manage Setting
		public function getSettings(Request $request)
		{
			$UserArray = array();
            $WritePermission = 'Yes';
			$ClientPermission = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				$WritePermission = ManagerHelper::ManagerWritePermission('Setting Read Write');
				$ClientPermission = ManagerHelper::ManagerWritePermission('Clients Read Write');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id', Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			
			$EditPackage=array();
			$EditVendorPackage=array();
			
			$GroupColumns=array();
			$ClientPackageColumns=array();
			$VendorPackageColumns=array();
			$FacebookAdsAccountTable=array();
			$AdFormTable=array();
			$FacebookPageTable=array();
			
			
			$GroupSubCategories = DB::table('group_sub_categories')->where('group_category_id', 6)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();	
			
			$Pages = DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Block Facebook Page Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Pages as $Page)
			{
				$FacebookPageTable[] = $Page->column_value;
			}
			
			$AdForms=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Block Facebook Ad Form Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($AdForms as $AdForm)
			{
				$AdFormTable[]=$AdForm->column_value;
			}
			
			$GColumns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Setting Group Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($GColumns as $GColumn)
			{
				$GroupColumns[]=$GColumn->column_value;
			}
			
			$CPColumns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Setting Client Package Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($CPColumns as $CPColumn)
			{
				$ClientPackageColumns[]=$CPColumn->column_value;
			}
			
			$VPColumns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Setting Vendor Package Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($VPColumns as $VPColumn)
			{
				$VendorPackageColumns[]=$VPColumn->column_value;
			}
			
			$Columns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Setting Facebook Ads Account Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$FacebookAdsAccountTable[]=$Column->column_value;
			}
			
			$LeadFormArray=array();
			$LeadFormQuery = DB::table('group_lead_forms')
			->leftJoin('groups', 'group_lead_forms.group_id', '=', 'groups.id')
			->select('group_lead_forms.facebook_ads_lead_id')
			->where('group_lead_forms.is_deleted',0)
			->where('groups.is_deleted',0)
			->groupBy('group_lead_forms.facebook_ads_lead_id')
			->get();
			foreach($LeadFormQuery as $row)
			{
				$LeadFormArray[] = $row->facebook_ads_lead_id;
			}
			
			$LeadForms = DB::table('facebook_ads_leads')->select('id', 'name')->whereNotIn('id', $LeadFormArray)->where('status', 'ACTIVE')->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$CLeadFormArray=array();
			$LeadFormQuery = DB::table('client_lead_forms')->leftJoin('clients', 'client_lead_forms.client_id', '=', 'clients.id')->select('client_lead_forms.facebook_ads_lead_id')->where('client_lead_forms.is_deleted',0)->where('clients.is_deleted',0)->groupBy('client_lead_forms.facebook_ads_lead_id')->get();
			foreach($LeadFormQuery as $row)
			{
				$CLeadFormArray[] = $row->facebook_ads_lead_id;
			}
			
			$FacebookAdsLeads=DB::table('facebook_ads_leads')->select('id', 'name')->whereNotIn('id', $LeadFormArray)->whereNotIn('id', $CLeadFormArray)->where('status', 'ACTIVE')->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			//
			$PageArray=array();
			$GroupPages = DB::table('group_page_engagements')
			->leftJoin('groups', 'group_page_engagements.group_id', '=', 'groups.id')
			->select('group_page_engagements.facebook_page_id')
			->where('group_page_engagements.is_deleted',0)
			->where('groups.is_deleted',0)
			->groupBy('group_page_engagements.facebook_page_id')
			->get();
			foreach($GroupPages as $GroupPage)
			{
				$PageArray[] = $GroupPage->facebook_page_id;
			}
			
			$ClientPages = DB::table('page_engagements')
			->leftJoin('clients', 'page_engagements.client_id', '=', 'clients.id')
			->select('page_engagements.facebook_page_id')
			->where('page_engagements.is_deleted', 0)
			->where('clients.is_deleted', 0)
			->groupBy('page_engagements.facebook_page_id')
			->get();
			foreach($ClientPages as $ClientPage)
			{
				$PageArray[] = $ClientPage->facebook_page_id;
			}
			
			$facebook_pages = DB::table('facebook_pages')->select('id', 'name')->whereNotIn('id', $PageArray)->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			//
			
			if($request->has('package_id'))
			{
				$EditPackage=DB::table('packages')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->where('id',$request->package_id)->first();
				
				if(is_null($EditPackage))
				{
					return redirect('settings')->with('alert_danger','Unauthorized group access failed');
				}
			}
			
			if($request->has('vendor_package_id'))
			{
				$EditVendorPackage=DB::table('vendor_packages')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->where('id',$request->vendor_package_id)->first();
				
				if(is_null($EditVendorPackage))
				{
					return redirect('settings')->with('alert_danger','Unauthorized group access failed');
				}
			}
			
			if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
			{
				$Clients = DB::table('clients')->select('id','name','email')->where('status', 1)->where('is_deleted', 0)->orderBy('name', 'ASC')->orderBy('email', 'ASC')->get();
			}
			else if(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
			{
				if(Session::get('user_category')=='vendor')
				{
					$vendor_id = Session::get('vendor_id');
				}
				else
				{
					$manager = DB::table('managers')->select('vendor_id')->where('id', Session::get('manager_id'))->first();
					$vendor_id = $manager->vendor_id;
				}
				
				$Clients = DB::table('clients')->select('id','name','email')->where('vendor_id', $vendor_id)->where('is_deleted', 0)->where('status', 1)->orderBy('name', 'ASC')->orderBy('email', 'ASC')->get();
			}
			
			$Packages=DB::table('packages')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$VendorPackages=DB::table('vendor_packages')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$Groups=DB::table('groups')
			->leftJoin('group_categories', 'groups.group_category_id', '=', 'group_categories.id')
			->leftJoin('group_sub_categories', 'groups.group_sub_category_id', '=', 'group_sub_categories.id')
			->select('groups.*','group_categories.name as group_category_name','group_sub_categories.name as group_sub_category_name')
			->whereIn('groups.created_user_id', $UserArray)
			->where('groups.is_deleted', 0)
			->orderBy('groups.name', 'ASC')
			->get();
			
			//echo Session::get('client_id');die;
			$AllManagers = DB::table('managers')
			->where(function ($query) {
				if(Session::get('client_id') > 0)
				{
					$query->where('client_id', Session::get('client_id'));
				}
				else
				{
					$query->where('appointment_notifier', 'On')
					->orWhere('lead_notifier', 'On');
				}
			})
			->where('status', 1)
			->where('is_deleted', 0)
			->orderBy('full_name', 'ASC')
			->get();
			
			$GroupCategories=DB::table('group_categories')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$facebook_ads_accounts=DB::table('facebook_ads_accounts')->where('updated_user_id', Session::get('user_id'))->where('is_deleted', 0)->orderBy('facebook_account_name', 'ASC')->orderBy('account_id', 'ASC')->get();
			
			$Vendors = DB::table('users')->where('category', 'vendor')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
		    $invoice_settings=DB::table('invoice_prepopulate_fields')->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->first();
            
            $invoice_settings_common_for_all=DB::table('invoice_settings')->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->first();
            
            $closed_deals_cron=DB::table('cron_fetch_closed_deals')->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->select('status')->orderByDesc('id')->first();
            
            $closed_deals_settings=DB::table('closed_deals_settings')->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->first();
            
            //$facebook_pages = DB::table('facebook_pages As A')->leftJoin('page_engagements As B','A.id','=','B.facebook_page_id')->select('A.id', 'A.name')->whereIn('A.updated_user_id', $UserArray)->where('A.is_blocked', 0)->where('A.is_deleted', 0)->whereRaw('B.facebook_page_id IS NULL')->orderBy('A.name', 'ASC')->get();
			
			return view('panel.frontend.settings', ['Clients' => $Clients, 'LeadForms' => $LeadForms, 'Groups' => $Groups, 'GroupCategories' => $GroupCategories, 'GroupSubCategories' => $GroupSubCategories, 'Packages' => $Packages, 'VendorPackages' => $VendorPackages, 'EditPackage' => $EditPackage, 'EditVendorPackage' => $EditVendorPackage, 'GroupColumns' => $GroupColumns, 'ClientPackageColumns' => $ClientPackageColumns, 'VendorPackageColumns' => $VendorPackageColumns, 'WritePermission' => $WritePermission, 'ClientPermission' => $ClientPermission, 'facebook_ads_accounts' => $facebook_ads_accounts, 'Vendors' => $Vendors, 'FacebookAdsAccountTable' => $FacebookAdsAccountTable, 'invoice_settings' => $invoice_settings,"invoice_settings_common_for_all" => $invoice_settings_common_for_all, "closed_deals_cron" => $closed_deals_cron , 'closed_deals_settings'=>$closed_deals_settings , 'facebook_pages' => $facebook_pages, 'FacebookAdsLeads' => $FacebookAdsLeads, 'AllManagers' => $AllManagers, 'AdFormTable' => $AdFormTable, 'FacebookPageTable' => $FacebookPageTable]);
		}
		
		//Manage Group
		public function getGroupDetails(Request $request)
		{
			$UserArray = array();
			$WritePermission = 'Yes';
			$ClientPermission = 'Yes';
			$LeadList = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				$WritePermission = ManagerHelper::ManagerWritePermission('Setting Read Write');
				$ClientPermission = ManagerHelper::ManagerWritePermission('Clients Read Write');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
				{
					$LeadList = 'Yes';
				}
				else
				{
					$LeadList = 'No';
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			
			$GroupSubCategories=array();
			
			$ClientColumns=array();
			
			$Columns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Group Client Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$ClientColumns[]=$Column->column_value;
			}
			
			$group = DB::table('groups')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->where('id',$request->id)->first();
			
			if(is_null($group))
			{
				return redirect('settings')->with('alert_danger','Unauthorized group access failed');
			}
			
			$GroupSubCategories = DB::table('group_sub_categories')->where('group_category_id', $group->group_category_id)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();	
			
			$facebook_ads_lead_id = explode(",", $group->facebook_ads_lead_id);
			$facebook_page_id = explode(",", $group->facebook_page_id);
			$client_id = explode(",", $group->client_id);
			
			$LeadFormArray = array();
			$LeadFormQuery = DB::table('group_lead_forms')
			->leftJoin('groups', 'group_lead_forms.group_id', '=', 'groups.id')
			->select('group_lead_forms.facebook_ads_lead_id')
			->where('group_lead_forms.group_id', '!=', $request->id)
			->where('group_lead_forms.is_deleted',0)
			->where('groups.is_deleted',0)
			->groupBy('group_lead_forms.facebook_ads_lead_id')
			->get();
			foreach($LeadFormQuery as $row)
			{
				$LeadFormArray[] = $row->facebook_ads_lead_id;
			}
			
			$LeadForms = DB::table('facebook_ads_leads')->select('id', 'name')->whereNotIn('id', $LeadFormArray)->where('status', 'ACTIVE')->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$CLeadFormArray = array();
			$LeadFormQuery = DB::table('client_lead_forms')->leftJoin('clients', 'client_lead_forms.client_id', '=', 'clients.id')->select('client_lead_forms.facebook_ads_lead_id')->where('client_lead_forms.is_deleted',0)->where('clients.is_deleted',0)->groupBy('client_lead_forms.facebook_ads_lead_id')->get();
			foreach($LeadFormQuery as $row)
			{
				$CLeadFormArray[] = $row->facebook_ads_lead_id;
			}
			
			$LeadFormArray1=array();
			$LeadFormQuery1 = DB::table('group_lead_forms')
			->leftJoin('groups', 'group_lead_forms.group_id', '=', 'groups.id')
			->select('group_lead_forms.facebook_ads_lead_id')
			->where('group_lead_forms.is_deleted',0)
			->where('groups.is_deleted',0)
			->groupBy('group_lead_forms.facebook_ads_lead_id')
			->get();
			foreach($LeadFormQuery1 as $row1)
			{
				$LeadFormArray1[] = $row1->facebook_ads_lead_id;
			}
			
			$FacebookAdsLeads=DB::table('facebook_ads_leads')->select('id', 'name')->whereNotIn('id', $LeadFormArray1)->whereNotIn('id', $CLeadFormArray)->where('status', 'ACTIVE')->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			//
			$PageArray=array();
			$GroupPages = DB::table('group_page_engagements')
			->leftJoin('groups', 'group_page_engagements.group_id', '=', 'groups.id')
			->select('group_page_engagements.facebook_page_id')
			->where('group_page_engagements.group_id', '!=', $request->id)
			->where('group_page_engagements.is_deleted',0)
			->where('groups.is_deleted',0)
			->groupBy('group_page_engagements.facebook_page_id')
			->get();
			foreach($GroupPages as $GroupPage)
			{
				$PageArray[] = $GroupPage->facebook_page_id;
			}
			
			$ClientPages = DB::table('page_engagements')->leftJoin('clients', 'page_engagements.client_id', '=', 'clients.id')->select('page_engagements.facebook_page_id')->where('page_engagements.is_deleted',0)->where('clients.is_deleted',0)->groupBy('page_engagements.facebook_page_id')->get();
			foreach($ClientPages as $ClientPage)
			{
				$PageArray[] = $ClientPage->facebook_page_id;
			}
			
			$page_engagements = DB::table('facebook_pages')->select('id', 'name')->whereNotIn('id', $PageArray)->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			//
			
			if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
			{
				$Clients = DB::table('clients')->select('id','name','email')->where('status', 1)->where('is_deleted', 0)->orderBy('name', 'ASC')->orderBy('email', 'ASC')->get();
			}
			else if(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
			{
				if(Session::get('user_category')=='vendor')
				{
					$vendor_id = Session::get('vendor_id');
				}
				else
				{
					$manager = DB::table('managers')->select('vendor_id')->where('id', Session::get('manager_id'))->first();
					$vendor_id = $manager->vendor_id;
				}
				
				$Clients = DB::table('clients')->select('id','name','email')->where('vendor_id', $vendor_id)->where('is_deleted', 0)->where('status', 1)->orderBy('name', 'ASC')->orderBy('email', 'ASC')->get();
			}
			
			$Packages = DB::table('packages')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$GroupCategories = DB::table('group_categories')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$group_clients = DB::table('clients')
			->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
			->leftJoin('managers', 'clients.manager_id', '=', 'managers.id')
			->leftJoin('vendors', 'clients.vendor_id', '=', 'vendors.id')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->selectRAW('clients.client_no, clients.id, clients.name, clients.manager_id, clients.vendor_id, clients.status, clients.created_at, packages.name as package_name, managers.full_name as manager_name, vendors.name as vendor_name, count(DISTINCT facebook_ads_lead_users.id) as LDs, count(DISTINCT facebook_ads_lead_users.campaign_id) as CPs, count(DISTINCT facebook_ads_lead_users.ad_id) as Ads')
			->whereIn('clients.id', $client_id)
			->whereIn('facebook_ads_lead_users.facebook_ads_lead_id', $facebook_ads_lead_id)
			->where(function ($query) use ($facebook_ads_lead_id, $facebook_page_id){
				if(count($facebook_ads_lead_id) > 0)
				{
					$query->whereIn('facebook_ads_lead_users.facebook_ads_lead_id', $facebook_ads_lead_id)
					->where('facebook_ads_lead_users.lead_type', 'Pownder™ Lead');
				}
				else
				{
					$query->whereIn('facebook_ads_lead_users.facebook_page_id', $facebook_page_id)
					->where('facebook_ads_lead_users.lead_type', 'Pownder™ Messenger')
					->where('facebook_ads_lead_users.facebook_ads_lead_id', 0);
				}
			})
			->where('clients.status', 1)
			->where('clients.is_deleted', 0)
			->groupBy('clients.id')
			->orderBy('clients.name', 'ASC')
			->get();
			
			$PageArray=array();
			$GroupPages = DB::table('group_page_engagements')
			->leftJoin('groups', 'group_page_engagements.group_id', '=', 'groups.id')
			->select('group_page_engagements.facebook_page_id')
			->where('group_page_engagements.is_deleted',0)
			->where('groups.is_deleted',0)
			->groupBy('group_page_engagements.facebook_page_id')
			->get();
			foreach($GroupPages as $GroupPage)
			{
				$PageArray[] = $GroupPage->facebook_page_id;
			}
			
			$ClientPages = DB::table('page_engagements')
			->leftJoin('clients', 'page_engagements.client_id', '=', 'clients.id')
			->select('page_engagements.facebook_page_id')
			->where('page_engagements.is_deleted', 0)
			->where('clients.is_deleted', 0)
			->groupBy('page_engagements.facebook_page_id')
			->get();
			foreach($ClientPages as $ClientPage)
			{
				$PageArray[] = $ClientPage->facebook_page_id;
			}
			
			$facebook_pages = DB::table('facebook_pages')->select('id', 'name')->whereNotIn('id', $PageArray)->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
		    return view('panel.frontend.group', ['Clients' => $Clients, 'group_clients' => $group_clients, 'LeadForms' => $LeadForms, 'GroupCategories' => $GroupCategories, 'GroupSubCategories' => $GroupSubCategories, 'group' => $group, 'Packages' => $Packages, 'ClientColumns' => $ClientColumns, 'WritePermission' => $WritePermission, 'ClientPermission' => $ClientPermission, 'LeadList' => $LeadList, 'FacebookAdsLeads' => $FacebookAdsLeads, 'facebook_pages' => $facebook_pages, 'page_engagements' => $page_engagements]);
		}
		
		//-----------------------------------(Group Setting Start)---------------------------------------------------//
		//Add Group
		public function addGroupSubmit(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			$rule = ['name' => 'required', 'group_category_id' => 'required|integer', 'group_sub_category_id' => 'required', 'zipcode' => 'zip_code_valid'];
			
			$this->validate($request,$rule);
			
			$group_sub_category_id = '';
			if(count($request->group_sub_category_id) > 0)			
            {				
                $group_sub_category_id = implode(',', $request->group_sub_category_id);				
			}			
            
			$facebook_ads_lead_id = '';
			if(count($request->lead_form) > 0)			
            {				
                $facebook_ads_lead_id = implode(',', $request->lead_form);				
			}			
			
			$client_id = '';
			if(count($request->client_list) > 0)			
            {				
                $client_id = implode(',', $request->client_list);				
			}
			
			$facebook_page_id = '';
			if(count($request->page_engagements) > 0)			
            {				
                $facebook_page_id = implode(',', $request->page_engagements);				
			}			
			
			$group_id=DB::table('groups')->insertGetId([
			"name" => $request->name,
			"group_category_id" => $request->group_category_id,
			"group_sub_category_id" => $group_sub_category_id,
			"facebook_ads_lead_id" => $facebook_ads_lead_id,
			"facebook_page_id" => $facebook_page_id,
			"client_id" => $client_id,
			"group_type" => $request->group_type,
			"zipcode" => $request->zipcode,
			"milesradius" => $request->milesradius,
			"created_user_id" => Session::get('user_id'),
			'created_user_ip' => $request->ip(),
			"created_at" => $this->getcurrenttime(),
			"updated_user_id" => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			"updated_at" => $this->getcurrenttime()
			]);
			
			$client_list = $request->client_list;
			for($i = 0; $i < count($client_list); $i++)
			{
				if($client_list[$i] != '')
				{
					DB::table('client_groups')->insert([
					"client_id" => $client_list[$i],
					"group_id" => $group_id,
					"created_user_id" => Session::get('user_id'),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
			}
			
			$lead_form = $request->lead_form;
			for($i = 0; $i < count($lead_form); $i++)
			{
				if($lead_form[$i] != '')
				{
					DB::table('group_lead_forms')->insert([
					"group_id" => $group_id,
					"facebook_ads_lead_id" => $lead_form[$i],
					"created_user_id" => Session::get('user_id'),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
			}
			
			$page_engagements = $request->page_engagements;
			for($i = 0; $i < count($page_engagements); $i++)
			{
				if($page_engagements[$i] != '')
				{
					DB::table('group_page_engagements')->insert([
					"group_id" => $group_id,
					"facebook_page_id" => $page_engagements[$i],
					"created_user_id" => Session::get('user_id'),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
			}
			
			if(count($request->lead_form) > 0)
			{
				$NewClientForm = $request->lead_form;
			}
			else
			{
				$NewClientForm = array();
			}
			$NewClientId = $request->client_list;
			for($j=0; $j<count($NewClientId); $j++)
			{
				$client_id = $NewClientId[$j];
				$campaignForm = array();
				$campaignPage = array();
				$campaign = DB::table('campaigns')->where('client_id', $client_id)->where('is_deleted', 0)->orderBy('id', 'ASC')->first();
				if(!is_null($campaign))
				{
					$campaign_id = $campaign->id;
					$campaign_form = explode(",", $campaign->facebook_ads_lead_id);
					
					$campaignForm = array_unique(array_merge($campaign_form, $NewClientForm));
					
					$facebook_ads_leads = DB::table('facebook_ads_leads')->select('facebook_page_id')->whereIn('id', $campaignForm)->groupBy('facebook_page_id')->where('is_blocked', 0)->where('is_deleted', 0)->get();
					foreach($facebook_ads_leads as $facebook_ads_lead)
					{
						$campaignPage[] = $facebook_ads_lead->facebook_page_id;
					}
					
					$facebook_ads_lead_id = implode(',', $campaignForm);
					$facebook_page_id = implode(',', $campaignPage);
					
					DB::table('campaigns')->where('id', $campaign_id)
					->update(['facebook_page_id' => $facebook_page_id, 'facebook_ads_lead_id' => $facebook_ads_lead_id, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
					
					DB::table('campaign_logs')->insert([
					'client_id' => $client_id,
					'campaign_id' => $campaign_id,
					'facebook_page_id' => $facebook_page_id,
					'facebook_ads_lead_id' => $facebook_ads_lead_id,
					'created_user_id' => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					'created_at' => date('Y-m-d H:i:s'),
					'updated_user_id' => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					'updated_at' => date('Y-m-d H:i:s')
					]);
				}
			} 
			
			return redirect('settings')->with('alert_success','Group created successfully');
		}
		
		//Edit Group
		public function editGroupSubmit(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			$rule = ['name' => 'required', 'group_category_id' => 'required|integer', 'group_sub_category_id' => 'required', 'zipcode' => 'zip_code_valid'];
			
			$this->validate($request,$rule);
			
			$group = DB::table('groups')->where('id', $request->id)->first();
			
			$group_sub_category_id = '';
			if(count($request->group_sub_category_id) > 0)			
            {				
                $group_sub_category_id = implode(',', $request->group_sub_category_id);				
			}			
			
			$facebook_ads_lead_id = '';
			if(count($request->lead_form) > 0)			
            {				
                $facebook_ads_lead_id = implode(',', $request->lead_form);				
			}			
			
			$client_id = '';
			if(count($request->client_list) > 0)			
            {				
                $client_id = implode(',', $request->client_list);				
			}
			
			$facebook_page_id = '';
			if(count($request->page_engagements) > 0)			
            {				
                $facebook_page_id = implode(',', $request->page_engagements);				
			}
			
			DB::table('groups')->where('id', $request->id)->where('is_deleted', 0)
			->update([
			"name" => $request->name,
			"group_category_id" => $request->group_category_id,
			"group_sub_category_id" => $group_sub_category_id,
			"facebook_page_id" => $facebook_page_id,
			"facebook_ads_lead_id" => $facebook_ads_lead_id,
			"client_id" => $client_id,
			"group_type" => $request->group_type,
			"zipcode" => $request->zipcode,
			"milesradius" => $request->milesradius,
			"updated_user_id" => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			"updated_at" => $this->getcurrenttime()
			]);
			
			DB::table('client_groups')->where('group_id', $request->id)
			->update(['is_deleted' => 1]);
			
			$client_list = $request->client_list;
			for($i=0;$i<count($client_list);$i++)
			{
				if($client_list[$i]!='')
				{
					$checkExists=DB::table('client_groups')->where('group_id', $request->id)->where('client_id', $client_list[$i])->first();
					if(is_null($checkExists))
					{
						DB::table('client_groups')->insert([
						"client_id"=>$client_list[$i],
						"group_id"=>$request->id,
						"created_user_id"=>Session::get('user_id'),
						"created_at"=>$this->getcurrenttime(),
						"updated_user_id"=>Session::get('user_id'),
						"updated_at"=>$this->getcurrenttime()
						]);
					}
					else
					{
						DB::table('client_groups')->where('group_id', $request->id)->where('client_id', $client_list[$i])
						->update(["updated_user_id"=>Session::get('user_id'), "updated_at"=>$this->getcurrenttime(), "is_deleted" => 0]);
					}
				}
			}
			
			DB::table('group_lead_forms')->where('group_id', $request->id)
			->update(['is_deleted' => 1]);
			
			$lead_form = $request->lead_form;
			for($i=0;$i<count($lead_form);$i++)
			{
				if($lead_form[$i]!='')
				{
					$checkExists=DB::table('group_lead_forms')->where('group_id', $request->id)->where('facebook_ads_lead_id', $lead_form[$i])->first();
					if(is_null($checkExists))
					{
						DB::table('group_lead_forms')->insert([
						"group_id"=>$request->id,
						"facebook_ads_lead_id"=>$lead_form[$i],
						"created_user_id"=>Session::get('user_id'),
						"created_at"=>$this->getcurrenttime(),
						"updated_user_id"=>Session::get('user_id'),
						"updated_at"=>$this->getcurrenttime()
						]);
					}
					else
					{
						DB::table('group_lead_forms')->where('group_id', $request->id)->where('facebook_ads_lead_id', $lead_form[$i])
						->update(["updated_user_id" => Session::get('user_id'), "updated_at" => $this->getcurrenttime(), "is_deleted" => 0]);
					}
				}
			}
			
			DB::table('group_page_engagements')->where('group_id', $request->id)
			->update(['is_deleted' => 1]);
			
			$page_engagements = $request->page_engagements;
			for($i = 0; $i < count($page_engagements); $i++)
			{
				if($page_engagements[$i] != '')
				{
					$checkExists=DB::table('group_page_engagements')->where('group_id', $request->id)->where('facebook_page_id', $page_engagements[$i])->first();
					if(is_null($checkExists))
					{
						DB::table('group_page_engagements')->insert([
						"group_id" => $request->id,
						"facebook_page_id" => $page_engagements[$i],
						"created_user_id" => Session::get('user_id'),
						"created_at" => $this->getcurrenttime(),
						"updated_user_id" => Session::get('user_id'),
						"updated_at" => $this->getcurrenttime()
						]);
					}
					else
					{
						DB::table('group_page_engagements')->where('group_id', $request->id)->where('facebook_page_id', $page_engagements[$i])
						->update(["updated_user_id" => Session::get('user_id'), "updated_at" => $this->getcurrenttime(), "is_deleted" => 0]);
					}
				}
			}
			
			$OldClientForm = explode(",", $group->facebook_ads_lead_id);
			$OldClientId = explode(",", $group->client_id);
			for($i=0; $i<count($OldClientId); $i++)
			{
				$client_id = $OldClientId[$i];
				$campaignForm = array();
				$campaignPage = array();
				$campaign = DB::table('campaigns')->where('client_id', $client_id)->where('is_deleted', 0)->orderBy('id', 'ASC')->first();
				if(!is_null($campaign))
				{
					$campaign_id = $campaign->id;
					$campaign_form = explode(",", $campaign->facebook_ads_lead_id);
					
					$campaignForm = array_diff($campaign_form, $OldClientForm);
					
					$facebook_ads_leads = DB::table('facebook_ads_leads')->select('facebook_page_id')->whereIn('id', $campaignForm)->groupBy('facebook_page_id')->where('is_blocked', 0)->where('is_deleted', 0)->get();
					foreach($facebook_ads_leads as $facebook_ads_lead)
					{
						$campaignPage[] = $facebook_ads_lead->facebook_page_id;
					}
					
					$facebook_ads_lead_id = implode(',', $campaignForm);
					$facebook_page_id = implode(',', $campaignPage);
					
					DB::table('campaigns')->where('id', $campaign_id)
					->update(['facebook_page_id' => $facebook_page_id, 'facebook_ads_lead_id' => $facebook_ads_lead_id, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
					
					DB::table('campaign_logs')->insert([
					'client_id' => $client_id,
					'campaign_id' => $campaign_id,
					'facebook_page_id' => $facebook_page_id,
					'facebook_ads_lead_id' => $facebook_ads_lead_id,
					'created_user_id' => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					'created_at' => date('Y-m-d H:i:s'),
					'updated_user_id' => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					'updated_at' => date('Y-m-d H:i:s')
					]);
				}
			}
			
			if(count($request->lead_form) > 0)
			{
				$NewClientForm = $request->lead_form;
			}
			else
			{
				$NewClientForm = array();
			}
			$NewClientId = $request->client_list;
			for($j=0; $j<count($NewClientId); $j++)
			{
				$client_id = $NewClientId[$j];
				$campaignForm = array();
				$campaignPage = array();
				$campaign = DB::table('campaigns')->where('client_id', $client_id)->where('is_deleted', 0)->orderBy('id', 'ASC')->first();
				if(!is_null($campaign))
				{
					$campaign_id = $campaign->id;
					$campaign_form = explode(",", $campaign->facebook_ads_lead_id);
					$campaign_form = array_diff($campaign_form, $OldClientForm);
					$campaignForm = array_merge($campaign_form, $NewClientForm);
					
					$facebook_ads_leads = DB::table('facebook_ads_leads')->select('facebook_page_id')->whereIn('id', $campaignForm)->groupBy('facebook_page_id')->where('is_blocked', 0)->where('is_deleted', 0)->get();
					foreach($facebook_ads_leads as $facebook_ads_lead)
					{
						$campaignPage[] = $facebook_ads_lead->facebook_page_id;
					}
					
					$facebook_ads_lead_id = implode(',', $campaignForm);
					$facebook_page_id = implode(',', $campaignPage);
					
					DB::table('campaigns')->where('id', $campaign_id)
					->update(['facebook_page_id' => $facebook_page_id, 'facebook_ads_lead_id' => $facebook_ads_lead_id, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
					
					DB::table('campaign_logs')->insert([
					'client_id' => $client_id,
					'campaign_id' => $campaign_id,
					'facebook_page_id' => $facebook_page_id,
					'facebook_ads_lead_id' => $facebook_ads_lead_id,
					'created_user_id' => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					'created_at' => date('Y-m-d H:i:s'),
					'updated_user_id' => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					'updated_at' => date('Y-m-d H:i:s')
					]);
				}
			}  
			
			return redirect('settings')->with('alert_success','Group updated successfully');
		}
		
		//Delete Group
		public function deleteGroup(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			$group_id = $request->id;
			
			$clients=DB::table('clients')
            ->leftJoin('client_groups', 'clients.id', '=', 'client_groups.client_id')
			->where('clients.is_deleted', 0)
			->where('client_groups.is_deleted', 0)
			->where('client_groups.group_id', $group_id)
			->where('clients.created_user_id', Session::get('user_id'))
			->where('client_groups.created_user_id', Session::get('user_id'))
			->get();
			
			if(count($clients)==0)
			{			
				DB::table('groups')->where('id', $group_id)->where('is_deleted', 0)
				->update(["updated_user_id" => Session::get('user_id'), 'updated_user_ip' => $request->ip(), "updated_at" => $this->getcurrenttime(), "is_deleted" => 1]);
				
				DB::table('client_groups')->where('group_id', $group_id)->where('is_deleted', 0)
				->update(["updated_user_id" => Session::get('user_id'), 'updated_user_ip' => $request->ip(), "updated_at" => $this->getcurrenttime(), "is_deleted" => 1]);
				
				return "Yes";
			}
			else
			{
				return count($clients);
			}
		}
		//-----------------------------------(Group Setting End)---------------------------------------------------//
		
		//-----------------------------------(Package Setting Start)---------------------------------------------------//
		public function addPackageSubmit(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			DB::table('packages')->insertGetId([
			"name"=>$request->package_name,
			"package_type"=>$request->package_type,
			"per_lead_value"=>$request->per_lead_value,
			"monthly_lead_quantity"=>$request->monthly_lead_quantity,
			"amount"=>$request->amount,
			"created_user_id"=>Session::get('user_id'),
			"created_at"=>$this->getcurrenttime(),
			"updated_user_id"=>Session::get('user_id'),
			"updated_at"=>$this->getcurrenttime()
			]);
			
			return redirect('settings')->with('alert_success','Client package created successfully');
		}
		
		public function editPackageSubmit(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			DB::table('packages')->where('id', $request->package_id)->where('is_deleted', 0)
			->update([
			"name"=>$request->package_name,
			"package_type"=>$request->package_type,
			"per_lead_value"=>$request->per_lead_value,
			"monthly_lead_quantity"=>$request->monthly_lead_quantity,
			"amount"=>$request->amount,
			"updated_user_id"=>Session::get('user_id'),
			"updated_at"=>$this->getcurrenttime()
			]);
			
			return redirect('settings')->with('alert_success','Client package updated successfully');
		}
		
		public function deletePackage(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			$package_id=$request->id;
			
			$clients=DB::table('clients')->select('id')->where('package_id', $package_id)->where('is_deleted', 0)->get();
			
			if(count($clients)==0)
			{
				DB::table('packages')->where('id', $package_id)->where('is_deleted', 0)
				->update(["updated_user_id"=>Session::get('user_id'), "updated_at"=>$this->getcurrenttime(), "is_deleted"=>1]); 
				
				return 'Yes';
			}
			else
			{
				return count($clients);
			}
		}
		//-----------------------------------(Package Setting End)---------------------------------------------------//
		
		//-----------------------------------(Vendor Package Setting Start)---------------------------------------------------//
		public function addVendorPackageSubmit(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			$rule=[
			'vendor_package_name'=>'required',
			'vendor_client_limits'=>'required',
			//'vendor_client_limit_integer'=>'integer',
			'vendor_ads_accounts'=>'required',
			//'vendor_ads_account_integer'=>'integer',
			'vendor_campaigns'=>'required',
			//'vendor_campaign_integer'=>'integer',
			'vendor_leads_amount'=>'required',
			//'vendor_leads_amount_integer'=>'integer',
			'vendor_fb_messenger'=>'required',
			];
			
			$this->validate($request,$rule);
			
			DB::table('vendor_packages')->insert([
			"name"=>$request->vendor_package_name,
			"client_limit"=>$request->vendor_client_limits,
			"client_limits"=>isset($request->vendor_client_limit_integer)?$request->vendor_client_limit_integer:0,
			"ads_account"=>$request->vendor_ads_accounts,
			"ads_accounts"=>isset($request->vendor_ads_account_integer)?$request->vendor_ads_account_integer:0,
			"campaign"=>$request->vendor_campaigns,
			"campaigns"=>isset($request->vendor_campaign_integer)?$request->vendor_campaign_integer:0,
			"leads_amount"=>$request->vendor_leads_amount,
			"leads_amounts"=>isset($request->vendor_leads_amount_integer)?$request->vendor_leads_amount_integer:0,
			"fb_messenger"=>$request->vendor_fb_messenger,
			"created_user_id"=>Session::get('user_id'),
			"created_at"=>$this->getcurrenttime(),
			"updated_user_id"=>Session::get('user_id'),
			"updated_at"=>$this->getcurrenttime()
			]);
			
			return redirect('settings')->with('alert_success','Vendor package created successfully');
		}
		
		public function editVendorPackageSubmit(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			$rule=[
			'vendor_package_name'=>'required',
			'vendor_client_limits'=>'required',
			//'vendor_client_limit_integer'=>'integer',
			'vendor_ads_accounts'=>'required',
			//'vendor_ads_account_integer'=>'integer',
			'vendor_campaigns'=>'required',
			//'vendor_campaign_integer'=>'integer',
			'vendor_leads_amount'=>'required',
			//'vendor_leads_amount_integer'=>'integer',
			'vendor_fb_messenger'=>'required',
			];
			
			$this->validate($request,$rule);
			
			DB::table('vendor_packages')->where('id', $request->vendor_package_id)->where('is_deleted', 0)
			->update([
			"name"=>$request->vendor_package_name,
			"client_limit"=>$request->vendor_client_limits,
			"client_limits"=>isset($request->vendor_client_limit_integer)?$request->vendor_client_limit_integer:0,
			"ads_account"=>$request->vendor_ads_accounts,
			"ads_accounts"=>isset($request->vendor_ads_account_integer)?$request->vendor_ads_account_integer:0,
			"campaign"=>$request->vendor_campaigns,
			"campaigns"=>isset($request->vendor_campaign_integer)?$request->vendor_campaign_integer:0,
			"leads_amount"=>$request->vendor_leads_amount,
			"leads_amounts"=>isset($request->vendor_leads_amount_integer)?$request->vendor_leads_amount_integer:0,
			"fb_messenger"=>$request->vendor_fb_messenger,
			"updated_user_id"=>Session::get('user_id'),
			"updated_at"=>$this->getcurrenttime()
			]);
			
			return redirect('settings')->with('alert_success','Vendor package updated successfully');
		}
		
		public function deleteVendorPackage(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			$vendor_package_id=$request->id;
			
			$vendors=DB::table('vendors')->select('id')->where('vendor_package_id', $vendor_package_id)->where('is_deleted', 0)->get();
			
			if(count($vendors)==0)
			{
				DB::table('vendor_packages')->where('id', $vendor_package_id)->where('is_deleted', 0)
				->update(["updated_user_id" => Session::get('user_id'), "updated_at" => date('Y-m-d H:i:s'), "is_deleted" => 1]);
				
				return "Yes";
			}
			else
			{
				return count($vendors);
			}
		}
		//-----------------------------------(Vendor Package Setting End)---------------------------------------------------//
		
		//-----------------------------------(Group Category Setting Start)---------------------------------------------------//
		public function createGroupCategory(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			DB::table('group_categories')->insertGetId([
			"name"=>$request->group_category_name,
			"created_user_id"=>Session::get('user_id'),
			"created_at"=>$this->getcurrenttime(),
			"updated_user_id"=>Session::get('user_id'),
			"updated_at"=>$this->getcurrenttime()
			]);	
			
			$GroupCategories=DB::table('group_categories')->select('id', 'name')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return response()->json($GroupCategories);
		}
		//-----------------------------------(Group Category Setting End)---------------------------------------------------//
		
		//-----------------------------------(Group Sub Category Setting Start)---------------------------------------------------//
		public function createGroupSubCategory(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			DB::table('group_sub_categories')->insert([
			"group_category_id"=>$request->group_category_id,
			"name"=>$request->group_sub_category_name,
			"created_user_id"=>Session::get('user_id'),
			"created_at"=>$this->getcurrenttime(),
			"updated_user_id"=>Session::get('user_id'),
			"updated_at"=>$this->getcurrenttime()
			]);	
			
			$GroupSubCategories=DB::table('group_sub_categories')->select('id', 'name')->where('group_category_id', $request->group_category_id)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return response()->json($GroupSubCategories);
		}
		//-----------------------------------(Group Sub Category Setting End)---------------------------------------------------//
		
		//-----------------------------------(Get Package Vendor)---------------------------------------------------//
		public function getPackageVendorDetail(Request $request)
		{
			$PackageVendors=DB::table('vendors')->where('vendor_package_id', $request->id)->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return response()->json($PackageVendors);
		}
		
		//-----------------------------------(Get Package Client)---------------------------------------------------//
		public function getPackageClientDetail(Request $request)
		{
			$PackageClients=DB::table('clients')->where('package_id', $request->id)->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return response()->json($PackageClients);
		}
		
		//-----------------------------------(Get Package Client)---------------------------------------------------//
		public function getGroupClientDetail(Request $request)
		{
			$group_id=$request->id;
			
			$GroupClients=DB::table('clients')
            ->leftJoin('client_groups', 'clients.id', '=', 'client_groups.client_id')
			->select('clients.*')
			->where('clients.is_deleted', 0)
			->where('client_groups.is_deleted', 0)
			->where('client_groups.group_id', $group_id)
			->where('clients.created_user_id', Session::get('user_id'))
			->where('client_groups.created_user_id', Session::get('user_id'))
			->get();
			
			return response()->json($GroupClients);
		}
		
		//-----------------------------------(Get Group Sub Category)---------------------------------------------------//
		public function getGroupClient()
		{
			$Clients=DB::table('clients')->select('id','name','email')->where('status', 1)->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->orderBy('name', 'ASC')->orderBy('email', 'ASC')->get();
			
			return response()->json($Clients);
		}
		
		//-----------------------------------(Group Client Create)---------------------------------------------------//
		public function createGroupClient(Request $request)
		{
			if(Session::get('user_category') == 'user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			if ($request->hasFile('client_photo')) 
			{
				$rule = [
				'client_name' => 'required',
				'client_address' => 'required',
				'client_zip' => 'required|integer|zip_code_valid',
				'client_mobile' => 'required|phone_format|unique:clients,mobile',
				'client_email' => 'required|unique:users,email|email',
				'client_url' => 'required|url',
				'client_crm_email'=>'required',
				'client_package_id'=>'required',
				'client_group_category_id'=>'required',
				'client_group_sub_category_id'=>'required',
				'client_roi_automation'=>'required',
				'client_start_date'=>'required',
				'client_pro_rate'=>'required',
				'client_photo'=>'image|dimensions:width=180,height=180',
                'page_engagements'=>'nullable|in:On,Off'
				];
			}
			else
			{
				$rule=[
				'client_name'=>'required',
				'client_address'=>'required',
				'client_zip'=>'required|integer|zip_code_valid',
				'client_mobile'=>'required|phone_format|unique:clients,mobile',
				'client_email'=>'required|unique:users,email|email',
				'client_url'=>'required|url',
				'client_crm_email'=>'required',
				'client_roi_automation'=>'required',
				'client_group_category_id'=>'required',
				'client_group_sub_category_id'=>'required',
				'client_start_date'=>'required',
				'client_pro_rate'=>'required',
				'client_package_id'=>'required',
                'page_engagements'=>'nullable|in:On,Off'
				];
			}
			
			$this->validate($request,$rule);
			
			$client_lead_form = json_decode($request->client_lead_form);
			$facebook_pages = json_decode($request->facebook_pages);
			
			$facebook_ads_lead_id = '';
			if(count($client_lead_form) > 0)			
			{				
				$facebook_ads_lead_id = implode(',', $client_lead_form);				
			}			
			
			$facebook_page_id = '';	
			if(count($facebook_pages)>0)			
			{				
				$facebook_page_id = implode(',', $facebook_pages);				
			}			
			
			$photo='';
			if ($request->hasFile('client_photo')) 
			{
				$image = $request->file('client_photo');
				$photo = date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/clients/',$photo);
			} 
			
			$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($request->client_zip)."&sensor=false";
			$result_string = file_get_contents($url);
			$result = json_decode($result_string, true);
			$val = isset($result['results'][0]['geometry']['location']) ? $result['results'][0]['geometry']['location'] : array();
			$latitude = isset($val['lat']) ? $val['lat'] : '';
			$longitude = isset($val['lng']) ? $val['lng'] : '';
			
			
			$s_date = explode("-",$request->client_start_date);
			$start_date = $s_date[2].'-'.$s_date[0].'-'.$s_date[1];
            
            $page_engagement_status=($request->has('page_engagements')) ? $request->page_engagements : '';
			
			$client_id = DB::table('clients')->insertGetId([
			'vendor_id' => Session::get('vendor_id'),
			'name' => $request->client_name,
			'address' => $request->client_address,
			'zip' => $request->client_zip,
			'mobile' => $request->client_mobile,
			'email' => $request->client_email,
			'url' => $request->client_url,
			'group_category_id' => $request->client_group_category_id,
			'group_sub_category_id' => $request->client_group_sub_category_id,
			'contact_name' => $request->client_contact_name,
			'package_id' => $request->client_package_id,
			'per_sold_value' => $request->client_per_sold_value,
			'discount' => $request->client_discount,
			'is_override' => $request->client_is_override,
			'crm_email' => $request->client_crm_email,
			'roi_automation' => $request->client_roi_automation,
			'roi_id' => $request->client_roi_id,
            'page_engagements' => $page_engagement_status,
			'photo' => $photo,
			'start_date' => $start_date,
			'pro_rate' => $request->client_pro_rate,
			'latitude' => $latitude,
			'longitude' => $longitude,
			'facebook_ads_lead_id' => $facebook_ads_lead_id,
			'facebook_page_id' => $facebook_page_id,
			'created_user_id' => Session::get('user_id'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_user_id' => Session::get('user_id'),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			if($client_id > 999){
				$client_no = 'POW'.$client_id;
				}elseif($client_id > 99){
				$client_no = 'POW0'.$client_id;
				}elseif($client_id > 9){
				$client_no = 'POW00'.$client_id;
				}else{
				$client_no = 'POW000'.$client_id;
			}
			
			DB::table('clients')->where('id', $client_id)->update(['client_no' => $client_no]);
            
            if($request->has('page_engagements'))
            {
                if($request->page_engagements=="On")
                {
					for($i = 0; $i < count($facebook_pages); $i++)
					{
						if($facebook_pages[$i]!='')
						{
							DB::table('page_engagements')->insert([
							"client_id" => $client_id,
							"facebook_page_id" => $facebook_pages[$i],
							"created_user_id" => Session::get('user_id'),
							"created_at" => date('Y-m-d H:i:s'),
							"updated_user_id" => Session::get('user_id'),
							"updated_at" => date('Y-m-d H:i:s')
							]);
						}
					}
				}                
			}
			
			for($i = 0; $i < count($client_lead_form); $i++)
			{
				if($client_lead_form[$i]!='')
				{
					DB::table('client_lead_forms')->insert([
					"client_id" => $client_id,
					"facebook_ads_lead_id" => $client_lead_form[$i],
					"created_user_id" => Session::get('user_id'),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
			}
			
			do 
			{
				$user_password = Helper::GenerateRandomPassword(8,true,true,true,true);
			}
			while($user_password=="FALSE");
			
			$password=password_hash($user_password,PASSWORD_BCRYPT); 
			
			DB::table('users')->insert([
			'client_id' => $client_id,
			'name' => $request->client_name,
			'email' => $request->client_email,
            'address' => $request->client_address,
			'password' => $password,
			'category' => 'client',
			'created_user_id' => Session::get('user_id'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_user_id' => Session::get('user_id'),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			$data = array(
			'name' => $request->client_name,
			'email' => $request->client_email,
			'user_password' => $user_password
			);
			
			Mail::send('emails.login_details', $data, function ($message) use ($request) {
				$message->from('noreply@pownder.com', 'Pownder');
				$message->to($request->client_email)->subject('Pownder™ Credentials');
			});
		}
		
		public function getClientPackageList(Request $request)
		{
			$UserArray = array();
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			
			$offset = isset($request->offset) ? $request->offset : 0;
			$limit = isset($request->limit) ? $request->limit : 9999999999;
			
			if($limit=='All' )
			{
				$limit=9999999999;
			}
			$order = $request->order;
			if(!isset($request->sort))
			{
				$order = 'desc';
			}
			
			$sortString=isset($request->sort)?$request->sort:'Name';
			
			$search=isset($request->search)?$request->search:'';
			
			switch($sortString) 
			{
				case 'Created Time':
				$sort = 'created_at';
				break;
				case 'Name':
				$sort = 'name';
				break;
				case 'Type':
				$sort = 'package_type';
				break;
				case 'Per Lead Value':
				$sort = 'per_lead_value';
				break;
				case 'Monthly Lead Quantity':
				$sort = 'monthly_lead_quantity';
				break;
				case 'Package Value (Amount $)':
				$sort = 'amount';
				break;
				
				default:
				$sort = 'name';
			}
			
			$data=array();
			$rows=array();
			
			$columns=['name','package_type','per_lead_value','monthly_lead_quantity','amount','created_at'];
			
			$TotalRecords=DB::table('packages')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->whereIn('created_user_id', $UserArray)
			->where('is_deleted', 0)
			->orderBy($sort, $order)
			->get();
			
			$results=DB::table('packages')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->whereIn('created_user_id', $UserArray)
			->where('is_deleted', 0)
			->orderBy($sort, $order)
			->skip($offset)
			->take($limit)
			->get();
			
			$sno=$offset+1;
			foreach($results as $result)
			{
				$package=array();
				$package['#']=$sno++;
				$package['Created Time']=date('m-d-Y h:i:s A',strtotime($result->created_at));
				$package['Name']=$result->name;
				$package['Type']=$result->package_type;
				if($result->per_lead_value=='')
				{
					$package['Per Lead Value']='';
				}
				else
				{
					$package['Per Lead Value']='$'.number_format($result->per_lead_value,0);
				}
				
				$package['Monthly Lead Quantity']=number_format($result->monthly_lead_quantity,0);
				
				if($result->amount=='')
				{
					$package['Package Value (Amount $)']='';
				}
				else
				{
					$package['Package Value (Amount $)']='$'.number_format($result->amount,0);
				}
				
				$package['Action']='<div class="ui-group-buttons"> <form style="display: inline;"> <input type="hidden" name="package_id" value="'.$result->id.'" /> <button type="submit" title="Edit"  class="btn btn-success" role="button"><span class="glyphicon glyphicon-edit"></span></button> </form> <button type="button" title="Delete" class="btn btn-danger package-delete" role="button" onClick="deleteClientPackage('.$result->id.')"><span class="glyphicon glyphicon-trash"></span></button> </div>';
				$rows[]=$package;
			}
			
			$data['total']=count($TotalRecords);
			$data['rows']=$rows;
			
			return \Response::json($data);
		}
		
		public function postEmailNotification(Request $request)
		{
			$noyification_type = $request->noyification_type;
			$noyification_status = $request->noyification_status;
			
			$email_notification = DB::table('email_notifications')->where('noyification_type', $noyification_type)->where('created_user_id', Session::get('user_id'))->first();
			if(is_null($email_notification))
			{
				DB::table('email_notifications')->insert([
				"noyification_type"=>$request->noyification_type,
				"noyification_status"=>$request->noyification_status,
				"created_user_id"=>Session::get('user_id'),
				"created_user_ip"=>$request->ip(),
				"created_at"=>$this->getcurrenttime(),
				"updated_user_id"=>Session::get('user_id'),
				"updated_user_ip"=>$request->ip(),
				"updated_at"=>$this->getcurrenttime()
				]);
			}
			else
			{
				DB::table('email_notifications')
				->where('noyification_type', $noyification_type)
				->where('created_user_id', Session::get('user_id'))
				->update([
				"noyification_type"=>$request->noyification_type,
				"noyification_status"=>$request->noyification_status,
				"updated_user_id"=>Session::get('user_id'),
				"updated_user_ip"=>$request->ip(),
				"updated_at"=>$this->getcurrenttime()
				]);
			}
		}
		
		//Client Setting
		public function getClientSettings(Request $request)
		{
			$Manager_Array = array();
			
			$Results = DB::table('managers')->select('id')->where('email_notification', 'On')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->get();
			foreach($Results as $Result)
			{
				$Manager_Array[] = $Result->id;
			}
			
			$Managers = DB::table('managers')
			->leftJoin('departments', 'managers.department_id', '=', 'departments.id')
			->leftJoin('job_titles', 'managers.job_title_id', '=', 'job_titles.id')
			->leftJoin('teams', 'managers.team_id', '=', 'teams.id')
			->select('managers.*', 'departments.name as department_name', 'job_titles.name as job_title_name', 'teams.name as team_name')
			->where('managers.created_user_id', Session::get('user_id'))
			->where('managers.is_deleted', 0)
			->get();
			
			return view('panel.frontend.client_setting', ['Managers' => $Managers, 'Manager_Array' => $Manager_Array]);
		}
		
		public function postUserEmailNotification(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Setting Read Write');
			}
			
			DB::table('managers')->where('created_user_id', Session::get('user_id'))
			->update(['email_notification' => 'Off', 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			
			if(count($request->manager_list)>0)
			{
				DB::table('managers')->whereIn('id', $request->manager_list)
				->update(['email_notification' => 'On', 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			return redirect('csettings')->with('alert_success','Email notification updated successfully');
		}
		
		public function postUserLeadNotifier(Request $request)
		{
			DB::table('managers')->where('id', $request->id)
			->update(['lead_notifier' => $request->lead_notifier, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
		}
		
		public function postAllUserLeadNotifier(Request $request)
		{
			if($request->client_id > 0)
			{
				DB::table('managers')->where('client_id', $request->client_id)->where('is_deleted', 0)
				->update(['lead_notifier' => $request->lead_notifier, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			else
			{
				DB::table('managers')
				->where(function ($query) {
					$query->where('appointment_notifier', 'On')
					->orWhere('lead_notifier', 'On');
				})
				->where('is_deleted', 0)
				->update(['lead_notifier' => $request->lead_notifier, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
		}
		
		public function postUserAppointmentNotifier(Request $request)
		{
			DB::table('managers')
			->where('id', $request->id)
			->update(['appointment_notifier' => $request->appointment_notifier, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
		}
		
		public function postAllUserAppointmentNotifier(Request $request)
		{
			if($request->client_id > 0)
			{
				DB::table('managers')->where('client_id', $request->client_id)->where('is_deleted', 0)
				->update(['appointment_notifier' => $request->appointment_notifier, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			else
			{
				DB::table('managers')
				->where(function ($query) {
					$query->where('appointment_notifier', 'On')
					->orWhere('lead_notifier', 'On');
				})
				->where('is_deleted', 0)
				->update(['appointment_notifier' => $request->appointment_notifier, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
		}
		
        //Update Payment terms conditions
        public function updatePaymentTNC(Request $request)
        {
			$rule=["payment_tnc"=>"required"];
			$this->validate($request,$rule);
			$already_exist=DB::table('invoice_prepopulate_fields')->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->count('id');
			if(!$already_exist)
			{
				DB::table('invoice_prepopulate_fields')->insert([
				'created_user_id'=>session('user')->id,
				'created_user_category'=>session('user')->category,
				'payment_tnc'=>$request->payment_tnc,
				'created_at'=>$this->getcurrenttime()
				]);
			}
			else
			{
				DB::table('invoice_prepopulate_fields')
				->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])
				->update(['payment_tnc'=>$request->payment_tnc,'updated_at'=>$this->getcurrenttime()]);
			}
			return Redirect::back()->with('alert_success','Payment  Terms & Conditions updated successfully.'); 
		}
		
        //Update invoice settings 
        public function updateInvoiceSetting(Request $request)
        {
			if($request->type=="cc_charge")
            {
				$rule=["cc_charge"=>"required|numeric"];
				$msg='Credit card processing fee updated successfully.';
			}
            else if($request->type=="invoice_email")
            {
				$rule=["invoice_email"=>"required|email"];
				$msg='Invoice from email updated successfully.';   
			}
            else if($request->type=="invoice_subject")
            {
				$rule=["invoice_subject"=>"required|max:300"];
				$msg='Invoice subject updated successfully.';   
			}
            else if($request->type=="invoice_msg")
            {
				$rule=["invoice_msg"=>"required"];
				$msg='Invoice email text updated successfully.';   
			}
            else if($request->type=="invoice_tnc")
            {
				$rule=["invoice_tnc"=>"required"];
				$msg='Invoice terms and conditions updated successfully.';   
			}
            else if($request->type=="invoice_reminder")
            {
				$rule=["reminder_subject"=>"required|max:300","reminder_msg"=>"required"];
				$msg='Reminder subject and message updated successfully.';   
			}
            
			$this->validate($request,$rule);
			$already_exist=DB::table('invoice_settings')->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->count('id');
			if(!$already_exist)
			{
				$values=[
				'created_user_id'=>session('user')->id,
				'created_user_category'=>session('user')->category,
				'created_at'=>$this->getcurrenttime()
				];
                if($request->type=="cc_charge")
                {
					$values['cc_charge']=$request->cc_charge;  
				}
                else if($request->type=="invoice_email")
                {
					$values['invoice_email']=$request->invoice_email;     
				}
                else if($request->type=="invoice_subject")
                {
					$values['invoice_subject']=$request->invoice_subject;     
				}
                else if($request->type=="invoice_msg")
                {
					$values['invoice_msg']=$request->invoice_msg;     
				}
                else if($request->type=="invoice_tnc")
                {
					$values['invoice_tnc']=$request->invoice_tnc;     
				}
                else if($request->type=="invoice_reminder")
                {
					$values['reminder_msg']=$request->reminder_msg;
                    $values['reminder_subject']=$request->reminder_subject;      
				}                
				
                DB::table('invoice_settings')->insert($values);
			}
			else
			{
				$values=['updated_at'=>$this->getcurrenttime()];
				if($request->type=="cc_charge")
                {
					$values['cc_charge']=$request->cc_charge;  
				}
                else if($request->type=="invoice_email")
                {
					$values['invoice_email']=$request->invoice_email;     
				}
                else if($request->type=="invoice_subject")
                {
					$values['invoice_subject']=$request->invoice_subject;     
				}
                else if($request->type=="invoice_msg")
                {
					$values['invoice_msg']=$request->invoice_msg;     
				}
                else if($request->type=="invoice_tnc")
                {
					$values['invoice_tnc']=$request->invoice_tnc;     
				}
                else if($request->type=="invoice_reminder")
                {
					$values['reminder_msg']=$request->reminder_msg;
                    $values['reminder_subject']=$request->reminder_subject;      
				} 
                
                DB::table('invoice_settings')
				->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])
				->update($values);
			}
			return Redirect::back()->with('alert_success',$msg); 
		}
        //Fetch closed deals
        public function fetchClosedDeals(Request $request)
        {
            if(session('user')->category!="admin")
            {
                abort(404); 
			}
            
            $already_running=DB::table('cron_fetch_closed_deals')->whereIn('status',[1,2])->count('id');
            
            if(!$already_running)
            {
				DB::table('cron_fetch_closed_deals')->insert(["created_user_id"=>session('user')->id,"created_user_category"=>session('user')->category,"created_at"=>$this->getcurrenttime()]);
				
				return redirect()->back()->with('alert_success','Your request has been saved. It may take some time to complete the process.');
				
			}
            else
            {
				return redirect()->back()->with('alert_danger','Oops! a process is already running. please try after some time.');  
			}            
		}
        
        //Update closed deals settings
        public function updateClosedDealsSetting(Request $request)
        {          
			if($request->type=="time")
			{
				$rule=
				[
				"time1"=>"required|date_format:H:i",
				"time2"=>"required|date_format:H:i|after:time1",
				"time3"=>"required|date_format:H:i|after:time2"
				];
				$msg='Data fetching times updated successfully.';
			}  
            $this->validate($request,$rule);
           	
            $already_exist=DB::table('closed_deals_settings')->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])->count('id');
			
            if(!$already_exist)
			{
				$values=[
				'created_user_id'=>session('user')->id,
				'created_user_category'=>session('user')->category,
				'created_at'=>$this->getcurrenttime()
				];
                if($request->type=="time")
                {
					$values['fetch_time1']=$request->time1; 
					$values['fetch_time2']=$request->time2;
					$values['fetch_time3']=$request->time3; 
				}                
				
                DB::table('closed_deals_settings')->insert($values);
			}
            else
            {
				$values=['updated_at'=>$this->getcurrenttime()];
                if($request->type=="time")
                {
					$values['fetch_time1']=$request->time1; 
					$values['fetch_time2']=$request->time2;
					$values['fetch_time3']=$request->time3; 
				}
                DB::table('closed_deals_settings')
				->where([['created_user_id',session('user')->id],['created_user_category',session('user')->category]])
				->update($values);
			} 
            return Redirect::back()->with('alert_success',$msg);
		}
		
		//Check Client Lead Form
        public function checkClientLeadForm(Request $request)
        {          
			$client_id = explode(",", $request->client_id);
			$facebook_ads_lead_id = explode(",", $request->facebook_ads_lead_id);
			
			$client_lead_forms = DB::table('client_lead_forms')
			->leftJoin('clients', 'client_lead_forms.client_id', '=', 'clients.id')
			->select('client_lead_forms.id')
			->whereIn('client_lead_forms.facebook_ads_lead_id', $facebook_ads_lead_id)
			->whereNotIn('client_lead_forms.client_id', $client_id)
			->where('client_lead_forms.is_deleted',0)
			->where('clients.is_deleted',0)
			->count('client_lead_forms.id'); 
			
			return $client_lead_forms;
		}
		
		//Facebook Page List
		public function getFacebookPage(Request $request)
		{
			$offset = isset($request->offset) ? $request->offset : 0;
			$limit = isset($request->limit) ? $request->limit : 9999999999;
			
			if($limit == 'All')
			{
				$limit = 9999999999;
			}
			
			$order = $request->order;
			
			if(!isset($request->sort))
			{
				$order = 'ASC';
			}
			
			$search = isset($request->search) ? $request->search : '';
			
			$data = array();
			$rows = array();
			
			$columns = ['facebook_pages.name', 'users.name'];
			
			$total = DB::table('facebook_pages')
			->leftJoin('users', 'facebook_pages.updated_user_id', '=', 'users.id')
			->select('facebook_pages.id')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('facebook_pages.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query){
				if(Session::get('user_category') != 'admin')
				{
					$query->where('facebook_pages.updated_user_id', Session::get('user_id'));
				}
			})
			->where('facebook_pages.is_deleted', 0)
			->count('facebook_pages.id');
			
			$results = DB::table('facebook_pages')
			->leftJoin('users', 'facebook_pages.updated_user_id', '=', 'users.id')
			->select('facebook_pages.*', 'users.name as user_name')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('facebook_pages.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query){
				if(Session::get('user_category') != 'admin')
				{
					$query->where('facebook_pages.updated_user_id', Session::get('user_id'));
				}
			})
			->where('facebook_pages.is_deleted', 0)
			->orderBy('facebook_pages.is_blocked', $order)
			->orderBy('facebook_pages.name', $order)
			->skip($offset)
			->take($limit)
			->get();
			
			$sno = $offset+1;
			foreach($results as $result)
			{
				$record = array();
				$record['#'] = $sno++;
				
				if($result->updated_user_id == Session::get('user_id'))
				{
					$record['Page Name'] = $result->name;
				}
				else
				{
					$record['Page Name'] = $result->name.' ('.$result->user_name.')';
				}
				
				if($result->is_blocked == 0)
				{
					$record['Action'] = '<div class="switch02" style="display: inline-flex;">
					<input id="cmn-toggle-0'.$result->id.'" type="checkbox" value="'.$result->id.'" class="cmn-toggle cmn-toggle-round" checked><label for="cmn-toggle-0'.$result->id.'"></label>
					</div>';
				}
				else
				{
					$record['Action'] = '<div class="switch02" style="display: inline-flex;">
					<input id="cmn-toggle-0'.$result->id.'" type="checkbox" value="'.$result->id.'" class="cmn-toggle cmn-toggle-round"><label for="cmn-toggle-0'.$result->id.'"></label>
					</div>';
				}
				
				$rows[] = $record;
			}
			
			$data['total'] = $total;
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
		
		//Facebook Page Hide / Show
        public function hideFacebookPage(Request $request)
        {          
			$is_blocked = $request->is_blocked;
			$facebook_page_id = $request->id;
			
			if($is_blocked == 0)
			{
				DB::table('facebook_pages')->where('id', $facebook_page_id)
				->update(['blocked_user_id' => Session::get('user_id'), 'blocked_user_ip' => $request->ip(), 'blocked_at' => date('Y-m-d H:i:s'), 'is_blocked' => $is_blocked]);
			}
			else
			{
				DB::table('facebook_pages')->where('id', $facebook_page_id)
				->update(['blocked_user_id' => Session::get('user_id'), 'blocked_user_ip' => $request->ip(), 'blocked_at' => date('Y-m-d H:i:s'), 'is_blocked' => $is_blocked]);
				
				DB::table('bot_activation')->where('fb_page_id', $facebook_page_id)->where('status', 1)
				->update(['updated_user_id' => Session::get('user_id'), 'updated_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 0]);
				
				DB::table('group_page_engagements')->where('facebook_page_id', $facebook_page_id)->where('is_deleted', 0)
				->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
				
				DB::table('page_engagements')->where('facebook_page_id', $facebook_page_id)->where('is_deleted', 0)
				->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
				
				$clients = DB::table('clients')->where('facebook_page_id', 'like', '%'.$facebook_page_id.'%')->get();
				foreach($clients as $client)
				{
					$arr = explode(',', $client->facebook_page_id);
					
					$arr = array_diff($arr, array($facebook_page_id));
					
					$string = implode(',', $arr);
					
					DB::table('clients')->where('id', $client->id)
					->update(['facebook_page_id' => $string, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
				}
				
				$groups = DB::table('groups')->where('facebook_page_id', 'like', '%'.$facebook_page_id.'%')->get();
				foreach($groups as $group)
				{
					$arr = explode(',', $group->facebook_page_id);
					
					$arr = array_diff($arr, array($facebook_page_id));
					
					$string = implode(',', $arr);
					
					DB::table('groups')->where('id', $group->id)
					->update(['facebook_page_id' => $string, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
				}
				
				$campaigns = DB::table('campaigns')->where('facebook_page_id', 'like', '%'.$facebook_page_id.'%')->get();
				foreach($campaigns as $campaign)
				{
					$arr = explode(',', $campaign->facebook_page_id);
					
					$arr = array_diff($arr, array($facebook_page_id));
					
					$string = implode(',', $arr);
					
					DB::table('campaigns')->where('id', $campaign->id)
					->update(['facebook_page_id' => $string, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
				}
			}
		}
		
		//Ad Form List
		public function getLeadForm(Request $request)
		{
			$offset = isset($request->offset) ? $request->offset : 0;
			$limit = isset($request->limit) ? $request->limit : 9999999999;
			
			if($limit == 'All')
			{
				$limit = 9999999999;
			}
			$order = $request->order;
			if(!isset($request->sort))
			{
				$order = 'ASC';
			}
			
			$search = isset($request->search) ? $request->search : '';
			
			$data = array();
			$rows = array();
			
			$columns = ['facebook_ads_leads.name', 'users.name'];
			
			$total = DB::table('facebook_ads_leads')
			->leftJoin('users', 'facebook_ads_leads.updated_user_id', '=', 'users.id')
			->select('facebook_ads_leads.id')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('facebook_ads_leads.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query){
				if(Session::get('user_category') != 'admin')
				{
					$query->where('facebook_ads_leads.updated_user_id', Session::get('user_id'));
				}
			})
			->where('facebook_ads_leads.status', 'ACTIVE')
			->where('facebook_ads_leads.is_deleted', 0)
			->count('facebook_ads_leads.id');
			
			$results = DB::table('facebook_ads_leads')
			->leftJoin('users', 'facebook_ads_leads.updated_user_id', '=', 'users.id')
			->select('facebook_ads_leads.*', 'users.name as user_name')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('facebook_ads_leads.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query){
				if(Session::get('user_category') != 'admin')
				{
					$query->where('facebook_ads_leads.updated_user_id', Session::get('user_id'));
				}
			})
			->where('facebook_ads_leads.status', 'ACTIVE')
			->where('facebook_ads_leads.is_deleted', 0)
			->orderBy('facebook_ads_leads.is_blocked', $order)
			->orderBy('facebook_ads_leads.name', $order)
			->skip($offset)
			->take($limit)
			->get();
			
			$sno = $offset+1;
			foreach($results as $result)
			{
				$record = array();
				$record['#'] = $sno++;
				
				if($result->updated_user_id == Session::get('user_id'))
				{
					$record['Form Name'] = $result->name;
				}
				else
				{
					$record['Form Name'] = $result->name.' ('.$result->user_name.')';
				}
				
				if($result->is_blocked == 0)
				{
					$record['Action'] = '<div class="switch01" style="display: inline-flex;">
					<input id="cmn-toggle-0'.$result->id.'" type="checkbox" value="'.$result->id.'" class="cmn-toggle cmn-toggle-round" checked><label for="cmn-toggle-0'.$result->id.'"></label>
					</div>';
				}
				else
				{
					$record['Action'] = '<div class="switch01" style="display: inline-flex;">
					<input id="cmn-toggle-0'.$result->id.'" type="checkbox" value="'.$result->id.'" class="cmn-toggle cmn-toggle-round"><label for="cmn-toggle-0'.$result->id.'"></label>
					</div>';
				}
				
				$rows[] = $record;
			}
			
			$data['total'] = $total;
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
		
		//Ad Form Block / Unblocked
        public function blockLeadform(Request $request)
        {          
			$is_blocked = $request->is_blocked;
			$facebook_ads_lead_id = $request->id;
			
			if($is_blocked == 0)
			{
				DB::table('facebook_ads_leads')->where('id', $facebook_ads_lead_id)
				->update(['blocked_user_id' => Session::get('user_id'), 'blocked_user_ip' => $request->ip(), 'blocked_at' => date('Y-m-d H:i:s'), 'is_blocked' => $is_blocked]);
			}
			else
			{
				DB::table('facebook_ads_leads')->where('id', $facebook_ads_lead_id)
				->update(['blocked_user_id' => Session::get('user_id'), 'blocked_user_ip' => $request->ip(), 'blocked_at' => date('Y-m-d H:i:s'), 'is_blocked' => $is_blocked]);
				
				DB::table('group_lead_forms')->where('facebook_ads_lead_id', $facebook_ads_lead_id)->where('is_deleted', 0)
				->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
				
				DB::table('client_lead_forms')->where('facebook_ads_lead_id', $facebook_ads_lead_id)->where('is_deleted', 0)
				->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
				
				$clients = DB::table('clients')->where('facebook_ads_lead_id', 'like', '%'.$facebook_ads_lead_id.'%')->get();
				foreach($clients as $client)
				{
					$arr = explode(',', $client->facebook_ads_lead_id);
					
					$arr = array_diff($arr, array($facebook_ads_lead_id));
					
					$string = implode(',', $arr);
					
					DB::table('clients')->where('id', $client->id)
					->update(['facebook_ads_lead_id' => $string, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
				}
				
				$groups = DB::table('groups')->where('facebook_ads_lead_id', 'like', '%'.$facebook_ads_lead_id.'%')->get();
				foreach($groups as $group)
				{
					$arr = explode(',', $group->facebook_ads_lead_id);
					
					$arr = array_diff($arr, array($facebook_ads_lead_id));
					
					$string = implode(',', $arr);
					
					DB::table('groups')->where('id', $group->id)
					->update(['facebook_ads_lead_id' => $string, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
				}
				
				$campaigns = DB::table('campaigns')->where('facebook_ads_lead_id', 'like', '%'.$facebook_ads_lead_id.'%')->get();
				foreach($campaigns as $campaign)
				{
					$arr = explode(',', $campaign->facebook_ads_lead_id);
					
					$arr = array_diff($arr, array($facebook_ads_lead_id));
					
					$string = implode(',', $arr);
					
					DB::table('campaigns')->where('id', $campaign->id)
					->update(['facebook_ads_lead_id' => $string, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
				}
			}
		}
		
		//User Email Notification List
		public function getUserEmailNotification(Request $request)
		{
			$client_id = isset($request->client_id) ? $request->client_id : 0;
			$order = isset($request->order) ? $request->order : 'ASC';
			$offset = isset($request->offset) ? $request->offset : 0;
			$limit = isset($request->limit) ? $request->limit : 9999999999;
			
			if($limit == 'All')
			{
				$limit = 9999999999;
			}
			
			$search = isset($request->search) ? $request->search : '';
			
			$data = array();
			$rows = array();
			
			$total = DB::table('managers')->select('id')
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
				else
				{
					$query->where('appointment_notifier', 'On')
					->orWhere('lead_notifier', 'On');
				}
			})
			->where(function ($query) use($search){
				if($search != '')
				{
					$query->where('full_name', 'like', '%'.$search.'%');
				}
			})
			->where('status', 1)
			->where('is_deleted', 0)
			->count('id');
			
			$results = DB::table('managers')
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
				else
				{
					$query->where('appointment_notifier', 'On')
					->orWhere('lead_notifier', 'On');
				}
			})
			->where(function ($query) use($search){
				if($search != '')
				{
					$query->where('full_name', 'like', '%'.$search.'%');
				}
			})
			->where('status', 1)
			->where('is_deleted', 0)
			->orderBy('full_name', $order)
			->skip($offset)
			->take($limit)
			->get();
			
			$sno = $offset+1;
			foreach($results as $result)
			{
				$record = array();
				$record['#'] = $sno++;
				
				$record['User Name'] = title_case($result->full_name);
				
				if($result->appointment_notifier == 'On')
				{
					$record['Appointment Notifier'] = '<div class="switch1" onClick="AppointmentNotifier('.$result->id.', \'Off\');" style="display: inline-flex;"><input  id="cmn-toggle-1'.$result->id.'" type="checkbox" value="'.$result->id.'" class="cmn-toggle appointment_notifier cmn-toggle-round abcdef" checked><label for="cmn-toggle-1'.$result->id.'"></label></div>';
				}
				else
				{
					$record['Appointment Notifier'] = '<div class="switch1" onClick="AppointmentNotifier('.$result->id.', \'On\');" style="display: inline-flex;"><input  id="cmn-toggle-1'.$result->id.'" type="checkbox" value="'.$result->id.'" class="cmn-toggle appointment_notifier cmn-toggle-round abcdef"><label for="cmn-toggle-1'.$result->id.'"></label></div>';
				}
				
				if($result->lead_notifier == 'On')
				{
					$record['Lead Notifier'] = '<div class="switch2" onClick="LeadNotifier('.$result->id.', \'Off\');" style="display: inline-flex;"><input  id="cmn-toggle-2'.$result->id.'" type="checkbox" value="'.$result->id.'" class="cmn-toggle lead_notifier cmn-toggle-round ghijkl" checked><label for="cmn-toggle-2'.$result->id.'"></label></div>';
				}
				else
				{
					$record['Lead Notifier'] = '<div class="switch2" onClick="LeadNotifier('.$result->id.', \'On\');" style="display: inline-flex;"><input  id="cmn-toggle-2'.$result->id.'" type="checkbox" value="'.$result->id.'" class="cmn-toggle lead_notifier cmn-toggle-round ghijkl"><label for="cmn-toggle-2'.$result->id.'"></label></div>';
				}
				
				$rows[] = $record;
			}
			
			$data['total'] = $total;
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
		
		//Check Email Notification
		public function checkEmailNotification(Request $request)
		{
			$client_id = isset($request->client_id) ? $request->client_id : 0;
			$search = isset($request->search) ? $request->search : '';
			
			$appointment_notifier = 'On';
			$lead_notifier = 'On';
			
			$results = DB::table('managers')
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('client_id', $client_id);
				}
				else
				{
					$query->where('appointment_notifier', 'On')
					->orWhere('lead_notifier', 'On');
				}
			})
			->where(function ($query) use($search){
				if($search != '')
				{
					$query->where('full_name', 'like', '%'.$search.'%');
				}
			})
			->where('status', 1)
			->where('is_deleted', 0)
			->get();
			
			foreach($results as $result)
			{
				if($result->appointment_notifier == 'Off')
				{
					$appointment_notifier = 'Off';
				}
				
				if($result->lead_notifier == 'Off')
				{
					$lead_notifier = 'Off';
				}
			}
			
			$data = array();
			$data['lead_notifier'] = $lead_notifier;
			$data['appointment_notifier'] = $appointment_notifier;
			
			return \Response::json($data);
		}
	}																															