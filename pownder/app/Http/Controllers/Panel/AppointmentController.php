<?php
	namespace App\Http\Controllers\Panel;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	
	use DB;
	use Session;
	use ManagerHelper;
	use Mail;
	use Helper;
	
	class AppointmentController extends Controller
	{
		//Appointments page
        public function getAppointments()
		{
			if(Session::get('user_category') == 'admin' || ManagerHelper::ManagerCategory()=='admin' || Session::get('user_category') == 'vendor' || ManagerHelper::ManagerCategory()=='vendor')
			{
				$ShowLead = '';
				$UserArray = array();
				$ClientArray = array();
				if(Session::get('user_category')=='user')
				{
					$ShowLead = ManagerHelper::ManagerLeadMenuAction();
					$UserArray[] = Session::get('user_id');
					$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
					$UserArray[] = $manager->created_user_id;
					$users = DB::table('users')->select('id')->where('created_user_id',$manager->created_user_id)->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
					}
					
					$Vendors = DB::table('users')->select('id')->where('id',$manager->created_user_id)->get();
					foreach($Vendors as $Vendor)
					{
						$Clients = DB::table('clients')->select('id')->where('vendor_id', $Vendor->id)->get();
						foreach($Clients as $Client)
						{
							$ClientArray[] = $Client->id;
							$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
							foreach($users as $user)
							{
								$UserArray[] = $user->id;
								$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
								foreach($Managers as $Manager)
								{
									$UserArray[] = $Manager->id;
								}
							}
						}
					}
				}
				else
				{
					$UserArray[] = Session::get('user_id');
					$users = DB::table('users')->select('id')->where('created_user_id',Session::get('user_id'))->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
					}
					
					$Clients = DB::table('clients')->select('id')->where('vendor_id',Session::get('vendor_id'))->get();
					foreach($Clients as $Client)
					{
						$ClientArray[] = $Client->id;
						$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
						foreach($users as $user)
						{
							$UserArray[] = $user->id;
							$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
							foreach($Managers as $Manager)
							{
								$UserArray[] = $Manager->id;
							}
						}
					}
				}
				
				$Appointments = DB::table('facebook_ads_lead_users')
				->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', Session::get('manager_id'));
					}
					elseif(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
					{
						$query->where('id', '>', 0);
					}
					else
					{
						$query->whereIn('created_user_id', $UserArray)->whereIn('client_id', $ClientArray, 'or');
					}
				})
				->where(function ($query) {
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})
				->where('client_id', '>', 0)
				->where('setting', 'Appointment')
				->where('is_deleted', 0)
				->get();
				
				$TotalAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where(function ($query) {
					if(Session::get('client_id')>0)
					{
						$query->where('facebook_ads_lead_users.client_id', Session::get('client_id'));
					}
				})
				->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
					}
					elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
					{
						$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
					}
				})
				->count('facebook_ads_lead_user_settings.id');
				
				$ConfirmedAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Confirmed')->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where(function ($query) {
					if(Session::get('client_id')>0)
					{
						$query->where('facebook_ads_lead_users.client_id', Session::get('client_id'));
					}
				})
				->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
					}
					elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
					{
						$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
					}
				})
				->count('facebook_ads_lead_user_settings.id');
				
				$ShownAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Shown')->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where(function ($query) {
					if(Session::get('client_id')>0)
					{
						$query->where('facebook_ads_lead_users.client_id', Session::get('client_id'));
					}
				})
				->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
					}
					elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
					{
						$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
					}
				})
				->count('facebook_ads_lead_user_settings.id');
				
				$CanceledAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Canceled')->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where(function ($query) {
					if(Session::get('client_id')>0)
					{
						$query->where('facebook_ads_lead_users.client_id', Session::get('client_id'));
					}
				})
				->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
					}
					elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
					{
						$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
					}
				})
				->count('facebook_ads_lead_user_settings.id');
			}
			
			if(Session::get('user_category') == 'client' || ManagerHelper::ManagerCategory()=='client')
			{
				$ShowLead = '';
				if(Session::get('user_category') == 'user')
				{
					$ShowLead = ManagerHelper::ManagerLeadMenuAction();
					$manager = DB::table('managers')->select('client_id')->where('id', Session::get('manager_id'))->first();
					$client_id = $manager->client_id;
				}
				else
				{
					$client_id = Session::get('client_id');
				}
				
				$Appointments = DB::table('facebook_ads_lead_users')
				->where(function ($query) use($client_id, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', Session::get('manager_id'));
					}
					else
					{
						$query->where('client_id', $client_id);
					}
				})
				->where('client_id', '>', 0)
				->where('setting', 'Appointment')
				->where('is_deleted', 0)
				->get();
				
				$TotalAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where(function ($query) use($client_id, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
					}
					else
					{
						$query->where('facebook_ads_lead_users.client_id', $client_id);
					}
				})
				->count('facebook_ads_lead_user_settings.id');
				
				$ConfirmedAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Confirmed')->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where(function ($query) use($client_id, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
					}
					else
					{
						$query->where('facebook_ads_lead_users.client_id', $client_id);
					}
				})
				->count('facebook_ads_lead_user_settings.id');
				
				$ShownAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Shown')->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where(function ($query) use($client_id, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
					}
					else
					{
						$query->where('facebook_ads_lead_users.client_id', $client_id);
					}
				})
				->count('facebook_ads_lead_user_settings.id');
				
				$CanceledAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Canceled')->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where(function ($query) use($client_id, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
					}
					else
					{
						$query->where('facebook_ads_lead_users.client_id', $client_id);
					}
				})
				->count('facebook_ads_lead_user_settings.id');
			}
			
			return view('panel.frontend.appointments',['Appointments' => $Appointments, 'TotalAppointment' => $TotalAppointment, 'ConfirmedAppointment' => $ConfirmedAppointment, 'ShownAppointment' => $ShownAppointment, 'CanceledAppointment' => $CanceledAppointment]);
		}
		
		//Event Add
        public function postEvents(Request $request)
		{
			$results = array();
			if($request->facebook_ads_lead_user_id > 0)
			{
				$event = DB::table('events')->where('facebook_ads_lead_user_id', $request->facebook_ads_lead_user_id)->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->first();
				if(is_null($event))
				{
					$event_id = DB::table('events')->insertGetId(['admin_id' => Session::get('admin_id'), 'client_id' => Session::get('client_id'), 'manager_id' => Session::get('manager_id'), 'vendor_id' => Session::get('vendor_id'), 'category' => Session::get('user_category'), 'event_name' => $request->event, 'event_type' => $request->event_type, 'event_url' => $request->url, 'facebook_ads_lead_user_id' => $request->facebook_ads_lead_user_id, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s'), 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')
					]);
					
					$results['response'] = $event_id;
					return response()->json($results);
				}
				else
				{
					DB::table('events')->where('facebook_ads_lead_user_id', $request->facebook_ads_lead_user_id)->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)
					->update(['admin_id' => Session::get('admin_id'), 'client_id' => Session::get('client_id'), 'manager_id' => Session::get('manager_id'), 'vendor_id' => Session::get('vendor_id'), 'category' => Session::get('user_category'), 'event_name' => $request->event, 'event_type' => $request->event_type, 'event_url' => $request->url, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')
					]);
					
					$results['response'] = '';
					return response()->json($results);
				}
			}
			
			$results['response'] = '';
			return response()->json($results);
		}
		
		//Event Add
        public function searchEventLeads(Request $request)
		{
			$search = $request->term;
			
			if(Session::get('user_category') == 'admin' || ManagerHelper::ManagerCategory()=='admin' || Session::get('user_category') == 'vendor' || ManagerHelper::ManagerCategory()=='vendor')
			{
				$ShowLead = '';
				$UserArray = array();
				$ClientArray[] = array();
				if(Session::get('user_category')=='user')
				{
					$ShowLead = ManagerHelper::ManagerLeadMenuAction();
					$UserArray[] = Session::get('user_id');
					$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
					$UserArray[] = $manager->created_user_id;
					$users = DB::table('users')->select('id')->where('created_user_id',$manager->created_user_id)->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
					}
					
					$Vendors = DB::table('users')->select('id')->where('id',$manager->created_user_id)->get();
					foreach($Vendors as $Vendor)
					{
						$Clients = DB::table('clients')->select('id')->where('vendor_id', $Vendor->id)->get();
						foreach($Clients as $Client)
						{
							$ClientArray[] = $Client->id;
							$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
							foreach($users as $user)
							{
								$UserArray[] = $user->id;
								$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
								foreach($Managers as $Manager)
								{
									$UserArray[] = $Manager->id;
								}
							}
						}
					}
				}
				else
				{
					$UserArray[] = Session::get('user_id');
					$users = DB::table('users')->select('id')->where('created_user_id',Session::get('user_id'))->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
					}
					
					$Clients = DB::table('clients')->select('id')->where('vendor_id',Session::get('vendor_id'))->get();
					foreach($Clients as $Client)
					{
						$ClientArray[] = $Client->id;
						$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
						foreach($users as $user)
						{
							$UserArray[] = $user->id;
							$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
							foreach($Managers as $Manager)
							{
								$UserArray[] = $Manager->id;
							}
						}
					}
				}
				
				$Leads = DB::table('facebook_ads_lead_users')
				->leftJoin('clients', 'facebook_ads_lead_users.client_id', '=', 'clients.id')
				->select('facebook_ads_lead_users.id', 'facebook_ads_lead_users.full_name', 'facebook_ads_lead_users.email', 'facebook_ads_lead_users.phone_number', 'clients.name as client_name')
				->where(function ($query) use($search){
					if($search!='')
					{
						$query->where('facebook_ads_lead_users.full_name', 'like', '%'.$search.'%');
					}
				})
				->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
					}
					elseif(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
					{
						$query->where('facebook_ads_lead_users.created_user_id', '>', 0);
					}
					else
					{
						$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
					}
				})
				->where('facebook_ads_lead_users.client_id', '>', 0)
				->orderBy('facebook_ads_lead_users.full_name', 'ASC')
				->take(25)
				->get();	
			}
			
			if(Session::get('user_category') == 'client' || ManagerHelper::ManagerCategory()=='client')
			{
				$ShowLead = '';
				if(Session::get('user_category') == 'user')
				{
					$ShowLead = ManagerHelper::ManagerLeadMenuAction();
					$manager = DB::table('managers')->select('client_id')->where('id', Session::get('manager_id'))->first();
					$client_id = $manager->client_id;
				}
				else
				{
					$client_id = Session::get('client_id');
				}
				
				$Leads = DB::table('facebook_ads_lead_users')
				->leftJoin('clients', 'facebook_ads_lead_users.client_id', '=', 'clients.id')
				->select('facebook_ads_lead_users.id', 'facebook_ads_lead_users.full_name', 'facebook_ads_lead_users.email', 'facebook_ads_lead_users.phone_number', 'clients.name as client_name')
				->where(function ($query) use($search){
					if($search!='')
					{
						$query->where('facebook_ads_lead_users.full_name', 'like', '%'.$search.'%');
					}
				})
				->where(function ($query) use($client_id, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
					}
					else
					{
						$query->where('facebook_ads_lead_users.client_id', $client_id);
					}
				})
				->where('facebook_ads_lead_users.client_id', '>', 0)
				->orderBy('facebook_ads_lead_users.full_name', 'ASC')
				->take(25)
				->get();
			}
			
			$results=array();
			
			foreach ($Leads as $key => $v) {
				$results[] = ['id'=> "'".$v->id."'", 'value'=>$v->full_name." (".$v->client_name.")"];
			}
			
			return response()->json($results);
		}
		
		public function postAppointment(Request $request)
		{
			$app_date = explode('-',$request->appointment_date);
			$appointment_date = $app_date[2].'-'.$app_date[0].'-'.$app_date[1];
			$facebook_ads_lead_user_id = $request->facebook_ads_lead_user_id;
			
			$oldData = DB::table('facebook_ads_lead_user_settings')->where('facebook_ads_lead_user_id', $request->facebook_ads_lead_user_id)->where('is_deleted', 0)->orderBy('id', 'DESC')->first();
			
			if(is_null($oldData))
			{
				DB::table('facebook_ads_lead_user_settings')->insert([
				'facebook_ads_lead_user_id' => $request->facebook_ads_lead_user_id, 
				'setting' => 'Appointment', 
				'appointment_date' => $appointment_date,
				'appointment_time' => $request->appointment_time,
				'appointment_cal' => 'Yes',
				'comment' => '',
				'borderColor' => $request->borderColor,
				'backgroundColor' => $request->backgroundColor,
				'created_user_id' => Session::get('user_id'), 
				'created_user_ip' => $request->ip(),
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			else
			{
				if($oldData->setting == 'Appointment')
				{
					DB::table('facebook_ads_lead_user_settings')->where('id', $oldData->id)
					->update([
					'appointment_date' => $appointment_date,
					'appointment_time' => $request->appointment_time,
					'comment' => '',
					'borderColor' => $request->borderColor,
					'backgroundColor' => $request->backgroundColor,
					'updated_user_id' => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					'updated_at' => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('facebook_ads_lead_user_settings')->insert([
					'facebook_ads_lead_user_id' => $request->facebook_ads_lead_user_id, 
					'setting' => 'Appointment', 
					'appointment_date' => $appointment_date,
					'appointment_time' => $request->appointment_time,
					'comment' => '',
					'appointment_cal' => 'Yes',
					'borderColor' => $request->borderColor,
					'backgroundColor' => $request->backgroundColor,
					'created_user_id' => Session::get('user_id'), 
					'created_user_ip' => $request->ip(),
					'created_at' => date('Y-m-d H:i:s'), 
					'updated_user_id' => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					'updated_at' => date('Y-m-d H:i:s')
					]);
				}
			}
			
			DB::table('facebook_ads_lead_users')->where('id', $request->facebook_ads_lead_user_id)
			->update(['setting' => 'Appointment', 'appointment_status' => '', 'appointment_date' => $appointment_date, 'appointment_time' => $request->appointment_time, 'appointment_cal' => 'Yes', 'borderColor' => $request->borderColor, 'backgroundColor' => $request->backgroundColor, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
			
			$leadUser = DB::table('facebook_ads_lead_users')->where('id', $request->facebook_ads_lead_user_id)->first();
			$facebook_page_id = $leadUser->facebook_page_id;
			$lead_user_id = $leadUser->created_user_id;
			$client_id = $leadUser->client_id;
			$manager_id = $leadUser->manager_id;
			
			if($leadUser->full_name != '')
			{
				$leadName = $leadUser->full_name;
			}
			else
			{
				$leadName = $leadUser->first_name.' '.$leadUser->last_name;
			}
			
			$leadType = $leadUser->lead_type;
			$leadEmail = $leadUser->email;
			$leadPhone = $leadUser->phone_number;
			$leadCity = $leadUser->city;
			
			$ManagerArray = array();
			
			$leadAddedBy = '';
			$User = DB::table('users')->where('id', $lead_user_id)->first();
			if(($leadType == 'Pownder™ Call' || $leadType == 'Pownder™ Messenger') && $facebook_page_id == 0)
			{
				$leadAddedBy = $User->name.' ('.title_case($User->category).')';
			}
			
			$manager_name = '';
			$LeadManager = DB::table('managers')->where('id', $manager_id)->first();
			if(!is_null($LeadManager))
			{
				$manager_name = title_case($LeadManager->full_name);
			}
			
			$Client = DB::table('users')->where('client_id', $client_id)->first();
			
			$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
			foreach($CampaignClients as $CampaignClient)
			{
				if($CampaignClient->email_lead_notification != ''  && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only Appointments'))
				{
					$data = array('name' => $CampaignClient->client_contact, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'Client', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
					
					Mail::send('emails.appointment', $data, function ($message) use ($CampaignClient) {
						$message->from('noreply@pownder.com', 'Pownder');
						$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('Facebook Lead Appointment');
					});
				}
				
				if($CampaignClient->user_notification == 'No')
				{
					if($CampaignClient->manager_id != '')
					{
						$ManagerArray = explode(",", $CampaignClient->manager_id);
						$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
						foreach($Managers as $Manager)
						{
							$data = array('name' => $Manager->full_name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'User', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
							
							Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
								$message->from('noreply@pownder.com', 'Pownder');
								$message->to($Manager->email)->subject('Facebook Lead Appointment');
							});
						}
					}
				}
				else
				{
					$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
					if(!is_null($ManagerExists))
					{
						if($CampaignClient->notification == 'For Appointments' || $CampaignClient->notification == 'Both')
						{
							if($CampaignClient->manager_id != '')
							{
								$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
								
								$ManagerArray = explode(",", $CampaignClient->manager_id);
								
								$Array = array();
								$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
								foreach($queries as $query)
								{
									$Array[] = $query->id;
								}
								
								if(count($Array) > 0)
								{
									$index = $LeadCount % count($Array);
									
									$Manager = DB::table('managers')->where('id', $Array[$index])->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
									if(!is_null($Manager))
									{
										$data = array('name' => $Manager->full_name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'User', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
										
										Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
											$message->from('noreply@pownder.com', 'Pownder');
											$message->to($Manager->email)->subject('Facebook Lead Appointment');
										});
										
										DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
										->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
									}
								}
							}
						}
					}
				}
			}
			
			$Managers = DB::table('managers')->where('appointment_notifier', 'On')->where('client_id', $client_id)->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
			foreach($Managers as $Manager)
			{
				if (!in_array($Manager->id, $ManagerArray))
				{
					$manager_permission = DB::table('manager_permissions')->where('permission', 'All Leads')->where('manager_id', $Manager->id)->where('is_deleted', 0)->get();
					if(!is_null($manager_permission))
					{
						$ManagerArray[] = $Manager->id;
						
						$data = array('name' => $Manager->full_name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'User', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
						
						Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
							$message->from('noreply@pownder.com', 'Pownder');
							$message->to($Manager->email)->subject('Facebook Lead Appointment');
						});
					}
				}
			}
			
			if (!in_array($manager_id, $ManagerArray) && $manager_id > 0)
			{
				$Manager = DB::table('users')->where('manager_id', $manager_id)->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
				if(!is_null($Manager))
				{
					$data = array('name' => $Manager->name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => $Manager->category, 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
					
					Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
						$message->from('noreply@pownder.com', 'Pownder');
						$message->to($Manager->email)->subject('Facebook Lead Appointment');
					});
				}
			}
			
			$admin = DB::table('admins')->where('id', 1)->where('appointment_email', '!=', '')->where('appointment_option', 'Yes')->where('is_deleted', 0)->first();
			if(!is_null($admin))
			{
				if($admin->receive_notification == 'Everytime')
				{
					$data = array('name' => $admin->name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'admin', 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
					
					Mail::send('emails.appointment', $data, function ($message) use ($admin) {
						$message->from('noreply@pownder.com', 'Pownder');
						$message->to($admin->appointment_email)->subject('Facebook Lead Appointment');
					});
				}
				else
				{
					DB::table('admin_emails')->insert(['facebook_ads_lead_user_id' => $request->facebook_ads_lead_user_id, 'subject' => 'Facebook Lead Appointment', 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s')]);
				}
			}
			
			$data = array();
			$result = DB::table('facebook_ads_lead_users')->where('id', $request->facebook_ads_lead_user_id)->first(); 
			if(is_null($result))
			{
				$data['title'] = '';
				$data['start'] = '';
				$data['backgroundColor'] = '';
				$data['borderColor'] = '';
				$data['id'] = '';
				$data['className'] = '';
			}
			else
			{
				$data['title'] = $result->full_name;
				$data['start'] = $result->appointment_date.'T'.date("H:i:s",strtotime($result->appointment_time));
				$data['backgroundColor'] = $result->backgroundColor;
				$data['borderColor'] = $result->borderColor;
				//	$data['id'] = "'".$result->id."'";
				$data['id'] = $result->id;
				$data['className'] = 'appointmentHide';
			}
			
			return response()->json($data);
		}
		
		public function removeEvent(Request $request)
		{
			DB::table('events')->where('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => '1']);
		}
		
		public function removeResyncError(Request $request)
        {   
			DB::table('resync_error')->where('id', $request->id)
            ->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			
			$resync_error = DB::table('resync_error')->where('id', $request->id)->first();
			
			DB::table('resync_error')->where('is_deleted', 0)->where('facebook_ads_lead_user_id', $resync_error->facebook_ads_lead_user_id)->where('reason', $resync_error->reason)
            ->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
		}
		
		public function getLeadDetails(Request $request)
        {   
			$results = DB::table('facebook_ads_lead_users')
			->leftJoin('clients', 'facebook_ads_lead_users.client_id', '=', 'clients.id')
			->select('facebook_ads_lead_users.*', 'clients.name as client_name')
			->where('facebook_ads_lead_users.id', $request->id)
			->first();
			
			return response()->json($results);
		}
		
		public function updateAppointment(Request $request)
		{
			$oldData = DB::table('facebook_ads_lead_users')->where('setting', 'Appointment')->where('id', $request->facebook_ads_lead_user_id)->first();
			
			if(!is_null($oldData))
			{
				$extand = $request->dayDelta;
				
				$OldDate = strtotime($oldData->appointment_date.' '.$oldData->appointment_time);
				
				$strtotime = $OldDate+($extand/1000);
				
				$appointment_date = date('Y-m-d', $strtotime);
				$appointment_time = date('h:i A', $strtotime);
				
				$lastData = DB::table('facebook_ads_lead_user_settings')->where('setting', 'Appointment')->where('facebook_ads_lead_user_id', $request->facebook_ads_lead_user_id)->where('is_deleted', 0)->orderBy('id', 'DESC')->first();
				
				DB::table('facebook_ads_lead_user_settings')->where('id', $lastData->id)->where('facebook_ads_lead_user_id', $request->facebook_ads_lead_user_id)
				->update(['appointment_date' => $appointment_date, 'appointment_time' => $appointment_time, 'updated_user_id' => Session::get('user_id'),
				'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
				
				DB::table('facebook_ads_lead_users')->where('id', $request->facebook_ads_lead_user_id)
				->update(['appointment_date' => $appointment_date, 'appointment_time' => $appointment_time, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]); 
			}
		}	
		
		public function deleteAppointment(Request $request)
		{
			$lastData = DB::table('facebook_ads_lead_user_settings')->where('setting', 'Appointment')->where('facebook_ads_lead_user_id', $request->facebook_ads_lead_user_id)->where('is_deleted', 0)->orderBy('id', 'DESC')->first();
			if(!is_null($lastData))
			{
				DB::table('facebook_ads_lead_user_settings')->where('id', $lastData->id)->where('facebook_ads_lead_user_id', $request->facebook_ads_lead_user_id)
				->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			}
			
			DB::table('facebook_ads_lead_users')->where('id', $request->facebook_ads_lead_user_id)
			->update(['setting' => '', 'comment' => '', 'appointment_date' => '', 'appointment_time' => '', 'appointment_status' => '',  'appointment_url' => '',  'appointment_cal' => 'No',  'borderColor' => '#4FC1E9',  'backgroundColor' => '#4FC1E9', 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]); 
		}
		
		public function cancelAppointment(Request $request)
		{
			$lastData = DB::table('facebook_ads_lead_user_settings')->where('setting', 'Appointment')->where('facebook_ads_lead_user_id', $request->facebook_ads_lead_user_id)->where('is_deleted', 0)->orderBy('id', 'DESC')->first();
			if(!is_null($lastData))
			{
				DB::table('facebook_ads_lead_user_settings')->where('id', $lastData->id)->where('facebook_ads_lead_user_id', $request->facebook_ads_lead_user_id)
				->update(['comment' => 'Canceled', 'borderColor' => '#FB8678', 'backgroundColor' => '#FB8678', 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			
			DB::table('facebook_ads_lead_users')->where('id', $request->facebook_ads_lead_user_id)
			->update(['appointment_status' => 'Canceled', 'borderColor' => '#FB8678', 'backgroundColor' => '#FB8678', 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]); 
			
			$data = array();
			$result = DB::table('facebook_ads_lead_users')->where('id', $request->facebook_ads_lead_user_id)->first(); 
			if(is_null($result))
			{
				$data['title'] = '';
				$data['start'] = '';
				$data['backgroundColor'] = '';
				$data['borderColor'] = '';
				$data['id'] = '';
				$data['className'] = '';
			}
			else
			{
				$data['title'] = $result->full_name;
				$data['start'] = $result->appointment_date.'T'.date("H:i:s",strtotime($result->appointment_time));
				$data['backgroundColor'] = $result->backgroundColor;
				$data['borderColor'] = $result->borderColor;
				//	$data['id'] = "'".$result->id."'";
				$data['id'] = $result->id;
				$data['className'] = 'appointmentHide';
			}
			
			return response()->json($data);
		}
		
		public function soldAppointment(Request $request)
		{
			DB::table('facebook_ads_lead_user_settings')
			->insert(['facebook_ads_lead_user_id' => $request->facebook_ads_lead_user_id, 'setting' => 'Sold', 'comment' => '', 'borderColor' => '#4FC1E9', 'backgroundColor' => '#4FC1E9', 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s'), 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			
			DB::table('facebook_ads_lead_users')->where('id', $request->facebook_ads_lead_user_id)
			->update(['setting' => 'Sold', 'comment' => '', 'appointment_date' => '', 'appointment_time' => '', 'appointment_status' => '', 'borderColor' => '#4FC1E9', 'backgroundColor' => '#4FC1E9', 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
		}
		
		public function shownAppointment(Request $request)
		{
			$lastData = DB::table('facebook_ads_lead_user_settings')->where('setting', 'Appointment')->where('facebook_ads_lead_user_id', $request->facebook_ads_lead_user_id)->where('is_deleted', 0)->orderBy('id', 'DESC')->first();
			if(!is_null($lastData))
			{
				DB::table('facebook_ads_lead_user_settings')->where('id', $lastData->id)->where('facebook_ads_lead_user_id', $request->facebook_ads_lead_user_id)
				->update(['comment' => 'Shown', 'borderColor' => '#4FC1E9', 'backgroundColor' => '#4FC1E9', 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			
			DB::table('facebook_ads_lead_users')->where('id', $request->facebook_ads_lead_user_id)
			->update(['appointment_status' => 'Shown', 'borderColor' => '#4FC1E9', 'backgroundColor' => '#4FC1E9', 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]); 
			
			$data = array();
			$result = DB::table('facebook_ads_lead_users')->where('id', $request->facebook_ads_lead_user_id)->first(); 
			if(is_null($result))
			{
				$data['title'] = '';
				$data['start'] = '';
				$data['backgroundColor'] = '';
				$data['borderColor'] = '';
				$data['id'] = '';
				$data['className'] = '';
			}
			else
			{
				$data['title'] = $result->full_name;
				$data['start'] = $result->appointment_date.'T'.date("H:i:s",strtotime($result->appointment_time));
				$data['backgroundColor'] = $result->backgroundColor;
				$data['borderColor'] = $result->borderColor;
				//	$data['id'] = "'".$result->id."'";
				$data['id'] = $result->id;
				$data['className'] = 'appointmentHide';
			}
			
			return response()->json($data);
		}
		
		public function rescheduleAppointment(Request $request)
		{
			$data = array();
			$results = DB::table('facebook_ads_lead_users')->where('id', $request->facebook_ads_lead_user_id)->first(); 
			if(is_null($results))
			{
				$data['appointment_date'] = '';
				$data['appointment_time'] = '';
			}
			else
			{
				$data['appointment_date'] = date('m-d-Y', strtotime($results->appointment_date));
				$data['appointment_time'] = $results->appointment_time;
			}
			
			return response()->json($data);
		}
		
		public function appointmentReschedule(Request $request)
		{
			$facebook_ads_lead_user_id = $request->facebook_ads_lead_user_id;
			$appointment_time = $request->appointment_time;
			
			$app_date = explode('-',$request->appointment_date);
			$appointment_date = $app_date[2].'-'.$app_date[0].'-'.$app_date[1];
			
			$oldData = DB::table('facebook_ads_lead_users')->where('setting', 'Appointment')->where('id', $facebook_ads_lead_user_id)->first();
			
			if(!is_null($oldData))
			{
				$lastData = DB::table('facebook_ads_lead_user_settings')->where('setting', 'Appointment')->where('facebook_ads_lead_user_id', $facebook_ads_lead_user_id)->where('is_deleted', 0)->orderBy('id', 'DESC')->first();
				
				DB::table('facebook_ads_lead_user_settings')->where('id', $lastData->id)->where('facebook_ads_lead_user_id', $facebook_ads_lead_user_id)
				->update(['appointment_date' => $appointment_date, 'appointment_time' => $appointment_time, 'comment' => 'Reschedule', 'borderColor' => '#4FC1E9', 'backgroundColor' => '#4FC1E9', 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			
			DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
			->update(['appointment_date' => $appointment_date, 'appointment_time' => $appointment_time, 'appointment_status' => 'Reschedule', 'borderColor' => '#4FC1E9', 'backgroundColor' => '#4FC1E9', 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]); 
			
			$leadUser = DB::table('facebook_ads_lead_users')->where('id', $request->facebook_ads_lead_user_id)->first();
			$facebook_page_id = $leadUser->facebook_page_id;
			$lead_user_id = $leadUser->created_user_id;
			$client_id = $leadUser->client_id;
			$manager_id = $leadUser->manager_id;
			
			if($leadUser->full_name != '')
			{
				$leadName = $leadUser->full_name;
			}
			else
			{
				$leadName = $leadUser->first_name.' '.$leadUser->last_name;
			}
			
			$leadType = $leadUser->lead_type;
			$leadEmail = $leadUser->email;
			$leadPhone = $leadUser->phone_number;
			$leadCity = $leadUser->city;
			
			$leadAddedBy = '';
			$User = DB::table('users')->where('id', $lead_user_id)->first();
			if(($leadType == 'Pownder™ Call' || $leadType == 'Pownder™ Messenger') && $facebook_page_id == 0)
			{
				$leadAddedBy = $User->name.' ('.title_case($User->category).')';
			}
			
			$manager_name = '';
			$LeadManager = DB::table('managers')->where('id', $manager_id)->first();
			if(!is_null($LeadManager))
			{
				$manager_name = title_case($LeadManager->full_name);
			}
			
			$Client = DB::table('users')->where('client_id', $client_id)->first();
			
			$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
			foreach($CampaignClients as $CampaignClient)
			{
				if($CampaignClient->email_lead_notification != ''  && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only Appointments'))
				{
					$data = array('name' => $CampaignClient->client_contact, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'Client', 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
					
					Mail::send('emails.appointment', $data, function ($message) use ($CampaignClient) {
						$message->from('noreply@pownder.com', 'Pownder');
						$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('Facebook Reschedule Appointment');
					});
				}
				
				if($CampaignClient->user_notification == 'Yes' && $manager_id == 0)
				{
					$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
					if(!is_null($ManagerExists))
					{
						if($CampaignClient->notification == 'For Appointments' || $CampaignClient->notification == 'Both')
						{
							if($CampaignClient->manager_id != '')
							{
								$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
								
								$ManagerArray = explode(",", $CampaignClient->manager_id);
								
								$Array = array();
								$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
								foreach($queries as $query)
								{
									$Array[] = $query->id;
								}
								
								if(count($Array) > 0)
								{
									$index = $LeadCount % count($Array);
									
									$Manager = DB::table('managers')->where('id', $Array[$index])->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
									if(!is_null($Manager))
									{
										$data = array('name' => $Manager->full_name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'User', 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
										
										Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
											$message->from('noreply@pownder.com', 'Pownder');
											$message->to($Manager->email)->subject('Facebook Reschedule Appointment');
										});
										
										DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
										->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
									}
								}
							}
						}
					}
				}
			}
			
			if($manager_id > 0)
			{
				$Manager = DB::table('users')->where('manager_id', $manager_id)->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
				if(!is_null($Manager))
				{
					$data = array('name' => $Manager->name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => $Manager->category, 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
					
					Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
						$message->from('noreply@pownder.com', 'Pownder');
						$message->to($Manager->email)->subject('Facebook Reschedule Appointment');
					});
				}
			}
			
			$admin = DB::table('admins')->where('id', 1)->where('appointment_email', '!=', '')->where('appointment_option', 'Yes')->where('is_deleted', 0)->first();
			if(!is_null($admin))
			{
				if($admin->receive_notification == 'Everytime')
				{
					$data = array('name' => $admin->name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'admin', 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
					
					Mail::send('emails.appointment', $data, function ($message) use ($admin) {
						$message->from('noreply@pownder.com', 'Pownder');
						$message->to($admin->appointment_email)->subject('Facebook Reschedule Appointment');
					});
				}
				else
				{
					DB::table('admin_emails')->insert(['facebook_ads_lead_user_id' => $request->facebook_ads_lead_user_id, 'subject' => 'Facebook Reschedule Appointment', 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s')]);
				}
			}
			
			$data = array();
			$result = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->first(); 
			if(is_null($result))
			{
				$data['title'] = '';
				$data['start'] = '';
				$data['backgroundColor'] = '';
				$data['borderColor'] = '';
			$data['id'] = '';
			$data['className'] = '';
			}
			else
			{
				$data['title'] = $result->full_name;
				$data['start'] = $result->appointment_date.'T'.date("H:i:s",strtotime($result->appointment_time));
				$data['backgroundColor'] = $result->backgroundColor;
				$data['borderColor'] = $result->borderColor;
				//	$data['id'] = "'".$result->id."'";
				$data['id'] = $result->id;
				$data['className'] = 'appointmentHide';
			}
			
			return response()->json($data);
		}
		
		public function getClientAppointment(Request $request)
		{
			$appointment_status = isset($request->appointment_status) ? $request->appointment_status : '';
			$client_id = isset($request->client_id) ? $request->client_id : '';
			$dateRange = isset($request->dateRange) ? $request->dateRange : '';
			
			$start_date = '';
			$end_date = '';
			if($dateRange != '')
			{
				$explodeDate=explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			$data = array();
			if($client_id > 0)
			{	
				$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')
				->select('id', 'full_name', 'appointment_date', 'appointment_time', 'backgroundColor', 'borderColor')
				->where(function ($query) use($start_date, $end_date){
					if($start_date != '' && $end_date != '')
					{
						$query->whereDate('appointment_date', '>=', $start_date)
						->whereDate('appointment_date', '<=', $end_date);
					}
				})
				->where(function ($query) use($appointment_status){
					if($appointment_status != '')
					{
						$query->where('appointment_status', $appointment_status);
					}
				})
				->where('client_id', $client_id)
				->where('setting', 'Appointment')
				->where('is_deleted', 0)
				->get();
				
				$TotalAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')
				->where(function ($query) use($start_date, $end_date){
					if($start_date != '' && $end_date != '')
					{
						$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
						->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
					}
				})
				->where('facebook_ads_lead_user_settings.setting', 'Appointment')
				->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where('facebook_ads_lead_users.client_id', $client_id)
				->count('facebook_ads_lead_user_settings.id');
				
				$ConfirmedAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')
				->where(function ($query) use($start_date, $end_date){
					if($start_date != '' && $end_date != '')
					{
						$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
						->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
					}
				})
				->where('facebook_ads_lead_user_settings.setting', 'Appointment')
				->where('facebook_ads_lead_user_settings.comment', 'Confirmed')
				->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where('facebook_ads_lead_users.client_id', $client_id)
				->count('facebook_ads_lead_user_settings.id');
				
				$ShownAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')
				->where(function ($query) use($start_date, $end_date){
					if($start_date != '' && $end_date != '')
					{
						$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
						->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
					}
				})
				->where('facebook_ads_lead_user_settings.setting', 'Appointment')
				->where('facebook_ads_lead_user_settings.comment', 'Shown')
				->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where('facebook_ads_lead_users.client_id', $client_id)
				->count('facebook_ads_lead_user_settings.id');
				
				$CanceledAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')
				->where(function ($query) use($start_date, $end_date){
					if($start_date != '' && $end_date != '')
					{
						$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
						->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
					}
				})
				->where('facebook_ads_lead_user_settings.setting', 'Appointment')
				->where('facebook_ads_lead_user_settings.comment', 'Canceled')
				->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where('facebook_ads_lead_users.client_id', $client_id)
				->count('facebook_ads_lead_user_settings.id');
			}
			else
			{
				if(Session::get('user_category') == 'admin' || ManagerHelper::ManagerCategory()=='admin' || Session::get('user_category') == 'vendor' || ManagerHelper::ManagerCategory()=='vendor')
				{
					$ShowLead = '';
					$UserArray = array();
					$ClientArray = array();
					if(Session::get('user_category')=='user')
					{
						$ShowLead = ManagerHelper::ManagerLeadMenuAction();
						$UserArray[] = Session::get('user_id');
						$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
						$UserArray[] = $manager->created_user_id;
						$users = DB::table('users')->select('id')->where('created_user_id',$manager->created_user_id)->get();
						foreach($users as $user)
						{
							$UserArray[] = $user->id;
						}
						
						$Vendors = DB::table('users')->select('id')->where('id',$manager->created_user_id)->get();
						foreach($Vendors as $Vendor)
						{
							$Clients = DB::table('clients')->select('id')->where('vendor_id', $Vendor->id)->get();
							foreach($Clients as $Client)
							{
								$ClientArray[] = $Client->id;
								$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
								foreach($users as $user)
								{
									$UserArray[] = $user->id;
									$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
									foreach($Managers as $Manager)
									{
										$UserArray[] = $Manager->id;
									}
								}
							}
						}
					}
					else
					{
						$UserArray[] = Session::get('user_id');
						$users = DB::table('users')->select('id')->where('created_user_id',Session::get('user_id'))->get();
						foreach($users as $user)
						{
							$UserArray[] = $user->id;
						}
						
						$Clients = DB::table('clients')->select('id')->where('vendor_id',Session::get('vendor_id'))->get();
						foreach($Clients as $Client)
						{
							$ClientArray[] = $Client->id;
							$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
							foreach($users as $user)
							{
								$UserArray[] = $user->id;
								$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
								foreach($Managers as $Manager)
								{
									$UserArray[] = $Manager->id;
								}
							}
						}
					}
					
					$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')
					->select('id', 'full_name', 'appointment_date', 'appointment_time', 'backgroundColor', 'borderColor')
					->where(function ($query) use($start_date, $end_date){
						if($start_date != '' && $end_date != '')
						{
							$query->whereDate('appointment_date', '>=', $start_date)
							->whereDate('appointment_date', '<=', $end_date);
						}
					})
					->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
						if($ShowLead=='Allotted Leads')
						{
							$query->where('manager_id', Session::get('manager_id'));
						}
						elseif(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
						{
							$query->where('id', '>', 0);
						}
						else
						{
							$query->whereIn('created_user_id', $UserArray)->whereIn('client_id', $ClientArray, 'or');
						}
					})
					->where(function ($query) use($appointment_status){
						if($appointment_status != '')
						{
							$query->where('appointment_status', $appointment_status);
						}
					})
					->where('client_id', '>', 0)
					->where('setting', 'Appointment')
					->where('is_deleted', 0)
					->get();
					
					$TotalAppointment = DB::table('facebook_ads_lead_user_settings')
					->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
					->select('facebook_ads_lead_user_settings.id')
					->where('facebook_ads_lead_user_settings.setting', 'Appointment')
					->where('facebook_ads_lead_user_settings.is_deleted', 0)
					->where(function ($query) use($start_date, $end_date){
						if($start_date != '' && $end_date != '')
						{
							$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
							->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
						}
					})
					->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
						if($ShowLead=='Allotted Leads')
						{
							$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
						}
						elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
						{
							$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)
							->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
						}
					})
					->count('facebook_ads_lead_user_settings.id');
					
					$ConfirmedAppointment = DB::table('facebook_ads_lead_user_settings')
					->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
					->select('facebook_ads_lead_user_settings.id')
					->where('facebook_ads_lead_user_settings.setting', 'Appointment')
					->where('facebook_ads_lead_user_settings.comment', 'Confirmed')
					->where('facebook_ads_lead_user_settings.is_deleted', 0)
					->where(function ($query) use($start_date, $end_date){
						if($start_date != '' && $end_date != '')
						{
							$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
							->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
						}
					})
					->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
						if($ShowLead=='Allotted Leads')
						{
							$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
						}
						elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
						{
							$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)
							->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
						}
					})
					->count('facebook_ads_lead_user_settings.id');
					
					$ShownAppointment = DB::table('facebook_ads_lead_user_settings')
					->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
					->select('facebook_ads_lead_user_settings.id')
					->where('facebook_ads_lead_user_settings.setting', 'Appointment')
					->where('facebook_ads_lead_user_settings.comment', 'Shown')
					->where('facebook_ads_lead_user_settings.is_deleted', 0)
					->where(function ($query) use($start_date, $end_date){
						if($start_date != '' && $end_date != '')
						{
							$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
							->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
						}
					})
					->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
						if($ShowLead=='Allotted Leads')
						{
							$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
						}
						elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
						{
							$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)
							->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
						}
					})
					->count('facebook_ads_lead_user_settings.id');
					
					$CanceledAppointment = DB::table('facebook_ads_lead_user_settings')
					->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
					->select('facebook_ads_lead_user_settings.id')
					->where('facebook_ads_lead_user_settings.setting', 'Appointment')
					->where('facebook_ads_lead_user_settings.comment', 'Canceled')
					->where('facebook_ads_lead_user_settings.is_deleted', 0)
					->where(function ($query) use($start_date, $end_date){
						if($start_date != '' && $end_date != '')
						{
							$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
							->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
						}
					})
					->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
						if($ShowLead=='Allotted Leads')
						{
							$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
						}
						elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
						{
							$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)
							->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
						}
					})
					->count('facebook_ads_lead_user_settings.id');
				}
			}
			
			foreach($facebook_ads_lead_users as $facebook_ads_lead_user)
			{
				$rows = array();
				$rows['title'] = $facebook_ads_lead_user->full_name;
				$rows['start'] = $facebook_ads_lead_user->appointment_date.'T'.date("H:i:s",strtotime($facebook_ads_lead_user->appointment_time));
				$rows['backgroundColor'] = $facebook_ads_lead_user->backgroundColor;
				$rows['borderColor'] = $facebook_ads_lead_user->borderColor;
				$rows['id'] = "'".$facebook_ads_lead_user->id."'";
				$data[] = $rows;
			}
			
			$response = array();
			
			$response['data'] = $data;
			$response['TotalAppointment'] = number_format($TotalAppointment,0);
			
			if($TotalAppointment == 0)
			{
				$response['ConfirmedAppointment'] = '0%';	
			}
			else
			{
				$response['ConfirmedAppointment'] = number_format(($ConfirmedAppointment/$TotalAppointment)*100,2).'%';	
			}
			
			if($TotalAppointment == 0)
			{
				$response['ShownAppointment'] = '0%';	
			}
			else
			{
				$response['ShownAppointment'] = number_format(($ShownAppointment/$TotalAppointment)*100,2).'%';	
			}
			
			if($TotalAppointment == 0)
			{
				$response['CanceledAppointment'] = '0%';	
			}
			else
			{
				$response['CanceledAppointment'] = number_format(($CanceledAppointment/$TotalAppointment)*100,2).'%';	
			}
			
			return response()->json($response);
		}
		
		public function getAppointmentSummary(Request $request)
		{
			
			$client_id = isset($request->client_id) ? $request->client_id : '';
			$appointment_status = isset($request->appointment_status) ? $request->appointment_status : '';
			$dateRange = isset($request->dateRange) ? $request->dateRange : '';
			
			$start_date = '';
			$end_date = '';
			if($dateRange != '')
			{
				$explodeDate=explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			if($client_id > 0)
			{	
				$TotalAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')
				->where(function ($query) use($start_date, $end_date){
					if($start_date != '' && $end_date != '')
					{
						$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
						->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
					}
				})
				->where('facebook_ads_lead_user_settings.setting', 'Appointment')
				->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where('facebook_ads_lead_users.client_id', $client_id)
				->count('facebook_ads_lead_user_settings.id');
				
				$ConfirmedAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')
				->where(function ($query) use($start_date, $end_date){
					if($start_date != '' && $end_date != '')
					{
						$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
						->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
					}
				})
				->where('facebook_ads_lead_user_settings.setting', 'Appointment')
				->where('facebook_ads_lead_user_settings.comment', 'Confirmed')
				->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where('facebook_ads_lead_users.client_id', $client_id)
				->count('facebook_ads_lead_user_settings.id');
				
				$ShownAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')
				->where(function ($query) use($start_date, $end_date){
					if($start_date != '' && $end_date != '')
					{
						$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
						->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
					}
				})
				->where('facebook_ads_lead_user_settings.setting', 'Appointment')
				->where('facebook_ads_lead_user_settings.comment', 'Shown')
				->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where('facebook_ads_lead_users.client_id', $client_id)
				->count('facebook_ads_lead_user_settings.id');
				
				$CanceledAppointment = DB::table('facebook_ads_lead_user_settings')
				->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
				->select('facebook_ads_lead_user_settings.id')
				->where(function ($query) use($start_date, $end_date){
					if($start_date != '' && $end_date != '')
					{
						$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
						->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
					}
				})
				->where('facebook_ads_lead_user_settings.setting', 'Appointment')
				->where('facebook_ads_lead_user_settings.comment', 'Canceled')
				->where('facebook_ads_lead_user_settings.is_deleted', 0)
				->where('facebook_ads_lead_users.client_id', $client_id)
				->count('facebook_ads_lead_user_settings.id');
			}
			else
			{
				if(Session::get('user_category') == 'admin' || ManagerHelper::ManagerCategory()=='admin' || Session::get('user_category') == 'vendor' || ManagerHelper::ManagerCategory()=='vendor')
				{
					$ShowLead = '';
					$UserArray = array();
					$ClientArray = array();
					if(Session::get('user_category')=='user')
					{
						$ShowLead = ManagerHelper::ManagerLeadMenuAction();
						$UserArray[] = Session::get('user_id');
						$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
						$UserArray[] = $manager->created_user_id;
						$users = DB::table('users')->select('id')->where('created_user_id',$manager->created_user_id)->get();
						foreach($users as $user)
						{
							$UserArray[] = $user->id;
						}
						
						$Vendors = DB::table('users')->select('id')->where('id',$manager->created_user_id)->get();
						foreach($Vendors as $Vendor)
						{
							$Clients = DB::table('clients')->select('id')->where('vendor_id', $Vendor->id)->get();
							foreach($Clients as $Client)
							{
								$ClientArray[] = $Client->id;
								$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
								foreach($users as $user)
								{
									$UserArray[] = $user->id;
									$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
									foreach($Managers as $Manager)
									{
										$UserArray[] = $Manager->id;
									}
								}
							}
						}
					}
					else
					{
						$UserArray[] = Session::get('user_id');
						$users = DB::table('users')->select('id')->where('created_user_id',Session::get('user_id'))->get();
						foreach($users as $user)
						{
							$UserArray[] = $user->id;
						}
						
						$Clients = DB::table('clients')->select('id')->where('vendor_id',Session::get('vendor_id'))->get();
						foreach($Clients as $Client)
						{
							$ClientArray[] = $Client->id;
							$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
							foreach($users as $user)
							{
								$UserArray[] = $user->id;
								$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
								foreach($Managers as $Manager)
								{
									$UserArray[] = $Manager->id;
								}
							}
						}
					}
					
					$TotalAppointment = DB::table('facebook_ads_lead_user_settings')
					->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
					->select('facebook_ads_lead_user_settings.id')
					->where('facebook_ads_lead_user_settings.setting', 'Appointment')
					->where('facebook_ads_lead_user_settings.is_deleted', 0)
					->where(function ($query) use($start_date, $end_date){
						if($start_date != '' && $end_date != '')
						{
							$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
							->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
						}
					})
					->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
						if($ShowLead=='Allotted Leads')
						{
							$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
						}
						elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
						{
							$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)
							->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
						}
					})
					->count('facebook_ads_lead_user_settings.id');
					
					$ConfirmedAppointment = DB::table('facebook_ads_lead_user_settings')
					->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
					->select('facebook_ads_lead_user_settings.id')
					->where('facebook_ads_lead_user_settings.setting', 'Appointment')
					->where('facebook_ads_lead_user_settings.comment', 'Confirmed')
					->where('facebook_ads_lead_user_settings.is_deleted', 0)
					->where(function ($query) use($start_date, $end_date){
						if($start_date != '' && $end_date != '')
						{
							$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
							->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
						}
					})
					->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
						if($ShowLead=='Allotted Leads')
						{
							$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
						}
						elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
						{
							$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)
							->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
						}
					})
					->count('facebook_ads_lead_user_settings.id');
					
					$ShownAppointment = DB::table('facebook_ads_lead_user_settings')
					->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
					->select('facebook_ads_lead_user_settings.id')
					->where('facebook_ads_lead_user_settings.setting', 'Appointment')
					->where('facebook_ads_lead_user_settings.comment', 'Shown')
					->where('facebook_ads_lead_user_settings.is_deleted', 0)
					->where(function ($query) use($start_date, $end_date){
						if($start_date != '' && $end_date != '')
						{
							$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
							->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
						}
					})
					->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
						if($ShowLead=='Allotted Leads')
						{
							$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
						}
						elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
						{
							$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)
							->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
						}
					})
					->count('facebook_ads_lead_user_settings.id');
					
					$CanceledAppointment = DB::table('facebook_ads_lead_user_settings')
					->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
					->select('facebook_ads_lead_user_settings.id')
					->where('facebook_ads_lead_user_settings.setting', 'Appointment')
					->where('facebook_ads_lead_user_settings.comment', 'Canceled')
					->where('facebook_ads_lead_user_settings.is_deleted', 0)
					->where(function ($query) use($start_date, $end_date){
						if($start_date != '' && $end_date != '')
						{
							$query->whereDate('facebook_ads_lead_user_settings.appointment_date', '>=', $start_date)
							->whereDate('facebook_ads_lead_user_settings.appointment_date', '<=', $end_date);
						}
					})
					->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
						if($ShowLead=='Allotted Leads')
						{
							$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
						}
						elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
						{
							$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)
							->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
						}
					})
					->count('facebook_ads_lead_user_settings.id');
				}
			}
			
			$response = array();
			
			$response['TotalAppointment'] = number_format($TotalAppointment,0);
			
			if($TotalAppointment == 0)
			{
				$response['ConfirmedAppointment'] = '0%';	
			}
			else
			{
				$response['ConfirmedAppointment'] = number_format(($ConfirmedAppointment/$TotalAppointment)*100,2).'%';	
			}
			
			if($TotalAppointment == 0)
			{
				$response['ShownAppointment'] = '0%';	
			}
			else
			{
				$response['ShownAppointment'] = number_format(($ShownAppointment/$TotalAppointment)*100,2).'%';	
			}
			
			if($TotalAppointment == 0)
			{
				$response['CanceledAppointment'] = '0%';	
			}
			else
			{
				$response['CanceledAppointment'] = number_format(($CanceledAppointment/$TotalAppointment)*100,2).'%';	
			}
			
			return response()->json($response);
		}
	}																										