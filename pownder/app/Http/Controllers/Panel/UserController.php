<?php
	namespace App\Http\Controllers\Panel;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	
	use DB;
	use Helper;
	use ManagerHelper;
	use AdminHelper;
	use VendorHelper;
	use Redirect;
	use Session;
	use Mail;
	
	class UserController extends Controller
	{
		//--------------(Admin Section Start)---------------//
		public function getAdminUser()
		{			
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Read');
			}
			
			$TableColumn=array();
			$Columns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Admin User List Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$TableColumn[]=$Column->column_value;
			}
			
			return view('panel.frontend.admin_user',['TableColumn' => $TableColumn]);
		}
		
		public function getAdminUserAjax(Request $request)
		{			
			$order = isset($request->order)?$request->order:'ASC';
			$sortString = isset($request->sort)?$request->sort:'User Name';
			$search = isset($request->search)?$request->search:'';
			$client_id = isset($request->client_id)?$request->client_id:'';
			$offset = isset($request->offset)?$request->offset:'0';
			$limit = isset($request->limit)?$request->limit:'9999999999';
			$status = isset($request->status)?$request->status:'1';
			
			if($limit == 'All' )
			{
				$limit = 9999999999;
			}
			
			$start_date = '';
			$end_date = '';
			$dateRange = isset($request->dateRange)?$request->dateRange:'';
			if($dateRange != '')
			{
				$explodeDate = explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			switch($sortString) 
			{
				case 'User Group':
				$sort = 'departments.name';
				break;
				case 'Job Title':
				$sort = 'job_titles.name';
				break;
				case 'User Name':
				$sort = 'managers.full_name';
				break;
				case 'Cell #':
				$sort = 'managers.cell_number';
				break;
				case 'Team':
				$sort = 'teams.name';
				break;
				case 'Employee ID':
				$sort = 'managers.employee_number';
				break;
				case 'Owner':
				$sort = 'users.name';
				break;
				case 'Status':
				$sort = 'managers.status';
				break;
				default:
				$sort = 'managers.full_name';
			}
			
			$data = array();
			$rows = array();
			
			$columns = ['departments.name', 'job_titles.name', 'managers.full_name', 'managers.cell_number', 'teams.name', 'managers.employee_number', 'users.name', 'managers.status'];
			
			$TotalUsers = DB::table('managers')
			->leftJoin('departments', 'managers.department_id', '=', 'departments.id')
			->leftJoin('job_titles', 'managers.job_title_id', '=', 'job_titles.id')
			->leftJoin('teams', 'managers.team_id', '=', 'teams.id')
			->leftJoin('users', 'managers.created_user_id', '=', 'users.id')
			->select('managers.id')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('managers.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($client_id){
				if($client_id!='')
				{
					$query->where('managers.client_id', '=', $client_id);
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('managers.created_at', '=', $start_date);
					}
					else
					{
						$query->whereDate('managers.created_at', '>=', $start_date)
						->whereDate('managers.created_at', '<=', $end_date);
					}
				}
			})
			->where('managers.status', $status)
			->where('managers.is_deleted', 0)
			->count('managers.id');
			
			$Users = DB::table('managers')
			->leftJoin('departments', 'managers.department_id', '=', 'departments.id')
			->leftJoin('job_titles', 'managers.job_title_id', '=', 'job_titles.id')
			->leftJoin('teams', 'managers.team_id', '=', 'teams.id')
			->leftJoin('users', 'managers.created_user_id', '=', 'users.id')
			->select('managers.*', 'departments.name as department', 'job_titles.name as job_title', 'teams.name as team', 'users.name as user_name', 'users.category as user_category')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('managers.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($client_id){
				if($client_id!='')
				{
					$query->where('managers.client_id', '=', $client_id);
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('managers.created_at', '=', $start_date);
					}
					else
					{
						$query->whereDate('managers.created_at', '>=', $start_date)
						->whereDate('managers.created_at', '<=', $end_date);
					}
				}
			})
			->where('managers.status', $status)
			->where('managers.is_deleted', 0)
			->orderBy($sort, $order)
			->skip($offset)
			->take($limit)
			->get();
			
			foreach($Users as $User)
			{
				$user = array();
				$user[''] = '<input type="checkbox" class="user_id" value="'.$User->id.'" />';
				$user['User Name'] = title_case($User->full_name);
				$user['Cell #'] = $User->cell_number;
				$user['Employee ID'] = $User->employee_number;
				
				if($User->team == ''){
					$user['Team'] = '';
					}else{
					$user['Team'] = $User->team;
				}
				
				if($User->department == ''){
					$user['User Group'] = '';
					}else{
					$user['User Group'] = $User->department;
				}
				
				if($User->job_title == ''){
					$user['Job Title'] = '';
					}else{
					$user['Job Title'] = $User->job_title;
				}
				
				if($User->admin_id == Session::get('admin_id')){
					$user['Owner'] = 'Admin';
					}else{
					$user['Owner'] = $User->user_name;
				}
				
				if($User->status==1){
					$user['Status'] = 'Active'; 
					}else{ 
					$user['Status'] = 'Inactive'; 
				}
				
				$ActionString = '<a href="user_edit/'.$User->id.'" title="Edit"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i></a> ';
				
				$ActionString = $ActionString.'<a href="#" onClick="UserDelete('.$User->id.')"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i></a> ';
				
				if($User->status==1){
					$ActionString = $ActionString.'<a href="#" onClick="UserInactive('.$User->id.')"><i class="fa fa-fw fa-star text-danger actions_icon" title="Inactive User"></i></a>';
					}else{
					$ActionString = $ActionString.'<a href="#" onClick="UserActive('.$User->id.')"><i class="fa fa-fw fa-star text-success actions_icon" title="Active User"></i></a>';
				}
				
				$user['Actions'] = $ActionString;
				$rows[] = $user;
			}
			$data['total'] = $TotalUsers;
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
		
		public function getAdminUserAdd()
		{		
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$Departments = DB::table('departments')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$Teams = DB::table('teams')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$JobTitles = DB::table('job_titles')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$Vendors = DB::table('users')->where('category', 'vendor')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$Clients = DB::table('clients')->where('vendor_id', '=', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return view('panel.frontend.admin_user_add',['Departments' => $Departments, 'JobTitles' => $JobTitles, 'Teams' => $Teams, 'Vendors' => $Vendors, 'Clients' => $Clients]);
		}
		
		public function postAdminUserAddValidation(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$rule = ['first_name' => 'required', 'last_name' => 'required', 'job_title' => 'required', 'cell_number' => 'phone_format', 'phone_number' => 'phone_format', 'team' => 'required', 'email_address' => 'required|email|unique:users,email'];
			 
			$this->validate($request,$rule);
		}
		
		public function postAdminUserEditValidation(Request $request)
		{
			if(Session::get('user_category') == 'user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$user = DB::table('users')->where('manager_id', $request->id)->first();
			
			if($request->employee_id > 0)
			{
				$rule = ['first_name' => 'required', 'cell_number' => 'phone_format', 'phone_number' => 'phone_format', 'email_address' => 'email_array'];
			}
			else
			{
				$rule = ['first_name' => 'required', 'last_name' => 'required', 'job_title' => 'required', 'team' => 'required', 'email_address' => 'required|unique:users,email,'.$user->id.'|email', 'cell_number' => 'phone_format', 'phone_number' => 'phone_format'];
			}
			
			$this->validate($request, $rule);
		}
		
		public function postAdminUserAdd(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$picture='';
			if($request->hasFile('picture')) 
			{
				$image=$request->file('picture');
				$picture=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/users/',$picture);
			}
			
			$created_user_id = Session::get('user_id');
			$admin_id = Session::get('admin_id');
			$category = Session::get('user_category');
			$client_id = 0;
			$vendor_id = 0;
			
			if($request->user_type == 'vendor' && $request->vendor_id > 0) 
			{
				$User = DB::table('users')->where('vendor_id', $request->vendor_id)->first();
				$created_user_id = $User->id;
				
				$admin_id = 0;
				$client_id = 0;
				$vendor_id = $request->vendor_id;
				$category = 'vendor';
			}
			
			if($request->user_type == 'client' && $request->client_id > 0) 
			{
				$User = DB::table('users')->where('client_id', $request->client_id)->first();
				$created_user_id = $User->id;
				$admin_id = 0;
				$client_id = $request->client_id;
				$vendor_id = 0;
				$category = 'client';
			}
			
			$manager_id = DB::table('managers')->insertGetId([
			'admin_id' => $admin_id,
			'client_id' => $client_id,
			'vendor_id' => $vendor_id,
			'department_id' => $request->department,
			'job_title_id' => $request->job_title,
			'team_id' => $request->team,
			'category' => $category,
			'user_type' => $category,
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'full_name' => $request->first_name.' '.$request->last_name,
			'username' => $request->username,
			'email' => $request->email_address,
			'employee_number' => $request->employee_number,
			'cell_number' => $request->cell_number,
			'phone_number' => $request->phone_number,
			'picture' => $picture,
			'user_type' => $request->user_type,
			'created_user_id' => $created_user_id,
			'created_user_ip' => $request->ip(),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_user_id' => $created_user_id,
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			DB::table('job_title_menus')->where('job_title_id', $request->job_title)->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			$menu = $request->menu;
			for($i=0;$i<count($menu);$i++)
			{
				if($menu[$i]!='')
				{
					DB::table('manager_menus')->insert([
					"manager_id" => $manager_id,
					"menu" => $menu[$i],
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
					
					$checkExists=DB::table('job_title_menus')->where('job_title_id', $request->job_title)->where('menu', $menu[$i])->where('created_user_id', Session::get('user_id'))->first();
					if(is_null($checkExists))
					{
						DB::table('job_title_menus')->insert([
						"job_title_id" => $request->job_title,
						"menu" => $menu[$i],
						"created_user_id" => Session::get('user_id'),
						'created_user_ip' => $request->ip(),
						"created_at" => date('Y-m-d H:i:s'),
						"updated_user_id" => Session::get('user_id'),
						'updated_user_ip' => $request->ip(),
						"updated_at" => date('Y-m-d H:i:s')
						]);
					}
					else
					{
						DB::table('job_title_menus')->where('job_title_id', $request->job_title)->where('menu', $menu[$i])->where('created_user_id', Session::get('user_id'))
						->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
					}
				}
			}
			
			if(isset($request->dashboard_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->dashboard_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->dashboard_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->dashboard_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->dashboard_permission)->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->campaigns_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->campaigns_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->campaigns_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->campaigns_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->campaigns_permission)->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->lead_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->lead_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->lead_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->lead_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->lead_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->calltrack_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->calltrack_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->calltrack_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->calltrack_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->calltrack_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->fbmassenger_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->fbmassenger_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->fbmassenger_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->fbmassenger_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->fbmassenger_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->reports_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->reports_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->reports_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->reports_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->reports_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->clients_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->clients_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->clients_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->clients_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->clients_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->client_view_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->client_view_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->client_view_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->client_view_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->client_view_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->vendors_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->vendors_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->vendors_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->vendors_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->vendors_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->invoice_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->invoice_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->invoice_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->invoice_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->invoice_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->expense_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->expense_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->expense_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->expense_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->expense_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->setting_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->setting_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->setting_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->setting_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->setting_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			do 
			{
				$user_password = Helper::GenerateRandomPassword(8,true,true,true,true);
			}
			while($user_password=="FALSE");
			
			$password=password_hash($user_password,PASSWORD_BCRYPT); 
			
			DB::table('users')->insert([
			'manager_id'=>$manager_id,
			'name'=>$request->first_name.' '.$request->last_name,
			'email'=>$request->email_address,
			'password'=>$password,
			'category'=>'user',
			'created_user_id'=>$created_user_id,
			'created_user_ip' => $request->ip(),
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_user_id'=>Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at'=>date('Y-m-d H:i:s')
			]);
			
			$data = array(
			'name' => $request->first_name.' '.$request->last_name,
			'email' => $request->email_address,
			'user_password' => $user_password
			);
			
			Mail::send('emails.login_details', $data, function ($message) use ($request) {
				$message->from('noreply@pownder.com', 'Pownder');
				$message->to($request->email_address)->subject('Pownder™ Credentials');
			});
			
			return redirect('user')->with('alert_success','User added successfully'); 
			
		}
		
		public function getAdminUserEdit($id)
		{	
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$UserArray[] = Session::get('user_id');
			$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
			foreach($users as $user)
			{
				$UserArray[] = $user->id;
			}
			
			$User = DB::table('managers')->where('id', $id)->where('is_deleted', 0)->first();
			
			$Departments = DB::table('departments')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$Teams = DB::table('teams')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$JobTitles = DB::table('job_titles')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$Vendors = DB::table('users')->where('category', 'vendor')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$Clients = DB::table('clients')->where('vendor_id', '<=', 0)->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			return view('panel.frontend.admin_user_edit',['User' => $User, 'Departments' => $Departments, 'JobTitles' => $JobTitles, 'Teams' => $Teams, 'Vendors' => $Vendors, 'Clients' => $Clients]);
		}
		
		public function postAdminUserEdit(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$picture='';
			if($request->hasFile('picture')) 
			{
				$image=$request->file('picture');
				$picture=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/users/',$picture);
			}
			else
			{
				$User = DB::table('managers')->where('id', $request->id)->first();
				$picture = $User->picture;
			}
			
			$created_user_id = Session::get('user_id');
			$admin_id = Session::get('admin_id');
			$category = Session::get('user_category');
			$client_id = 0;
			$vendor_id = 0;
			
			if($request->user_type == 'vendor' && $request->vendor_id > 0) 
			{
				$User = DB::table('users')->where('vendor_id', $request->vendor_id)->first();
				$created_user_id = $User->id;
				
				$admin_id = 0;
				$client_id = 0;
				$vendor_id = $request->vendor_id;
				$category = 'vendor';
			}
			
			if($request->user_type == 'client' && $request->client_id > 0) 
			{
				$User = DB::table('users')->where('client_id', $request->client_id)->first();
				$created_user_id = $User->id;
				$admin_id = 0;
				$client_id = $request->client_id;
				$vendor_id = 0;
				$category = 'client';
			}
			
			
			DB::table('managers')
			->where('id', $request->id)
			->update([
			'admin_id' => $admin_id,
			'client_id' => $client_id,
			'vendor_id' => $vendor_id,
			'department_id' => $request->department,
			'job_title_id' => $request->job_title,
			'team_id' => $request->team,
			'category' => $category,
			'user_type' => $category,
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'full_name' => $request->first_name.' '.$request->last_name,
			'username' => $request->username,
			'email' => $request->email_address,
			'employee_number' => $request->employee_number,
			'cell_number' => $request->cell_number,
			'phone_number' => $request->phone_number,
			'picture' => $picture,
			'user_type' => $request->user_type,
			'created_user_id' => $created_user_id,
			'updated_user_id' => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			if($request->employee_id > 0)
			{
				DB::table('ftp_csv_data_1')
				->where('client_id', $client_id)
				->where(function ($query)  use ($request) {
					$query->where('col105', '=', $request->employee_id)
					->orWhere('col107', '=', $request->employee_id)
					->orWhere('col109', '=', $request->employee_id)
					->orWhere('col111', '=', $request->employee_id)
					->orWhere('col113', '=', $request->employee_id)
					->orWhere('col115', '=', $request->employee_id);
				})
				->update(['department' => $request->department]);
			}
			
			DB::table('manager_menus')->where('manager_id', $request->id)->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			DB::table('manager_permissions')->where('manager_id', $request->id)->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			DB::table('job_title_menus')->where('job_title_id', $request->job_title)->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			$menu = $request->menu;
			for($i=0;$i<count($menu);$i++)
			{
				if($menu[$i]!='')
				{
					$checkExists=DB::table('manager_menus')->where('manager_id', $request->id)->where('menu', $menu[$i])->first();
					if(is_null($checkExists))
					{
						DB::table('manager_menus')->insert([
						"manager_id" => $request->id,
						"menu" => $menu[$i],
						"created_user_id" => Session::get('user_id'),
						'created_user_ip' => $request->ip(),
						"created_at" => date('Y-m-d H:i:s'),
						"updated_user_id" => Session::get('user_id'),
						'updated_user_ip' => $request->ip(),
						"updated_at" => date('Y-m-d H:i:s')
						]);
					}
					else
					{
						DB::table('manager_menus')->where('manager_id', $request->id)->where('menu', $menu[$i])
						->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
					}
					
					$checkExists=DB::table('job_title_menus')->where('job_title_id', $request->job_title)->where('menu', $menu[$i])->where('created_user_id', Session::get('user_id'))->first();
					if(is_null($checkExists))
					{
						DB::table('job_title_menus')->insert([
						"job_title_id" => $request->job_title,
						"menu" => $menu[$i],
						"created_user_id" => Session::get('user_id'),
						'created_user_ip' => $request->ip(),
						"created_at" => date('Y-m-d H:i:s'),
						"updated_user_id" => Session::get('user_id'),
						'updated_user_ip' => $request->ip(),
						"updated_at" => date('Y-m-d H:i:s')
						]);
					}
					else
					{
						DB::table('job_title_menus')->where('job_title_id', $request->job_title)->where('menu', $menu[$i])->where('created_user_id', Session::get('user_id'))
						->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
					}
				}
			}
			
			if(isset($request->dashboard_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->dashboard_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->dashboard_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->dashboard_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->dashboard_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->dashboard_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->dashboard_permission)->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->campaigns_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->campaigns_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->campaigns_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->campaigns_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->campaigns_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->campaigns_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->campaigns_permission)->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->lead_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->lead_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->lead_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->lead_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->lead_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->lead_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->lead_permission)->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->calltrack_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->calltrack_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->calltrack_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->calltrack_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->calltrack_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->calltrack_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->calltrack_permission)->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->fbmassenger_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->fbmassenger_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->fbmassenger_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->fbmassenger_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->fbmassenger_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->fbmassenger_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->fbmassenger_permission)->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->reports_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->reports_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->reports_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->reports_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->reports_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->reports_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->reports_permission)->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->clients_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->clients_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->clients_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->clients_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->clients_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->clients_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->clients_permission)->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->client_view_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->client_view_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->client_view_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->client_view_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->client_view_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->client_view_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->client_view_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->vendors_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->vendors_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->vendors_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->vendors_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->vendors_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->vendors_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->vendors_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->invoice_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->invoice_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->invoice_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->invoice_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->invoice_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->invoice_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->invoice_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->expense_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->expense_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->expense_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->expense_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->expense_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->expense_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->expense_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->setting_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->setting_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->setting_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->setting_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->setting_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->setting_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->setting_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			DB::table('users')
			->where('manager_id', $request->id)
			->update([
			'name'=>$request->first_name.' '.$request->last_name,
			'email'=>$request->email_address,
			'created_user_id'=>$created_user_id,
			'updated_user_id'=>Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at'=>date('Y-m-d H:i:s')
			]);
			
			return redirect('user')->with('alert_success','User updated successfully');
		}
		//--------------(Admin Section End)---------------//
		
		//--------------(Vendor Section Start)---------------//
		public function getVendorUser()
		{		
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Read');
			}
			
			$TableColumn=array();
			$Columns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Vendor User List Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$TableColumn[]=$Column->column_value;
			}
			
			return view('panel.frontend.vendor_user',['TableColumn' => $TableColumn]);
		}
		
		public function getVendorUserAjax(Request $request)
		{			
			$order = isset($request->order)?$request->order:'ASC';
			$sortString = isset($request->sort)?$request->sort:'User Name';
			$search = isset($request->search)?$request->search:'';
			$offset = isset($request->offset)?$request->offset:'0';
			$limit = isset($request->limit)?$request->limit:'9999999999';
			$status = isset($request->status)?$request->status:'1';
			$client_id = isset($request->client_id)?$request->client_id:'';
			
			if($limit == 'All' )
			{
				$limit = 9999999999;
			}
			
			$start_date = '';
			$end_date = '';
			$dateRange = isset($request->dateRange)?$request->dateRange:'';
			if($dateRange != '')
			{
				$explodeDate = explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			switch($sortString) 
			{
				case 'User Group':
				$sort = 'departments.name';
				break;
				case 'Job Title':
				$sort = 'job_titles.name';
				break;
				case 'User Name':
				$sort = 'managers.full_name';
				break;
				case 'Cell #':
				$sort = 'managers.cell_number';
				break;
				case 'Team':
				$sort = 'teams.name';
				break;
				case 'Employee ID':
				$sort = 'managers.employee_number';
				break;
				case 'Owner':
				$sort = 'users.name';
				break;
				case 'Status':
				$sort = 'managers.status';
				break;
				default:
				$sort = 'managers.full_name';
			}
			
			$data = array();
			$rows = array();
			
			$columns = ['departments.name', 'job_titles.name', 'managers.full_name', 'managers.cell_number', 'teams.name', 'managers.employee_number', 'users.name', 'managers.status'];
			
			$TotalUsers = DB::table('managers')
			->leftJoin('departments', 'managers.department_id', '=', 'departments.id')
			->leftJoin('job_titles', 'managers.job_title_id', '=', 'job_titles.id')
			->leftJoin('teams', 'managers.team_id', '=', 'teams.id')
			->leftJoin('users', 'managers.created_user_id', '=', 'users.id')
			->select('managers.id')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('managers.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($client_id){
				if($client_id!='')
				{
					$query->where('managers.client_id', '=', $client_id);
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('managers.created_at', '=', $start_date);
					}
					else
					{
						$query->whereDate('managers.created_at', '>=', $start_date)
						->whereDate('managers.created_at', '<=', $end_date);
					}
				}
			})
			->where('managers.vendor_id', Session::get('vendor_id'))
			->where('managers.employee_id', '')
			->where('managers.status', $status)
			->where('managers.is_deleted', 0)
			->count('managers.id');
			
			$Users = DB::table('managers')
			->leftJoin('departments', 'managers.department_id', '=', 'departments.id')
			->leftJoin('job_titles', 'managers.job_title_id', '=', 'job_titles.id')
			->leftJoin('teams', 'managers.team_id', '=', 'teams.id')
			->leftJoin('users', 'managers.created_user_id', '=', 'users.id')
			->select('managers.*', 'departments.name as department', 'job_titles.name as job_title', 'teams.name as team', 'users.name as user_name', 'users.category as user_category')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('managers.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($client_id){
				if($client_id!='')
				{
					$query->where('managers.client_id', '=', $client_id);
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('managers.created_at', '=', $start_date);
					}
					else
					{
						$query->whereDate('managers.created_at', '>=', $start_date)
						->whereDate('managers.created_at', '<=', $end_date);
					}
				}
			})
			->where('managers.vendor_id', Session::get('vendor_id'))
			->where('managers.employee_id', '')
			->where('managers.status', $status)
			->where('managers.is_deleted', 0)
			->orderBy($sort, $order)
			->skip($offset)
			->take($limit)
			->get();
			
			foreach($Users as $User)
			{
				$user = array();
				$user[''] = '<input type="checkbox" class="user_id" value="'.$User->id.'" />';
				$user['User Name'] = title_case($User->full_name);
				$user['Cell #'] = $User->cell_number;
				$user['Employee ID'] = $User->employee_number;
				
				if($User->team == ''){
					$user['Team'] = '';
					}else{
					$user['Team'] = $User->team;
				}
				
				if($User->department == ''){
					$user['User Group'] = '';
					}else{
					$user['User Group'] = $User->department;
				}
				
				if($User->job_title == ''){
					$user['Job Title'] = '';
					}else{
					$user['Job Title'] = $User->job_title;
				}
				
				if($User->status==1){
					$user['Status'] = 'Active'; 
					}else{ 
					$user['Status'] = 'Inactive'; 
				}
				
				$ActionString = '<a href="vuser_edit/'.$User->id.'" title="Edit"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i></a> ';
				
				$ActionString = $ActionString.'<a href="#" onClick="UserDelete('.$User->id.')"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i></a> ';
				
				if($User->status==1){
					$ActionString = $ActionString.'<a href="#" onClick="UserInactive('.$User->id.')"><i class="fa fa-fw fa-star text-danger actions_icon" title="Inactive User"></i></a>';
					}else{
					$ActionString = $ActionString.'<a href="#" onClick="UserActive('.$User->id.')"><i class="fa fa-fw fa-star text-success actions_icon" title="Active User"></i></a>';
				}
				
				$user['Actions'] = $ActionString;
				$rows[] = $user;
			}
			$data['total'] = $TotalUsers;
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
		
		public function getVendorUserAdd()
		{			
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$Departments = DB::table('departments')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$Teams = DB::table('teams')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$JobTitles = DB::table('job_titles')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return view('panel.frontend.vendor_user_add',['Departments' => $Departments, 'JobTitles' => $JobTitles, 'Teams' => $Teams]);
		}
		
		public function postVendorUserAdd(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$picture='';
			if($request->hasFile('picture')) 
			{
				$image=$request->file('picture');
				$picture=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/users/',$picture);
			}
			
			$manager_id = DB::table('managers')->insertGetId([
			'department_id' => $request->department,
			'job_title_id' => $request->job_title,
			'team_id' => $request->team,
			'category' => Session::get('user_category'),
			'client_id' => Session::get('client_id'),
			'vendor_id' => Session::get('vendor_id'),
			'user_type' => Session::get('user_category'),
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'full_name' => $request->first_name.' '.$request->last_name,
			'username' => $request->username,
			'email' => $request->email_address,
			'employee_number' => $request->employee_number,
			'cell_number' => $request->cell_number,
			'phone_number' => $request->phone_number,
			'picture' => $picture,
			'created_user_id' => Session::get('user_id'),
			'created_user_ip' => $request->ip(),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_user_id' => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			DB::table('job_title_menus')
			->where('job_title_id', $request->job_title)
			->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			DB::table('job_title_permissions')
			->where('job_title_id', $request->job_title)
			->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			$menu = $request->menu;
			for($i=0;$i<count($menu);$i++)
			{
				if($menu[$i]!='')
				{
					DB::table('manager_menus')->insert([
					"manager_id" => $manager_id,
					"menu" => $menu[$i],
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
					
					$checkExists=DB::table('job_title_menus')->where('job_title_id', $request->job_title)->where('menu', $menu[$i])->where('created_user_id', Session::get('user_id'))->first();
					if(is_null($checkExists))
					{
						DB::table('job_title_menus')->insert([
						"job_title_id" => $request->job_title,
						"menu" => $menu[$i],
						"created_user_id" => Session::get('user_id'),
						'created_user_ip' => $request->ip(),
						"created_at" => date('Y-m-d H:i:s'),
						"updated_user_id" => Session::get('user_id'),
						'updated_user_ip' => $request->ip(),
						"updated_at" => date('Y-m-d H:i:s')
						]);
					}
					else
					{
						DB::table('job_title_menus')
						->where('job_title_id', $request->job_title)
						->where('menu', $menu[$i])
						->where('created_user_id', Session::get('user_id'))
						->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
					}
				}
			}
			
			if(isset($request->dashboard_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->dashboard_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->dashboard_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->dashboard_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->dashboard_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->campaigns_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->campaigns_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->campaigns_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->campaigns_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->campaigns_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->lead_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->lead_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->lead_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->lead_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->lead_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->calltrack_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->calltrack_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->calltrack_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->calltrack_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->calltrack_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->fbmassenger_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->fbmassenger_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->fbmassenger_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->fbmassenger_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->fbmassenger_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->reports_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->reports_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->reports_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->reports_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->reports_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->clients_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->clients_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->clients_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->clients_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->clients_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->client_view_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->client_view_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->client_view_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->client_view_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->client_view_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->invoice_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->invoice_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->invoice_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->invoice_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->invoice_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->expense_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->expense_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->expense_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->expense_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->expense_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->setting_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->setting_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->setting_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->setting_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->setting_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			do 
			{
				$user_password = Helper::GenerateRandomPassword(8,true,true,true,true);
			}
			while($user_password=="FALSE");
			
			$password=password_hash($user_password,PASSWORD_BCRYPT); 
			
			DB::table('users')->insert([
			'manager_id'=>$manager_id,
			'name'=>$request->first_name.' '.$request->last_name,
			'email'=>$request->email_address,
			'password'=>$password,
			'category'=>'user',
			'created_user_id'=>Session::get('user_id'),
			'created_user_ip' => $request->ip(),
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_user_id'=>Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at'=>date('Y-m-d H:i:s')
			]);
			
			$data = array(
			'name' => $request->first_name.' '.$request->last_name,
			'email' => $request->email_address,
			'user_password' => $user_password
			);
			
			Mail::send('emails.login_details', $data, function ($message) use ($request) {
				$message->from('noreply@pownder.com', 'Pownder');
				$message->to($request->email_address)->subject('Pownder™ Credentials');
			});
			
			return redirect('vuser')->with('alert_success','User added successfully'); 
			
		}
		
		public function getVendorUserEdit($id)
		{	
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			$User = DB::table('managers')->where('id', $id)->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->first();
			
			if(is_null($User))
			{
				return redirect('vuser')->with('alert_danger','Unauthorized user access failed');
			}
			
			$Departments = DB::table('departments')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$Teams = DB::table('teams')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$JobTitles = DB::table('job_titles')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return view('panel.frontend.vendor_user_edit',['User' => $User, 'Departments' => $Departments, 'JobTitles' => $JobTitles, 'Teams' => $Teams]);
		}
		
		public function postVendorUserEdit(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$picture='';
			if($request->hasFile('picture')) 
			{
				$image=$request->file('picture');
				$picture=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/users/',$picture);
			}
			else
			{
				$User = DB::table('managers')->where('id', $request->id)->first();
				$picture = $User->picture;
			}
			
			DB::table('managers')
			->where('id', $request->id)
			->update([
			'department_id' => $request->department,
			'job_title_id' => $request->job_title,
			'team_id' => $request->team,
			'category' => Session::get('user_category'),
			'client_id' => Session::get('client_id'),
			'vendor_id' => Session::get('vendor_id'),
			'user_type' => Session::get('user_category'),
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'full_name' => $request->first_name.' '.$request->last_name,
			'username' => $request->username,
			'email' => $request->email_address,
			'employee_number' => $request->employee_number,
			'cell_number' => $request->cell_number,
			'phone_number' => $request->phone_number,
			'picture' => $picture,
			'updated_user_id' => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			DB::table('manager_menus')
			->where('manager_id', $request->id)
			->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			DB::table('manager_permissions')
			->where('manager_id', $request->id)
			->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			DB::table('job_title_menus')
			->where('job_title_id', $request->job_title)
			->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			DB::table('job_title_permissions')
			->where('job_title_id', $request->job_title)
			->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			$menu = $request->menu;
			for($i=0;$i<count($menu);$i++)
			{
				if($menu[$i]!='')
				{
					$checkExists=DB::table('manager_menus')->where('manager_id', $request->id)->where('menu', $menu[$i])->first();
					if(is_null($checkExists))
					{
						DB::table('manager_menus')->insert([
						"manager_id" => $request->id,
						"menu" => $menu[$i],
						"created_user_id" => Session::get('user_id'),
						'created_user_ip' => $request->ip(),
						"created_at" => date('Y-m-d H:i:s'),
						"updated_user_id" => Session::get('user_id'),
						'updated_user_ip' => $request->ip(),
						"updated_at" => date('Y-m-d H:i:s')
						]);
					}
					else
					{
						DB::table('manager_menus')
						->where('manager_id', $request->id)
						->where('menu', $menu[$i])
						->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
					}
					
					$checkExists=DB::table('job_title_menus')->where('job_title_id', $request->job_title)->where('menu', $menu[$i])->where('created_user_id', Session::get('user_id'))->first();
					if(is_null($checkExists))
					{
						DB::table('job_title_menus')->insert([
						"job_title_id" => $request->job_title,
						"menu" => $menu[$i],
						"created_user_id" => Session::get('user_id'),
						'created_user_ip' => $request->ip(),
						"created_at" => date('Y-m-d H:i:s'),
						"updated_user_id" => Session::get('user_id'),
						'updated_user_ip' => $request->ip(),
						"updated_at" => date('Y-m-d H:i:s')
						]);
					}
					else
					{
						DB::table('job_title_menus')
						->where('job_title_id', $request->job_title)
						->where('menu', $menu[$i])
						->where('created_user_id', Session::get('user_id'))
						->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
					}
				}
			}
			
			if(isset($request->dashboard_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->dashboard_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->dashboard_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->dashboard_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->dashboard_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->dashboard_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->dashboard_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->campaigns_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->campaigns_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->campaigns_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->campaigns_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->campaigns_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->campaigns_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->campaigns_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->lead_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->lead_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->lead_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->lead_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->lead_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->lead_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->lead_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->calltrack_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->calltrack_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->calltrack_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->calltrack_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->calltrack_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->calltrack_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->calltrack_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->fbmassenger_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->fbmassenger_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->fbmassenger_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->fbmassenger_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->fbmassenger_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->fbmassenger_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->fbmassenger_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->reports_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->reports_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->reports_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->reports_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->reports_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->reports_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->reports_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->clients_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->clients_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->clients_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->clients_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->clients_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->clients_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->clients_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->client_view_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->client_view_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->client_view_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->client_view_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->client_view_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->client_view_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->client_view_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->invoice_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->invoice_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->invoice_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->invoice_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->invoice_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->invoice_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->invoice_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->expense_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->expense_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->expense_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->expense_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->expense_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->expense_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->expense_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->setting_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->setting_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->setting_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->setting_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->setting_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->setting_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->setting_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			DB::table('users')
			->where('manager_id', $request->id)
			->update([
			'name'=>$request->first_name.' '.$request->last_name,
			'email'=>$request->email_address,
			'updated_user_id'=>Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at'=>date('Y-m-d H:i:s')
			]);
			
			return redirect('vuser')->with('alert_success','User updated successfully');
		}
		//--------------(Vendor Section End)---------------//
		
		//--------------(Client Section Start)---------------//
		public function getClientUser()
		{	
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Read');
			}
			
			$TableColumn=array();
			$Columns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Client User List Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$TableColumn[]=$Column->column_value;
			}
			
			return view('panel.frontend.client_user',['TableColumn' => $TableColumn]);
		}
		
		public function getClientUserAjax(Request $request)
		{			
			$order = isset($request->order)?$request->order:'ASC';
			$sortString = isset($request->sort)?$request->sort:'User Name';
			$search = isset($request->search)?$request->search:'';
			$offset = isset($request->offset)?$request->offset:'0';
			$limit = isset($request->limit)?$request->limit:'9999999999';
			$status = isset($request->status)?$request->status:'1';
			
			if($limit == 'All' )
			{
				$limit = 9999999999;
			}
			
			$start_date = '';
			$end_date = '';
			$dateRange = isset($request->dateRange)?$request->dateRange:'';
			if($dateRange != '')
			{
				$explodeDate = explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			switch($sortString) 
			{
				case 'User Group':
				$sort = 'departments.name';
				break;
				case 'Job Title':
				$sort = 'job_titles.name';
				break;
				case 'User Name':
				$sort = 'managers.full_name';
				break;
				case 'Cell #':
				$sort = 'managers.cell_number';
				break;
				case 'Team':
				$sort = 'teams.name';
				break;
				case 'Employee ID':
				$sort = 'managers.employee_number';
				break;
				case 'Status':
				$sort = 'managers.status';
				break;
				default:
				$sort = 'managers.full_name';
			}
			
			$data = array();
			$rows = array();
			
			$columns = ['departments.name', 'job_titles.name', 'managers.full_name', 'managers.cell_number', 'teams.name', 'managers.employee_number', 'users.name', 'managers.status'];
			
			$TotalUsers = DB::table('managers')
			->leftJoin('departments', 'managers.department_id', '=', 'departments.id')
			->leftJoin('job_titles', 'managers.job_title_id', '=', 'job_titles.id')
			->leftJoin('teams', 'managers.team_id', '=', 'teams.id')
			->leftJoin('users', 'managers.created_user_id', '=', 'users.id')
			->select('managers.id')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('managers.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('managers.created_at', '=', $start_date);
					}
					else
					{
						$query->whereDate('managers.created_at', '>=', $start_date)
						->whereDate('managers.created_at', '<=', $end_date);
					}
				}
			})
			->where('managers.client_id', Session::get('client_id'))
			->where('managers.employee_id', '')
			->where('managers.status', $status)
			->where('managers.is_deleted', 0)
			->count('managers.id');
			
			$Users = DB::table('managers')
			->leftJoin('departments', 'managers.department_id', '=', 'departments.id')
			->leftJoin('job_titles', 'managers.job_title_id', '=', 'job_titles.id')
			->leftJoin('teams', 'managers.team_id', '=', 'teams.id')
			->leftJoin('users', 'managers.created_user_id', '=', 'users.id')
			->select('managers.*', 'departments.name as department', 'job_titles.name as job_title', 'teams.name as team', 'users.name as user_name', 'users.category as user_category')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('managers.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('managers.created_at', '=', $start_date);
					}
					else
					{
						$query->whereDate('managers.created_at', '>=', $start_date)
						->whereDate('managers.created_at', '<=', $end_date);
					}
				}
			})
			->where('managers.client_id', Session::get('client_id'))
			->where('managers.employee_id', '')
			->where('managers.status', $status)
			->where('managers.is_deleted', 0)
			->orderBy($sort, $order)
			->skip($offset)
			->take($limit)
			->get();
			
			foreach($Users as $User)
			{
				$user = array();
				$user[''] = '<input type="checkbox" class="user_id" value="'.$User->id.'" />';
				$user['User Name'] = title_case($User->full_name);
				$user['Cell #'] = $User->cell_number;
				$user['Employee ID'] = $User->employee_number;
				
				if($User->team == ''){
					$user['Team'] = '';
					}else{
					$user['Team'] = $User->team;
				}
				
				if($User->department == ''){
					$user['User Group'] = '';
					}else{
					$user['User Group'] = $User->department;
				}
				
				if($User->job_title == ''){
					$user['Job Title'] = '';
					}else{
					$user['Job Title'] = $User->job_title;
				}
				
				if($User->status==1){
					$user['Status'] = 'Active'; 
					}else{ 
					$user['Status'] = 'Inactive'; 
				}
				
				$ActionString = '<a href="cuser_edit/'.$User->id.'" title="Edit"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i></a> ';
				
				$ActionString = $ActionString.'<a href="#" onClick="UserDelete('.$User->id.')"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i></a> ';
				
				if($User->status==1){
					$ActionString = $ActionString.'<a href="#" onClick="UserInactive('.$User->id.')"><i class="fa fa-fw fa-star text-danger actions_icon" title="Inactive User"></i></a>';
					}else{
					$ActionString = $ActionString.'<a href="#" onClick="UserActive('.$User->id.')"><i class="fa fa-fw fa-star text-success actions_icon" title="Active User"></i></a>';
				}
				
				$user['Actions'] = $ActionString;
				$rows[] = $user;
			}
			$data['total'] = $TotalUsers;
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
		
		public function getClientUserAdd()
		{			
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$Departments = DB::table('departments')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$Teams = DB::table('teams')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$JobTitles = DB::table('job_titles')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return view('panel.frontend.client_user_add',['Departments' => $Departments, 'JobTitles' => $JobTitles, 'Teams' => $Teams]);
		}
		
		public function postClientUserAdd(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$picture='';
			if($request->hasFile('picture')) 
			{
				$image=$request->file('picture');
				$picture=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/users/',$picture);
			}
			
			$manager_id = DB::table('managers')->insertGetId([
			'department_id' => $request->department,
			'job_title_id' => $request->job_title,
			'team_id' => $request->team,
			'category' => Session::get('user_category'),
			'client_id' => Session::get('client_id'),
			'vendor_id' => Session::get('vendor_id'),
			'user_type' => Session::get('user_category'),
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'full_name' => $request->first_name.' '.$request->last_name,
			'username' => $request->username,
			'email' => $request->email_address,
			'employee_number' => $request->employee_number,
			'cell_number' => $request->cell_number,
			'phone_number' => $request->phone_number,
			'picture' => $picture,
			'created_user_id' => Session::get('user_id'),
			'created_user_ip' => $request->ip(),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_user_id' => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			DB::table('job_title_menus')
			->where('job_title_id', $request->job_title)
			->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			DB::table('job_title_permissions')
			->where('job_title_id', $request->job_title)
			->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			$menu = $request->menu;
			for($i=0;$i<count($menu);$i++)
			{
				if($menu[$i]!='')
				{
					DB::table('manager_menus')->insert([
					"manager_id" => $manager_id,
					"menu" => $menu[$i],
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
					
					$checkExists=DB::table('job_title_menus')->where('job_title_id', $request->job_title)->where('menu', $menu[$i])->where('created_user_id', Session::get('user_id'))->first();
					if(is_null($checkExists))
					{
						DB::table('job_title_menus')->insert([
						"job_title_id" => $request->job_title,
						"menu" => $menu[$i],
						"created_user_id" => Session::get('user_id'),
						'created_user_ip' => $request->ip(),
						"created_at" => date('Y-m-d H:i:s'),
						"updated_user_id" => Session::get('user_id'),
						'updated_user_ip' => $request->ip(),
						"updated_at" => date('Y-m-d H:i:s')
						]);
					}
					else
					{
						DB::table('job_title_menus')
						->where('job_title_id', $request->job_title)
						->where('menu', $menu[$i])
						->where('created_user_id', Session::get('user_id'))
						->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
					}
				}
			}
			
			if(isset($request->dashboard_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->dashboard_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->dashboard_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->dashboard_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->dashboard_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->lead_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->lead_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->lead_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->lead_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->lead_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->calltrack_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->calltrack_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->calltrack_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->calltrack_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->calltrack_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->fbmassenger_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->fbmassenger_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->fbmassenger_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->fbmassenger_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->fbmassenger_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->reports_permission))
			{
				DB::table('manager_permissions')->insert([
				"manager_id" => $manager_id,
				"permission" => $request->reports_permission,
				"created_user_id" => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				"created_at" => date('Y-m-d H:i:s'),
				"updated_user_id" => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				"updated_at" => date('Y-m-d H:i:s')
				]);
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->reports_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->reports_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->reports_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			do 
			{
				$user_password = Helper::GenerateRandomPassword(8,true,true,true,true);
			}
			while($user_password=="FALSE");
			
			$password=password_hash($user_password,PASSWORD_BCRYPT); 
			
			DB::table('users')->insert([
			'manager_id'=>$manager_id,
			'name'=>$request->first_name.' '.$request->last_name,
			'email'=>$request->email_address,
			'password'=>$password,
			'category'=>'user',
			'created_user_id'=>Session::get('user_id'),
			'created_user_ip' => $request->ip(),
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_user_id'=>Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at'=>date('Y-m-d H:i:s')
			]);
			
			$data = array(
			'name' => $request->first_name.' '.$request->last_name,
			'email' => $request->email_address,
			'user_password' => $user_password
			);
			
			Mail::send('emails.login_details', $data, function ($message) use ($request) {
				$message->from('noreply@pownder.com', 'Pownder');
				$message->to($request->email_address)->subject('Pownder™ Credentials');
			});
			
			return redirect('cuser')->with('alert_success','User added successfully'); 
			
		}
		
		public function getClientUserEdit($id)
		{	
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$User = DB::table('managers')->where('id', $id)->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->first();
			
			if(is_null($User))
			{
				return redirect('cuser')->with('alert_danger','Unauthorized user access failed');
			}
			
			$Departments = DB::table('departments')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$Teams = DB::table('teams')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			$JobTitles = DB::table('job_titles')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return view('panel.frontend.client_user_edit',['User' => $User, 'Departments' => $Departments, 'JobTitles' => $JobTitles, 'Teams' => $Teams]);
		}
		
		public function postClientUserEdit(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Write');
			}
			
			$picture='';
			if($request->hasFile('picture')) 
			{
				$image=$request->file('picture');
				$picture=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/users/',$picture);
			}
			else
			{
				$User = DB::table('managers')->where('id', $request->id)->first();
				$picture = $User->picture;
			}
			
			DB::table('managers')
			->where('id', $request->id)
			->update([
			'department_id' => $request->department,
			'job_title_id' => $request->job_title,
			'team_id' => $request->team,
			'category' => Session::get('user_category'),
			'client_id' => Session::get('client_id'),
			'vendor_id' => Session::get('vendor_id'),
			'user_type' => Session::get('user_category'),
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'full_name' => $request->first_name.' '.$request->last_name,
			'username' => $request->username,
			'email' => $request->email_address,
			'employee_number' => $request->employee_number,
			'cell_number' => $request->cell_number,
			'phone_number' => $request->phone_number,
			'picture' => $picture,
			'updated_user_id' => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			DB::table('manager_menus')
			->where('manager_id', $request->id)
			->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			DB::table('manager_permissions')
			->where('manager_id', $request->id)
			->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			DB::table('job_title_menus')
			->where('job_title_id', $request->job_title)
			->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			DB::table('job_title_permissions')
			->where('job_title_id', $request->job_title)
			->where('created_user_id', Session::get('user_id'))
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1]);
			
			$menu = $request->menu;
			for($i=0;$i<count($menu);$i++)
			{
				if($menu[$i]!='')
				{
					$checkExists=DB::table('manager_menus')->where('manager_id', $request->id)->where('menu', $menu[$i])->first();
					if(is_null($checkExists))
					{
						DB::table('manager_menus')->insert([
						"manager_id" => $request->id,
						"menu" => $menu[$i],
						"created_user_id" => Session::get('user_id'),
						'created_user_ip' => $request->ip(),
						"created_at" => date('Y-m-d H:i:s'),
						"updated_user_id" => Session::get('user_id'),
						'updated_user_ip' => $request->ip(),
						"updated_at" => date('Y-m-d H:i:s')
						]);
					}
					else
					{
						DB::table('manager_menus')
						->where('manager_id', $request->id)
						->where('menu', $menu[$i])
						->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
					}
					
					$checkExists=DB::table('job_title_menus')->where('job_title_id', $request->job_title)->where('menu', $menu[$i])->where('created_user_id', Session::get('user_id'))->first();
					if(is_null($checkExists))
					{
						DB::table('job_title_menus')->insert([
						"job_title_id" => $request->job_title,
						"menu" => $menu[$i],
						"created_user_id" => Session::get('user_id'),
						'created_user_ip' => $request->ip(),
						"created_at" => date('Y-m-d H:i:s'),
						"updated_user_id" => Session::get('user_id'),
						'updated_user_ip' => $request->ip(),
						"updated_at" => date('Y-m-d H:i:s')
						]);
					}
					else
					{
						DB::table('job_title_menus')
						->where('job_title_id', $request->job_title)
						->where('menu', $menu[$i])
						->where('created_user_id', Session::get('user_id'))
						->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
					}
				}
			}
			
			if(isset($request->dashboard_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->dashboard_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->dashboard_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->dashboard_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->dashboard_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->dashboard_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->dashboard_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->lead_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->lead_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->lead_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->lead_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->lead_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->lead_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->lead_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->calltrack_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->calltrack_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->calltrack_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->calltrack_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->calltrack_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->calltrack_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')
					->where('job_title_id', $request->job_title)
					->where('permission', $request->calltrack_permission)
					->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->fbmassenger_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->fbmassenger_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->fbmassenger_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')
					->where('manager_id', $request->id)
					->where('permission', $request->fbmassenger_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->fbmassenger_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->fbmassenger_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->fbmassenger_permission)->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			if(isset($request->reports_permission))
			{
				$checkExists=DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->reports_permission)->first();
				if(is_null($checkExists))
				{
					DB::table('manager_permissions')->insert([
					"manager_id" => $request->id,
					"permission" => $request->reports_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('manager_permissions')->where('manager_id', $request->id)->where('permission', $request->reports_permission)
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
				
				$checkExists=DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->reports_permission)->where('created_user_id', Session::get('user_id'))->first();
				if(is_null($checkExists))
				{
					DB::table('job_title_permissions')->insert([
					"job_title_id" => $request->job_title,
					"permission" => $request->reports_permission,
					"created_user_id" => Session::get('user_id'),
					'created_user_ip' => $request->ip(),
					"created_at" => date('Y-m-d H:i:s'),
					"updated_user_id" => Session::get('user_id'),
					'updated_user_ip' => $request->ip(),
					"updated_at" => date('Y-m-d H:i:s')
					]);
				}
				else
				{
					DB::table('job_title_permissions')->where('job_title_id', $request->job_title)->where('permission', $request->reports_permission)->where('created_user_id', Session::get('user_id'))
					->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 0]);
				}
			}
			
			DB::table('users')
			->where('manager_id', $request->id)
			->update(['name'=>$request->first_name.' '.$request->last_name, 'email'=>$request->email_address, 'updated_user_id'=>Session::get('user_id'),
			'updated_user_ip' => $request->ip(), 'updated_at'=>date('Y-m-d H:i:s')]);
			
			return redirect('cuser')->with('alert_success','User updated successfully');
		}
		//--------------(Client Section End)---------------//        
		
		public function editUser()
		{
			$user = DB::table('managers')->where('id', Session::get('manager_id'))->first();
			return view('panel.frontend.edit_user', ['user' => $user]);
		}
		
		public function editUserProfile(Request $request)
		{
			$rule = ['first_name'=>'required', 'last_name'=>'required', 'email'=>'required|email|unique:users,email,'.Session::get('user_id').'', 'photo'=>'image|dimensions:width=180,height=180'];
			
			$this->validate($request,$rule);
			
			if ($request->hasFile('photo')) 
			{
				$image=$request->file('photo');
				$photo=date('dmYHis').$image->getClientOriginalName();
				$image->move('pownder/storage/app/uploads/users/',$photo);
			}
			else
			{
				$client_result=DB::table('managers')->select('picture')->where('id', Session::get('manager_id'))->first();
				$photo=$client_result->picture;
			}
			
			DB::table('managers')->where('id', Session::get('manager_id'))
			->update(['first_name'=>$request->first_name,
			'last_name'=>$request->last_name,
			'full_name' => $request->first_name.' '.$request->last_name,
			'email'=>$request->email,
			'cell_number'=>$request->cell_number,
			'phone_number'=>$request->phone_number,
			'picture'=>$photo,
			'updated_user_id'=>Session::get('user_id'),
			'updated_user_ip'=>$request->ip(),
			'updated_at'=>date('Y-m-d H:i:s')]);
			
			$user=DB::table('users')->where('id',Session::get('user_id'))->first();
			if($request->email==$user->email)
			{
				DB::table('users')->where('id', Session::get('user_id'))
				->update(['name'=>$request->first_name.' '.$request->last_name, 'updated_user_id'=>Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at'=>date('Y-m-d H:i:s')]);
			}
			else
			{
				do 
				{
					$user_password = Helper::GenerateRandomPassword(8,true,true,true,true);
				}
				while($user_password=="FALSE");
				
				$password=password_hash($user_password,PASSWORD_BCRYPT); 
				
				DB::table('users')->where('id', Session::get('user_id'))
				->update(['name'=>$request->first_name.' '.$request->last_name, 'email'=>$request->email, 'password'=>$password, 'updated_user_id'=>Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at'=>date('Y-m-d H:i:s')]);
				
				$data = array('name' => $request->first_name.' '.$request->last_name, 'email' => $request->email, 'user_password' => $user_password);
				
				Mail::send('emails.login_details', $data, function ($message) use ($request) {
					$message->from('noreply@pownder.com', 'Pownder');
					$message->to($request->email)->subject('Pownder™ Credentials');
				});
				
				Session::put('EmailChange','Yes');
			}
			
			Session::put('user_name',$request->first_name.' '.$request->last_name);
			Session::put('photo',$photo);
			
			return Redirect::back()->with('alert_success','Profile updated successfully');
		}
		
		public function changePassword(Request $request)
		{
			$rule = ['password' => 'required|min:8|confirmed', 'password_confirmation'=>'required'];
			
			$this->validate($request,$rule);
			
			DB::table('users')->where('id', Session::get('user_id'))
			->update(['password'=>password_hash($request->password,PASSWORD_BCRYPT), 'updated_user_id'=>Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at'=>date('Y-m-d H:i:s')]);
			
			return Redirect::back()->with('alert_success','Password changed successfully'); 
		}
		
		public function deleteUser(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Read Write');
			}
			
			DB::table('managers')->where('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			
			$user = DB::table('users')->where('manager_id', $request->id)->first();
			
			DB::table('users')->where('manager_id', $request->id)
			->update(['email' => $user->email.'(del)', 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
		}
		
		public function activeUser(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Read Write');
			}
			
			DB::table('managers')->where('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 1]);
			
			DB::table('users')->where('manager_id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 1]);
		}
		
		public function activeBulkUser(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Read Write');
			}
			
			DB::table('managers')->whereIn('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 1]);
			
			DB::table('users')->whereIn('manager_id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 1]);
		}
		
		public function inactiveUser(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Read Write');
			}
			
			DB::table('managers')->where('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 0]);
			
			DB::table('users')->where('manager_id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 0]);
		}
		
		public function inactiveBulkUser(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('User Read Write');
			}
			
			DB::table('managers')->whereIn('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 0]);
			
			DB::table('users')->whereIn('manager_id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 0]);
		}
	}																																															