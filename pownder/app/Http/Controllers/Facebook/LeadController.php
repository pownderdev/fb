<?php
	namespace App\Http\Controllers\Facebook;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	use App\Models\Clients;
	use App\Models\FacebookAdsLeads;
	use App\Models\FacebookAdsLeadUsers;
	
	use DB;
	use Helper;
	use Session;
	use ManagerHelper;
	use AdminHelper;
	use ConsoleHelper;
	use Mail;
	
	class LeadController extends Controller
	{
		public function getLeads()
		{	
			$ShowClient = '';
			$ShowLead = '';
			$vendor_id = 0;
			$UserArray = array();
			$ClientArray = array();
			
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				$ShowClient = ManagerHelper::ManagerClientMenuAction();
				
				if(ManagerHelper::ManagerCategory()=='vendor')
				{
					$UserArray[] = Session::get('user_id');
					$Vendor = DB::table('managers')->where('id',Session::get('manager_id'))->first();
					$vendor_id = $Vendor->vendor_id;
					$Clients = DB::table('clients')->select('id')->where('vendor_id', $vendor_id)->get();
					foreach($Clients as $Client)
					{
						$ClientArray[] = $Client->id;
						$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
						foreach($users as $user)
						{
							$UserArray[] = $user->id;
							$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
							foreach($Managers as $Manager)
							{
								$UserArray[] = $Manager->id;
							}
						}
					}
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				$vendor_id = Session::get('vendor_id');
				
				$Clients = DB::table('clients')->select('id')->where('vendor_id',Session::get('vendor_id'))->get();
				foreach($Clients as $Client)
				{
					$ClientArray[] = $Client->id;
					$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
						$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
						foreach($Managers as $Manager)
						{
							$UserArray[] = $Manager->id;
						}
					}
				}
			}
			
			$UnassignLeads = DB::table('facebook_ads_lead_users')->select('id')
			->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
				elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
				{
					$query->whereIn('created_user_id', $UserArray)
					->whereIn('client_id', $ClientArray, 'or');
				}
			})
			->where('client_id', 0)
			->count('id');
			
			$LeadColumns=array();
			$Columns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Lead Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$LeadColumns[]=$Column->column_value;
			}
			
			$Clients = DB::table('clients')->select('id', 'name', 'email')->where('status', 1)
			->where(function ($query) use($UserArray, $ClientArray, $ShowClient, $vendor_id){
				if($ShowClient=='Allotted Clients')
				{
					$query->where('clients.manager_id', Session::get('manager_id'));
				}
				else
				{
					if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
					{
						$query->whereIn('created_user_id', $UserArray)->where('vendor_id','<=', 0);
					}
					else
					{
						$query->whereIn('created_user_id', $UserArray)->whereIn('id', $ClientArray, 'or')->orWhere('vendor_id', $vendor_id);
					}
				}
			})
			->where('is_deleted', 0)->orderBy('name', 'ASC')->orderBy('email', 'ASC')->get();
			
			if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
			{
				$Managers = DB::table('managers')->select('id', 'first_name', 'last_name')->where('status', 1)->where('is_deleted', 0)->orderBy('first_name', 'ASC')->orderBy('last_name', 'ASC')->get();
			}
			else
			{
				$Managers = DB::table('managers')->select('id', 'first_name', 'last_name')->whereIn('created_user_id', $UserArray)->where('status', 1)->where('is_deleted', 0)->orderBy('first_name', 'ASC')->orderBy('last_name', 'ASC')->get();
			}
			
			$Vendors = DB::table('vendors')->whereIn('created_user_id', $UserArray)->where('status', 1)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$AutomotiveSubCategories=DB::table('group_sub_categories')->select('id', 'name')->where('group_category_id', 6)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$LegalSubCategories=DB::table('group_sub_categories')->select('id', 'name')->where('group_category_id', 18)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$Client = DB::table('clients')->where('id', Session::get('client_id'))->first();
			
			$TotalAppointment = DB::table('facebook_ads_lead_user_settings')
            ->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) {
				if(Session::get('client_id')>0)
				{
					$query->where('facebook_ads_lead_users.client_id', Session::get('client_id'));
				}
			})
			->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
				elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
				{
					$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)
					->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
				}
			})
			->count('facebook_ads_lead_user_settings.id');
			
			$ConfirmedAppointment = DB::table('facebook_ads_lead_user_settings')
            ->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Confirmed')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) {
				if(Session::get('client_id')>0)
				{
					$query->where('facebook_ads_lead_users.client_id', Session::get('client_id'));
				}
			})
			->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
				elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
				{
					$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)
					->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
				}
			})
			->count('facebook_ads_lead_user_settings.id');
			
			$ShownAppointment = DB::table('facebook_ads_lead_user_settings')
            ->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Shown')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) {
				if(Session::get('client_id')>0)
				{
					$query->where('facebook_ads_lead_users.client_id', Session::get('client_id'));
				}
			})
			->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
				elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
				{
					$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)
					->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
				}
			})
			->count('facebook_ads_lead_user_settings.id');
			
			$CanceledAppointment = DB::table('facebook_ads_lead_user_settings')
            ->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Canceled')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) {
				if(Session::get('client_id')>0)
				{
					$query->where('facebook_ads_lead_users.client_id', Session::get('client_id'));
				}
			})
			->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
				elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
				{
					$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)
					->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
				}
			})
			->count('facebook_ads_lead_user_settings.id');
			
			return view('panel.frontend.leads',['Clients' => $Clients, 'Vendors' => $Vendors, 'Client' => $Client, 'Managers' => $Managers, 'LeadColumns' => $LeadColumns, 'AutomotiveSubCategories' => $AutomotiveSubCategories, 'LegalSubCategories' => $LegalSubCategories, 'CanceledAppointment' => $CanceledAppointment, 'ConfirmedAppointment' => $ConfirmedAppointment, 'ShownAppointment' => $ShownAppointment, 'TotalAppointment' => $TotalAppointment, 'UnassignLeads' => $UnassignLeads]);
		}
		
		public function getSearchLeads(Request $request)
		{
			$ShowClient = '';
			$ShowLead = '';
			$vendor_id = 0;
			$UserArray = array();
			$ClientArray = array();
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				$ShowClient = ManagerHelper::ManagerClientMenuAction();
				
				if(ManagerHelper::ManagerCategory()=='vendor')
				{
					$UserArray[] = Session::get('user_id');
					$Vendor = DB::table('managers')->where('id',Session::get('manager_id'))->first();
					$vendor_id = $Vendor->vendor_id;
					$Clients = DB::table('clients')->select('id')->where('vendor_id', $vendor_id)->get();
					foreach($Clients as $Client)
					{
						$ClientArray[] = $Client->id;
						$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
						foreach($users as $user)
						{
							$UserArray[] = $user->id;
							$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
							foreach($Managers as $Manager)
							{
								$UserArray[] = $Manager->id;
							}
						}
					}
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				$vendor_id = Session::get('vendor_id');
				
				$Clients = DB::table('clients')->select('id')->where('vendor_id',Session::get('vendor_id'))->get();
				foreach($Clients as $Client)
				{
					$ClientArray[] = $Client->id;
					$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
						$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
						foreach($Managers as $Manager)
						{
							$UserArray[] = $Manager->id;
						}
					}
				}
			}
			
			$start_date='';
			$end_date='';
			$dateRange=isset($request->dateRange)?$request->dateRange:'';
			if($dateRange!='')
			{
				$explodeDate=explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			$client_id = isset($request->client_id) ? $request->client_id : '';
			$email = isset($request->email) ? $request->email : '';
			$phone_number = isset($request->phone_number) ? $request->phone_number : '';
			$search_setting = isset($request->search_setting) ? $request->search_setting : '';
			$search_appointment_status = isset($request->search_appointment_status) ? $request->search_appointment_status : '';
			$search_unassign_lead = isset($request->search_unassign_lead) ? $request->search_unassign_lead : '';
			$search_phone_number_hide = isset($request->search_phone_number_hide) ? $request->search_phone_number_hide : 'No';
			$search_lead_type = isset($request->search_lead_type) ? $request->search_lead_type : 'All';
			$offset = isset($request->offset) ? $request->offset : '0';
			$limit = isset($request->limit) ? $request->limit : 9999999999;
			
			if($limit == 'All')
			{
				$limit = 9999999999;
			}
			elseif($limit < 10)
			{
				$limit = 10;
			}
			
			$order = $request->order;
			if(!isset($request->sort))
			{
				$order = 'desc';
			}
			
			$sortString = isset($request->sort) ? $request->sort : 'Created Time';
			
			$search = isset($request->search) ? $request->search : '';
			$facebook_ads_lead_id = isset($request->search_lead_form) ? explode(",",$request->search_lead_form) : array();
			$facebook_page_id = isset($request->search_lead_page) ? explode(",",$request->search_lead_page) : array();
			
			switch($sortString) 
			{
				case 'Created Time':
				$sort = 'facebook_ads_lead_users.created_time';
				break;
				case 'Appt. Date & Time':
				$sort = 'facebook_ads_lead_users.appointment_date';
				break;
				case 'Campaign':
				$sort = 'facebook_ads_lead_users.campaign_name';
				break;
				case 'Ad Set Name':
				$sort = 'facebook_ads_lead_users.adset_name';
				break;
				case 'Ad Name':
				$sort = 'facebook_ads_lead_users.ad_name';
				break;
				case 'Full Name':
				$sort = 'facebook_ads_lead_users.full_name';
				break;
				case 'Type':
				$sort = 'facebook_ads_lead_users.lead_type';
				break;
				case 'Email':
				$sort = 'facebook_ads_lead_users.email';
				break;
				case 'Phone #':
				$sort = 'facebook_ads_lead_users.phone_number';
				break;
				case 'City':
				$sort = 'facebook_ads_lead_users.city';
				break;
				case 'User':
				$sort = 'managers.full_name';
				break;
				case 'Client':
				$sort = 'clients.name';
				break;
				default:
				$sort = 'facebook_ads_lead_users.created_time';
			}
			
			$data=array();
			$rows=array();
			
			$columns=['facebook_ads_lead_users.created_time', 'facebook_ads_lead_users.lead_type', 'facebook_ads_lead_users.campaign_name', 'facebook_ads_lead_users.adset_name', 'facebook_ads_lead_users.ad_name', 'facebook_ads_lead_users.full_name', 'facebook_ads_lead_users.first_name', 'facebook_ads_lead_users.last_name', 'facebook_ads_lead_users.email', 'facebook_ads_lead_users.phone_number', 'facebook_ads_lead_users.city', 'clients.name', 'managers.full_name'];
			
			$LeadUsers1 = DB::table('facebook_ads_lead_users')
			->leftJoin('clients', 'facebook_ads_lead_users.client_id', '=', 'clients.id')
			->leftJoin('managers', 'facebook_ads_lead_users.manager_id', '=', 'managers.id')
			->select('facebook_ads_lead_users.id')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('facebook_ads_lead_users.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('facebook_ads_lead_users.created_time', '=', $start_date);
					}
					else
					{
						$query->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
						->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date);
					}
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('facebook_ads_lead_users.client_id', $client_id);
				}
			})
			->where(function ($query) use($search_unassign_lead){
				if($search_unassign_lead == 'show')
				{
					$query->where('facebook_ads_lead_users.client_id', 0);
				}
			})
			->where(function ($query) use($facebook_ads_lead_id){
				if(count($facebook_ads_lead_id) > 0)
				{
					$query->whereIn('facebook_ads_lead_users.facebook_ads_lead_id', $facebook_ads_lead_id);
				}
			})
			->where(function ($query) use($facebook_page_id){
				if(count($facebook_page_id) > 0)
				{
					$query->whereIn('facebook_ads_lead_users.facebook_page_id', $facebook_page_id)
					->where('facebook_ads_lead_users.facebook_ads_lead_id', 0);
				}
			})
			->where(function ($query) use($email){
				if($email!='')
				{
					$query->where('facebook_ads_lead_users.email', '=', $email);
				}
			})
			->where(function ($query) use($phone_number){
				if($phone_number!='')
				{
					$query->where('facebook_ads_lead_users.phone_number', '=', $phone_number);
				}
			})
			->where(function ($query) use($search_phone_number_hide){
				if($search_phone_number_hide == 'Yes')
				{
					$query->where('facebook_ads_lead_users.phone_number_hide', '=', $search_phone_number_hide);
				}
			})
			->where(function ($query) use($search_lead_type){
				if($search_lead_type != 'All')
				{
					$query->where('facebook_ads_lead_users.lead_type', $search_lead_type);
				}
			})
			->where(function ($query) use($search_appointment_status){
				if($search_appointment_status!='')
				{
					$query->where('facebook_ads_lead_users.appointment_status', '=', $search_appointment_status);
				}
			})
			->where(function ($query) use($search_setting){
				if($search_setting!='')
				{
					$query->where('facebook_ads_lead_users.setting', '=', $search_setting);
				}
			})
			->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
				}
				elseif(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin' || Session('vendor_id') == 13)
				{
					$query->where('facebook_ads_lead_users.created_user_id', '>', 0);
				}
				else
				{
					$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
				}
			})->count('facebook_ads_lead_users.id');
			
			$LeadUsers = DB::table('facebook_ads_lead_users')
			->leftJoin('clients', 'facebook_ads_lead_users.client_id', '=', 'clients.id')
			->leftJoin('managers', 'facebook_ads_lead_users.manager_id', '=', 'managers.id')
			->select('facebook_ads_lead_users.id', 'facebook_ads_lead_users.resync', 'facebook_ads_lead_users.phone_number_hide', 'facebook_ads_lead_users.manager_id', 'facebook_ads_lead_users.messenger_lead_id', 'facebook_ads_lead_users.lead_type', 'facebook_ads_lead_users.created_time', 'facebook_ads_lead_users.campaign_name', 'facebook_ads_lead_users.adset_name', 'facebook_ads_lead_users.ad_name', 'facebook_ads_lead_users.full_name', 'facebook_ads_lead_users.first_name', 'facebook_ads_lead_users.last_name', 'facebook_ads_lead_users.email', 'facebook_ads_lead_users.phone_number', 'facebook_ads_lead_users.city', 'facebook_ads_lead_users.client_id', 'facebook_ads_lead_users.setting', 'facebook_ads_lead_users.appointment_date', 'facebook_ads_lead_users.appointment_time', 'facebook_ads_lead_users.appointment_status', 'facebook_ads_lead_users.created_user_id', 'facebook_ads_lead_users.is_deleted', 'clients.name as client_name', 'clients.group_category_id as group_category_id', 'managers.full_name as manager_name')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('facebook_ads_lead_users.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('facebook_ads_lead_users.created_time', '=', $start_date);
					}
					else
					{
						$query->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
						->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date);
					}
				}
			})
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('facebook_ads_lead_users.client_id', $client_id);
				}
			})
			->where(function ($query) use($search_unassign_lead){
				if($search_unassign_lead == 'show')
				{
					$query->where('facebook_ads_lead_users.client_id', 0);
				}
			})
			->where(function ($query) use($facebook_ads_lead_id){
				if(count($facebook_ads_lead_id) > 0)
				{
					$query->whereIn('facebook_ads_lead_users.facebook_ads_lead_id', $facebook_ads_lead_id);
				}
			})
			->where(function ($query) use($facebook_page_id){
				if(count($facebook_page_id) > 0)
				{
					$query->whereIn('facebook_ads_lead_users.facebook_page_id', $facebook_page_id)
					->where('facebook_ads_lead_users.facebook_ads_lead_id', 0);
				}
			})
			->where(function ($query) use($email){
				if($email!='')
				{
					$query->where('facebook_ads_lead_users.email', '=', $email);
				}
			})
			->where(function ($query) use($phone_number){
				if($phone_number!='')
				{
					$query->where('facebook_ads_lead_users.phone_number', '=', $phone_number);
				}
			})
			->where(function ($query) use($search_appointment_status){
				if($search_appointment_status!='')
				{
					$query->where('facebook_ads_lead_users.appointment_status', '=', $search_appointment_status);
				}
			})
			->where(function ($query) use($search_setting){
				if($search_setting!='')
				{
					$query->where('facebook_ads_lead_users.setting', '=', $search_setting);
				}
			})
			->where(function ($query) use($search_phone_number_hide){
				if($search_phone_number_hide == 'Yes')
				{
					$query->where('facebook_ads_lead_users.phone_number_hide', '=', $search_phone_number_hide);
				}
			})
			->where(function ($query) use($search_lead_type){
				if($search_lead_type != 'All')
				{
					$query->where('facebook_ads_lead_users.lead_type', $search_lead_type);
				}
			})
			->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
				}
				elseif(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin' || Session('vendor_id') == 13)
				{
					$query->where('facebook_ads_lead_users.created_user_id', '>', 0);
				}
				else
				{
					$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');
				}
			})
			->orderBy($sort, $order)
			->skip($offset)
			->take($limit)
			->get();
			
			$sno=$offset+1;
			foreach($LeadUsers as $LeadUser)
			{
				$user = array();
				$NormalId = "'".$LeadUser->id."'";
				$user[''] = '<input type="checkbox" class="LeadId" value="'.$LeadUser->id.'" />';
				$user['#'] = $sno++;
				$user['Created Time'] = date('m-d-Y h:i:s A', strtotime($LeadUser->created_time));
				$user['Type'] = $LeadUser->lead_type;
				$user['Campaign'] = $LeadUser->campaign_name;
				$user['Ad Set Name'] = $LeadUser->adset_name;
				$user['Ad Name'] = $LeadUser->ad_name;
				if($LeadUser->full_name != '')
				{
					$user['Full Name'] = title_case($LeadUser->full_name);
				}
				else
				{
					$user['Full Name'] = title_case($LeadUser->first_name).' '.title_case($LeadUser->last_name);
				}		
				
				$user['City'] = title_case($LeadUser->city);
				
				if($LeadUser->phone_number == null || $LeadUser->phone_number == '')
				{
					$user['Phone #'] = '';
				}
				else
				{
					if($LeadUser->phone_number_hide == 'Yes')
					{
						$user['Phone #'] = '<i class="fa fa-bell actions_icon" style="color:#32CD32" onClick="hidePhoneNumber('.$NormalId.', \'No\');" title="Show Phone #"></i> ';
					}
					else
					{
						$user['Phone #'] = $LeadUser->phone_number.' <i class="fa fa-bell-slash actions_icon" style="color:#e01800" onClick="hidePhoneNumber('.$NormalId.', \'Yes\');" title="Hide Phone #"></i> ';
					}
				}
				
				if($LeadUser->email == null)
				{
					$user['Email'] = '';
				}
				else
				{
					$user['Email'] = $LeadUser->email;
				}
				
				if($LeadUser->client_name == null || $LeadUser->client_name == '')
				{
					$user['Client'] = '';
				}
				else
				{
					$user['Client'] = $LeadUser->client_name;
				}
				
				if($LeadUser->setting == 'Appointment')
				{
					$user['Appt. Date & Time'] = $LeadUser->appointment_date.' '.$LeadUser->appointment_time;
				}
				else
				{
					$user['Appt. Date & Time'] = '';
				}
				
				$ActinString = '';
				
				if(Session('vendor_id') == 13)
				{
					if(in_array($LeadUser->client_id, $ClientArray) || in_array($LeadUser->created_user_id, $UserArray))
					{
						if($LeadUser->lead_type != 'Pownder™ Lead' && $LeadUser->messenger_lead_id == '')
						{
							$ActinString = $ActinString.'<i class="fa fa-pencil text-primary actions_icon" onClick="EditLead('.$NormalId.');" title="Edit"></i> ';
						}
						
						if($LeadUser->client_id > 0)
						{
							$group_category_id = $LeadUser->group_category_id;
							if($group_category_id == '6')
							{
								if($LeadUser->setting == '')
								{
									$ActinString = $ActinString.'<i class="fa fa-cogs text-primary actions_icon" onClick="LeadSetting('.$NormalId.');" title="Setting"></i> ';
								}
								else
								{
									if($LeadUser->setting == 'Sold')
									{
										$ActinString = $ActinString.'<i class="fa fa-car text-primary actions_icon" onClick="Sold('.$NormalId.');" title="Sold" style="color:#1cc916"></i> ';
									}
									elseif($LeadUser->setting == 'Need CO-X')
									{
										$ActinString = $ActinString.'<i class="fa fa-user-plus actions_icon" title="Need CO-X" style="color:#18b1c9"  onClick="LeadSetting('.$NormalId.');"></i> ';
									}
									elseif($LeadUser->setting == 'Undecided')
									{
										$ActinString = $ActinString.'<i class="fa fa-exclamation actions_icon" title="Undecided" style="color:#1b42d1"  onClick="LeadSetting('.$NormalId.');"></i> ';
									}
									elseif($LeadUser->setting == 'Bad Credit')
									{
										$ActinString = $ActinString.'<i class="fa fa-thumbs-o-down actions_icon" title="Bad Credit" style="color:#971bd1"  onClick="LeadSetting('.$NormalId.');"></i> ';
									}
									elseif($LeadUser->setting == 'Pre-Check')
									{
										$ActinString = $ActinString.'<i class="fa fa-question actions_icon" title="Pre-Check" style="color:#ce1a9e"  onClick="LeadSetting('.$NormalId.');"></i> ';
									}
									elseif($LeadUser->setting == 'Appointment')
									{
										if($LeadUser->appointment_status == '')
										{
											$ActinString = $ActinString.'<i class="fa fa-cogs text-primary" title="Setting"></i> ';
											$ActinString = $ActinString.'<i class="fa fa-calendar-o actions_icon" title="Appointment @ '.date('m-d-Y',strtotime($LeadUser->appointment_date)).', '.$LeadUser->appointment_time.'" style="color:#c6192e" onClick="AppointmentLeadSetting('.$NormalId.',\'\',\'\',\'\');"></i> ';
										}
										else
										{
											$ActinString = $ActinString.'<i class="fa fa-cogs text-primary actions_icon" onClick="LeadSetting('.$NormalId.');" title="Setting"></i> ';
											$ActinString = $ActinString.'<i class="fa fa-calendar-o actions_icon" title="Appointment '.$LeadUser->appointment_status.' @ '.date('m-d-Y',strtotime($LeadUser->appointment_date)).', '.$LeadUser->appointment_time.'" style="color:#c6192e" onClick="AppointmentLeadSetting('.$NormalId.',\''.$LeadUser->appointment_status.'\',\''.date('m-d-Y',strtotime($LeadUser->appointment_date)).'\',\''.$LeadUser->appointment_time.'\');"></i> ';
										}
									}
								}
							}
							elseif($group_category_id=='18')
							{
								if($LeadUser->setting=='')
								{
									$ActinString=$ActinString.'<i class="fa fa-cogs text-primary actions_icon" onClick="LegalLeadSetting('.$NormalId.');" title="Setting"></i> ';
								}
								else
								{
									if($LeadUser->setting=='Closed')
									{
										$ActinString=$ActinString.'<i class="fa fa-balance-scale text-primary actions_icon" onClick="Legal('.$NormalId.');" title="Closed"></i></a> ';
									}
									elseif($LeadUser->setting=="Doesn't Qualify")
									{
										$ActinString=$ActinString.'<i class="fa fa-question actions_icon" title="Dosen\'t Qualify" style="color:#ce1a9e" onClick="LegalLeadSetting('.$NormalId.');"></i> ';
									}
									elseif($LeadUser->setting=='Undecided')
									{
										$ActinString=$ActinString.'<i class="fa fa-exclamation actions_icon" title="Undecided" style="color:#1b42d1" onClick="LegalLeadSetting('.$NormalId.');"></i> ';
									}
									elseif($LeadUser->setting=='Appointment')
									{
										if($LeadUser->appointment_status == '')
										{
											$ActinString=$ActinString.'<i class="fa fa-cogs text-primary" title="Setting"></i> ';
											$ActinString=$ActinString.'<i class="fa fa-calendar-o actions_icon" title="Appointment @ '.date('m-d-Y',strtotime($LeadUser->appointment_date)).', '.$LeadUser->appointment_time.'" style="color:#c6192e" onClick="AppointmentLeadSetting('.$NormalId.',\'\',\'\',\'\');"></i> ';
										}
										else
										{
											$ActinString=$ActinString.'<i class="fa fa-cogs text-primary actions_icon" onClick="LegalLeadSetting('.$NormalId.');" title="Setting"></i> ';
											$ActinString=$ActinString.'<i class="fa fa-calendar-o actions_icon" title="Appointment '.$LeadUser->appointment_status.' @ '.date('m-d-Y',strtotime($LeadUser->appointment_date)).', '.$LeadUser->appointment_time.'" style="color:#c6192e" onClick="AppointmentLeadSetting('.$NormalId.',\''.$LeadUser->appointment_status.'\',\''.date('m-d-Y',strtotime($LeadUser->appointment_date)).'\',\''.$LeadUser->appointment_time.'\');"></i> ';
										}
									}
								}
							}
							else
							{
								$ActinString=$ActinString.'<i class="fa fa-usd text-primary actions_icon" onClick="Default('.$NormalId.');" title="Other Category"></i> ';
							}
						}
						
						if(Session::get('user_category')!= 'user')
						{
							if($LeadUser->manager_id==0)
							{
								$user['User'] = '';
								$ActinString = $ActinString.'<i class="fa fa-user-plus text-primary actions_icon" onClick="UserLeadAllot('.$NormalId.');" title="Allot Lead to User"></i> ';
							}
							else
							{
								$user['User'] = $LeadUser->manager_name;
								$ActinString = $ActinString.'<i class="fa fa-user-plus text-success actions_icon" onClick="UserLeadReAllot('.$NormalId.','.$LeadUser->manager_id.');" title="Allot Lead to another User"></i> ';
							}
						}
						
						if($LeadUser->resync == 'Yes')
						{
							$ActinString = $ActinString.'<i class="fa fa-fw fa-refresh text-success actions_icon" onClick="ReSync('.$NormalId.');" title="Re-sync"></i> ';
						}
						else
						{
							$ActinString = $ActinString.'<i class="fa fa-fw fa-refresh text-danger actions_icon" onClick="ReSync('.$NormalId.');" title="Re-sync"></i> ';
						}
						
						
						if($LeadUser->is_deleted == 2 || $LeadUser->client_id > 0)
						{
							$ActinString = $ActinString.'<i class="fa fa-fw fa-retweet text-success actions_icon" onClick="ReAssign('.$NormalId.');" title="Re-assign"></i> ';
						}
						else
						{
							$ActinString = $ActinString.'<i class="fa fa-fw fa-retweet actions_icon" title="Re-assign" style="color: #aba8a8;"></i> ';
						}
						
						if($LeadUser->is_deleted == 2)
						{
							$ActinString=$ActinString.'<i class="fa fa-thumbs-o-down text-primary actions_icon" title="Dispute Lead"></i> ';
						}
					}
					else
					{
						if(Session::get('user_category')!= 'user')
						{
							if($LeadUser->manager_id==0)
							{
								$user['User'] = '';
							}
							else
							{
								$user['User'] = $LeadUser->manager_name;
							}
						}
					}
				}
				else
				{
					if($LeadUser->lead_type != 'Pownder™ Lead' && $LeadUser->messenger_lead_id == '')
					{
						$ActinString = $ActinString.'<i class="fa fa-pencil text-primary actions_icon" onClick="EditLead('.$NormalId.');" title="Edit"></i> ';
					}
					
					if($LeadUser->client_id > 0)
					{
						$group_category_id = $LeadUser->group_category_id;
						if($group_category_id == '6')
						{
							if($LeadUser->setting == '')
							{
								$ActinString = $ActinString.'<i class="fa fa-cogs text-primary actions_icon" onClick="LeadSetting('.$NormalId.');" title="Setting"></i> ';
							}
							else
							{
								if($LeadUser->setting == 'Sold')
								{
									$ActinString = $ActinString.'<i class="fa fa-car text-primary actions_icon" onClick="Sold('.$NormalId.');" title="Sold" style="color:#1cc916"></i> ';
								}
								elseif($LeadUser->setting == 'Need CO-X')
								{
									$ActinString = $ActinString.'<i class="fa fa-user-plus actions_icon" title="Need CO-X" style="color:#18b1c9"  onClick="LeadSetting('.$NormalId.');"></i> ';
								}
								elseif($LeadUser->setting == 'Undecided')
								{
									$ActinString = $ActinString.'<i class="fa fa-exclamation actions_icon" title="Undecided" style="color:#1b42d1"  onClick="LeadSetting('.$NormalId.');"></i> ';
								}
								elseif($LeadUser->setting == 'Bad Credit')
								{
									$ActinString = $ActinString.'<i class="fa fa-thumbs-o-down actions_icon" title="Bad Credit" style="color:#971bd1"  onClick="LeadSetting('.$NormalId.');"></i> ';
								}
								elseif($LeadUser->setting == 'Pre-Check')
								{
									$ActinString = $ActinString.'<i class="fa fa-question actions_icon" title="Pre-Check" style="color:#ce1a9e"  onClick="LeadSetting('.$NormalId.');"></i> ';
								}
								elseif($LeadUser->setting == 'Appointment')
								{
									if($LeadUser->appointment_status == '')
									{
										$ActinString = $ActinString.'<i class="fa fa-cogs text-primary" title="Setting"></i> ';
										$ActinString = $ActinString.'<i class="fa fa-calendar-o actions_icon" title="Appointment @ '.date('m-d-Y',strtotime($LeadUser->appointment_date)).', '.$LeadUser->appointment_time.'" style="color:#c6192e" onClick="AppointmentLeadSetting('.$NormalId.',\'\',\'\',\'\');"></i> ';
									}
									else
									{
										$ActinString = $ActinString.'<i class="fa fa-cogs text-primary actions_icon" onClick="LeadSetting('.$NormalId.');" title="Setting"></i> ';
										$ActinString = $ActinString.'<i class="fa fa-calendar-o actions_icon" title="Appointment '.$LeadUser->appointment_status.' @ '.date('m-d-Y',strtotime($LeadUser->appointment_date)).', '.$LeadUser->appointment_time.'" style="color:#c6192e" onClick="AppointmentLeadSetting('.$NormalId.',\''.$LeadUser->appointment_status.'\',\''.date('m-d-Y',strtotime($LeadUser->appointment_date)).'\',\''.$LeadUser->appointment_time.'\');"></i> ';
									}
								}
							}
						}
						elseif($group_category_id=='18')
						{
							if($LeadUser->setting=='')
							{
								$ActinString=$ActinString.'<i class="fa fa-cogs text-primary actions_icon" onClick="LegalLeadSetting('.$NormalId.');" title="Setting"></i> ';
							}
							else
							{
								if($LeadUser->setting=='Closed')
								{
									$ActinString=$ActinString.'<i class="fa fa-balance-scale text-primary actions_icon" onClick="Legal('.$NormalId.');" title="Closed"></i></a> ';
								}
								elseif($LeadUser->setting=="Doesn't Qualify")
								{
									$ActinString=$ActinString.'<i class="fa fa-question actions_icon" title="Dosen\'t Qualify" style="color:#ce1a9e" onClick="LegalLeadSetting('.$NormalId.');"></i> ';
								}
								elseif($LeadUser->setting=='Undecided')
								{
									$ActinString=$ActinString.'<i class="fa fa-exclamation actions_icon" title="Undecided" style="color:#1b42d1" onClick="LegalLeadSetting('.$NormalId.');"></i> ';
								}
								elseif($LeadUser->setting=='Appointment')
								{
									if($LeadUser->appointment_status == '')
									{
										$ActinString=$ActinString.'<i class="fa fa-cogs text-primary" title="Setting"></i> ';
										$ActinString=$ActinString.'<i class="fa fa-calendar-o actions_icon" title="Appointment @ '.date('m-d-Y',strtotime($LeadUser->appointment_date)).', '.$LeadUser->appointment_time.'" style="color:#c6192e" onClick="AppointmentLeadSetting('.$NormalId.',\'\',\'\',\'\');"></i> ';
									}
									else
									{
										$ActinString=$ActinString.'<i class="fa fa-cogs text-primary actions_icon" onClick="LegalLeadSetting('.$NormalId.');" title="Setting"></i> ';
										$ActinString=$ActinString.'<i class="fa fa-calendar-o actions_icon" title="Appointment '.$LeadUser->appointment_status.' @ '.date('m-d-Y',strtotime($LeadUser->appointment_date)).', '.$LeadUser->appointment_time.'" style="color:#c6192e" onClick="AppointmentLeadSetting('.$NormalId.',\''.$LeadUser->appointment_status.'\',\''.date('m-d-Y',strtotime($LeadUser->appointment_date)).'\',\''.$LeadUser->appointment_time.'\');"></i> ';
									}
								}
							}
						}
						else
						{
							$ActinString=$ActinString.'<i class="fa fa-usd text-primary actions_icon" onClick="Default('.$NormalId.');" title="Other Category"></i> ';
						}
					}
					
					if(Session::get('user_category')!= 'user')
					{
						if($LeadUser->manager_id==0)
						{
							$user['User'] = '';
							$ActinString = $ActinString.'<i class="fa fa-user-plus text-primary actions_icon" onClick="UserLeadAllot('.$NormalId.');" title="Allot Lead to User"></i> ';
						}
						else
						{
							$user['User'] = $LeadUser->manager_name;
							$ActinString = $ActinString.'<i class="fa fa-user-plus text-success actions_icon" onClick="UserLeadReAllot('.$NormalId.','.$LeadUser->manager_id.');" title="Allot Lead to another User"></i> ';
						}
					}
					
					if($LeadUser->resync == 'Yes')
					{
						$ActinString = $ActinString.'<i class="fa fa-fw fa-refresh text-success actions_icon" onClick="ReSync('.$NormalId.');" title="Re-sync"></i> ';
					}
					else
					{
						$ActinString = $ActinString.'<i class="fa fa-fw fa-refresh text-danger actions_icon" onClick="ReSync('.$NormalId.');" title="Re-sync"></i> ';
					}
					
					
					if($LeadUser->is_deleted == 2 || $LeadUser->client_id > 0)
					{
						$ActinString = $ActinString.'<i class="fa fa-fw fa-retweet text-success actions_icon" onClick="ReAssign('.$NormalId.');" title="Re-assign"></i> ';
					}
					else
					{
						$ActinString = $ActinString.'<i class="fa fa-fw fa-retweet actions_icon" title="Re-assign" style="color: #aba8a8;"></i> ';
					}
					
					if($LeadUser->is_deleted == 2)
					{
						$ActinString=$ActinString.'<i class="fa fa-thumbs-o-down text-primary actions_icon" title="Dispute Lead"></i> ';
					}
				}
				
				$user['Action'] = $ActinString;
				$rows[] = $user;
			}		
			
			$data['total']=$LeadUsers1;
			$data['rows']=$rows;
			
			return \Response::json($data);
		}
		
		public function getClientLeads()
		{	
			$ShowLead = '';
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				$manager = DB::table('managers')->select('client_id')->where('id',Session::get('manager_id'))->first();
				$client_id = $manager->client_id;
			}
			else
			{
				$client_id = Session::get('client_id');
			}
			
			$TotalAppointment = DB::table('facebook_ads_lead_user_settings')
            ->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) use($client_id, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->where('facebook_ads_lead_users.client_id', $client_id);
				}
			})->count('facebook_ads_lead_user_settings.id');
			
			$ConfirmedAppointment = DB::table('facebook_ads_lead_user_settings')
            ->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Confirmed')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) use($client_id, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->where('facebook_ads_lead_users.client_id', $client_id);
				}
			})->count('facebook_ads_lead_user_settings.id');
			
			$ShownAppointment = DB::table('facebook_ads_lead_user_settings')
            ->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Shown')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) use($client_id, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->where('facebook_ads_lead_users.client_id', $client_id);
				}
			})->count('facebook_ads_lead_user_settings.id');
			
			$CanceledAppointment = DB::table('facebook_ads_lead_user_settings')
            ->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Canceled')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) use($client_id, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->where('facebook_ads_lead_users.client_id', $client_id);
				}
			})->count('facebook_ads_lead_user_settings.id');
			
			$ClientLeadColumns=array();
			$Columns=DB::table('table_visible_columns')->select('column_value')	->where('table_name', 'Client Lead Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$ClientLeadColumns[]=$Column->column_value;
			}
			
			$group_category_id = Helper::ClientGroupCategory($client_id);
			
			$GroupSubCategories = DB::table('group_sub_categories')->select('id', 'name')->where('group_category_id', $group_category_id)
			->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			if($group_category_id==6)
			{
				$displayModel='Yes';
			}
			else
			{
				$displayModel='No';
			}
			
			$AutomotiveSubCategories=DB::table('group_sub_categories')->select('id', 'name')->where('group_category_id', 6)
			->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$GroupCategories=DB::table('group_categories')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$LegalSubCategories=DB::table('group_sub_categories')->select('id', 'name')->where('group_category_id', 18)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$Managers=DB::table('managers')->select('id', 'first_name', 'last_name')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->orderBy('first_name', 'ASC')->orderBy('last_name', 'ASC')->get();
			
			return view('panel.frontend.client_leads',['ClientLeadColumns' => $ClientLeadColumns, 'Managers' => $Managers, 'GroupSubCategories' => $GroupSubCategories, 'GroupCategories' => $GroupCategories, 'displayModel' => $displayModel, 'AutomotiveSubCategories' => $AutomotiveSubCategories, 'LegalSubCategories' => $LegalSubCategories, 'lead_client_id' => $client_id, 'CanceledAppointment' => $CanceledAppointment, 'ConfirmedAppointment' => $ConfirmedAppointment, 'ShownAppointment' => $ShownAppointment, 'TotalAppointment' => $TotalAppointment]);
		}
		
		public function getClientSearchLeads(Request $request)
		{
			$ShowLead = '';
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				$manager = DB::table('managers')->select('client_id')->where('id', Session::get('manager_id'))->first();
				$client_id = $manager->client_id;
			}
			else
			{
				$client_id = Session::get('client_id');
			}
			
			$start_date='';
			$end_date='';
			$dateRange=isset($request->dateRange)?$request->dateRange:'';
			if($dateRange!='')
			{
				$explodeDate=explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			$offset=isset($request->offset)?$request->offset:'0';
			$email=isset($request->email)?$request->email:'';
			$phone_number=isset($request->phone_number)?$request->phone_number:'';
			$limit=isset($request->limit)?$request->limit:'9999999999';
			$search_setting=isset($request->search_setting)?$request->search_setting:'';
			$search_appointment_status=isset($request->search_appointment_status)?$request->search_appointment_status:'';
			$search_phone_number_hide=isset($request->search_phone_number_hide)?$request->search_phone_number_hide:'No';
			$search_lead_type = isset($request->search_lead_type) ? $request->search_lead_type : 'All';
			
			if($limit == 'All')
			{
				$limit = 9999999999;
			}
			elseif($limit < 10)
			{
				$limit = 10;
			}
			
			$order = $request->order;
			if(!isset($request->sort))
			{
				$order='desc';
			}
			
			$sortString=isset($request->sort)?$request->sort:'Created Time';
			
			$search=isset($request->search)?$request->search:'';
			
			switch($sortString) 
			{
				case 'Created Time':
				$sort = 'facebook_ads_lead_users.created_time';
				break;
				case 'Appt. Date & Time':
				$sort = 'facebook_ads_lead_users.appointment_date';
				break;
				case 'Full Name':
				$sort = 'facebook_ads_lead_users.full_name';
				break;
				case 'Email':
				$sort = 'facebook_ads_lead_users.email';
				break;
				case 'Phone #':
				$sort = 'facebook_ads_lead_users.phone_number';
				break;
				case 'City':
				$sort = 'facebook_ads_lead_users.city';
				break;
				case 'Type':
				$sort = 'facebook_ads_lead_users.lead_type';
				break;
				case 'User':
				$sort = 'managers.full_name';
				break;
				default:
				$sort = 'facebook_ads_lead_users.created_time';
			}
			
			$data=array();
			$rows=array();
			
			$columns=['facebook_ads_lead_users.created_time', 'facebook_ads_lead_users.lead_type', 'facebook_ads_lead_users.full_name', 'facebook_ads_lead_users.first_name', 'facebook_ads_lead_users.last_name', 'facebook_ads_lead_users.email', 'facebook_ads_lead_users.phone_number', 'facebook_ads_lead_users.city', 'managers.full_name'];
			
			$LeadUsers1 = DB::table('facebook_ads_lead_users')
			->leftJoin('managers', 'facebook_ads_lead_users.manager_id', '=', 'managers.id')
			->select('facebook_ads_lead_users.id')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('facebook_ads_lead_users.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('facebook_ads_lead_users.created_time', '=', $start_date);
					}
					else
					{
						$query->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
						->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date);
					}
				}
			})
			->where(function ($query) use($client_id, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
				}
				else
				{
					$query->where('facebook_ads_lead_users.client_id', $client_id);
				}
			})
			->where(function ($query) use($email){
				if($email!='')
				{
					$query->where('facebook_ads_lead_users.email', '=', $email);
				}
			})
			->where(function ($query) use($phone_number){
				if($phone_number!='')
				{
					$query->where('facebook_ads_lead_users.phone_number', '=', $phone_number);
				}
			})
			->where(function ($query) use($search_appointment_status){
				if($search_appointment_status!='')
				{
					$query->where('facebook_ads_lead_users.appointment_status', '=', $search_appointment_status);
				}
			})
			->where(function ($query) use($search_setting){
				if($search_setting!='')
				{
					$query->where('facebook_ads_lead_users.setting', '=', $search_setting);
				}
			})
			->where(function ($query) use($search_phone_number_hide){
				if($search_phone_number_hide == 'Yes')
				{
					$query->where('facebook_ads_lead_users.phone_number_hide', '=', $search_phone_number_hide);
				}
			})
			->where(function ($query) use($search_lead_type){
				if($search_lead_type != 'All')
				{
					$query->where('facebook_ads_lead_users.lead_type', $search_lead_type);
				}
			})
			->count('facebook_ads_lead_users.id');
			
			$LeadUsers=DB::table('facebook_ads_lead_users')
			->leftJoin('managers', 'facebook_ads_lead_users.manager_id', '=', 'managers.id')
			->leftJoin('clients', 'facebook_ads_lead_users.client_id', '=', 'clients.id')
			->select('facebook_ads_lead_users.id', 'facebook_ads_lead_users.resync', 'facebook_ads_lead_users.phone_number_hide', 'facebook_ads_lead_users.manager_id', 'facebook_ads_lead_users.messenger_lead_id', 'facebook_ads_lead_users.lead_type', 'facebook_ads_lead_users.created_time', 'facebook_ads_lead_users.full_name', 'facebook_ads_lead_users.first_name', 'facebook_ads_lead_users.last_name', 'facebook_ads_lead_users.email', 'facebook_ads_lead_users.phone_number', 'facebook_ads_lead_users.city', 'facebook_ads_lead_users.client_ads_lead_user_id', 'facebook_ads_lead_users.setting', 'facebook_ads_lead_users.appointment_date', 'facebook_ads_lead_users.appointment_time', 'facebook_ads_lead_users.appointment_status', 'facebook_ads_lead_users.client_id', 'managers.full_name as manager_name', 'clients.group_category_id as group_category_id')
			->where(function ($query) use($search, $columns){
				if($search!='')
				{
					$query->where('facebook_ads_lead_users.id', 0);
					foreach($columns as $column)
					{
						$query->orWhere($column, 'like', '%'.$search.'%');
					}
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					if($start_date==$end_date)
					{
						$query->whereDate('facebook_ads_lead_users.created_time', '=', $start_date);
					}
					else
					{
						$query->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
						->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date);
					}
				}
			})
			->where(function ($query) use($client_id, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));
				}
				else
				{
					$query->where('facebook_ads_lead_users.client_id', $client_id);
				}
			})
			->where(function ($query) use($email){
				if($email!='')
				{
					$query->where('facebook_ads_lead_users.email', '=', $email);
				}
			})
			->where(function ($query) use($phone_number){
				if($phone_number!='')
				{
					$query->where('facebook_ads_lead_users.phone_number', '=', $phone_number);
				}
			})
			->where(function ($query) use($search_appointment_status){
				if($search_appointment_status!='')
				{
					$query->where('facebook_ads_lead_users.appointment_status', '=', $search_appointment_status);
				}
			})
			->where(function ($query) use($search_setting){
				if($search_setting!='')
				{
					$query->where('facebook_ads_lead_users.setting', '=', $search_setting);
				}
			})
			->where(function ($query) use($search_phone_number_hide){
				if($search_phone_number_hide == 'Yes')
				{
					$query->where('facebook_ads_lead_users.phone_number_hide', '=', $search_phone_number_hide);
				}
			})
			->where(function ($query) use($search_lead_type){
				if($search_lead_type != 'All')
				{
					$query->where('facebook_ads_lead_users.lead_type', $search_lead_type);
				}
			})
			->orderBy($sort, $order)
			->skip($offset)
			->take($limit)
			->get();
			
			$sno = $offset+1;
			foreach($LeadUsers as $LeadUser)
			{
				$user = array();
				$NormalId = "'".$LeadUser->id."'";
				$user[''] = '<input type="checkbox" class="LeadId" value="'.$LeadUser->id.'" />';
				$user['#']=$sno++;
				$user['Created Time']=date('m-d-Y h:i:s A', strtotime($LeadUser->created_time));
				$user['Type']=$LeadUser->lead_type;
				if($LeadUser->full_name!='')
				{
					$user['Full Name']=title_case($LeadUser->full_name);
				}
				else
				{
					$user['Full Name']=title_case($LeadUser->first_name).' '.title_case($LeadUser->last_name);
				}		
				
				$user['City']=title_case($LeadUser->city);
				
				if($LeadUser->phone_number == null || $LeadUser->phone_number == '')
				{
					$user['Phone #'] = '';
				}
				else
				{
					if($LeadUser->phone_number_hide == 'Yes')
					{
						$user['Phone #'] = '<i class="fa fa-bell actions_icon" style="color:#32CD32" onClick="hidePhoneNumber('.$NormalId.', \'No\');" title="Show Phone #"></i> ';
					}
					else
					{
						$user['Phone #'] = $LeadUser->phone_number.' <i class="fa fa-bell-slash actions_icon" style="color:#e01800" onClick="hidePhoneNumber('.$NormalId.', \'Yes\');" title="Hide Phone #"></i> ';
					}
				}
				
				if($LeadUser->email==null)
				{
					$user['Email']='';
				}
				else
				{
					$user['Email']=$LeadUser->email;
				}
				
				if($LeadUser->setting == 'Appointment')
				{
					$user['Appt. Date & Time'] = $LeadUser->appointment_date.' '.$LeadUser->appointment_time;
				}
				else
				{
					$user['Appt. Date & Time'] = '';
				}
				
				$ActinString = '';
				
				if($LeadUser->lead_type != 'Pownder™ Lead' && $LeadUser->messenger_lead_id == '')
				{
					$ActinString=$ActinString.'<i class="fa fa-pencil text-primary actions_icon" onClick="EditLead('.$NormalId.');" title="Edit"></i> ';
				}
				
				$group_category_id=$LeadUser->group_category_id;
				if($group_category_id=='6')
				{
					if($LeadUser->setting=='')
					{
						$ActinString=$ActinString.'<i class="fa fa-cogs text-primary actions_icon" onClick="LeadSetting('.$NormalId.');" title="Setting"></i> ';
					}
					else
					{
						if($LeadUser->setting=='Sold')
						{
							$ActinString=$ActinString.'<i class="fa fa-car text-primary actions_icon" onClick="Sold('.$NormalId.');" title="Sold" style="color:#1cc916"></i> ';
						}
						elseif($LeadUser->setting=='Need CO-X')
						{
							$ActinString=$ActinString.'<i class="fa fa-user-plus actions_icon" title="Need CO-X" style="color:#18b1c9" onClick="LeadSetting('.$NormalId.');"></i> ';
						}
						elseif($LeadUser->setting=='Undecided')
						{
							$ActinString=$ActinString.'<i class="fa fa-exclamation actions_icon" title="Undecided" style="color:#1b42d1" onClick="LeadSetting('.$NormalId.');"></i> ';
						}
						elseif($LeadUser->setting=='Bad Credit')
						{
							$ActinString=$ActinString.'<i class="fa fa-thumbs-o-down actions_icon" title="Bad Credit" style="color:#971bd1" onClick="LeadSetting('.$NormalId.');"></i> ';
						}
						elseif($LeadUser->setting=='Pre-Check')
						{
							$ActinString=$ActinString.'<i class="fa fa-question actions_icon" title="Pre-Check" style="color:#ce1a9e" onClick="LeadSetting('.$NormalId.');"></i> ';
						}
						elseif($LeadUser->setting=='Appointment')
						{
							if($LeadUser->appointment_status == '')
							{
								$ActinString=$ActinString.'<i class="fa fa-cogs text-primary" title="Setting"></i> ';
								$ActinString=$ActinString.'<i class="fa fa-calendar-o actions_icon" title="Appointment @ '.date('m-d-Y',strtotime($LeadUser->appointment_date)).', '.$LeadUser->appointment_time.'" style="color:#c6192e" onClick="AppointmentLeadSetting('.$NormalId.',\'\',\'\',\'\');"></i> ';
							}
							else
							{
								$ActinString=$ActinString.'<i class="fa fa-cogs text-primary actions_icon" onClick="LeadSetting('.$NormalId.');" title="Setting"></i> ';
								$ActinString=$ActinString.'<i class="fa fa-calendar-o actions_icon" title="Appointment '.$LeadUser->appointment_status.' @ '.date('m-d-Y',strtotime($LeadUser->appointment_date)).', '.$LeadUser->appointment_time.'" style="color:#c6192e" onClick="AppointmentLeadSetting('.$NormalId.',\''.$LeadUser->appointment_status.'\',\''.date('m-d-Y',strtotime($LeadUser->appointment_date)).'\',\''.$LeadUser->appointment_time.'\');"></i> ';
							}
						}
					}
				}
				elseif($group_category_id=='18')
				{
					if($LeadUser->setting=='')
					{
						$ActinString=$ActinString.'<i class="fa fa-cogs text-primary actions_icon" onClick="LegalLeadSetting('.$NormalId.');" title="Setting"></i> ';
					}
					else
					{
						if($LeadUser->setting=='Closed')
						{
							$ActinString=$ActinString.'<i class="fa fa-balance-scale text-primary actions_icon" onClick="Legal('.$NormalId.');" title="Closed"></i></a> ';
						}
						elseif($LeadUser->setting=="Doesn't Qualify")
						{
							$ActinString=$ActinString.'<i class="fa fa-question actions_icon" title="Dosen\'t Qualify" style="color:#ce1a9e" onClick="LegalLeadSetting('.$NormalId.');"></i> ';
						}
						elseif($LeadUser->setting=='Undecided')
						{
							$ActinString=$ActinString.'<i class="fa fa-exclamation actions_icon" title="Undecided" style="color:#1b42d1" onClick="LegalLeadSetting('.$NormalId.');"></i> ';
						}
						elseif($LeadUser->setting=='Appointment')
						{
							if($LeadUser->appointment_status == '')
							{
								$ActinString=$ActinString.'<i class="fa fa-cogs text-primary" title="Setting"></i> ';
								$ActinString=$ActinString.'<i class="fa fa-calendar-o actions_icon" title="Appointment @ '.date('m-d-Y',strtotime($LeadUser->appointment_date)).', '.$LeadUser->appointment_time.'" style="color:#c6192e" onClick="AppointmentLeadSetting('.$NormalId.',\'\',\'\',\'\');"></i> ';
							}
							else
							{
								$ActinString=$ActinString.'<i class="fa fa-cogs text-primary actions_icon" onClick="LegalLeadSetting('.$NormalId.');" title="Setting"></i> ';
								$ActinString=$ActinString.'<i class="fa fa-calendar-o actions_icon" title="Appointment '.$LeadUser->appointment_status.' @ '.date('m-d-Y',strtotime($LeadUser->appointment_date)).', '.$LeadUser->appointment_time.'" style="color:#c6192e" onClick="AppointmentLeadSetting('.$NormalId.',\''.$LeadUser->appointment_status.'\',\''.date('m-d-Y',strtotime($LeadUser->appointment_date)).'\',\''.$LeadUser->appointment_time.'\');"></i> ';
							}
						}
					}
				}
				else
				{
					$ActinString=$ActinString.'<i class="fa fa-usd text-primary actions_icon" onClick="Default('.$NormalId.');" title="Other Category"></i> ';
				}
				
				if(Session::get('user_category')!='user')
				{
					if($LeadUser->manager_id==0)
					{
						$user['User'] = '';
						$ActinString=$ActinString.'<i class="fa fa-user-plus text-primary actions_icon" onClick="UserLeadAllot('.$NormalId.');" title="Allot Lead to User"></i> ';
					}
					else
					{
						$user['User'] = $LeadUser->manager_name;
						$ActinString=$ActinString.'<i class="fa fa-user-plus text-success actions_icon" onClick="UserLeadReAllot('.$NormalId.','.$LeadUser->manager_id.');" title="Allot Lead to another User"></i> ';
					}
				}
				
				$ActinString=$ActinString.'<i class="fa fa-thumbs-o-down text-primary actions_icon" onclick="DisputeLead('.$LeadUser->client_ads_lead_user_id.');" title="Dispute"></i> ';
				
				if($LeadUser->resync == 'Yes')
				{
					$ActinString = $ActinString.'<i class="fa fa-fw fa-refresh text-success actions_icon" onClick="ReSync('.$NormalId.');" title="Re-sync"></i> ';
				}
				else
				{
					$ActinString = $ActinString.'<i class="fa fa-fw fa-refresh text-danger actions_icon" onClick="ReSync('.$NormalId.');" title="Re-sync"></i> ';
				}
				
				$user['Action']=$ActinString;
				$rows[]=$user;
			}		
			
			$data['total'] = $LeadUsers1;
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
		
		public function postDisputeLead(Request $request)
		{			
			$LeadUser=DB::table('users')
			->leftJoin('facebook_ads_lead_users', 'users.id', '=', 'facebook_ads_lead_users.created_user_id')
			->leftJoin('client_ads_lead_users', 'facebook_ads_lead_users.id', '=', 'client_ads_lead_users.facebook_ads_lead_user_id')
			->select('users.id', 'users.name', 'users.email', 'facebook_ads_lead_users.id as facebook_ads_lead_user_id', 'facebook_ads_lead_users.facebook_account_id', 'facebook_ads_lead_users.facebook_page_id', 'facebook_ads_lead_users.facebook_ads_lead_id', 'facebook_ads_lead_users.ad_name', 'facebook_ads_lead_users.adset_name', 'facebook_ads_lead_users.campaign_name', 'facebook_ads_lead_users.first_name', 'facebook_ads_lead_users.last_name', 'facebook_ads_lead_users.full_name', 'facebook_ads_lead_users.email as leadEmail', 'facebook_ads_lead_users.phone_number', 'facebook_ads_lead_users.city')
			->where('client_ads_lead_users.id',$request->client_ads_lead_user_id)
			->first();
			
			$userId=$LeadUser->id;
			$userName=$LeadUser->name;
			$userEmail=$LeadUser->email;
			
			$facebook_account_id=$LeadUser->facebook_account_id;
			$facebook_page_id=$LeadUser->facebook_page_id;
			$facebook_ads_lead_id=$LeadUser->facebook_ads_lead_id;
			$facebook_ads_lead_user_id=$LeadUser->facebook_ads_lead_user_id;
			$leadAd=$LeadUser->ad_name;
			$leadAdset=$LeadUser->adset_name;
			$leadCampaign=$LeadUser->campaign_name;
			if($LeadUser->full_name=='' || $LeadUser->full_name==NULL)
			{
				$leadName=$LeadUser->first_name.' '.$LeadUser->last_name;
			}
			else
			{
				$leadName=$LeadUser->full_name;
			}
			$leadPhone=$LeadUser->phone_number;
			$leadEmail=$LeadUser->leadEmail;
			$leadCity=$LeadUser->city;
			
			if($request->optradio=='Dispute')
			{
				$Dispute=implode(',',$request->Dispute);
				
				$dispute_lead_id=DB::table('dispute_leads')->insertGetId([
				'client_id' => Session::get('client_id'),
				'client_ads_lead_user_id' => $request->client_ads_lead_user_id,
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
				'select_option' => $request->optradio,
				'option_value' => $Dispute,
				'comment' => $request->dispute_comment,
				'created_user_id'=>Session::get('user_id'),
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_user_id'=>Session::get('user_id'),
				'updated_at'=>date('Y-m-d H:i:s')
				]);
				
				DB::table('client_ads_lead_users')->where('id', $request->client_ads_lead_user_id)
				->update(['dispute_lead_id' => $dispute_lead_id, 'remark' => $request->optradio, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
				
				//
				DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
				->update(['client_id' => 0, 'client_ads_lead_user_id' => 0, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 2]);
				
				$data = array(
				'userName' => $userName,
				'optradio' => $request->optradio,
				'Dispute' => $Dispute,
				'leadAd' => $leadAd,
				'leadAdset' => $leadAdset,
				'leadCampaign' => $leadCampaign,
				'leadName' => $leadName,
				'leadEmail' => $leadEmail,
				'leadPhone' => Helper::emailPhoneFormat($leadPhone),
				'leadCity' => $leadCity
				);
				
				Mail::send('emails.dispute_lead1', $data, function ($message) use ($userEmail) {
					$message->from('noreply@pownder.com', 'Pownder');
					$message->to($userEmail)->subject('Pownder™ Dispute Lead');
				});
			}
			else
			{
				if(count($request->year_between)>0)
				{
					$year_between=implode(',',$request->year_between);
				}
				else
				{
					$year_between='';
				}
				$dispute_lead_id=DB::table('dispute_leads')->insertGetId([
				'client_id' => Session::get('client_id'),
				'client_ads_lead_user_id' => $request->client_ads_lead_user_id,
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
				'select_option' => $request->optradio,
				'option_value' => $request->AnotherMake,
				'year_between' => $year_between,
				'group_sub_category_id' => $request->dispute_group_sub_category_id,
				'model' => $request->disputeModel,
				'comment' => $request->dispute_comment,
				'created_user_id'=>Session::get('user_id'),
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_user_id'=>Session::get('user_id'),
				'updated_at'=>date('Y-m-d H:i:s')
				]);
				
				DB::table('client_ads_lead_users')->where('id', $request->client_ads_lead_user_id)
				->update(['dispute_lead_id' => $dispute_lead_id, 'remark' => $request->optradio, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
				
				$leadAssign=0;
				$BrandClients = DB::table('clients')->where('group_sub_category_id', $request->dispute_group_sub_category_id)->where('created_user_id', $userId)->where('is_deleted',0)->get();
				if(count($BrandClients)>0)
				{
					foreach($BrandClients as $BrandClient)
					{
						if($leadAssign==0)
						{
							$client_id = $BrandClient->id;
							$client_latitude = (float)$BrandClient->latitude;
							$client_longitude = (float)$BrandClient->longitude;
							$package_id = $BrandClient->package_id;
							
							$Clients = DB::table('clients')->where('id', $client_id)->where('group_sub_category_id', $request->dispute_group_sub_category_id)->where('facebook_ads_lead_id', 'like', '%'.$facebook_ads_lead_id.'%')->where('created_user_id', $userId)->where('is_deleted',0)->get();
							
							$GroupLeadForm = array();
							$ClientGroups = DB::table('client_groups')->select('group_id')->where('client_id', $client_id)->where('created_user_id', $userId)->where('is_deleted', 0)->get();
							foreach($ClientGroups as $ClientGroup)
							{
								$group_id = $ClientGroup->group_id;
								$Groups = DB::table('groups')->select('facebook_ads_lead_id')->where('id', $group_id)->where('facebook_ads_lead_id', 'like', '%'.$facebook_ads_lead_id.'%')->where('created_user_id', $userId)->where('is_deleted', 0)->get();
								foreach($Groups as $Group)
								{
									$LeadForm1=explode(',',$Group->facebook_ads_lead_id);
									$GroupLeadForm=array_unique(array_merge($GroupLeadForm,$LeadForm1));
								}
							}
							
							if(count($GroupLeadForm)>0 || count($Clients)>0)
							{
								$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
								'user_id' => $userId,
								'client_id' => $client_id, 
								'package_id' => $package_id,
								'facebook_account_id' => $facebook_account_id,
								'facebook_page_id' => $facebook_page_id,
								'facebook_ads_lead_id' => $facebook_ads_lead_id,
								'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
								'remark' => $request->optradio,
								'created_user_id' => Session::get('user_id'),
								'created_at' => date('Y-m-d H:i:s'),
								'updated_user_id' => Session::get('user_id'),
								'updated_at' => date('Y-m-d H:i:s')
								]);
								
								DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
								->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
								
								$leadAssign=1;
								
								break;
							}
						}
					}
				}
				
				if($leadAssign==0)
				{
					DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
					->update(['client_id' => 0, 'client_ads_lead_user_id' => 0, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 2]);
					
					$data = array(
					'userName' => $userName,
					'optradio' => $request->optradio,
					'AnotherMake' => $request->AnotherMake,
					'GroupSubCategoryName' => Helper::GroupSubCategoryName($request->dispute_group_sub_category_id),
					'disputeModel' => $request->disputeModel,
					'leadAd' => $leadAd,
					'leadAdset' => $leadAdset,
					'leadCampaign' => $leadCampaign,
					'leadName' => $leadName,
					'leadEmail' => $leadEmail,
					'leadPhone' => Helper::emailPhoneFormat($leadPhone),
					'leadCity' => $leadCity
					);
					
					Mail::send('emails.dispute_lead', $data, function ($message) use ($userEmail) {
						$message->from('noreply@pownder.com', 'Pownder');
						$message->to($userEmail)->subject('Pownder™ Dispute Lead');
					}); 
				}
			}
			
			return redirect('cleads')->with('alert_success','Dispute lead added successfully');
		}
		
		public function postLeadReAssign(Request $request)
		{			
			$facebook_ads_lead_user_id = $request->ReAssignLeadUserId;
			$client_id = $request->ReAssignClientId;
			
			$LeadUser=DB::table('facebook_ads_lead_users')->where('id',$facebook_ads_lead_user_id)->first();
			$facebook_account_id = $LeadUser->facebook_account_id;
			$facebook_page_id = $LeadUser->facebook_page_id;
			$facebook_ads_lead_id = $LeadUser->facebook_ads_lead_id;
			$user_id = $LeadUser->created_user_id;
			
			$lead_client_ads_lead_user_id = $LeadUser->client_ads_lead_user_id;
			if($lead_client_ads_lead_user_id > 0)
			{
				$dispute_lead_id = DB::table('dispute_leads')->insertGetId([
				'client_ads_lead_user_id' => $lead_client_ads_lead_user_id,
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
				'select_option' => Session::get('user_category'),
				'option_value' => 'Re-assign',
				'created_user_id' => Session::get('user_id'),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_user_id' => Session::get('user_id'),
				'updated_at' => date('Y-m-d H:i:s')
				]);
				
				DB::table('client_ads_lead_users')->where('id', $lead_client_ads_lead_user_id)
				->update(['dispute_lead_id' => $dispute_lead_id, 'remark' => 'Re-assign', 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			}
			
			$client = DB::table('clients')->where('id',$client_id)->first();
			$package_id = $client->package_id;
			
			$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
			'user_id' => $user_id,
			'client_id' => $client_id, 
			'package_id' => $package_id,
			'facebook_account_id' => $facebook_account_id,
			'facebook_page_id' => $facebook_page_id,
			'facebook_ads_lead_id' => $facebook_ads_lead_id,
			'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
			'remark' => 'Re-assign',
			'created_user_id' => Session::get('user_id'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_user_id' => Session::get('user_id'),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
			->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id, 'updated_user_id' => Session::get('user_id'), 	'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 0]);
			
			return redirect('leads')->with('alert_success','Lead re-assign successfully');
		}
		
		public function postUserLeadAllot(Request $request)
		{			
			$facebook_ads_lead_user_id = $request->AllotUserLeadId;
			$manager_id = $request->manager_id;
			
			DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
			->update(['manager_id' => $manager_id, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
			
			DB::table('user_allotted_leads')->insert([
			"manager_id" => $manager_id,
			"facebook_ads_lead_user_id" => $facebook_ads_lead_user_id,
			"created_user_id" => Session::get('user_id'),
			"created_user_ip" => $request->ip(),
			"created_at" => date('Y-m-d H:i:s'),
			"updated_user_id" => Session::get('user_id'),
			"updated_user_ip" => $request->ip(),
			"updated_at" => date('Y-m-d H:i:s')
			]);
			
			if($request->Action=='Allot')
			{
				return redirect('leads')->with('alert_success','Lead allotted to user successfully');
			}
			else
			{
				return redirect('leads')->with('alert_success','Lead re-allotted to another user successfully');
			}	
		}
		
		public function postClientUserLeadAllot(Request $request)
		{			
			$facebook_ads_lead_user_id = $request->AllotUserLeadId;
			$manager_id = $request->manager_id;
			
			DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
			->update(['manager_id' => $manager_id, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
			
			DB::table('user_allotted_leads')->insert([
			"manager_id" => $manager_id,
			"facebook_ads_lead_user_id" => $facebook_ads_lead_user_id,
			"created_user_id" => Session::get('user_id'),
			"created_user_ip" => $request->ip(),
			"created_at" => date('Y-m-d H:i:s'),
			"updated_user_id" => Session::get('user_id'),
			"updated_user_ip" => $request->ip(),
			"updated_at" => date('Y-m-d H:i:s')
			]);
			
			if($request->Action=='Allot')
			{
				return redirect('cleads')->with('alert_success','Lead allotted to user successfully');
			}
			else
			{
				return redirect('cleads')->with('alert_success','Lead re-allotted to another user successfully');
			}	
		}
		
		public function getClientName(Request $request)
		{		
			$Client = DB::table('clients')->select('name')->where('id', $request->client_id)->first();
			if(is_null($Client))
			{
				$name = '';
			}
			else
			{
				$name = $Client->name;
			}
			
			return $name;
		}
		
		public function getClientROIStatus(Request $request)
		{		
			$Result = DB::table('clients')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->select('clients.roi_automation')
			->where('facebook_ads_lead_users.id', $request->facebook_ads_lead_user_id)
			->first();
			if(is_null($Result))
			{
				return '';
			}
			else
			{
				return $Result->roi_automation;
			}
		}
		
		public function postLeadSold(Request $request)
		{		
			$facebook_ads_lead_user_id=$request->facebook_ads_lead_user_id;
			$Result = DB::table('clients')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
			->select('packages.amount as package_amount', 'packages.per_lead_value', 'packages.package_type', 'clients.id as client_id', 'clients.package_id', 'clients.per_sold_value', 'clients.discount as client_discount', 'facebook_ads_lead_users.first_name', 'facebook_ads_lead_users.last_name', 'facebook_ads_lead_users.full_name', 'facebook_ads_lead_users.email', 'facebook_ads_lead_users.phone_number')
			->where('facebook_ads_lead_users.id', $facebook_ads_lead_user_id)
			->first();
			
			$email=$Result->email;
			$phone_number=$Result->phone_number;
			$client_id=$Result->client_id;
			$package_id=$Result->package_id;
			$package_type=$Result->package_type;
			$per_lead_value=$Result->per_lead_value;
			$per_sold_value=$Result->per_sold_value;
			$package_amount=$Result->package_amount;
			if($Result->full_name!='')
			{
				$facebook_ads_lead_user_name=$Result->full_name;
			}
			else
			{
				$facebook_ads_lead_user_name=$Result->first_name.' '.$Result->last_name;
			}
			$package_amount=$Result->package_amount;
			$client_discount=$Result->client_discount;
			$sold_date=explode('-',$request->sold_date);
			
			if($request->roi_automation=='No')
			{
				$cost=$package_amount-$client_discount;
				
				if($cost==0)
				{
					$roi_percentage=0;
				}
				else
				{
					$roi_percentage=($request->total_profit/$cost)*100;
				}
				
				DB::table('sold_leads')->insert([
				'category' => 'Sold',
				'client_id' => $client_id, 
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 
				'facebook_ads_lead_user_name' => $facebook_ads_lead_user_name, 
				'real_buyer_name' => $request->real_buyer_name, 
				'sold_date' => $sold_date[2].'-'.$sold_date[0].'-'.$sold_date[1], 
				'sold_type' => 'Manual',
				'stock' => $request->stock,
				'sold_year' => $request->sold_year, 
				'group_sub_category_id' => $request->group_sub_category_id,
				'model' => $request->model,
				'roi_automation' => $request->roi_automation,
				'roi_percentage' => $roi_percentage, 
				'package_id' => $package_id,
				'package_type' => $package_type,
				'per_lead_value' => $per_lead_value,
				'per_sold_value' => $per_sold_value,
				'package_amount' => $package_amount,
				'client_discount' => $client_discount,
				'email' => $email,
				'phone_number' => $phone_number,
				'sold_type' => 'Manual',
				'total_profit' => $request->total_profit,
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			else
			{
				DB::table('sold_leads')->insert([
				'category' => 'Sold',
				'client_id' => $client_id, 
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 
				'facebook_ads_lead_user_name' => $facebook_ads_lead_user_name, 
				'real_buyer_name' => $request->real_buyer_name, 
				'sold_date' => $sold_date[2].'-'.$sold_date[0].'-'.$sold_date[1],
				'sold_type' => 'Manual',
				'stock' => $request->stock,
				'sold_year' => $request->sold_year, 
				'group_sub_category_id' => $request->group_sub_category_id,
				'model' => $request->model,
				'roi_automation' => $request->roi_automation, 
				'package_id' => $package_id,
				'package_type' => $package_type,
				'per_lead_value' => $per_lead_value,
				'per_sold_value' => $per_sold_value,
				'package_amount' => $package_amount,
				'client_discount' => $client_discount,
				'email' => $email,
				'phone_number' => $phone_number,
				'sold_type' => 'Manual',
				'total_profit' => $request->profit_total,
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			return redirect('leads')->with('alert_success','Lead sold added successfully');
		}
		
		///////////////////
		public function postLegalLead(Request $request)
		{		
			$facebook_ads_lead_user_id=$request->legal_facebook_ads_lead_user_id;
			$Result = DB::table('clients')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
			->select('packages.amount as package_amount', 'packages.per_lead_value', 'packages.package_type', 'clients.id as client_id', 'clients.package_id', 'clients.per_sold_value', 'clients.discount as client_discount', 'facebook_ads_lead_users.first_name', 'facebook_ads_lead_users.last_name', 'facebook_ads_lead_users.full_name', 'facebook_ads_lead_users.email', 'facebook_ads_lead_users.phone_number')
			->where('facebook_ads_lead_users.id', $facebook_ads_lead_user_id)
			->first();
			
			$email=$Result->email;
			$phone_number=$Result->phone_number;
			$client_id=$Result->client_id;
			$package_id=$Result->package_id;
			$package_type=$Result->package_type;
			$per_lead_value=$Result->per_lead_value;
			$per_sold_value=$Result->per_sold_value;
			$package_amount=$Result->package_amount;
			if($Result->full_name!='')
			{
				$facebook_ads_lead_user_name=$Result->full_name;
			}
			else
			{
				$facebook_ads_lead_user_name=$Result->first_name.' '.$Result->last_name;
			}
			$package_amount=$Result->package_amount;
			$client_discount=$Result->client_discount;
			$sold_date=explode('-',$request->case_date);
			
			if($request->legal_client_roi_automation=='No')
			{
				$cost=$package_amount-$client_discount;
				
				if($cost==0)
				{
					$roi_percentage=0;
				}
				else
				{
					$roi_percentage=($request->legal_total/$cost)*100;
				}
				
				DB::table('sold_leads')->insert([
				'category' => 'Legal',
				'client_id' => $client_id, 
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 
				'facebook_ads_lead_user_name' => $facebook_ads_lead_user_name, 
				'sold_date' => $sold_date[2].'-'.$sold_date[0].'-'.$sold_date[1],
				'sold_type' => 'Manual',
				'stock' => $request->case_number,
				'group_sub_category_id' => $request->legal_group_sub_category_id,
				'roi_automation' => $request->legal_client_roi_automation,
				'roi_percentage' => $roi_percentage, 
				'package_id' => $package_id,
				'package_type' => $package_type,
				'per_lead_value' => $per_lead_value,
				'per_sold_value' => $per_sold_value,
				'package_amount' => $package_amount,
				'client_discount' => $client_discount,
				'down_payment' => $request->down_payment,
				'total_profit' => $request->legal_total,
				'email' => $email,
				'phone_number' => $phone_number,
				'sold_type' => 'Manual',
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			else
			{
				DB::table('sold_leads')->insert([
				'category' => 'Legal',
				'client_id' => $client_id, 
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 
				'facebook_ads_lead_user_name' => $facebook_ads_lead_user_name, 
				'sold_date' => $sold_date[2].'-'.$sold_date[0].'-'.$sold_date[1],
				'sold_type' => 'Manual',
				'stock' => $request->case_number,
				'group_sub_category_id' => $request->legal_group_sub_category_id,
				'roi_automation' => $request->legal_client_roi_automation,
				'package_id' => $package_id,
				'package_type' => $package_type,
				'per_lead_value' => $per_lead_value,
				'per_sold_value' => $per_sold_value,
				'package_amount' => $package_amount,
				'client_discount' => $client_discount,
				'down_payment' => $request->down_payment,
				'total_profit' => $request->legal_total,
				'email' => $email,
				'phone_number' => $phone_number,
				'sold_type' => 'Manual',
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			if(Session::get('user_category')=='client')
			{
				return redirect('cleads')->with('alert_success','Lead legal added successfully');
			}
			else
			{
				return redirect('leads')->with('alert_success','Lead legal added successfully');
			}
		}
		
		public function postDefaultLead(Request $request)
		{		
			$facebook_ads_lead_user_id=$request->default_facebook_ads_lead_user_id;
			$Result = DB::table('clients')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
			->select('packages.amount as package_amount', 'packages.per_lead_value', 'packages.package_type', 'clients.id as client_id', 'clients.package_id', 'clients.per_sold_value', 'clients.discount as client_discount', 'facebook_ads_lead_users.first_name', 'facebook_ads_lead_users.last_name', 'facebook_ads_lead_users.full_name', 'facebook_ads_lead_users.email', 'facebook_ads_lead_users.phone_number')
			->where('facebook_ads_lead_users.id', $facebook_ads_lead_user_id)
			->first();
			
			$email=$Result->email;
			$phone_number=$Result->phone_number;
			$client_id=$Result->client_id;
			$package_id=$Result->package_id;
			$package_type=$Result->package_type;
			$per_lead_value=$Result->per_lead_value;
			$per_sold_value=$Result->per_sold_value;
			$package_amount=$Result->package_amount;
			if($Result->full_name!='')
			{
				$facebook_ads_lead_user_name=$Result->full_name;
			}
			else
			{
				$facebook_ads_lead_user_name=$Result->first_name.' '.$Result->last_name;
			}
			$package_amount=$Result->package_amount;
			$client_discount=$Result->client_discount;
			$sold_date=explode('-',$request->conversion_date);
			
			if($request->legal_client_roi_automation=='No')
			{
				$cost=$package_amount-$client_discount;
				
				if($cost==0)
				{
					$roi_percentage=0;
				}
				else
				{
					$roi_percentage=($request->legal_total/$cost)*100;
				}
				
				DB::table('sold_leads')->insert([
				'category' => 'Other',
				'client_id' => $client_id, 
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 
				'facebook_ads_lead_user_name' => $facebook_ads_lead_user_name, 
				'sold_date' => $sold_date[2].'-'.$sold_date[0].'-'.$sold_date[1], 
				'sold_type' => 'Manual',
				'roi_automation' => $request->default_client_roi_automation,
				'roi_percentage' => $roi_percentage, 
				'package_id' => $package_id,
				'package_type' => $package_type,
				'per_lead_value' => $per_lead_value,
				'per_sold_value' => $per_sold_value,
				'package_amount' => $package_amount,
				'client_discount' => $client_discount,
				'total_profit' => $request->default_total_profit,
				'email' => $email,
				'phone_number' => $phone_number,
				'sold_type' => 'Manual',
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			else
			{
				DB::table('sold_leads')->insert([
				'category' => 'Other',
				'client_id' => $client_id, 
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 
				'facebook_ads_lead_user_name' => $facebook_ads_lead_user_name, 
				'sold_date' => $sold_date[2].'-'.$sold_date[0].'-'.$sold_date[1], 
				'sold_type' => 'Manual',
				'roi_automation' => $request->default_client_roi_automation,
				'package_id' => $package_id,
				'package_type' => $package_type,
				'per_lead_value' => $per_lead_value,
				'per_sold_value' => $per_sold_value,
				'package_amount' => $package_amount,
				'client_discount' => $client_discount,
				'total_profit' => $request->default_total_profit,
				'email' => $email,
				'phone_number' => $phone_number,
				'sold_type' => 'Manual',
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			return redirect('leads')->with('alert_success','Lead detail added successfully');
		}
		///////////////////
		
		public function postClientLeadSold(Request $request)
		{		
			$facebook_ads_lead_user_id = $request->sold_facebook_ads_lead_user_id;
			$Result = DB::table('clients')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
			->select('packages.amount as package_amount', 'packages.per_lead_value', 'packages.package_type', 'clients.id as client_id', 'clients.package_id', 'clients.per_sold_value', 'clients.discount as client_discount', 'facebook_ads_lead_users.first_name', 'facebook_ads_lead_users.last_name', 'facebook_ads_lead_users.full_name', 'facebook_ads_lead_users.email', 'facebook_ads_lead_users.phone_number')
			->where('facebook_ads_lead_users.id', $facebook_ads_lead_user_id)
			->first();
			
			$email=$Result->email;
			$phone_number=$Result->phone_number;
			$client_id=$Result->client_id;
			$package_id=$Result->package_id;
			$package_type=$Result->package_type;
			$per_lead_value=$Result->per_lead_value;
			$per_sold_value=$Result->per_sold_value;
			$package_amount=$Result->package_amount;
			if($Result->full_name!='')
			{
				$facebook_ads_lead_user_name=$Result->full_name;
			}
			else
			{
				$facebook_ads_lead_user_name=$Result->first_name.' '.$Result->last_name;
			}
			$package_amount=$Result->package_amount;
			$client_discount=$Result->client_discount;
			$sold_date=explode('-',$request->sold_date);
			
			if($request->roi_automation=='No')
			{
				$cost=$package_amount-$client_discount;
				
				if($cost==0)
				{
					$roi_percentage=0;
				}
				else
				{
					$roi_percentage=($request->total_profit/$cost)*100;
				}
				
				DB::table('sold_leads')->insert([
				'category' => 'Sold',
				'client_id' => $client_id, 
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 
				'facebook_ads_lead_user_name' => $facebook_ads_lead_user_name, 
				'real_buyer_name' => $request->real_buyer_name, 
				'sold_date' => $sold_date[2].'-'.$sold_date[0].'-'.$sold_date[1], 
				'sold_type' => 'Manual',
				'stock' => $request->stock,
				'sold_year' => $request->sold_year, 
				'group_sub_category_id' => $request->group_sub_category_id,
				'model' => $request->model,
				'roi_automation' => $request->roi_automation,
				'roi_percentage' => $roi_percentage, 
				'package_id' => $package_id,
				'package_type' => $package_type,
				'per_lead_value' => $per_lead_value,
				'per_sold_value' => $per_sold_value,
				'package_amount' => $package_amount,
				'client_discount' => $client_discount,
				'email' => $email,
				'phone_number' => $phone_number,
				'sold_type' => 'Manual',
				'total_profit' => $request->total_profit,
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			else
			{
				DB::table('sold_leads')->insert([
				'category' => 'Sold',
				'client_id' => $client_id, 
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 
				'facebook_ads_lead_user_name' => $facebook_ads_lead_user_name, 
				'real_buyer_name' => $request->real_buyer_name, 
				'sold_date' => $sold_date[2].'-'.$sold_date[0].'-'.$sold_date[1], 
				'sold_type' => 'Manual',
				'stock' => $request->stock,
				'sold_year' => $request->sold_year, 
				'group_sub_category_id' => $request->group_sub_category_id,
				'model' => $request->model,
				'roi_automation' => $request->roi_automation, 
				'package_id' => $package_id,
				'package_type' => $package_type,
				'per_lead_value' => $per_lead_value,
				'per_sold_value' => $per_sold_value,
				'package_amount' => $package_amount,
				'client_discount' => $client_discount,
				'email' => $email,
				'phone_number' => $phone_number,
				'sold_type' => 'Manual',
				'total_profit' => $request->profit_total,
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			return redirect('cleads')->with('alert_success','Lead sold added successfully');
		}
		
		///////////////////
		public function postClientDefaultLead(Request $request)
		{		
			$facebook_ads_lead_user_id=$request->default_facebook_ads_lead_user_id;
			$Result = DB::table('clients')
			->leftJoin('facebook_ads_lead_users', 'clients.id', '=', 'facebook_ads_lead_users.client_id')
			->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
			->select('packages.amount as package_amount', 'packages.per_lead_value', 'packages.package_type', 'clients.id as client_id', 'clients.package_id', 'clients.per_sold_value', 'clients.discount as client_discount', 'facebook_ads_lead_users.first_name', 'facebook_ads_lead_users.last_name', 'facebook_ads_lead_users.full_name', 'facebook_ads_lead_users.email', 'facebook_ads_lead_users.phone_number')
			->where('facebook_ads_lead_users.id', $facebook_ads_lead_user_id)
			->first();
			
			$email=$Result->email;
			$phone_number=$Result->phone_number;
			$client_id=$Result->client_id;
			$package_id=$Result->package_id;
			$package_type=$Result->package_type;
			$per_lead_value=$Result->per_lead_value;
			$per_sold_value=$Result->per_sold_value;
			$package_amount=$Result->package_amount;
			if($Result->full_name!='')
			{
				$facebook_ads_lead_user_name=$Result->full_name;
			}
			else
			{
				$facebook_ads_lead_user_name=$Result->first_name.' '.$Result->last_name;
			}
			$package_amount=$Result->package_amount;
			$client_discount=$Result->client_discount;
			$sold_date=explode('-',$request->conversion_date);
			
			if($request->legal_client_roi_automation=='No')
			{
				$cost=$package_amount-$client_discount;
				
				if($cost==0)
				{
					$roi_percentage=0;
				}
				else
				{
					$roi_percentage=($request->legal_total/$cost)*100;
				}
				
				DB::table('sold_leads')->insert([
				'category' => 'Other',
				'client_id' => $client_id, 
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 
				'facebook_ads_lead_user_name' => $facebook_ads_lead_user_name, 
				'sold_date' => $sold_date[2].'-'.$sold_date[0].'-'.$sold_date[1], 
				'sold_type' => 'Manual',
				'roi_automation' => $request->default_client_roi_automation,
				'roi_percentage' => $roi_percentage, 
				'package_id' => $package_id,
				'package_type' => $package_type,
				'per_lead_value' => $per_lead_value,
				'per_sold_value' => $per_sold_value,
				'package_amount' => $package_amount,
				'client_discount' => $client_discount,
				'total_profit' => $request->default_total_profit,
				'email' => $email,
				'phone_number' => $phone_number,
				'sold_type' => 'Manual',
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			else
			{
				DB::table('sold_leads')->insert([
				'category' => 'Other',
				'client_id' => $client_id, 
				'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 
				'facebook_ads_lead_user_name' => $facebook_ads_lead_user_name, 
				'sold_date' => $sold_date[2].'-'.$sold_date[0].'-'.$sold_date[1],
				'sold_type' => 'Manual',
				'roi_automation' => $request->default_client_roi_automation,
				'package_id' => $package_id,
				'package_type' => $package_type,
				'per_lead_value' => $per_lead_value,
				'per_sold_value' => $per_sold_value,
				'package_amount' => $package_amount,
				'client_discount' => $client_discount,
				'total_profit' => $request->default_total_profit,
				'email' => $email,
				'phone_number' => $phone_number,
				'sold_type' => 'Manual',
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
			return redirect('cleads')->with('alert_success','Lead detail added successfully');
		}
		
		function postLeadSetting(Request $request)
		{
			$facebook_ads_lead_user_id = $request->setting_facebook_ads_lead_user_id;
			
			if($request->lead_setting=='Appointment')
			{
				$app_date=explode('-',$request->appointment_date);
				$appointment_date=$app_date[2].'-'.$app_date[0].'-'.$app_date[1];
				
				DB::table('facebook_ads_lead_user_settings')->insert([
				'facebook_ads_lead_user_id' => $request->setting_facebook_ads_lead_user_id, 
				'setting' => $request->lead_setting, 
				'appointment_date' => $appointment_date,
				'appointment_time' => $request->appointment_time,
				'appointment_cal' => 'No',
				'borderColor' => '#4FC1E9', 
				'backgroundColor' => '#4FC1E9',
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
				
				DB::table('facebook_ads_lead_users')->where('id', $request->setting_facebook_ads_lead_user_id)
				->update(['setting' => $request->lead_setting, 'appointment_date' => $appointment_date, 'appointment_time' => $request->appointment_time, 'borderColor' => '#4FC1E9', 'backgroundColor' => '#4FC1E9', 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
				
				$leadUser = DB::table('facebook_ads_lead_users')->where('id',$request->setting_facebook_ads_lead_user_id)->first();
				$facebook_page_id = $leadUser->facebook_page_id;
				$lead_user_id = $leadUser->created_user_id;
				$client_id = $leadUser->client_id;
				$manager_id = $leadUser->manager_id;
				
				if($leadUser->full_name != '')
				{
					$leadName = $leadUser->full_name;
				}
				else
				{
					$leadName = $leadUser->first_name.' '.$leadUser->last_name;
				}
				
				$leadType = $leadUser->lead_type;
				$leadEmail = $leadUser->email;
				$leadPhone = $leadUser->phone_number;
				$leadCity = $leadUser->city;
				
				$ManagerArray = array();
				
				$leadAddedBy = '';
				$User = DB::table('users')->where('id', $lead_user_id)->first();
				if(($leadType == 'Pownder™ Call' || $leadType == 'Pownder™ Messenger') && $facebook_page_id == 0)
				{
					$leadAddedBy = $User->name.' ('.title_case($User->category).')';
				}
				
				$manager_name = '';
				$LeadManager = DB::table('managers')->where('id', $manager_id)->first();
				if(!is_null($LeadManager))
				{
					$manager_name = title_case($LeadManager->full_name);
				}
				
				$Client = DB::table('users')->where('client_id', $client_id)->first();
				
				$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
				foreach($CampaignClients as $CampaignClient)
				{
					if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only Appointments'))
					{
						$data = array('name' => $CampaignClient->client_contact, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'Client', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
						
						Mail::send('emails.appointment', $data, function ($message) use ($CampaignClient) {
							$message->from('noreply@pownder.com', 'Pownder');
							$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('Facebook Lead Appointment');
						});
					}
					
					if($CampaignClient->user_notification == 'No')
					{
						if($CampaignClient->manager_id != '')
						{
							$ManagerArray = explode(",", $CampaignClient->manager_id);
							$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
							foreach($Managers as $Manager)
							{
								$data = array('name' => $Manager->full_name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'User', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
								
								Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
									$message->from('noreply@pownder.com', 'Pownder');
									$message->to($Manager->email)->subject('Facebook Lead Appointment');
								});
							}
						}
					}
					else
					{
						$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
						if(!is_null($ManagerExists))
						{
							if($CampaignClient->notification == 'For Appointments' || $CampaignClient->notification == 'Both')
							{
								if($CampaignClient->manager_id != '')
								{
									$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
									
									$ManagerArray = explode(",", $CampaignClient->manager_id);
									
									$Array = array();
									$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
									foreach($queries as $query)
									{
										$Array[] = $query->id;
									}
									
									if(count($Array) > 0)
									{
										$index = $LeadCount % count($Array);
										
										$Manager = DB::table('managers')->where('id', $Array[$index])->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
										if(!is_null($Manager))
										{
											$data = array('name' => $Manager->full_name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'User', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
											
											Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
												$message->from('noreply@pownder.com', 'Pownder');
												$message->to($Manager->email)->subject('Facebook Lead Appointment');
											});
											
											DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
											->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
										}
									}
								}
							}
						}
					}
				}
				
				$Managers = DB::table('managers')->where('appointment_notifier', 'On')->where('client_id', $client_id)->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
				foreach($Managers as $Manager)
				{
					if (!in_array($Manager->id, $ManagerArray))
					{
						$manager_permission = DB::table('manager_permissions')->where('permission', 'All Leads')->where('manager_id', $Manager->id)->where('is_deleted', 0)->get();
						if(!is_null($manager_permission))
						{
							$ManagerArray[] = $Manager->id;
							
							$data = array('name' => $Manager->first_name.' '.$Manager->last_name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'User', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
							
							Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
								$message->from('noreply@pownder.com', 'Pownder');
								$message->to($Manager->email)->subject('Facebook Lead Appointment');
							});
						}
					}
				}
				
				if (!in_array($manager_id, $ManagerArray) && $manager_id > 0)
				{
					$Manager=DB::table('users')->where('manager_id',$manager_id)->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
					if(!is_null($Manager))
					{
						$data = array('name' => $Manager->name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => $Manager->category, 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity
						);
						
						Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
							$message->from('noreply@pownder.com', 'Pownder');
							$message->to($Manager->email)->subject('Facebook Lead Appointment');
						});
					}
				}
				
				$admin = DB::table('admins')->where('id', 1)->where('appointment_email', '!=', '')->where('appointment_option', 'Yes')->where('is_deleted', 0)->first();
				if(!is_null($admin))
				{
					if($admin->receive_notification == 'Everytime')
					{
						$data = array('name' => $admin->name, 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'category' => 'admin', 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
						
						Mail::send('emails.appointment', $data, function ($message) use ($admin) {
							$message->from('noreply@pownder.com', 'Pownder');
							$message->to($admin->appointment_email)->subject('Facebook Lead Appointment');
						});
					}
					else
					{
						DB::table('admin_emails')->insert(['facebook_ads_lead_user_id' => $request->setting_facebook_ads_lead_user_id, 'subject' => 'Facebook Lead Appointment', 'appointment_date' => $request->appointment_date, 'appointment_time' => $request->appointment_time, 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s')]);
					}
				}
			}
			else
			{
				DB::table('facebook_ads_lead_user_settings')->insert([
				'facebook_ads_lead_user_id' => $request->setting_facebook_ads_lead_user_id, 
				'setting' => $request->lead_setting, 
				'comment' => $request->setting_comment,
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
				
				DB::table('facebook_ads_lead_users')
				->where('id', $request->setting_facebook_ads_lead_user_id)
				->update(['setting' => $request->lead_setting, 'comment' => $request->setting_comment, 'updated_user_id' => Session::get('user_id'), 		'updated_at' => date('Y-m-d H:i:s')]);
			}
			
			return $request->lead_setting;
		}
		
		function postLegalLeadSetting(Request $request)
		{
			if($request->legal_lead_setting=='Appointment')
			{
				$app_date=explode('-',$request->legal_appointment_date);
				$appointment_date=$app_date[2].'-'.$app_date[0].'-'.$app_date[1];
				
				DB::table('facebook_ads_lead_user_settings')->insert([
				'facebook_ads_lead_user_id' => $request->legal_setting_facebook_ads_lead_user_id, 
				'setting' => $request->legal_lead_setting, 
				'appointment_date' => $appointment_date,
				'appointment_time' => $request->legal_appointment_time,
				'borderColor' => '#4FC1E9', 
				'backgroundColor' => '#4FC1E9',
				'appointment_cal' => 'No',
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
				
				DB::table('facebook_ads_lead_users')->where('id', $request->legal_setting_facebook_ads_lead_user_id)
				->update(['setting' => $request->legal_lead_setting, 'appointment_date' => $appointment_date, 'appointment_time' => $request->legal_appointment_time, 'borderColor' => '#4FC1E9', 'backgroundColor' => '#4FC1E9', 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
				
				$leadUser=DB::table('facebook_ads_lead_users')->where('id',$request->legal_setting_facebook_ads_lead_user_id)->first();
				$facebook_page_id=$leadUser->facebook_page_id;
				$lead_user_id=$leadUser->created_user_id;
				$client_id=$leadUser->client_id;
				$manager_id=$leadUser->manager_id;
				
				if($leadUser->full_name!='')
				{
					$leadName=$leadUser->full_name;
				}
				else
				{
					$leadName=$leadUser->first_name.' '.$leadUser->last_name;
				}
				
				$leadType=$leadUser->lead_type;
				$leadEmail=$leadUser->email;
				$leadPhone=$leadUser->phone_number;
				$leadCity=$leadUser->city;
				
				$ManagerArray = array();
				
				$leadAddedBy = '';
				$User=DB::table('users')->where('id',$lead_user_id)->first();
				if(($leadType == 'Pownder™ Call' || $leadType == 'Pownder™ Messenger') && $facebook_page_id == 0)
				{
					$leadAddedBy = $User->name.' ('.title_case($User->category).')';
				}
				
				$manager_name = '';
				$LeadManager = DB::table('managers')->where('id', $manager_id)->first();
				if(!is_null($LeadManager))
				{
					$manager_name = title_case($LeadManager->full_name);
				}
				
				$Client=DB::table('users')->where('client_id', $client_id)->first();
				
				$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
				foreach($CampaignClients as $CampaignClient)
				{
					if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only Appointments'))
					{
						$data = array('name' => $CampaignClient->client_contact, 'appointment_date' => $request->legal_appointment_date, 'appointment_time' => $request->legal_appointment_time, 'category' => 'Client', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
						
						Mail::send('emails.appointment', $data, function ($message) use ($CampaignClient) {
							$message->from('noreply@pownder.com', 'Pownder');
							$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('Facebook Lead Appointment');
						});
					}
					
					if($CampaignClient->user_notification == 'No')
					{
						if($CampaignClient->manager_id != '')
						{
							$ManagerArray = explode(",", $CampaignClient->manager_id);
							$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
							foreach($Managers as $Manager)
							{
								$data = array('name' => $Manager->full_name, 'appointment_date' => $request->legal_appointment_date, 'appointment_time' => $request->legal_appointment_time, 'category' => 'User', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
								
								Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
									$message->from('noreply@pownder.com', 'Pownder');
									$message->to($Manager->email)->subject('Facebook Lead Appointment');
								});
							}
						}
					}
					else
					{
						$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
						if(!is_null($ManagerExists))
						{
							if($CampaignClient->notification == 'For Appointments' || $CampaignClient->notification == 'Both')
							{
								if($CampaignClient->manager_id != '')
								{
									$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
									
									$ManagerArray = explode(",", $CampaignClient->manager_id);
									
									$Array = array();
									$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
									foreach($queries as $query)
									{
										$Array[] = $query->id;
									}
									
									if(count($Array) > 0)
									{
										$index = $LeadCount % count($Array);
										
										$Manager = DB::table('managers')->where('id', $Array[$index])->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
										if(!is_null($Manager))
										{
											$data = array('name' => $Manager->full_name, 'appointment_date' => $request->legal_appointment_date, 'appointment_time' => $request->legal_appointment_time, 'category' => 'User', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
											
											Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
												$message->from('noreply@pownder.com', 'Pownder');
												$message->to($Manager->email)->subject('Facebook Lead Appointment');
											});
											
											DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
											->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
										}
									}
								}
							}
						}
					}
				}
				
				$Managers = DB::table('managers')->where('appointment_notifier', 'On')->where('client_id', $client_id)->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
				foreach($Managers as $Manager)
				{
					if (!in_array($Manager->id, $ManagerArray))
					{
						$manager_permission = DB::table('manager_permissions')->where('permission', 'All Leads')->where('manager_id', $Manager->id)->where('is_deleted', 0)->get();
						if(!is_null($manager_permission))
						{
							$ManagerArray[] = $Manager->id;
							
							$data = array('name' => $Manager->full_name, 'appointment_date' => $request->legal_appointment_date, 'appointment_time' => $request->legal_appointment_time, 'category' => 'User', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
							
							Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
								$message->from('noreply@pownder.com', 'Pownder');
								$message->to($Manager->email)->subject('Facebook Lead Appointment');
							});
						}
					}
				}
				
				if (!in_array($manager_id, $ManagerArray)  && $manager_id > 0)
				{
					$Manager = DB::table('users')->where('manager_id', $manager_id)->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
					if(!is_null($Manager))
					{
						$data = array('name' => $Manager->name, 'appointment_date' => $request->legal_appointment_date, 'appointment_time' => $request->legal_appointment_time, 'category' => $Manager->category, 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
						
						Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
							$message->from('noreply@pownder.com', 'Pownder');
							$message->to($Manager->email)->subject('Facebook Lead Appointment');
						});
					}
				}
				
				$admin = DB::table('admins')->where('id', 1)->where('appointment_email', '!=', '')->where('appointment_option', 'Yes')->where('is_deleted', 0)->first();
				if(!is_null($admin))
				{
					if($admin->receive_notification == 'Everytime')
					{
						$data = array('name' => $admin->name, 'appointment_date' => $request->legal_appointment_date, 'appointment_time' => $request->legal_appointment_time, 'category' => 'admin', 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
						
						Mail::send('emails.appointment', $data, function ($message) use ($admin) {
							$message->from('noreply@pownder.com', 'Pownder');
							$message->to($admin->appointment_email)->subject('Facebook Lead Appointment');
						});
					}
					else
					{
						DB::table('admin_emails')->insert(['facebook_ads_lead_user_id' => $request->legal_setting_facebook_ads_lead_user_id, 'subject' => 'Facebook Lead Appointment', 'appointment_date' => $request->legal_appointment_date, 'appointment_time' => $request->legal_appointment_time, 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s')]);
					}
				}
			}
			else
			{
				DB::table('facebook_ads_lead_user_settings')->insert([
				'facebook_ads_lead_user_id' => $request->legal_setting_facebook_ads_lead_user_id, 
				'setting' => $request->legal_lead_setting, 
				'comment' => $request->legal_setting_comment,
				'created_user_id' => Session::get('user_id'), 
				'created_at' => date('Y-m-d H:i:s'), 
				'updated_user_id' => Session::get('user_id'), 
				'updated_at' => date('Y-m-d H:i:s')
				]);
				
				DB::table('facebook_ads_lead_users')->where('id', $request->legal_setting_facebook_ads_lead_user_id)
				->update(['setting' => $request->legal_lead_setting, 'comment' => $request->legal_setting_comment, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			
			return $request->legal_lead_setting;
		}
		
		function postLeadAdd(Request $request)
		{
			$rule=[
			'first_name' => 'required',
			'last_name' => 'required',
			'email' => 'email_array',
			'phone' => 'required|phone_format',
			'zip' => 'zip_code_valid',
			'lead_type' => 'required',
			'client_name' => 'required'			
			];
			
			$this->validate($request,$rule);
			
			$city=$request->city;
			$state=$request->state;
			
			if($request->zip!='')
			{
				$Address = Helper::get_zip_info($request->zip);
				if($city=='')
				{
					$city=$Address['city'];
				}
				
				if($state=='')
				{
					$state=$Address['state'];
				}
			}
			
			$latitude = '';
			$longitude = '';
			
			if($request->zip != '')
			{
				$address = str_replace(" ","+",$request->zip);
				$url = "http://maps.google.com/maps/api/geocode/json?address=$address";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$response = curl_exec($ch);
				curl_close($ch);
				
				$response_a = json_decode($response);
				if(isset($response_a->results))
				{
					if(count($response_a->results)>0)
					{
						$latitude = $response_a->results[0]->geometry->location->lat;
						$longitude = $response_a->results[0]->geometry->location->lng;
					}
				}
			}
			elseif($city != '')
			{
				$address = str_replace(" ","+",$city);
				$url = "http://maps.google.com/maps/api/geocode/json?address=$address";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$response = curl_exec($ch);
				curl_close($ch);
				
				$response_a = json_decode($response);
				if(isset($response_a->results))
				{
					if(count($response_a->results)>0)
					{
						$latitude = $response_a->results[0]->geometry->location->lat;
						$longitude = $response_a->results[0]->geometry->location->lng;
					}
				}
			}
			
			$client_id = $request->client_name;
			
			$Client = DB::table('clients')->where('id', $client_id)->first();
			
			$group_category_id = $Client->group_category_id;
			$client_name = $Client->name;
			
			$facebook_ads_lead_user_id = DB::table('facebook_ads_lead_users')->insertGetId([
			'lead_type' => $request->lead_type,
			'adset_name' => $request->lead_type,
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'full_name' => $request->first_name.' '.$request->last_name,
			'campaign_name' => $Client->name,
			'email' => $request->email,
			'phone_number' => $request->phone,
			'city' => $city,
			'state' => $state,
			'street_address' => $request->full_address,
			'post_code' => $request->zip,
			'latitude' => $latitude,
			'longitude' => $longitude,
			'client_id' => $client_id,
			'created_time' => date('Y-m-d H:i:s'),
			'created_user_id' => Session::get('user_id'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_user_id' => Session::get('user_id'),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
			'user_id' => $Client->created_user_id,
			'client_id' => $client_id, 
			'package_id' => $Client->package_id,
			'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
			'created_user_id' => Session::get('user_id'),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_user_id' => Session::get('user_id'),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
			->update(['client_ads_lead_user_id' => $client_ads_lead_user_id]);
			
			$ManagerArray = array();
			$phone = Helper::emailPhoneFormat($request->phone);
			$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
			foreach($CampaignClients as $CampaignClient)
			{
				if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
				{
					$data = array('name' => $CampaignClient->client_contact, 'category' => 'Client', 'client_name' => $client_name, 'lead_type' => $request->lead_type, 'UserName' => Session::get('user_name'), 'UserType' => Session::get('user_category'), 'first_name' => $request->first_name, 'last_name' => $request->last_name, 'email' => $request->email, 'phone' => $phone, 'city' => $city);
					
					Mail::send('emails.new_lead', $data, function ($message) use ($CampaignClient) {
						$message->from('noreply@pownder.com', 'Pownder');
						$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
					});
				}
				
				if($CampaignClient->user_notification == 'No')
				{
					if($CampaignClient->manager_id != '')
					{
						$ManagerArray = explode(",", $CampaignClient->manager_id);
						$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
						foreach($Managers as $Manager)
						{
							$data = array('name' => $Manager->full_name, 'category' => 'User', 'client_name' => $client_name, 'lead_type' => $request->lead_type, 'UserName' => Session::get('user_name'), 'UserType' => Session::get('user_category'), 'first_name' => $request->first_name, 'last_name' => $request->last_name, 'email' => $request->email, 'phone' => $phone, 'city' => $city);
							
							Mail::send('emails.new_lead', $data, function ($message) use ($Manager) {
								$message->from('noreply@pownder.com', 'Pownder');
								$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
							});
						}
					}
				}
				else
				{
					$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
					if(!is_null($ManagerExists))
					{
						if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
						{
							if($CampaignClient->manager_id != '')
							{
								$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
								
								$ManagerArray = explode(",", $CampaignClient->manager_id);
								
								$Array = array();
								$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
								foreach($queries as $query)
								{
									$Array[] = $query->id;
								}
								
								if(count($Array) > 0)
								{
									$index = $LeadCount % count($Array);
									
									$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
									if(!is_null($Manager))
									{
										$data = array('name' => $Manager->full_name, 'category' => 'User', 'client_name' => $client_name, 'lead_type' => $request->lead_type, 'UserName' => Session::get('user_name'), 'UserType' => Session::get('user_category'), 'first_name' => $request->first_name, 'last_name' => $request->last_name, 'email' => $request->email, 'phone' => $phone, 'city' => $city);
										
										Mail::send('emails.new_lead', $data, function ($message) use ($Manager) {
											$message->from('noreply@pownder.com', 'Pownder');
											$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
										});
										
										DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
										->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
									}
								}
							}
						}
					}
				}
			}
			
			$Managers = DB::table('managers')->where('lead_notifier', 'On')->where('client_id', $client_id)->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
			foreach($Managers as $Manager)
			{
				if (!in_array($Manager->id, $ManagerArray))
				{	
					$manager_permission = DB::table('manager_permissions')->where('permission', 'All Leads')->where('manager_id', $Manager->id)->where('is_deleted', 0)->get();
					if(!is_null($manager_permission))
					{
						$data = array('name' => $Manager->full_name, 'category' => 'User', 'client_name' => $client_name, 'lead_type' => $request->lead_type, 'UserName' => Session::get('user_name'), 'UserType' => Session::get('user_category'), 'first_name' => $request->first_name, 'last_name' => $request->last_name, 'email' => $request->email, 'phone' => $phone, 'city' => $city);
						
						Mail::send('emails.new_lead', $data, function ($message) use ($Manager) {
							$message->from('noreply@pownder.com', 'Pownder');
							$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
						});
					}
				}
			}
			
			$admin = DB::table('admins')->where('id', 1)->where('new_lead_email', '!=', '')->where('new_lead_option', 'Yes')->where('is_deleted', 0)->first();
			if(!is_null($admin))
			{
				if($admin->receive_notification == 'Everytime')
				{
					$data = array('name' => $admin->name, 'category' => 'admin', 'client_name' => $client_name, 'lead_type' => $request->lead_type, 'UserName' => Session::get('user_name'), 'UserType' => Session::get('user_category'), 'first_name' => $request->first_name, 'last_name' => $request->last_name, 'email' => $request->email, 'phone' => $phone, 'city' => $city);
					
					Mail::send('emails.new_lead', $data, function ($message) use ($admin) {
						$message->from('noreply@pownder.com', 'Pownder');
						$message->to($admin->new_lead_email)->subject('NEW FACEBOOK LEAD');
					});
				}
				else
				{
					DB::table('admin_emails')->insert(['facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 'subject' => 'NEW FACEBOOK LEAD', 'client_name' => $client_name, 'leadType' => $request->lead_type, 'UserName' =>Session::get('user_name'), 'UserType' => Session::get('user_category'), 'leadEmail' => $request->email, 'leadPhone' => $phone, 'leadCity' => $city, 'first_name' => $request->first_name, 'last_name' => $request->last_name, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s')]);
				}
			}
			
			$data = array();
			$data['group_category_id'] = "'".$group_category_id."'";
			$data['facebook_ads_lead_user_id'] = "'".$facebook_ads_lead_user_id."'";
			
			return response()->json($data);
		}
		
		public function getLeadEdit(Request $request)
		{
			$data = DB::table('facebook_ads_lead_users')->where('id', $request->id)->first();
			return response()->json($data);
		}
		
		function postLeadEdit(Request $request)
		{
			$rule=[
			'first_name' => 'required',
			'last_name' => 'required',
			'email' => 'email_array',
			'phone' => 'required|phone_format',
			'zip' => 'zip_code_valid',
			'edit_lead_type' => 'required',
			'client_name' => 'required'			
			];
			
			$this->validate($request,$rule);
			
			$city=$request->city;
			$state=$request->state;
			
			if($request->zip!='')
			{
				$Address = Helper::get_zip_info($request->zip);
				if($city=='')
				{
					$city=$Address['city'];
				}
				
				if($state=='')
				{
					$state=$Address['state'];
				}
			}
			
			$latitude = '';
			$longitude = '';
			
			if($request->zip != '')
			{
				$address = str_replace(" ","+",$request->zip);
				$url = "http://maps.google.com/maps/api/geocode/json?address=$address";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$response = curl_exec($ch);
				curl_close($ch);
				
				$response_a = json_decode($response);
				if(isset($response_a->results))
				{
					if(count($response_a->results)>0)
					{
						$latitude = $response_a->results[0]->geometry->location->lat;
						$longitude = $response_a->results[0]->geometry->location->lng;
					}
				}
			}
			elseif($city != '')
			{
				$address = str_replace(" ","+",$city);
				$url = "http://maps.google.com/maps/api/geocode/json?address=$address";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$response = curl_exec($ch);
				curl_close($ch);
				
				$response_a = json_decode($response);
				if(isset($response_a->results))
				{
					if(count($response_a->results)>0)
					{
						$latitude = $response_a->results[0]->geometry->location->lat;
						$longitude = $response_a->results[0]->geometry->location->lng;
					}
				}
			}
			
			$facebook_ads_lead_user_id = $request->edit_facebook_ads_lead_user_id;
			$client_id = $request->client_name;
			$Client = DB::table('clients')->where('id', $client_id)->first();
			
			DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
			->update([
			'lead_type' => $request->edit_lead_type,
			'adset_name' => $request->edit_lead_type,
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'full_name' => $request->first_name.' '.$request->last_name,
			'campaign_name' => $Client->name,
			'email' => $request->email,
			'phone_number' => $request->phone,
			'city' => $city,
			'state' => $state,
			'street_address' => $request->full_address,
			'post_code' => $request->zip,
			'latitude' => $latitude,
			'longitude' => $longitude,
			'client_id' => $client_id,
			'updated_user_id' => Session::get('user_id'),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			$data = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->first();
			
			DB::table('client_ads_lead_users')->where('id', $data->client_ads_lead_user_id)
			->update(['user_id' => $Client->created_user_id, 'client_id' => $client_id, 'package_id' => $Client->package_id, 'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
		}
		
		public function getDisputeLeads()
		{
			$Columns = array();
			
			$Results = DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Dispute Lead List Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Results as $Result)
			{
				$Columns[] = $Result->column_value;
			}
			
			$Leads = DB::table('facebook_ads_lead_users')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 2)->get();
			
			return view('panel.frontend.dispute_lead',['Columns' => $Columns, 'Leads' => $Leads]);
		}
		
		function postDisputeLeadEdit(Request $request)
		{
			$rule=[
			'first_name' => 'required',
			'last_name' => 'required',
			'email' => 'email_array',
			'phone' => 'required|phone_format',
			'zip' => 'zip_code_valid',			
			];
			
			$this->validate($request,$rule);
			
			$city=$request->city;
			$state=$request->state;
			
			if($request->zip!='')
			{
				$Address = Helper::get_zip_info($request->zip);
				if($city=='')
				{
					$city=$Address['city'];
				}
				
				if($state=='')
				{
					$state=$Address['state'];
				}
			}
			
			$latitude = '';
			$longitude = '';
			
			if($request->zip != '')
			{
				$address = str_replace(" ","+",$request->zip);
				$url = "http://maps.google.com/maps/api/geocode/json?address=$address";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$response = curl_exec($ch);
				curl_close($ch);
				
				$response_a = json_decode($response);
				if(isset($response_a->results))
				{
					if(count($response_a->results)>0)
					{
						$latitude = $response_a->results[0]->geometry->location->lat;
						$longitude = $response_a->results[0]->geometry->location->lng;
					}
				}
			}
			elseif($city != '')
			{
				$address = str_replace(" ","+",$city);
				$url = "http://maps.google.com/maps/api/geocode/json?address=$address";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$response = curl_exec($ch);
				curl_close($ch);
				
				$response_a = json_decode($response);
				if(isset($response_a->results))
				{
					if(count($response_a->results)>0)
					{
						$latitude = $response_a->results[0]->geometry->location->lat;
						$longitude = $response_a->results[0]->geometry->location->lng;
					}
				}
			}
			
			$facebook_ads_lead_user_id = $request->edit_facebook_ads_lead_user_id;
			
			DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
			->update([
			'first_name' => $request->first_name,
			'last_name' => $request->last_name,
			'full_name' => $request->first_name.' '.$request->last_name,
			'email' => $request->email,
			'phone_number' => $request->phone,
			'city' => $city,
			'state' => $state,
			'street_address' => $request->full_address,
			'post_code' => $request->zip,
			'latitude' => $latitude,
			'longitude' => $longitude,
			'updated_user_id' => Session::get('user_id'),
			'updated_at' => date('Y-m-d H:i:s'),
			'is_deleted' => 0
			]);
		}
		
		public function getLeadReAssignClient(Request $request)
		{
			$facebook_ads_lead_user_id = $request->facebook_ads_lead_user_id;
			
			$lead_clients = array();
			$client_ads_lead_users = DB::table('client_ads_lead_users')->select('client_id')->where('facebook_ads_lead_user_id', $facebook_ads_lead_user_id)->get();
			foreach($client_ads_lead_users as $client_ads_lead_user)
			{
				$lead_clients[] = $client_ads_lead_user->client_id;
			}
			
			$ShowClient = '';
			$vendor_id = 0;
			if(Session::get('user_category')=='user')
			{
				$ShowClient = ManagerHelper::ManagerClientMenuAction();
				
				$manager = DB::table('managers')->select('vendor_id')->where('id',Session::get('manager_id'))->first();
				$vendor_id = $manager->vendor_id;
			}
			else
			{
				$vendor_id = Session::get('vendor_id');
			}
			
			$Clients = DB::table('clients')
			->select('id', 'name', 'email')
			->where(function ($query) use($ShowClient, $vendor_id){
				if($ShowClient == 'Allotted Clients')
				{
					$query->where('manager_id', Session::get('manager_id'));
				}
				else
				{
					if(Session::get('user_category') == 'admin' || ManagerHelper::ManagerCategory() == 'admin')
					{
						$query->where('vendor_id','=', 0);
					}
					else
					{
						$query->where('vendor_id', $vendor_id);
					}
				}
			})
			->whereNotIn('id', $lead_clients)
			->where('status', 1)
			->where('is_deleted', 0)
			->orderBy('name', 'ASC')
			->orderBy('email', 'ASC')
			->get();
			
			return response()->json($Clients);
		}
		
		public function getLeadReSync(Request $request)
		{
			$facebook_ads_lead_user_id = $request->facebook_ads_lead_user_id;
			$client_id = 0;
			$reason = '';
			
			$facebook_ads_lead_user = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('client_id', '>', 0)->first();
			if(!is_null($facebook_ads_lead_user))
			{
				$lead_type = $facebook_ads_lead_user->lead_type;
				$messenger_lead_id = $facebook_ads_lead_user->messenger_lead_id;
				
				$facebook_ads_lead_id = $facebook_ads_lead_user->facebook_ads_lead_id;
				
				$client_id = $facebook_ads_lead_user->client_id;
				if($lead_type == 'Pownder™ Lead')
				{
					$campaign = DB::table('campaigns')->where('facebook_ads_lead_id', 'like', '%'.$facebook_ads_lead_id.'%')->where('client_id', $client_id)->where('is_deleted', 0)->first();
				}
				elseif($messenger_lead_id != '')
				{
					$campaign = DB::table('campaigns')->where('page_engagement', 'Yes')->where('client_id', $client_id)->where('is_deleted', 0)->first();
				}
				else
				{
					$campaign = DB::table('campaigns')->where('manual_lead_type', 'like', '%'.$lead_type.'%')->where('client_id', $client_id)->where('is_deleted', 0)->first();
				}
				
				if(!is_null($campaign))
				{
					$campaign_id = $campaign->id;
					$crm_account = $campaign->crm_account;
					if($campaign->is_active == 0)
					{
						if($campaign->crm_account != '')
						{
							if($campaign->crm_account == 'Agile' || $campaign->crm_account == 'Zoho')
							{
								$CRMAccount = DB::table('crm_accounts')->where('client_id', $client_id)->where('campaign_id', $campaign_id)->where('is_deleted',0)->first();
								if(!is_null($CRMAccount))
								{
									$crm_account_id = $CRMAccount->id;
									$crm_account = $CRMAccount->account;
									$crm_domain = $CRMAccount->domain;
									$crm_username = $CRMAccount->username;
									$crm_password = $CRMAccount->password;
									$AccessToken = $CRMAccount->access_token;
									
									if($crm_account == 'Zoho')
									{
										$TokenStatus=ConsoleHelper::ZohoTokenExpiryCheck($AccessToken);
										if($TokenStatus=='expire')
										{
											$AccessToken = ConsoleHelper::ZohoOauthToken($crm_username, $crm_password);
											if($AccessToken!='')
											{
												DB::table('crm_accounts')
												->where('id', $crm_account_id)
												->update(['access_token' => $AccessToken]);
											}
											else
											{
												continue;
											}
										}
									}
									
									$facebook_ads_lead_user_id = $facebook_ads_lead_user->id;
									$lead_type = $facebook_ads_lead_user->lead_type;
									$facebook_ads_lead_id = $facebook_ads_lead_user->facebook_ads_lead_id;
									$facebook_page_id = $facebook_ads_lead_user->facebook_page_id;
									$facebook_account_id = $facebook_ads_lead_user->facebook_account_id;
									
									$first_name = $facebook_ads_lead_user->first_name;
									if($first_name=='')
									{
										$first_name = $facebook_ads_lead_user->full_name;
									}
									
									$last_name = $facebook_ads_lead_user->last_name;
									$email = $facebook_ads_lead_user->email;
									$phone_number = $facebook_ads_lead_user->phone_number;
									$street_address = $facebook_ads_lead_user->street_address;
									$city = $facebook_ads_lead_user->city;
									$state = $facebook_ads_lead_user->state;
									$province = $facebook_ads_lead_user->province;
									$country = $facebook_ads_lead_user->country;
									
									if($crm_account == 'Agile')
									{
										$response = ConsoleHelper::AgileCreateContact($street_address,$city,$state,$country,$email,'','',$lead_type,$first_name,$last_name,'',$phone_number,'','',$crm_domain,$crm_username,$crm_password);
										
										if($response==200)
										{
											$crm_contact_id = DB::table('crm_contacts')->insertGetId([
											'facebook_account_id' => $facebook_account_id,
											'facebook_page_id' => $facebook_page_id,
											'facebook_ads_lead_id' => $facebook_ads_lead_id,
											'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
											'campaign_id' => $campaign_id,
											'client_id' => $client_id,
											'crm_account_id' => $crm_account_id,
											'crm_account' => $crm_account,
											'crm_domain' => $crm_domain,
											'crm_username' => $crm_username,
											'crm_password' => $crm_password,
											'resync' => 'Yes',
											'created_user_id' => Session::get('user_id'),
											'created_user_ip' => $request->ip(),
											'created_at' => date('Y-m-d H:i:s'),
											'updated_user_id' => Session::get('user_id'),
											'updated_user_ip' => $request->ip(),
											'updated_at' => date('Y-m-d H:i:s')
											]);
											
											DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
											->update(['crm_contact_id' => $crm_contact_id, 'resync' => 'Yes']);
											
											$reason = 'Lead Re-Sync successfully!.';
											
											DB::table('resync_error')->where('facebook_ads_lead_user_id', $facebook_ads_lead_user_id)->where('is_deleted', 0)
											->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
										}
										else
										{
											$reason = 'Please try again some time later.';
										}
									}	
									
									if($crm_account == 'Zoho')
									{
										if($AccessToken!='')
										{
											$response = ConsoleHelper::ZohoCreateContact($first_name,$last_name,$email,'',$phone_number,'','','',$AccessToken);
											if (strpos($response, 'added successfully') !== false) 
											{
												$crm_contact_id = DB::table('crm_contacts')->insertGetId([
												'facebook_account_id' => $facebook_account_id,
												'facebook_page_id' => $facebook_page_id,
												'facebook_ads_lead_id' => $facebook_ads_lead_id,
												'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
												'campaign_id' => $campaign_id,
												'client_id' => $client_id,
												'crm_account_id' => $crm_account_id,
												'crm_account' => $crm_account,
												'crm_domain' => $crm_domain,
												'crm_username' => $crm_username,
												'crm_password' => $crm_password,
												'resync' => 'Yes',
												'created_user_id' => Session::get('user_id'),
												'created_user_ip' => $request->ip(),
												'created_at' => date('Y-m-d H:i:s'),
												'updated_user_id' => Session::get('user_id'),
												'updated_user_ip' => $request->ip(),
												'updated_at' => date('Y-m-d H:i:s')
												]);
												
												DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
												->update(['crm_contact_id' => $crm_contact_id, 'resync' => 'Yes']);
												
												$reason = 'Lead Re-Sync successfully!.';
												
												DB::table('resync_error')->where('facebook_ads_lead_user_id', $facebook_ads_lead_user_id)->where('is_deleted', 0)
												->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
											}
											else
											{
												$reason = 'Please try again some time later.';
											}
										}
									}			
								}
								else
								{
									$reason = 'This lead related campaign CRM account not define.';
								}
							}
							else
							{
								$autometive_campaign = DB::table('autometive_campaigns')->where('campaign_id', $campaign_id)->where('is_deleted',0)->first();
								if(!is_null($autometive_campaign))
								{
									$autometive_campaign_id = $autometive_campaign->id;
									$prospect = $autometive_campaign->prospect;
									$phone_time = $autometive_campaign->phone_time;
									$phone_type = $autometive_campaign->phone_type;
									
									$provider_name = $autometive_campaign->provider_name;
									$service_name = $autometive_campaign->service_name;
									$provider_email = $autometive_campaign->provider_email;
									$provider_phone = $autometive_campaign->provider_phone;
									$provider_url = $autometive_campaign->provider_url;
									$provider_zip = $autometive_campaign->provider_zip;
									$provider_city = $autometive_campaign->provider_city;
									$provider_state = $autometive_campaign->provider_state;
									
									$vendor_name = $autometive_campaign->vendor_name;
									$vendor_email = $autometive_campaign->vendor_email;
									$vendor_phone = $autometive_campaign->vendor_phone;
									$vendor_url = $autometive_campaign->vendor_url;
									$vendor_address = $autometive_campaign->vendor_address;
									$vendor_zip = $autometive_campaign->vendor_zip;
									$vendor_city = $autometive_campaign->vendor_city;
									$vendor_state = $autometive_campaign->vendor_state;
									
									$facebook_ads_lead_user_id = $facebook_ads_lead_user->id;
									$facebook_ads_lead_id = $facebook_ads_lead_user->facebook_ads_lead_id;
									$facebook_page_id = $facebook_ads_lead_user->facebook_page_id;
									$facebook_account_id = $facebook_ads_lead_user->facebook_account_id;
									
									$address = $facebook_ads_lead_user->street_address;
									$state = $facebook_ads_lead_user->state; 
									
									$first_name = '';
									$last_name = '';
									$email = '';
									$phone = '';
									$city = '';
									$post_code = '';
									
									if($autometive_campaign->auto_email == 'Yes')
									{
										$email = $facebook_ads_lead_user->email;
									}
									
									if($autometive_campaign->auto_first_name == 'Yes')
									{
										if($facebook_ads_lead_user->first_name == '')
										{
											$first_name = $facebook_ads_lead_user->full_name;
										}
										else
										{
											$first_name = $facebook_ads_lead_user->first_name;
										}
									}
									
									if($autometive_campaign->auto_last_name == 'Yes')
									{
										$last_name = $facebook_ads_lead_user->last_name;
									}
									
									if($autometive_campaign->auto_phone_number == 'Yes')
									{
										$phone = $facebook_ads_lead_user->phone_number;
									}
									
									if($autometive_campaign->auto_city == 'Yes')
									{
										$city = $facebook_ads_lead_user->city;
									}
									
									if($autometive_campaign->auto_zip_code == 'Yes')
									{
										$post_code = $facebook_ads_lead_user->post_code;
									}
									
									$response = '';
									
									if($crm_account == 'Advent')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'CDK')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'Dealerpeak')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'DealerSocket')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'DealerTrack')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'Dominion Dealer Solutions')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'eflow automotive')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'eLead CRM')
									{
										$response = ConsoleHelper::eLeadCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'FordDirect')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'HigherGear')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'Momentum')
									{
										$response = ConsoleHelper::MomentumCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'ProMax Unlimited')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'Reynolds & Reynolds')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'VinSolutions')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									else if($crm_account == 'Votenza')
									{
										$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
									}
									
									if($response == 'success')
									{
										$crm_contact_id = DB::table('crm_contacts')->insertGetId([
										'facebook_account_id' => $facebook_account_id,
										'facebook_page_id' => $facebook_page_id,
										'facebook_ads_lead_id' => $facebook_ads_lead_id,
										'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
										'campaign_id' => $campaign_id,
										'client_id' => $client_id,
										'autometive_campaign_id' => $autometive_campaign_id,
										'crm_account' => $crm_account,
										'resync' => 'Yes',
										'created_user_id' => Session::get('user_id'),
										'created_user_ip' => $request->ip(),
										'created_at' => date('Y-m-d H:i:s'),
										'updated_user_id' => Session::get('user_id'),
										'updated_user_ip' => $request->ip(),
										'updated_at' => date('Y-m-d H:i:s')
										]);
										
										DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
										->update(['crm_contact_id' => $crm_contact_id, 'resync' => 'Yes']);
										
										$reason = 'Lead Re-Sync successfully!.';
										
										DB::table('resync_error')->where('facebook_ads_lead_user_id', $facebook_ads_lead_user_id)->where('is_deleted', 0)
										->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
									}
									else
									{
										$reason = 'Please try again some time later.';
									}
								}
								else
								{
									$reason = 'This lead related campaign CRM account not define.';
								}
							}
						}
						else
						{
							$reason = 'This lead related campaign CRM account not define.';
						}
					}
					else
					{
						$reason = 'This lead related campaign inactive.';
					}
				}
				else
				{
					$reason = 'Campaign not found for this lead.';
				}
			}
			else
			{
				$reason = 'Lead not assign to any client.';
			}
			
			if($reason != 'Lead Re-Sync successfully!.')
			{
				DB::table('resync_error')->insert(['facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 'client_id' => $client_id, 'reason' => $reason, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s'), 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
		}
		
		function postAppointmentLeadSetting(Request $request)
		{
			$lastData = DB::table('facebook_ads_lead_user_settings')->where('setting', 'Appointment')->where('facebook_ads_lead_user_id', $request->appointment_facebook_ads_lead_user_id)->where('is_deleted', 0)->orderBy('id', 'DESC')->first();
			
			if($request->appointment_lead_setting=='Reschedule')
			{
				$app_date = explode('-',$request->reschedule_appointment_date);
				$appointment_date = $app_date[2].'-'.$app_date[0].'-'.$app_date[1];
				$facebook_ads_lead_user_id = $request->appointment_facebook_ads_lead_user_id;
				
				DB::table('facebook_ads_lead_user_settings')->where('id', $lastData->id)->where('facebook_ads_lead_user_id', $request->appointment_facebook_ads_lead_user_id)
				->update(['comment' => $request->appointment_lead_setting, 'appointment_date' => $appointment_date, 'appointment_time' => $request->reschedule_appointment_time, 'borderColor' => '#4FC1E9', 'backgroundColor' => '#4FC1E9', 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
				
				DB::table('facebook_ads_lead_users')->where('id', $request->appointment_facebook_ads_lead_user_id)
				->update(['appointment_status' => $request->appointment_lead_setting, 'appointment_date' => $appointment_date, 'appointment_time' => $request->reschedule_appointment_time, 'borderColor' => '#4FC1E9', 'backgroundColor' => '#4FC1E9', 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
				
				$leadUser = DB::table('facebook_ads_lead_users')->where('id', $request->appointment_facebook_ads_lead_user_id)->first();
				$facebook_page_id = $leadUser->facebook_page_id;
				$lead_user_id = $leadUser->created_user_id;
				$client_id = $leadUser->client_id;
				$manager_id = $leadUser->manager_id;
				
				if($leadUser->full_name != '')
				{
					$leadName = $leadUser->full_name;
				}
				else
				{
					$leadName = $leadUser->first_name.' '.$leadUser->last_name;
				}
				
				$leadType = $leadUser->lead_type;
				$leadEmail = $leadUser->email;
				$leadPhone = $leadUser->phone_number;
				$leadCity = $leadUser->city;
				
				$leadAddedBy = '';
				$User = DB::table('users')->where('id', $lead_user_id)->first();
				if(($leadType == 'Pownder™ Call' || $leadType == 'Pownder™ Messenger') && $facebook_page_id == 0)
				{
					$leadAddedBy = $User->name.' ('.title_case($User->category).')';
				}
				
				$manager_name = '';
				$LeadManager = DB::table('managers')->where('id', $manager_id)->first();
				if(!is_null($LeadManager))
				{
					$manager_name = title_case($LeadManager->full_name);
				}
				
				$Client = DB::table('users')->where('client_id', $client_id)->first();
				
				$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
				foreach($CampaignClients as $CampaignClient)
				{
					if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only Appointments'))
					{
						$data = array('name' => $CampaignClient->client_contact, 'appointment_date' => $request->reschedule_appointment_date, 'appointment_time' => $request->reschedule_appointment_time, 'category' => 'Client', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
						
						Mail::send('emails.appointment', $data, function ($message) use ($CampaignClient) {
							$message->from('noreply@pownder.com', 'Pownder');
							$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('Facebook Reschedule Appointment');
						});
					}
					
					if($CampaignClient->user_notification == 'Yes' && $manager_id == 0)
					{
						$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
						if(!is_null($ManagerExists))
						{
							if($CampaignClient->notification == 'For Appointments' || $CampaignClient->notification == 'Both')
							{
								if($CampaignClient->manager_id != '')
								{
									$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
									
									$ManagerArray = explode(",", $CampaignClient->manager_id);
									
									$Array = array();
									$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
									foreach($queries as $query)
									{
										$Array[] = $query->id;
									}
									
									if(count($Array) > 0)
									{
										$index = $LeadCount % count($Array);
										
										$Manager = DB::table('managers')->where('id', $Array[$index])->where('appointment_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
										if(!is_null($Manager))
										{
											$data = array('name' => $Manager->full_name, 'appointment_date' => $request->reschedule_appointment_date, 'appointment_time' => $request->reschedule_appointment_time, 'category' => 'User', 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
											
											Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
												$message->from('noreply@pownder.com', 'Pownder');
												$message->to($Manager->email)->subject('Facebook Reschedule Appointment');
											});
											
											DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
											->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
										}
									}
								}
							}
						}
					}
				}
				
				if($manager_id > 0)
				{
					$Manager = DB::table('users')->where('manager_id', $manager_id)->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
					if(!is_null($Manager))
					{
						$data = array('name' => $Manager->name, 'appointment_date' => $request->reschedule_appointment_date, 'appointment_time' => $request->reschedule_appointment_time, 'category' => $Manager->category, 'client_name' => $Client->name, 'manager_name' => $manager_name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
						
						Mail::send('emails.appointment', $data, function ($message) use ($Manager) {
							$message->from('noreply@pownder.com', 'Pownder');
							$message->to($Manager->email)->subject('Facebook Reschedule Appointment');
						});
					}
				}
				
				$admin = DB::table('admins')->where('id', 1)->where('appointment_email', '!=', '')->where('appointment_option', 'Yes')->where('is_deleted', 0)->first();
				if(!is_null($admin))
				{
					if($admin->receive_notification == 'Everytime')
					{
						$data = array('name' => $admin->name, 'appointment_date' => $request->reschedule_appointment_date, 'appointment_time' => $request->reschedule_appointment_time, 'category' => 'admin', 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity);
						
						Mail::send('emails.appointment', $data, function ($message) use ($admin) {
							$message->from('noreply@pownder.com', 'Pownder');
							$message->to($admin->appointment_email)->subject('Facebook Reschedule Appointment');
						});
					}
					else
					{
						DB::table('admin_emails')->insert(['facebook_ads_lead_user_id' => $request->appointment_facebook_ads_lead_user_id, 'subject' => 'Facebook Reschedule Appointment', 'appointment_date' => $request->reschedule_appointment_date, 'appointment_time' => $request->reschedule_appointment_time, 'manager_name' => $manager_name, 'client_name' => $Client->name, 'leadType' => $leadType, 'leadAddedBy' => $leadAddedBy, 'leadName' => $leadName, 'leadEmail' => $leadEmail, 'leadPhone' => Helper::emailPhoneFormat($leadPhone), 'leadCity' => $leadCity, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s')]);
					}
				}
			}
			elseif($request->appointment_lead_setting=='Sold')
			{
				DB::table('facebook_ads_lead_user_settings')
				->insert(['facebook_ads_lead_user_id' => $request->appointment_facebook_ads_lead_user_id, 'setting' => $request->appointment_lead_setting, 'comment' => '', 'borderColor' => '#4FC1E9', 'backgroundColor' => '#4FC1E9', 'created_user_id' => Session::get('user_id'), 'created_at' => date('Y-m-d H:i:s'), 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
				
				DB::table('facebook_ads_lead_users')->where('id', $request->appointment_facebook_ads_lead_user_id)
				->update(['setting' => $request->appointment_lead_setting, 'comment' => '', 'appointment_date' => '', 'appointment_time' => '', 'appointment_status' => '', 'borderColor' => '#4FC1E9', 'backgroundColor' => '#4FC1E9', 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			else
			{
				$borderColor = '#4FC1E9';
				$backgroundColor = '#4FC1E9';
				if($request->appointment_lead_setting == 'Confirmed')
				{
					$borderColor = '#22D69D';
					$backgroundColor = '#22D69D';
				}
				elseif($request->appointment_lead_setting == 'Canceled')
				{
					$borderColor = '#FB8678';
					$backgroundColor = '#FB8678';
				}
				
				DB::table('facebook_ads_lead_user_settings')->where('id', $lastData->id)->where('facebook_ads_lead_user_id', $request->appointment_facebook_ads_lead_user_id)
				->update(['comment' => $request->appointment_lead_setting, 'borderColor' => $borderColor, 'backgroundColor' => $backgroundColor, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
				
				DB::table('facebook_ads_lead_users')->where('id', $request->appointment_facebook_ads_lead_user_id)
				->update(['appointment_status' => $request->appointment_lead_setting, 'borderColor' => $borderColor, 'backgroundColor' => $backgroundColor, 'updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			
			return $request->appointment_lead_setting;
		}
		
		public function postMultipleLeadReSync(Request $request)
		{
			$dataId = $request->id;
			for($ij = 0; $ij < count($dataId); $ij++)
			{
				$facebook_ads_lead_user_id = $dataId[$ij];
				$client_id = 0;
				$reason = '';
				$facebook_ads_lead_user = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('client_id', '>', 0)->first();
				if(!is_null($facebook_ads_lead_user))
				{
					$lead_type = $facebook_ads_lead_user->lead_type;
					$messenger_lead_id = $facebook_ads_lead_user->messenger_lead_id;
					
					$facebook_ads_lead_id = $facebook_ads_lead_user->facebook_ads_lead_id;
					
					$client_id = $facebook_ads_lead_user->client_id;
					if($lead_type == 'Pownder™ Lead')
					{
						$campaign = DB::table('campaigns')->where('facebook_ads_lead_id', 'like', '%'.$facebook_ads_lead_id.'%')->where('client_id', $client_id)->where('is_deleted', 0)->first();
					}
					elseif($messenger_lead_id != '')
					{
						$campaign = DB::table('campaigns')->where('page_engagement', 'Yes')->where('client_id', $client_id)->where('is_deleted', 0)->first();
					}
					else
					{
						$campaign = DB::table('campaigns')->where('manual_lead_type', 'like', '%'.$lead_type.'%')->where('client_id', $client_id)->where('is_deleted', 0)->first();
					}
					
					if(!is_null($campaign))
					{
						$campaign_id = $campaign->id;
						$crm_account = $campaign->crm_account;
						if($campaign->is_active == 0)
						{
							if($campaign->crm_account != '')
							{
								if($campaign->crm_account == 'Agile' || $campaign->crm_account == 'Zoho')
								{
									$CRMAccount = DB::table('crm_accounts')->where('client_id', $client_id)->where('campaign_id', $campaign_id)->where('is_deleted',0)->first();
									if(!is_null($CRMAccount))
									{
										$crm_account_id = $CRMAccount->id;
										$crm_account = $CRMAccount->account;
										$crm_domain = $CRMAccount->domain;
										$crm_username = $CRMAccount->username;
										$crm_password = $CRMAccount->password;
										$AccessToken = $CRMAccount->access_token;
										
										if($crm_account == 'Zoho')
										{
											$TokenStatus=ConsoleHelper::ZohoTokenExpiryCheck($AccessToken);
											if($TokenStatus=='expire')
											{
												$AccessToken = ConsoleHelper::ZohoOauthToken($crm_username, $crm_password);
												if($AccessToken!='')
												{
													DB::table('crm_accounts')
													->where('id', $crm_account_id)
													->update(['access_token' => $AccessToken]);
												}
												else
												{
													continue;
												}
											}
										}
										
										$facebook_ads_lead_user_id = $facebook_ads_lead_user->id;
										$lead_type = $facebook_ads_lead_user->lead_type;
										$facebook_ads_lead_id = $facebook_ads_lead_user->facebook_ads_lead_id;
										$facebook_page_id = $facebook_ads_lead_user->facebook_page_id;
										$facebook_account_id = $facebook_ads_lead_user->facebook_account_id;
										
										$first_name = $facebook_ads_lead_user->first_name;
										if($first_name=='')
										{
											$first_name = $facebook_ads_lead_user->full_name;
										}
										
										$last_name = $facebook_ads_lead_user->last_name;
										$email = $facebook_ads_lead_user->email;
										$phone_number = $facebook_ads_lead_user->phone_number;
										$street_address = $facebook_ads_lead_user->street_address;
										$city = $facebook_ads_lead_user->city;
										$state = $facebook_ads_lead_user->state;
										$province = $facebook_ads_lead_user->province;
										$country = $facebook_ads_lead_user->country;
										
										if($crm_account == 'Agile')
										{
											$response = ConsoleHelper::AgileCreateContact($street_address,$city,$state,$country,$email,'','',$lead_type,$first_name,$last_name,'',$phone_number,'','',$crm_domain,$crm_username,$crm_password);
											
											if($response==200)
											{
												$crm_contact_id = DB::table('crm_contacts')->insertGetId([
												'facebook_account_id' => $facebook_account_id,
												'facebook_page_id' => $facebook_page_id,
												'facebook_ads_lead_id' => $facebook_ads_lead_id,
												'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
												'campaign_id' => $campaign_id,
												'client_id' => $client_id,
												'crm_account_id' => $crm_account_id,
												'crm_account' => $crm_account,
												'crm_domain' => $crm_domain,
												'crm_username' => $crm_username,
												'crm_password' => $crm_password,
												'resync' => 'Yes',
												'created_user_id' => Session::get('user_id'),
												'created_user_ip' => $request->ip(),
												'created_at' => date('Y-m-d H:i:s'),
												'updated_user_id' => Session::get('user_id'),
												'updated_user_ip' => $request->ip(),
												'updated_at' => date('Y-m-d H:i:s')
												]);
												
												DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
												->update(['crm_contact_id' => $crm_contact_id, 'resync' => 'Yes']);
												
												$reason = 'Lead Re-Sync successfully!.';
												
												DB::table('resync_error')->where('facebook_ads_lead_user_id', $facebook_ads_lead_user_id)->where('is_deleted', 0)
												->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
											}
											else
											{
												$reason = 'Please try again some time later.';
											}
										}	
										
										if($crm_account == 'Zoho')
										{
											if($AccessToken!='')
											{
												$response = ConsoleHelper::ZohoCreateContact($first_name,$last_name,$email,'',$phone_number,'','','',$AccessToken);
												if (strpos($response, 'added successfully') !== false) 
												{
													$crm_contact_id = DB::table('crm_contacts')->insertGetId([
													'facebook_account_id' => $facebook_account_id,
													'facebook_page_id' => $facebook_page_id,
													'facebook_ads_lead_id' => $facebook_ads_lead_id,
													'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
													'campaign_id' => $campaign_id,
													'client_id' => $client_id,
													'crm_account_id' => $crm_account_id,
													'crm_account' => $crm_account,
													'crm_domain' => $crm_domain,
													'crm_username' => $crm_username,
													'crm_password' => $crm_password,
													'resync' => 'Yes',
													'created_user_id' => Session::get('user_id'),
													'created_user_ip' => $request->ip(),
													'created_at' => date('Y-m-d H:i:s'),
													'updated_user_id' => Session::get('user_id'),
													'updated_user_ip' => $request->ip(),
													'updated_at' => date('Y-m-d H:i:s')
													]);
													
													DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
													->update(['crm_contact_id' => $crm_contact_id, 'resync' => 'Yes']);
													
													$reason = 'Lead Re-Sync successfully!.';
													
													DB::table('resync_error')->where('facebook_ads_lead_user_id', $facebook_ads_lead_user_id)->where('is_deleted', 0)
													->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
												}
												else
												{
													$reason = 'Please try again some time later.';
												}
											}
										}			
									}
									else
									{
										$reason = 'This lead related campaign CRM account not define.';
									}
								}
								else
								{
									$autometive_campaign = DB::table('autometive_campaigns')->where('campaign_id', $campaign_id)->where('is_deleted',0)->first();
									if(!is_null($autometive_campaign))
									{
										$autometive_campaign_id = $autometive_campaign->id;
										$prospect = $autometive_campaign->prospect;
										$phone_time = $autometive_campaign->phone_time;
										$phone_type = $autometive_campaign->phone_type;
										
										$provider_name = $autometive_campaign->provider_name;
										$service_name = $autometive_campaign->service_name;
										$provider_email = $autometive_campaign->provider_email;
										$provider_phone = $autometive_campaign->provider_phone;
										$provider_url = $autometive_campaign->provider_url;
										$provider_zip = $autometive_campaign->provider_zip;
										$provider_city = $autometive_campaign->provider_city;
										$provider_state = $autometive_campaign->provider_state;
										
										$vendor_name = $autometive_campaign->vendor_name;
										$vendor_email = $autometive_campaign->vendor_email;
										$vendor_phone = $autometive_campaign->vendor_phone;
										$vendor_url = $autometive_campaign->vendor_url;
										$vendor_address = $autometive_campaign->vendor_address;
										$vendor_zip = $autometive_campaign->vendor_zip;
										$vendor_city = $autometive_campaign->vendor_city;
										$vendor_state = $autometive_campaign->vendor_state;
										
										$facebook_ads_lead_user_id = $facebook_ads_lead_user->id;
										$facebook_ads_lead_id = $facebook_ads_lead_user->facebook_ads_lead_id;
										$facebook_page_id = $facebook_ads_lead_user->facebook_page_id;
										$facebook_account_id = $facebook_ads_lead_user->facebook_account_id;
										
										$address = $facebook_ads_lead_user->street_address;
										$state = $facebook_ads_lead_user->state; 
										
										$first_name = '';
										$last_name = '';
										$email = '';
										$phone = '';
										$city = '';
										$post_code = '';
										
										if($autometive_campaign->auto_email == 'Yes')
										{
											$email = $facebook_ads_lead_user->email;
										}
										
										if($autometive_campaign->auto_first_name == 'Yes')
										{
											if($facebook_ads_lead_user->first_name == '')
											{
												$first_name = $facebook_ads_lead_user->full_name;
											}
											else
											{
												$first_name = $facebook_ads_lead_user->first_name;
											}
										}
										
										if($autometive_campaign->auto_last_name == 'Yes')
										{
											$last_name = $facebook_ads_lead_user->last_name;
										}
										
										if($autometive_campaign->auto_phone_number == 'Yes')
										{
											$phone = $facebook_ads_lead_user->phone_number;
										}
										
										if($autometive_campaign->auto_city == 'Yes')
										{
											$city = $facebook_ads_lead_user->city;
										}
										
										if($autometive_campaign->auto_zip_code == 'Yes')
										{
											$post_code = $facebook_ads_lead_user->post_code;
										}
										
										$response = '';
										
										if($crm_account == 'Advent')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'CDK')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'Dealerpeak')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'DealerSocket')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'DealerTrack')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'Dominion Dealer Solutions')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'eflow automotive')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'eLead CRM')
										{
											$response = ConsoleHelper::eLeadCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'FordDirect')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'HigherGear')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'Momentum')
										{
											$response = ConsoleHelper::MomentumCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'ProMax Unlimited')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'Reynolds & Reynolds')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'VinSolutions')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										else if($crm_account == 'Votenza')
										{
											$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
										}
										
										if($response == 'success')
										{
											$crm_contact_id = DB::table('crm_contacts')->insertGetId([
											'facebook_account_id' => $facebook_account_id,
											'facebook_page_id' => $facebook_page_id,
											'facebook_ads_lead_id' => $facebook_ads_lead_id,
											'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
											'campaign_id' => $campaign_id,
											'client_id' => $client_id,
											'autometive_campaign_id' => $autometive_campaign_id,
											'crm_account' => $crm_account,
											'resync' => 'Yes',
											'created_user_id' => Session::get('user_id'),
											'created_user_ip' => $request->ip(),
											'created_at' => date('Y-m-d H:i:s'),
											'updated_user_id' => Session::get('user_id'),
											'updated_user_ip' => $request->ip(),
											'updated_at' => date('Y-m-d H:i:s')
											]);
											
											DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
											->update(['crm_contact_id' => $crm_contact_id, 'resync' => 'Yes']);
											
											$reason = 'Lead Re-Sync successfully!.';
											
											DB::table('resync_error')->where('facebook_ads_lead_user_id', $facebook_ads_lead_user_id)->where('is_deleted', 0)
											->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
										}
										else
										{
											$reason = 'Please try again some time later.';
										}
									}
									else
									{
										$reason = 'This lead related campaign CRM account not define.';
									}
								}
							}
							else
							{
								$reason = 'This lead related campaign CRM account not define.';
							}
						}
						else
						{
							$reason = 'This lead related campaign inactive.';
						}
					}
					else
					{
						$reason = 'Campaign not found for this lead.';
					}
				}
				else
				{
					$reason = 'Lead not assign to any client.';
				}
				
				if($reason != 'Lead Re-Sync successfully!.')
				{
					DB::table('resync_error')->insert(['facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 'client_id' => $client_id, 'reason' => $reason, 'created_user_id' => Session::get('user_id'), 'created_user_ip' => $request->ip(), 'created_at' => date('Y-m-d H:i:s'), 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
				}
			}
		}
		
		public function getClientAppointmentFilters(Request $request)
		{	
			$ShowClient = '';
			$ShowLead = '';
			$vendor_id = 0;
			$UserArray = array();
			$ClientArray = array();
			
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();	
				$ShowClient = ManagerHelper::ManagerClientMenuAction();				
				
				if(ManagerHelper::ManagerCategory()=='vendor')
				{
					$UserArray[] = Session::get('user_id');	
					$Vendor = DB::table('managers')->where('id',Session::get('manager_id'))->first();
					$vendor_id = $Vendor->vendor_id;
					$Clients = DB::table('clients')->select('id')->where('vendor_id', $vendor_id)->get();
					
					foreach($Clients as $Client)
					{
						$ClientArray[] = $Client->id;
						
						$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
						foreach($users as $user)
						{
							$UserArray[] = $user->id;
							
							$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
							foreach($Managers as $Manager)
							{
								$UserArray[] = $Manager->id;	
							}
						}
					}
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();	
				foreach($users as $user)
				{
					$UserArray[] = $user->id;	
				}
				
				$vendor_id = Session::get('vendor_id');
				
				$Clients = DB::table('clients')->select('id')->where('vendor_id',Session::get('vendor_id'))->get();
				foreach($Clients as $Client)
				{
					$ClientArray[] = $Client->id;
					
					$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
					foreach($users as $user)
					{			
						$UserArray[] = $user->id;
						
						$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
						foreach($Managers as $Manager)
						{
							$UserArray[] = $Manager->id;	
						}	
					}	
				}
			}
			
			$client_id = isset($request->client_id)?$request->client_id:'';
			
			$start_date='';
			$end_date='';
			$dateRange=isset($request->dateRange)?$request->dateRange:'';
			
			if($dateRange!='')
			{
				$explodeDate=explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			$TotalAppointment = DB::table('facebook_ads_lead_user_settings')
			->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('facebook_ads_lead_users.client_id', '=', $client_id);	
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					$query->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
					->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date);			
				}
			})
			->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));	
				}
				elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
				{
					$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');	
				}
			})->count('facebook_ads_lead_user_settings.id');
			
			$ConfirmedAppointment = DB::table('facebook_ads_lead_user_settings')
			->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Confirmed')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('facebook_ads_lead_users.client_id', '=', $client_id);	
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					$query->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
					->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date);		
				}
			})
			->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));	
				}
				elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
				{
					$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');	
				}
			})->count('facebook_ads_lead_user_settings.id');
			
			$ShownAppointment = DB::table('facebook_ads_lead_user_settings')
			->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Shown')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('facebook_ads_lead_users.client_id', '=', $client_id);	
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					$query->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
					->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date);		
				}
			})
			->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));	
				}
				elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
				{
					$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');	
				}	
			})->count('facebook_ads_lead_user_settings.id');
			
			$CanceledAppointment = DB::table('facebook_ads_lead_user_settings')
			->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Canceled')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) use($client_id){
				if($client_id > 0)
				{
					$query->where('facebook_ads_lead_users.client_id', '=', $client_id);	
				}
			})
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					$query->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
					->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date);		
				}
			})
			->where(function ($query) use($UserArray, $ClientArray, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', '=', Session::get('manager_id'));	
				}
				elseif(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
				{
					$query->whereIn('facebook_ads_lead_users.created_user_id', $UserArray)->whereIn('facebook_ads_lead_users.client_id', $ClientArray, 'or');	
				}	
			})->count('facebook_ads_lead_user_settings.id');
			
			$data = array();
			
			$data['TotalAppointment'] = number_format($TotalAppointment,0);
			
			if($TotalAppointment == 0)
			{
				$data['ConfirmedAppointment'] = '0%';	
			}
			else
			{
				$data['ConfirmedAppointment'] = number_format(($ConfirmedAppointment/$TotalAppointment)*100,2).'%';	
			}
			
			if($TotalAppointment == 0)
			{
				$data['ShownAppointment'] = '0%';	
			}
			else
			{
				$data['ShownAppointment'] = number_format(($ShownAppointment/$TotalAppointment)*100,2).'%';	
			}
			
			if($TotalAppointment == 0)
			{
				$data['CanceledAppointment'] = '0%';	
			}
			else
			{
				$data['CanceledAppointment'] = number_format(($CanceledAppointment/$TotalAppointment)*100,2).'%';	
			}
			
			return \Response::json($data);	
		}
		
		public function getClientFilterAppointment(Request $request)
		{	
			$ShowLead = '';
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				$manager = DB::table('managers')->select('client_id')->where('id',Session::get('manager_id'))->first();
				$client_id = $manager->client_id;
			}
			else
			{
				$client_id = Session::get('client_id');
			}
			
			$start_date='';
			$end_date='';
			$dateRange = isset($request->dateRange)?$request->dateRange:'';
			
			if($dateRange != '')
			{
				$explodeDate = explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			$TotalAppointment = DB::table('facebook_ads_lead_user_settings')
			->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					$query->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
					->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date);		
				}
			})
			->where(function ($query) use($client_id, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->where('facebook_ads_lead_users.client_id', $client_id);
				}
			})->count('facebook_ads_lead_user_settings.id');
			
			$ConfirmedAppointment = DB::table('facebook_ads_lead_user_settings')
			->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Confirmed')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					$query->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
					->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date);		
				}
			})
			->where(function ($query) use($client_id, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->where('facebook_ads_lead_users.client_id', $client_id);
				}
			})->count('facebook_ads_lead_user_settings.id');
			
			$ShownAppointment = DB::table('facebook_ads_lead_user_settings')
			->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Shown')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					$query->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
					->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date);		
				}
			})
			->where(function ($query) use($client_id, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->where('facebook_ads_lead_users.client_id', $client_id);
				}
			})->count('facebook_ads_lead_user_settings.id');
			
			$CanceledAppointment = DB::table('facebook_ads_lead_user_settings')
			->leftJoin('facebook_ads_lead_users', 'facebook_ads_lead_user_settings.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_user_settings.id')->where('facebook_ads_lead_user_settings.setting', 'Appointment')->where('facebook_ads_lead_user_settings.comment', 'Canceled')->where('facebook_ads_lead_user_settings.is_deleted', 0)
			->where(function ($query) use($start_date, $end_date){
				if($start_date!='' && $end_date!='')
				{
					$query->whereDate('facebook_ads_lead_users.created_time', '>=', $start_date)
					->whereDate('facebook_ads_lead_users.created_time', '<=', $end_date);		
				}
			})
			->where(function ($query) use($client_id, $ShowLead){
				if($ShowLead=='Allotted Leads')
				{
					$query->where('facebook_ads_lead_users.manager_id', Session::get('manager_id'));
				}
				else
				{
					$query->where('facebook_ads_lead_users.client_id', $client_id);
				}
			})->count('facebook_ads_lead_user_settings.id');
			
			$data = array();
			
			$data['TotalAppointment'] = number_format($TotalAppointment,0);
			
			if($TotalAppointment == 0)
			{
				$data['ConfirmedAppointment'] = '0%';	
			}
			else
			{
				$data['ConfirmedAppointment'] = number_format(($ConfirmedAppointment/$TotalAppointment)*100,2).'%';	
			}
			
			if($TotalAppointment == 0)
			{
				$data['ShownAppointment'] = '0%';	
			}
			else
			{
				$data['ShownAppointment'] = number_format(($ShownAppointment/$TotalAppointment)*100,2).'%';	
			}
			
			if($TotalAppointment == 0)
			{
				$data['CanceledAppointment'] = '0%';	
			}
			else
			{
				$data['CanceledAppointment'] = number_format(($CanceledAppointment/$TotalAppointment)*100,2).'%';	
			}
			
			return \Response::json($data);
		}
		
		public function hidePhoneNumber(Request $request)
		{
			DB::table('facebook_ads_lead_users')->where('id', $request->id)->where('is_deleted', 0)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'phone_number_hide' => $request->phone_number_hide]);
		}
	}																																																							