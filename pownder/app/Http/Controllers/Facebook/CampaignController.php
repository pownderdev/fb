<?php
	namespace App\Http\Controllers\Facebook;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	
	use DB;
	use Session;
	use AdminHelper;
	use ManagerHelper;
	use ConsoleHelper;
	use Helper;
	
	class CampaignController extends Controller
	{
		public function getCampaign(Request $request)
		{
			$WritePermission = 'Yes';
			$LeadList = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$WritePermission = ManagerHelper::ManagerWritePermission('Campaigns Read Write');
				if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
				{
					$LeadList = 'Yes';
				}
				else
				{
					$LeadList = 'No';
				}
			}
			
			$CampaignColumns=array();
			$Columns = DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Campaign Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$CampaignColumns[]=$Column->column_value;
			}
			
			return view('panel.frontend.campaigns',['CampaignColumns' => $CampaignColumns, 'WritePermission' => $WritePermission, 'LeadList' => $LeadList]);	
		}
		
		public function getCampaignAjax(Request $request)
		{
			$UserArray = array();
			$WritePermission = 'Yes';
			$LeadList = 'Yes';
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				$WritePermission = ManagerHelper::ManagerWritePermission('Campaigns Read Write');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
				{
					$LeadList = 'Yes';
				}
				else
				{
					$LeadList = 'No';
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			
			$order = isset($request->order)?$request->order:'DESC';
			$sortString = isset($request->sort)?$request->sort:'Campaign ID';
			$search = isset($request->search)?$request->search:'';
			$client_id = isset($request->client_id)?$request->client_id:'';
			$offset = isset($request->offset)?$request->offset:'0';
			$limit = isset($request->limit)?$request->limit:'9999999999';
			$status = isset($request->status)?$request->status:'0';
			
			if($limit == 'All' )
			{
				$limit = 9999999999;
			}
			
			$start_date = '';
			$end_date = '';
			$dateRange = isset($request->dateRange)?$request->dateRange:'';
			if($dateRange != '')
			{
				$explodeDate = explode(' to ',$request->dateRange);
				
				$start_date = $explodeDate[0];
				$end_date = $explodeDate[1];
			}
			
			switch($sortString) 
			{
				case 'Date':
				$sort = 'campaigns.created_at';
				break;
				case 'Clients':
				$sort = 'clients.name';
				break;
				case 'Campaign Name':
				$sort = 'campaigns.custom_campaign_name';
				break;
				case 'CRM':
				$sort = 'campaigns.crm_account';
				break;
				default:
				$sort = 'campaigns.id';
			}
			
			$data = array();
			$rows = array();
			
			$columns = ['campaigns.created_at', 'campaigns.custom_campaign_name', 'campaigns.crm_account', 'clients.name'];
			
			if(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
			{
				if(Session::get('user_category')=='vendor')
				{
					$vendor_id = Session::get('vendor_id');
					$vendor_user_id = Session::get('user_id');
				}
				else
				{
					$manager = DB::table('managers')->select('vendor_id')->where('id',Session::get('manager_id'))->first();
					$vendor_id = $manager->vendor_id;
					
					$user = DB::table('users')->select('id')->where('vendor_id',$manager->vendor_id)->get();
					$vendor_user_id = $user->id;
				}
				
				$ClientArray = array();
				$Clients = DB::table('clients')->select('id')->where('vendor_id', $vendor_id)->where('status', 1)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
				foreach($Clients as $Client)
				{
					$ClientArray[]=$Client->id;
				}
				
				$TotalCampaigns = DB::table('campaigns')
				->leftJoin('clients', 'campaigns.client_id', '=', 'clients.id')
				->select('campaigns.*', 'clients.name as client_name')
				->where('clients.is_deleted', 0)
				//->where('clients.status', 1)
				->where('campaigns.is_deleted', 0)
				->where(function ($query) use($ClientArray, $UserArray) {
					$query->whereIn('campaigns.client_id', $ClientArray)
					->whereIn('campaigns.created_user_id', $UserArray, 'or');
				})
				->where(function ($query) use($search, $columns){
					if($search!='')
					{
						$query->where('campaigns.id', 0);
						foreach($columns as $column)
						{
							$query->orWhere($column, 'like', '%'.$search.'%');
						}
					}
				})
				->where(function ($query) use($start_date, $end_date){
					if($start_date!='' && $end_date!='')
					{
						if($start_date==$end_date)
						{
							$query->whereDate('campaigns.created_at', '=', $start_date);
						}
						else
						{
							$query->whereDate('campaigns.created_at', '>=', $start_date)
							->whereDate('campaigns.created_at', '<=', $end_date);
						}
					}
				})
				->where(function ($query) use($client_id){
					if($client_id > 0)
					{
						$query->where('campaigns.client_id', $client_id);
					}
				})
				->where('campaigns.is_active', $status)
				->where('campaigns.is_deleted', 0)
				->get();
				
				$Campaigns = DB::table('campaigns')
				->leftJoin('clients', 'campaigns.client_id', '=', 'clients.id')
				->select('campaigns.*', 'clients.name as client_name')
				->where('clients.is_deleted', 0)
				//->where('clients.status', 1)
				->where('campaigns.is_deleted', 0)
				->where(function ($query) use($ClientArray, $UserArray) {
					$query->whereIn('campaigns.client_id', $ClientArray)
					->whereIn('campaigns.created_user_id', $UserArray, 'or');
				})
				->where(function ($query) use($search, $columns){
					if($search!='')
					{
						$query->where('campaigns.id', 0);
						foreach($columns as $column)
						{
							$query->orWhere($column, 'like', '%'.$search.'%');
						}
					}
				})
				->where(function ($query) use($start_date, $end_date){
					if($start_date!='' && $end_date!='')
					{
						if($start_date==$end_date)
						{
							$query->whereDate('campaigns.created_at', '=', $start_date);
						}
						else
						{
							$query->whereDate('campaigns.created_at', '>=', $start_date)
							->whereDate('campaigns.created_at', '<=', $end_date);
						}
					}
				})
				->where(function ($query) use($client_id){
					if($client_id > 0)
					{
						$query->where('campaigns.client_id', $client_id);
					}
				})
				->where('campaigns.is_active', $status)
				->where('campaigns.is_deleted', 0)
				->orderBy($sort, $order)
				->skip($offset)
				->take($limit)
				->get();
				
			}
			else
			{
				$TotalCampaigns = DB::table('campaigns')
				->leftJoin('clients', 'campaigns.client_id', '=', 'clients.id')
				->select('campaigns.*', 'clients.name as client_name')
				->where('clients.is_deleted', 0)
				//->where('clients.status', 1)
				->where('campaigns.is_deleted', 0)
				->where(function ($query) use($search, $columns){
					if($search!='')
					{
						$query->where('campaigns.id', 0);
						foreach($columns as $column)
						{
							$query->orWhere($column, 'like', '%'.$search.'%');
						}
					}
				})
				->where(function ($query) use($start_date, $end_date){
					if($start_date!='' && $end_date!='')
					{
						if($start_date==$end_date)
						{
							$query->whereDate('campaigns.created_at', '=', $start_date);
						}
						else
						{
							$query->whereDate('campaigns.created_at', '>=', $start_date)
							->whereDate('campaigns.created_at', '<=', $end_date);
						}
					}
				})
				->where(function ($query) use($client_id){
					if($client_id > 0)
					{
						$query->where('campaigns.client_id', $client_id);
					}
				})
				->where('campaigns.is_active', $status)
				->where('campaigns.is_deleted', 0)
				->get();
				
				$Campaigns = DB::table('campaigns')
				->leftJoin('clients', 'campaigns.client_id', '=', 'clients.id')
				->select('campaigns.*', 'clients.name as client_name')
				->where('clients.is_deleted', 0)
				//->where('clients.status', 1)
				->where('campaigns.is_deleted', 0)
				->where(function ($query) use($search, $columns){
					if($search!='')
					{
						$query->where('campaigns.id', 0);
						foreach($columns as $column)
						{
							$query->orWhere($column, 'like', '%'.$search.'%');
						}
					}
				})
				->where(function ($query) use($start_date, $end_date){
					if($start_date!='' && $end_date!='')
					{
						if($start_date==$end_date)
						{
							$query->whereDate('campaigns.created_at', '=', $start_date);
						}
						else
						{
							$query->whereDate('campaigns.created_at', '>=', $start_date)
							->whereDate('campaigns.created_at', '<=', $end_date);
						}
					}
				})
				->where(function ($query) use($client_id){
					if($client_id > 0)
					{
						$query->where('campaigns.client_id', $client_id);
					}
				})
				->where('campaigns.is_active', $status)
				->where('campaigns.is_deleted', 0)
				->orderBy($sort, $order)
				->skip($offset)
				->take($limit)
				->get();
			}
			
			foreach($Campaigns as $Campaign)
			{
				$user = array();
				$user[''] = '<input type="checkbox" class="campaign_id" value="'.$Campaign->id.'" />';
				$user['Date'] = date('m/d/Y',strtotime($Campaign->created_at));
				$user['Clients'] = $Campaign->client_name;
				$user['Campaign Name'] = $Campaign->custom_campaign_name;
				
				$user['CRM'] = $Campaign->crm_account;
				$user['Leads'] = '';
				
				if($LeadList=='Yes')
				{
					if(Helper::CampaignTotalContact($Campaign->id)>0)
					{
						$user['Leads'] = '<a href="campaign_lead/'.$Campaign->id.'">'.Helper::CampaignTotalContact($Campaign->id).'</a>';
					} 
				}
				else 
				{
					if(Helper::CampaignTotalContact($Campaign->id)>0)
					{
						$user['Leads'] = Helper::CampaignTotalContact($Campaign->id);
					}
				}
				$user['Last Lead'] = AdminHelper::CampaignLastLead($Campaign->id);
				
				if($Campaign->is_active==0)
				{
					$checked = 'checked'; 
				}
				else
				{
					$checked = '';
				}
				
				$ActionString = '<center><a href="campaign_edit/'.$Campaign->id.'" class="active_client" title="Edit"><i class="fa fa-pencil fa-lg" aria-hidden="true"></i></a> <div class="switch1" style="display: inline-flex;"><input  id="cmn-toggle-'.$Campaign->id.'" type="checkbox" value="'.$Campaign->id.'" class="cmn-toggle cmn-toggle-round" '.$checked.'><label for="cmn-toggle-'.$Campaign->id.'"></label></div></center>';
				
				$user['Action'] = $ActionString;
				$rows[] = $user;
			}
			$data['total'] = count($TotalCampaigns);
			$data['rows'] = $rows;
			
			return \Response::json($data);
		}
		
		public function getAddCampaign(Request $request)
		{
			$UserArray = array();
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				ManagerHelper::ManagerMenuAction('Campaigns Read Write');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			
			$Clients=array();
			$ServiceProvider=array();
			
			$FacebookPages = DB::table('facebook_pages')->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$Vendors = DB::table('users')->where('category', 'vendor')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
			{
				if(Session::get('user_category')=='admin')
				{
					$admin_id = Session::get('admin_id');
				}
				else
				{
					$manager = DB::table('managers')->select('admin_id')->where('id',Session::get('manager_id'))->first();
					$admin_id = $manager->admin_id;
				}
				$ServiceProvider = DB::table('admins')->where('id',$admin_id)->first();
				$Clients = DB::table('clients')->where('vendor_id', '=', 0)->where('status', 1)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			}
			else if(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
			{
				if(Session::get('user_category')=='vendor')
				{
					$vendor_id = Session::get('vendor_id');
				}
				else
				{
					$manager = DB::table('managers')->select('vendor_id')->where('id',Session::get('manager_id'))->first();
					$vendor_id = $manager->vendor_id;
				}
				
				$ServiceProvider = DB::table('vendors')->where('id', $vendor_id)->first();
				$Clients = DB::table('clients')->where('vendor_id', $vendor_id)->where('status', 1)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			}
			
			return view('panel.frontend.campaign_add',['FacebookPages' => $FacebookPages, 'Clients' => $Clients, 'Vendors' => $Vendors, 'ServiceProvider' => $ServiceProvider]);	
		}
		
		public function postAddCampaign(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Campaigns Read Write');
			}
			
			if($request->crm_account == 'Pownder™')
			{
				$rule = [
				'facebook_page' => 'required',
				'facebook_ad_form' => 'required',
				'custom_campaign_name' => 'required',
				'lead_sync' => 'required',
				'client_id' => 'required',
				'client_name' => 'required',
				'client_contact' => 'required',
				'from' => 'email_array',
				'crm_email' => 'email_array',
				'cc' => 'email_array',
				'bcc' => 'email_array',
				'email_lead_notification' => 'email_array',
				'provider_name' => 'required',
				'provider_id' => 'required',
				'name_of_service' => 'required',
				'email' => 'required|email',
				'phone' => 'required|phone_format',
				'url' => 'required|url',
				'prospect_id_source' => 'required'
				];
			}
			else
			{
				$rule = [
				'facebook_page' => 'required',
				'facebook_ad_form' => 'required',
				'custom_campaign_name' => 'required',
				'lead_sync' => 'required',
				'client_id' => 'required',
				'client_name' => 'required',
				'client_contact' => 'required',
				'from' => 'required|email',
				'crm_email' => 'required|email_array',
				'cc' => 'email_array',
				'bcc' => 'email_array',
				'subject' => 'required',
				'email_lead_notification' => 'email_array',
				'provider_name' => 'required',
				'provider_id' => 'required',
				'name_of_service' => 'required',
				'email' => 'required|email',
				'phone' => 'required|phone_format',
				'url' => 'required|url',
				'prospect_id_source' => 'required'
				];
			}
			
			$this->validate($request,$rule);
			
			if(count($request->manual_lead_type)>0)
			{
				$manual_lead_type = implode(",", $request->manual_lead_type);
			}
			else
			{
				$manual_lead_type = '';
			}
			
			if(count($request->manager_id) > 0)
			{
				$manager_id = implode(",", $request->manager_id);
				DB::table('managers')->whereIn('id', $request->manager_id)
				->update(['lead_notifier' => 'On', 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			else
			{
				$manager_id = '';
			}
			
			$FormArray = array();
			if(count($request->facebook_ad_form) > 0)
			{
				$facebook_ads_lead_id = implode(",", $request->facebook_ad_form);
			}
			
			$facebook_page_id = '';
			if(count($request->facebook_page) > 0)
			{
				$facebook_page_id = implode(",", $request->facebook_page);
			}
			
			$client = DB::table('clients')->select('facebook_ads_lead_id')->where('id',$request->client_name)->first();
			if($client->facebook_ads_lead_id != '')
			{
				$ClientForm = explode(",",$client->facebook_ads_lead_id);
				$FormArray = array_merge($FormArray, $ClientForm);
			}
			
			$client_groups = DB::table('client_groups')
			->leftJoin('groups', 'client_groups.group_id', '=', 'groups.id')
			->select('groups.facebook_ads_lead_id')
			->where('groups.facebook_ads_lead_id', '!=', '')
			->where('client_groups.client_id', $request->client_name)
			->where('client_groups.is_deleted', 0)
			->where('groups.is_deleted', 0)
			->get();
			foreach($client_groups as $client_group)
			{
				$GroupForm = explode(",",$client_group->facebook_ads_lead_id);
				$FormArray = array_merge($FormArray,$GroupForm);
			}
			
			$FormArray = array_unique($FormArray);
			
			$result = array_intersect($FormArray, $request->facebook_ad_form);
			
			if(count($result) == count($request->facebook_ad_form))
			{
				$exists = DB::table('campaigns')->where('client_id', $request->client_name)->where('is_deleted',0)->get();
				if(count($exists) == 0)
				{
					$group_clients = DB::table('client_groups')
					->leftJoin('groups', 'client_groups.group_id', '=', 'groups.id')
					->select('facebook_ads_lead_id')
					->where('groups.facebook_ads_lead_id', '!=', '')
					->where('client_groups.client_id', $request->client_name)
					->where('client_groups.is_deleted', 0)
					->where('groups.is_deleted', 0)
					->get();
					foreach($group_clients as $group_client)
					{
						$facebook_ads_lead_id =  $facebook_ads_lead_id.','.$group_client->facebook_ads_lead_id;
						$GroupForm = explode(",", $group_client->facebook_ads_lead_id);
						
						$facebook_ads_leads = DB::table('facebook_ads_leads')->select('facebook_page_id')->whereIn('id', $GroupForm)->groupBy('facebook_page_id')->where('is_blocked', 0)->where('is_deleted', 0)->get();
						foreach($facebook_ads_leads as $facebook_ads_lead)
						{
							$facebook_page_id =  $facebook_page_id.','.$facebook_ads_lead->facebook_page_id;
						}
					}
					
					$client = DB::table('clients')->select('facebook_ads_lead_id')->where('id', $request->client_name)->first();
					if($client->facebook_ads_lead_id != '')
					{
						$facebook_ads_lead_id =  $facebook_ads_lead_id.','.$client->facebook_ads_lead_id;
						$ClientForm = explode(",", $client->facebook_ads_lead_id);
						
						$facebook_ads_leads = DB::table('facebook_ads_leads')->select('facebook_page_id')->whereIn('id', $ClientForm)->groupBy('facebook_page_id')->where('is_blocked', 0)->where('is_deleted', 0)->get();
						foreach($facebook_ads_leads as $facebook_ads_lead)
						{
							$facebook_page_id =  $facebook_page_id.','.$facebook_ads_lead->facebook_page_id;
						}
					}
				}
				
				$check=DB::table('campaigns')->where('client_id', $request->client_name)->where('crm_account', $request->crm_account)->where('facebook_ads_lead_id', $facebook_ads_lead_id)->where('is_deleted',0)->first();
				if(is_null($check) || $request->crm_account == 'Pownder™')
				{
					$campaign_id = DB::table('campaigns')->insertGetId([
					'facebook_page_id' => $facebook_page_id, 
					'manual_lead_type' => $manual_lead_type,
					'manager_id' => $manager_id,
					'facebook_ads_lead_id' => $facebook_ads_lead_id,
					'custom_campaign_name' => $request->custom_campaign_name, 
					'lead_sync' => $request->lead_sync,
					'crm_account' => $request->crm_account, 
					'client_user_id' => $request->client_id, 
					'client_id' => $request->client_name, 
					'client_contact' => $request->client_contact, 
					'email_from' => $request->from, 
					'email_crm' => $request->crm_email, 
					'email_cc' => $request->cc, 
					'email_bcc' => $request->bcc, 
					'email_subject' => $request->subject, 
					'lead_notification' => $request->lead_notification, 
					'email_lead_notification' => $request->email_lead_notification, 
					'service_provider_name' => $request->provider_name, 
					'service_provider_id' => $request->provider_id, 
					'service_name' => $request->name_of_service, 
					'service_provider_email' => $request->email, 
					'service_provider_phone' => $request->phone, 
					'service_provider_url' => $request->url, 
					'service_provider_source' => $request->prospect_id_source, 
					'user_notification' => $request->user_notification, 
					'page_engagement' => $request->page_engagement, 
					'notification' => isset($request->notification) ? $request->notification : '',
					'created_user_id' => Session::get('user_id'),
					'created_at' => date('Y-m-d H:i:s'),
					'active_user_id' => Session::get('user_id'),
					'active_at' => date('Y-m-d H:i:s'),
					'updated_user_id' => Session::get('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
					]);
					
					if($campaign_id>0)
					{
						DB::table('campaign_logs')->insert([
						'campaign_id' => $campaign_id,
						'facebook_page_id' => $facebook_page_id,
						'manual_lead_type' => $manual_lead_type,
						'manager_id' => $manager_id,
						'facebook_ads_lead_id' => $facebook_ads_lead_id,
						'custom_campaign_name' => $request->custom_campaign_name, 
						'lead_sync' => $request->lead_sync,
						'crm_account' => $request->crm_account, 
						'client_user_id' => $request->client_id, 
						'client_id' => $request->client_name, 
						'client_contact' => $request->client_contact, 
						'email_from' => $request->from, 
						'email_crm' => $request->crm_email, 
						'email_cc' => $request->cc, 
						'email_bcc' => $request->bcc, 
						'email_subject' => $request->subject, 
						'lead_notification' => $request->lead_notification, 
						'email_lead_notification' => $request->email_lead_notification, 
						'service_provider_name' => $request->provider_name, 
						'service_provider_id' => $request->provider_id, 
						'service_name' => $request->name_of_service, 
						'service_provider_email' => $request->email, 
						'service_provider_phone' => $request->phone, 
						'service_provider_url' => $request->url, 
						'service_provider_source' => $request->prospect_id_source, 
						'user_notification' => $request->user_notification, 
						'page_engagement' => $request->page_engagement, 
						'notification' => isset($request->notification) ? $request->notification : '',
						'created_user_id' => Session::get('user_id'),
						'created_at' => date('Y-m-d H:i:s'),
						'active_user_id' => Session::get('user_id'),
						'active_at' => date('Y-m-d H:i:s'),
						'updated_user_id' => Session::get('user_id'),
						'updated_at' => date('Y-m-d H:i:s')
						]);
						return "success";
					}
					else
					{
						return "Please try again";
					}
				}
				else
				{
					return "Campaign Already exists";
				}
			}
			else
			{
				return "Facebook ads form not assign to client";
			}
		}
		
		public function postAddCampaign1(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Campaigns Read Write');
			}
			
			$rule=[
			'facebook_page' => 'required',
			'facebook_ad_form' => 'required',
			'custom_campaign_name' => 'required',
			'lead_sync' => 'required',
			'client_id' => 'required',
			'client_name' => 'required',
			'client_contact' => 'required',
			'from' => 'required|email',
			'crm_email' => 'required|email_array',
			'cc' => 'email_array',
			'bcc' => 'email_array',
			'subject' => 'required',
			'email_lead_notification' => 'email_array',
			'provider_name' => 'required',
			'provider_id' => 'required',
			'name_of_service' => 'required',
			'email'=>'required|email',
			'phone'=>'required|phone_format',
			'url'=>'required|url',
			'prospect_id_source'=>'required'
			];
			
			$this->validate($request,$rule);
			
			if(count($request->manual_lead_type)>0)
			{
				$manual_lead_type=implode(",",$request->manual_lead_type);
			}
			else
			{
				$manual_lead_type='';
			}
			
			$FormArray=array();
			if(count($request->facebook_ad_form)>0)
			{
				$facebook_ads_lead_id=implode(",",$request->facebook_ad_form);
			}
			
			$facebook_page_id='';
			if(count($request->facebook_page)>0)
			{
				$facebook_page_id=implode(",",$request->facebook_page);
			}
			
			if(count($request->manager_id)>0)
			{
				$manager_id=implode(",",$request->manager_id);
				DB::table('managers')->whereIn('id', $request->manager_id)
				->update(['lead_notifier' => 'On', 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			else
			{
				$manager_id='';
			}
			
			$client=DB::table('clients')->select('facebook_ads_lead_id')->where('id',$request->client_name)->first();
			if($client->facebook_ads_lead_id!='')
			{
				$ClientForm = explode(",",$client->facebook_ads_lead_id);
				$FormArray = array_merge($FormArray,$ClientForm);
			}
			
			$client_groups=DB::table('client_groups')
			->leftJoin('groups', 'client_groups.group_id', '=', 'groups.id')
			->select('groups.facebook_ads_lead_id')
			->where('groups.facebook_ads_lead_id', '!=', '')
			->where('client_groups.client_id', $request->client_name)
			->where('client_groups.is_deleted', 0)
			->where('groups.is_deleted', 0)
			->get();
			foreach($client_groups as $client_group)
			{
				$GroupForm = explode(",",$client_group->facebook_ads_lead_id);
				$FormArray = array_merge($FormArray,$GroupForm);
			}
			
			$FormArray=array_unique($FormArray);
			
			$result=array_intersect($FormArray,$request->facebook_ad_form);
			
			$data=array();
			
			if(count($result)==count($request->facebook_ad_form))
			{
				$exists = DB::table('campaigns')->where('client_id', $request->client_name)->where('is_deleted',0)->get();
				if(count($exists) == 0)
				{
					$group_clients=DB::table('client_groups')
					->leftJoin('groups', 'client_groups.group_id', '=', 'groups.id')
					->select('facebook_ads_lead_id')
					->where('groups.facebook_ads_lead_id', '!=', '')
					->where('client_groups.client_id', $request->client_name)
					->where('client_groups.is_deleted', 0)
					->where('groups.is_deleted', 0)
					->get();
					foreach($group_clients as $group_client)
					{
						$facebook_ads_lead_id =  $facebook_ads_lead_id.','.$group_client->facebook_ads_lead_id;
						$GroupForm = explode(",", $group_client->facebook_ads_lead_id);
						
						$facebook_ads_leads = DB::table('facebook_ads_leads')->select('facebook_page_id')->whereIn('id', $GroupForm)->groupBy('facebook_page_id')->where('is_blocked', 0)->where('is_deleted', 0)->get();
						foreach($facebook_ads_leads as $facebook_ads_lead)
						{
							$facebook_page_id =  $facebook_page_id.','.$facebook_ads_lead->facebook_page_id;
						}
					}
					
					$client = DB::table('clients')->select('facebook_ads_lead_id')->where('id', $request->client_name)->first();
					if($client->facebook_ads_lead_id != '')
					{
						$facebook_ads_lead_id =  $facebook_ads_lead_id.','.$client->facebook_ads_lead_id;
						$ClientForm = explode(",", $client->facebook_ads_lead_id);
						
						$facebook_ads_leads = DB::table('facebook_ads_leads')->select('facebook_page_id')->whereIn('id', $ClientForm)->groupBy('facebook_page_id')->where('is_blocked', 0)->where('is_deleted', 0)->get();
						foreach($facebook_ads_leads as $facebook_ads_lead)
						{
							$facebook_page_id =  $facebook_page_id.','.$facebook_ads_lead->facebook_page_id;
						}
					}
				}
				
				$check = DB::table('campaigns')->where('client_id', $request->client_name)->where('crm_account',$request->crm_account)->where('facebook_ads_lead_id', $facebook_ads_lead_id)->where('is_deleted',0)->first();
				if(is_null($check))
				{
					$campaign_id = DB::table('campaigns')->insertGetId([
					'facebook_page_id' => $facebook_page_id, 
					'manual_lead_type' => $manual_lead_type,
					'manager_id' => $manager_id,
					'facebook_ads_lead_id' => $facebook_ads_lead_id,
					'custom_campaign_name' => $request->custom_campaign_name, 
					'lead_sync' => $request->lead_sync,
					'crm_account' => $request->crm_account, 
					'client_user_id' => $request->client_id, 
					'client_id' => $request->client_name, 
					'client_contact' => $request->client_contact, 
					'email_from' => $request->from, 
					'email_crm' => $request->crm_email, 
					'email_cc' => $request->cc, 
					'email_bcc' => $request->bcc, 
					'email_subject' => $request->subject, 
					'lead_notification' => $request->lead_notification, 
					'email_lead_notification' => $request->email_lead_notification, 
					'service_provider_name' => $request->provider_name, 
					'service_provider_id' => $request->provider_id, 
					'service_name' => $request->name_of_service, 
					'service_provider_email' => $request->email, 
					'service_provider_phone' => $request->phone, 
					'service_provider_url' => $request->url, 
					'service_provider_source' => $request->prospect_id_source, 
					'user_notification' => $request->user_notification, 
					'page_engagement' => $request->page_engagement, 
					'notification' => isset($request->notification) ? $request->notification : '', 
					'created_user_id' => Session::get('user_id'),
					'created_at' => date('Y-m-d H:i:s'),
					'active_user_id' => Session::get('user_id'),
					'active_at' => date('Y-m-d H:i:s'),
					'updated_user_id' => Session::get('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
					]);
					
					if($campaign_id>0)
					{
						DB::table('campaign_logs')->insert([
						'campaign_id' => $campaign_id, 
						'facebook_page_id' => $facebook_page_id, 
						'manual_lead_type' => $manual_lead_type,
						'manager_id' => $manager_id,
						'facebook_ads_lead_id' => $facebook_ads_lead_id,
						'custom_campaign_name' => $request->custom_campaign_name, 
						'lead_sync' => $request->lead_sync,
						'crm_account' => $request->crm_account, 
						'client_user_id' => $request->client_id, 
						'client_id' => $request->client_name, 
						'client_contact' => $request->client_contact, 
						'email_from' => $request->from, 
						'email_crm' => $request->crm_email, 
						'email_cc' => $request->cc, 
						'email_bcc' => $request->bcc, 
						'email_subject' => $request->subject, 
						'lead_notification' => $request->lead_notification, 
						'email_lead_notification' => $request->email_lead_notification, 
						'service_provider_name' => $request->provider_name, 
						'service_provider_id' => $request->provider_id, 
						'service_name' => $request->name_of_service, 
						'service_provider_email' => $request->email, 
						'service_provider_phone' => $request->phone, 
						'service_provider_url' => $request->url, 
						'service_provider_source' => $request->prospect_id_source, 
						'user_notification' => $request->user_notification, 
						'page_engagement' => $request->page_engagement, 
						'notification' => isset($request->notification) ? $request->notification : '',
						'created_user_id' => Session::get('user_id'),
						'created_at' => date('Y-m-d H:i:s'),
						'active_user_id' => Session::get('user_id'),
						'active_at' => date('Y-m-d H:i:s'),
						'updated_user_id' => Session::get('user_id'),
						'updated_at' => date('Y-m-d H:i:s')
						]);
						
						$data['msg'] = "success";
						$data['campaign_id'] = $campaign_id;
						
						return response()->json($data);
					}
					else
					{
						$data['msg'] = "Please try again";
						$data['campaign_id'] = 0;
						
						return response()->json($data);
					}
				}
				else
				{
					$data['msg'] = "Campaign Already exists";
					$data['campaign_id'] = 0;
					
					return response()->json($data);
				}
			}
			else
			{
				$data['msg'] = "Facebook ads form not assign to client";
				$data['campaign_id'] = 0;
				
				return response()->json($data);
			}
		}
		
		public function getEditCampaign($id)
		{
			$UserArray = array();
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				ManagerHelper::ManagerMenuAction('Campaigns Read Write');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			
			$Campaign = DB::table('campaigns')->where('id', $id)->whereIn('created_user_id', $UserArray)->first();
			
			if(is_null($Campaign))
			{
				return redirect('campaigns')->with('alert_danger','Unauthorized campaign access failed');
			}
			
			$FacebookPages = DB::table('facebook_pages')->whereIn('updated_user_id', $UserArray)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$FacebookAdsLeads = DB::table('facebook_ads_leads')->whereIn('facebook_page_id', explode(",", $Campaign->facebook_page_id))->where('status', 'ACTIVE')->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			$managers = DB::table('managers')->select('id', 'full_name')->where('client_id', $Campaign->client_id)->where('status', '1')->where('is_deleted', 0)->orderBy('full_name', 'ASC')->get();
			
			if(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
			{
				if(Session::get('user_category')=='vendor')
				{
					$vendor_id = Session::get('vendor_id');
				}
				else
				{
					$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
					$user = DB::table('users')->select('vendor_id')->where('id',$manager->created_user_id)->first();
					$vendor_id = $user->vendor_id;
				}
				
				$ClientArray = array();
				$Clients = DB::table('clients')->select('id')->where(function ($query) use ($UserArray, $vendor_id) {
					$query->where('vendor_id', $vendor_id)
					->whereIn('created_user_id', $UserArray, 'or');
				})->where('status', 1)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
				foreach($Clients as $Client)
				{
					$ClientArray[]=$Client->id;
				}
				
				$Campaigns=DB::table('campaigns')
				->leftJoin('clients', 'campaigns.client_id', '=', 'clients.id')
				->select('campaigns.*', 'clients.name as client_name')
				->where('clients.is_deleted', 0)
				//->where('clients.status', 1)
				->where('campaigns.is_deleted', 0)
				->where(function ($query) use($ClientArray, $UserArray) {
					$query->whereIn('campaigns.client_id', $ClientArray)
					->whereIn('campaigns.created_user_id', $UserArray, 'or');
				})
				->orderBy('campaigns.id', 'DESC')
				->get();
			}
			else
			{
				$Campaigns=DB::table('campaigns')
				->leftJoin('clients', 'campaigns.client_id', '=', 'clients.id')
				->select('campaigns.*', 'clients.name as client_name')
				->where('clients.is_deleted', 0)
				//->where('clients.status', 1)
				->where('campaigns.is_deleted', 0)
				->whereIn('campaigns.created_user_id', $UserArray)
				->orderBy('campaigns.id', 'DESC')
				->get();
			}
			
			if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
			{
				if(Session::get('user_category')=='admin')
				{
					$admin_id = Session::get('admin_id');
				}
				else
				{
					$manager = DB::table('managers')->select('admin_id')->where('id',Session::get('manager_id'))->first();
					$admin_id = $manager->admin_id;
				}
				$ServiceProvider = DB::table('admins')->where('id', $admin_id)->first();
				$Clients = DB::table('clients')->where('vendor_id', 0)->where('status', 1)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			}
			else if(Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
			{
				if(Session::get('user_category')=='vendor')
				{
					$vendor_id = Session::get('vendor_id');
				}
				else
				{
					$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
					$user = DB::table('users')->select('vendor_id')->where('id',$manager->created_user_id)->first();
					$vendor_id = $user->vendor_id;
				}
				
				$ServiceProvider=DB::table('vendors')->where('id',$vendor_id)->first();
				$Clients = DB::table('clients')->where(function ($query) use ($vendor_id, $UserArray) {
					$query->where('vendor_id', $vendor_id)
					->whereIn('created_user_id', $UserArray, 'or');
				})->where('status', 1)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			}
			
			$Vendors = DB::table('users')->where('category', 'vendor')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return view('panel.frontend.campaign_edit',['FacebookPages' => $FacebookPages, 'Clients' => $Clients, 'Vendors' => $Vendors, 'managers' => $managers, 'FacebookAdsLeads' => $FacebookAdsLeads, 'Campaign' => $Campaign]);	
		}
		
		public function postEditCampaign(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Campaigns Read Write');
			}
			
			if($request->crm_account == 'Pownder™')
			{
				$rule=[
				'facebook_page' => 'required',
				'facebook_ad_form' => 'required',
				'custom_campaign_name' => 'required',
				'lead_sync' => 'required',
				'client_id' => 'required',
				'client_name' => 'required',
				'client_contact' => 'required',
				'from' => 'email_array',
				'crm_email' => 'email_array',
				'cc' => 'email_array',
				'bcc' => 'email_array',
				'email_lead_notification' => 'email_array',
				'provider_name' => 'required',
				'provider_id' => 'required',
				'name_of_service' => 'required',
				'email'=>'required|email',
				'phone'=>'required|phone_format',
				'url'=>'required|url',
				'prospect_id_source'=>'required'
				];
			}
			else
			{
				$rule=[
				'facebook_page' => 'required',
				'facebook_ad_form' => 'required',
				'custom_campaign_name' => 'required',
				'lead_sync' => 'required',
				'client_id' => 'required',
				'client_name' => 'required',
				'client_contact' => 'required',
				'from' => 'required|email',
				'crm_email' => 'required|email_array',
				'cc' => 'email_array',
				'bcc' => 'email_array',
				'subject' => 'required',
				'email_lead_notification' => 'email_array',
				'provider_name' => 'required',
				'provider_id' => 'required',
				'name_of_service' => 'required',
				'email'=>'required|email',
				'phone'=>'required|phone_format',
				'url'=>'required|url',
				'prospect_id_source'=>'required'
				];
			}
			
			$this->validate($request,$rule);
			
			if(count($request->manual_lead_type)>0)
			{
				$manual_lead_type=implode(",",$request->manual_lead_type);
			}
			else
			{
				$manual_lead_type='';
			}
			
			if(count($request->manager_id)>0)
			{
				$manager_id=implode(",",$request->manager_id);
				DB::table('managers')->whereIn('id', $request->manager_id)
				->update(['lead_notifier' => 'On', 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			else
			{
				$manager_id='';
			}
			
			$FormArray=array();
			if(count($request->facebook_ad_form)>0)
			{
				$facebook_ads_lead_id=implode(",",$request->facebook_ad_form);
			}
			
			$facebook_page_id='';
			if(count($request->facebook_page)>0)
			{
				$facebook_page_id=implode(",",$request->facebook_page);
			}
			
			$client=DB::table('clients')->select('facebook_ads_lead_id')->where('id',$request->client_name)->first();
			if($client->facebook_ads_lead_id!='')
			{
				$ClientForm = explode(",",$client->facebook_ads_lead_id);
				$FormArray = array_merge($FormArray,$ClientForm);
			}
			
			$client_groups=DB::table('client_groups')
			->leftJoin('groups', 'client_groups.group_id', '=', 'groups.id')
			->select('groups.facebook_ads_lead_id')
			->where('groups.facebook_ads_lead_id', '!=', '')
			->where('client_groups.client_id', $request->client_name)
			->where('client_groups.is_deleted', 0)
			->where('groups.is_deleted', 0)
			->get();
			foreach($client_groups as $client_group)
			{
				$GroupForm = explode(",",$client_group->facebook_ads_lead_id);
				$FormArray = array_merge($FormArray,$GroupForm);
			}
			
			$FormArray=array_unique($FormArray);
			
			$result=array_intersect($FormArray,$request->facebook_ad_form);
			
			if(count($result)==count($request->facebook_ad_form))
			{
				$ClientCheck = DB::table('campaigns')->where('id', $request->id)->first();
				if($request->client_name != $ClientCheck->client_id)
				{
					$exists = DB::table('campaigns')->where('client_id', $request->client_name)->where('is_deleted',0)->get();
					if(count($exists) == 0)
					{
						$group_clients=DB::table('client_groups')
						->leftJoin('groups', 'client_groups.group_id', '=', 'groups.id')
						->select('facebook_ads_lead_id')
						->where('groups.facebook_ads_lead_id', '!=', '')
						->where('client_groups.client_id', $request->client_name)
						->where('client_groups.is_deleted', 0)
						->where('groups.is_deleted', 0)
						->get();
						foreach($group_clients as $group_client)
						{
							$facebook_ads_lead_id =  $facebook_ads_lead_id.','.$group_client->facebook_ads_lead_id;
							$GroupForm = explode(",", $group_client->facebook_ads_lead_id);
							
							$facebook_ads_leads = DB::table('facebook_ads_leads')->select('facebook_page_id')->whereIn('id', $GroupForm)->groupBy('facebook_page_id')->where('is_blocked', 0)->where('is_deleted', 0)->get();
							foreach($facebook_ads_leads as $facebook_ads_lead)
							{
								$facebook_page_id =  $facebook_page_id.','.$facebook_ads_lead->facebook_page_id;
							}
						}
						
						$client = DB::table('clients')->select('facebook_ads_lead_id')->where('id', $request->client_name)->first();
						if($client->facebook_ads_lead_id != '')
						{
							$facebook_ads_lead_id =  $facebook_ads_lead_id.','.$client->facebook_ads_lead_id;
							$ClientForm = explode(",", $client->facebook_ads_lead_id);
							
							$facebook_ads_leads = DB::table('facebook_ads_leads')->select('facebook_page_id')->whereIn('id', $ClientForm)->groupBy('facebook_page_id')->where('is_blocked', 0)->where('is_deleted', 0)->get();
							foreach($facebook_ads_leads as $facebook_ads_lead)
							{
								$facebook_page_id =  $facebook_page_id.','.$facebook_ads_lead->facebook_page_id;
							}
						}
					}
				}
				
				$check=DB::table('campaigns')->where('client_id', $request->client_name)->where('crm_account',$request->crm_account)->where('facebook_ads_lead_id', $facebook_ads_lead_id)->where('id', '!=', $request->id)->where('is_deleted',0)->first();
				if(is_null($check) || $request->crm_account == 'Pownder™')
				{
					DB::table('campaigns')->where('id', $request->id)
					->update([
					'facebook_page_id' => $facebook_page_id, 
					'facebook_ads_lead_id' => $facebook_ads_lead_id, 
					'manual_lead_type' => $manual_lead_type,
					'manager_id' => $manager_id,
					'custom_campaign_name' => $request->custom_campaign_name, 
					'lead_sync' => $request->lead_sync,
					'crm_account' => $request->crm_account, 
					'client_user_id' => $request->client_id, 
					'client_id' => $request->client_name, 
					'client_contact' => $request->client_contact, 
					'email_from' => $request->from, 
					'email_crm' => $request->crm_email, 
					'email_cc' => $request->cc, 
					'email_bcc' => $request->bcc, 
					'email_subject' => $request->subject, 
					'lead_notification' => $request->lead_notification, 
					'email_lead_notification' => $request->email_lead_notification, 
					'service_provider_name' => $request->provider_name, 
					'service_provider_id' => $request->provider_id, 
					'service_name' => $request->name_of_service, 
					'service_provider_email' => $request->email, 
					'service_provider_phone' => $request->phone, 
					'service_provider_url' => $request->url, 
					'service_provider_source' => $request->prospect_id_source, 
					'user_notification' => $request->user_notification, 
					'page_engagement' => $request->page_engagement, 
					'notification' => isset($request->notification) ? $request->notification : '',
					'updated_user_id' => Session::get('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
					]);
					
					DB::table('campaign_logs')->insert([
					'campaign_id' => $request->id,
					'facebook_page_id' => $facebook_page_id,
					'manual_lead_type' => $manual_lead_type,
					'manager_id' => $manager_id,
					'facebook_ads_lead_id' => $facebook_ads_lead_id,
					'custom_campaign_name' => $request->custom_campaign_name, 
					'lead_sync' => $request->lead_sync,
					'crm_account' => $request->crm_account, 
					'client_user_id' => $request->client_id, 
					'client_id' => $request->client_name, 
					'client_contact' => $request->client_contact, 
					'email_from' => $request->from, 
					'email_crm' => $request->crm_email, 
					'email_cc' => $request->cc, 
					'email_bcc' => $request->bcc, 
					'email_subject' => $request->subject, 
					'lead_notification' => $request->lead_notification, 
					'email_lead_notification' => $request->email_lead_notification, 
					'service_provider_name' => $request->provider_name, 
					'service_provider_id' => $request->provider_id, 
					'service_name' => $request->name_of_service, 
					'service_provider_email' => $request->email, 
					'service_provider_phone' => $request->phone, 
					'service_provider_url' => $request->url, 
					'service_provider_source' => $request->prospect_id_source, 
					'user_notification' => $request->user_notification, 
					'page_engagement' => $request->page_engagement, 
					'notification' => isset($request->notification) ? $request->notification : '',
					'created_user_id' => Session::get('user_id'),
					'created_at' => date('Y-m-d H:i:s'),
					'updated_user_id' => Session::get('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
					]);
					
					$Campaign = DB::table('autometive_campaigns')->where('campaign_id', $request->id)->first();
					
					if(!is_null($Campaign))
					{
						DB::table('autometive_campaigns')
						->where('campaign_id', $request->id)
						->update([
						'provider_name' => $request->provider_name,
						'service_name' => $request->name_of_service,
						'provider_email' => $request->email,
						'provider_phone' => $request->phone,
						'provider_url' => $request->url,
						'updated_user_id' => Session::get('user_id'),
						'updated_user_ip' => $request->ip(),
						'updated_at' => date('Y-m-d H:i:s')
						]);
					}
					
					return "success";
				}
				else
				{
					return "Campaign Already exists";
				}
			}
			else
			{
				return "Facebook ads form not assign to client";
			}
		}
		
		public function postEditCampaign1(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Campaigns Read Write');
			}
			
			$rule=[
			'facebook_page' => 'required',
			'facebook_ad_form' => 'required',
			'custom_campaign_name' => 'required',
			'lead_sync' => 'required',
			'client_id' => 'required',
			'client_name' => 'required',
			'client_contact' => 'required',
			'from' => 'required|email',
			'crm_email' => 'required|email_array',
			'cc' => 'email_array',
			'bcc' => 'email_array',
			'subject' => 'required',
			'email_lead_notification' => 'email_array',
			'provider_name' => 'required',
			'provider_id' => 'required',
			'name_of_service' => 'required',
			'email'=>'required|email',
			'phone'=>'required|phone_format',
			'url'=>'required|url',
			'prospect_id_source'=>'required'
			];
			
			$this->validate($request,$rule);
			
			if(count($request->manual_lead_type)>0)
			{
				$manual_lead_type=implode(",",$request->manual_lead_type);
			}
			else
			{
				$manual_lead_type='';
			}
			
			$FormArray=array();
			if(count($request->facebook_ad_form)>0)
			{
				$facebook_ads_lead_id=implode(",",$request->facebook_ad_form);
			}
			
			$facebook_page_id='';
			if(count($request->facebook_page)>0)
			{
				$facebook_page_id=implode(",",$request->facebook_page);
			}
			
			if(count($request->manager_id)>0)
			{
				$manager_id = implode(",",$request->manager_id);
				DB::table('managers')->whereIn('id', $request->manager_id)
				->update(['lead_notifier' => 'On', 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
			}
			else
			{
				$manager_id = '';
			}
			
			$client = DB::table('clients')->select('facebook_ads_lead_id')->where('id',$request->client_name)->first();
			if($client->facebook_ads_lead_id!='')
			{
				$ClientForm = explode(",",$client->facebook_ads_lead_id);
				$FormArray = array_merge($FormArray,$ClientForm);
			}
			
			$client_groups = DB::table('client_groups')
			->leftJoin('groups', 'client_groups.group_id', '=', 'groups.id')
			->select('groups.facebook_ads_lead_id')
			->where('groups.facebook_ads_lead_id', '!=', '')
			->where('client_groups.client_id', $request->client_name)
			->where('client_groups.is_deleted', 0)
			->where('groups.is_deleted', 0)
			->get();
			foreach($client_groups as $client_group)
			{
				$GroupForm = explode(",", $client_group->facebook_ads_lead_id);
				$FormArray = array_merge($FormArray, $GroupForm);
			}
			
			$FormArray = array_unique($FormArray);
			
			$result = array_intersect($FormArray, $request->facebook_ad_form);
			
			$data = array();
			
			if(count($result) == count($request->facebook_ad_form))
			{
				$ClientCheck = DB::table('campaigns')->where('id', $request->id)->first();
				if($request->client_name != $ClientCheck->client_id)
				{
					$exists = DB::table('campaigns')->where('client_id', $request->client_name)->where('is_deleted',0)->get();
					if(count($exists) == 0)
					{
						$group_clients=DB::table('client_groups')
						->leftJoin('groups', 'client_groups.group_id', '=', 'groups.id')
						->select('facebook_ads_lead_id')
						->where('groups.facebook_ads_lead_id', '!=', '')
						->where('client_groups.client_id', $request->client_name)
						->where('client_groups.is_deleted', 0)
						->where('groups.is_deleted', 0)
						->get();
						foreach($group_clients as $group_client)
						{
							$facebook_ads_lead_id =  $facebook_ads_lead_id.','.$group_client->facebook_ads_lead_id;
							$GroupForm = explode(",", $group_client->facebook_ads_lead_id);
							
							$facebook_ads_leads = DB::table('facebook_ads_leads')->select('facebook_page_id')->whereIn('id', $GroupForm)->groupBy('facebook_page_id')->where('is_blocked', 0)->where('is_deleted', 0)->get();
							foreach($facebook_ads_leads as $facebook_ads_lead)
							{
								$facebook_page_id =  $facebook_page_id.','.$facebook_ads_lead->facebook_page_id;
							}
						}
						
						$client = DB::table('clients')->select('facebook_ads_lead_id')->where('id', $request->client_name)->first();
						if($client->facebook_ads_lead_id != '')
						{
							$facebook_ads_lead_id =  $facebook_ads_lead_id.','.$client->facebook_ads_lead_id;
							$ClientForm = explode(",", $client->facebook_ads_lead_id);
							
							$facebook_ads_leads = DB::table('facebook_ads_leads')->select('facebook_page_id')->whereIn('id', $ClientForm)->groupBy('facebook_page_id')->where('is_blocked', 0)->where('is_deleted', 0)->get();
							foreach($facebook_ads_leads as $facebook_ads_lead)
							{
								$facebook_page_id =  $facebook_page_id.','.$facebook_ads_lead->facebook_page_id;
							}
						}
					}
				}
				
				$check = DB::table('campaigns')->where('client_id', $request->client_name)->where('crm_account',$request->crm_account)->where('facebook_ads_lead_id', $facebook_ads_lead_id)->where('id', '!=', $request->id)->where('is_deleted',0)->first();
				if(is_null($check))
				{
					DB::table('campaigns')->where('id', $request->id)
					->update([
					'facebook_page_id' => $facebook_page_id, 
					'facebook_ads_lead_id' => $facebook_ads_lead_id, 
					'manual_lead_type' => $manual_lead_type,
					'manager_id' => $manager_id,
					'custom_campaign_name' => $request->custom_campaign_name,
					'lead_sync' => $request->lead_sync,
					'crm_account' => $request->crm_account, 
					'client_user_id' => $request->client_id, 
					'client_id' => $request->client_name, 
					'client_contact' => $request->client_contact, 
					'email_from' => $request->from, 
					'email_crm' => $request->crm_email, 
					'email_cc' => $request->cc, 
					'email_bcc' => $request->bcc, 
					'email_subject' => $request->subject, 
					'lead_notification' => $request->lead_notification, 
					'email_lead_notification' => $request->email_lead_notification, 
					'service_provider_name' => $request->provider_name, 
					'service_provider_id' => $request->provider_id, 
					'service_name' => $request->name_of_service, 
					'service_provider_email' => $request->email, 
					'service_provider_phone' => $request->phone, 
					'service_provider_url' => $request->url, 
					'service_provider_source' => $request->prospect_id_source, 
					'user_notification' => $request->user_notification,
					'page_engagement' => $request->page_engagement,
					'notification' => isset($request->notification) ? $request->notification : '',
					'updated_user_id' => Session::get('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
					]);
					
					DB::table('campaign_logs')->insert([
					'campaign_id' => $request->id,
					'facebook_page_id' => $facebook_page_id,
					'manual_lead_type' => $manual_lead_type,
					'manager_id' => $manager_id,
					'facebook_ads_lead_id' => $facebook_ads_lead_id,
					'custom_campaign_name' => $request->custom_campaign_name, 
					'lead_sync' => $request->lead_sync,
					'crm_account' => $request->crm_account, 
					'client_user_id' => $request->client_id, 
					'client_id' => $request->client_name, 
					'client_contact' => $request->client_contact, 
					'email_from' => $request->from, 
					'email_crm' => $request->crm_email, 
					'email_cc' => $request->cc, 
					'email_bcc' => $request->bcc, 
					'email_subject' => $request->subject, 
					'lead_notification' => $request->lead_notification, 
					'email_lead_notification' => $request->email_lead_notification, 
					'service_provider_name' => $request->provider_name, 
					'service_provider_id' => $request->provider_id, 
					'service_name' => $request->name_of_service, 
					'service_provider_email' => $request->email, 
					'service_provider_phone' => $request->phone, 
					'service_provider_url' => $request->url, 
					'service_provider_source' => $request->prospect_id_source, 
					'user_notification' => $request->user_notification, 
					'page_engagement' => $request->page_engagement, 
					'notification' => isset($request->notification) ? $request->notification : '',
					'created_user_id' => Session::get('user_id'),
					'created_at' => date('Y-m-d H:i:s'),
					'updated_user_id' => Session::get('user_id'),
					'updated_at' => date('Y-m-d H:i:s')
					]);
					
					$Campaign = DB::table('autometive_campaigns')->where('campaign_id', $request->id)->first();
					
					if(!is_null($Campaign))
					{
						DB::table('autometive_campaigns')->where('campaign_id', $request->id)
						->update(['provider_name' => $request->provider_name, 'service_name' => $request->name_of_service, 'provider_email' => $request->email, 'provider_phone' => $request->phone, 'provider_url' => $request->url, 'updated_user_id' => Session::get('user_id'), 	'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
					}
					
					$data['msg'] = "success";
					$data['campaign_id'] = $request->id;
					
					return response()->json($data);
				}
				else
				{
					$data['msg'] = "Campaign Already exists";
					$data['campaign_id'] = 0;
					
					return response()->json($data);
				}
			}
			else
			{
				$data['msg'] = "Facebook ads form not assign to client";
				$data['campaign_id'] = 0;
				
				return response()->json($data);
			}
		}
		
		public function changeCampaignStatus(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Campaigns Read Write');
			}
			
			DB::table('campaigns')->where('id', $request->id)
			->update(['active_user_id' => Session::get('user_id'), 'active_at' => date('Y-m-d H:i:s'), 'is_active'=>$request->is_active]);
			
			DB::table('campaign_logs')
			->insert(['campaign_id' => $request->id, 'is_active'=>$request->is_active, 'active_user_id' => Session::get('user_id'), 'active_at' => date('Y-m-d H:i:s')]);
		}
		
		public function postCampaignDelete(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Campaigns Read Write');
			}
			
			DB::table('campaigns')->whereIn('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
		}
		
		public function postCampaignClone(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Campaigns Read Write');
			}
			
			$results = DB::table('campaigns')->whereIn('id', $request->id)->get();
			foreach($results as $result)
			{
				DB::table('campaigns')->insert([
				'facebook_page_id' => $result->facebook_page_id, 
				'manual_lead_type' => $result->manual_lead_type,
				'facebook_ads_lead_id' => $result->facebook_ads_lead_id,
				'custom_campaign_name' => $result->custom_campaign_name.' (c)', 
				'crm_account' => $result->crm_account, 
				'client_user_id' => $result->client_user_id, 
				'client_id' => $result->client_id, 
				'client_contact' => $result->client_contact, 
				'email_from' => $result->email_from, 
				'email_crm' => $result->email_crm, 
				'email_cc' => $result->email_cc, 
				'email_bcc' => $result->email_bcc, 
				'email_subject' => $result->email_subject, 
				'lead_notification' => $result->lead_notification, 
				'email_lead_notification' => $result->email_lead_notification, 
				'service_provider_name' => $result->service_provider_name, 
				'service_provider_id' => $result->service_provider_id, 
				'service_name' => $result->service_name, 
				'service_provider_email' => $result->service_provider_email, 
				'service_provider_phone' => $result->service_provider_phone, 
				'service_provider_url' => $result->service_provider_url, 
				'service_provider_source' => $result->service_provider_source,
				'is_clone' => $result->id,
				'is_active' => $result->is_active,
				'created_user_id' => Session::get('user_id'),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_user_id' => Session::get('user_id'),
				'updated_at' => date('Y-m-d H:i:s')
				]);
			}
		}
		
		public function getCampaignLead($campaign_id)
		{
			$WritePermission = 'Yes';
			if(Session::get('user_category') == 'user')
			{
				$WritePermission = ManagerHelper::ManagerWritePermission('Campaigns Read Write');
			}
			
			$CampaignColumns=array();
			
			$Columns = DB::table('table_visible_columns')->select('column_value')->where('table_name', 'Campaign Lead List Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$CampaignColumns[] = $Column->column_value;
			}
			
			$Campaign = DB::table('campaigns')->where('id', $campaign_id)->where('is_deleted', 0)->first();
			
			if(is_null($Campaign))
			{
				return redirect('campaigns')->with('alert_danger','Unauthorized campaign access failed');
			}
			
			$CampaignLeads = DB::table('crm_contacts')
			->leftJoin('facebook_ads_lead_users', 'crm_contacts.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_users.*', 'crm_contacts.created_at as contact_created_at')
			->where('crm_contacts.campaign_id', $campaign_id)
			->orderBy('crm_contacts.id', 'DESC')
			->get();
			
			return view('panel.frontend.campaign_lead_list',['CampaignColumns' => $CampaignColumns, 'Campaign' => $Campaign, 'CampaignLeads' => $CampaignLeads, 'WritePermission' => $WritePermission]);	
		}
		
		public function getCRMAccountLead($crm_account_id)
		{
			$WritePermission = 'Yes';
			if(Session::get('user_category') == 'user')
			{
				ManagerHelper::ManagerMenuAction('Clients Read');
				$WritePermission = ManagerHelper::ManagerWritePermission('Clients Read Write');
			}
			
			$CampaignColumns = array();
			
			$Columns = DB::table('table_visible_columns')->select('column_value')->where('table_name', 'CRM Account Lead List Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$CampaignColumns[] = $Column->column_value;
			}
			
			$CRMccount = DB::table('crm_accounts')->where('id', $crm_account_id)->where('is_deleted', 0)->first();
			
			if(is_null($CRMccount))
			{
				return back()->with('alert_danger','Unauthorized CRM account access failed');
			}
			
			$CampaignLeads = DB::table('crm_contacts')
			->leftJoin('facebook_ads_lead_users', 'crm_contacts.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->select('facebook_ads_lead_users.*', 'crm_contacts.created_at as contact_created_at')
			->where('crm_contacts.crm_account_id', $crm_account_id)
			->orderBy('crm_contacts.id', 'DESC')
			->get();
			
			return view('panel.frontend.crm_contact_list',['CampaignColumns' => $CampaignColumns, 'CRMccount' => $CRMccount, 'CampaignLeads' => $CampaignLeads, 'WritePermission' => $WritePermission]);	
		}
		
		public function getCampaignCRMLead(Request $request)
		{
			if(Session::get('user_category')=='user')
			{
				ManagerHelper::ManagerMenuAction('Clients Read Write');
			}
			
			$CampaignColumns=array();
			
			$Columns = DB::table('table_visible_columns')->select('column_value')->where('table_name', 'CRM Account Lead List Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$CampaignColumns[]=$Column->column_value;
			}
			
			$Campaign = DB::table('campaigns')->where('id', $request->campaign)->first();
			$AutometiveCampaign = DB::table('autometive_campaigns')->where('campaign_id', $request->campaign)->first();
			
			$Client = DB::table('clients')->where('id', $Campaign->client_id)->first();
			
			$User = DB::table('users')->where('id', $Campaign->created_user_id)->first();
			if($User->category == 'admin')
			{
				$ServiceProvider = DB::table('admins')->where('id', $User->admin_id)->first();
			}
			else
			{
				$ServiceProvider = DB::table('vendors')->where('id', $User->vendor_id)->first();
			}
			
			$Leads = DB::table('facebook_ads_lead_users')->where('client_id', $Campaign->client_id)->whereIn('facebook_ads_lead_id', explode(",", $Campaign->facebook_ads_lead_id))->orderBy('created_at', 'DESC')->take(3)->get();
			
			return view('panel.frontend.campaign_crm_lead',['CampaignColumns' => $CampaignColumns, 'Campaign' => $Campaign, 'Client' => $Client, 'ServiceProvider' => $ServiceProvider, 'Leads' => $Leads, 'AutometiveCampaign' => $AutometiveCampaign]);	
		}
		
		public function createTestADFXML(Request $request)
		{
			$campaign_id = $request->campaign_id;
			
			$prospect = $request->prospect;
			$phone_time = $request->phone_time;
			$phone_type = $request->phone_type;
			
			$provider_name = $request->provider_name;
			$service_name = $request->service_name;
			$provider_email = $request->provider_email;
			$provider_phone = $request->provider_phone;
			$provider_url = $request->provider_url;
			$provider_zip = $request->provider_zip;
			$provider_city = $request->provider_city;
			$provider_state = $request->provider_state;
			
			$vendor_name = $request->vendor_name;
			$vendor_email = $request->vendor_email;
			$vendor_phone = $request->vendor_phone;
			$vendor_url = $request->vendor_url;
			$vendor_address = $request->vendor_address;
			$vendor_zip = $request->vendor_zip;
			$vendor_city = $request->vendor_city;
			$vendor_state = $request->vendor_state;
			
			$first_name = '';
			$last_name = '';
			$email = '';
			$phone = '';
			$address = '';
			$city = '';
			$state = '';
			$post_code = '';
			
			if($request->auto_email == 'Yes')
			{
				$email = $request->email;
			}
			
			if($request->auto_first_name == 'Yes')
			{
				$first_name = $request->first_name;
			}
			
			if($request->auto_last_name == 'Yes')
			{
				$last_name = $request->last_name;
			}
			
			if($request->auto_phone_number == 'Yes')
			{
				$phone = $request->phone_number;
			}
			
			if($request->auto_city == 'Yes')
			{
				$city = $request->city;
			}
			
			if($request->auto_zip_code == 'Yes')
			{
				$post_code = $request->post_code;
			}
			
			$Campaign = DB::table('campaigns')->where('id', $campaign_id)->first();
			
			if($Campaign->crm_account == 'Advent')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'CDK')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'Dealerpeak')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'DealerSocket')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'DealerTrack')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'Dominion Dealer Solutions')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'eflow automotive')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'eLead CRM')
			{
				ConsoleHelper::eLeadCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'FordDirect')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'HigherGear')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'Momentum')
			{
				ConsoleHelper::MomentumCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'ProMax Unlimited')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'Reynolds & Reynolds')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'VinSolutions')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'Votenza')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
		}
		
		public function createManualADFXML(Request $request)
		{
			$campaign_id = $request->campaign_id;
			$facebook_ads_lead_user_id = $request->facebook_ads_lead_user_id;
			
			$prospect = $request->prospect;
			$phone_time = $request->phone_time;
			$phone_type = $request->phone_type;
			
			$provider_name = $request->provider_name;
			$service_name = $request->service_name;
			$provider_email = $request->provider_email;
			$provider_phone = $request->provider_phone;
			$provider_url = $request->provider_url;
			$provider_zip = $request->provider_zip;
			$provider_city = $request->provider_city;
			$provider_state = $request->provider_state;
			
			$vendor_name = $request->vendor_name;
			$vendor_email = $request->vendor_email;
			$vendor_phone = $request->vendor_phone;
			$vendor_url = $request->vendor_url;
			$vendor_address = $request->vendor_address;
			$vendor_zip = $request->vendor_zip;
			$vendor_city = $request->vendor_city;
			$vendor_state = $request->vendor_state;
			
			$Lead = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->first();
			
			$address = $Lead->street_address;
			$state = $Lead->state; 
			
			$first_name = '';
			$last_name = '';
			$email = '';
			$phone = '';
			$city = '';
			$post_code = '';
			
			if($request->auto_email == 'Yes')
			{
				$email = $Lead->email;
			}
			
			if($request->auto_first_name == 'Yes')
			{
				if($Lead->first_name == '')
				{
					$first_name = $Lead->full_name;
				}
				else
				{
					$first_name = $Lead->first_name;
				}
			}
			
			if($request->auto_last_name == 'Yes')
			{
				$last_name = $Lead->last_name;
			}
			
			if($request->auto_phone_number == 'Yes')
			{
				$phone = $Lead->phone_number;
			}
			
			if($request->auto_city == 'Yes')
			{
				$city = $Lead->city;
			}
			
			if($request->auto_zip_code == 'Yes')
			{
				$post_code = $Lead->post_code;
			}
			
			$Campaign = DB::table('campaigns')->where('id', $campaign_id)->first();
			
			if($Campaign->crm_account == 'Advent')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'CDK')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'Dealerpeak')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'DealerSocket')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'DealerTrack')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'Dominion Dealer Solutions')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'eflow automotive')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'eLead CRM')
			{
				ConsoleHelper::eLeadCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'FordDirect')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'HigherGear')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'Momentum')
			{
				ConsoleHelper::MomentumCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'ProMax Unlimited')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'Reynolds & Reynolds')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'VinSolutions')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}
			else if($Campaign->crm_account == 'Votenza')
			{
				ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
			}			
		}
		
		public function publishCampaign(Request $request)
		{
			$Campaign = DB::table('autometive_campaigns')->where('campaign_id', $request->campaign_id)->first();
			
			if(is_null($Campaign))
			{
				DB::table('autometive_campaigns')->insert([
				'campaign_id' => $request->campaign_id,
				'prospect' => $request->prospect,
				'auto_email' => $request->auto_email,
				'auto_first_name' => $request->auto_first_name,
				'auto_last_name' => $request->auto_last_name,
				'auto_phone_number' => $request->auto_phone_number,
				'auto_city' => $request->auto_city,
				'auto_zip_code' => $request->auto_zip_code,
				'phone_time' => $request->phone_time,
				'phone_type' => $request->phone_type,
				'provider_name' => $request->provider_name,
				'service_name' => $request->service_name,
				'provider_email' => $request->provider_email,
				'provider_phone' => $request->provider_phone,
				'provider_url' => $request->provider_url,
				'provider_zip' => $request->provider_zip,
				'provider_city' => $request->provider_city,
				'provider_state' => $request->provider_state,
				'vendor_name' => $request->vendor_name,
				'vendor_email' => $request->vendor_email,
				'vendor_phone' => $request->vendor_phone,
				'vendor_url' => $request->vendor_url,
				'vendor_address' => $request->vendor_address,
				'vendor_zip' => $request->vendor_zip,
				'vendor_city' => $request->vendor_city,
				'vendor_state' => $request->vendor_state,
				'checked' => implode(",",$request->checked),
				'created_user_id' => Session::get('user_id'),
				'created_user_ip' => $request->ip(),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_user_id' => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				'updated_at' => date('Y-m-d H:i:s')
				]);
				
				echo "success";
			}
			else
			{
				DB::table('autometive_campaigns')->where('campaign_id', $request->campaign_id)
				->update([
				'auto_email' => $request->auto_email,
				'auto_first_name' => $request->auto_first_name,
				'auto_last_name' => $request->auto_last_name,
				'auto_phone_number' => $request->auto_phone_number,
				'auto_city' => $request->auto_city,
				'auto_zip_code' => $request->auto_zip_code,
				'prospect' => $request->prospect,
				'phone_time' => $request->phone_time,
				'phone_type' => $request->phone_type,
				'provider_name' => $request->provider_name,
				'service_name' => $request->service_name,
				'provider_email' => $request->provider_email,
				'provider_phone' => $request->provider_phone,
				'provider_url' => $request->provider_url,
				'provider_zip' => $request->provider_zip,
				'provider_city' => $request->provider_city,
				'provider_state' => $request->provider_state,
				'vendor_name' => $request->vendor_name,
				'vendor_email' => $request->vendor_email,
				'vendor_phone' => $request->vendor_phone,
				'vendor_url' => $request->vendor_url,
				'vendor_address' => $request->vendor_address,
				'vendor_zip' => $request->vendor_zip,
				'vendor_city' => $request->vendor_city,
				'vendor_state' => $request->vendor_state,
				'checked' => implode(",",$request->checked),
				'updated_user_id' => Session::get('user_id'),
				'updated_user_ip' => $request->ip(),
				'updated_at' => date('Y-m-d H:i:s')
				]);
				
				echo "success"; 
			}
		}
		
		public function publishCampaignSuccess()
		{
			return redirect('clients')->with('alert_success', 'Campaign published successfully');
		}
		
		public function checkUserEmail(Request $request)
		{
			$manager_id = explode(',', $request->manager_id);
			$managers = DB::table('managers')->select('id', 'email')->whereIn('id', $manager_id)->orderBy('full_name', 'ASC')->get();
			foreach($managers as $manager)
			{
				if(trim($manager->email) == '' || $manager->email == null)
				{
					return $manager->id;
				}
			}
		}
		
		public function getUserdetails(Request $request)
		{
			$manager = DB::table('managers')->select('id', 'full_name')->where('id', $request->id)->first();
			return response()->json($manager);
		}
		
		public function updateUserEmail(Request $request)
		{
			$rule = ['user_email' => 'required|email'];
			
			$this->validate($request, $rule);
			
			DB::table('managers')->where('id', $request->UserId)
			->update(['email' => $request->user_email, 'updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s')]);
		}
	}																																																						