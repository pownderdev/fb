<?php
	namespace App\Http\Controllers\Facebook;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Controller;
	use App\Models\FacebookAccounts;
	use App\Models\FacebookAdsAccounts;
	use App\Models\FacebookAdsLeads;
	use App\Models\FacebookAdsLeadUsers;
	use App\Models\FacebookPages;
	use DB;
	use Redirect;
	use Session;
	use ManagerHelper;
	
	class FacebookController extends Controller
	{
		public function getFacebookLogin(Request $request)
		{
			$accessToken = $request->AccessToken;
			
			$curl = curl_init('https://graph.facebook.com/v2.10/me?fields=id,name,first_name,last_name,email,age_range,link,gender,locale,picture,timezone,updated_time,verified&access_token='.$accessToken);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			
			$auth = curl_exec($curl);
			
			curl_close($curl);
			
			$secret = json_decode($auth);
			
			$email = $secret->email;
			//$email = 'rakesh.kashyap38@gmail.com';
			//$email = 'admin@pownder.com';
			
			/***********
				echo "<pre>";
				print_r($secret);
				echo "<pre>"; 
			***********/
			
			$user = DB::table('users')->where('email', $email)->first();
			
			if(is_null($user))
			{
				//$errors=array('loginfailed'=>'Invalid User');
				//return redirect('/')->withErrors($errors);
				
				$vendor_id = DB::table('vendors')->insertGetId(['name' => $secret->name, 'email' => $secret->email, 'created_at'=>date('Y-m-d H:i:s'), 'updated_at'=>date('Y-m-d H:i:s')]);
				
				DB::table('users')->insert([
				'vendor_id' => $vendor_id,
				'name' => $secret->name,
				'email' => $secret->email,
				'category' => 'vendor',
				'created_user_ip' => $request->ip(),
				'created_at' => date('Y-m-d H:i:s'),
				'updated_user_ip' => $request->ip(),
				'updated_at' => date('Y-m-d H:i:s')
				]);
				
				$user = DB::table('users')->where('email', $email)->first();
				
				$user->name = $user->name;
				$user->user_id = $user->id;
				$user->email = $user->email;
				$user->category = $user->category;  
				Session::put('user', $user);
				$table_r='';
				if(session('user')->category=="admin")
				{
					$table_r = "admins";
					$id_r = session('user')->admin_id;
				}
				else if(session('user')->category == "vendor")
				{
					$table_r = "vendors";
					$id_r = session('user')->vendor_id;
				} 
				if($table_r)
				{                        
					$data_r = DB::table($table_r)->where('id',$id_r)->select('mobile','city','state','zip','url')->first();
					$user_r = session('user');
					$user_r->mobile = $data_r->mobile;
					$user_r->city = $data_r->city;
					$user_r->state = $data_r->state;
					$user_r->zip = $data_r->zip;
					$user_r->url = $data_r->url;
					Session::put('user', $user_r);
				}
				Session::put('user_id', $user->id);
				Session::put('user_name', $user->name);
				Session::put('user_category', $user->category);
				Session::put('admin_id', $user->admin_id);
				Session::put('vendor_id', $user->vendor_id);
				Session::put('client_id', $user->client_id);
				Session::put('manager_id', $user->manager_id);
				
				$vendor=DB::table('vendors')->where('id', $user->vendor_id)->first();
				Session::put('photo', $vendor->photo);
				return Redirect::route('vdash');
			}
			else
			{
				if($user->is_deleted==0 && $user->status==1)
				{
					$user->name = $user->name;
					$user->user_id = $user->id;
					$user->email = $user->email;
					$user->category = $user->category;  
					Session::put('user', $user);
					$table_r='';
					if(session('user')->category=="admin")
					{
						$table_r = "admins";
						$id_r = session('user')->admin_id;
					}
					else if(session('user')->category == "vendor")
					{
						$table_r = "vendors";
						$id_r = session('user')->vendor_id;
					} 
					if($table_r)
					{                        
						$data_r = DB::table($table_r)->where('id',$id_r)->select('mobile','city','state','zip','url')->first();
						$user_r = session('user');
						$user_r->mobile = $data_r->mobile;
						$user_r->city = $data_r->city;
						$user_r->state = $data_r->state;
						$user_r->zip = $data_r->zip;
						$user_r->url = $data_r->url;
						Session::put('user', $user_r);
					}
					Session::put('user_id', $user->id);
					Session::put('user_name', $user->name);
					Session::put('user_category', $user->category);
					Session::put('admin_id',$user->admin_id);
					Session::put('vendor_id',$user->vendor_id);
					Session::put('client_id',$user->client_id);
					Session::put('manager_id',$user->manager_id);
					
					if($user->category == 'admin')
					{
						$admin=DB::table('admins')->where('id', $user->admin_id)->first();
						Session::put('photo',$admin->photo);
						return Redirect::route('dashboard');
					}
					elseif($user->category=='vendor')
					{
						$vendor=DB::table('vendors')->where('id', $user->vendor_id)->first();
						Session::put('photo',$vendor->photo);
						return Redirect::route('vdash');
					}
					elseif($user->category=='client')
					{
						$client=DB::table('clients')->where('id', $user->client_id)->first();
						Session::put('photo',$client->photo);
						
						return Redirect::route('cdash');
					}
					elseif($user->category=='user')
					{
						$manager=DB::table('managers')->where('id', $user->manager_id)->first();
						Session::put('photo',$manager->picture);
						
						$data = DB::table('users')->where('id', $user->created_user_id)->first();
						
						if($data->category=='admin')
						{
							if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
							{
								return Redirect::route('dashboard');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Campaigns')=='Yes')
							{
								return Redirect::route('campaigns');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
							{
								return Redirect::route('leads');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
							{
								return Redirect::route('calltrack');
							}
							
							if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
							{
								return Redirect::route('dashboard');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
							{
								return Redirect::route('reports');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Clients')=='Yes')
							{
								return Redirect::route('clients');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Vendors')=='Yes')
							{
								return Redirect::route('vendors');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Invoice')=='Yes')
							{
								return Redirect::route('invoice');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Expense')=='Yes')
							{
								return Redirect::route('expenses');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Setting')=='Yes')
							{
								return Redirect::route('settings');
							}
						}
						elseif($data->category=='vendor')
						{
							if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
							{
								return Redirect::route('vdash');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Campaigns')=='Yes')
							{
								return Redirect::route('campaigns');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
							{
								return Redirect::route('leads');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
							{
								return Redirect::route('calltrack');
							}
							
							if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
							{
								return Redirect::route('vdash');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
							{
								return Redirect::route('reports');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Clients')=='Yes')
							{
								return Redirect::route('clients');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Invoice')=='Yes')
							{
								return Redirect::route('invoice');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Expense')=='Yes')
							{
								return Redirect::route('expenses');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Setting')=='Yes')
							{
								return Redirect::route('settings');
							}
						}
						elseif($data->category=='client')
						{
							if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
							{
								return Redirect::route('cdash');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
							{
								return Redirect::route('cleads');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
							{
								return Redirect::route('calltrack');
							}
							
							if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
							{
								return Redirect::route('cdash');
							}
							
							if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
							{
								return Redirect::route('reports');
							}
						}
						
						return Redirect::route('Error401');
					}
				}
				else
				{
					$errors=array('loginfailed'=>'Suspend User');
					return redirect('/')->withErrors($errors);
				}
			}
		}
		
		public function getFacebookRedirect(Request $request)
		{
			session_start();
			ini_set('max_execution_time', 0);
			
			if (isset($_SESSION['facebook_access_token'])) 
			{
				session(['facebook_access_token' => $_SESSION['facebook_access_token']]);
				
				$accessToken = session('facebook_access_token');
				
				//echo "Access Token = ".$accessToken;die;
				$curl = curl_init('https://graph.facebook.com/v2.10/me?fields=id,name,first_name,last_name,email,age_range,link,gender,locale,picture,timezone,updated_time,verified&access_token='.$accessToken);
				
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				$auth = curl_exec($curl);
				
				curl_close($curl);
				
				$secret = json_decode($auth);
				
				/*********** 
					echo "<pre>";
					print_r($secret);die;
					echo "<pre>"; 
				***********/
				
				$facebook_account_id = $secret->id;
				$facebook_account_name = $secret->name;
				$facebook_account_picture = $secret->picture->data->url;
				if($facebook_account_id>0)
				{
					$check_user = FacebookAccounts::where('id', $facebook_account_id)->where('updated_user_id', "!=", Session::get('user_id'))->where('is_deleted', 0)->get();
					if(count($check_user) == 0)
					{
						$check_exists = FacebookAccounts::where('id', $facebook_account_id)->first();
						if(is_null($check_exists))
						{
							$FacebookAccounts = new FacebookAccounts;
							
							$FacebookAccounts->id = $facebook_account_id;
							$FacebookAccounts->access_token = $accessToken;
							$FacebookAccounts->name = $facebook_account_name;
							$FacebookAccounts->first_name = $secret->first_name;
							$FacebookAccounts->last_name = $secret->last_name;
							$FacebookAccounts->email = $secret->email;
							$FacebookAccounts->age_range = $secret->age_range->min;
							$FacebookAccounts->link = $secret->link;
							$FacebookAccounts->gender = $secret->gender;
							$FacebookAccounts->locale = $secret->locale;
							$FacebookAccounts->picture = $facebook_account_picture;
							$FacebookAccounts->timezone = $secret->timezone;
							$FacebookAccounts->created_time = $secret->updated_time;
							$FacebookAccounts->updated_time = $secret->updated_time;
							$FacebookAccounts->verified = $secret->verified;
							$FacebookAccounts->created_user_id = Session::get('user_id');
							$FacebookAccounts->created_at = date('Y-m-d H:i:s');
							$FacebookAccounts->updated_user_id = Session::get('user_id');
							$FacebookAccounts->updated_at = date('Y-m-d H:i:s');
							
							$FacebookAccounts->save();
							
							Session::put('FirstTimeOauth', 'Yes');
						}
						else
						{
							$FacebookAccounts = FacebookAccounts::find($facebook_account_id);
							
							$FacebookAccounts->access_token = $accessToken;
							$FacebookAccounts->name = $facebook_account_name;
							$FacebookAccounts->first_name = $secret->first_name;
							$FacebookAccounts->last_name = $secret->last_name;
							$FacebookAccounts->email = $secret->email;
							$FacebookAccounts->age_range = $secret->age_range->min;
							$FacebookAccounts->link = $secret->link;
							$FacebookAccounts->gender = $secret->gender;
							$FacebookAccounts->locale = $secret->locale;
							$FacebookAccounts->picture = $facebook_account_picture;
							$FacebookAccounts->timezone = $secret->timezone;
							$FacebookAccounts->updated_time = $secret->updated_time;
							$FacebookAccounts->verified = $secret->verified;
							$FacebookAccounts->updated_user_id = Session::get('user_id');
							$FacebookAccounts->updated_at = date('Y-m-d H:i:s');
							$FacebookAccounts->is_deleted = 0;
							
							$FacebookAccounts->save();
						}
						
						
						//Read facebook ads accounts
						$url = "http://big.pownder.com/fb_campaign/read_account.php?access_token=$accessToken";
						
						$ch = curl_init();
						
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_TIMEOUT, 15);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						
						$response = curl_exec($ch);
						$info = curl_getinfo($ch);
						
						curl_close($ch);
						
						$result = json_decode($response, true);
						
						for($i=0;$i<count($result);$i++)
						{
							$account_id = $result[$i]['account_id'];
							$facebook_id = $result[$i]['id'];
							
							$data = FacebookAdsAccounts::where('facebook_account_id', $facebook_account_id)->where('account_id', $account_id)->first();
							if(is_null($data))
							{
								$FacebookAdsAccounts = new FacebookAdsAccounts;
								
								$FacebookAdsAccounts->facebook_account_id = $facebook_account_id;
								$FacebookAdsAccounts->facebook_account_name = $facebook_account_name;
								$FacebookAdsAccounts->facebook_account_picture = $facebook_account_picture;
								$FacebookAdsAccounts->account_id = $account_id;
								$FacebookAdsAccounts->facebook_id = $facebook_id;
								$FacebookAdsAccounts->created_user_id = Session::get('user_id');
								$FacebookAdsAccounts->created_at = date('Y-m-d H:i:s');
								$FacebookAdsAccounts->updated_user_id = Session::get('user_id');
								$FacebookAdsAccounts->updated_at = date('Y-m-d H:i:s');
								
								$FacebookAdsAccounts->save();
							}
							else
							{
								$FacebookAdsAccounts = FacebookAdsAccounts::find($data['id']);
								
								$FacebookAdsAccounts->facebook_account_id = $facebook_account_id;
								$FacebookAdsAccounts->facebook_account_name = $facebook_account_name;
								$FacebookAdsAccounts->facebook_account_picture = $facebook_account_picture;
								$FacebookAdsAccounts->account_id = $account_id;
								$FacebookAdsAccounts->facebook_id = $facebook_id;
								$FacebookAdsAccounts->updated_user_id = Session::get('user_id');
								$FacebookAdsAccounts->updated_at = date('Y-m-d H:i:s');
								$FacebookAdsAccounts->is_deleted = 0;
								
								$FacebookAdsAccounts->save();
							}
						}
						
						//Read facebook pages
						$url1 = "http://big.pownder.com/fb_campaign/page_list.php?accessToken=$accessToken";
						
						$ch1 = curl_init();
						
						curl_setopt($ch1, CURLOPT_URL, $url1);
						curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch1, CURLOPT_TIMEOUT, 15);
						curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, 0);
						curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, 0);
						
						$response1 = curl_exec($ch1);
						$info1 = curl_getinfo($ch1);
						
						curl_close($ch1);
						
						$result1 = json_decode($response1, true);
						
						//echo "<pre>";
						//print_r($result1);
						
						for($i=0;$i<count($result1);$i++)
						{
							$facebook_page_id = $result1[$i]['id'];
							
							if($facebook_page_id>0)
							{
								$check_page_exist=FacebookPages::where('id', $facebook_page_id)->first();
								if(is_null($check_page_exist))
								{
									$FacebookPages = new FacebookPages;
									
									$FacebookPages->id = $facebook_page_id;
									$FacebookPages->facebook_account_id = $facebook_account_id;
									$FacebookPages->name = $result1[$i]['name'];
									$FacebookPages->category = $result1[$i]['category'];
									$FacebookPages->access_token = $result1[$i]['access_token'];
									$FacebookPages->created_user_id = Session::get('user_id');
									$FacebookPages->created_at = date('Y-m-d H:i:s');
									$FacebookPages->updated_user_id = Session::get('user_id');
									$FacebookPages->updated_at = date('Y-m-d H:i:s');
									
									$FacebookPages->save();
								}
								else
								{
									$FacebookPages = FacebookPages::find($facebook_page_id);
									
									$FacebookPages->facebook_account_id = $facebook_account_id;
									$FacebookPages->name = $result1[$i]['name'];
									$FacebookPages->category = $result1[$i]['category'];
									$FacebookPages->access_token = $result1[$i]['access_token'];
									$FacebookPages->updated_user_id = Session::get('user_id');
									$FacebookPages->updated_at = date('Y-m-d H:i:s');
									$FacebookPages->is_deleted = 0;
									
									$FacebookPages->save();
								}
								
								DB::table('facebook_ads_leads')->where('facebook_page_id', $facebook_page_id)
								->update(['updated_user_id' => Session::get('user_id'), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 0]);
							}
						}
						
						if(Session::get('user_category')=='admin')
						{
							return redirect('/dashboard')->with('alert_success',$facebook_account_name.' facebook account connected successfully');
						}
						else
						{
							return redirect('/vdash')->with('alert_success',$facebook_account_name.' facebook account connected successfully');
						}
					}
					else
					{
						if(Session::get('user_category')=='admin')
						{
							return redirect('/dashboard')->with('alert_danger',$facebook_account_name.' facebook account already connected to another user');
						}
						else
						{
							return redirect('/vdash')->with('alert_danger',$facebook_account_name.' facebook account already connected to another user');
						}
					}
				}
			}
			else
			{
				if(Session::get('user_category')=='admin')
				{
					return redirect('/dashboard')->with('alert_danger','Please try again');
				}
				else
				{
					return redirect('/vdash')->with('alert_danger','Please try again');
				}
			}
		}
	}							