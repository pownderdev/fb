<?php
	namespace App\Http\Controllers\Facebook;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	
	use DB;
	use Session;
	
	class TestController extends Controller
	{
		public function getTest(Request $request)
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$challenge = $_REQUEST['hub_challenge'];
			$verify_token = $_REQUEST['hub_verify_token'];
			
			if ($verify_token === 'test') {
				echo $challenge;
			}
			
			$input = json_decode(file_get_contents('php://input'), true);
			$output=print_r($input, true);
			//file_put_contents('file.txt', $output);
			//$data = print_r($_POST, 1);// this is showing all array
			$file = "file.txt";
			$fp = fopen($file, "a") or die("Couldn't open $file for login!");
			fwrite($fp, $output); 
			//fwrite($fp, $contact_id);
			fclose($fp);
			
		}
		
		public function page_app_subs($page_id,$access_token)
		{
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.10/$page_id/subscribed_apps");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "access_token=".$access_token);
			curl_setopt($ch, CURLOPT_POST, 1);
			
			$headers = array();
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			
			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close ($ch);
			echo "<pre>";
			print_r(json_decode($result,true));
		}
		
		public function subscription($app_id,$app_access_token)
		{
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.10/$app_id/subscriptions");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "object=page&callback_url=https%3A%2F%2Fapiintegrationexperts.com%2Ftrilliontrade%2Ffacebook%2Fcallback.php&fields=leadgen&verify_token=test&access_token=".$app_access_token);
			curl_setopt($ch, CURLOPT_POST, 1);
			
			$headers = array();
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			
			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close ($ch);
			echo "<pre>";
			print_r(json_decode($result,true));
		}
		
		public function app_access_token($client_id,$client_secret)
		{
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.10/oauth/access_token?client_id=$client_id&client_secret=$client_secret&grant_type=client_credentials");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"GET");
			
			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close ($ch);
			echo "<pre>";
			print_r(json_decode($result,true));
		}
	}																	