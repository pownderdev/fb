<?php
	namespace App\Http\Controllers\Facebook;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	
	use DB;
	use Helper;
	use ConsoleHelper;
	use Session;
	
	class FTPController extends Controller
	{
		public function getFTPAccount(Request $request)
		{	
			if(Session::get('user_category')=='user' || Session::get('user_category')=='client' || Session::get('vendor_id') == '13')
			{
				return redirect('Error401');
			}
			
			$TableColumn=array();
			$Columns=DB::table('table_visible_columns')->select('column_value')->where('table_name', 'FTP Account List Table')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 1)->get();
			foreach($Columns as $Column)
			{
				$TableColumn[]=$Column->column_value;
			}
			
			if(Session::get('user_category')=='admin')
			{
				$ftp_accounts = DB::table('ftp_accounts')->where('is_deleted', 0)->get();
			}
			else
			{
				$ftp_accounts = DB::table('ftp_accounts')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->get();
			}
			
			return view('panel.frontend.ftp_account_list',['ftp_accounts' => $ftp_accounts, 'TableColumn' => $TableColumn]);
		}
		
		public function getAddFTPAccount(Request $request)
		{
			if(Session::get('user_category')=='user' || Session::get('user_category')=='client' || Session::get('vendor_id') == '13')
			{
				return redirect('Error401');
			}
			
			return view('panel.frontend.ftp_account_add');
		}
		
		public function validateAddFTPAccount(Request $request)
		{
			if(Session::get('user_category')=='user' || Session::get('user_category')=='client' || Session::get('vendor_id') == '13')
			{
				return redirect('Error401');
			}
			
			$rule=[
			'name' => 'required',
			'provider' => 'required',
			'ftp_host' => 'required',
			'ftp_port' => 'required|numeric',
			'ftp_username' => 'required',
			'ftp_password' => 'required',
			];
			
			$this->validate($request,$rule);
		}
		
		public function postAddFTPAccount(Request $request)
		{
			if(Session::get('user_category')=='user' || Session::get('user_category')=='client' || Session::get('vendor_id') == '13')
			{
				return redirect('Error401');
			}
			
			$rule=[
			'name' => 'required',
			'provider' => 'required',
			'ftp_host' => 'required',
			'ftp_port' => 'required|numeric',
			'ftp_username' => 'required',
			'ftp_password' => 'required',
			];
			
			$this->validate($request,$rule);
			
			DB::table('ftp_accounts')->insert([
			'name' => $request->name,
			'provider' => $request->provider,
			'use_sftp' => $request->use_sftp,
			'ftp_host' => $request->ftp_host,
			'ftp_port' => $request->ftp_port,
			'username' => $request->ftp_username,
			'password' => $request->ftp_password,
			'directory' => $request->directory,
			'created_user_id' => Session::get('user_id'),
			'created_user_ip' => $request->ip(),
			'created_at' => date('Y-m-d H:i:s'),
			'updated_user_id' => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			return redirect('ftp-account')->with('alert_success','FTP account added successfully');
		}
		
		public function getEditFTPAccount($id)
		{
			if(Session::get('user_category')=='user' || Session::get('user_category')=='client' || Session::get('vendor_id') == '13')
			{
				return redirect('Error401');
			}
			
			$ftp_account = DB::table('ftp_accounts')->where('id', $id)->first();
			return view('panel.frontend.ftp_account_edit',['ftp_account' => $ftp_account]);
		}
		
		public function postEditFTPAccount(Request $request)
		{
			if(Session::get('user_category')=='user' || Session::get('user_category')=='client' || Session::get('vendor_id') == '13')
			{
				return redirect('Error401');
			}
			
			$rule=[
			'name' => 'required',
			'provider' => 'required',
			'ftp_host' => 'required',
			'ftp_port' => 'required|numeric',
			'ftp_username' => 'required',
			'ftp_password' => 'required',
			];
			
			$this->validate($request,$rule);
			
			DB::table('ftp_accounts')->where('id', $request->id)
            ->update([
			'name' => $request->name,
			'provider' => $request->provider,
			'use_sftp' => $request->use_sftp,
			'ftp_host' => $request->ftp_host,
			'ftp_port' => $request->ftp_port,
			'username' => $request->ftp_username,
			'password' => $request->ftp_password,
			'directory' => $request->directory,
			'updated_user_id' => Session::get('user_id'),
			'updated_user_ip' => $request->ip(),
			'updated_at' => date('Y-m-d H:i:s')
			]);
			
			return redirect('ftp-account')->with('alert_success','FTP account updated successfully');
		}
		
		public function deleteFTPAccount(Request $request)
		{
			if(Session::get('user_category')=='user' || Session::get('user_category')=='client' || Session::get('vendor_id') == '13')
			{
				return redirect('Error401');
			}
			
			DB::table('ftp_accounts')->where('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
		}
		
		public function activeFTPAccount(Request $request)
		{
			if(Session::get('user_category')=='user' || Session::get('user_category')=='client' || Session::get('vendor_id') == '13')
			{
				return redirect('Error401');
			}
			
			DB::table('ftp_accounts')->where('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 1]);
		}
		
		public function inactiveFTPAccount(Request $request)
		{
			if(Session::get('user_category')=='user' || Session::get('user_category')=='client' || Session::get('vendor_id') == '13')
			{
				return redirect('Error401');
			}
			
			DB::table('ftp_accounts')->where('id', $request->id)
			->update(['updated_user_id' => Session::get('user_id'), 'updated_user_ip' => $request->ip(), 'updated_at' => date('Y-m-d H:i:s'), 'status' => 0]);
		}
	}						