<?php
	namespace App\Http\Controllers\Facebook;
	
	use App\Http\Controllers\Controller;
	use Illuminate\Http\Request;
	use Illuminate\Filesystem\Filesystem;
	
	use DB;
	use Helper;
	use ConsoleHelper;
	use WebhookHelper;
	use Session;
	use Mail;
	use DateTime;
	use App\Models\FacebookAccounts;
	use App\Models\FacebookAdsAccounts;
	use App\Models\FacebookAdsLeads;
	use App\Models\FacebookAdsLeadUsers;
	use App\Models\FacebookPages;
	use Redirect;
	use ManagerHelper;
	class TestController extends Controller
	{
		public function getTest(Request $request)
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$dr=new DateTime("today");
			$date= $dr->format('U');
			$d=array(["field"=> "time_created", "operator"=> "GREATER_THAN", "value"=>$date]);
			$a=json_encode($d);
			
			$counter = 1;
			
			$facebook_pages = DB::table('facebook_pages')->where('is_deleted', 0)->inRandomOrder()->get();
			foreach($facebook_pages as $facebook_page)
			{
				if($counter%100 == 0)
				{
					sleep(20);
				}
				
				$counter++;
				
				$facebook_page_id = $facebook_page->id;
				$facebook_page_access_token = $facebook_page->access_token;
				$facebook_account_id = $facebook_page->facebook_account_id;
				$created_user_id = $facebook_page->updated_user_id;
				$updated_user_id = $facebook_page->updated_user_id;
				
				//Read facebook ads lead 
				$url2 = "http://big.pownder.com/fb_campaign/get_lead_ads.php?facebook_page_access_token=$facebook_page_access_token&facebook_page_id=$facebook_page_id";
				
				$ch2 = curl_init();
				
				curl_setopt($ch2, CURLOPT_URL,$url2);
				curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
				
				$response2 = curl_exec($ch2);
				
				curl_close($ch2);
				
				$result2 = json_decode($response2, true);
				
				for($j=0;$j<count($result2);$j++)
				{
					$facebook_ads_lead_id = $result2[$j]['id'];
					
					if($facebook_ads_lead_id>0)
					{
						$check_exist=DB::table('facebook_ads_leads')->where('id', $facebook_ads_lead_id)->first();
						if(is_null($check_exist))
						{
							DB::table('facebook_ads_leads')->insert([
							'id' => $facebook_ads_lead_id,
							'facebook_account_id' => $facebook_account_id,
							'facebook_page_id' => $facebook_page_id,
							'name' => $result2[$j]['name'],
							'leadgen_export_csv_url' => $result2[$j]['leadgen_export_csv_url'],
							'locale' => $result2[$j]['locale'],
							'status' => $result2[$j]['status'],
							'created_user_id' => $created_user_id,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_user_id' => $updated_user_id,
							'updated_at' => date('Y-m-d H:i:s')
							]);
							
							$is_blocked = 0;
						}
						else
						{
							DB::table('facebook_ads_leads')->where('id', $facebook_ads_lead_id)
							->update([
							'facebook_account_id' => $facebook_account_id,
							'facebook_page_id' => $facebook_page_id,
							'name' => $result2[$j]['name'],
							'leadgen_export_csv_url' => $result2[$j]['leadgen_export_csv_url'],
							'locale' => $result2[$j]['locale'],
							'status' => $result2[$j]['status'],
							'updated_user_id' => $updated_user_id,
							'updated_at' => date('Y-m-d H:i:s'),
							'is_deleted' => 0
							]);
							
							$is_blocked = $check_exist->is_blocked;
						}
						
						if($result2[$j]['status'] == 'ACTIVE' && $is_blocked == 0)
						{
							//Read facebook ads lead user
							$url3 = "https://graph.facebook.com/v2.10/".$facebook_ads_lead_id."/leads?limit=50000&filtering=".$a."&access_token=".$facebook_page_access_token;
							
							$ch3 = curl_init();
							curl_setopt($ch3, CURLOPT_URL,$url3);
							curl_setopt($ch3, CURLOPT_RETURNTRANSFER, 1);
							$response3 = curl_exec($ch3);
							curl_close($ch3);
							
							//$result3 = json_decode($response3, true);
							$result13 = json_decode($response3, true);
							$result3 = isset($result13['data']) ? $result13['data'] : array();
							//echo "<pre>";
							//print_r($result3);
							for($k=0;$k<count($result3);$k++)
							{
								if(isset($result3[$k]['id']))
								{
									$facebook_ads_lead_user_id = $result3[$k]['id'];
									
									if($facebook_ads_lead_user_id>0)
									{
										$check_page_lead_exist=DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->first();
										
										if(is_null($check_page_lead_exist))
										{
											$field_data=$result3[$k]['field_data'];
											
											$first_name='';
											$last_name='';
											$full_name='';
											$email='';
											$phone_number='';
											$street_address='';
											$city='';
											$state='';
											$province='';
											$country='';
											$post_code='';
											$zip_code='';
											$date_of_birth='';
											$gender='';
											$marital_status='';
											$relationship_status='';
											$company_name='';
											$military_status='';
											$job_title='';
											$work_phone_number='';
											$work_email='';
											$numero_telefonico='';
											
											for($m=0; $m<count($field_data);$m++)
											{
												$$field_data[$m]['name']=$field_data[$m]['values'][0];
											}
											
											if($phone_number!='')
											{
												$phone_number=Helper::PhoneNumber($phone_number);
											}
											
											if($full_name=='')
											{
												$full_name=$first_name.' '.$last_name;
											}
											
											$latitude = '';
											$longitude = '';
											
											if($post_code>0)
											{
												$address = str_replace(" ","+",$post_code);
												$url = "http://maps.google.com/maps/api/geocode/json?address=$address";
												$ch = curl_init();
												curl_setopt($ch, CURLOPT_URL, $url);
												curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
												curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
												curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
												curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
												$response = curl_exec($ch);
												curl_close($ch);
												
												$response_a = json_decode($response);
												if(count($response_a->results)>0)
												{
													$latitude = $response_a->results[0]->geometry->location->lat;
													$longitude = $response_a->results[0]->geometry->location->lng;
												}
											}
											elseif($city != '')
											{
												$address = str_replace(" ","+",$city);
												$url = "http://maps.google.com/maps/api/geocode/json?address=$address";
												$ch = curl_init();
												curl_setopt($ch, CURLOPT_URL, $url);
												curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
												curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
												curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
												curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
												$response = curl_exec($ch);
												curl_close($ch);
												
												$response_a = json_decode($response);
												if(count($response_a->results)>0)
												{
													$latitude = $response_a->results[0]->geometry->location->lat;
													$longitude = $response_a->results[0]->geometry->location->lng;
												}
											}
											
											$created_time =  date('Y-m-d H:i:s', strtotime($result3[$k]['created_time']));
											
											DB::table('facebook_ads_lead_users')->insert([
											'id' => $facebook_ads_lead_user_id,
											'facebook_account_id' => $facebook_account_id,
											'facebook_page_id' => $facebook_page_id,
											'facebook_ads_lead_id' => $facebook_ads_lead_id,
											'ad_id' => isset($result3[$k]['ad_id'])?$result3[$k]['ad_id']:0,
											'ad_name' => isset($result3[$k]['ad_name'])?$result3[$k]['ad_name']:'',
											'adset_id' => isset($result3[$k]['adset_id'])?$result3[$k]['adset_id']:0,
											'adset_name' => isset($result3[$k]['adset_name'])?$result3[$k]['adset_name']:'',
											'campaign_id' => isset($result3[$k]['campaign_id'])?$result3[$k]['campaign_id']:0,
											'campaign_name' => isset($result3[$k]['campaign_name'])?$result3[$k]['campaign_name']:'',
											'first_name' => $first_name,
											'last_name' => $last_name,
											'full_name' => $full_name,
											'email' => $email,
											'phone_number' => $phone_number,
											'street_address' => $street_address,
											'city' => $city,
											'state' => $state,
											'province' => $province,
											'country' => $country,
											'post_code' => $post_code,
											'zip_code' => $zip_code,
											'date_of_birth' => $date_of_birth,
											'gender' => $gender,
											'marital_status' => $marital_status,
											'relationship_status' => $relationship_status,
											'company_name' => $company_name,
											'military_status' => $military_status,
											'job_title' => $job_title,
											'work_phone_number' => $work_phone_number,
											'work_email' => $work_email,
											'numero_telefonico' => $numero_telefonico,
											'latitude' => $latitude,
											'longitude' => $longitude,
											'created_time' => $created_time,
											'created_user_id' => $created_user_id,
											'created_at' => date('Y-m-d H:i:s'),
											'updated_user_id' => $updated_user_id,
											'updated_at' => date('Y-m-d H:i:s')
											]);
											
											$admin = DB::table('admins')->where('id', 1)->where('new_lead_email', '!=', '')->where('new_lead_option', 'Yes')->where('is_deleted', 0)->first();
											if(!is_null($admin))
											{
												if($admin->receive_notification == 'Everytime')
												{
													$data14 = array('name' => $admin->name, 'full_name' => $full_name, 'email' => $email, 'phone' => Helper::emailPhoneFormat($phone_number), 'city' => $city);
													
													Mail::send('emails.cron_new_lead', $data14, function ($message) use ($admin) {
														$message->from('noreply@pownder.com', 'Pownder');
														$message->to($admin->new_lead_email)->subject('NEW FACEBOOK LEAD');
													});
												}
												else
												{
													DB::table('admin_emails')->insert(['facebook_ads_lead_user_id' => $facebook_ads_lead_user_id, 'subject' => 'NEW FACEBOOK LEAD', 'client_name' => '', 'leadType' => 'Pownder™ Lead', 'leadEmail' => $email, 'leadPhone' => Helper::emailPhoneFormat($phone_number), 'leadCity' => $city, 'first_name' => $full_name, 'created_at' => date('Y-m-d H:i:s')]);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		public function getTest1(Request $request)
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$data = array('name' => '', 'appointment_date' => '', 'appointment_time' => '', 'category' => '', 'client_name' => '', 'manager_name' => '', 'leadType' => '', 'leadAddedBy' => '', 'leadName' => '', 'leadEmail' => '', 'leadPhone' => '', 'leadCity' => '');
			
			Mail::send('emails.appointment', $data, function ($message) {
				$message->from('noreply@pownder.com', 'Pownder');
				//$message->to('manpreet.constacloud@gmail.com')->subject('Facebook Dummy Appointment');
				$message->to('appt@pownder.com')->subject('Facebook Dummy Appointment');
			});
			
			}
		
		public function getTest2(Request $request)
		{
			$facebook_pages = DB::table('facebook_pages')->orderBy('name', 'ASC')->get();
			foreach($facebook_pages as $facebook_page)
			{
				echo "<pre>";
				echo "Page Id: ".$facebook_page->id;
				echo "<pre>";
				echo "Page Name: ".$facebook_page->name;
				$page_access_token = $facebook_page->access_token;
				$page_id = $facebook_page->id;;
				$ch = curl_init();
				
				curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.11/$page_id/insights/page_engaged_users?period=lifetime&access_token=".$page_access_token);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				
				$result = curl_exec($ch);
				if (curl_errno($ch)) {
					echo 'Error:' . curl_error($ch);
				}
				curl_close ($ch);
				echo "<pre>";
				print_r(json_decode($result,true)); 
				
				$ch = curl_init();
				
				curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.11/$page_id/insights/page_post_engagements?period=lifetime&access_token=".$page_access_token);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				
				$result = curl_exec($ch);
				if (curl_errno($ch)) {
					echo 'Error:' . curl_error($ch);
				}
				curl_close ($ch);
				echo "<pre>";
				print_r(json_decode($result,true));
				
			} 
		}		
	}																																											