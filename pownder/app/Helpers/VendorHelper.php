<?php
	namespace App\Helpers;
	
	use DB;
	use Session;
	
	class VendorHelper
	{
		public static function MonthLead($StartDate, $EndDate)
		{
			$ShowLead = '';
			$UserArray = array();
			$ClientArray = array();
			
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				$UserArray[] = Session::get('user_id');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				$Vendor = DB::table('users')->select('vendor_id')->where('category', 'vendor')->where('id', $manager->created_user_id)->first();
				
				$Clients = DB::table('clients')->select('id')->where('vendor_id', $Vendor->vendor_id)->get();
				foreach($Clients as $Client)
				{
					$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
						$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
						foreach($Managers as $Manager)
						{
							$UserArray[] = $Manager->id;
						}
					}
				}
				
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')
				->where(function ($query) use($UserArray, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', '=', Session::get('manager_id'));
					}
					else
					{
						$query->whereIn('created_user_id', $UserArray);
					}
				})
				->where(function ($query){
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})
				->whereDate('created_time', '>=', $StartDate)
				->whereDate('created_time', '<=', $EndDate)
				->count('id');
				return $FacebookAdsLeadUsers;
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				$Clients = DB::table('clients')->select('id')->where('vendor_id',Session::get('vendor_id'))->get();
				foreach($Clients as $Client)
				{
					$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
						$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
						foreach($Managers as $Manager)
						{
							$UserArray[] = $Manager->id;
						}
					}
				}
				
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->whereIn('created_user_id', $UserArray)->whereDate('created_time', '>=', $StartDate)->whereDate('created_time', '<=', $EndDate)
				->where(function ($query){
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})->count('id');
				return $FacebookAdsLeadUsers;				
			}
		}
		
		public static function DateWiseAllLeads($created_time)
		{
			$ShowLead = '';
			$UserArray = array();
			$ClientArray = array();
			
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				$UserArray[] = Session::get('user_id');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				$Vendor = DB::table('users')->select('vendor_id')->where('category', 'vendor')->where('id', $manager->created_user_id)->first();
				
				$Clients = DB::table('clients')->select('id')->where('vendor_id', $Vendor->vendor_id)->get();
				foreach($Clients as $Client)
				{
					$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
						$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
						foreach($Managers as $Manager)
						{
							$UserArray[] = $Manager->id;
						}
					}
				}
				
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')
				->where(function ($query) use($UserArray, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', '=', Session::get('manager_id'));
					}
					else
					{
						$query->whereIn('created_user_id', $UserArray);
					}
				})
				->whereDate('created_time', '=', $created_time)
				->where(function ($query){
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})->count('id');
				return $FacebookAdsLeadUsers;
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				$Clients = DB::table('clients')->select('id')->where('vendor_id',Session::get('vendor_id'))->get();
				foreach($Clients as $Client)
				{
					$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
						$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
						foreach($Managers as $Manager)
						{
							$UserArray[] = $Manager->id;
						}
					}
				}
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->whereIn('created_user_id', $UserArray)->whereDate('created_time', '=', $created_time)
				->where(function ($query){
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})->count('id');
				return $FacebookAdsLeadUsers;				
			}
		}
		
		public static function DateWiseFacebookLeads($created_time)
		{
			$ShowLead = '';
			$UserArray = array();
			$ClientArray = array();
			
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				$UserArray[] = Session::get('user_id');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				$Vendor = DB::table('users')->select('vendor_id')->where('category', 'vendor')->where('id', $manager->created_user_id)->first();
				
				$Clients = DB::table('clients')->select('id')->where('vendor_id', $Vendor->vendor_id)->get();
				foreach($Clients as $Client)
				{
					$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
						$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
						foreach($Managers as $Manager)
						{
							$UserArray[] = $Manager->id;
						}
					}
				}
				
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')
				->where(function ($query) use($UserArray, $ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', '=', Session::get('manager_id'));
					}
					else
					{
						$query->whereIn('created_user_id', $UserArray);
					}
				})
				->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $created_time)
				->where(function ($query){
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})->count('id');
				return $FacebookAdsLeadUsers;
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				$Clients = DB::table('clients')->select('id')->where('vendor_id',Session::get('vendor_id'))->get();
				foreach($Clients as $Client)
				{
					$users = DB::table('users')->select('id')->where('client_id',$Client->id)->get();
					foreach($users as $user)
					{
						$UserArray[] = $user->id;
						$Managers = DB::table('users')->select('id')->where('created_user_id',$user->id)->get();
						foreach($Managers as $Manager)
						{
							$UserArray[] = $Manager->id;
						}
					}
				}
				
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->where('lead_type', 'Pownder™ Lead')->whereIn('created_user_id', $UserArray)->whereDate('created_time', '=', $created_time)
				->where(function ($query){
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})->count('id');
				return $FacebookAdsLeadUsers;				
			}
		}
		
		public static function VendorClients($vendor_id)
		{
			$Clients = DB::table('clients')
			->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
			->leftJoin('managers', 'clients.manager_id', '=', 'managers.id')
			->select('clients.*', 'packages.name as package_name', 'managers.first_name', 'managers.last_name')
			->where('clients.vendor_id', $vendor_id)
			->where('clients.is_deleted', 0)
			->orderBy('clients.name', 'ASC')
			->get();
			
			return $Clients;
		}
		
		public static function CampaignVendorClients($id)
		{
			$Clients=array();
			$user = DB::table('users')->select('vendor_id')->where('id', $id)->where('category','vendor')->first();
			if(!is_null($user))
			{
				$Clients=DB::table('clients')
				->where(function ($query) use ($user, $id) {
					$query->where('vendor_id', $user->vendor_id)
					->orWhere('created_user_id', $id);
				})
				->where('is_deleted', 0)
				->where('status', 1)
				->orderBy('name', 'ASC')
				->get();
			}
			
			return $Clients;
		}
		
		public static function VendorFacebookPages($updated_user_id)
		{
			$FacebookPages = DB::table('facebook_pages')->where('updated_user_id', $updated_user_id)->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			return $FacebookPages;
		}
		
		public static function VendorName($vendor_id)
		{
			$vendor = DB::table('vendors')->where('id', $vendor_id)->orderBy('name', 'ASC')->first();
			if(is_null($vendor))
			{
				return '';
			}
			else
			{
				return $vendor->name;
			}
		}
		
		public static function vendorCampaignLeads($UserArray, $ClientArray, $ManagerArray, $campaign_id, $s_date, $e_date)
		{
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->select('id')
			->whereIn('campaign_id', $campaign_id)
			->whereDate('created_time', '>=', $s_date)
			->whereDate('created_time', '<=', $e_date)
			->where(function ($query) use($UserArray, $ClientArray, $ManagerArray){
				$query->whereIn('created_user_id', array_unique($UserArray))
				->whereIn('client_id', array_unique($ClientArray), 'or')
				->whereIn('manager_id', array_unique($ManagerArray), 'or');
			})
			->count('id');
			
			return $facebook_ads_lead_users;
		}
	}																			