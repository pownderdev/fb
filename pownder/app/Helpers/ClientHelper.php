<?php
	namespace App\Helpers;
	
	use DB;
	use Session;
	
	class ClientHelper
	{
		public static function MonthLead($StartDate, $EndDate)
		{
			if(Session::get('user_category')=='user')
			{
				$manager = DB::table('managers')->select('client_id')->where('id',Session::get('manager_id'))->first();
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')
				->where(function ($query) use($ShowLead, $manager){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', Session::get('manager_id'));
					}
					else
					{
						$query->where('client_id', $manager->client_id);
					}
				})
				->whereDate('created_time', '>=', $StartDate)->whereDate('created_time', '<=', $EndDate)->count('id');
				return $FacebookAdsLeadUsers;
				
			}
			else
			{
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', '=', Session('client_id'))->whereDate('created_time', '>=', $StartDate)->whereDate('created_time', '<=', $EndDate)->count('id');
				
				return $FacebookAdsLeadUsers;				
			}
		}
		
		public static function DateWiseAllLeads($created_time)
		{
			if(Session::get('user_category')=='user')
			{
				$manager = DB::table('managers')->select('client_id')->where('id',Session::get('manager_id'))->first();
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')
				->where(function ($query) use($ShowLead, $manager){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', Session::get('manager_id'));
					}
					else
					{
						$query->where('client_id', $manager->client_id);
					}
				})
				->whereDate('created_time', '=', $created_time)->count('id');
				
				return $FacebookAdsLeadUsers;
				
			}
			else
			{
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', Session('client_id'))->whereDate('created_time', '=', $created_time)->count('id');
				
				return $FacebookAdsLeadUsers;				
			}
		}
		
		public static function DateWiseFacebookLeads($created_time)
		{
			if(Session::get('user_category')=='user')
			{
				$manager = DB::table('managers')->select('client_id')->where('id',Session::get('manager_id'))->first();
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->where('lead_type', 'Pownder™ Lead')
				->where(function ($query) use($ShowLead, $manager){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', Session::get('manager_id'));
					}
					else
					{
						$query->where('client_id', $manager->client_id);
					}
				})
				->whereDate('created_time', '=', $created_time)->count('id');
				
				return $FacebookAdsLeadUsers;
				
			}
			else
			{
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', Session('client_id'))->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $created_time)->count('id');
				
				return $FacebookAdsLeadUsers;				
			}
		}
		
		public static function TotalLeads()
		{
			if(Session::get('user_category')=='user')
			{
				$manager = DB::table('managers')->select('client_id')->where('id',Session::get('manager_id'))->first();
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')
				->where(function ($query) use($ShowLead, $manager){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', Session::get('manager_id'));
					}
					else
					{
						$query->where('client_id', $manager->client_id);
					}
				})->count('id');
				
				return $FacebookAdsLeadUsers;
				
			}
			else
			{
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', Session('client_id'))->count('id');
				
				return $FacebookAdsLeadUsers;				
			}
		}
		
		public static function ClientAds($client_id)
		{
			$ClientAds = DB::table('facebook_ads_lead_users')->select('ad_id')->where('client_id', $client_id)->where('ad_id', '>', 0)->groupBy('ad_id')->count('ad_id');
			
			return $ClientAds;
		}
		
		public static function ClientCPs($client_id)
		{
			$ClientCPs = DB::table('facebook_ads_lead_users')->select('campaign_id')->where('client_id', $client_id)->where('campaign_id', '>', 0)->groupBy('campaign_id')->count('campaign_id');
			
			return $ClientCPs;
		}
		
		public static function ClientLeadCPs($client_id, $s_date, $e_date)
		{
			if($s_date == '' || $e_date == '')
			{
				$s_date = date('Y-m').'-01';
				$e_date = date('Y-m-d');
			}
			
			$array = array();
			$ClientCPs = DB::table('facebook_ads_lead_users')->select('campaign_id')->where('client_id', $client_id)->where('campaign_id', '>', 0)->whereDate('created_time', '>=', $s_date)->whereDate('created_time', '<=', $e_date)->groupBy('campaign_id')->get();
			
			foreach($ClientCPs as $ClientCP)
			{
				$array[] = $ClientCP->campaign_id;
			}
			
			return $array;
		}
		
		public static function ClientCampaign($client_id)
		{
			$ClientCampaign = DB::table('campaigns')->select('id')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->count('id');
			
			return $ClientCampaign;
		}
		
		public static function ClientLDs($client_id, $s_date, $e_date)
		{
			if($s_date == '' || $e_date == '')
			{
				$s_date = date('Y-m').'-01';
				$e_date = date('Y-m-d');
			}
			
			$ClientLDs = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->whereDate('created_time', '>=', $s_date)->whereDate('created_time', '<=', $e_date)->count('id');
			
			return $ClientLDs;
		}
		
		public static function ClientFacebookLeads($client_id, $s_date, $e_date)
		{
			if($s_date == '' || $e_date == '')
			{
				$s_date = date('Y-m').'-01';
				$e_date = date('Y-m-d');
			}
			
			$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '>=', $s_date)->whereDate('created_time', '<=', $e_date)->count('id');
			
			return $FacebookAdsLeadUsers;				
		}
		
		public static function ClientSold($client_id, $s_date, $e_date)
		{
			if($s_date == '' || $e_date == '')
			{
				$s_date = date('Y-m').'-01';
				$e_date = date('Y-m-d');
			}
			
			$ClientSold = DB::table('sold_leads')->select('id')->where('sold_leads.source', 'Pownder™')->where('client_id', $client_id)->where('is_deleted', 0)->whereDate('sold_date', '>=', $s_date)->whereDate('sold_date', '<=', $e_date)->count('id');
			
			return $ClientSold;
		}
		
		public static function ClientProfit($client_id, $s_date, $e_date)
		{
			if($s_date == '' || $e_date == '')
			{
				$s_date = date('Y-m').'-01';
				$e_date = date('Y-m-d');
			}
			
			$ClientProfit = DB::table('sold_leads')->select('total_profit')->where('client_id', $client_id)->where('sold_leads.source', 'Pownder™')->where('is_deleted', 0)->whereDate('sold_date', '>=', $s_date)->whereDate('sold_date', '<=', $e_date)->sum('total_profit');
			
			return $ClientProfit;
		}
		
		public static function ContactCreateLastLead($crm_account_id)
		{
			$CRMContact = DB::table('crm_contacts')->select('updated_at')->where('crm_account_id', $crm_account_id)->where('is_deleted', 0)->orderBy('updated_at', 'DESC')->first();
			
			if(is_null($CRMContact))
			{
				return '';
			}
			else
			{
				return date('m/d/Y h:i A',strtotime($CRMContact->updated_at));
			}
		}
		
		public static function clientCampaignLeads($client_id, $campaign_id, $s_date, $e_date)
		{
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', $client_id)->whereIn('campaign_id', $campaign_id)->whereDate('created_time', '>=', $s_date)->whereDate('created_time', '<=', $e_date)->count('id');
			
			return $facebook_ads_lead_users;
		}
		
		public static function CampaignLeads($campaign_id, $s_date, $e_date)
		{
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->select('id')->whereIn('campaign_id', $campaign_id)->whereDate('created_time', '>=', $s_date)->whereDate('created_time', '<=', $e_date)->count('id');
			
			return $facebook_ads_lead_users;
		}
		
		public static function CRMTotalContact($crm_account_id)
		{
			$CRMContacts = DB::table('crm_contacts')->select('id')->where('crm_account_id', $crm_account_id)->count('id');
			
			return $CRMContacts;
		}
	}																							