<?php
	namespace App\Helpers;
	
	use DB;
	use Session;
	
	class Boot_Notifications
	{
		public static function GetRecurringNotifications()
		{
	      //Check for any new recurring invoice sent notification
            
          $recurring_notific=DB::select('select invoice_num,invoice_id from invoice where created_user_id=? and recurring_notific=?',[Session::get('user')->id,1]);

          return $recurring_notific;
		}		
		
	}																