<?php
	namespace App\Helpers;
	
	use DB;
	use Session;
	
	class ManagerHelper
	{
		public static function ManagerName($manager_id)
		{
			$Manager = DB::table('managers')->where('id', $manager_id)->first();
			
			if(is_null($Manager))
			{
				return '';
			}
			else
			{
				return $Manager->first_name.' '.$Manager->last_name;
			}
		}
		
		public static function ManagerCategory()
		{
			if(Session::get('manager_id') != 0)
			{
				$manager = DB::table('managers')->where('id', Session::get('manager_id'))->first();
				
				$ManagerUser = DB::table('users')->select('category')->where('admin_id', $manager->admin_id)->where('client_id', $manager->client_id)->where('vendor_id', $manager->vendor_id)->first();
				
				if(!is_null($ManagerUser))
				{
					return $ManagerUser->category;
				}
			}
			
			return '';
		}
		
		public static function ManagerMenuVisible($menu)
		{
			$ManagerMenu = DB::table('manager_menus')->select('id')->where('manager_id', Session::get('manager_id'))->where('menu', $menu)->where('is_deleted', 0)->first();
			
			if(!is_null($ManagerMenu))
			{
				return "Yes";
			}
		}
		
		public static function ManagerMenuAction($permission)
		{
			$ManagerPermission = DB::table('manager_permissions')->select('id')->where('manager_id', Session::get('manager_id'))->where('permission', $permission)->where('is_deleted', 0)->first();
			if(is_null($ManagerPermission))
			{
				header('Location: http://big.pownder.com/Error401');
				dd($ManagerPermission);
			}
			else
			{
				return "Yes";
			}
		}
		
		public static function ManagerWritePermission($permission)
		{
			$ManagerPermission = DB::table('manager_permissions')->select('id')->where('manager_id', Session::get('manager_id'))->where('permission', $permission)->where('is_deleted', 0)->first();
			if(is_null($ManagerPermission))
			{
				return "No";
			}
			else
			{
				return "Yes";
			}
		}
		
		public static function ManagerLeadMenuAction()
		{
			$ManagerPermission1 = DB::table('manager_permissions')->select('id')->where('manager_id', Session::get('manager_id'))->where('permission', 'All Leads')->where('is_deleted', 0)->first();
			if(is_null($ManagerPermission1))
			{
				$ManagerPermission2 = DB::table('manager_permissions')->select('id')->where('manager_id', Session::get('manager_id'))->where('permission', 'Allotted Leads')->where('is_deleted', 0)->first();
				if(is_null($ManagerPermission2))
				{
					header('Location: http://big.pownder.com/Error401');
					dd($ManagerPermission2);
				}
				else
				{
					return "Allotted Leads";
				}
			}
			else
			{
				return "All Leads";
			}
		}
		
		public static function ManagerClientMenuAction()
		{
			$ManagerPermission1 = DB::table('manager_permissions')->select('id')->where('manager_id', Session::get('manager_id'))->where('permission', 'All Clients')->where('is_deleted', 0)->first();
			if(is_null($ManagerPermission1))
			{
				$ManagerPermission2 = DB::table('manager_permissions')->select('id')->where('manager_id', Session::get('manager_id'))->where('permission', 'Allotted Clients')->where('is_deleted', 0)->first();
				if(is_null($ManagerPermission2))
				{
					header('Location: http://big.pownder.com/Error401');
					dd($ManagerPermission2);
				}
				else
				{
					return "Allotted Clients";
				}
			}
			else
			{
				return "All Clients";
			}
		}
		
		public static function ManagerLeadCPs($manager_id, $s_date, $e_date)
		{
			if($s_date == '' || $e_date == '')
			{
				$s_date = date('Y-m').'-01';
				$e_date = date('Y-m-d');
			}
			
			$array = array();
			$ClientCPs = DB::table('facebook_ads_lead_users')->select('campaign_id')->where('manager_id', $manager_id)->where('campaign_id', '>', 0)->whereDate('created_time', '>=', $s_date)->whereDate('created_time', '<=', $e_date)->groupBy('campaign_id')->get();
			
			foreach($ClientCPs as $ClientCP)
			{
				$array[] = $ClientCP->campaign_id;
			}
			
			return $array;
		}
		
		public static function managerCampaignLeads($manager_id, $campaign_id, $s_date, $e_date)
		{
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->select('id')->where('manager_id', $manager_id)->whereIn('campaign_id', $campaign_id)->whereDate('created_time', '>=', $s_date)->whereDate('created_time', '<=', $e_date)->count('id');
			
			return $facebook_ads_lead_users;
		}
	}																	