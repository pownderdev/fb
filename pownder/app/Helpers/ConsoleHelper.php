<?php
	namespace App\Helpers;
	
	use DB;
	use Session;
	use Mail;
	
	class ConsoleHelper
	{
		//---Agile CRM API
		public static function CheckValidAgileAccount($domain, $username, $password)
		{
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, "https://".$domain.".agilecrm.com/dev/api/contacts?page_size=20");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);
			
			$response = curl_exec($ch);
			$info = curl_getinfo($ch);
			
			curl_close($ch);
			
			$status=$info['http_code'];
			
			return $status;
		}
		
		//---Agile CRM API
		public static function AgileCreateContact($address,$city,$state,$country,$email,$lead_score,$star_value,$tags,$first_name,$last_name,$title,$phone,$team_member,$date_of_joining,$domain,$username,$password)
		{
			$address = array("address"=>$address, "city"=>$city, "state"=>$state, "country"=>$country);
			
			$contact_email = $email;
			
			$str = array("lead_score"=>"$lead_score", "star_value"=>"$star_value", "tags"=>array($tags),
			"properties"=>array(
			array("name"=>"first_name", "value"=>$first_name, "type"=>"SYSTEM"),
			array("name"=>"last_name", "value"=>$last_name, "type"=>"SYSTEM"),
			array("name"=>"email", "value"=>$contact_email, "type"=>"SYSTEM"),  
			array("name"=>"title", "value"=>$title, "type"=>"SYSTEM"),
			array("name"=>"address", "value"=>json_encode($address), "type"=>"SYSTEM"),
			array("name"=>"phone", "value"=>$phone, "type"=>"SYSTEM"),
			array("name"=>"TeamNumbers", "value"=>$team_member, "type"=>"CUSTOM"),
			array("name"=>"Date Of Joining", "value"=>$date_of_joining,	"type"=>"CUSTOM")));
			
			$data=json_encode($str);
			
			$headers = array();
			$headers[] = "Accept: application/json";
			$headers[] = "Content-Type: application/json";
			
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, "https://".$domain.".agilecrm.com/dev/api/contacts");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_USERPWD, $username . ":" .$password);//password use api key
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			
			$result = curl_exec($ch);
			$info = curl_getinfo($ch);
			
			curl_close ($ch);
			
			//print_r($result);die;
			//return $result;
			
			$status=$info['http_code'];
			
			return $status;
		}
		
		//---Hubspot CRM API
		public static function hub_get_contact($access_token)
		{
			$headers = array();
			$headers[] = "Accept: application/json";
			$headers[] = "Authorization: Bearer $access_token";
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://api.hubapi.com/contacts/v1/lists/all/contacts/all");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$output=curl_exec($ch);
			curl_close ($ch);
			$assoc =true;
			$depth = 250;
			$options = 250;
			$r = json_decode ($output, $assoc, $depth, $options);
			echo "<br>";
			echo "<pre>";
			print_r($r);
			echo "</pre>";
		}
		
		//---Hubspot CRM API
		public static function hub_create_contact($email,$first_name,$last_name,$website,$company,$phone,$city,$state,$zip,$authtoken)
		{
			$arr = array(
            'properties' => array(
			array(
			'property' => 'email',
			'value' => $email
			),
			array(
			'property' => 'firstname',
			'value' => $first_name
			),
			array(
			'property' => 'lastname',
			'value' => $last_name
			),
			array(
			'property' => 'website',
			'value' => $website
			),
			array(
			'property' => 'company',
			'value' => $company
			),
			array(
			'property' => 'phone',
			'value' => $phone
			),
			array(
			'property' => 'city',
			'value' => $city
			),
			array(
			'property' => 'state',
			'value' => $state
			),
			array(
			'property' => 'zip',
			'value' => $zip
			)
            )
			);
			$data=json_encode($arr);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,'https://api.hubapi.com/contacts/v1/contact/');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_POST, TRUE); 
			curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
			$headers = array();
			$headers[] ="Authorization: Bearer $authtoken";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			$response = curl_exec($ch);
			curl_close($ch);
			
			$assoc =true;
			$depth = 250;
			$options = 250;
			$r = json_decode ($response, $assoc, $depth, $options);
			echo "<br>";
			echo "<pre>";
			print_r($r);
			echo "</pre>";
		}
		
		//---Zoho CRM API
		public static function ZohoOauthToken($email_id, $password)
		{
			$ch = curl_init();
			
			curl_setopt($ch,CURLOPT_URL,'https://accounts.zoho.com/apiauthtoken/nb/create?SCOPE=ZohoCRM/crmapi&EMAIL_ID='.$email_id.'&PASSWORD='.$password);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_HEADER, false);
			
			$response=curl_exec($ch);
			
			curl_close($ch);
			
			$result=explode(" ",$response);
			$result1=explode("\n",$result[5]);
			$Oauth=explode("=",$result1[1]);
			if($Oauth[0]=='AUTHTOKEN' && $Oauth[1]!='')
			{
				return $Oauth[1];
			}
			else
			{
				return "";
			}
		}
		
		//---Zoho CRM API
		public static function zoho_get_contact($authtoken)
		{
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL,'https://crm.zoho.com/crm/private/json/Contacts/getRecords?authtoken='.$authtoken.'&scope=crmapi&newFormat=1');
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_HEADER, false); 
			$output=curl_exec($ch);
			curl_close($ch);
			$assoc =true;
			$depth = 250;
			$options = 250;
			$r = json_decode ($output, $assoc, $depth, $options);
			echo "<br>";
			echo "<pre>";
			print_r($r);
			echo "</pre>";
		}
		
		//---Zoho CRM API
		public static function ZohoTokenExpiryCheck($authtoken)
		{
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL,'https://crm.zoho.com/crm/private/json/Contacts/getRecords?authtoken='.$authtoken.'&scope=crmapi&newFormat=1');
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_HEADER, false); 
			$output=curl_exec($ch);
			curl_close($ch);
			$assoc =true;
			$depth = 250;
			$options = 250;
			$r = json_decode ($output, $assoc, $depth, $options);
			//echo "<br>";
			//echo "<pre>";
			//print_r($r['response']);
			//print_r($r['response']['error']);
			//echo "</pre>";
			
			if(isset($r['response']['error']))
			{
				return "expire";
			}
			else
			{
				return "success";
			}
		}
		
		//---Zoho CRM API
		public static function ZohoCreateContact($first_name,$last_name,$email,$department,$phone,$fax,$mobile,$assistant,$authtoken)
		{
			$xml= '<?xml version="1.0" encoding="UTF-8"?>
			<Contact>
			<row no="1">
			<FL val="First Name">'.$first_name.'</FL>
			<FL val="Last Name">'.$last_name.'</FL>
			<FL val="Email">'.$email.'</FL>
			<FL val="Department">'.$department.'</FL>
			<FL val="Phone">'.$phone.'</FL>
			<FL val="Fax">'.$fax.'</FL>
			<FL val="Mobile">'.$mobile.'</FL>
			<FL val="Assistant">'.$assistant.'</FL>
			</row>
			</Contact>';
			
			$auth=$authtoken;
			
			$url ="https://crm.zoho.com/crm/private/xml/Contacts/insertRecords";
			$query="authtoken=".$auth."&scope=crmapi&newFormat=1&xmlData=".$xml;
			
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
			
			$response = curl_exec($ch);
			
			curl_close($ch);
			
			//print_r($response);
			return $response;
		}
		
		//---Infusionsoft CRM API
		public static function infusionsoft_get_contact($access_token)
		{
			$ch = curl_init();
			curl_setopt($ch,CURLOPT_URL,'https://api.infusionsoft.com/crm/rest/v1/contacts?access_token='.$access_token);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
			curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch,CURLOPT_HEADER, false); 
			$output=curl_exec($ch);
			$assoc =true;
			$depth = 250;
			$options = 250;
			$r = json_decode ($output, $assoc, $depth, $options);
			echo "<br>";
			echo "<pre>";
			print_r($r);
			echo "</pre>";
		}
		
		//---Infusionsoft CRM API
		public static function infusionsoft_create_contact($access_token,$email_id,$phone,$fax,$comapany_name,$job_title)
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,'https://api.infusionsoft.com/crm/rest/v1/contacts?access_token='.$access_token);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
			curl_setopt($ch, CURLOPT_POST, TRUE); 
			curl_setopt($ch, CURLOPT_POSTFIELDS,'{
			"email_addresses": [
			{
			"email":"'. $email_id.'",
			"field": "EMAIL1"
			}
			],
			"phone_numbers": [
			{
			"field": "PHONE1",
			"number": "'.$phone.'"
			}
			],
			"fax_numbers": [
			{
			"field": "FAX1",
			"number": "'.$fax.'"
			}
			],
			"company": {
			"company_name": "'.$comapany_name.'"
			},
			"job_title": "'.$job_title.'"
			}');
			$response = curl_exec($ch);
			curl_close($ch);
			
			$assoc =true;
			$depth = 250;
			$options = 250;
			$r = json_decode ($response, $assoc, $depth, $options);
			echo "<br>";
			echo "<pre>";
			print_r($r);
			echo "</pre>";
		}
		
		//---Autometive eLead CRM
		public static function eLeadCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code)
		{
			$phone = str_replace("(","",$phone);
			$phone = str_replace(")","",$phone);
			$phone = str_replace(" ","",$phone);
			$phone = str_replace("-","",$phone);
			
			$provider_phone = str_replace("(","",$provider_phone);
			$provider_phone = str_replace(")","",$provider_phone);
			$provider_phone = str_replace(" ","",$provider_phone);
			$provider_phone = str_replace("-","",$provider_phone);
			
			$vendor_phone = str_replace("(","",$vendor_phone);
			$vendor_phone = str_replace(")","",$vendor_phone);
			$vendor_phone = str_replace(" ","",$vendor_phone);
			$vendor_phone = str_replace("-","",$vendor_phone);
			
			$msg = '<?xml version="1.0" encoding="UTF-8"?> 
			<?adf version="1.0"?> 
			<adf> 
			<prospect> 
			<id source="3736">Pownder</id>  
			<requestdate>'.date("Y-m-d H:i:s").'</requestdate>  
			<customer>
			<contact> 
			<name part="first">'.$first_name.'</name> 
			<name part="last">'.$last_name.'</name> 
			<email>'.$email.'</email> 
			<phone type="'.$phone_type.'" time="'.$phone_time.'" preferredcontact="1">'.$phone.'</phone>
			<address> 
			<street line="1">'.$address.'</street> 
			<city>'.$city.'</city> 
			<regioncode>'.$state.'</regioncode> 
			<postalcode>'.$post_code.'</postalcode> 
			</address> 
			</contact> 
			</customer> 
			<provider>
			<name part="full">'.$provider_name.'</name> 
			<sales></sales> 
			<url>'.$provider_url.'</url> 
			<email>'.$provider_email.'</email> 
			<phone>'.$provider_phone.'</phone> 
			<contact primarycontact="1"> 
			<name part="full">'.$provider_name.'</name> 
			<email>'.$provider_email.'</email> 
			<phone type="'.$phone_type.'" time="'.$phone_time.'">'.$provider_phone.'</phone>
			<address> 
			<street line="1"></street> 
			<street line="2"></street> 
			<city>'.$provider_city.'</city> 
			<regioncode>'.$provider_state.'</regioncode> 
			<postalcode>'.$provider_zip.'</postalcode> 
			<country>US</country> 
			</address> 
			</contact> 
			</provider> 
			<vendor>
			<vendorname>'.$vendor_name.'</vendorname> 
			<contact primarycontact="1"> 
			<email>'.$vendor_email.'</email> 
			<phone type="'.$phone_type.'" time="'.$phone_time.'">'.$vendor_phone.'</phone>
			<address> 
			<street line="1">'.$vendor_address.'</street> 
			<city>'.$vendor_city.'</city> 
			<regioncode>'.$vendor_state.'</regioncode> 
			<postalcode>'.$vendor_zip.'</postalcode> 
			<country>US</country> 
			<url>'.$vendor_url.'</url> 
			</address> 
			</contact> 
			</vendor> 
			</prospect> 
			</adf>';
			
			$Campaign = DB::table('campaigns')->where('id', $campaign_id)->first();
			
			/* $url = 'https://api.sendgrid.com/';
				$user = 'help@constacloud.in';
				$pass = 'main1234';
				$params = array(
				'api_user'  => $user,
				'api_key'   => $pass,
				'to'        => $Campaign->email_crm,
				'bcc'        => $Campaign->email_bcc,
				'cc'        => $Campaign->email_cc,
				'subject'   => $Campaign->email_subject,
				'text'      => $msg,
				'from'      => $Campaign->email_from,
				'fromname'  => $Campaign->email_subject,
			); */
			$cc = $Campaign->email_cc;
			$bcc = $Campaign->email_bcc;
			$to = $Campaign->email_crm;
			//$to = 'sohan.constacloud@gmail.com';
			$from = $Campaign->email_from;
			$subject = $Campaign->email_subject;
			$header = "MIME-Version: 1.0\r\n";
			$header.="Content-type: text/xml; charset=UTF-8\r\n";
			$header.="To: Pownder <".$to.">\r\n"; 
			$header.="From:".$from."\r\n";
			$header.="Cc:".$cc."\r\n";
			$header.="Bcc:".$bcc."\r\n";
			
			if(mail($to,$subject,$msg,$header))
			{
				return 'success';
			}
			else
			{
				return '';
			}
			
			/* $request = $url.'api/mail.send.json';
				$session = curl_init($request);
				curl_setopt($session, CURLOPT_POST, true);
				curl_setopt($session, CURLOPT_POSTFIELDS, $params);
				curl_setopt($session, CURLOPT_HEADER, false);
				curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
				curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($session);
				curl_close($session);
				
				$data = json_decode($response);
			return $data->message; */
		}
		
		//---Autometive Momentum CRM
		public static function MomentumCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code)
		{
			$phone = str_replace("(","",$phone);
			$phone = str_replace(")","",$phone);
			$phone = str_replace(" ","",$phone);
			$phone = str_replace("-","",$phone);
			
			$provider_phone = str_replace("(","",$provider_phone);
			$provider_phone = str_replace(")","",$provider_phone);
			$provider_phone = str_replace(" ","",$provider_phone);
			$provider_phone = str_replace("-","",$provider_phone);
			
			$vendor_phone = str_replace("(","",$vendor_phone);
			$vendor_phone = str_replace(")","",$vendor_phone);
			$vendor_phone = str_replace(" ","",$vendor_phone);
			$vendor_phone = str_replace("-","",$vendor_phone);
			
			$msg = '<?xml version="1.0"?>
			<?adf version="1.0"?>
			<adf>
			<prospect status="'.$prospect.'">
			<customer>
			<contact>
			<address>
			<city>'.$city.'</city>
			</address>
			<email>'.$email.'</email>
			<name part="first" type="individual">'.$first_name.'</name>
			<name part="full" type="individual">'.$first_name.' '.$last_name.'</name>
			<name part="last" type="individual">'.$last_name.'</name>
			<phone type="'.$phone_type.'" time="'.$phone_time.'" preferredcontact="1">'.$phone.'</phone>
			</contact>
			</customer>
			<provider>
			<name>'.$provider_name.'</name>
			<name part="full" type="business">Pownder<img src="https://s.w.org/images/core/emoji/72x72/2122.png" alt="™" class="wp-smiley" style="height: 1em; max-height: 1em;" /></name>
			<service>'.$service_name.'</service>
			</provider>
			<requestdate>'.date("Y-m-d\TH:i:s-0700").'</requestdate>
			<vendor>
			<dealershipname>'.$vendor_name.'</dealershipname>
			<vendorname>Pownder</vendorname>
			</vendor>
			</prospect>
			</adf>';
			
			$Campaign = DB::table('campaigns')->where('id', $campaign_id)->first();
			
			$toArray = explode(",",str_replace(" ","",$Campaign->email_crm));
			$ccArray = explode(",",str_replace(" ","",$Campaign->email_cc));
			$ToCC = array_merge($toArray,$ccArray);
			
			$json_string = array(
			'to' => $ToCC,
			'category' => 'test_category'
			);
			
			$bcc = '';
			$bccArray = explode(",",str_replace(" ","",$Campaign->email_bcc));
			if(count($bccArray) > 0)
			{
				$bcc = $bccArray[0];
			}
			
			$url = 'https://api.sendgrid.com/';
			$user = 'help@constacloud.in';
			$pass = 'main1234';
			$params = array(
			'api_user'  => $user,
			'api_key'   => $pass,
			'x-smtpapi' => json_encode($json_string),
			'to'        => 'example1@sendgrid.com',
			'bcc'        => $bcc,
			'cc'        => '',
			'subject'   => $Campaign->email_subject,
			'text'      => $msg,
			'from'      => $Campaign->email_from,
			'fromname'  => $Campaign->email_subject,
			);
			
			$request =  $url.'api/mail.send.json';
			$session = curl_init($request);
			curl_setopt($session, CURLOPT_POST, true);
			curl_setopt($session, CURLOPT_POSTFIELDS, $params);
			curl_setopt($session, CURLOPT_HEADER, false);
			curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
			curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($session);
			curl_close($session);
			
			$data = json_decode($response);
			return $data->message;
		}
		
		//---Autometive DealerSocket CRM
		public static function DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code)
		{
			//$phone = str_replace("(","",$phone);
			//$phone = str_replace(")","",$phone);
			//$phone = str_replace(" ","",$phone);
			//$phone = str_replace("-","",$phone);
			
			//$provider_phone = str_replace("(","",$provider_phone);
			//$provider_phone = str_replace(")","",$provider_phone);
			//$provider_phone = str_replace(" ","",$provider_phone);
			//$provider_phone = str_replace("-","",$provider_phone);
			
			//$vendor_phone = str_replace("(","",$vendor_phone);
			//$vendor_phone = str_replace(")","",$vendor_phone);
			//$vendor_phone = str_replace(" ","",$vendor_phone);
			//$vendor_phone = str_replace("-","",$vendor_phone);
			
			$msg = '<?xml version="1.0"?>
			<?adf version="1.0"?>
			<adf>
			<prospect>
			<requestdate>'.date("Y-m-d\TH:i:s-10:00").'</requestdate>
			<customer>
			<contact>
            <name part="first">'.$first_name.'</name>
            <name part="last">'.$last_name.'</name>
            <name part="middle"></name>
            <email>'.$email.'</email>
            <phone type="'.$phone_type.'" time="'.$phone_time.'">'.$phone.'</phone>
            <address type="home">
			<street line="1">'.$address.'</street>
			<street line="2"></street>
			<city>'.$city.'</city>
			<regioncode>'.$state.'</regioncode>
			<postalcode>'.$post_code.'</postalcode>
			<country>US</country>
            </address>
			</contact>
			</customer>
			<provider>
			<name part="full">'.$provider_name.'</name>
			<service>'.$service_name.'</service>
			<url>'.$provider_url.'</url>
			<email>'.$provider_email.'</email>
			<contact primarycontact="1">
            <name part="full" type="business">'.$provider_name.'</name>
            <phone type="voice" time="day">'.$provider_phone.'</phone>
            <phone type="'.$phone_type.'" time="'.$phone_time.'">'.$provider_phone.'</phone>
            <address>
			<street line="1"></street>
			<city>'.$provider_city.'</city>
			<regioncode>'.$provider_state.'</regioncode>
			<postalcode>'.$provider_zip.'</postalcode>
			<country>US</country>
            </address>
			</contact>
			</provider>
			</prospect>
			</adf>';
			
			$Campaign = DB::table('campaigns')->where('id', $campaign_id)->first();
			
			if($Campaign->crm_account == 'DealerSocket')
			{
				$toArray = explode(",",str_replace(" ","",$Campaign->email_crm));
				$ccArray = explode(",",str_replace(" ","",$Campaign->email_cc));
				$ToCC = array_merge($toArray,$ccArray);
				
				$json_string = array(
				'to' => $ToCC,
				'category' => 'test_category'
				);
				
				$bcc = '';
				$bccArray = explode(",",str_replace(" ","",$Campaign->email_bcc));
				if(count($bccArray) > 0)
				{
					$bcc = $bccArray[0];
				}
				
				$url = 'https://api.sendgrid.com/';
				$user = 'help@constacloud.in';
				$pass = 'main1234';
				$params = array(
				'api_user'  => $user,
				'api_key'   => $pass,
				'x-smtpapi' => json_encode($json_string),
				'to'        => 'example1@sendgrid.com',
				'bcc'        => $bcc,
				'cc'        => '',
				'subject'   => $Campaign->email_subject,
				'text'      => $msg,
				'from'      => $Campaign->email_from,
				'fromname'  => $Campaign->email_subject,
				);
				
				$request = $url.'api/mail.send.json';
				$session = curl_init($request);
				curl_setopt($session, CURLOPT_POST, true);
				curl_setopt($session, CURLOPT_POSTFIELDS, $params);
				curl_setopt($session, CURLOPT_HEADER, false);
				curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
				curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
				$response = curl_exec($session);
				curl_close($session);
				
				$data = json_decode($response);
				return $data->message;
			}
			else
			{
				$cc = $Campaign->email_cc;
				$bcc = $Campaign->email_bcc;
				$to = $Campaign->email_crm;
				$from = $Campaign->email_from;
				$subject = $Campaign->email_subject;
				$header = "MIME-Version: 1.0\r\n";
				$header.="Content-type: text/xml; charset=UTF-8\r\n";
				$header.="To: Pownder <".$to.">\r\n"; 
				$header.="From:".$from."\r\n";
				$header.="Cc:".$cc."\r\n";
				$header.="Bcc:".$bcc."\r\n";
				
				if(mail($to,$subject,$msg,$header))
				{
					return 'success';
				}
				else
				{
					return '';
				}
			}
		}
	}																			