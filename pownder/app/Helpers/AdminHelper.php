<?php
	namespace App\Helpers;
	
	use DB;
	use Session;
	
	class AdminHelper
	{
		public static function MonthLead($StartDate, $EndDate)
		{
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')
				->where(function ($query) use($ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', Session::get('manager_id'));
					}
				})
				->where(function ($query){
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})
				->whereDate('created_time', '>=', $StartDate)->whereDate('created_time', '<=', $EndDate)->count('id');
				return $FacebookAdsLeadUsers;
			}
			else
			{
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->whereDate('created_time', '>=', $StartDate)->whereDate('created_time', '<=', $EndDate)
				->where(function ($query){
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})->count('id');
				return $FacebookAdsLeadUsers;
			}
		}
		
		public static function DateWiseAllLeads($created_time)
		{
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')
				->where(function ($query) use($ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', Session::get('manager_id'));
					}
				})
				->where(function ($query){
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})
				->whereDate('created_time', '=', $created_time)->count('id');
				return $FacebookAdsLeadUsers;
			}
			else
			{
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->whereDate('created_time', '=', $created_time)
				->where(function ($query){
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})->count('id');
				return $FacebookAdsLeadUsers;
			}
		}
		
		public static function DateWiseFacebookLeads($created_time)
		{
			if(Session::get('user_category')=='user')
			{
				$ShowLead = ManagerHelper::ManagerLeadMenuAction();
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')
				->where(function ($query) use($ShowLead){
					if($ShowLead=='Allotted Leads')
					{
						$query->where('manager_id', Session::get('manager_id'));
					}
				})
				->where(function ($query){
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})
				->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $created_time)->count('id');
				return $FacebookAdsLeadUsers;
			}
			else
			{
				$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', '=', $created_time)
				->where(function ($query){
					if(Session::get('client_id') > 0)
					{
						$query->where('client_id', Session::get('client_id'));
					}
				})->count('id');
				return $FacebookAdsLeadUsers;
			}
		}
		
		public static function CampaignLastLead($campaign_id)
		{
			$CRMContact = DB::table('crm_contacts')->select('updated_at')->where('campaign_id', $campaign_id)->where('is_deleted', 0)->orderBy('id', 'DESC')->first();
			
			if(is_null($CRMContact))
			{
				return '';
			}
			else
			{
				return date('m/d/Y h:i A',strtotime($CRMContact->updated_at));
			}
		}
		
		public static function LeadOwner($user_id)
		{
			$User = DB::table('users')->select('client_id', 'name', 'category', 'created_user_id')->where('id', $user_id)->first();
			
			if(is_null($User))
			{
				return '';
			}
			else
			{
				$OwnerName = $User->name;
				$OwnerCategory = $User->category;
				
				if($OwnerCategory == 'admin')
				{
					return $OwnerName.' (Own)';
				}
				
				if($OwnerCategory == 'vendor')
				{
					return $OwnerName.' (Vendor)';
				}
				
				if($OwnerCategory == 'client')
				{
					$Client = DB::table('clients')->where('id',$User->client_id)->first();
					
					if($Client->vendor_id > 0)
					{
						return $OwnerName." (Vendor's Client)";
					}
					else
					{
						return $OwnerName.' (Client)';
					}
				}
				
				if($OwnerCategory == 'user')
				{
					$UserType = DB::table('managers')->where('id',$User->manager_id)->first();
					if($UserType->category == 'admin')
					{
						return $OwnerName.' (User)';
					}
					elseif($UserType->category == 'vendor')
					{
						return $OwnerName." (Vendor's User)";
					}
					elseif($UserType->category == 'client')
					{
						return $OwnerName." (Client's User)";
					}
				}
			}
		}
		
		public static function ClientClicks($client_id, $s_date, $e_date)
		{
			if($s_date == '' || $e_date == '')
			{
				$s_date = date('Y-m').'-01';
				$e_date = date('Y-m-d');
			}
			
			$Clicks = 0;
			$CampaignArray = ClientHelper::ClientLeadCPs($client_id, $s_date, $e_date);
			
			$TotalClicks = DB::table('facebook_campaign_values')->select('clicks', 'campaign_id')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $s_date)->whereDate('campaign_date', '<=', $e_date)->sum('clicks');
			
			$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $s_date, $e_date);
			$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $s_date, $e_date);
			
			if($CampaignLeads > 0)
			{
				$Clicks = $Clicks + (($TotalClicks / $CampaignLeads) * $clientCampaignLeads);
			}
			
			return ceil($Clicks);
		}
		
		public static function ClientSpents($client_id, $s_date, $e_date)
		{
			if($s_date == '' || $e_date == '')
			{
				$s_date = date('Y-m').'-01';
				$e_date = date('Y-m-d');
			}
			
			$Spents = 0;
			$CampaignArray = ClientHelper::ClientLeadCPs($client_id, $s_date, $e_date);
			
			$TotalSpend = DB::table('facebook_campaign_values')->select('spend')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $s_date)->whereDate('campaign_date', '<=', $e_date)->sum('spend');
			$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $s_date, $e_date);
			$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $s_date, $e_date);
			
			if($CampaignLeads > 0)
			{
				$Spents = $Spents + (($TotalSpend / $CampaignLeads) * $clientCampaignLeads);
			}
			
			return $Spents;
		}
		
		public static function ClientCTR($client_id, $s_date, $e_date)
		{
			if($s_date == '' || $e_date == '')
			{
				$s_date = date('Y-m').'-01';
				$e_date = date('Y-m-d');
			}
			
			$CTR = 0;
			$CampaignArray = ClientHelper::ClientLeadCPs($client_id, $s_date, $e_date);
			
			$TotalCtr = DB::table('facebook_campaign_values')->select('ctr')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $s_date)->whereDate('campaign_date', '<=', $e_date)->sum('ctr');
			
			$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $s_date, $e_date);
			$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $s_date, $e_date);
			
			if($CampaignLeads > 0)
			{
				$CTR = $CTR + (($TotalCtr / $CampaignLeads) * $clientCampaignLeads);
			}
			
			return $CTR;
		}
		
		public static function ClientFreq($client_id, $s_date, $e_date)
		{
			if($s_date == '' || $e_date == '')
			{
				$s_date = date('Y-m').'-01';
				$e_date = date('Y-m-d');
			}
			
			$Freq = 0;
			$CampaignArray = ClientHelper::ClientLeadCPs($client_id, $s_date, $e_date);
			
			$TotalFreq = DB::table('facebook_campaign_values')->select('frequency')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $s_date)->whereDate('campaign_date', '<=', $e_date)->sum('frequency');
			$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $s_date, $e_date);
			$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $s_date, $e_date);
			
			if($CampaignLeads > 0)
			{
				$Freq = $Freq + (($TotalFreq / $CampaignLeads) * $clientCampaignLeads);
			}
			
			return $Freq;
		}
		
		public static function ClientReach($client_id, $s_date, $e_date)
		{
			if($s_date == '' || $e_date == '')
			{
				$s_date = date('Y-m').'-01';
				$e_date = date('Y-m-d');
			}
			
			$Reach = 0;
			$CampaignArray = ClientHelper::ClientLeadCPs($client_id, $s_date, $e_date);
			
			$TotalReach = DB::table('facebook_campaign_values')->select('reach')->whereIn('campaign_id', array_unique($CampaignArray))->whereDate('campaign_date', '>=', $s_date)->whereDate('campaign_date', '<=', $e_date)->sum('reach');
			$CampaignLeads = ClientHelper::CampaignLeads(array_unique($CampaignArray), $s_date, $e_date);
			$clientCampaignLeads = ClientHelper::clientCampaignLeads($client_id, array_unique($CampaignArray), $s_date, $e_date);
			
			if($CampaignLeads > 0)
			{
				$Reach = $Reach + (($TotalReach / $CampaignLeads) * $clientCampaignLeads);
			}
			
			return ceil($Reach);
		}
	}																								