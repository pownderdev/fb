<?php
	namespace App\Helpers;
	
	use DB;
	
	class WebhookHelper
	{
		//--App Access Token
		public static function AppAccessToken($client_id, $client_secret)
		{
			$client_id = '246735105809032'; 
			$client_secret = '629464d21ed809347c652afcdd014b9f';
			
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.10/oauth/access_token?client_id=$client_id&client_secret=$client_secret&grant_type=client_credentials");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"GET");
			
			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			} 
			curl_close ($ch);
			$data = json_decode($result,true);
			//echo "<pre>";
			//print_r($data);
			return $data['access_token'];
		}
		
		//--App Access Token
		public static function getpages($access_token)
		{
			// Oauth access_token
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.10/me/accounts?limit=2500&access_token=".$access_token);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close ($ch);
			echo "<pre>";
			print_r(json_decode($result,true));
		}
		
		//Page subscription to app
		public static function page_app_subs($page_id,$access_token)
		{
			// Page access_token
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.10/$page_id/subscribed_apps");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "access_token=".$access_token);
			curl_setopt($ch, CURLOPT_POST, 1);
			
			$headers = array();
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			
			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close ($ch);
			echo "<pre>";
			print_r(json_decode($result,true));
		}
		
		public static function subscription($app_id,$app_access_token)
		{
			// App access_token
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.10/$app_id/subscriptions");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "object=page&callback_url=https%3A%2F%2Fapiintegrationexperts.com%2Ftrilliontrade%2Ffacebook%2Fcallback.php&fields=leadgen&verify_token=test&access_token=".$app_access_token);
			curl_setopt($ch, CURLOPT_POST, 1);
			
			$headers = array();
			$headers[] = "Content-Type: application/x-www-form-urlencoded";
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			
			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close ($ch);
			echo "<pre>";
			print_r(json_decode($result,true));
		}
		
		//GET LEADS DETAIL
		public static function get_lead_details($form_id,$access_token)
		{
			$form_id='260086721143698';
			$access_token='EAADgZA35V2ogBAHef9Jx1flC1fpP2aJdozxhZA2AArf5eyvAfPRiLzDaGctZB3yJAwhhYJslCiZCqjZAg3RrQ7pTEMP0lZA8fXGbW5XgzZBLZAClMSejzJfQJO6V2F5UIQbqiu8RIHIZBnfZBjxvRG4U9aPNoTesNbzljtyMep5i4wdwZDZD';
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.10/$form_id/leads?limit=25&access_token=".$access_token);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close ($ch);
			echo "<pre>";
			$re=json_decode($result,true);
			print_r($re);
			$data=$re['data'];
			
			foreach ($data as $array)
			{
				$lead_id = $array['id'];
				$field_data=$array['field_data'];
				foreach ($field_data as $arr)
				{
					echo $arr['name']."=";
					echo $arr['values']['0']."<br>";
				}
				$ch = curl_init();
				
				curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/v2.10/$lead_id?fields=ad_id%2Cadset_id%2Cad_name%2Cadset_name%2Ccampaign_name%2Ccampaign_id&access_token=".$access_token);
				
				//$curl = curl_init('https://graph.facebook.com/v2.9/'.$lead_id. '?fields=id,created_time,ad_id,ad_name,adset_id,adset_name,campaign_id,campaign_name,form_id,field_data&access_token='. $access_token);
				
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				
				$result = curl_exec($ch);
				if (curl_errno($ch)) {
					echo 'Error:' . curl_error($ch);
				}
				curl_close ($ch);
				echo "<pre>";
				print_r(json_decode($result,true));
			}
		}
		
	}																						