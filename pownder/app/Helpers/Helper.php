<?php
	namespace App\Helpers;
	use DB;
	use Session;
	
	class Helper
	{
		public static function homePageURL()
		{
			return url('/');
		}
		
		public static function emailPhoneFormat($Phone)
		{
			$Phone = str_replace(') ', '-', $Phone);
			$Phone = str_replace('(', '', $Phone);
			return $Phone;
		}
		
		public static function NumberFormat($Number)
		{
			$Number = str_replace(",", "", $Number);
			if($Number >= 1000000)
			{
				$new = number_format($Number/1000000);
				$last = substr($new,-3);
				if($last == '.00')
				{
					return number_format($Number/1000000,0).'M';
				}
				else
				{
					return number_format($Number/1000000,2).'M';
				}
			}
			else if($Number >= 1000)
			{
				$new = number_format($Number/1000);
				$last = substr($new,-3);
				if($last == '.00')
				{
					return number_format($Number/1000,0).'K';
				}
				else
				{
					return number_format($Number/1000,2).'K';
				}
			}
			else
			{
				$last = substr($Number,-3);
				if($last == '.00')
				{
					return number_format($Number,0);
				}
				else
				{
					return number_format($Number,2);
				}
			}
		}
		
		public static function UserName($user_id)
		{
			$user = DB::table('users')->where('id', $user_id)->first();
			if(is_null($user))
			{
				return '';
			}
			else
			{
				return $user->name;
			}
		}
		
		public static function UserCategory($user_id)
		{
			$user = DB::table('users')->where('id', $user_id)->first();
			if(is_null($user))
			{
				return '';
			}
			else
			{
				return $user->category;
			}
		}
		
		public static function FacebookAdsAccounts($facebook_account_id)
		{
			$UserArray = array();
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			
			$FacebookAdsAccounts = DB::table('facebook_ads_accounts')->where('facebook_account_id', $facebook_account_id)->whereIn('updated_user_id', $UserArray)->where('is_deleted', 0)->orderBy('facebook_account_name', 'ASC')->orderBy('account_id', 'ASC')->get();
			return $FacebookAdsAccounts;
		}
		
		public static function FacebookAccounts()
		{
			$UserArray = array();
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			
			$FacebookAccounts = DB::table('facebook_accounts')->whereIn('updated_user_id', $UserArray)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			return $FacebookAccounts;
		}
		
		public static function UserTotalLeads()
		{
			$UserArray = array();
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			
			$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->whereIn('created_user_id', $UserArray)->get();
			return count($FacebookAdsLeadUsers);
		}
		
		public static function CampaignTotalContact($campaign_id)
		{
			$CRMContacts = DB::table('crm_contacts')->select('id')->where('campaign_id', $campaign_id)->get();
			return count($CRMContacts);
		}
		
		public static function GroupCategoryClients($group_category_id)
		{
			$ShowClient = '';
			$vendor_id = 0;
			$UserArray = array();
			if(Session::get('user_category')=='user')
			{
				$ShowClient = ManagerHelper::ManagerClientMenuAction();
				$UserArray[] = Session::get('user_id');
				
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				if(ManagerHelper::ManagerCategory()=='vendor')
				{
					$Vendor = DB::table('users')->where('id',$manager->created_user_id)->first();
					$vendor_id = $Vendor->vendor_id;
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
				
				$vendor_id = Session::get('vendor_id');
			}
			
			$Clients=DB::table('clients')
			->select('id', 'name', 'email','client_no')
			->where('group_category_id', $group_category_id)
			->where(function ($query) use($UserArray, $ShowClient, $vendor_id){
				if($ShowClient=='Allotted Clients')
				{
					$query->where('clients.manager_id', Session::get('manager_id'));
				}
				else
				{
					if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
					{
						$query->where('clients.created_user_id', '!=', 0);
					}
					else
					{
						$query->where('clients.vendor_id', $vendor_id);
					}
				}
			})
			->where('is_deleted', 0)->where('status', 1)->orderBy('name', 'ASC')->orderBy('email', 'ASC')->get();
			
			return $Clients;
		}
		
		public static function ClientGroupCategory($client_id)
		{
			$ClientGroupCategory=DB::table('clients')->select('group_category_id')->where('id', $client_id)->first();
			if(is_null($ClientGroupCategory))
			{
				return 0;
			}
			else
			{
				return $ClientGroupCategory->group_category_id;
			}
		}
		
		public static function GroupCategories()
		{
			$Result=DB::table('group_categories')->select('id', 'name')->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			return $Result;
		}
		
		public static function GroupCategoryName($id)
		{
			$Result=DB::table('group_categories')->select('name')->where('id', $id)->first();
			
			if(is_null($Result))
			{
				return '';
			}
			else
			{
				return $Result->name;
			}
		}
		
		public static function GroupSubCategoryName($id)
		{
			$Result=DB::table('group_sub_categories')->select('name')->where('id', $id)->first();
			
			if(is_null($Result))
			{
				return '';
			}
			else
			{
				return $Result->name;
			}
		}
		
		public static function DistanceLatLon($lat1, $lon1, $lat2, $lon2) 
		{
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			
			return $miles;
		}
		
		public static function PhoneNumber($phone_number)
		{
			$matches = null;
			preg_match_all('!\d+!', $phone_number, $matches);
			
			$string='';
			$number=$matches[0];
			for($i=0;$i<count($number);$i++)
			{
				$string=$string.$number[$i];
			}
			
			$newstring = substr($string, -10);
			
			if(strlen($newstring)==10)
			{
				$first=substr($newstring,0,3);
				$second=substr($newstring,3,3);
				$third=substr($newstring,6,10);
				
				$new_phone_number = '('.$first.') '.$second.'-'.$third;
				
				return $new_phone_number;
			}
			else
			{
				return $phone_number;
			}	
		}
		
		public static function GenerateRandomPassword($length,$useUpperCase,$useLowerCase,$useNumbers,$useSpecialChars)
		{
			//define strings for each category
			$upperCase = "ABCDEFGHJKPQRSTUXYZ";
			$lowerCase = "abcdefghjkopqrstuxyz";
			$numbers = "23456789";
			$specialChars = "!?~@#-_+<>[]{}()";
			
			//adds characters from category if selected
			$toUse = "";
			$countToUse = 0;
			if($useUpperCase == "true") { $toUse .= $upperCase; $countToUse++; }
			if($useLowerCase == "true") { $toUse .= $lowerCase; $countToUse++; }
			if($useNumbers == "true") { $toUse .= $numbers; $countToUse++; }
			if($useSpecialChars == "true") { $toUse .= $specialChars;  $countToUse++; }
			
			if($length < $countToUse) //if number entered is less that the count of selected character sets.
			{
				return "<i--><b>'number of characters'</b> can not be less than selection of characters to use.
				";
				break;
			}
			
			//no errors, generate the password
			$password = "";
			for ($i = 0; $i < $length; $i++)
			{
				$password .= $toUse[(rand() % strlen($toUse))];
			}
			
			//define the array to return
			$passwordArray[0] = $password;
			$passwordArray[1] = $upperCase;
			$passwordArray[2] = $lowerCase;
			$passwordArray[3] = $numbers;
			$passwordArray[4] = $specialChars;
			
			//check that the password contains at LEAST 1 character from each selected category
			$hasUpper = strpbrk($passwordArray[0],$passwordArray[1]);
			$hasLower = strpbrk($passwordArray[0],$passwordArray[2]);
			$hasNumber = strpbrk($passwordArray[0],$passwordArray[3]);
			$hasSpecChar = strpbrk($passwordArray[0],$passwordArray[4]);
			if(($useUpperCase == "true" && $hasUpper == "") || ($useLowerCase == "true" && $hasLower == "") || ($useNumbers == "true" && $hasNumber == "") || ($useSpecialChars == "true" && $hasSpecChar == ""))
			{
				return "FALSE";
			}
			
			//it contains everything it needs... return the password
			else
			{
				//$password = "Your password is: <b>".$passwordArray[0]."</b>";
				return $passwordArray[0];
			}
		}
		
		public static function UnassignLeadForm()
		{
			$UserArray = array();
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			
			$SoldForm=array();
			$results = DB::table('client_lead_forms')->leftJoin('clients', 'client_lead_forms.client_id', '=', 'clients.id')->select('client_lead_forms.facebook_ads_lead_id')->where('client_lead_forms.is_deleted',0)->where('clients.is_deleted',0)->groupBy('client_lead_forms.facebook_ads_lead_id')->get();
			foreach($results as $result)
			{
				$SoldForm[] = $result->facebook_ads_lead_id;
			}
			
			$results = DB::table('group_lead_forms')->leftJoin('groups', 'group_lead_forms.group_id', '=', 'groups.id')->select('group_lead_forms.facebook_ads_lead_id')->where('group_lead_forms.is_deleted',0)->where('groups.is_deleted',0)->groupBy('group_lead_forms.facebook_ads_lead_id')->get();
			foreach($results as $result)
			{
				$SoldForm[] = $result->facebook_ads_lead_id;
			}
			
			$LeadForms = DB::table('facebook_ads_leads')->whereNotIn('id', $SoldForm)->whereIn('updated_user_id', $UserArray)->where('status', 'ACTIVE')->orderBy('name', 'ASC')->where('is_blocked', 0)->where('is_deleted', 0)->get();
			
			return $LeadForms;
		}
		
		public static function UnassignLeadFormGroup()
		{
			$groups = DB::table('groups')->where('facebook_ads_lead_id', '')->where('created_user_id', Session::get('user_id'))->where('is_deleted', 0)->get();
			
			return $groups;
		}
		
		public static function UnassignLeadFormClient()
		{
			$UserArray = array();
			if(Session::get('user_category')=='user')
			{
				$UserArray[] = Session::get('user_id');
				$manager = DB::table('managers')->select('created_user_id')->where('id',Session::get('manager_id'))->first();
				$UserArray[] = $manager->created_user_id;
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',$manager->created_user_id)->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			else
			{
				$UserArray[] = Session::get('user_id');
				$users = DB::table('users')->select('id')->where('category', 'user')->where('created_user_id',Session::get('user_id'))->get();
				foreach($users as $user)
				{
					$UserArray[] = $user->id;
				}
			}
			
			$ClientId=array();
			$clients=DB::table('clients')->where('facebook_ads_lead_id', '')->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)->where('status', 1)->get();
			foreach($clients as $client)
			{
				$client_id=$client->id;
				$clientGroups=DB::table('client_groups')
				->leftJoin('groups', 'client_groups.group_id', '=', 'groups.id')
				->where('groups.facebook_ads_lead_id','!=', '')
				->where('client_groups.client_id', $client_id)
				->where('client_groups.is_deleted', 0)
				->where('groups.is_deleted', 0)
				->get();
				if(count($clientGroups)==0)
				{
					$ClientId[]=$client_id;
				}
			}
			
			$Results = DB::table('clients')->whereIn('id', $ClientId)->where('status', 1)->whereIn('created_user_id', $UserArray)->where('is_deleted', 0)
			->orderBy('name', 'ASC')->get();
			
			return $Results;
		}
		
		public static function get_zip_info($zip) 
		{ 
			$city = '';
			$state = '';
			$url = "http://maps.google.com/maps/api/geocode/json?address=".$zip."&sensor=true_or_false";
			$result = file_get_contents($url);
			$json = json_decode($result);
			
			foreach ($json->results as $result) {
				foreach($result->address_components as $addressPart) {
					if ((in_array('locality', $addressPart->types)) && (in_array('political', $addressPart->types)))
					$city = $addressPart->long_name;
					else if ((in_array('administrative_area_level_1', $addressPart->types)) && (in_array('political', $addressPart->types)))
					$state = $addressPart->long_name;
					else if ((in_array('country', $addressPart->types)) && (in_array('political', $addressPart->types)))
					$country = $addressPart->long_name;
				}
			}
			
			$zipinfo['zip']   = $zip;
			$zipinfo['city']  = $city;
			$zipinfo['state'] = $state;
			return $zipinfo;
		}
		
		public static function UserMenu($manager_id,$menu)
		{
			$result = DB::table('manager_menus')->where('manager_id', $manager_id)->where('menu', $menu)->where('is_deleted', 0)->first();
			if(is_null($result))
			{
				echo '';
			}
			else
			{
				echo 'checked="checked"';
			}
		}
		
		public static function UserMenuPermission($manager_id,$menu)
		{
			$result = DB::table('manager_menus')->where('manager_id', $manager_id)->where('menu', $menu)->where('is_deleted', 0)->first();
			if(is_null($result))
			{
				echo 'style="padding-left: 25px;display:none"';
			}
			else
			{
				echo 'style="padding-left: 25px;"';
			}
		}
		
		public static function UserPermission($manager_id,$permission)
		{
			$result = DB::table('manager_permissions')->where('manager_id', $manager_id)->where('permission', $permission)->where('is_deleted', 0)->first();
			if(is_null($result))
			{
				echo '';
			}
			else
			{
				echo 'checked="checked"';
			}
		}
		
		public static function FacebooklastSyncLeadDate($facebook_account_id)
		{
			$facebook_ads_lead_user = DB::table('facebook_ads_lead_users')->select('created_at')->where('facebook_account_id', $facebook_account_id)->where('is_deleted', 0)->orderBy('created_at','DESC')->first();
			if(is_null($facebook_ads_lead_user))
			{
				return '';
			}
			else
			{
				return date('m-d-Y h:i:s A',strtotime($facebook_ads_lead_user->created_at));
			}
		}
		
		public static function EmailNotification($noyification_type)
		{
			$email_notification = DB::table('email_notifications')->select('noyification_status')->where('created_user_id', Session::get('user_id'))->where('noyification_type', $noyification_type)->where('is_deleted', 0)->first();
			
			if(is_null($email_notification))
			{
				return "";
			}
			else
			{
				return $email_notification->noyification_status;	
			}
		}
		
		public static function EmailNotificationAllow($noyification_type, $created_user_id)
		{
			$email_notification = DB::table('email_notifications')->select('noyification_status')->where('noyification_type', $noyification_type)->where('created_user_id', $created_user_id)->where('is_deleted', 0)->first();
			
			if(is_null($email_notification))
			{
				return "";
			}
			else
			{
				return $email_notification->noyification_status;	
			}
		}
		
		public static function GetlatLong($address)
		{
			$address = str_replace(" ","+",$address);
			$url = "http://maps.google.com/maps/api/geocode/json?address=$address";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$response = curl_exec($ch);
			curl_close($ch);
			
			$response_a = json_decode($response);
			//print_r($response_a);
			if(count($response_a->results)>0)
			{
				$lat = $response_a->results[0]->geometry->location->lat;
				$long = $response_a->results[0]->geometry->location->lng;
				
				if($lat != '' && $long != '')
				{
					return $lat.', '.$long;
				}
				else
				{
					return '';
				}
			}
			else
			{
				return '';
			}
		}
		
		public static function resync_error()
		{
			$resync_error = DB::table('resync_error')
			->leftJoin('facebook_ads_lead_users', 'resync_error.facebook_ads_lead_user_id', '=', 'facebook_ads_lead_users.id')
			->leftJoin('clients', 'resync_error.client_id', '=', 'clients.id')
			->leftJoin('facebook_ads_leads', 'facebook_ads_lead_users.facebook_ads_lead_id', '=', 'facebook_ads_leads.id')
			->select('resync_error.*', 'facebook_ads_lead_users.full_name as lead_name', 'clients.name as client_name', 'facebook_ads_leads.name as form_name')
			->where('resync_error.created_user_id', Session::get('user_id'))
			->where('resync_error.is_deleted', 0)
			->groupBy('resync_error.facebook_ads_lead_user_id')
			->groupBy('resync_error.reason')
			->get();
			
			return $resync_error;
		}
		
		public static function SubCategoryName($group_id)
		{
			$group = DB::table('groups')->select('group_sub_category_id')->where('id', $group_id)->first();
			$group_sub_category_id = explode(',', $group->group_sub_category_id);
			
			$name = '';
			$group_sub_categories = DB::table('group_sub_categories')->select('name')->whereIn('id', $group_sub_category_id)->orderBy('name', 'ASC')->get();
			foreach($group_sub_categories as $row)
			{
				$name = $name.$row->name.', ';
			}
			
			$name = substr($name, 0 ,-2);
			
			return $name;
		}
	}																																											