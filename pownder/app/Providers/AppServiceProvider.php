<?php
	
	namespace App\Providers;
	
	use Illuminate\Support\ServiceProvider;
	use Validator;
	
	class AppServiceProvider extends ServiceProvider
	{
		/**
			* Bootstrap any application services.
			*
			* @return void
		*/
		public function boot()
		{
			//
			Validator::extend('greater_than_field', function($attribute, $value, $parameters, $validator) {
				$min_field = $parameters[0];
				$data = $validator->getData();
				$min_value = $data[$min_field];
				return $value > $min_value;
			});   
			
			Validator::replacer('greater_than_field', function($message, $attribute, $rule, $parameters) {
				return str_replace(':field', $parameters[0], 'The monthly limit must be greater than the daily limit');
			});
			
			Validator::extend('zip_code_valid', function($attribute, $value, $parameters, $validator) {
				if($value=='')
				{
					return true;
				}
				else
				{
					$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($value)."&sensor=false";
					$result_string = file_get_contents($url);
					$result = json_decode($result_string, true);
					//return $result['results'][0]['geometry']['location'];
					
					return count($result['results'])> 0;
				}
			});   
			
			Validator::replacer('zip_code_valid', function($message, $attribute, $rule, $parameters) {
				return str_replace(':field', '', 'The given zip code is invalid');
			});
			
			// Email array validator
			Validator::extend('email_array', function($attribute, $value, $parameters, $validator) {
				$value = str_replace(' ','',$value);
				$array = explode(',', $value);
				foreach($array as $email) //loop over values
				{
					$email_to_validate['alert_email'][]=$email;
				}
				$rules = array('alert_email.*'=>'email');
				$messages = array('alert_email.*'=>trans('validation.email_array'));
				$validator = Validator::make($email_to_validate,$rules,$messages);
				if ($validator->passes()) 
				{
					return true;
				} 
				else 
				{
					return false;
				}
			});
			
			Validator::replacer('email_array', function($message, $attribute, $rule, $parameters) {
				return str_replace(':field', '', 'The email must be a valid email address');
			});
			
			// Email array validator
			Validator::extend('phone_format', function($attribute, $value, $parameters, $validator) {
				$matches = null;
				preg_match_all('!\d+!', $value, $matches);
				
				$string = '';
				$number = $matches[0];
				for($i = 0; $i < count($number); $i++)
				{
					$string = $string.$number[$i];
				}
				
				if(strlen($string) == 10 || strlen($string) == 0)
				{			
					return true;
				}
				else
				{
					return false;
				}	
			});
			
			Validator::replacer('phone_format', function($message, $attribute, $rule, $parameters) {
				return str_replace(':field', '', 'The phone number format is invalid');
			});
		}
		
		/**
			* Register any application services.
			*
			* @return void
		*/
		public function register()
		{
			//
		}
	}
