<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
	use Helper;
	
	class GetFacebookCampaign extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:GetFacebookCampaign';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Get all facebook ad account campaigns';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$FacebookAccounts = DB::table('facebook_accounts')->select('id', 'access_token', 'updated_user_id')->where('is_deleted', 0)->orderBy('updated_at', 'ASC')->get();
			foreach($FacebookAccounts as $FacebookAccount)
			{
				$facebook_account_id=$FacebookAccount->id;
				$accessToken=$FacebookAccount->access_token;
				$created_user_id=$FacebookAccount->updated_user_id;
				$updated_user_id=$FacebookAccount->updated_user_id;
				
				$FacebookAdsAccounts = DB::table('facebook_ads_accounts')->select('id', 'account_id')->where('facebook_account_id', $facebook_account_id)->where('is_deleted', 0)->orderBy('updated_at', 'ASC')->get();
				foreach($FacebookAdsAccounts as $FacebookAdsAccount)
				{					
					$facebook_ads_account_id=$FacebookAdsAccount->id;
					$account_id=$FacebookAdsAccount->account_id;
					//Read facebook pages
					$url1 = "http://big.pownder.com/fb_campaign/get_campaign.php?accessToken=".$accessToken."&account_id=".$account_id;
					
					$ch1 = curl_init();
					
					curl_setopt($ch1, CURLOPT_URL,$url1);
					curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, 0);
					
					$response1 = curl_exec($ch1);
					$info1 = curl_getinfo($ch1);
					
					curl_close($ch1);
					
					$result1 = json_decode($response1, true);
					
					//echo "<pre>";
					//print_r($result1);
					
					for($i=0;$i<count($result1);$i++)
					{
						$facebook_campaign_id = $result1[$i]['id'];
						
						if($facebook_campaign_id>0)
						{
							$check_page_exist=DB::table('facebook_campaigns')->where('id', $facebook_campaign_id)->first();
							if(is_null($check_page_exist))
							{
								DB::table('facebook_campaigns')->insert([
								'id' => $facebook_campaign_id,
								'facebook_account_id' => $facebook_account_id,
								'facebook_ads_account_id' => $facebook_ads_account_id,
								'name' => $result1[$i]['name'],
								'objective' => $result1[$i]['objective'],
								'created_user_id' => $created_user_id,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_user_id' => $updated_user_id,
								'updated_at' => date('Y-m-d H:i:s')
								]);
							}
							else
							{
								DB::table('facebook_campaigns')
								->where('id', $facebook_campaign_id)
								->update([
								'facebook_account_id' => $facebook_account_id,
								'facebook_ads_account_id' => $facebook_ads_account_id,
								'name' => $result1[$i]['name'],
								'objective' => $result1[$i]['objective'],
								'updated_user_id' => $updated_user_id,
								'updated_at' => date('Y-m-d H:i:s')
								]);
							}
						}
					}
				}	
			}
		}
	}
