<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	
	use DB;
	use Helper;
	use Mail;
	
	class PageEngagementClientLead extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:PageEngagementClientLead';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Page Engagement Client Lead';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$page_engagements = DB::table('page_engagements')
			->leftJoin('clients', 'page_engagements.client_id', '=', 'clients.id')
			->select('page_engagements.facebook_page_id', 'page_engagements.client_id')
			->where('page_engagements.is_deleted', 0)
			->where('clients.is_deleted', 0)
			->groupBy('page_engagements.facebook_page_id')
			->get();
			foreach($page_engagements as $page_engagement)
			{
				$facebook_page_id = $page_engagement->facebook_page_id;
				$client_id = $page_engagement->client_id;
				
				$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')->where('facebook_page_id', $facebook_page_id)->where('lead_type', 'Pownder™ Messenger')->where('client_id', 0)->where('is_deleted', 0)->get();
				foreach($facebook_ads_lead_users as $lead)
				{					
					$facebook_ads_lead_user_id = $lead->id;
					$facebook_account_id = $lead->facebook_account_id;
					$facebook_page_id = $lead->facebook_page_id;
					$facebook_ads_lead_id = $lead->facebook_ads_lead_id;
					$user_id = $lead->created_user_id;
					
					$leadName = $lead->full_name;
					$leadCity = $lead->city;
					$leadEmail = $lead->email;
					$leadPhone = Helper::emailPhoneFormat($lead->phone_number);
					
					DB::transaction(function () use ($user_id, $client_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
					{
						$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
						'user_id' => $user_id,
						'client_id' => $client_id,
						'facebook_account_id' => $facebook_account_id,
						'facebook_page_id' => $facebook_page_id,
						'facebook_ads_lead_id' => $facebook_ads_lead_id,
						'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
						'created_at' => date('Y-m-d H:i:s')
						]);
						
						DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
						->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
					});
					
					$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
					foreach($CampaignClients as $CampaignClient)
					{
						if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
						{
							$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
							
							Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
								$message->from('noreply@pownder.com', 'Pownder');
								$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
							});
						}
						
						if($CampaignClient->user_notification == 'No')
						{
							if($CampaignClient->manager_id != '')
							{
								$ManagerArray = explode(",", $CampaignClient->manager_id);
								$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
								foreach($Managers as $Manager)
								{
									$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
									
									Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
										$message->from('noreply@pownder.com', 'Pownder');
										$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
									}); 
								}
							}
						}
						else
						{
							$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
							if(!is_null($ManagerExists))
							{
								if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
								{
									if($CampaignClient->manager_id != '')
									{
										$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
										
										$ManagerArray = explode(",", $CampaignClient->manager_id);
										
										$Array = array();
										$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
										foreach($queries as $query)
										{
											$Array[] = $query->id;
										}
										
										if(count($Array) > 0)
										{
											$index = $LeadCount % count($Array);
											
											$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
											if(!is_null($Manager))
											{
												$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
												
												Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
													$message->from('noreply@pownder.com', 'Pownder');
													$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
												}); 
												
												DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
												->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
											}
										}
									}
								}
							}
						}	
					}
				}
			}
		}	
	}																																	