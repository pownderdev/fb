<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	
	use DB;
	use Helper;
	
	class FacebookFormAutometicAssign extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:FacebookFormAutometicAssign';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Facebook Form Autometic Assign';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$LeadFormArray = array();
			$LeadFormQuery = DB::table('client_lead_forms')->leftJoin('clients', 'client_lead_forms.client_id', '=', 'clients.id')->select('client_lead_forms.facebook_ads_lead_id')->where('client_lead_forms.is_deleted',0)->where('clients.is_deleted',0)->groupBy('client_lead_forms.facebook_ads_lead_id')->get();
			foreach($LeadFormQuery as $row)
			{
				$LeadFormArray[] = $row->facebook_ads_lead_id;
			}
			
			$FacebookAdsLeads = DB::table('facebook_ads_leads')->select('id', 'name')->whereNotIn('id', $LeadFormArray)->where('status', 'ACTIVE')->where('is_blocked', 0)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
			
			foreach($FacebookAdsLeads as $FacebookAdsLead)
			{
				$facebook_ads_lead_id = $FacebookAdsLead->id;
				$name = $FacebookAdsLead->name;
				
				$client = DB::table('clients')->select('id', 'facebook_ads_lead_id')->where('name', $name)->where('status', '1')->where('is_deleted', 0)->first();
				if(!is_null($client))
				{
					$client_id = $client->id;
					$client_form = $client->facebook_ads_lead_id; 
					
					$client_form = $client_form.','.$facebook_ads_lead_id; 
					
					DB::table('clients')->where('id', $client_id)
					->update(['facebook_ads_lead_id' => $client_form]);
					
					$client_lead_form = DB::table('client_lead_forms')->where('client_id', $client_id)->where('facebook_ads_lead_id', $facebook_ads_lead_id)->first();
					if(is_null($client_lead_form))
					{
						DB::table('client_lead_forms')->insert([
						"client_id" => $client_id,
						"facebook_ads_lead_id" => $facebook_ads_lead_id,
						"created_user_id" => 0,
						"created_at" => date('Y-m-d H:i:s'),
						"updated_user_id" => 0,
						"updated_at" => date('Y-m-d H:i:s')
						]);
					}
					else
					{
						DB::table('client_lead_forms')->where('client_id', $client_id)->where('facebook_ads_lead_id', $facebook_ads_lead_id)
						->update(["updated_user_id" => 0, "updated_at" => date('Y-m-d H:i:s'), 'is_deleted' => 0]);
					}
				}
			}
		}	
	}																													