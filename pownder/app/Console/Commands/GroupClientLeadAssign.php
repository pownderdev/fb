<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	
	use DB;
	use Helper;
	use Mail;
	
	class GroupClientLeadAssign extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:GroupClientLeadAssign';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Group Client Lead Assign';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			//------(Remaining Day Calculate)------
			$total_date = date("t");
			$today_date = date("j");
			$remaining_limit_day = $total_date - $today_date + 1;
			
			$prev_date = date('Y-m-d', strtotime(' -1 day'));
			//-------------------------------------
			
			$groups = DB::table('groups')->where('facebook_ads_lead_id', '!=', '')->where('client_id', '!=', '')->where('is_deleted', 0)->get();
			
			foreach($groups as $group)
			{
				$ClientArray = explode(",", $group->client_id);
				$LeadForm = explode(",", $group->facebook_ads_lead_id);
				
				$loop  = 0;
				$counter = 0;
				
				while ($loop <= 0)
				{
					//$assign_lead_quantity = DB::table('client_ads_lead_users')->select('id')->whereIn('facebook_ads_lead_id', $LeadForm)->whereDate('created_at', '>=', $prev_date)->whereDate('created_at', '<=',  date('Y-m-d'))->where('is_deleted', 0)->count('id');
					$assign_lead_quantity = DB::table('client_ads_lead_users')->select('id')->whereIn('facebook_ads_lead_id', $LeadForm)->whereIn('client_id', $ClientArray)->where('is_deleted', 0)->count('id');
					
					$index = ($assign_lead_quantity % count($ClientArray));
					
					$clients = DB::table('clients')
					->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
					->select('clients.id', 'clients.facebook_ads_lead_id as client_lead_form', 'clients.package_id', 'clients.zip', 'clients.latitude', 'clients.longitude', 'packages.monthly_lead_quantity', 'packages.package_type')
					->where('clients.status', 1)
					->where('clients.is_deleted', 0)
					->where('clients.id', $ClientArray[$index])
					->orderBy('clients.id', 'ASC')
					->get();
					if(count($clients) > 0)
					{
						foreach($clients as $client)
						{
							$client_id = $client->id;
							$client_latitude = (float)$client->latitude;
							$client_longitude = (float)$client->longitude;
							$client_lead_form = $client->client_lead_form;
							$package_id = $client->package_id;
							$package_type = $client->package_type;
							$monthly_lead_quantity = $client->monthly_lead_quantity;
							
							if($package_type == 'Monthly')
							{
								//------(Remaining Total Lead Calculate)------
								$assign_lead_quantity = DB::table('client_ads_lead_users')->select('id')->whereIn('facebook_ads_lead_id', $LeadForm)->where('client_id', $client_id)->whereYear('created_at', '=', date('Y'))->whereMonth('created_at', '=', date('m'))->whereDate('created_at', '!=', date('Y-m-d'))->where('is_deleted', 0)->count('id');
								$remaining_lead_quantity = $monthly_lead_quantity - $assign_lead_quantity;
								//--------------------------------------------
								
								if($remaining_lead_quantity > 0)
								{
									//------(Remaining Today Lead Calculate)------
									if($remaining_limit_day == 1)
									{
										$today_limit = $remaining_lead_quantity;
									}
									else
									{
										$today_limit = (integer)($remaining_lead_quantity / $remaining_limit_day);
									}
									
									$today_assign_lead_quantity = DB::table('client_ads_lead_users')->select('id')->whereIn('facebook_ads_lead_id', $LeadForm)->where('client_id', $client_id)->whereDate('created_at','=', date('Y-m-d'))->where('is_deleted', 0)->count('id');
									
									$remaining_today_limit = $today_limit - $today_assign_lead_quantity;
									//--------------------------------------------
									
									if($remaining_today_limit > 0)
									{
										$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->whereIn('facebook_ads_lead_id', $LeadForm)->where('city', '!=', '')->where('client_id', 0)->where('is_deleted', 0)->get();
										if(count($FacebookAdsLeadUsers)>0)
										{
											//echo "Pass 9";die;
											$facebook_ads_lead_user_id = 0;
											$facebook_account_id = 0;
											$facebook_page_id = 0;
											$facebook_ads_lead_id = 0;
											$user_id = 0;
											$leadName = '';
											$leadCity = '';
											$leadEmail = '';
											$leadPhone = '';
											$miles = '';
											foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
											{
												$city = $FacebookAdsLeadUser->city;
												
												$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($city)."&sensor=false";
												$result_string = file_get_contents($url);
												$result = json_decode($result_string, true);
												if(count($result['results']) > 0)
												{
													$val = $result['results'][0]['geometry']['location'];
													$user_latitude = $val['lat'];
													$user_longitude = $val['lng'];
													$distance_lat_lon = Helper::DistanceLatLon($client_latitude, $client_longitude, $user_latitude, $user_longitude);
													if($miles == '')
													{
														$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
														$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
														$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
														$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
														$user_id = $FacebookAdsLeadUser->created_user_id;
														$leadName = $FacebookAdsLeadUser->full_name;
														$leadCity = $FacebookAdsLeadUser->city;
														$leadEmail = $FacebookAdsLeadUser->email;
														$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
														$miles = $distance_lat_lon;
													}
													else if($miles > $distance_lat_lon)
													{
														$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
														$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
														$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
														$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
														$user_id = $FacebookAdsLeadUser->created_user_id;
														$leadName = $FacebookAdsLeadUser->full_name;
														$leadCity = $FacebookAdsLeadUser->city;
														$leadEmail = $FacebookAdsLeadUser->email;
														$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
														$miles = $distance_lat_lon;
													}
												}
												else
												{
													$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
													$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
													$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
													$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
													$user_id = $FacebookAdsLeadUser->created_user_id;
													$leadName = $FacebookAdsLeadUser->full_name;
													$leadCity = $FacebookAdsLeadUser->city;
													$leadEmail = $FacebookAdsLeadUser->email;
													$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
													break;
												}
											}
											
											DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
											{
												$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
												'user_id' => $user_id,
												'client_id' => $client_id, 
												'package_id' => $package_id,
												'facebook_account_id' => $facebook_account_id,
												'facebook_page_id' => $facebook_page_id,
												'facebook_ads_lead_id' => $facebook_ads_lead_id,
												'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
												'created_at' => date('Y-m-d H:i:s')
												]);
												
												DB::table('facebook_ads_lead_users')
												->where('id', $facebook_ads_lead_user_id)
												->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
											});
											
											$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
											foreach($CampaignClients as $CampaignClient)
											{
												if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
												{
													$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
													
													Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
														$message->from('noreply@pownder.com', 'Pownder');
														$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
													});
												}
												
												if($CampaignClient->user_notification == 'No')
												{
													if($CampaignClient->manager_id != '')
													{
														$ManagerArray = explode(",", $CampaignClient->manager_id);
														$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
														foreach($Managers as $Manager)
														{
															$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
															
															Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																$message->from('noreply@pownder.com', 'Pownder');
																$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
															}); 
														}
													}
												}
												else
												{
													$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
													if(!is_null($ManagerExists))
													{
														if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
														{
															if($CampaignClient->manager_id != '')
															{
																$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
																
																$ManagerArray = explode(",", $CampaignClient->manager_id);
																
																$Array = array();
																$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																foreach($queries as $query)
																{
																	$Array[] = $query->id;
																}
																
																if(count($Array) > 0)
																{
																	$index = $LeadCount % count($Array);
																	
																	$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																	if(!is_null($Manager))
																	{
																		$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																		
																		Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																			$message->from('noreply@pownder.com', 'Pownder');
																			$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																		}); 
																		
																		DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																		->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																	}
																}
															}
														}
													}
												}	
											}
										}
										else
										{
											$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->whereIn('facebook_ads_lead_id', $LeadForm)->where('city', '')->where('client_id', 0)->where('is_deleted', 0)->take(1)->get();
											if(count($FacebookAdsLeadUsers) > 0)
											{
												foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
												{
													$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
													$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
													$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
													$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
													$user_id = $FacebookAdsLeadUser->created_user_id;
													$leadName = $FacebookAdsLeadUser->full_name;
													$leadCity = $FacebookAdsLeadUser->city;
													$leadEmail = $FacebookAdsLeadUser->email;
													$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
													
													DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
													{
														$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
														'user_id' => $user_id,
														'client_id' => $client_id,
														'package_id' => $package_id,
														'facebook_account_id' => $facebook_account_id,
														'facebook_page_id' => $facebook_page_id,
														'facebook_ads_lead_id' => $facebook_ads_lead_id,
														'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
														'created_at' => date('Y-m-d H:i:s')
														]);
														
														DB::table('facebook_ads_lead_users')
														->where('id', $facebook_ads_lead_user_id)
														->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
													});
													
													$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
													foreach($CampaignClients as $CampaignClient)
													{
														if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
														{
															$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
															
															Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
																$message->from('noreply@pownder.com', 'Pownder');
																$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
															});
														}
														
														if($CampaignClient->user_notification == 'No')
														{
															if($CampaignClient->manager_id != '')
															{
																$ManagerArray = explode(",", $CampaignClient->manager_id);
																$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																foreach($Managers as $Manager)
																{
																	$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																	
																	Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																		$message->from('noreply@pownder.com', 'Pownder');
																		$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																	}); 
																}
															}
														}
														else
														{
															$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
															if(!is_null($ManagerExists))
															{
																if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
																{
																	if($CampaignClient->manager_id != '')
																	{
																		$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
																		
																		$ManagerArray = explode(",", $CampaignClient->manager_id);
																		
																		$Array = array();
																		$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																		foreach($queries as $query)
																		{
																			$Array[] = $query->id;
																		}
																		
																		if(count($Array) > 0)
																		{
																			$index = $LeadCount % count($Array);
																			
																			$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																			if(!is_null($Manager))
																			{
																				$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																				
																				Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																					$message->from('noreply@pownder.com', 'Pownder');
																					$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																				}); 
																				
																				DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																				->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																			}
																		}
																	}
																}
															}
														}	
													}
												}
											}
											else
											{
												$counter++;
												unset($ClientArray[$index]);
												$ClientArray = array_values($ClientArray);
												if(count($ClientArray) == 0)
												{
													$loop++;
												}
											}
										}
									}
									else
									{
										$counter++;
										unset($ClientArray[$index]);
										$ClientArray = array_values($ClientArray);
										if(count($ClientArray) == 0)
										{
											$loop++;
										}
									}
								}
								else
								{
									$counter++;
									unset($ClientArray[$index]);
									$ClientArray = array_values($ClientArray);
									if(count($ClientArray) == 0)
									{
										$loop++;
									}
								}
							}
							elseif($package_type == 'Lead' || $package_type == 'Sold')
							{
								if($monthly_lead_quantity == '')
								{
									$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->whereIn('facebook_ads_lead_id', $LeadForm)->where('city', '!=', '')->where('client_id', 0)->where('is_deleted', 0)->get();
									if(count($FacebookAdsLeadUsers)>0)
									{
										//echo "Pass 9";die;
										$facebook_ads_lead_user_id = 0;
										$facebook_account_id = 0;
										$facebook_page_id = 0;
										$facebook_ads_lead_id = 0;
										$user_id = 0;
										$leadName = '';
										$leadCity = '';
										$leadEmail = '';
										$leadPhone = '';
										$miles='';
										foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
										{
											$city=$FacebookAdsLeadUser->city;
											
											$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($city)."&sensor=false";
											$result_string = file_get_contents($url);
											$result = json_decode($result_string, true);
											if(count($result['results']) > 0)
											{
												$val = $result['results'][0]['geometry']['location'];
												$user_latitude = $val['lat'];
												$user_longitude = $val['lng'];
												$distance_lat_lon = Helper::DistanceLatLon($client_latitude, $client_longitude, $user_latitude, $user_longitude);
												if($miles == '')
												{
													$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
													$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
													$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
													$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
													$user_id = $FacebookAdsLeadUser->created_user_id;
													$leadName = $FacebookAdsLeadUser->full_name;
													$leadCity = $FacebookAdsLeadUser->city;
													$leadEmail = $FacebookAdsLeadUser->email;
													$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
													$miles = $distance_lat_lon;
												}
												else if($miles > $distance_lat_lon)
												{
													$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
													$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
													$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
													$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
													$user_id = $FacebookAdsLeadUser->created_user_id;
													$leadName = $FacebookAdsLeadUser->full_name;
													$leadCity = $FacebookAdsLeadUser->city;
													$leadEmail = $FacebookAdsLeadUser->email;
													$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
													$miles = $distance_lat_lon;
												}
											}
											else
											{
												$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
												$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
												$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
												$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
												$user_id = $FacebookAdsLeadUser->created_user_id;
												$leadName = $FacebookAdsLeadUser->full_name;
												$leadCity = $FacebookAdsLeadUser->city;
												$leadEmail = $FacebookAdsLeadUser->email;
												$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
												break;
											}
										}
										
										DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
										{
											$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
											'user_id' => $user_id,
											'client_id' => $client_id, 
											'package_id' => $package_id,
											'facebook_account_id' => $facebook_account_id,
											'facebook_page_id' => $facebook_page_id,
											'facebook_ads_lead_id' => $facebook_ads_lead_id,
											'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
											'created_at' => date('Y-m-d H:i:s')
											]);
											
											DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
											->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
										});
										
										$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
										foreach($CampaignClients as $CampaignClient)
										{
											if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
											{
												$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
												
												Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
													$message->from('noreply@pownder.com', 'Pownder');
													$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
												});
											}
											
											if($CampaignClient->user_notification == 'No')
											{
												if($CampaignClient->manager_id != '')
												{
													$ManagerArray = explode(",", $CampaignClient->manager_id);
													$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
													foreach($Managers as $Manager)
													{
														$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
														
														Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
															$message->from('noreply@pownder.com', 'Pownder');
															$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
														}); 
													}
												}
											}
											else
											{
												$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
												if(!is_null($ManagerExists))
												{
													if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
													{
														if($CampaignClient->manager_id != '')
														{
															$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
															
															$ManagerArray = explode(",", $CampaignClient->manager_id);
															
															$Array = array();
															$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
															foreach($queries as $query)
															{
																$Array[] = $query->id;
															}
															
															if(count($Array) > 0)
															{
																$index = $LeadCount % count($Array);
																
																$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																if(!is_null($Manager))
																{
																	$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																	
																	Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																		$message->from('noreply@pownder.com', 'Pownder');
																		$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																	}); 
																	
																	DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																	->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																}
															}
														}
													}
												}
											}	
										}
									}
									else
									{
										$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->whereIn('facebook_ads_lead_id', $LeadForm)->where('city', '')->where('client_id', 0)->where('is_deleted', 0)->take(1)->get();
										if(count($FacebookAdsLeadUsers) > 0)
										{
											foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
											{
												$facebook_ads_lead_user_id=$FacebookAdsLeadUser->id;
												$facebook_account_id=$FacebookAdsLeadUser->facebook_account_id;
												$facebook_page_id=$FacebookAdsLeadUser->facebook_page_id;
												$facebook_ads_lead_id=$FacebookAdsLeadUser->facebook_ads_lead_id;
												$user_id = $FacebookAdsLeadUser->created_user_id;
												$leadName = $FacebookAdsLeadUser->full_name;
												$leadCity = $FacebookAdsLeadUser->city;
												$leadEmail = $FacebookAdsLeadUser->email;
												$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
												
												DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
												{
													$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
													'user_id' => $user_id,
													'client_id' => $client_id,
													'package_id' => $package_id,
													'facebook_account_id' => $facebook_account_id,
													'facebook_page_id' => $facebook_page_id,
													'facebook_ads_lead_id' => $facebook_ads_lead_id,
													'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
													'created_at' => date('Y-m-d H:i:s')
													]);
													
													DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
													->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
												});
												
												$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
												foreach($CampaignClients as $CampaignClient)
												{
													if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
													{
														$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
														
														Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
															$message->from('noreply@pownder.com', 'Pownder');
															$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
														});
													}
													
													if($CampaignClient->user_notification == 'No')
													{
														if($CampaignClient->manager_id != '')
														{
															$ManagerArray = explode(",", $CampaignClient->manager_id);
															$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
															foreach($Managers as $Manager)
															{
																$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																
																Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																	$message->from('noreply@pownder.com', 'Pownder');
																	$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																}); 
															}
														}
													}
													else
													{
														$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
														if(!is_null($ManagerExists))
														{
															if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
															{
																if($CampaignClient->manager_id != '')
																{
																	$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
																	
																	$ManagerArray = explode(",", $CampaignClient->manager_id);
																	
																	$Array = array();
																	$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																	foreach($queries as $query)
																	{
																		$Array[] = $query->id;
																	}
																	
																	if(count($Array) > 0)
																	{
																		$index = $LeadCount % count($Array);
																		
																		$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																		if(!is_null($Manager))
																		{
																			$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																			
																			Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																				$message->from('noreply@pownder.com', 'Pownder');
																				$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																			}); 
																			
																			DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																			->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																		}
																	}
																}
															}
														}
													}	
												}
											}
										}
										else
										{
											$counter++;
											unset($ClientArray[$index]);
											$ClientArray = array_values($ClientArray);
											if(count($ClientArray) == 0)
											{
												$loop++;
											}
										}
									}
								}
								else
								{
									//------(Remaining Total Lead Calculate)------
									$assign_lead_quantity = DB::table('client_ads_lead_users')->select('id')->whereIn('facebook_ads_lead_id', $LeadForm)->where('client_id', $client_id)->whereYear('created_at', '=', date('Y'))->whereMonth('created_at', '=', date('m'))->whereDate('created_at', '!=', date('Y-m-d'))->where('is_deleted', 0)->count('id');
									$remaining_lead_quantity = $monthly_lead_quantity - $assign_lead_quantity;
									//--------------------------------------------
									
									if($remaining_lead_quantity > 0)
									{
										//echo "Pass 4";die;
										//------(Remaining Today Lead Calculate)------
										if($remaining_limit_day == 1)
										{
											$today_limit = $remaining_lead_quantity;
										}
										else
										{
											$today_limit = (integer)($remaining_lead_quantity / $remaining_limit_day);
										}
										
										$today_assign_lead_quantity = DB::table('client_ads_lead_users')->select('id')->whereIn('facebook_ads_lead_id', $LeadForm)->where('client_id', $client_id)->whereDate('created_at','=', date('Y-m-d'))->where('is_deleted', 0)->count('id');
										
										$remaining_today_limit = $today_limit - $today_assign_lead_quantity;
										//--------------------------------------------
										
										if($remaining_today_limit > 0)
										{
											$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->whereIn('facebook_ads_lead_id', $LeadForm)->where('city', '!=', '')->where('client_id', 0)->where('is_deleted', 0)->get();
											if(count($FacebookAdsLeadUsers) > 0)
											{
												//echo "Pass 9";die;
												$facebook_ads_lead_user_id = 0;
												$facebook_account_id = 0;
												$facebook_page_id = 0;
												$facebook_ads_lead_id = 0;
												$user_id = 0;
												$leadName = '';
												$leadCity = '';
												$leadEmail = '';
												$leadPhone = '';
												$miles = '';
												foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
												{
													$city = $FacebookAdsLeadUser->city;
													
													$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($city)."&sensor=false";
													$result_string = file_get_contents($url);
													$result = json_decode($result_string, true);
													if(count($result['results']) > 0)
													{
														$val = $result['results'][0]['geometry']['location'];
														$user_latitude = $val['lat'];
														$user_longitude = $val['lng'];
														$distance_lat_lon = Helper::DistanceLatLon($client_latitude, $client_longitude, $user_latitude, $user_longitude);
														if($miles == '')
														{
															$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
															$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
															$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
															$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
															$user_id = $FacebookAdsLeadUser->created_user_id;
															$leadName = $FacebookAdsLeadUser->full_name;
															$leadCity = $FacebookAdsLeadUser->city;
															$leadEmail = $FacebookAdsLeadUser->email;
															$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
															$miles = $distance_lat_lon;
														}
														else if($miles > $distance_lat_lon)
														{
															$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
															$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
															$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
															$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
															$user_id = $FacebookAdsLeadUser->created_user_id;
															$leadName = $FacebookAdsLeadUser->full_name;
															$leadCity = $FacebookAdsLeadUser->city;
															$leadEmail = $FacebookAdsLeadUser->email;
															$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
															$miles = $distance_lat_lon;
														}
													}
													else
													{
														$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
														$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
														$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
														$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
														$user_id = $FacebookAdsLeadUser->created_user_id;
														$leadName = $FacebookAdsLeadUser->full_name;
														$leadCity = $FacebookAdsLeadUser->city;
														$leadEmail = $FacebookAdsLeadUser->email;
														$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
														break;
													}
												}
												
												DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
												{
													$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
													'user_id' => $user_id,
													'client_id' => $client_id, 
													'package_id' => $package_id,
													'facebook_account_id' => $facebook_account_id,
													'facebook_page_id' => $facebook_page_id,
													'facebook_ads_lead_id' => $facebook_ads_lead_id,
													'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
													'created_at' => date('Y-m-d H:i:s')
													]);
													
													DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
													->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
												});
												
												$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
												foreach($CampaignClients as $CampaignClient)
												{
													if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
													{
														$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
														
														Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
															$message->from('noreply@pownder.com', 'Pownder');
															$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
														});
													}
													
													if($CampaignClient->user_notification == 'No')
													{
														if($CampaignClient->manager_id != '')
														{
															$ManagerArray = explode(",", $CampaignClient->manager_id);
															$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
															foreach($Managers as $Manager)
															{
																$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																
																Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																	$message->from('noreply@pownder.com', 'Pownder');
																	$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																}); 
															}
														}
													}
													else
													{
														$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
														if(!is_null($ManagerExists))
														{
															if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
															{
																if($CampaignClient->manager_id != '')
																{
																	$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
																	
																	$ManagerArray = explode(",", $CampaignClient->manager_id);
																	
																	$Array = array();
																	$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																	foreach($queries as $query)
																	{
																		$Array[] = $query->id;
																	}
																	
																	if(count($Array) > 0)
																	{
																		$index = $LeadCount % count($Array);
																		
																		$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																		if(!is_null($Manager))
																		{
																			$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																			
																			Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																				$message->from('noreply@pownder.com', 'Pownder');
																				$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																			}); 
																			
																			DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																			->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																		}
																	}
																}
															}
														}
													}	
												}
											}
											else
											{
												$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->whereIn('facebook_ads_lead_id', $LeadForm)->where('city', '')->where('client_id', 0)->where('is_deleted', 0)->take(1)->get();
												if(count($FacebookAdsLeadUsers) > 0 )
												{
													foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
													{
														$facebook_ads_lead_user_id=$FacebookAdsLeadUser->id;
														$facebook_account_id=$FacebookAdsLeadUser->facebook_account_id;
														$facebook_page_id=$FacebookAdsLeadUser->facebook_page_id;
														$facebook_ads_lead_id=$FacebookAdsLeadUser->facebook_ads_lead_id;
														$user_id = $FacebookAdsLeadUser->created_user_id;
														$leadName = $FacebookAdsLeadUser->full_name;
														$leadCity = $FacebookAdsLeadUser->city;
														$leadEmail = $FacebookAdsLeadUser->email;
														$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
														
														DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
														{
															$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
															'user_id' => $user_id,
															'client_id' => $client_id,
															'package_id' => $package_id,
															'facebook_account_id' => $facebook_account_id,
															'facebook_page_id' => $facebook_page_id,
															'facebook_ads_lead_id' => $facebook_ads_lead_id,
															'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
															'created_at' => date('Y-m-d H:i:s')
															]);
															
															DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
															->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
														});
														
														$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
														foreach($CampaignClients as $CampaignClient)
														{
															if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
															{
																$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																
																Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
																	$message->from('noreply@pownder.com', 'Pownder');
																	$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
																});
															}
															
															if($CampaignClient->user_notification == 'No')
															{
																if($CampaignClient->manager_id != '')
																{
																	$ManagerArray = explode(",", $CampaignClient->manager_id);
																	$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																	foreach($Managers as $Manager)
																	{
																		$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																		
																		Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																			$message->from('noreply@pownder.com', 'Pownder');
																			$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																		}); 
																	}
																}
															}
															else
															{
																$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
																if(!is_null($ManagerExists))
																{
																	if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
																	{
																		if($CampaignClient->manager_id != '')
																		{
																			$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
																			
																			$ManagerArray = explode(",", $CampaignClient->manager_id);
																			
																			$Array = array();
																			$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																			foreach($queries as $query)
																			{
																				$Array[] = $query->id;
																			}
																			
																			if(count($Array) > 0)
																			{
																				$index = $LeadCount % count($Array);
																				
																				$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																				if(!is_null($Manager))
																				{
																					$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																					
																					Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																						$message->from('noreply@pownder.com', 'Pownder');
																						$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																					}); 
																					
																					DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																					->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																				}
																			}
																		}
																	}
																}
															}	
														}
													}
												}
												else
												{
													$counter++;
													unset($ClientArray[$index]);
													$ClientArray = array_values($ClientArray);
													if(count($ClientArray) == 0)
													{
														$loop++;
													}
												}
											}
										}
										else
										{
											$counter++;
											unset($ClientArray[$index]);
											$ClientArray = array_values($ClientArray);
											if(count($ClientArray) == 0)
											{
												$loop++;
											}
										}
									}
									else
									{
										$counter++;
										unset($ClientArray[$index]);
										$ClientArray = array_values($ClientArray);
										if(count($ClientArray) == 0)
										{
											$loop++;
										}
									}
								}
							}
							else
							{
								$counter++;
								unset($ClientArray[$index]);
								$ClientArray = array_values($ClientArray);
								if(count($ClientArray) == 0)
								{
									$loop++;
								}
							}
						}
					}
					else
					{
						$counter++;
						unset($ClientArray[$index]);
						$ClientArray = array_values($ClientArray);
						if(count($ClientArray) == 0)
						{
							$loop++;
						}
					}
				}
			}
		}	
	}																																				