<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
	
	class FacebookAdsetCtr extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:FacebookAdsetCtr';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Get Facebook Adset ctr';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$last_adset_id = 0;
			$last_CheckExists = DB::table('facebook_adset_values')->where('category', 'ctr')->orderBy('id', 'DESC')->first();
			if(!is_null($last_CheckExists))
			{
				$last_adset_id = $last_CheckExists->adset_id;
			}
			
			$FacebookAccounts = DB::table('facebook_ads_lead_users')
			->join('facebook_accounts', 'facebook_ads_lead_users.facebook_account_id', '=', 'facebook_accounts.id')
			->select('facebook_ads_lead_users.adset_id', 'facebook_ads_lead_users.adset_name', 'facebook_accounts.access_token')
			->where('facebook_ads_lead_users.adset_id', '>=', $last_adset_id)
			->groupBy('facebook_ads_lead_users.adset_id')
			->orderBy('facebook_ads_lead_users.adset_id', 'ASC')
			->get();
			foreach($FacebookAccounts as $FacebookAccount)
			{
				$adset_id = $FacebookAccount->adset_id;
				$access_token = $FacebookAccount->access_token;
				$adset_date = '';
				$CheckExists = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('category', 'ctr')->orderBy('id', 'DESC')->first();
				if(!is_null($CheckExists))
				{
					$adset_date = date('Y-m-d', strtotime($CheckExists->adset_date. ' - 1 days'));
				}
				
				$curl = curl_init('https://graph.facebook.com/v2.10/'.$adset_id.'/insights?&date_preset=lifetime&fields=ctr&access_token='.$access_token);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				$response = curl_exec($curl);
				$result = json_decode($response,true);
				
				//echo "<pre>";
				//print_r($result);
				
				if(isset($result['data'][0]['ctr']))
				{
					$date_start = $result['data'][0]['date_start']; 
					$date_stop = $result['data'][0]['date_stop'];
					
					if($adset_date == '')
					{
						$s_date = $date_start;
					}
					else
					{
						$s_date = $adset_date;
					}
					$e_date = $date_start;
					
					$i = 0;
					while($i == 0)
					{
						if(strtotime($s_date) <= strtotime($date_stop))
						{							
							$curl = curl_init('https://graph.facebook.com/v2.10/'.$adset_id.'/insights?&time_range[since]='.$s_date.'&time_range[until]='.$s_date.'&fields=ctr&access_token='.$access_token);
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
							$response1 = curl_exec($curl);
							$result1 = json_decode($response1,true);
							
							//echo "<pre>";
							//print_r($result1);
							
							if(isset($result1['data']))
							{
								if(isset($result1['data'][0]['ctr']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1['data'][0]['ctr'], 'category' => 'ctr']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')
										->update(['value' => $result1['data'][0]['ctr']]);
									}
									
									$s_date = date('Y-m-d', strtotime($s_date. ' + 1 days'));
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'ctr']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')
										->update(['value' => 0]);
									}
									
									$s_date = date('Y-m-d', strtotime($s_date. ' + 1 days'));
								}
							}
							else
							{
								$i++;
							}
						}
						else
						{
							$i++;
						}
					}
				}
			}
			
			$FacebookAccounts = DB::table('facebook_ads_lead_users')
			->join('facebook_accounts', 'facebook_ads_lead_users.facebook_account_id', '=', 'facebook_accounts.id')
			->select('facebook_ads_lead_users.adset_id', 'facebook_ads_lead_users.adset_name', 'facebook_accounts.access_token')
			->groupBy('facebook_ads_lead_users.adset_id')
			->orderBy('facebook_ads_lead_users.adset_id', 'ASC')
			->get();
			foreach($FacebookAccounts as $FacebookAccount)
			{
				$adset_id = $FacebookAccount->adset_id;
				$access_token = $FacebookAccount->access_token;
				$adset_date = '';
				$CheckExists = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('category', 'ctr')->orderBy('id', 'DESC')->first();
				if(!is_null($CheckExists))
				{
					$adset_date = date('Y-m-d', strtotime($CheckExists->adset_date. ' - 1 days'));
				}
				
				$curl = curl_init('https://graph.facebook.com/v2.10/'.$adset_id.'/insights?&date_preset=lifetime&fields=ctr&access_token='.$access_token);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				$response = curl_exec($curl);
				$result = json_decode($response,true);
				
				//echo "<pre>";
				//print_r($result);
				
				if(isset($result['data'][0]['ctr']))
				{
					$date_start = $result['data'][0]['date_start']; 
					$date_stop = $result['data'][0]['date_stop'];
					
					if($adset_date == '')
					{
						$s_date = $date_start;
					}
					else
					{
						$s_date = $adset_date;
					}
					$e_date = $date_start;
					
					$i = 0;
					while($i == 0)
					{
						if(strtotime($s_date) <= strtotime($date_stop))
						{							
							$curl = curl_init('https://graph.facebook.com/v2.10/'.$adset_id.'/insights?&time_range[since]='.$s_date.'&time_range[until]='.$s_date.'&fields=ctr&access_token='.$access_token);
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
							$response1 = curl_exec($curl);
							$result1 = json_decode($response1,true);
							
							//echo "<pre>";
							//print_r($result1);
							
							if(isset($result1['data']))
							{
								if(isset($result1['data'][0]['ctr']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1['data'][0]['ctr'], 'category' => 'ctr']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')
										->update(['value' => $result1['data'][0]['ctr']]);
									}
									
									$s_date = date('Y-m-d', strtotime($s_date. ' + 1 days'));
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'ctr']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')
										->update(['value' => 0]);
									}
									
									$s_date = date('Y-m-d', strtotime($s_date. ' + 1 days'));
								}
							}
							else
							{
								$i++;
							}
						}
						else
						{
							$i++;
						}
					}
				}
			}
		}
	}