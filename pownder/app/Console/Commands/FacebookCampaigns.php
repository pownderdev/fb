<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
	
	class FacebookCampaigns extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:FacebookCampaigns';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Get Facebook Campaigns';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$counter = 1;
			
			$s_date = '2017-01-01';
			$e_date = date('Y-m-d');
			
			$FacebookAccounts = DB::table('facebook_accounts')
			->leftJoin('facebook_campaigns', 'facebook_accounts.id', '=', 'facebook_campaigns.facebook_account_id')
			->select('facebook_campaigns.id', 'facebook_accounts.access_token')
			->where('facebook_accounts.is_deleted', 0)
			->where('facebook_campaigns.is_deleted', 0)
			->groupBy('facebook_campaigns.id')
			->orderBy('facebook_campaigns.updated_at', 'DESC')
			->get();
			
			foreach($FacebookAccounts as $FacebookAccount)
			{
				if($counter%50 == 0)
				{
					sleep(10);
				}
				
				$counter++;
				
				$campaign_id = $FacebookAccount->id;
				$access_token = $FacebookAccount->access_token;
				
				$CheckExists = DB::table('facebook_campaign_values')->where('campaign_id', $campaign_id)->orderBy('id', 'DESC')->first();
				if(!is_null($CheckExists))
				{
					$s_date = date('Y-m-d', strtotime($CheckExists->campaign_date. ' - 3 days'));
				}
				
				//echo "<pre>";
				//echo 'facebook campaigns id = '.$campaign_id;
				
				$curl = curl_init('https://graph.facebook.com/v2.10/'.$campaign_id.'/insights?time_range[since]='.$s_date.'&time_range[until]='.$e_date.'&fields=reach,impressions,clicks,frequency,ctr,spend&time_increment=1&limit=2500&access_token='.$access_token);
				
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				$response = curl_exec($curl);
				curl_close($curl);
				
				$result = json_decode($response,true);
				
				//echo "<pre>";
				//print_r($result);
				
				if(isset($result['data']))
				{
					$data = $result['data'];
					if(count($data) > 0)
					{
						for($j = 0; $j < count($data); $j++)
						{
							$campaign_date = $data[$j]['date_start'];
							$reach = isset($data[$j]['reach']) ? $data[$j]['reach'] : 0;
							$impressions = isset($data[$j]['impressions']) ? $data[$j]['impressions'] : 0;
							$clicks = isset($data[$j]['clicks']) ? $data[$j]['clicks'] :0;
							$frequency = isset($data[$j]['frequency']) ? $data[$j]['frequency'] : 0;
							$ctr = isset($data[$j]['ctr']) ? $data[$j]['ctr'] : 0;
							$spend = isset($data[$j]['spend']) ? $data[$j]['spend'] : 0;
							
							$leads = DB::table('facebook_ads_lead_users')->select('id')->where('campaign_id', $campaign_id)->where('lead_type', 'Pownder™ Lead')->whereDate('created_time', $campaign_date)->count('id');
							$cpa = 0;
							if($spend > 0 && $leads >0)
							{
								$cpa = $spend/$leads;
							}
							
							$lgr = 0;
							if($clicks > 0 && $leads > 0)
							{
								$lgr = ($leads/$clicks)*100;
							}
							
							$Check = DB::table('facebook_campaign_values')->where('campaign_id', $campaign_id)->where('campaign_date', $campaign_date)->first();
							if(is_null($Check))
							{
								DB::table('facebook_campaign_values')->insert(
								['campaign_id' => $campaign_id, 'campaign_date' => $campaign_date, 'clicks' => $clicks, 'cpa' => $cpa, 'ctr' => $ctr, 'frequency' => $frequency, 'impressions' => $impressions, 'leads' => $leads, 'lgr' => $lgr, 'reach' => $reach, 'spend' => $spend, 'created_at' =>date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d  H:i:s')]
								);
							}
							else
							{
								DB::table('facebook_campaign_values')
								->where('campaign_id', $campaign_id)->where('campaign_date', $campaign_date)
								->update(['clicks' => $clicks, 'cpa' => $cpa, 'ctr' => $ctr, 'frequency' => $frequency, 'impressions' => $impressions, 'leads' => $leads, 'lgr' => $lgr, 'reach' => $reach, 'spend' => $spend, 'updated_at' => date('Y-m-d H:i:s') ]);
							} 
						}
					}	
				}
			}
		}
	}				