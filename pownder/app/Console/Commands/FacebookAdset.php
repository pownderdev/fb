<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
	
	class FacebookAdset extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:FacebookAdset';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Get Facebook Adset';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$last_adset_id = 0;
			$last_CheckExists = DB::table('facebook_adset_values')->where('category', 'spend')->orderBy('id', 'DESC')->first();
			if(!is_null($last_CheckExists))
			{
				$last_adset_id = $last_CheckExists->adset_id;
			}
			
			$FacebookAccounts = DB::table('facebook_ads_lead_users')
			->join('facebook_accounts', 'facebook_ads_lead_users.facebook_account_id', '=', 'facebook_accounts.id')
			->select('facebook_ads_lead_users.adset_id', 'facebook_ads_lead_users.adset_name', 'facebook_accounts.access_token')
			->where('facebook_ads_lead_users.adset_id', '>=', $last_adset_id)
			->groupBy('facebook_ads_lead_users.adset_id')
			->orderBy('facebook_ads_lead_users.adset_id', 'ASC')
			->take(5)
			->get();
			
			$take = 5 - count($FacebookAccounts);
			
			foreach($FacebookAccounts as $FacebookAccount)
			{
				$adset_id = $FacebookAccount->adset_id;
				$access_token = $FacebookAccount->access_token;
				$adset_date = '';
				$CheckExists = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('category', 'spend')->orderBy('id', 'DESC')->first();
				if(!is_null($CheckExists))
				{
					$adset_date = date('Y-m-d', strtotime($CheckExists->adset_date. ' - 1 days'));
				}
				
				$curl = curl_init('https://graph.facebook.com/v2.10/'.$adset_id.'/insights?&date_preset=lifetime&fields=spend&access_token='.$access_token);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				$response = curl_exec($curl);
				$result = json_decode($response,true);
				
				//echo "<pre>";
				//print_r($result);
				
				if(isset($result['data'][0]['spend']))
				{
					$date_start = $result['data'][0]['date_start']; 
					$date_stop = $result['data'][0]['date_stop'];
					
					if($adset_date == '')
					{
						$s_date = $date_start;
					}
					else
					{
						$s_date = $adset_date;
					}
					$e_date = $date_start;
					
					$i = 0;
					$j = 0;
					while($i == 0)
					{
						if(strtotime($s_date) <= strtotime($date_stop))
						{		
							$j++;
							$curl = curl_init('http://big.pownder.com/fblogin/facebook_adset_insights.php?adset_id='.$adset_id.'&s_date='.$s_date.'&access_token='.$access_token);
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
							$responses = curl_exec($curl);
							$info = curl_getinfo($curl);
							$result1 = json_decode($responses,true);
							
							//echo "<pre>";
							//print_r($result1);
							
							if(isset($result1[0]['data']))
							{
								if(isset($result1[0]['data'][0]['impressions']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'impressions')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1[0]['data'][0]['impressions'], 'category' => 'impressions']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'impressions')
										->update(['value' => $result1[0]['data'][0]['impressions']]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'impressions')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'impressions']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'impressions')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							if(isset($result1[1]['data']))
							{
								if(isset($result1[1]['data'][0]['ctr']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1[1]['data'][0]['ctr'], 'category' => 'ctr']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')
										->update(['value' => $result1[1]['data'][0]['ctr']]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'ctr']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							if(isset($result1[3]['data']))
							{
								if(isset($result1[3]['data'][0]['clicks']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'clicks')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1[3]['data'][0]['clicks'], 'category' => 'clicks']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'clicks')
										->update(['value' => $result1[3]['data'][0]['clicks']]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'clicks')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'clicks']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'clicks')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							if(isset($result1[4]['data']))
							{
								if(isset($result1[4]['data'][0]['spend']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'spend')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1[4]['data'][0]['spend'], 'category' => 'spend']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'spend')
										->update(['value' => $result1[4]['data'][0]['spend']]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'spend')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'spend']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'spend')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							if(isset($result1[5]['data']))
							{
								if(isset($result1[5]['data'][0]['frequency']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'frequency')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1[5]['data'][0]['frequency'], 'category' => 'frequency']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'frequency')
										->update(['value' => $result1[5]['data'][0]['frequency']]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'frequency')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'frequency']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'frequency')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							if(isset($result1[6]['data']))
							{
								if(isset($result1[6]['data'][0]['reach']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'reach')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1[6]['data'][0]['reach'], 'category' => 'reach']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'reach')
										->update(['value' => $result1[6]['data'][0]['reach']]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'reach')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'reach']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'reach')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							if(isset($result1[2]['data']))
							{
								if(isset($result1[2]['data'][0]['cost_per_action_type']))
								{
									$cost_per_action_type = 0;
									$abcd = $result1[2]['data'][0]['cost_per_action_type'];
									for($a = 0; $a < count($abcd); $a++)
									{
										$cost_per_action_type = $cost_per_action_type + $abcd[$a]['value'];
									}
									
									
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'cost_per_action_type')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $cost_per_action_type, 'category' => 'cost_per_action_type']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'cost_per_action_type')
										->update(['value' => $cost_per_action_type]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'cost_per_action_type')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'cost_per_action_type']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'cost_per_action_type')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							$s_date = date('Y-m-d', strtotime($s_date. ' + 1 days'));
							
							if($j == 5)
							{
								$i++;
							}
						}
						else
						{
							$i++;
						}
					}
				}
				else
				{
					continue;
				}
				
				sleep(5);
			}
			
			$FacebookAccounts = DB::table('facebook_ads_lead_users')
			->join('facebook_accounts', 'facebook_ads_lead_users.facebook_account_id', '=', 'facebook_accounts.id')
			->select('facebook_ads_lead_users.adset_id', 'facebook_ads_lead_users.adset_name', 'facebook_accounts.access_token')
			->groupBy('facebook_ads_lead_users.adset_id')
			->orderBy('facebook_ads_lead_users.adset_id', 'ASC')
			->take($take)
			->get();
			foreach($FacebookAccounts as $FacebookAccount)
			{
				$adset_id = $FacebookAccount->adset_id;
				$access_token = $FacebookAccount->access_token;
				$adset_date = '';
				$CheckExists = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('category', 'spend')->orderBy('id', 'DESC')->first();
				if(!is_null($CheckExists))
				{
					$adset_date = date('Y-m-d', strtotime($CheckExists->adset_date. ' - 1 days'));
				}
				
				$curl = curl_init('https://graph.facebook.com/v2.10/'.$adset_id.'/insights?&date_preset=lifetime&fields=spend&access_token='.$access_token);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				$response = curl_exec($curl);
				$result = json_decode($response,true);
				
				//echo "<pre>";
				//print_r($result);
				
				if(isset($result['data'][0]['spend']))
				{
					$date_start = $result['data'][0]['date_start']; 
					$date_stop = $result['data'][0]['date_stop'];
					
					if($adset_date == '')
					{
						$s_date = $date_start;
					}
					else
					{
						$s_date = $adset_date;
					}
					$e_date = $date_start;
					
					$i = 0;
					$j = 0;
					while($i == 0)
					{
						if(strtotime($s_date) <= strtotime($date_stop))
						{		
							$j++;
							$curl = curl_init('http://big.pownder.com/fblogin/facebook_adset_insights.php?adset_id='.$adset_id.'&s_date='.$s_date.'&access_token='.$access_token);
							curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
							$responses = curl_exec($curl);
							$info = curl_getinfo($curl);
							$result1 = json_decode($responses,true);
							
							//echo "<pre>";
							//print_r($result1);
							
							if(isset($result1[0]['data']))
							{
								if(isset($result1[0]['data'][0]['impressions']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'impressions')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1[0]['data'][0]['impressions'], 'category' => 'impressions']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'impressions')
										->update(['value' => $result1[0]['data'][0]['impressions']]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'impressions')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'impressions']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'impressions')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							if(isset($result1[1]['data']))
							{
								if(isset($result1[1]['data'][0]['ctr']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1[1]['data'][0]['ctr'], 'category' => 'ctr']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')
										->update(['value' => $result1[1]['data'][0]['ctr']]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'ctr']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'ctr')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							if(isset($result1[3]['data']))
							{
								if(isset($result1[3]['data'][0]['clicks']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'clicks')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1[3]['data'][0]['clicks'], 'category' => 'clicks']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'clicks')
										->update(['value' => $result1[3]['data'][0]['clicks']]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'clicks')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'clicks']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'clicks')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							if(isset($result1[4]['data']))
							{
								if(isset($result1[4]['data'][0]['spend']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'spend')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1[4]['data'][0]['spend'], 'category' => 'spend']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'spend')
										->update(['value' => $result1[4]['data'][0]['spend']]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'spend')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'spend']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'spend')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							if(isset($result1[5]['data']))
							{
								if(isset($result1[5]['data'][0]['frequency']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'frequency')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1[5]['data'][0]['frequency'], 'category' => 'frequency']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'frequency')
										->update(['value' => $result1[5]['data'][0]['frequency']]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'frequency')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'frequency']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'frequency')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							if(isset($result1[6]['data']))
							{
								if(isset($result1[6]['data'][0]['reach']))
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'reach')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $result1[6]['data'][0]['reach'], 'category' => 'reach']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'reach')
										->update(['value' => $result1[6]['data'][0]['reach']]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'reach')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'reach']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'reach')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							if(isset($result1[2]['data']))
							{
								if(isset($result1[2]['data'][0]['cost_per_action_type']))
								{
									$cost_per_action_type = 0;
									$abcd = $result1[2]['data'][0]['cost_per_action_type'];
									for($a = 0; $a < count($abcd); $a++)
									{
										$cost_per_action_type = $cost_per_action_type + $abcd[$a]['value'];
									}
									
									
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'cost_per_action_type')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => $cost_per_action_type, 'category' => 'cost_per_action_type']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'cost_per_action_type')
										->update(['value' => $cost_per_action_type]);
									}
								}
								else
								{
									$Check = DB::table('facebook_adset_values')->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'cost_per_action_type')->first();
									if(is_null($Check))
									{
										DB::table('facebook_adset_values')->insert(
										['adset_id' => $adset_id, 'adset_date' => $s_date, 'value' => 0, 'category' => 'cost_per_action_type']
										);
									}
									else
									{
										DB::table('facebook_adset_values')
										->where('adset_id', $adset_id)->where('adset_date', $s_date)->where('category', 'cost_per_action_type')
										->update(['value' => 0]);
									}
								}
							}
							else
							{
								$i++;
							}
							
							$s_date = date('Y-m-d', strtotime($s_date. ' + 1 days'));
							
							if($j == 5)
							{
								$i++;
							}
						}
						else
						{
							$i++;
						}
					}
				}
				else
				{
					continue;
				}
				
				sleep(5);
			}
		}
	}