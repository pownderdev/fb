<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
	class NoClosedDealNotification extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:NoClosedDealNotification';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'No Closed Deals Data Feed';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
        
        var $now='';        
        const SENDGRID_USER='help@constacloud.in'; //Sendgrid user name to send emails
        const SENDGRID_PASS='main1234';            //Sendgrid password to send emails
       
		public function __construct()
		{
			parent::__construct();
            $this->setTimeZone('America/Los_Angeles');
            $this->now=date('Y-m-d H:i:s');
			
		}
		public function setTimeZone($tz)
        {
			return  date_default_timezone_set($tz);
		}
       
        
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
            $from=date('Y-m-d').' 12:00:00';
			$to=date('Y-m-d').' 09:59:00';
            		
            $count_new_data=DB::table('ftp_csv_data_1')->whereIn(DB::raw('LOWER(col69)'),["new","used","n","u"])->where([['created_at','>=',$from],['created_at','<=',$to]])->count('id');  
            
            if(!$count_new_data)
            {           
            
            $admin_email_data=DB::table('admins')->where([['is_deleted',0],['status',1]])->select('id','close_deal_email','close_deal_option','invoice_option')->first();
            if(is_null($admin_email_data))
            {
               // Admin Deleted Or Inactive
            }
            else if($admin_email_data->invoice_option=='No')
            {
                // Admin Don't want notification
            }            
            else
            {
                
            $urls = 'https://api.sendgrid.com/';
            $users = self::SENDGRID_USER;
            $passs = self::SENDGRID_PASS;
            $subject='No Closed Deals Data Feed';
            $msg='No new closed deals data was received till 10 AM Today.';
            $from='noreply@pownder.com';
                    
            $paramss = array(
            'api_user' => $users,
            'api_key' => $passs,            
            'subject' => $subject,
            'html' => $msg,
            'from' => $from,
            'fromname' => "Pownder�",
            'to'=>$admin_email_data->close_deal_email            
            );
                        
            $requests = $urls.'api/mail.send.json';
            $sessions = curl_init($requests);
            curl_setopt ($sessions, CURLOPT_POST, true);
            curl_setopt ($sessions, CURLOPT_POSTFIELDS, $paramss);
            curl_setopt($sessions, CURLOPT_HEADER, false);
            curl_setopt($sessions, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($sessions, CURLOPT_RETURNTRANSFER, true);
            $responses = curl_exec($sessions);
            $info=curl_getinfo($sessions);
            curl_close($sessions);
            } 
            
            }   
		}
	}
