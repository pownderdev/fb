<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
	use Helper;
	use ConsoleHelper;
	
	class CreateAutomotiveContact extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:CreateAutomotiveContact';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Create Automotive Contact';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$AutomotiveCRM = array('Advent', 'CDK', 'Dealerpeak', 'DealerSocket', 'DealerTrack', 'Dominion Dealer Solutions', 'eflow automotive', 'eLead CRM', 'FordDirect', 'HigherGear', 'Momentum', 'ProMax Unlimited', 'Reynolds & Reynolds', 'VinSolutions', 'Votenza');
			
			$Campaigns = DB::table('campaigns')->whereIn('crm_account', $AutomotiveCRM)->where('is_active', 0)->where('is_deleted', 0)->orderBy('updated_at', 'DESC')->get();
			foreach($Campaigns as $Campaign)
			{
				$campaign_id = $Campaign->id;
				$client_id = $Campaign->client_id;
				$LeadType = explode(",", $Campaign->manual_lead_type);
				$LeadForm = explode(",", $Campaign->facebook_ads_lead_id);
				$lead_sync = $Campaign->lead_sync;
				$active_at = $Campaign->active_at;
				$crm_account = $Campaign->crm_account;
				
				$autometive_campaigns = DB::table('autometive_campaigns')->where('campaign_id', $campaign_id)->where('is_deleted', 0)->get();
				foreach($autometive_campaigns as $autometive_campaign)
				{
					$autometive_campaign_id = $autometive_campaign->id;
					$prospect = $autometive_campaign->prospect;
					$phone_time = $autometive_campaign->phone_time;
					$phone_type = $autometive_campaign->phone_type;
					
					$provider_name = $autometive_campaign->provider_name;
					$service_name = $autometive_campaign->service_name;
					$provider_email = $autometive_campaign->provider_email;
					$provider_phone = $autometive_campaign->provider_phone;
					$provider_url = $autometive_campaign->provider_url;
					$provider_zip = $autometive_campaign->provider_zip;
					$provider_city = $autometive_campaign->provider_city;
					$provider_state = $autometive_campaign->provider_state;
					
					$vendor_name = $autometive_campaign->vendor_name;
					$vendor_email = $autometive_campaign->vendor_email;
					$vendor_phone = $autometive_campaign->vendor_phone;
					$vendor_url = $autometive_campaign->vendor_url;
					$vendor_address = $autometive_campaign->vendor_address;
					$vendor_zip = $autometive_campaign->vendor_zip;
					$vendor_city = $autometive_campaign->vendor_city;
					$vendor_state = $autometive_campaign->vendor_state;
					
					$facebookAdsLeadUsers = DB::table('facebook_ads_lead_users')
					->where(function ($query) use ($LeadForm, $LeadType){
						$query->whereIn('facebook_ads_lead_id', $LeadForm)
						->whereIn('lead_type', $LeadType, 'or');
					})
					->where(function ($query) use ($lead_sync, $active_at){
						if($lead_sync == 'Only New')
						{
							$query->whereDate('created_time', '>=', $active_at);
						}
					})
					->whereNull('messenger_lead_id')
					->where('client_id', $client_id)
					->where('is_deleted', 0)
					->where('crm_contact_id', 0)
					->get();
					
					foreach($facebookAdsLeadUsers as $facebookAdsLeadUser)
					{
						$facebook_ads_lead_user_id = $facebookAdsLeadUser->id;
						$facebook_ads_lead_id = $facebookAdsLeadUser->facebook_ads_lead_id;
						$facebook_page_id = $facebookAdsLeadUser->facebook_page_id;
						$facebook_account_id = $facebookAdsLeadUser->facebook_account_id;
						
						$address = $facebookAdsLeadUser->street_address;
						$state = $facebookAdsLeadUser->state; 
						
						$first_name = '';
						$last_name = '';
						$email = '';
						$phone = '';
						$city = '';
						$post_code = '';
						
						if($autometive_campaign->auto_email == 'Yes')
						{
							$email = $facebookAdsLeadUser->email;
						}
						
						if($autometive_campaign->auto_first_name == 'Yes')
						{
							if($facebookAdsLeadUser->first_name == '')
							{
								$first_name = $facebookAdsLeadUser->full_name;
							}
							else
							{
								$first_name = $facebookAdsLeadUser->first_name;
							}
						}
						
						if($autometive_campaign->auto_last_name == 'Yes')
						{
							$last_name = $facebookAdsLeadUser->last_name;
						}
						
						if($autometive_campaign->auto_phone_number == 'Yes')
						{
							$phone = $facebookAdsLeadUser->phone_number;
						}
						
						if($autometive_campaign->auto_city == 'Yes')
						{
							$city = $facebookAdsLeadUser->city;
						}
						
						if($autometive_campaign->auto_zip_code == 'Yes')
						{
							$post_code = $facebookAdsLeadUser->post_code;
						}
						
						$response = '';
						
						if($crm_account == 'Advent')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'CDK')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'Dealerpeak')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'DealerSocket')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'DealerTrack')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'Dominion Dealer Solutions')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'eflow automotive')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'eLead CRM')
						{
							$response = ConsoleHelper::eLeadCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'FordDirect')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'HigherGear')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'Momentum')
						{
							$response = ConsoleHelper::MomentumCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'ProMax Unlimited')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'Reynolds & Reynolds')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'VinSolutions')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						else if($crm_account == 'Votenza')
						{
							$response = ConsoleHelper::DealerSocketCRM($campaign_id, $prospect, $phone_time, $phone_type, $provider_name, $service_name, $provider_email, $provider_phone, $provider_url, $provider_zip, $provider_city, $provider_state, $vendor_name, $vendor_email, $vendor_phone, $vendor_url, $vendor_address, $vendor_zip, $vendor_city, $vendor_state, $first_name, $last_name, $email, $phone, $address, $city, $state, $post_code);
						}
						
						if($response == 'success')
						{
							$crm_contact_id = DB::table('crm_contacts')->insertGetId([
							'facebook_account_id' => $facebook_account_id,
							'facebook_page_id' => $facebook_page_id,
							'facebook_ads_lead_id' => $facebook_ads_lead_id,
							'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
							'campaign_id' => $campaign_id,
							'client_id' => $client_id,
							'autometive_campaign_id' => $autometive_campaign_id,
							'crm_account' => $crm_account,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_at' => date('Y-m-d H:i:s')
							]);
							
							DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
							->update(['crm_contact_id' => $crm_contact_id]);
							
							DB::table('resync_error')->where('facebook_ads_lead_user_id', $facebook_ads_lead_user_id)->where('is_deleted', 0)
							->update(['updated_user_id' => 0, 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
						}
					}
				}
			}
		}
	}
