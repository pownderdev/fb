<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	
	use Mail;
	use DB;
	
	class SendEmailClientZeroLead extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:SendEmailClientZeroLead';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Send Email Client Zero Lead Every 12 Hours';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$admin = DB::table('admins')->where('id', 1)->where('other_email', '!=', '')->where('other_option', 'Yes')->where('is_deleted', 0)->first();
			if(!is_null($admin))
			{
				$i = 1;
				$sendMail = 'No';
				$data = array();
				$timeBefore12hours = date('Y-m-d H:i:s', strtotime('-12 hours'));
				$clients = DB::table('clients')->where('status', 1)->where('is_deleted', 0)->orderBy('name', 'ASC')->get();
				foreach($clients as $client)
				{
					$client_id = $client->id;
					$leads = DB::table('client_ads_lead_users')->select('id')->where('client_id', $client_id)->where('created_at', '>=', $timeBefore12hours)->count('id');
					if($leads == 0)
					{
						$record = array();
						$record['index'] = $i++;
						$record['name'] = $admin->name;
						$record['client_name'] = $client->name;
						$data[] = $record;
						$sendMail = 'Yes';
					}
				}
				
				if($sendMail == 'Yes')
				{
					Mail::send('emails.client0lead', ["data" => $data], function ($message) use ($admin) {
						$message->from('noreply@pownder.com', 'Pownder');
						$message->to($admin->other_email)->subject('Active client is not getting leads');
					});
				}
			}
		}	
	}
