<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
	use Mail;
	
	class FacebookLeadDetails extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:FacebookLeadDetails';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Get Facebook Lead Details';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')
			->leftJoin('facebook_pages', 'facebook_ads_lead_users.facebook_page_id', '=', 'facebook_pages.id')
			->select('facebook_ads_lead_users.id', 'facebook_pages.access_token')
			->where('facebook_pages.is_deleted', 0)
			->where('facebook_ads_lead_users.ad_id', 0)
			->where('facebook_ads_lead_users.adset_id', 0)
			->where('facebook_ads_lead_users.campaign_id', 0)
			->where('facebook_ads_lead_users.lead_type', 'Pownder™ Lead')
			->groupBy('facebook_ads_lead_users.id')
			->orderBy('facebook_ads_lead_users.created_time', 'DESC')
			->get();
			
			//echo "<pre>";
			//echo count($facebook_ads_lead_users);
			foreach($facebook_ads_lead_users as $row)
			{
				$lead_id = $row->id;
				$access_token = $row->access_token;
				
				$curl = curl_init('https://graph.facebook.com/v2.10/'.$lead_id. '?fields=id,created_time,ad_id,ad_name,adset_id,adset_name,campaign_id,campaign_name,form_id,field_data&access_token='. $access_token);
				
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				
				$response   = curl_exec($curl);
				
				curl_close($curl);
				
				$result = json_decode($response,true);
				
				//echo "<pre>";
				//print_r($result);
				
				if(isset($result['id']))
				{
					//echo $result['ad_name'];die;
					DB::table('facebook_ads_lead_users')->where('id', $lead_id)
					->update([
					'ad_id' => isset($result['ad_id'])?$result['ad_id']:0,
					'ad_name' => isset($result['ad_name'])?$result['ad_name']:'',
					'adset_id' => isset($result['adset_id'])?$result['adset_id']:0,
					'adset_name' => isset($result['adset_name'])?$result['adset_name']:'',
					'campaign_id' => isset($result['campaign_id'])?$result['campaign_id']:0,
					'campaign_name' => isset($result['campaign_name'])?$result['campaign_name']:''
					]);
				}
			}
		}
	}
		