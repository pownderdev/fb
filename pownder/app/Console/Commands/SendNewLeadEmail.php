<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	
	use Mail;
	use DB;
	
	class SendNewLeadEmail extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:SendNewLeadEmail';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Send New Lead Email';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$admin = DB::table('admins')->where('id', 1)->where('new_lead_email', '!=', '')->where('new_lead_option', 'Yes')->where('is_deleted', 0)->first();
			if(is_null($admin))
			{
				DB::table('admin_emails')->where('subject', 'NEW FACEBOOK LEAD')->where('is_deleted', 0)
				->update(['updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]); 
			}
			else
			{
				$data = array();
				$admin_emails = DB::table('admin_emails')->where('subject', 'NEW FACEBOOK LEAD')->where('is_deleted', 0)->get();
				foreach($admin_emails as $admin_email)
				{
					$record = array('name' => $admin->name, 'category' => 'admin', 'client_name' => $admin_email->client_name, 'lead_type' => $admin_email->leadType, 'UserName' => $admin_email->UserName, 'UserType' => $admin_email->UserType, 'first_name' => $admin_email->first_name, 'last_name' => $admin_email->last_name, 'email' => $admin_email->leadEmail, 'phone' => $admin_email->leadPhone, 'city' => $admin_email->leadCity);
					
					$data[] = $record;
				}
				
				Mail::send('emails.new_lead_cron', ["data" => $data], function ($message) use ($admin) {
					$message->from('noreply@pownder.com', 'Pownder');
					$message->to($admin->new_lead_email)->subject('NEW FACEBOOK LEAD');
				});
				
				DB::table('admin_emails')->where('subject', 'NEW FACEBOOK LEAD')->where('is_deleted', 0)
				->update(['name' => $admin->name, 'category' => 'admin', 'email' => $admin->new_lead_email, 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]); 
			}
		}	
	}
