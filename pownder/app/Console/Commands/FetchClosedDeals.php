<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
    
	class FetchClosedDeals extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:FetchClosedDeals';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Fetch closed deals';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
        
        var $now='';
		
        const FTPDETAILS=[        
        "1"=>['FTPHOST'=>"ftp.dealervault.com",'FTPUSER'=>"DVV01002",'FTPPASS'=>".18t[TJ)4oe2X"],
        "2"=>['FTPHOST'=>"ftp.dealervault.com",'FTPUSER'=>"DVV00531",'FTPPASS'=>"K;K^1#_=aK"]        
        
        ];       
		
		public function __construct()
		{
			parent::__construct();
            $this->setTimeZone('America/Los_Angeles');
            $this->now=$this->getCurrentDateTime();
			
		}
		public function setTimeZone($tz)
        {
			return  date_default_timezone_set($tz);
		}
        public function getCurrentDateTime()
        {
            date_default_timezone_set('America/Los_Angeles');
            return date('Y-m-d H:i:s');
		}
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			ini_set('max_execution_time',0);
			ini_set('memory_limit',-1);
			date_default_timezone_set('America/Los_Angeles');
            
			$times=DB::table('closed_deals_settings')->select('fetch_time1','fetch_time2','fetch_time3')->first(); 
            
            $autocron_time='';          
			
			if(strtotime($times->fetch_time1)<=strtotime(date('H:i:00')) && strtotime($times->fetch_time2)>strtotime(date('H:i:00')) && strtotime($times->fetch_time3)>strtotime(date('H:i:00')))
			{
				$has_runned=DB::table('cron_fetch_closed_deals')->where([["created_user_id",0],["created_user_category",'cron']])->first();
				
				if(!$has_runned)
				{
					DB::table('cron_fetch_closed_deals')->insert(["created_user_id"=>'0',"created_user_category"=>'cron',"created_at"=>$this->now,'remark'=>'time1']);
				    $autocron_time='time1';
                }
				else if($has_runned->remark=="time3")
				{
					DB::table('cron_fetch_closed_deals')->where([["created_user_id",0],["created_user_category",'cron']])
                    ->update(["created_at"=>$this->now,"updated_at"=>$this->now,'remark'=>'time1','status'=>1]);
				    $autocron_time='time1';
                }
                
			}
			else  if(strtotime($times->fetch_time1)<strtotime(date('H:i:00')) && strtotime($times->fetch_time2)<=strtotime(date('H:i:00')) && strtotime($times->fetch_time3)>strtotime(date('H:i:00')))
			{
				$has_runned=DB::table('cron_fetch_closed_deals')->where([["created_user_id",0],["created_user_category",'cron'],['remark','time2']])->first();
				if(!$has_runned)
				{
					DB::table('cron_fetch_closed_deals')->where([["created_user_id",0],["created_user_category",'cron']])
                    ->update(["updated_at"=>$this->now,'remark'=>'time2','status'=>1]);
                    $autocron_time='time2';
				}
                
			}
			else  if(strtotime($times->fetch_time1)<strtotime(date('H:i:00')) && strtotime($times->fetch_time2)<strtotime(date('H:i:00')) && strtotime($times->fetch_time3)<=strtotime(date('H:i:00')))
			{
				$has_runned=DB::table('cron_fetch_closed_deals')->where([["created_user_id",0],["created_user_category",'cron'],['remark','time3']])->first();
				if(!$has_runned)
				{
					DB::table('cron_fetch_closed_deals')->where([["created_user_id",0],["created_user_category",'cron']])
                    ->update(["updated_at"=>$this->now,'remark'=>'time3','status'=>1]);
                    $autocron_time='time3';
				}
                
			}
			
			$already_running=DB::table('cron_fetch_closed_deals')->where('status',2)->count('id'); 
			
			if(!$already_running)
			{
				$get_active=DB::table('cron_fetch_closed_deals')->where('status',1)->select('id')->first(); 
				
				if(count($get_active)>0)
				{
					DB::table('cron_fetch_closed_deals')->where([['id',$get_active->id],['status',1]])->update(['status'=>2,'updated_at'=>$this->now]); 
					
                    $new_records=$status_shown=$matched_leads=$new_managers=$unknown_client=0; 
                    
                    foreach(self::FTPDETAILS as $from_ftp=>$FTP)
                    {                        
						
						try
						{						
							$conn=ftp_connect($FTP['FTPHOST']) or die("Couldn't connect to ".$FTP['FTPHOST']."");
							try
							{
								$connected=ftp_login($conn,$FTP['FTPUSER'],$FTP['FTPPASS']);
								
								if($connected)
								{
									ftp_pasv($conn,true);
									
									ftp_set_option($conn, FTP_TIMEOUT_SEC ,3600000);
									
									//Get List Of All CSVs
									
									$files=ftp_nlist($conn, "/") or die("Couldn't read directory");
                                    
                                    $column_to_be_matched='';
                                    
                                    switch($from_ftp)
                                    {
                                        case "1" : $column_to_be_matched="client_no";break;
                                        case "2" : $column_to_be_matched="roi_id";break;
                                        default : $column_to_be_matched="client_no";break;
                                    }
									
									foreach($files as $file)
									{
										$filename_wtextension='';$ok=1;
										if($from_ftp=="2")
                                        {
                                            $filename_wtextension=explode('.',$file,2)[0];
                                            if(substr($filename_wtextension,-3,3)!="_SL")
                                            {
                                              $ok=0;  
                                            }
                                        }
                                        
                                        if($ok)
                                        {
                                        
                                        $download_file=fopen(__DIR__.'/../../../storage/app/ftp_csv/'.$file,'w');
										
										if(ftp_fget($conn,$download_file,'/'.$file,FTP_ASCII,0))
										{
											$csv_read=fopen(__DIR__.'/../../../storage/app/ftp_csv/'.$file,'r');
											
											$heads=fgetcsv($csv_read) or die('Could not read CSV.');
											
											DB::table('ftp_csv_heads')->truncate();
											
											foreach($heads as $val)
											{
												DB::table('ftp_csv_heads')->insert(["column_name"=>$val]);
											}
											
											$table1=$table2=$table3="";
											
											if(count($heads)>270)
											{
												$table1="ftp_csv_data_1";
												$table2="ftp_csv_data_2";
												$table3="ftp_csv_data_3";
											}
											else if(count($heads)>135)
											{
												$table1="ftp_csv_data_1";
												$table2="ftp_csv_data_2";
											}
											else
											{
												$table1="ftp_csv_data_1";
											}
											
											$insert1=$insert2=$insert3=$insert2_new=$insert3_new=[];											
											
											$insert_index=0;
                                            
											
											while(($row=fgetcsv($csv_read)) != FALSE )
											{
											  $value_to_be_matched = $row[1];  
                                                            
											  $client = DB::table('clients')->where($column_to_be_matched, $value_to_be_matched)->first(); 
											
                                              if(!is_null($client))   
											  {     		
												
                                                $already_exists=DB::table('ftp_csv_data_1')->where([['col5',$row[4]],['col70',$row[69]],['client_id',$client->id]])->count('id');
												if(!$already_exists) 
												{
													$collection1=$collection2=$collection3=[];
													for($i=0;$i<count($row);$i++)
													{                           
														if($i<=134)
														{
															$collection1["col".($i+1)]=$row[$i];														
														}
														else if($i<=269)
														{
															$collection2["col".($i+1)]=$row[$i];														
														}
														else
														{
															$collection3["col".($i+1)]=$row[$i];														
														}                           
													}
													$collection1['from_ftp']=$from_ftp;
                                                    $collection1['client_id']=$client->id;
                                                    $collection1['created_at']=$this->now;
													$new_records++; //New records found in CSV
													
													$source = "Other";
													$email_to_be_matched_in_leads=array();
													if(trim($collection1['col23']))
													{
														$email_to_be_matched_in_leads[]=strtolower(trim($collection1['col23']));
													}
													if(trim($collection1['col24']))
													{
														$email_to_be_matched_in_leads[]=strtolower(trim($collection1['col24']));
													}
													if(trim($collection1['col25']))
													{
														$email_to_be_matched_in_leads[]=strtolower(trim($collection1['col25']));
													}
													$email_to_be_matched_in_leads=array_unique($email_to_be_matched_in_leads);
													$phone_to_be_matched_in_leads=array();
													if(trim($collection1['col19']))
													{
														$phone_to_be_matched_in_leads[]=trim($collection1['col19']);
													}
													if(trim($collection1['col20']))
													{
														$phone_to_be_matched_in_leads[]=trim($collection1['col20']);
													}
													if(trim($collection1['col21']))
													{
														$phone_to_be_matched_in_leads[]=trim($collection1['col21']);
													}
													$phone_to_be_matched_in_leads=array_unique($phone_to_be_matched_in_leads);
                                                    
													if(count($email_to_be_matched_in_leads)>0 || count($phone_to_be_matched_in_leads)>0)
													{
														$contract_date=$collection2['col153'];
														$source_match = DB::table('facebook_ads_lead_users')
														->where(function($query) use($email_to_be_matched_in_leads,$phone_to_be_matched_in_leads,$contract_date){
															if(count($email_to_be_matched_in_leads)>0 && count($phone_to_be_matched_in_leads)>0)
															{
																$query->whereRaw('DATE(created_time)<=STR_TO_DATE("'.$contract_date.'","%m/%d/%Y") and  lower(email) IN ("'.implode('","',$email_to_be_matched_in_leads).'") and email !="" and email IS NOT NULL ')->orWhereRaw('DATE(created_time)<=STR_TO_DATE("'.$contract_date.'","%m/%d/%Y") and  lower(phone_number) IN ("'.strtolower(implode('","',$phone_to_be_matched_in_leads)).'") and phone_number !="" and phone_number IS NOT NULL');                                                               
															}
															else if(count($email_to_be_matched_in_leads)>0)
															{
																$query->whereRaw('DATE(created_time)<=STR_TO_DATE("'.$contract_date.'","%m/%d/%Y") and lower(email) IN ("'.implode('","',$email_to_be_matched_in_leads).'") and email !="" and email IS NOT NULL');															    
                                                            }
															else if(count($phone_to_be_matched_in_leads)>0)
															{
																$query->whereRaw('DATE(created_time)<=STR_TO_DATE("'.$contract_date.'","%m/%d/%Y") and lower(phone_number) IN ("'.strtolower(implode('","',$phone_to_be_matched_in_leads)).'") and phone_number !="" and phone_number IS NOT NULL ');															    
                                                            }
														})
														->select('lead_type','id','email','phone_number','created_time','setting')->get();
														
														$matched_emails=$matched_phone_number=[];$leads_times='';
														if(count($source_match)>0)
														{
															foreach($source_match as $lead)
															{
																if($collection2['col149']=="S")
																{
																	DB::table('facebook_ads_lead_users')->where('id',$lead->id) ->update(["appointment_status"=>"Shown"]);
                                                                    
                                                                    $lastData=DB::table('facebook_ads_lead_user_settings')->where([['setting','Appointment'],['facebook_ads_lead_user_id',$lead->id],['is_deleted',0]])->select('id')->orderByDesc('id')->first();
                                                                    
                                                                    if(!is_null($lastData))
                                                                    {
                                                                    DB::table('facebook_ads_lead_user_settings')->where('id',$lastData->id)->update(['comment' => 'Shown', 'updated_user_id' => 0, 'updated_at' => $this->now ]);
                                                                    }
                                                                    
																	$status_shown++;// Status updated to shown
																}
																(trim($lead->email) && !in_array($lead->email,$matched_emails) && in_array(strtolower($lead->email),$email_to_be_matched_in_leads)) ? array_push($matched_emails,$lead->email) : '';
                                                                (trim($lead->phone_number) && !in_array($lead->phone_number,$matched_phone_number) && in_array($lead->phone_number,$phone_to_be_matched_in_leads)) ? array_push($matched_phone_number,$lead->phone_number) : '';
                                                                
																$leads_times=$lead->created_time;
															}
															$matched_emails=implode(',',$matched_emails);
															$matched_phone_number=implode(',',$matched_phone_number);
															
															$source=trim(str_ireplace("Lead","",$source_match[0]->lead_type));
															$source=trim(str_ireplace("Messenger","",$source));
															$source=trim(str_ireplace("Call","",$source));
															$matched_leads++; //record matched to leads
														}
													}
													$collection1['source']=$source;
													$collection1['department']='7';
													
													if($table1 && $table2)
													{                                                    
														  
															//Show this as sold lead in Report page 
															
															if($source != "Other")
															{ 
																$Full_Name=  $collection1['col7']; 
																$BuyerFullName = $collection1['col36'];  //Co-Buyer Full Name
																$StockNumber = $collection1['col70'];
																$DealNumber = $collection1['col5'];     
																$Year = $collection1['col62'];       
																$Make = $collection1['col63'];   
																$Model = $collection1['col64'];
																$TotalProfit = trim(str_replace(",","",$collection2['col146']));
																$SoldDate = date('Y-m-d',strtotime($collection2['col153']));      
																$sold_lead = DB::table('sold_leads')->where([['client_id',  $client->id],['stock', $StockNumber],['deal_number', $DealNumber]])->first();    
																if(is_null($sold_lead))    
																{     
																	DB::table('sold_leads')->insert(['category' => 'Sold', 'client_id' => $client->id, 'real_buyer_name' => $BuyerFullName, 'sold_date' => $SoldDate, 'stock' => $StockNumber, 'sold_year' => $Year, 'group_sub_category_id' => $Make,  'model' => $Model,  'total_profit' => $TotalProfit, 'facebook_ads_lead_user_name'=>$Full_Name , 'deal_number'=>$DealNumber, 'source'=>$collection1['source'], 'email'=>$matched_emails, 'phone_number'=>$matched_phone_number, 'created_at' => $leads_times, 'created_user_id'=>'0',  'updated_at' => $this->now , 'updated_user_id'=>0]);    
																}
																
															}
															//Save saleman1,... as clients's managers/user 
															
															$User = DB::table('users')->where([['client_id', $client->id],['category','client']])->first();
															$created_user_id = $User->id;				                                   
															$client_id = $client->id;
															$category = 'client'; 
															
															$users_arr=[
															"salesman1"=>["number"=>$collection1['col105'],"name"=>$collection1['col106']],
															"salesman2"=>["number"=>$collection1['col107'],"name"=>$collection1['col108']],
															"salesman3"=>["number"=>$collection1['col109'],"name"=>$collection1['col110']],
															"closing_m"=>["number"=>$collection1['col111'],"name"=>$collection1['col112']],
															"finance_m"=>["number"=>$collection1['col113'],"name"=>$collection1['col114']],
															"sales_m"=>["number"=>$collection1['col115'],"name"=>$collection1['col116']]
															];
															
															foreach($users_arr as $type=>$user)
															{
																$manager=$job_title="";
																
																if(trim($user['number']))
																{
																	
																	$manager=DB::table('managers')->where([['client_id',$client_id],['employee_id',$user['number']]])->select('department_id')->first();
																	$department_id='7';
																	switch($type)
																	{
																		case "salesman1": $job_title=6;break;//Sales Associate
																		case "salesman2": $job_title=6;break;//Sales Associate
																		case "salesman3": $job_title=6;break;//Sales Associate
																		case "finance_m": $job_title=16;break;// Finance manager
																		case "closing_m": $job_title=18;break;// Closing manager
																		case "sales_m": $job_title=11;break;// Sales manager
																		default: $job_title=6;break;//Sales Associate
																	}
																	
																	if(is_null($manager))
																	{
																		DB::table('managers')->insertGetId([
																		'client_id' => $client_id,
																		'department_id' => $department_id,
																		'job_title_id' => $job_title,
																		'team_id' => '0',
																		'category' => $category,
																		'user_type' => $category,
																		'first_name' => explode(' ',$user['name'],2)[0],
																		'last_name' => explode(' ',$user['name'],2)[1],
																		'full_name' => $user['name'],
																		'employee_id'=>$user['number'],
																		'employee_number'=>$user['number'],
																		'created_user_id' => $created_user_id,
																		'created_at' => $this->now,
																		'updated_user_id' => $created_user_id,
																		'updated_at' => $this->now
																		]);
																		
																		$new_managers++;
																	}
																	else
																	{
																		$collection1['department']=$manager->department_id;  
																	}
																} 		
															} 
															
													} 
													
													if($table1)
													{
														DB::table($table1)->insert($collection1); 
														$record_id=DB::select('select LAST_INSERT_ID() as id');
														$record_id=$record_id[0]->id;                           
													}
													if($table2)
													{
														$collection2['record_id']=$record_id;
														DB::table($table2)->insert($collection2); 
													}
													if($table3)
													{
														$collection3['record_id']=$record_id;
														DB::table($table3)->insert($collection3); 
													}
												}
                                              }
                                              else
                                              {
                                                $unknown_client++;
                                              }  											
											}
											
											//Close Opened File 
											fclose($download_file);
											
											//Now delete temporary downloaded csv file
											unlink(__DIR__.'/../../../storage/app/ftp_csv/'.$file);
										}
                                      }  
									}
									//Close Ftp Connection After All CSV Retrieved
									ftp_close($conn);
									
									//Now rest for 1 min and then read new FTP
									
									sleep(60);
								}
							}
							catch(Exception $e)
							{
								//echo $e->getMessage();
							}
						}
						catch(Exception $e)
						{
							//echo $e->getMessage();
						}
						
					} 
					
					//Close cron job
                    if($autocron_time=='time1' || !$autocron_time)
                    {
					DB::table('cron_fetch_closed_deals')->where([['id',$get_active->id],['status',2]])->update(['status'=>0,'new_records'=>$new_records,'status_shown'=>$status_shown,'matched_leads'=>$matched_leads,'new_managers'=>$new_managers,'unknown_client'=>$unknown_client,'updated_at'=>$this->now]);
                    }
                    else
                    {
					DB::table('cron_fetch_closed_deals')->where([['id',$get_active->id],['status',2]])->update(['status'=>0,'new_records'=>DB::raw('concat(new_records,"/","'.$new_records.'")'),'status_shown'=>DB::raw('concat(status_shown,"/","'.$status_shown.'")'),'matched_leads'=>DB::raw('concat(matched_leads,"/","'.$matched_leads.'")'),'new_managers'=>DB::raw('concat(new_managers,"/","'.$new_managers.'")'),'unknown_client'=>DB::raw('concat(unknown_client,"/","'.$unknown_client.'")'),'updated_at'=>$this->now]);
                    }  
				}	
			}
		}
	}
