<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	
	use DB;
	use Helper;
	use Mail;
	
	class PageEngagementLead extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:PageEngagementLead';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Page Engagement Lead';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$PageArray=array();
			$GroupPages = DB::table('group_page_engagements')
			->leftJoin('groups', 'group_page_engagements.group_id', '=', 'groups.id')
			->select('group_page_engagements.facebook_page_id')
			->where('group_page_engagements.is_deleted', 0)
			->where('groups.is_deleted', 0)
			->groupBy('group_page_engagements.facebook_page_id')
			->get();
			foreach($GroupPages as $GroupPage)
			{
				$PageArray[] = $GroupPage->facebook_page_id;
			}
			
			$ClientPages = DB::table('page_engagements')
			->leftJoin('clients', 'page_engagements.client_id', '=', 'clients.id')
			->select('page_engagements.facebook_page_id')
			->where('page_engagements.is_deleted', 0)
			->where('clients.is_deleted', 0)
			->groupBy('page_engagements.facebook_page_id')
			->get();
			foreach($ClientPages as $ClientPage)
			{
				$PageArray[] = $ClientPage->facebook_page_id;
			}
			
			//DB::table('facebook_ads_lead_users')->whereNotIn('facebook_page_id', $PageArray)->where('lead_type', 'Pownder™ Messenger')->where('latitude', '!=', '')->where('longitude', '!=', '')->where('client_id', 0)->where('facebook_ads_lead_id', 0)->where('is_deleted', 0)->get();
			
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')
			->leftJoin('facebook_pages', 'facebook_ads_lead_users.facebook_page_id', '=', 'facebook_pages.id')
			->select('facebook_ads_lead_users.*')
			->whereNotIn('facebook_ads_lead_users.facebook_page_id', $PageArray)
			->where('facebook_ads_lead_users.lead_type', 'Pownder™ Messenger')
			->where('facebook_ads_lead_users.latitude', '!=', '')
			->where('facebook_ads_lead_users.longitude', '!=', '')
			->where('facebook_ads_lead_users.client_id', 0)
			->where('facebook_ads_lead_users.facebook_ads_lead_id', 0)
			->where('facebook_ads_lead_users.is_deleted', 0)
			->where('facebook_pages.is_deleted', 0)
			->where('facebook_pages.is_blocked', 0)
			->get();
			
			foreach($facebook_ads_lead_users as $lead)
			{					
				$facebook_ads_lead_user_id = $lead->id;
				$facebook_account_id = $lead->facebook_account_id;
				$facebook_page_id = $lead->facebook_page_id;
				$facebook_ads_lead_id = $lead->facebook_ads_lead_id;
				$zip_code = $lead->zip_code;
				$latitude = $lead->latitude;
				$longitude = $lead->longitude;
				$user_id = $lead->created_user_id;
				
				$leadName = $lead->full_name;
				$leadCity = $lead->city;
				$leadEmail = $lead->email;
				$leadPhone = Helper::emailPhoneFormat($lead->phone_number);
				
				$miles = '';
				
				$clients = DB::table('clients')->where('status', 1)->where('is_deleted', 0)->get();
				foreach($clients as $client)
				{
					$zip = $client->zip;
					
					$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($zip)."&sensor=false";
					$result_string = file_get_contents($url);
					$result = json_decode($result_string, true);
					if(count($result['results']) > 0)
					{
						$val = $result['results'][0]['geometry']['location'];
						$user_latitude = $val['lat'];
						$user_longitude = $val['lng'];
						$distance_lat_lon = Helper::DistanceLatLon($latitude, $longitude, $user_latitude, $user_longitude);
						if($miles == '')
						{
							$client_id = $client->id;
							$package_id = $client->package_id;
							$miles = $distance_lat_lon;
						}
						else if($miles > $distance_lat_lon)
						{
							$client_id = $client->id;
							$package_id = $client->package_id;
							$miles = $distance_lat_lon;
						}
					}
					else
					{
						$client_id = $client->id;
						$package_id = $client->package_id;
						break;
					}
				}
				
				DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
				{
					$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
					'user_id' => $user_id,
					'client_id' => $client_id, 
					'package_id' => $package_id,
					'facebook_account_id' => $facebook_account_id,
					'facebook_page_id' => $facebook_page_id,
					'facebook_ads_lead_id' => $facebook_ads_lead_id,
					'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
					'created_at' => date('Y-m-d H:i:s')
					]);
					
					DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
					->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
				});
				
				$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
				foreach($CampaignClients as $CampaignClient)
				{
					if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
					{
						$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
						
						Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
							$message->from('noreply@pownder.com', 'Pownder');
							$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
						});
					}
					
					if($CampaignClient->user_notification == 'No')
					{
						if($CampaignClient->manager_id != '')
						{
							$ManagerArray = explode(",", $CampaignClient->manager_id);
							$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
							foreach($Managers as $Manager)
							{
								$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
								
								Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
									$message->from('noreply@pownder.com', 'Pownder');
									$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
								}); 
							}
						}
					}
					else
					{
						$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
						if(!is_null($ManagerExists))
						{
							if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
							{
								if($CampaignClient->manager_id != '')
								{
									$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
									
									$ManagerArray = explode(",", $CampaignClient->manager_id);
									
									$Array = array();
									$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
									foreach($queries as $query)
									{
										$Array[] = $query->id;
									}
									
									if(count($Array) > 0)
									{
										$index = $LeadCount % count($Array);
										
										$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
										if(!is_null($Manager))
										{
											$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
											
											Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
												$message->from('noreply@pownder.com', 'Pownder');
												$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
											}); 
											
											DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
											->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
										}
									}
								}
							}
						}
					}	
				}
			}
			
			/* 
				DB::table('facebook_ads_lead_users')
				->where(function ($query) {
				$query->whereNull('latitude')
				->orWhere('latitude', '');
				})
				->where(function ($query) {
                $query->whereNull('longitude')
				->orWhere('longitude', '');
				})
				->whereNotIn('facebook_page_id', $PageArray)
				->where('lead_type', 'Pownder™ Messenger')
				->where('facebook_ads_lead_id', 0)
				->where('client_id', 0)
				->where('is_deleted', 0)
				->get(); 
			*/
			
			$facebook_ads_lead_users = DB::table('facebook_ads_lead_users')
			->leftJoin('facebook_pages', 'facebook_ads_lead_users.facebook_page_id', '=', 'facebook_pages.id')
			->select('facebook_ads_lead_users.*')
			->whereNotIn('facebook_ads_lead_users.facebook_page_id', $PageArray)
			->where('facebook_ads_lead_users.lead_type', 'Pownder™ Messenger')
			->where(function ($query) {
				$query->whereNull('facebook_ads_lead_users.latitude')
				->orWhere('facebook_ads_lead_users.latitude', '');
			})
			->where(function ($query) {
                $query->whereNull('facebook_ads_lead_users.longitude')
				->orWhere('facebook_ads_lead_users.longitude', '');
			})
			->where('facebook_ads_lead_users.client_id', 0)
			->where('facebook_ads_lead_users.facebook_ads_lead_id', 0)
			->where('facebook_ads_lead_users.is_deleted', 0)
			->where('facebook_pages.is_deleted', 0)
			->where('facebook_pages.is_blocked', 0)
			->get();
			
			foreach($facebook_ads_lead_users as $lead)
			{					
				$facebook_ads_lead_user_id = $lead->id;
				$facebook_account_id = $lead->facebook_account_id;
				$facebook_page_id = $lead->facebook_page_id;
				$facebook_ads_lead_id = $lead->facebook_ads_lead_id;
				$user_id = $lead->created_user_id;
				
				$leadName = $lead->full_name;
				$leadCity = $lead->city;
				$leadEmail = $lead->email;
				$leadPhone = Helper::emailPhoneFormat($lead->phone_number);
				
				$client = DB::table('clients')->where('status', 1)->where('is_deleted', 0)->inRandomOrder()->first();
				
				if(!is_null($client))
				{
					$client_id = $client->id;
					$package_id = $client->package_id;
					
					DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
					{
						$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
						'user_id' => $user_id,
						'client_id' => $client_id, 
						'package_id' => $package_id,
						'facebook_account_id' => $facebook_account_id,
						'facebook_page_id' => $facebook_page_id,
						'facebook_ads_lead_id' => $facebook_ads_lead_id,
						'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
						'created_at' => date('Y-m-d H:i:s')
						]);
						
						DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
						->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
					});
					
					$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
					foreach($CampaignClients as $CampaignClient)
					{
						if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
						{
							$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
							
							Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
								$message->from('noreply@pownder.com', 'Pownder');
								$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
							});
						}
						
						if($CampaignClient->user_notification == 'No')
						{
							if($CampaignClient->manager_id != '')
							{
								$ManagerArray = explode(",", $CampaignClient->manager_id);
								$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
								foreach($Managers as $Manager)
								{
									$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
									
									Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
										$message->from('noreply@pownder.com', 'Pownder');
										$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
									}); 
								}
							}
						}
						else
						{
							$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
							if(!is_null($ManagerExists))
							{
								if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
								{
									if($CampaignClient->manager_id != '')
									{
										$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
										
										$ManagerArray = explode(",", $CampaignClient->manager_id);
										
										$Array = array();
										$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
										foreach($queries as $query)
										{
											$Array[] = $query->id;
										}
										
										if(count($Array) > 0)
										{
											$index = $LeadCount % count($Array);
											
											$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
											if(!is_null($Manager))
											{
												$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
												
												Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
													$message->from('noreply@pownder.com', 'Pownder');
													$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
												}); 
												
												DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
												->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
											}
										}
									}
								}
							}
						}	
					}
				}
			}
		}	
	}																																					