<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
    use View;
	class ReminderForOverdueInvoices extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:ReminderForOverdueInvoices';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Send Automated Reminder For Overdue Invoices To Clients/Vendors';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
        
        var $now='';        
        const SENDGRID_USER='help@constacloud.in'; //Sendgrid user name to send emails
        const SENDGRID_PASS='main1234';            //Sendgrid password to send emails
        var $INVOICEEMAILID='invoice@pownder.com'; //Default- Send all Emails From this email id
		var $REMINDERSUBJECT='Reminder: Payment is due'; //Default Reminder Invoice email subject
        var $REMINDERMSG='Your payment is due for invoice [invoice_num]'; //Default Invoice email msg
        
		public function __construct()
		{
			parent::__construct();
            $this->setTimeZone('America/Los_Angeles');
            $this->now=$this->getCurrentDateTime();
            $invoice_settings=DB::table('invoice_settings')->first();
            
            if(count($invoice_settings)>0)
            {             
              if($invoice_settings->invoice_email)
              {   
              $this->INVOICEEMAILID=$invoice_settings->invoice_email;
              }
              if($invoice_settings->reminder_subject)
              {
                $this->REMINDERSUBJECT=$invoice_settings->reminder_subject;
              } 
              if($invoice_settings->reminder_msg)
              {
                $this->REMINDERMSG=$invoice_settings->reminder_msg;
              }      
            }
			
		}
		public function setTimeZone($tz)
        {
			return  date_default_timezone_set($tz);
		}
        public function getCurrentDateTime()
        {
            date_default_timezone_set('America/Los_Angeles');
            return date('Y-m-d H:i:s');
		}
        //Money Format
        public function money_format_view($number)
        {
          
          $number=str_replace(",","",$number);
          settype($number,"float"); 
          setlocale(LC_MONETARY, 'en_US'); 
          $f_number=money_format('%!.2i',abs( $number ));     
          $f_number=($number<0) ? '-$'.$f_number : '$'.$f_number;
          return $f_number;
        }
        //Replace Tags/Keywords in String
        public function replaceKeywords($text,$values)
        {
            foreach($values as $key=>$value)
            {
            $text=str_ireplace($key,$value,$text);
            }
            return $text;
        }
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
		  	ini_set('max_execution_time',0);
			ini_set('memory_limit',-1);
            
        //    $already_sending= DB::table('cron_reminder')->first();
//            if(is_null($already_sending))
//            {
//            DB::table('cron_reminder')->insert(['created_at'=>$this->now,'status'=>1]);
//            }
//            else if($already_sending->status==0)
//            {
//                DB::table('cron_reminder')->update(['created_at'=>$this->now,'status'=>1]);
//            }
//            else if($already_sending->status==1)
//            {
//               exit; 
//               //Already running a cron job 
//            }
		  $overdue_invoices=DB::table('invoice As A')->leftJoin('invoice_reminder As B',[['A.invoice_id','=','B.invoice_id'],['A.invoice_num','=','B.invoice_num']])
           ->where([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 3 DAY)'),'=',date('Y-m-d')]])->whereNull('B.auto_reminder_3')
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 3 DAY)'),'=',date('Y-m-d')],['B.auto_reminder_3',0]])          
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 7 DAY)'),'=',date('Y-m-d')],["B.created_user_id",0],["B.created_user_category",'cron'],['B.auto_reminder_7',0]])
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 7 DAY)'),'=',date('Y-m-d')]])->whereNull('B.auto_reminder_3')
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 7 DAY)'),'=',date('Y-m-d')],['B.auto_reminder_3',0]])           
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 14 DAY)'),'=',date('Y-m-d')],["B.created_user_id",0],["B.created_user_category",'cron'],['B.auto_reminder_14',0]])
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 14 DAY)'),'=',date('Y-m-d')]])->whereNull('B.auto_reminder_3')
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 14 DAY)'),'=',date('Y-m-d')],['B.auto_reminder_3',0]])     
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 21 DAY)'),'=',date('Y-m-d')],["B.created_user_id",0],["B.created_user_category",'cron'],['B.auto_reminder_21',0]])
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 21 DAY)'),'=',date('Y-m-d')]])->whereNull('B.auto_reminder_3')
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 21 DAY)'),'=',date('Y-m-d')],['B.auto_reminder_3',0]])          
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 30 DAY)'),'=',date('Y-m-d')],["B.created_user_id",0],["B.created_user_category",'cron'],['B.auto_reminder_30',0]])          
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 30 DAY)'),'=',date('Y-m-d')]])->whereNull('B.auto_reminder_3')          
           ->orWhere([['A.is_deleted',0],['A.status',1],[DB::raw('DATE_ADD(A.due_date,INTERVAL 30 DAY)'),'=',date('Y-m-d')],['B.auto_reminder_3',0]])
           ->select('A.*','B.id As reminder_id','B.auto_reminder_3','B.auto_reminder_7','B.auto_reminder_14','B.auto_reminder_21','B.auto_reminder_30')->get();
        
           foreach($overdue_invoices as $invoice)
           {
              
            if($invoice->client_type=="vendor")
            {
                $colname="vendor_id";
                $table="vendors";
            }
            else
            {
                $colname="client_id";
                $table="clients";
            }            
             
            $email=DB::table($table)->where([['id',$invoice->client_id],["is_deleted",0]])->select('invoice_email','name')->first();
               
            if(!is_null($email))
            {
            $pdffilename=$invoice->pdffilename;
            $invoice_id=$invoice->invoice_id;
            $invoice_num=$invoice->invoice_num;
            $client_name=$email->name;
             
            $due_diff=date_diff(date_create($invoice->due_date),date_create(date('Y-m-d')));
            $due_in_days=$due_diff->format('%a');
                        
            /** First check if manual reminder sent today or yesterday */
           $check_manually_send=DB::table('invoice_reminder')->where([["invoice_id",$invoice_id],["invoice_num",$invoice_num],["created_user_id",'!=',0],["created_user_category",'!=','cron'],[DB::raw('DATE(created_at)'),'>=',date('Y-m-d',strtotime('yesterday'))]])->count('id');   
           
            if($check_manually_send)
            {
            if(is_null($invoice->auto_reminder_3) || $invoice->auto_reminder_3==0)
            {    
            $check_already_inserted=DB::table('invoice_reminder')->where([["invoice_id",$invoice_id],["invoice_num",$invoice_num],["created_user_id",0],["created_user_category",'cron']])->count('id');   
           
            if(!$check_already_inserted)
            {               
            DB::table('invoice_reminder')->insert(["invoice_id"=>$invoice_id,"invoice_num"=>$invoice_num /*, "client_name"=>$client_name,"subject"=>$subject,"msg"=>$reminder_msg */ ,"created_user_id"=>0,"created_user_category"=>'cron',"created_at"=>$this->now,"updated_at"=>date('Y-m-d H:i:s',strtotime('-4 days'))]);
		    }
            }
            }
            else
            {
            $reminder_col='';
            
            if($due_in_days==3)
            {
                $reminder_col='auto_reminder_3';
            }
            else if($due_in_days==7)
            {
                $reminder_col='auto_reminder_7';
            }
            else if($due_in_days==14)
            {
                $reminder_col='auto_reminder_14';
            }
            else if($due_in_days==21)
            {
                $reminder_col='auto_reminder_21';
            }
            else if($due_in_days==30)
            {
                $reminder_col='auto_reminder_30';
            }
            $check_already_send=DB::table('invoice_reminder')->where([["invoice_id",$invoice_id],["invoice_num",$invoice_num],["created_user_id",0],["created_user_category",'cron'],[$reminder_col,1]])->count('id');   
            if($check_already_send)
            {
               exit; 
            }   
            
            if($reminder_col)
            {
            /** Calculate Due By Days */
            $due_days=0;
            $date1=date_create(date('Y-m-d'));
            $date2=date_create($invoice->due_date);    
            $diff=date_diff($date1,$date2); 
            $due_days=$diff->format('%a');
            /** Calculate Due By Days */
                                
            //email to client
            
            $key_values=['[name]'=>$email->name,'[invoice_num]'=>$invoice_num,'[due_date]'=>date('m/d/Y',strtotime($invoice->due_date)),'[Amount]'=>$this->money_format_view($invoice->balance),'[Company_Name]'=>$invoice->company_name,'[Company_Address]'=>nl2br($invoice->company_address),'[Due_Days]'=>$due_days];
                        
           $reminder_msg=$this->replaceKeywords($this->REMINDERMSG,$key_values);            
    
            $from=$this->INVOICEEMAILID;
       //     $subject='Reminder:'.$request->send_reminder_subject; 
            $subject=$this->replaceKeywords($this->REMINDERSUBJECT,$key_values);            
            $msg=View::make('invoice_payment.reminder_email_template',['company_name'=>$invoice->company_name,"email"=>$email,'invoice_num'=>$invoice_num,"msg"=>$reminder_msg])->render();//$request->send_reminder_msg
            
            $filepath=file_get_contents(__DIR__.'/../../../storage/app/uploads/invoice/invoice_pdf/'.$pdffilename,true); 
            $urls = 'https://api.sendgrid.com/';
            $users = self::SENDGRID_USER;
            $passs = self::SENDGRID_PASS;
            $paramss = array(
            'api_user' => $users,
            'api_key' => $passs,            
            'subject' => $subject,
            'html' => $msg,
            'from' => $from,
            'fromname' => "Pownder�",
            'files['.$pdffilename.']'=>'@'.$filepath.'/'.$pdffilename
            );
            $paramss=http_build_query($paramss);
            $emails=explode(',',$invoice->client_email);//     priya.constacloud2@gmail.com
            foreach($emails as $email)
            {
              $paramss.='&to[]='.$email;  
            }
            $emails=explode(',',$invoice->cc_email);//     priya.constacloud2@gmail.com
            foreach($emails as $email)
            {
              $paramss.='&cc[]='.$email; 
            }
            $requests = $urls.'api/mail.send.json';
            $sessions = curl_init($requests);
            curl_setopt ($sessions, CURLOPT_POST, true);
            curl_setopt ($sessions, CURLOPT_POSTFIELDS, $paramss);
            curl_setopt($sessions, CURLOPT_HEADER, false);
            curl_setopt($sessions, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($sessions, CURLOPT_RETURNTRANSFER, true);
            $responses = curl_exec($sessions);
            $info=curl_getinfo($sessions);
            curl_close($sessions);
            
            //Save Reminder Log
             if(is_null($invoice->auto_reminder_3) || $invoice->auto_reminder_3==0)
            {
            DB::table('invoice_reminder')->insert(["invoice_id"=>$invoice_id,"invoice_num"=>$invoice_num,"client_name"=>$client_name/*,"subject"=>$subject,"msg"=>$reminder_msg*/,"created_user_id"=>0,"created_user_category"=>'cron',"created_at"=>$this->now,"updated_at"=>$this->now,$reminder_col=>1]);
		    }
            else 
            {
            DB::table('invoice_reminder')->where("id",$invoice->reminder_id)->update([$reminder_col=>1,"updated_at"=>$this->now]);
		    }
            
            }	
             
            }
            
            }
           }
           
           //Close cron job
         //  DB::table('cron_reminder')->update(['updated_at'=>$this->now,'status'=>0,'remark'=>count($overdue_invoices).' reminders sent']);
            
		}
	}
