<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	
	use Mail;
	use DB;
	
	class SendAppointmentEmail extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:SendAppointmentEmail';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Send Appointment Email';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$admin = DB::table('admins')->where('id', 1)->where('appointment_email', '!=', '')->where('appointment_option', 'Yes')->where('is_deleted', 0)->first();
			if(is_null($admin))
			{
				DB::table('admin_emails')->where('subject', 'Facebook Lead Appointment')->where('is_deleted', 0)
				->update(['updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
				DB::table('admin_emails')->where('subject', 'Facebook Reschedule Appointment')->where('is_deleted', 0)
				->update(['updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			}
			else
			{
				$data = array();
				$admin_emails = DB::table('admin_emails')->where('subject', 'Facebook Lead Appointment')->where('is_deleted', 0)->get();
				foreach($admin_emails as $admin_email)
				{
					$record = array('name' => $admin->name, 'appointment_date' => $admin_email->appointment_date, 'appointment_time' => $admin_email->appointment_time, 'category' => 'admin', 'manager_name' => $admin_email->manager_name, 'client_name' => $admin_email->client_name, 'leadType' => $admin_email->leadType, 'leadAddedBy' => $admin_email->leadAddedBy, 'leadName' => $admin_email->leadName, 'leadEmail' => $admin_email->leadEmail, 'leadPhone' => $admin_email->leadPhone, 'leadCity' => $admin_email->leadCity); 
					
					$data[] = $record;
				}
				
				Mail::send('emails.appointment_cron', ["data" => $data], function ($message) use ($admin) {
					$message->from('noreply@pownder.com', 'Pownder');
					$message->to($admin->appointment_email)->subject('Facebook Lead Appointment');
				});
				
				DB::table('admin_emails')->where('subject', 'Facebook Lead Appointment')->where('is_deleted', 0)
				->update(['name' => $admin->name, 'category' => 'admin', 'email' => $admin->appointment_email, 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]); 
				
				$data = array();
				$admin_emails = DB::table('admin_emails')->where('subject', 'Facebook Reschedule Appointment')->where('is_deleted', 0)->get();
				foreach($admin_emails as $admin_email)
				{
					$record = array('name' => $admin->name, 'appointment_date' => $admin_email->appointment_date, 'appointment_time' => $admin_email->appointment_time, 'category' => 'admin', 'manager_name' => $admin_email->manager_name, 'client_name' => $admin_email->client_name, 'leadType' => $admin_email->leadType, 'leadAddedBy' => $admin_email->leadAddedBy, 'leadName' => $admin_email->leadName, 'leadEmail' => $admin_email->leadEmail, 'leadPhone' => $admin_email->leadPhone, 'leadCity' => $admin_email->leadCity); 
					
					$data[] = $record;
				}
				
				Mail::send('emails.appointment_cron', ["data" => $data], function ($message) use ($admin) {
					$message->from('noreply@pownder.com', 'Pownder');
					$message->to($admin->appointment_email)->subject('Facebook Reschedule Appointment');
				});
				
				DB::table('admin_emails')->where('subject', 'Facebook Reschedule Appointment')->where('is_deleted', 0)
				->update(['name' => $admin->name, 'category' => 'admin', 'email' => $admin->appointment_email, 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
			}
		}	
	}
