<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
	use Helper;
	use ConsoleHelper;
	
	class CreateContact extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:CreateContact';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Create Contact';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$BusinessCRM = array('Agile', 'Zoho');
			
			$Campaigns = DB::table('campaigns')->whereIn('crm_account', $BusinessCRM)->where('is_active', 0)->where('is_deleted', 0)->orderBy('updated_at', 'DESC')->get();
			foreach($Campaigns as $Campaign)
			{
				$campaign_id = $Campaign->id;
				$client_id = $Campaign->client_id;
				$LeadType = explode(",", $Campaign->manual_lead_type);
				$LeadForm = explode(",", $Campaign->facebook_ads_lead_id);
				$lead_sync = $Campaign->lead_sync;
				$active_at = $Campaign->active_at;
				
				$CRMAccounts = DB::table('crm_accounts')->where('client_id', $client_id)->where('campaign_id', $campaign_id)->where('is_deleted',0)->get();
				foreach($CRMAccounts as $CRMAccount)
				{
					$crm_account_id = $CRMAccount->id;
					$crm_account = $CRMAccount->account;
					$crm_domain = $CRMAccount->domain;
					$crm_username = $CRMAccount->username;
					$crm_password = $CRMAccount->password;
					$AccessToken = $CRMAccount->access_token;
					
					if($crm_account == 'Zoho')
					{
						$TokenStatus = ConsoleHelper::ZohoTokenExpiryCheck($AccessToken);
						if($TokenStatus == 'expire')
						{
							$AccessToken = ConsoleHelper::ZohoOauthToken($crm_username, $crm_password);
							if($AccessToken != '')
							{
								DB::table('crm_accounts')->where('id', $crm_account_id)
								->update(['access_token' => $AccessToken]);
							}
							else
							{
								continue;
							}
						}
					}
					
					$facebookAdsLeadUsers = DB::table('facebook_ads_lead_users')
					->where(function ($query) use ($LeadForm, $LeadType){
						$query->whereIn('facebook_ads_lead_id', $LeadForm)
						->whereIn('lead_type', $LeadType, 'or');
					})
					->where(function ($query) use ($lead_sync, $active_at){
						if($lead_sync == 'Only New')
						{
							$query->whereDate('created_time', '>=', $active_at);
						}
					})
					->whereNull('messenger_lead_id')
					->where('client_id', $client_id)
					->where('is_deleted', 0)
					->where('crm_contact_id', 0)
					->get();
					
					foreach($facebookAdsLeadUsers as $facebookAdsLeadUser)
					{
						$facebook_ads_lead_user_id = $facebookAdsLeadUser->id;
						$lead_type = $facebookAdsLeadUser->lead_type;
						$facebook_ads_lead_id = $facebookAdsLeadUser->facebook_ads_lead_id;
						$facebook_page_id = $facebookAdsLeadUser->facebook_page_id;
						$facebook_account_id = $facebookAdsLeadUser->facebook_account_id;
						
						$first_name = $facebookAdsLeadUser->first_name;
						if($first_name == '')
						{
							$first_name = $facebookAdsLeadUser->full_name;
						}
						
						$last_name = $facebookAdsLeadUser->last_name;
						$email = $facebookAdsLeadUser->email;
						$phone_number = $facebookAdsLeadUser->phone_number;
						$street_address = $facebookAdsLeadUser->street_address;
						$city = $facebookAdsLeadUser->city;
						$state = $facebookAdsLeadUser->state;
						$province = $facebookAdsLeadUser->province;
						$country = $facebookAdsLeadUser->country;
						$post_code = $facebookAdsLeadUser->post_code;
						$zip_code = $facebookAdsLeadUser->zip_code;
						$date_of_birth = $facebookAdsLeadUser->date_of_birth;
						$gender = $facebookAdsLeadUser->gender;
						$relationship_status = $facebookAdsLeadUser->relationship_status;
						$company_name = $facebookAdsLeadUser->company_name;
						$military_status = $facebookAdsLeadUser->military_status;
						$job_title = $facebookAdsLeadUser->job_title;
						$work_phone_number = $facebookAdsLeadUser->work_phone_number;
						$work_email = $facebookAdsLeadUser->work_email;
						$numero_telefonico = $facebookAdsLeadUser->numero_telefonico;
						$marital_status = $facebookAdsLeadUser->marital_status;
						$created_time = $facebookAdsLeadUser->created_time;
						
						if($crm_account == 'Agile')
						{
							$response = ConsoleHelper::AgileCreateContact($street_address, $city, $state, $country, $email, '', '', $lead_type, $first_name, $last_name, '', $phone_number, '', '', $crm_domain, $crm_username, $crm_password);
							
							if($response == 200)
							{
								$crm_contact_id = DB::table('crm_contacts')->insertGetId([
								'facebook_account_id' => $facebook_account_id,
								'facebook_page_id' => $facebook_page_id,
								'facebook_ads_lead_id' => $facebook_ads_lead_id,
								'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
								'campaign_id' => $campaign_id,
								'client_id' => $client_id,
								'crm_account_id' => $crm_account_id,
								'crm_account' => $crm_account,
								'crm_domain' => $crm_domain,
								'crm_username' => $crm_username,
								'crm_password' => $crm_password,
								'created_at' => date('Y-m-d H:i:s'),
								'updated_at' => date('Y-m-d H:i:s')
								]);
								
								DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
								->update(['crm_contact_id' => $crm_contact_id]);
								
								DB::table('resync_error')->where('facebook_ads_lead_user_id', $facebook_ads_lead_user_id)->where('is_deleted', 0)
								->update(['updated_user_id' => 0, 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
							}
						}	
						
						if($crm_account == 'Zoho')
						{
							if($AccessToken != '')
							{
								$response = ConsoleHelper::ZohoCreateContact($first_name,$last_name,$email,'',$phone_number,'','','',$AccessToken);
								if (strpos($response, 'added successfully') !== false) 
								{
									$crm_contact_id = DB::table('crm_contacts')->insertGetId([
									'facebook_account_id' => $facebook_account_id,
									'facebook_page_id' => $facebook_page_id,
									'facebook_ads_lead_id' => $facebook_ads_lead_id,
									'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
									'campaign_id' => $campaign_id,
									'client_id' => $client_id,
									'crm_account_id' => $crm_account_id,
									'crm_account' => $crm_account,
									'crm_domain' => $crm_domain,
									'crm_username' => $crm_username,
									'crm_password' => $crm_password,
									'created_at' => date('Y-m-d H:i:s'),
									'updated_at' => date('Y-m-d H:i:s')
									]);
									
									DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
									->update(['crm_contact_id' => $crm_contact_id]);
									
									DB::table('resync_error')->where('facebook_ads_lead_user_id', $facebook_ads_lead_user_id)->where('is_deleted', 0)
									->update(['updated_user_id' => 0, 'updated_at' => date('Y-m-d H:i:s'), 'is_deleted' => 1]);
								}
							}
						}
					}					
				}
			}
		}
	}
