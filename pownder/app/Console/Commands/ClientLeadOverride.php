<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	
	use DB;
	use Helper;
	use Mail;
	
	class ClientLeadOverride extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:ClientLeadOverride';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Client Lead Override';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			//------(Remaining Day Calculate)------
			$total_date = date("t");
			$today_date = date("j");
			$remaining_limit_day = $total_date - $today_date + 1;
			//-------------------------------------
			
			$start_client_id = 0;
			
			$last_records = DB::table('client_ads_lead_users')->select('user_id', 'client_id')->orderBy('id', 'DESC')->take(1)->get();
			if(count($last_records)>0)
			{
				foreach($last_records as $last_record)
				{
					$start_client_id = $last_record->client_id;
				}
			}
			
			$FacebookAdsLeads = DB::table('facebook_ads_leads')->where('is_blocked', 0)->where('is_deleted', 0)->inRandomOrder()->get();
			foreach($FacebookAdsLeads as $FacebookAdsLead)
			{
				//echo "Pass1";die;
				$ClientArray = array();
				$facebook_ads_lead_id = $FacebookAdsLead->id;
				$user_id = $FacebookAdsLead->updated_user_id;
				$TotalFacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->where('facebook_ads_lead_id', $facebook_ads_lead_id)->where('client_id', 0)->where('is_deleted', 0)->get();
				if(count($TotalFacebookAdsLeadUsers)>0)
				{
					//echo "Pass2";die;
					$LeadFormClients = DB::table('clients')->select('id')->where('facebook_ads_lead_id', 'like', '%'.$facebook_ads_lead_id.'%')->where('status', 1)->where('is_deleted', 0)->get();
					foreach($LeadFormClients as $LeadFormClient)
					{
						$ClientArray[] = $LeadFormClient->id;
					}
					
					$GroupClients = DB::table('client_groups')
					->leftJoin('groups', 'client_groups.group_id', '=', 'groups.id')
					->leftJoin('clients', 'client_groups.client_id', '=', 'clients.id')
					->select('client_groups.client_id')
					->where('groups.facebook_ads_lead_id', 'like', '%'.$facebook_ads_lead_id.'%')
					->where('clients.status', 1)
					//->where('groups.created_user_id', $user_id)
					//->where('clients.created_user_id', $user_id)
					//->where('client_groups.created_user_id', $user_id)
					->where('groups.is_deleted', 0)
					->where('clients.is_deleted', 0)
					->where('client_groups.is_deleted', 0)
					->groupBy('client_groups.client_id')
					->get();
					
					foreach($GroupClients as $GroupClient)
					{
						$ClientArray[] = $GroupClient->client_id;
					}
					
					$ClientArray = array_unique($ClientArray);
					if(count($ClientArray) > 0)
					{
						//echo "Pass3";die;
						$ClientCounter = 0;
						$Clients = DB::table('clients')
						->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
						->select('clients.id', 'clients.package_id', 'packages.monthly_lead_quantity', 'packages.package_type')
						->whereIn('clients.id', $ClientArray)
						->where('clients.status', 1)
						//->where('clients.created_user_id', $user_id)
						//->where('packages.created_user_id', $user_id)
						->where('clients.is_deleted', 0)
						->where('packages.is_deleted', 0)
						->orderBy('clients.id', 'ASC')
						->get();
						foreach($Clients as $Client)
						{
							//echo "Pass4";die;
							$client_id = $Client->id;
							$package_id = $Client->package_id;
							$package_type = $Client->package_type;
							$monthly_lead_quantity = $Client->monthly_lead_quantity;
							
							if($monthly_lead_quantity == '')
							{
								$ClientCounter++;
							}
							else
							{
								//echo "Pass5";die;
								//------(Remaining Total Lead Calculate)------
								$assign_lead_quantity = DB::table('client_ads_lead_users')->select('id')->where('client_id', $client_id)->whereYear('created_at', '=', date('Y'))->whereMonth('created_at', '=', date('m'))->whereDate('created_at', '!=', date('Y-m-d'))->where('is_deleted', 0)->count();
								$remaining_lead_quantity = $monthly_lead_quantity - $assign_lead_quantity;
								//--------------------------------------------
								
								if($remaining_lead_quantity > 0)
								{
									//------(Remaining Today Lead Calculate)------
									if($remaining_limit_day == 1)
									{
										$today_limit = $remaining_lead_quantity;
									}
									else
									{
										$today_limit = (integer)($remaining_lead_quantity/$remaining_limit_day);
									}
									
									$today_assign_lead_quantity = DB::table('client_ads_lead_users')->select('id')->where('client_id', $client_id)->whereDate('created_at','=', date('Y-m-d'))->where('is_deleted', 0)->count();
									
									$remaining_today_limit = $today_limit - $today_assign_lead_quantity;
									//--------------------------------------------
									
									if($remaining_today_limit <= 0)
									{
										//echo "Pass6";die;
										$ClientCounter++;
									}
								}
								else
								{
									//echo "Pass7";die;
									$ClientCounter++;
								}
							}
						}
						
						if(count($Clients) == $ClientCounter)
						{
							//echo "Pass8";die;
							$loop  = 0;
							
							while ($loop <= 0)
							{
								$clients = DB::table('clients')
								->whereIn('id', $ClientArray)
								->where('is_override', 'Yes')
								->where('status', 1)
								//->where('created_user_id', $user_id)
								->where(function ($query) use($start_client_id){
									if($start_client_id>0)
									{
										$query->where('id', '>', $start_client_id);
									}
								})
								->where('is_deleted', 0)
								->orderBy('id', 'ASC')
								->get();
								
								if(count($clients)>0)
								{
									//echo "Pass9";die;
									foreach($clients as $client)
									{
										$client_id = $client->id;
										$package_id = $client->package_id;
										
										$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->where('facebook_ads_lead_id', $facebook_ads_lead_id)->where('client_id', 0)->where('is_deleted', 0)->take(1)->get();
										if(count($FacebookAdsLeadUsers)>0)
										{
											//echo "Pass 10";die;
											foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
											{
												$facebook_ads_lead_user_id=$FacebookAdsLeadUser->id;
												$facebook_account_id=$FacebookAdsLeadUser->facebook_account_id;
												$facebook_page_id=$FacebookAdsLeadUser->facebook_page_id;
												$leadName = $FacebookAdsLeadUser->full_name;
												$leadCity = $FacebookAdsLeadUser->city;
												$leadEmail = $FacebookAdsLeadUser->email;
												$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
												
												DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
												{
													$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
													'user_id' => $user_id,
													'client_id' => $client_id,
													'package_id' => $package_id,
													'facebook_account_id' => $facebook_account_id,
													'facebook_page_id' => $facebook_page_id,
													'facebook_ads_lead_id' => $facebook_ads_lead_id,
													'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
													'created_at' => date('Y-m-d H:i:s')
													]);
													
													DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
													->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
												});
												
												$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
												foreach($CampaignClients as $CampaignClient)
												{
													if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
													{
														$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
														
														Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
															$message->from('noreply@pownder.com', 'Pownder');
															$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
														});
													}
													
													if($CampaignClient->user_notification == 'No')
													{
														if($CampaignClient->manager_id != '')
														{
															$ManagerArray = explode(",", $CampaignClient->manager_id);
															$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
															foreach($Managers as $Manager)
															{
																$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																
																Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																	$message->from('noreply@pownder.com', 'Pownder');
																	$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																}); 
															}
														}
													}
													else
													{
														$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
														if(!is_null($ManagerExists))
														{
															if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
															{
																if($CampaignClient->manager_id != '')
																{
																	$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
																	
																	$ManagerArray = explode(",", $CampaignClient->manager_id);
																	
																	$Array = array();
																	$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																	foreach($queries as $query)
																	{
																		$Array[] = $query->id;
																	}
																	
																	if(count($Array) > 0)
																	{
																		$index = $LeadCount % count($Array);
																		
																		$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																		if(!is_null($Manager))
																		{
																			$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																			
																			Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																				$message->from('noreply@pownder.com', 'Pownder');
																				$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																			}); 
																			
																			DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																			->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																		}
																	}
																}
															}
														}
													}	
												}
											}
										}
										else
										{
											$loop++;
										}
									}
								}
								else
								{
									if($start_client_id == 0)
									{
										$loop++;
									}
								}
								
								$start_client_id = 0;
							}
						}
					}
				}
			}
		}	
	}																														