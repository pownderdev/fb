<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
	class SendAutoNotifications extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:SendAutoNotifications';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Notify Admin';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
        
        var $now='';        
        const SENDGRID_USER='help@constacloud.in'; //Sendgrid user name to send emails
        const SENDGRID_PASS='main1234';            //Sendgrid password to send emails
        var $INVOICEEMAILID='invoice@pownder.com'; //Default- Send all Emails From this email id
	 
		public function __construct()
		{
			parent::__construct();
            $this->setTimeZone('America/Los_Angeles');
            $this->now=date('Y-m-d H:i:s');
            $invoice_settings=DB::table('invoice_settings')->first();
            
            if(count($invoice_settings)>0)
            {             
              if($invoice_settings->invoice_email)
              {   
              $this->INVOICEEMAILID=$invoice_settings->invoice_email;
              }                 
            }
			
		}
		public function setTimeZone($tz)
        {
			return  date_default_timezone_set($tz);
		}
      
       
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
		  	ini_set('max_execution_time',0);
			ini_set('memory_limit',-1);
            
            $admin_email=new \stdClass();
            
            $admin_email_data=DB::table('admins')->where([['is_deleted',0],['status',1]])->select('id','invoice_email','receive_notification','invoice_option')->first();
            if(is_null($admin_email_data))
            {
                $admin_email->email=''; // Admin Deleted Or Inactive
            }
            else if($admin_email_data->invoice_option=='No')
            {
                $admin_email->email=''; // Admin Don't want notification
                DB::table('admin_notifications')->where([['for','admin'],['for_person_id',$admin_email_data->id],['is_deleted', 0],['is_sent', 0]])->update(['is_deleted'=>1,'updated_at'=>$this->now]);
            }            
            else
            {
                
                //Send Notifications
                $notifications=DB::table('admin_notifications')->where([['for','admin'],['for_person_id',$admin_email_data->id],['is_deleted', 0],['is_sent', 0]])->get();
                         
                foreach($notifications as $notification)
                {
                $filename=$filepath=$filepath1='';    
                if($notification->attachment_path && $notification->attachment_file)
                {
                $filename=$notification->attachment_file;
                $filepath1=$notification->attachment_path;    
                $filepath=file_get_contents(__DIR__.'/../../../'.$filepath1.$filename,true); 
                }
                $urls = 'https://api.sendgrid.com/';
                $users = self::SENDGRID_USER;
                $passs = self::SENDGRID_PASS;
                
                $subject=$notification->subject;
                $msg=$notification->email_body;
                $from=$this->INVOICEEMAILID;
                $fromname=$notification->fromname;
                
                $paramss = array(
                'api_user' => $users,
                'api_key' => $passs,            
                'subject' => $subject,
                'html' => $msg,
                'from' => $from,
                'fromname' => $fromname
                );
                
                if($filename && $filepath && $filepath1)
                {
                    $paramss['files['.$filename.']']='@'.$filepath.'/'.$filename;
                }
                $paramss=http_build_query($paramss);
                
                foreach(explode(',',$notification->to) as $key=>$toemail)
                {
                    $paramss.='&to[]='.$toemail;                    
                }
                if($notification->cc)
                {
                foreach(explode(',',$notification->cc) as $key=>$ccemail)
                {
                    $paramss.='&cc[]='.$ccemail;                    
                }
                }
                $requests = $urls.'api/mail.send.json';
                $sessions = curl_init($requests);
                curl_setopt ($sessions, CURLOPT_POST, true);
                curl_setopt ($sessions, CURLOPT_POSTFIELDS, $paramss);
                curl_setopt($sessions, CURLOPT_HEADER, false);
                curl_setopt($sessions, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                curl_setopt($sessions, CURLOPT_RETURNTRANSFER, true);
                $responses = curl_exec($sessions);
                $http_code=curl_getinfo($sessions,CURLINFO_HTTP_CODE);
                curl_close($sessions);  
                if($http_code==200)
                {
                   DB::table('admin_notifications')->where('id',$notification->id)->update(['is_sent'=>1,'updated_at'=>$this->now]);             
                }
              
                }
            }  
		}
	}
