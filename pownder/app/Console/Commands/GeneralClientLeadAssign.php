<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	
	use DB;
	use Helper;
	use Mail;
	
	class GeneralClientLeadAssign extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:GeneralClientLeadAssign';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'General Client Lead Assign';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			DB::table('clients')->where('status', 1)->where('is_deleted', 0)
			->update(['start_lead_assign' => 0]);
			
			$start_user_id = 0;
			$start_client_id = 0;
			
			$last_records = DB::table('client_ads_lead_users')->select('user_id', 'client_id')->orderBy('id', 'DESC')->take(1)->get();
			if(count($last_records)>0)
			{
				foreach($last_records as $last_record)
				{
					$start_user_id = $last_record->user_id;
					$start_client_id = $last_record->client_id;
				}
			}
			
			$loop  = 0;
			
			while ($loop <= 0)
			{
				$TotalClients = DB::table('clients')->select('id')->where('status', 1)->where('is_deleted', 0)->where('start_lead_assign', 0)->get();
				
				if(count($TotalClients)>0)
				{
					//echo "Pass 1";die;
					$TotalFacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', 0)->where('is_deleted', 0)->get();
					
					if(count($TotalFacebookAdsLeadUsers)>0)
					{
						$Users = DB::table('users')
						->select('id', 'vendor_id')
						->where(function ($query) use($start_user_id){
							if($start_user_id>0)
							{
								$query->where('id', $start_user_id);
							}
						})
						->where(function ($query) {
							$query->where('category', 'admin')
							->orWhere('category', 'vendor');
						})
						->get();
						
						$start_user_id = 0;
						
						foreach($Users as $User)
						{
							$user_id = $User->id;
							$vendor_id = $User->vendor_id;
							
							//echo "Pass 2";die;
							$Clients = DB::table('clients')
							->leftJoin('packages', 'clients.package_id', '=', 'packages.id')
							->select('clients.id', 'clients.facebook_ads_lead_id as client_lead_form', 'clients.package_id', 'clients.zip', 'clients.latitude', 'clients.longitude', 'packages.monthly_lead_quantity', 'packages.package_type')
							->where(function ($query) use($start_client_id){
								if($start_client_id>0)
								{
									$query->where('clients.id', '>', $start_client_id);
								}
							})
							->where('clients.status', 1)
							->where('clients.is_deleted', 0)
							->where('clients.vendor_id', $vendor_id)
							->where('clients.start_lead_assign', 0)
							->orderBy('clients.id', 'ASC')
							->get();
							
							$start_client_id = 0;
							
							foreach($Clients as $Client)
							{
								//echo "Pass 3";die;
								$client_id = $Client->id;
								$client_latitude = (float)$Client->latitude;
								$client_longitude = (float)$Client->longitude;
								$client_lead_form = $Client->client_lead_form;
								$package_id = $Client->package_id;
								$package_type = $Client->package_type;
								$monthly_lead_quantity = $Client->monthly_lead_quantity;
								
								if($package_type == 'Monthly')
								{
									//------(Remaining Total Lead Calculate)------
									$assign_lead_quantity = DB::table('client_ads_lead_users')->select('id')->where('client_id', $client_id)->whereYear('created_at', '=', date('Y'))->whereMonth('created_at', '=', date('m'))->whereDate('created_at', '!=', date('Y-m-d'))->where('is_deleted', 0)->count();
									$remaining_lead_quantity = $monthly_lead_quantity-$assign_lead_quantity;
									//--------------------------------------------
									
									if($remaining_lead_quantity > 0)
									{
										//echo "Pass 4";die;
										if($client_lead_form != '')
										{
											//echo "Pass 8";die;
											$LeadForm = explode(',',$client_lead_form);
											
											$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->whereIn('facebook_ads_lead_id', $LeadForm)->where('city', '!=', '')->where('client_id', 0)->where('is_deleted', 0)->get();
											if(count($FacebookAdsLeadUsers)>0)
											{
												//echo "Pass 9";die;
												$facebook_ads_lead_user_id = 0;
												$facebook_account_id = 0;
												$facebook_page_id = 0;
												$facebook_ads_lead_id = 0;
												$leadName = '';
												$leadCity = '';
												$leadEmail = '';
												$leadPhone = '';
												$miles = '';
												foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
												{
													
													$city = $FacebookAdsLeadUser->city;
													
													$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($city)."&sensor=false";
													$result_string = file_get_contents($url);
													$result = json_decode($result_string, true);
													if(count($result['results'])>0)
													{
														$val = $result['results'][0]['geometry']['location'];
														$user_latitude = $val['lat'];
														$user_longitude = $val['lng'];
														$distance_lat_lon = Helper::DistanceLatLon($client_latitude, $client_longitude, $user_latitude, $user_longitude);
														if($miles == '')
														{
															$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
															$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
															$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
															$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
															$leadName = $FacebookAdsLeadUser->full_name;
															$leadCity = $FacebookAdsLeadUser->city;
															$leadEmail = $FacebookAdsLeadUser->email;
															$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
															$miles = $distance_lat_lon;
														}
														else if($miles > $distance_lat_lon)
														{
															$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
															$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
															$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
															$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
															$leadName = $FacebookAdsLeadUser->full_name;
															$leadCity = $FacebookAdsLeadUser->city;
															$leadEmail = $FacebookAdsLeadUser->email;
															$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
															$miles = $distance_lat_lon;
														}
													}
													else
													{
														$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
														$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
														$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
														$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
														$leadName = $FacebookAdsLeadUser->full_name;
														$leadCity = $FacebookAdsLeadUser->city;
														$leadEmail = $FacebookAdsLeadUser->email;
														$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
														break;
													}
												}
												
												DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
												{
													$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
													'user_id' => $user_id,
													'client_id' => $client_id, 
													'package_id' => $package_id,
													'facebook_account_id' => $facebook_account_id,
													'facebook_page_id' => $facebook_page_id,
													'facebook_ads_lead_id' => $facebook_ads_lead_id,
													'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
													'created_at' => date('Y-m-d H:i:s')
													]);
													
													DB::table('facebook_ads_lead_users')
													->where('id', $facebook_ads_lead_user_id)
													->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
												});
												
												$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
												foreach($CampaignClients as $CampaignClient)
												{
													if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
													{
														$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
														
														Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
															$message->from('noreply@pownder.com', 'Pownder');
															$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
														});
													}
													
													if($CampaignClient->user_notification == 'No')
													{
														if($CampaignClient->manager_id != '')
														{
															$ManagerArray = explode(",", $CampaignClient->manager_id);
															$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
															foreach($Managers as $Manager)
															{
																$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																
																Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																	$message->from('noreply@pownder.com', 'Pownder');
																	$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																}); 
															}
														}
													}
													else
													{
														$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
														if(!is_null($ManagerExists))
														{
															if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
															{
																if($CampaignClient->manager_id != '')
																{
																	$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
																	
																	$ManagerArray = explode(",", $CampaignClient->manager_id);
																	
																	$Array = array();
																	$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																	foreach($queries as $query)
																	{
																		$Array[] = $query->id;
																	}
																	
																	if(count($Array) > 0)
																	{
																		$index = $LeadCount % count($Array);
																		
																		$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																		if(!is_null($Manager))
																		{
																			$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																			
																			Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																				$message->from('noreply@pownder.com', 'Pownder');
																				$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																			}); 
																			
																			DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																			->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																		}
																	}
																}
															}
														}
													}	
												}
											}
											else
											{
												$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->whereIn('facebook_ads_lead_id', $LeadForm)->where('city', '')->where('client_id', 0)->where('is_deleted', 0)->take(1)->get();
												if(count($FacebookAdsLeadUsers) > 0)
												{
													//echo "Pass 9";die;
													foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
													{
														$facebook_ads_lead_user_id = $FacebookAdsLeadUser->id;
														$facebook_account_id = $FacebookAdsLeadUser->facebook_account_id;
														$facebook_page_id = $FacebookAdsLeadUser->facebook_page_id;
														$facebook_ads_lead_id = $FacebookAdsLeadUser->facebook_ads_lead_id;
														$leadName = $FacebookAdsLeadUser->full_name;
														$leadCity = $FacebookAdsLeadUser->city;
														$leadEmail = $FacebookAdsLeadUser->email;
														$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
														
														DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
														{
															$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
															'user_id' => $user_id,
															'client_id' => $client_id,
															'package_id' => $package_id,
															'facebook_account_id' => $facebook_account_id,
															'facebook_page_id' => $facebook_page_id,
															'facebook_ads_lead_id' => $facebook_ads_lead_id,
															'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
															'created_at' => date('Y-m-d H:i:s')
															]);
															
															DB::table('facebook_ads_lead_users')
															->where('id', $facebook_ads_lead_user_id)
															->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
														});
														
														$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
														foreach($CampaignClients as $CampaignClient)
														{
															if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
															{
																$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																
																Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
																	$message->from('noreply@pownder.com', 'Pownder');
																	$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
																});
															}
															
															if($CampaignClient->user_notification == 'No')
															{
																if($CampaignClient->manager_id != '')
																{
																	$ManagerArray = explode(",", $CampaignClient->manager_id);
																	$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																	foreach($Managers as $Manager)
																	{
																		$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																		
																		Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																			$message->from('noreply@pownder.com', 'Pownder');
																			$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																		}); 
																	}
																}
															}
															else
															{
																$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
																if(!is_null($ManagerExists))
																{
																	if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
																	{
																		if($CampaignClient->manager_id != '')
																		{
																			$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
																			
																			$ManagerArray = explode(",", $CampaignClient->manager_id);
																			
																			$Array = array();
																			$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																			foreach($queries as $query)
																			{
																				$Array[] = $query->id;
																			}
																			
																			if(count($Array) > 0)
																			{
																				$index = $LeadCount % count($Array);
																				
																				$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																				if(!is_null($Manager))
																				{
																					$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																					
																					Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																						$message->from('noreply@pownder.com', 'Pownder');
																						$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																					}); 
																					
																					DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																					->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																				}
																			}
																		}
																	}
																}
															}	
														}
													}
												}
												else
												{
													DB::table('clients')
													->where('id', $client_id)
													->update(['start_lead_assign' => 1]);
												}
											}
											
										}
										else
										{
											DB::table('clients')
											->where('id', $client_id)
											->update(['start_lead_assign' => 1]);
										}
										
									}
									else
									{
										DB::table('clients')
										->where('id', $client_id)
										->update(['start_lead_assign' => 1]);
									}
								}
								elseif($package_type=='Lead' || $package_type=='Sold')
								{
									if($monthly_lead_quantity=='')
									{
										if($client_lead_form != '')
										{
											//echo "Pass 8";die;
											$LeadForm = explode(',', $client_lead_form);
											
											$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->whereIn('facebook_ads_lead_id', $LeadForm)->where('city', '!=', '')->where('client_id', 0)->where('is_deleted', 0)->get();
											if(count($FacebookAdsLeadUsers)>0)
											{
												//echo "Pass 9";die;
												$facebook_ads_lead_user_id=0;
												$facebook_account_id=0;
												$facebook_page_id=0;
												$facebook_ads_lead_id=0;
												$leadName = '';
												$leadCity = '';
												$leadEmail = '';
												$leadPhone = '';
												$miles='';
												foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
												{
													
													$city=$FacebookAdsLeadUser->city;
													
													$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($city)."&sensor=false";
													$result_string = file_get_contents($url);
													$result = json_decode($result_string, true);
													if(count($result['results'])>0)
													{
														$val = $result['results'][0]['geometry']['location'];
														$user_latitude=$val['lat'];
														$user_longitude=$val['lng'];
														$distance_lat_lon=Helper::DistanceLatLon($client_latitude, $client_longitude, $user_latitude, $user_longitude);
														if($miles=='')
														{
															$facebook_ads_lead_user_id=$FacebookAdsLeadUser->id;
															$facebook_account_id=$FacebookAdsLeadUser->facebook_account_id;
															$facebook_page_id=$FacebookAdsLeadUser->facebook_page_id;
															$facebook_ads_lead_id=$FacebookAdsLeadUser->facebook_ads_lead_id;
															$leadName = $FacebookAdsLeadUser->full_name;
															$leadCity = $FacebookAdsLeadUser->city;
															$leadEmail = $FacebookAdsLeadUser->email;
															$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
															$miles=$distance_lat_lon;
														}
														else if($miles>$distance_lat_lon)
														{
															$facebook_ads_lead_user_id=$FacebookAdsLeadUser->id;
															$facebook_account_id=$FacebookAdsLeadUser->facebook_account_id;
															$facebook_page_id=$FacebookAdsLeadUser->facebook_page_id;
															$facebook_ads_lead_id=$FacebookAdsLeadUser->facebook_ads_lead_id;
															$leadName = $FacebookAdsLeadUser->full_name;
															$leadCity = $FacebookAdsLeadUser->city;
															$leadEmail = $FacebookAdsLeadUser->email;
															$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
															$miles=$distance_lat_lon;
														}
													}
													else
													{
														$facebook_ads_lead_user_id=$FacebookAdsLeadUser->id;
														$facebook_account_id=$FacebookAdsLeadUser->facebook_account_id;
														$facebook_page_id=$FacebookAdsLeadUser->facebook_page_id;
														$facebook_ads_lead_id=$FacebookAdsLeadUser->facebook_ads_lead_id;
														$leadName = $FacebookAdsLeadUser->full_name;
														$leadCity = $FacebookAdsLeadUser->city;
														$leadEmail = $FacebookAdsLeadUser->email;
														$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
														break;
													}
												}
												
												DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
												{
													$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
													'user_id' => $user_id,
													'client_id' => $client_id, 
													'package_id' => $package_id,
													'facebook_account_id' => $facebook_account_id,
													'facebook_page_id' => $facebook_page_id,
													'facebook_ads_lead_id' => $facebook_ads_lead_id,
													'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
													'created_at' => date('Y-m-d H:i:s')
													]);
													
													DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
													->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
												});
												
												$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
												foreach($CampaignClients as $CampaignClient)
												{
													if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
													{
														$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
														
														Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
															$message->from('noreply@pownder.com', 'Pownder');
															$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
														});
													}
													
													if($CampaignClient->user_notification == 'No')
													{
														if($CampaignClient->manager_id != '')
														{
															$ManagerArray = explode(",", $CampaignClient->manager_id);
															$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
															foreach($Managers as $Manager)
															{
																$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																
																Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																	$message->from('noreply@pownder.com', 'Pownder');
																	$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																}); 
															}
														}
													}
													else
													{
														$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
														if(!is_null($ManagerExists))
														{
															if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
															{
																if($CampaignClient->manager_id != '')
																{
																	$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
																	
																	$ManagerArray = explode(",", $CampaignClient->manager_id);
																	
																	$Array = array();
																	$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																	foreach($queries as $query)
																	{
																		$Array[] = $query->id;
																	}
																	
																	if(count($Array) > 0)
																	{
																		$index = $LeadCount % count($Array);
																		
																		$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																		if(!is_null($Manager))
																		{
																			$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																			
																			Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																				$message->from('noreply@pownder.com', 'Pownder');
																				$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																			}); 
																			
																			DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																			->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																		}
																	}
																}
															}
														}
													}	
												}
											}
											else
											{
												$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->whereIn('facebook_ads_lead_id', $LeadForm)->where('city', '')->where('client_id', 0)->where('is_deleted', 0)->take(1)->get();
												if(count($FacebookAdsLeadUsers)>0)
												{
													//echo "Pass 9";die;
													foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
													{
														$facebook_ads_lead_user_id=$FacebookAdsLeadUser->id;
														$facebook_account_id=$FacebookAdsLeadUser->facebook_account_id;
														$facebook_page_id=$FacebookAdsLeadUser->facebook_page_id;
														$facebook_ads_lead_id=$FacebookAdsLeadUser->facebook_ads_lead_id;
														$leadName = $FacebookAdsLeadUser->full_name;
														$leadCity = $FacebookAdsLeadUser->city;
														$leadEmail = $FacebookAdsLeadUser->email;
														$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
														
														DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
														{
															$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
															'user_id' => $user_id,
															'client_id' => $client_id,
															'package_id' => $package_id,
															'facebook_account_id' => $facebook_account_id,
															'facebook_page_id' => $facebook_page_id,
															'facebook_ads_lead_id' => $facebook_ads_lead_id,
															'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
															'created_at' => date('Y-m-d H:i:s')
															]);
															
															DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
															->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
														});
														
														$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
														foreach($CampaignClients as $CampaignClient)
														{
															if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
															{
																$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																
																Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
																	$message->from('noreply@pownder.com', 'Pownder');
																	$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
																});
															}
															
															if($CampaignClient->user_notification == 'No')
															{
																if($CampaignClient->manager_id != '')
																{
																	$ManagerArray = explode(",", $CampaignClient->manager_id);
																	$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																	foreach($Managers as $Manager)
																	{
																		$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																		
																		Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																			$message->from('noreply@pownder.com', 'Pownder');
																			$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																		}); 
																	}
																}
															}
															else
															{
																$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
																if(!is_null($ManagerExists))
																{
																	if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
																	{
																		if($CampaignClient->manager_id != '')
																		{
																			$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
																			
																			$ManagerArray = explode(",", $CampaignClient->manager_id);
																			
																			$Array = array();
																			$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																			foreach($queries as $query)
																			{
																				$Array[] = $query->id;
																			}
																			
																			if(count($Array) > 0)
																			{
																				$index = $LeadCount % count($Array);
																				
																				$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																				if(!is_null($Manager))
																				{
																					$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																					
																					Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																						$message->from('noreply@pownder.com', 'Pownder');
																						$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																					}); 
																					
																					DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																					->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																				}
																			}
																		}
																	}
																}
															}	
														}
													}
												}
												else
												{
													DB::table('clients')->where('id', $client_id)
													->update(['start_lead_assign' => 1]);
												}
											}
											
										}
										else
										{
											DB::table('clients')->where('id', $client_id)
											->update(['start_lead_assign' => 1]);
										}
									}
									else
									{
										//------(Remaining Total Lead Calculate)------
										$assign_lead_quantity=DB::table('client_ads_lead_users')->select('id')->where('client_id', $client_id)->whereYear('created_at', '=', date('Y'))->whereMonth('created_at', '=', date('m'))->whereDate('created_at', '!=', date('Y-m-d'))->where('is_deleted', 0)->count();
										$remaining_lead_quantity=$monthly_lead_quantity-$assign_lead_quantity;
										//--------------------------------------------
										
										if($remaining_lead_quantity>0)
										{
											//echo "Pass 4";die;
											if($client_lead_form != '')
											{
												//echo "Pass 8";die;
												$LeadForm = explode(',',$client_lead_form);
												
												$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->whereIn('facebook_ads_lead_id', $LeadForm)->where('city', '!=', '')->where('client_id', 0)->where('is_deleted', 0)->get();
												if(count($FacebookAdsLeadUsers)>0)
												{
													//echo "Pass 9";die;
													$facebook_ads_lead_user_id=0;
													$facebook_account_id=0;
													$facebook_page_id=0;
													$facebook_ads_lead_id=0;
													$leadName = '';
													$leadCity = '';
													$leadEmail = '';
													$leadPhone = '';
													$miles='';
													foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
													{
														
														$city=$FacebookAdsLeadUser->city;
														
														$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($city)."&sensor=false";
														$result_string = file_get_contents($url);
														$result = json_decode($result_string, true);
														if(count($result['results'])>0)
														{
															$val = $result['results'][0]['geometry']['location'];
															$user_latitude=$val['lat'];
															$user_longitude=$val['lng'];
															$distance_lat_lon=Helper::DistanceLatLon($client_latitude, $client_longitude, $user_latitude, $user_longitude);
															if($miles=='')
															{
																$facebook_ads_lead_user_id=$FacebookAdsLeadUser->id;
																$facebook_account_id=$FacebookAdsLeadUser->facebook_account_id;
																$facebook_page_id=$FacebookAdsLeadUser->facebook_page_id;
																$facebook_ads_lead_id=$FacebookAdsLeadUser->facebook_ads_lead_id;
																$leadName = $FacebookAdsLeadUser->full_name;
																$leadCity = $FacebookAdsLeadUser->city;
																$leadEmail = $FacebookAdsLeadUser->email;
																$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
																$miles=$distance_lat_lon;
															}
															else if($miles>$distance_lat_lon)
															{
																$facebook_ads_lead_user_id=$FacebookAdsLeadUser->id;
																$facebook_account_id=$FacebookAdsLeadUser->facebook_account_id;
																$facebook_page_id=$FacebookAdsLeadUser->facebook_page_id;
																$facebook_ads_lead_id=$FacebookAdsLeadUser->facebook_ads_lead_id;
																$leadName = $FacebookAdsLeadUser->full_name;
																$leadCity = $FacebookAdsLeadUser->city;
																$leadEmail = $FacebookAdsLeadUser->email;
																$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
																$miles=$distance_lat_lon;
															}
														}
														else
														{
															$facebook_ads_lead_user_id=$FacebookAdsLeadUser->id;
															$facebook_account_id=$FacebookAdsLeadUser->facebook_account_id;
															$facebook_page_id=$FacebookAdsLeadUser->facebook_page_id;
															$facebook_ads_lead_id=$FacebookAdsLeadUser->facebook_ads_lead_id;
															$leadName = $FacebookAdsLeadUser->full_name;
															$leadCity = $FacebookAdsLeadUser->city;
															$leadEmail = $FacebookAdsLeadUser->email;
															$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
															break;
														}
													}
													
													DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
													{
														$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
														'user_id' => $user_id,
														'client_id' => $client_id, 
														'package_id' => $package_id,
														'facebook_account_id' => $facebook_account_id,
														'facebook_page_id' => $facebook_page_id,
														'facebook_ads_lead_id' => $facebook_ads_lead_id,
														'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
														'created_at' => date('Y-m-d H:i:s')
														]);
														
														DB::table('facebook_ads_lead_users')
														->where('id', $facebook_ads_lead_user_id)
														->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
													});
													
													$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
													foreach($CampaignClients as $CampaignClient)
													{
														if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
														{
															$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
															
															Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
																$message->from('noreply@pownder.com', 'Pownder');
																$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
															});
														}
														
														if($CampaignClient->user_notification == 'No')
														{
															if($CampaignClient->manager_id != '')
															{
																$ManagerArray = explode(",", $CampaignClient->manager_id);
																$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																foreach($Managers as $Manager)
																{
																	$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																	
																	Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																		$message->from('noreply@pownder.com', 'Pownder');
																		$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																	}); 
																}
															}
														}
														else
														{
															$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
															if(!is_null($ManagerExists))
															{
																if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
																{
																	if($CampaignClient->manager_id != '')
																	{
																		$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
																		
																		$ManagerArray = explode(",", $CampaignClient->manager_id);
																		
																		$Array = array();
																		$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																		foreach($queries as $query)
																		{
																			$Array[] = $query->id;
																		}
																		
																		if(count($Array) > 0)
																		{
																			$index = $LeadCount % count($Array);
																			
																			$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																			if(!is_null($Manager))
																			{
																				$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																				
																				Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																					$message->from('noreply@pownder.com', 'Pownder');
																					$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																				}); 
																				
																				DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																				->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																			}
																		}
																	}
																}
															}
														}	
													}
												}
												else
												{
													$FacebookAdsLeadUsers = DB::table('facebook_ads_lead_users')->whereIn('facebook_ads_lead_id', $LeadForm)->where('city', '')->where('client_id', 0)->where('is_deleted', 0)->take(1)->get();
													if(count($FacebookAdsLeadUsers)>0)
													{
														//echo "Pass 9";die;
														foreach($FacebookAdsLeadUsers as $FacebookAdsLeadUser)
														{
															$facebook_ads_lead_user_id=$FacebookAdsLeadUser->id;
															$facebook_account_id=$FacebookAdsLeadUser->facebook_account_id;
															$facebook_page_id=$FacebookAdsLeadUser->facebook_page_id;
															$facebook_ads_lead_id=$FacebookAdsLeadUser->facebook_ads_lead_id;
															$leadName = $FacebookAdsLeadUser->full_name;
															$leadCity = $FacebookAdsLeadUser->city;
															$leadEmail = $FacebookAdsLeadUser->email;
															$leadPhone = Helper::emailPhoneFormat($FacebookAdsLeadUser->phone_number);
															
															DB::transaction(function () use ($user_id, $client_id, $package_id, $facebook_account_id, $facebook_page_id, $facebook_ads_lead_id, $facebook_ads_lead_user_id) 
															{
																$client_ads_lead_user_id = DB::table('client_ads_lead_users')->insertGetId([
																'user_id' => $user_id,
																'client_id' => $client_id,
																'package_id' => $package_id,
																'facebook_account_id' => $facebook_account_id,
																'facebook_page_id' => $facebook_page_id,
																'facebook_ads_lead_id' => $facebook_ads_lead_id,
																'facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,
																'created_at' => date('Y-m-d H:i:s')
																]);
																
																DB::table('facebook_ads_lead_users')
																->where('id', $facebook_ads_lead_user_id)
																->update(['client_ads_lead_user_id' => $client_ads_lead_user_id, 'client_id' => $client_id]);
															});
															
															$CampaignClients = DB::table('campaigns')->where('client_id', $client_id)->where('is_active', 0)->where('is_deleted', 0)->get();
															foreach($CampaignClients as $CampaignClient)
															{
																if($CampaignClient->email_lead_notification != '' && ($CampaignClient->lead_notification == 'Both' || $CampaignClient->lead_notification == 'Only New Leads'))
																{
																	$data13 = array('name' => $CampaignClient->client_contact, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																	
																	Mail::send('emails.cron_new_lead', $data13, function ($message) use ($CampaignClient) {
																		$message->from('noreply@pownder.com', 'Pownder');
																		$message->to(explode(",", str_replace(" ", "", $CampaignClient->email_lead_notification)))->subject('NEW FACEBOOK LEAD');
																	});
																}
																
																if($CampaignClient->user_notification == 'No')
																{
																	if($CampaignClient->manager_id != '')
																	{
																		$ManagerArray = explode(",", $CampaignClient->manager_id);
																		$Managers = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																		foreach($Managers as $Manager)
																		{
																			$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																			
																			Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																				$message->from('noreply@pownder.com', 'Pownder');
																				$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																			}); 
																		}
																	}
																}
																else
																{
																	$ManagerExists = DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->where('manager_id', 0)->first();
																	if(!is_null($ManagerExists))
																	{
																		if($CampaignClient->notification == 'For New Leads' || $CampaignClient->notification == 'Both')
																		{
																			if($CampaignClient->manager_id != '')
																			{
																				$LeadCount = DB::table('facebook_ads_lead_users')->select('id')->where('camp_id', $CampaignClient->id)->count('id');
																				
																				$ManagerArray = explode(",", $CampaignClient->manager_id);
																				
																				$Array = array();
																				$queries = DB::table('managers')->whereIn('id', $ManagerArray)->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->get();
																				foreach($queries as $query)
																				{
																					$Array[] = $query->id;
																				}
																				
																				if(count($Array) > 0)
																				{
																					$index = $LeadCount % count($Array);
																					
																					$Manager = DB::table('managers')->where('id', $Array[$index])->where('lead_notifier', 'On')->where('email', '!=', '')->where('status', 1)->where('is_deleted', 0)->first();
																					if(!is_null($Manager))
																					{
																						$data14 = array('name' => $Manager->full_name, 'full_name' => $leadName, 'email' => $leadEmail, 'phone' => $leadPhone, 'city' => $leadCity);
																						
																						Mail::send('emails.cron_new_lead', $data14, function ($message) use ($Manager) {
																							$message->from('noreply@pownder.com', 'Pownder');
																							$message->to($Manager->email)->subject('NEW FACEBOOK LEAD');
																						}); 
																						
																						DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)
																						->update(['camp_id' => $CampaignClient->id, 'manager_id' => $Manager->id]);
																					}
																				}
																			}
																		}
																	}
																}	
															}
														}
													}
													else
													{
														DB::table('clients')
														->where('id', $client_id)
														->update(['start_lead_assign' => 1]);
													}
												}
												
											}
											else
											{
												DB::table('clients')
												->where('id', $client_id)
												->update(['start_lead_assign' => 1]);
											}
											
										}
										else
										{
											DB::table('clients')
											->where('id', $client_id)
											->update(['start_lead_assign' => 1]);
										}
									}
								}
								else
								{
									DB::table('clients')->where('id', $client_id)
									->update(['start_lead_assign' => 1]);
								}
							}
						}
					}
					else
					{
						DB::table('clients')->where('status', 1)->where('is_deleted', 0)
						->update(['start_lead_assign' => 1]);
						$loop++;
					}
				}
				else
				{
					DB::table('clients')->where('status', 1)->where('is_deleted', 0)
					->update(['start_lead_assign' => 1]);
					$loop++;
				}
			}
		}	
	}																																			