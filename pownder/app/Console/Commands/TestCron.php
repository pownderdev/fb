<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	
	use Mail;
	
	class TestCron extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:TestCron';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Test Cron';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			$data = array('name' => 'Rakesh Kashyap', 'full_name' => 'Test', 'email' => 'test@abc.com', 'phone' => '0000000000', 'city' => 'Local');
			
			Mail::send('emails.cron_new_lead', $data, function ($message) {
				$message->from('noreply@pownder.com', 'Pownder');
				$message->to('rakesh.constacloud@gmail.com')->subject('NEW FACEBOOK LEAD');
			}); 
		}	
	}
