<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
    
	class SendRecurringInvoices extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:SendRecurringInvoices';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Send Automated Invoice To Clients/Vendors';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
        
        var $now='';        
		
		public function __construct()
		{
			parent::__construct();
            $this->setTimeZone('America/Los_Angeles');
            $this->now=$this->getCurrentDateTime();
			
		}
		public function setTimeZone($tz)
        {
			return  date_default_timezone_set($tz);
		}
        public function getCurrentDateTime()
        {
            date_default_timezone_set('America/Los_Angeles');
            return date('Y-m-d H:i:s');
		}
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			ini_set('max_execution_time',0);
			ini_set('memory_limit',-1);
			date_default_timezone_set('America/Los_Angeles');
			
			$invoices=DB::table('invoice')->where([['recurring_on',1],['has_recurred',0],['is_deleted',0],['recurring_date','!=','0000-00-00 00:00:00'],[DB::raw('DATE(recurring_date)'),'<=',DB::raw('DATE_ADD(CURDATE(), INTERVAL ( case when ( due_on_receipt<21) then due_on_receipt else 3 end )  DAY)')]])->get();
			
			foreach($invoices as $invoice)
			{
				$options=[
				CURLOPT_URL=>'http://big.pownder.com/send-invoice-auto?invoice_num='.$invoice->invoice_num.'&invoice_id='.$invoice->invoice_id,
				CURLOPT_HEADER=>false,
				CURLOPT_RETURNTRANSFER=>true
				]; 
				$curl=curl_init();
				curl_setopt_array($curl,$options);
				$response=curl_exec($curl);
				$http_code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
			}                                    
		}
	}
