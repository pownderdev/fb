<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
	
	class FacebookAdClicks extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:FacebookAdClicks';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Get facebook ad clicks';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$FacebookAccounts = DB::table('facebook_accounts')->select('id', 'access_token', 'updated_user_id')->where('is_deleted', 0)->orderBy('updated_at', 'DESC')->get();
			foreach($FacebookAccounts as $FacebookAccount)
			{
				$facebook_account_id = $FacebookAccount->id;
				$access_token = $FacebookAccount->access_token;
				$created_user_id = $FacebookAccount->updated_user_id;
				$updated_user_id = $FacebookAccount->updated_user_id;
				
				$FacebookAdsAccounts = DB::table('facebook_ads_accounts')->select('id', 'facebook_id')->where('facebook_account_id', $facebook_account_id)->where('is_deleted', 0)->orderBy('updated_at', 'DESC')->get();
				foreach($FacebookAdsAccounts as $FacebookAdsAccount)
				{					
					$facebook_ads_account_id=$FacebookAdsAccount->id;
					$ads_account = $FacebookAdsAccount->facebook_id;
					
					//Read facebook clicks
					$curl = curl_init('https://graph.facebook.com/v2.10/'.$ads_account.'/insights?fields=clicks&access_token='.$access_token);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
					
					$response = curl_exec($curl);
					$info = curl_getinfo($curl);
					
					curl_close($curl);
					
					$result = json_decode($response,true);
					//echo"<pre>";
					//print_r($result);die;
					
					if(isset($result['data']))
					{
						$data = $result['data'];
						
						for($i = 0; $i < count($data); $i++)
						{
							if(isset($data[$i]['clicks']))
							{
								$check_page_exist=DB::table('facebook_clicks')->where('facebook_account_id', $facebook_account_id)->where('facebook_ads_account_id', $facebook_ads_account_id)->first();
								if(is_null($check_page_exist))
								{
									DB::table('facebook_clicks')->insert([
									'facebook_account_id' => $facebook_account_id,
									'facebook_ads_account_id' => $facebook_ads_account_id,
									'ads_account' => $ads_account,
									'clicks' => $data[$i]['clicks'],
									'date_start' => $data[$i]['date_start'],
									'date_stop' => $data[$i]['date_stop'],
									'created_user_id' => $created_user_id,
									'created_at' => date('Y-m-d H:i:s'),
									'updated_user_id' => $updated_user_id,
									'updated_at' => date('Y-m-d H:i:s')
									]);
								}
								else
								{
									DB::table('facebook_clicks')
									->where('facebook_account_id', $facebook_account_id)
									->where('facebook_ads_account_id', $facebook_ads_account_id)
									->update([
									'facebook_account_id' => $facebook_account_id,
									'facebook_ads_account_id' => $facebook_ads_account_id,
									'ads_account' => $ads_account,
									'clicks' => $data[$i]['clicks'],
									'date_start' => $data[$i]['date_start'],
									'date_stop' => $data[$i]['date_stop'],
									'updated_user_id' => $updated_user_id,
									'updated_at' => date('Y-m-d H:i:s')
									]);
								}
							}
						}
					}	
				}
			}
		}
	}