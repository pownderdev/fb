<?php
	
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	use DB;
	use Helper;
	use Mail;
	
	class GetAdsLeadForm extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:GetAdsLeadForm';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Get all ads lead form for facebook page';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			set_time_limit(0);
			date_default_timezone_set('America/Los_Angeles');
			
			$counter = 1;
			
			$FacebookAccounts = DB::table('facebook_accounts')->select('id', 'access_token', 'updated_user_id')->where('is_deleted', 0)->orderBy('updated_at', 'DESC')->get();
			foreach($FacebookAccounts as $FacebookAccount)
			{
				$facebook_account_id=$FacebookAccount->id;
				$accessToken=$FacebookAccount->access_token;
				$created_user_id=$FacebookAccount->updated_user_id;
				$updated_user_id=$FacebookAccount->updated_user_id;
				
				//Read facebook pages
				$url1 = "http://big.pownder.com/fb_campaign/page_list.php?accessToken=$accessToken";
				
				$ch1 = curl_init();
				
				curl_setopt($ch1, CURLOPT_URL,$url1);
				curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch1, CURLOPT_TIMEOUT, 500);
				curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($ch1, CURLOPT_SSL_VERIFYHOST, 0);
				
				$response1 = curl_exec($ch1);
				$info1 = curl_getinfo($ch1);
				
				curl_close($ch1);
				
				$result1 = json_decode($response1, true);
				
				//echo "<pre>";
				//print_r($result1);
				
				for($i=0;$i<count($result1);$i++)
				{
					$facebook_page_id = $result1[$i]['id'];
					$facebook_page_access_token = $result1[$i]['access_token'];
					
					if($facebook_page_id>0)
					{
						$check_page_exist=DB::table('facebook_pages')->where('id', $facebook_page_id)->first();
						if(is_null($check_page_exist))
						{
							DB::table('facebook_pages')->insert([
							'id' => $facebook_page_id,
							'facebook_account_id' => $facebook_account_id,
							'name' => $result1[$i]['name'],
							'category' => $result1[$i]['category'],
							'access_token' => $result1[$i]['access_token'],
							'created_user_id' => $created_user_id,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_user_id' => $updated_user_id,
							'updated_at' => date('Y-m-d H:i:s')
							]);
						}
						else
						{
							DB::table('facebook_pages')
							->where('id', $facebook_page_id)
							->update([
							'facebook_account_id' => $facebook_account_id,
							'name' => $result1[$i]['name'],
							'category' => $result1[$i]['category'],
							'access_token' => $result1[$i]['access_token'],
							'updated_user_id' => $updated_user_id,
							'updated_at' => date('Y-m-d H:i:s'),
							'is_deleted' => 0
							]);
						}
					}
				}
				
				for($i=0;$i<count($result1);$i++)
				{
					$facebook_page_id = $result1[$i]['id'];
					$facebook_page_access_token = $result1[$i]['access_token'];
					
					if($facebook_page_id>0)
					{
						if($counter%50 == 0)
						{
							sleep(15);
						}
						
						$counter++;
						
						$check_page_exist=DB::table('facebook_pages')->where('id', $facebook_page_id)->first();
						if(is_null($check_page_exist))
						{
							DB::table('facebook_pages')->insert([
							'id' => $facebook_page_id,
							'facebook_account_id' => $facebook_account_id,
							'name' => $result1[$i]['name'],
							'category' => $result1[$i]['category'],
							'access_token' => $result1[$i]['access_token'],
							'created_user_id' => $created_user_id,
							'created_at' => date('Y-m-d H:i:s'),
							'updated_user_id' => $updated_user_id,
							'updated_at' => date('Y-m-d H:i:s')
							]);
						}
						else
						{
							DB::table('facebook_pages')->where('id', $facebook_page_id)
							->update([
							'facebook_account_id' => $facebook_account_id,
							'name' => $result1[$i]['name'],
							'category' => $result1[$i]['category'],
							'access_token' => $result1[$i]['access_token'],
							'updated_user_id' => $updated_user_id,
							'updated_at' => date('Y-m-d H:i:s'),
							'is_deleted' => 0
							]);
						}
						
						//Read facebook ads lead 
						$url2 = "http://big.pownder.com/fb_campaign/get_lead_ads.php?facebook_page_access_token=$facebook_page_access_token&facebook_page_id=$facebook_page_id";
						
						$ch2 = curl_init();
						
						curl_setopt($ch2, CURLOPT_URL,$url2);
						curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);
						
						$response2 = curl_exec($ch2);
						
						curl_close($ch2);
						
						$result2 = json_decode($response2, true);
						
						for($j=0;$j<count($result2);$j++)
						{
							$facebook_ads_lead_id = $result2[$j]['id'];
							
							if($facebook_ads_lead_id>0)
							{
								$check_exist = DB::table('facebook_ads_leads')->where('id', $facebook_ads_lead_id)->first();
								if(is_null($check_exist))
								{
									DB::table('facebook_ads_leads')->insert([
									'id' => $facebook_ads_lead_id,
									'facebook_account_id' => $facebook_account_id,
									'facebook_page_id' => $facebook_page_id,
									'name' => $result2[$j]['name'],
									'leadgen_export_csv_url' => $result2[$j]['leadgen_export_csv_url'],
									'locale' => $result2[$j]['locale'],
									'status' => $result2[$j]['status'],
									'created_user_id' => $created_user_id,
									'created_at' => date('Y-m-d H:i:s'),
									'updated_user_id' => $updated_user_id,
									'updated_at' => date('Y-m-d H:i:s')
									]);
									
									$is_blocked = 0;
								}
								else
								{
									DB::table('facebook_ads_leads')->where('id', $facebook_ads_lead_id)
									->update([
									'facebook_account_id' => $facebook_account_id,
									'facebook_page_id' => $facebook_page_id,
									'name' => $result2[$j]['name'],
									'leadgen_export_csv_url' => $result2[$j]['leadgen_export_csv_url'],
									'locale' => $result2[$j]['locale'],
									'status' => $result2[$j]['status'],
									'updated_user_id' => $updated_user_id,
									'updated_at' => date('Y-m-d H:i:s'),
									'is_deleted' => 0
									]);
									
									$is_blocked = $check_exist->is_blocked;
								}
								
								if($result2[$j]['status'] == 'ACTIVE' && $is_blocked == 0)
								{
									//Read facebook ads lead user
									$url3 = "http://big.pownder.com/fb_campaign/leads_detail.php?facebook_page_access_token=$facebook_page_access_token&facebook_ads_lead_form_id=$facebook_ads_lead_id";
									
									$ch3 = curl_init();
									
									curl_setopt($ch3, CURLOPT_URL,$url3);
									curl_setopt($ch3, CURLOPT_RETURNTRANSFER, 1);
									
									$response3 = curl_exec($ch3);
									
									curl_close($ch3);
									
									$result3 = json_decode($response3, true);
									
									for($k=0;$k<count($result3);$k++)
									{
										if(isset($result3[$k]['id']))
										{
											$facebook_ads_lead_user_id = $result3[$k]['id'];
											
											if($facebook_ads_lead_user_id>0)
											{												
												$check_page_lead_exist=DB::table('facebook_ads_lead_users')->where('id', $facebook_ads_lead_user_id)->first();
												
												if(is_null($check_page_lead_exist))
												{
													$field_data=$result3[$k]['field_data'];
													
													$first_name='';
													$last_name='';
													$full_name='';
													$email='';
													$phone_number='';
													$street_address='';
													$city='';
													$state='';
													$province='';
													$country='';
													$post_code='';
													$zip_code='';
													$date_of_birth='';
													$gender='';
													$marital_status='';
													$relationship_status='';
													$company_name='';
													$military_status='';
													$job_title='';
													$work_phone_number='';
													$work_email='';
													$numero_telefonico='';
													
													for($m=0; $m<count($field_data);$m++)
													{
														$$field_data[$m]['name']=$field_data[$m]['values'][0];
													}
													
													if($phone_number!='')
													{
														$phone_number=Helper::PhoneNumber($phone_number);
													}
													
													if($full_name=='')
													{
														$full_name=$first_name.' '.$last_name;
													}
													
													$latitude = '';
													$longitude = '';
													
													if($post_code>0)
													{
														$address = str_replace(" ","+",$post_code);
														$url = "http://maps.google.com/maps/api/geocode/json?address=$address";
														$ch = curl_init();
														curl_setopt($ch, CURLOPT_URL, $url);
														curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
														curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
														curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
														curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
														$response = curl_exec($ch);
														curl_close($ch);
														
														$response_a = json_decode($response);
														if(count($response_a->results)>0)
														{
															$latitude = $response_a->results[0]->geometry->location->lat;
															$longitude = $response_a->results[0]->geometry->location->lng;
														}
													}
													elseif($city != '')
													{
														$address = str_replace(" ","+",$city);
														$url = "http://maps.google.com/maps/api/geocode/json?address=$address";
														$ch = curl_init();
														curl_setopt($ch, CURLOPT_URL, $url);
														curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
														curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
														curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
														curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
														$response = curl_exec($ch);
														curl_close($ch);
														
														$response_a = json_decode($response);
														if(count($response_a->results)>0)
														{
															$latitude = $response_a->results[0]->geometry->location->lat;
															$longitude = $response_a->results[0]->geometry->location->lng;
														}
													}
													
													$created_time =  date('Y-m-d H:i:s', strtotime($result3[$k]['created_time']));
													
													DB::table('facebook_ads_lead_users')->insert([
													'id' => $facebook_ads_lead_user_id,
													'facebook_account_id' => $facebook_account_id,
													'facebook_page_id' => $facebook_page_id,
													'facebook_ads_lead_id' => $facebook_ads_lead_id,
													'ad_id' => isset($result3[$k]['ad_id'])?$result3[$k]['ad_id']:0,
													'ad_name' => isset($result3[$k]['ad_name'])?$result3[$k]['ad_name']:'',
													'adset_id' => isset($result3[$k]['adset_id'])?$result3[$k]['adset_id']:0,
													'adset_name' => isset($result3[$k]['adset_name'])?$result3[$k]['adset_name']:'',
													'campaign_id' => isset($result3[$k]['campaign_id'])?$result3[$k]['campaign_id']:0,
													'campaign_name' => isset($result3[$k]['campaign_name'])?$result3[$k]['campaign_name']:'',
													'first_name' => $first_name,
													'last_name' => $last_name,
													'full_name' => $full_name,
													'email' => $email,
													'phone_number' => $phone_number,
													'street_address' => $street_address,
													'city' => $city,
													'state' => $state,
													'province' => $province,
													'country' => $country,
													'post_code' => $post_code,
													'zip_code' => $zip_code,
													'date_of_birth' => $date_of_birth,
													'gender' => $gender,
													'marital_status' => $marital_status,
													'relationship_status' => $relationship_status,
													'company_name' => $company_name,
													'military_status' => $military_status,
													'job_title' => $job_title,
													'work_phone_number' => $work_phone_number,
													'work_email' => $work_email,
													'numero_telefonico' => $numero_telefonico,
													'latitude' => $latitude,
													'longitude' => $longitude,
													'created_time' => $created_time,
													'created_user_id' => $created_user_id,
													'created_at' => date('Y-m-d H:i:s'),
													'updated_user_id' => $updated_user_id,
													'updated_at' => date('Y-m-d H:i:s')
													]);
													
													$admin = DB::table('admins')->where('id', 1)->where('new_lead_email', '!=', '')->where('new_lead_option', 'Yes')->where('is_deleted', 0)->first();
													if(!is_null($admin))
													{
														if($admin->receive_notification == 'Everytime')
														{
															$data14 = array('name' => $admin->name, 'full_name' => $full_name, 'email' => $email, 'phone' => Helper::emailPhoneFormat($phone_number), 'city' => $city);
															
															Mail::send('emails.cron_new_lead', $data14, function ($message) use ($admin) {
																$message->from('noreply@pownder.com', 'Pownder');
																$message->to($admin->new_lead_email)->subject('NEW FACEBOOK LEAD');
															});
														}
														else
														{
															DB::table('admin_emails')->insert(['facebook_ads_lead_user_id' => $facebook_ads_lead_user_id,  'subject' => 'NEW FACEBOOK LEAD', 'client_name' => '', 'leadType' => 'Pownder™ Lead', 'leadEmail' => $email, 'leadPhone' => Helper::emailPhoneFormat($phone_number), 'leadCity' => $city, 'first_name' => $full_name, 'created_at' => date('Y-m-d H:i:s')]);
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}	
			}
		}
	}																												