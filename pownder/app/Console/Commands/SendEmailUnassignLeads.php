<?php
	namespace App\Console\Commands;
	
	use Illuminate\Console\Command;
	
	use Mail;
	use Helper;
	use DB;
	
	class SendEmailUnassignLeads extends Command
	{
		/**
			* The name and signature of the console command.
			*
			* @var string
		*/
		protected $signature = 'Cron:SendEmailUnassignLeads';
		
		/**
			* The console command description.
			*
			* @var string
		*/
		protected $description = 'Send Email Unassigned Leads';
		
		/**
			* Create a new command instance.
			*
			* @return void
		*/
		public function __construct()
		{
			parent::__construct();
		}
		
		/**
			* Execute the console command.
			*
			* @return mixed
		*/
		public function handle()
		{
			$admin = DB::table('admins')->where('id', 1)->where('other_email', '!=', '')->where('other_option', 'Yes')->where('is_deleted', 0)->first();
			if(!is_null($admin))
			{
				$UnassignLeads = DB::table('facebook_ads_lead_users')->select('id')->where('client_id', 0)->count('id');
				
				if($UnassignLeads > 0)
				{
					$data = array('UnassignLeads' => $UnassignLeads);
					$to = 'notifications@pownder.com';
					Mail::send('emails.unassign_leads', $data, function ($message) use($admin) {
						$message->from('noreply@pownder.com', 'Pownder');
						$message->to($admin->other_email)->subject('Unassigned Leads');
					});
				}
			}
		}	
	}
