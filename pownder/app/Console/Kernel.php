<?php
	
	namespace App\Console;
	
	use Illuminate\Console\Scheduling\Schedule;
	use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
	use DB;
	
	class Kernel extends ConsoleKernel
	{
		/**
			* The Artisan commands provided by your application.
			*
			* @var array
		*/
		protected $commands = [
        \App\Console\Commands\GetAdsLeadForm::class,
		//\App\Console\Commands\ClientLeadAssign::class,
		//\App\Console\Commands\FacebookAdsetClicks::class,
		//\App\Console\Commands\FacebookAdsetSpend::class,
		//\App\Console\Commands\TestCron::class,
		//\App\Console\Commands\FacebookAdsetCtr::class,
		\App\Console\Commands\GroupClientLeadAssign::class,
		\App\Console\Commands\GroupPageEngagementLeadAssign::class,
		\App\Console\Commands\GeneralClientLeadAssign::class,
		\App\Console\Commands\ClientLeadOverride::class,
		\App\Console\Commands\GetFacebookCampaign::class,
		\App\Console\Commands\CreateContact::class,
		\App\Console\Commands\CreateAutomotiveContact::class,
		\App\Console\Commands\PageEngagementAutomotiveCRM::class,
		\App\Console\Commands\PageEngagementBusinessCRM::class,
        \App\Console\Commands\SendRecurringInvoices::class,
		\App\Console\Commands\TodayFacebookLeads::class,
		\App\Console\Commands\PageEngagementClientLead::class,
		\App\Console\Commands\PageEngagementLead::class,
		\App\Console\Commands\FacebookFormAutometicAssign::class,
		\App\Console\Commands\SendEmailUnassignLeads::class,
		\App\Console\Commands\FacebookLeadDetails::class,
		\App\Console\Commands\FacebookCampaigns::class,
        \App\Console\Commands\FetchClosedDeals::class,
        \App\Console\Commands\ReminderForOverdueInvoices::class,
        \App\Console\Commands\SendAppointmentEmail::class,
        \App\Console\Commands\SendEmailClientZeroLead::class,
        \App\Console\Commands\SendNewLeadEmail::class,
        \App\Console\Commands\SendAutoNotifications::class,
        \App\Console\Commands\NoClosedDealNotification::class
		];
		
		/**
			* Define the application's command schedule.
			*
			* @param  \Illuminate\Console\Scheduling\Schedule  $schedule
			* @return void
		*/
		protected function schedule(Schedule $schedule)
		{
			//$schedule->command('inspire')->hourly();
			//$schedule->command('Cron:FacebookAdsetClicks')->everyTenMinutes();
			//$schedule->command('Cron:FacebookAdsetSpend')->everyTenMinutes();
			//$schedule->command('Cron:TestCron')->everyFiveMinutes();
			//$schedule->command('Cron:ClientLeadAssign')->everyFiveMinutes();
			
			$schedule->command('Cron:FacebookFormAutometicAssign')->everyThirtyMinutes();
			$schedule->command('Cron:SendEmailClientZeroLead')->hourly();
			$schedule->command('Cron:SendEmailUnassignLeads')->hourly();
            
            
            //Invoice            
			$schedule->command('Cron:SendRecurringInvoices')->everyTenMinutes();
            $schedule->command('Cron:ReminderForOverdueInvoices')/*->everyMinute()*/->twiceDaily(9,13)->timezone('America/Los_Angeles');
            
			//Closed Deals
			$schedule->command('Cron:FetchClosedDeals')->everyFiveMinutes();
            $schedule->command('Cron:NoClosedDealNotification')->dailyAt('10:00')->timezone('America/Los_Angeles');
			
			//Client Lead Assign
			$schedule->command('Cron:GroupClientLeadAssign')->everyFiveMinutes();
			$schedule->command('Cron:GeneralClientLeadAssign')->everyFiveMinutes();
			$schedule->command('Cron:GroupPageEngagementLeadAssign')->everyFiveMinutes();
			$schedule->command('Cron:PageEngagementClientLead')->everyFiveMinutes();
            $schedule->command('Cron:PageEngagementLead')->everyFiveMinutes();
			$schedule->command('Cron:ClientLeadOverride')->hourlyAt(18);
			
			//CRM Contact
			$schedule->command('Cron:CreateContact')->everyThirtyMinutes();
			$schedule->command('Cron:PageEngagementBusinessCRM')->everyThirtyMinutes();
			$schedule->command('Cron:CreateAutomotiveContact')->everyThirtyMinutes();
			$schedule->command('Cron:PageEngagementAutomotiveCRM')->everyThirtyMinutes();
			
			//Facebook API
			$schedule->command('Cron:GetAdsLeadForm')->hourlyAt(17);
			$schedule->command('Cron:TodayFacebookLeads')->everyFiveMinutes();
			$schedule->command('Cron:FacebookLeadDetails')->everyTenMinutes();
			$schedule->command('Cron:GetFacebookCampaign')->everyTenMinutes();
			$schedule->command('Cron:FacebookCampaigns')->everyTenMinutes();
			
			$admin = DB::table('admins')->where('id', 1)->where('is_deleted', 0)->first();
			if(!is_null($admin))
			{
				if($admin->receive_notification == 'Hourly')
				{
					$schedule->command('Cron:SendAppointmentEmail')->hourly();
					$schedule->command('Cron:SendNewLeadEmail')->hourly();
                    $schedule->command('Cron:SendAutoNotifications')->hourly();
				}
				elseif($admin->receive_notification == 'Daily')
				{
					$schedule->command('Cron:SendAppointmentEmail')->daily();
					$schedule->command('Cron:SendNewLeadEmail')->daily();
                    $schedule->command('Cron:SendAutoNotifications')->daily();
				}
				elseif($admin->receive_notification == 'Weekly')
				{
					$schedule->command('Cron:SendAppointmentEmail')->weekly();
					$schedule->command('Cron:SendNewLeadEmail')->weekly();
                    $schedule->command('Cron:SendAutoNotifications')->weekly();
					$schedule->command('Cron:SendEmailUnassignLeads')->weekly();
				}
				elseif($admin->receive_notification == 'Semi-monthly')
				{
					$schedule->command('Cron:SendAppointmentEmail')->cron('0 0 */15 * * *');
					$schedule->command('Cron:SendNewLeadEmail')->cron('0 0 */15 * * *');
                    $schedule->command('Cron:SendAutoNotifications')->cron('0 0 */15 * * *');
				}
				elseif($admin->receive_notification == 'Monthly')
				{
					$schedule->command('Cron:SendAppointmentEmail')->monthly();
					$schedule->command('Cron:SendNewLeadEmail')->monthly();
                    $schedule->command('Cron:SendAutoNotifications')->monthly();
				}
			}
		}
		
		/**
			* Register the Closure based commands for the application.
			*
			* @return void
		*/
		protected function commands()
		{
			require base_path('routes/console.php');
		}
	}
