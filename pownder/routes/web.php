<?php
	/*
		|--------------------------------------------------------------------------
		| Web Routes
		|--------------------------------------------------------------------------
		|
		| This file is where you may define all of the routes that are handled
		| by your application. Just tell Laravel the URIs it should respond
		| to using a Closure or controller method. Build something great!
		|
	*/
	
	Route::get('/',	['as'=>'/',	'uses'=>'Panel\MainController@ShowLoginPage']);
	Route::get('/login',	['as'=>'/login',	'uses'=>'Panel\MainController@ShowLoginPage']);
	Route::get('/facebook_login_redirect',	['as'=>'/facebook_login_redirect',	'uses'=>'Facebook\FacebookController@getFacebookLogin']);    
    
	Route::post('submit-login',['as'=>'submit-login','uses'=>'Panel\MainController@checkLogin']);
	Route::get('login-validate',['as'=>'login-validate','uses'=>'Panel\MainController@checkLoginValidate']);
	Route::get('forgot-password',['as'=>'forgot-password','uses'=>'Panel\MainController@forgotPassword']);
	Route::post('forgot-password-submit',['as'=>'forgot-password-submit','uses'=>'Panel\MainController@forgotPasswordSubmit']);
	Route::post('reset-password-submit',['as'=>'reset-password-submit','uses'=>'Panel\MainController@resetPasswordSubmit']);
	
	//If User first time login
	Route::get('password_reset',	['as'=>'password_reset',	'uses'=>'Panel\MainController@getResetUserPassword']);
	Route::post('password_reset',	['as'=>'password_reset',	'uses'=>'Panel\MainController@postResetUserPassword']);
	
	//Error Handling
	Route::get('Error401',	['as'=>'Error401',	'uses'=>'Panel\CommonController@showError401']);
	
	Route::group(['middleware'=>'checklogin'],function(){  
		
		Route::get('logout',['as'=>'logout','uses'=>'Panel\MainController@logout']);
		Route::get('dashboard',['as'=>'dashboard','uses'=>'Panel\MainController@dashboard']);
		
		Route::post('add-group-submit',['as'=>'add-group-submit','uses'=>'Panel\SettingController@addGroupSubmit']);
		Route::post('edit-group-submit',['as'=>'edit-group-submit','uses'=>'Panel\SettingController@editGroupSubmit']);
		Route::post('delete-group',['as'=>'delete-group','uses'=>'Panel\SettingController@deleteGroup']);
		Route::get('add-user',['as'=>'add-user','uses'=>'Panel\MainController@addUser']);
		Route::post('delete',['as'=>'delete','uses'=>'Panel\MainController@delete']);
		Route::get('edit-user',['as'=>'edit-user','uses'=>'Panel\MainController@editUser']);
		Route::get('user-profile',['as'=>'user-profile','uses'=>'Panel\MainController@userProfile']);
		Route::get('lockscreen',['as'=>'lockscreen','uses'=>'Panel\MainController@lockscreen']);
		Route::get('fb-messenger',['as'=>'fb-messenger','uses'=>'Panel\MainController@fbMessenger']);
		Route::get('settings',['as'=>'settings','uses'=>'Panel\SettingController@getSettings']);
		Route::get('check-client-lead-form', ['as'=>'check-client-lead-form', 'uses'=>'Panel\SettingController@checkClientLeadForm']);
		Route::get('lead-form-list', ['as'=>'lead-form-list', 'uses'=>'Panel\SettingController@getLeadForm']);
		Route::get('block-lead-form', ['as'=>'block-lead-form', 'uses'=>'Panel\SettingController@blockLeadform']);
		Route::get('facebook-page-list', ['as'=>'facebook-page-list', 'uses'=>'Panel\SettingController@getFacebookPage']);
		Route::get('facebook-page-hide', ['as'=>'facebook-page-hide', 'uses'=>'Panel\SettingController@hideFacebookPage']);
		Route::get('email-notification',	['as'=>'email-notification',	'uses'=>'Panel\SettingController@postEmailNotification']);
		Route::get('user-email-notification',	['as'=>'user-email-notification',	'uses'=>'Panel\SettingController@getUserEmailNotification']);
		Route::get('check-email-notification',	['as'=>'check-email-notification',	'uses'=>'Panel\SettingController@checkEmailNotification']);
		Route::post('user_email_notification',	['as'=>'user_email_notification',	'uses'=>'Panel\SettingController@postUserEmailNotification']);
		Route::get('group',	['as'=>'group',	'uses'=>'Panel\SettingController@getGroupDetails']);
		Route::get('csettings',['as'=>'csettings','uses'=>'Panel\SettingController@getClientSettings']);
		Route::get('user-profile',['as'=>'user-profile','uses'=>'Panel\MainController@userProfile']);
		Route::get('/calltrack',['as'=>'calltrack','uses'=>'Panel\MainController@Calltrack']);
		Route::get('/manage-numbers',['as'=>'manage-numbers','uses'=>'Panel\MainController@manageNumbers']);
		
		Route::get('page/{form}', 'CorePlusController@showView');
		
		//----------------------------------------('Rakesh Kashyap Start')-------------------------------------------------
		Route::get('facebook_redirect',		['as'=>'facebook_redirect',		'uses'=>'Facebook\FacebookController@getFacebookRedirect']);
		Route::get('edit-admin',				['as'=>'edit-admin',				'uses'=>'Panel\MainController@editAdmin']);
		Route::post('admin-edit-profile',		['as'=>'admin-edit-profile',		'uses'=>'Panel\MainController@editAdminProfile']);
		Route::post('admin-change-password',	['as'=>'admin-change-password',	'uses'=>'Panel\MainController@changePassword']);
		
		Route::get('admin-dashboard-ajax', ['as'=>'admin-dashboard-ajax',	'uses'=>'Panel\MainController@getAdminDashboardAjax']);
		Route::get('admin-dashboard-client', ['as'=>'admin-dashboard-client',	'uses'=>'Panel\MainController@getAdminDashboardClient']);
		Route::get('vendor-dashboard-ajax', ['as'=>'vendor-dashboard-ajax',	'uses'=>'Panel\VendorController@getVendorDashboardAjax']);
		Route::get('vendor-dashboard-client', ['as'=>'vendor-dashboard-client',	'uses'=>'Panel\VendorController@getVendorDashboardClient']);
		Route::get('client-dashboard-ajax', ['as'=>'client-dashboard-ajax',	'uses'=>'Panel\ClientController@getClientDashboardAjax']);
		
		//client package
		Route::post('add-package-submit',	['as'=>'add-package-submit',	'uses'=>'Panel\SettingController@addPackageSubmit']);
		Route::post('edit-package-submit',	['as'=>'edit-package-submit',	'uses'=>'Panel\SettingController@editPackageSubmit']);
		Route::post('delete-package',	['as'=>'delete-package',	'uses'=>'Panel\SettingController@deletePackage']);
		Route::get('package-client-detail',	['as'=>'package-client-detail',	'uses'=>'Panel\SettingController@getPackageClientDetail']);
		Route::get('client_package_list',	['as'=>'client_package_list',	'uses'=>'Panel\SettingController@getClientPackageList']);
		
		//client
		Route::get('cdash',	['as'=>'cdash',	'uses'=>'Panel\ClientController@getClientDashboard']);
		Route::get('cleads',	['as'=>'cleads',	'uses'=>'Facebook\LeadController@getClientLeads']);
		Route::get('client-lead-search',	['as'=>'client-lead-search',	'uses'=>'Facebook\LeadController@getClientSearchLeads']);
		Route::post('dispute_lead',	['as'=>'dispute_lead',	'uses'=>'Facebook\LeadController@postDisputeLead']);
		Route::post('add_crm_account',	['as'=>'add_crm_account',	'uses'=>'Panel\ClientController@addCRMAccount']);
		Route::get('crm_account/{client_id}',	['as'=>'crm_account',	'uses'=>'Panel\ClientController@getCRMAccount']);
		Route::get('crm_account_status',	['as'=>'crm_account_status',	'uses'=>'Panel\ClientController@changeCRMAccountStatus']);
		Route::get('client_crm_account',	['as'=>'client_crm_account',	'uses'=>'Panel\CommonController@ClientCRMAccount']);
		Route::get('client_campaign',	['as'=>'client_campaign',	'uses'=>'Panel\CommonController@ClientCampaign']);
		Route::post('user_client_allot',	['as'=>'user_client_allot',	'uses'=>'Panel\ClientController@postUserClientAllot']);
		Route::get('campaign_crm_leads',	['as'=>'campaign_crm_leads',	'uses'=>'Facebook\CampaignController@getCampaignCRMLead']);
		
		//Clients
		Route::get('clients',	['as'=>'clients',	'uses'=>'Panel\ClientController@getClientList']);
		Route::get('client-ajax',	['as'=>'client-ajax',	'uses'=>'Panel\ClientController@getClientSearch']);
		Route::post('add-client-submit',	['as'=>'add-client-submit',	'uses'=>'Panel\ClientController@addClientSubmit']);
		Route::post('edit-client-submit',	['as'=>'edit-client-submit',	'uses'=>'Panel\ClientController@editClientSubmit']);
		Route::get('delete-client',	['as'=>'delete-client',	'uses'=>'Panel\ClientController@deleteClient']);
		Route::get('active-client',	['as'=>'active-client',	'uses'=>'Panel\ClientController@activeClient']);
		Route::get('inactive-client',	['as'=>'inactive-client',	'uses'=>'Panel\ClientController@inactiveClient']);
		Route::post('active-bulk-client',	['as'=>'active-bulk-client',	'uses'=>'Panel\ClientController@activeBulkClient']);
		Route::post('inactive-bulk-client',	['as'=>'inactive-bulk-client',	'uses'=>'Panel\ClientController@inactiveBulkClient']);
		
		Route::get('edit-client',	['as'=>'edit-client',	'uses'=>'Panel\ClientController@editClient']);
		Route::post('client-edit-profile',	['as'=>'client-edit-profile',	'uses'=>'Panel\ClientController@editClientProfile']);
		Route::post('client-change-password',	['as'=>'client-change-password',	'uses'=>'Panel\ClientController@changePassword']);
		Route::get('check-package-type',	['as'=>'check-package-type',	'uses'=>'Panel\CommonController@checkPackageType']);
		Route::get('check-client-discount',	['as'=>'check-client-discount',	'uses'=>'Panel\CommonController@checkClientDiscount']);
		
		Route::get('client-current-month-leads',	['as'=>'client-current-month-leads',	'uses'=>'Panel\CommonController@GetClientCurrentMonthLeads']);
		Route::get('client-total-leads',	['as'=>'client-total-leads', 'uses'=>'Panel\CommonController@GetClientTotalLeads']);
		Route::get('client-header-filter',	['as'=>'client-header-filter', 'uses'=>'Panel\CommonController@setClientHeaderFilter']);
		Route::get('date-header-filter',	['as'=>'date-header-filter', 'uses'=>'Panel\CommonController@setDateHeaderFilter']);
		
		//vendor package
		Route::post('add-vendor-package-submit',	['as'=>'add-vendor-package-submit',	'uses'=>'Panel\SettingController@addVendorPackageSubmit']);
		Route::post('edit-vendor-package-submit',	['as'=>'edit-vendor-package-submit',	'uses'=>'Panel\SettingController@editVendorPackageSubmit']);
		Route::post('delete-vendor-package',	['as'=>'delete-vendor-package',	'uses'=>'Panel\SettingController@deleteVendorPackage']);
		Route::get('package-vendor-detail',	['as'=>'package-vendor-detail',	'uses'=>'Panel\SettingController@getPackageVendorDetail']);
		
		
		//vendor
		Route::get('vdash',	['as'=>'vdash',	'uses'=>'Panel\VendorController@getVendorDashboard']);
		Route::get('vendors',	['as'=>'vendors',	'uses'=>'Panel\VendorController@getVendor']);
		Route::get('vendor-list-ajax',	['as'=>'vendor-list-ajax',	'uses'=>'Panel\VendorController@getVendorAjax']);
		Route::post('add-vendor-submit',	['as'=>'add-vendor-submit',	'uses'=>'Panel\VendorController@addVendorSubmit']);
		Route::post('edit-vendor-submit',	['as'=>'edit-vendor-submit',	'uses'=>'Panel\VendorController@editVendorSubmit']);
		Route::get('delete-vendor',	['as'=>'delete-vendor',	'uses'=>'Panel\VendorController@deleteVendor']);
		Route::get('edit-vendor',	['as'=>'edit-vendor',	'uses'=>'Panel\VendorController@editVendor']);
		Route::post('vendor-edit-profile',	['as'=>'vendor-edit-profile',	'uses'=>'Panel\VendorController@editVendorProfile']);
		Route::post('vendor-change-password',	['as'=>'vendor-change-password',	'uses'=>'Panel\VendorController@changePassword']);
		Route::get('get-vendor',	['as'=>'get-vendor',	'uses'=>'Panel\VendorController@getVendorName']);
		Route::get('get-client',	['as'=>'get-client',	'uses'=>'Panel\CommonController@getAdminClient']);
		Route::get('get-vendor-client',	['as'=>'get-vendor-client',	'uses'=>'Panel\CommonController@getVendorClient']);
		Route::post('active-bulk-vendor',	['as'=>'active-bulk-vendor',	'uses'=>'Panel\VendorController@activeBulkVendor']);
		Route::post('inactive-bulk-vendor',	['as'=>'inactive-bulk-vendor',	'uses'=>'Panel\VendorController@inactiveBulkVendor']);
		
		Route::get('create_group_category',	['as'=>'create_group_category',	'uses'=>'Panel\SettingController@createGroupCategory']);
		Route::get('create_group_sub_category',	['as'=>'create_group_sub_category',	'uses'=>'Panel\SettingController@createGroupSubCategory']);
		Route::post('create_group_client',	['as'=>'create_group_client',	'uses'=>'Panel\SettingController@createGroupClient']);
		Route::get('get_group_sub_category',	['as'=>'get_group_sub_category',	'uses'=>'Panel\CommonController@getGroupSubCategory']);
		Route::get('client_user_list',	['as'=>'client_user_list',	'uses'=>'Panel\CommonController@getClientUserList']);
		Route::get('get_group_client',	['as'=>'get_group_client',	'uses'=>'Panel\SettingController@getGroupClient']);
		Route::get('group-client-detail',	['as'=>'group-client-detail',	'uses'=>'Panel\SettingController@getGroupClientDetail']);
		
		//leads
		Route::get('leads',	['as'=>'leads',	'uses'=>'Facebook\LeadController@getLeads']);
		Route::get('appointment-filter',	['as'=>'appointment-filter',	'uses'=>'Facebook\LeadController@getClientAppointmentFilters']);
		Route::get('filter-appointment',	['as'=>'filter-appointment',	'uses'=>'Facebook\LeadController@getClientFilterAppointment']);
		Route::get('lead-search',	['as'=>'lead-search',	'uses'=>'Facebook\LeadController@getSearchLeads']);
		Route::get('get_client_name',	['as'=>'get_client_name',	'uses'=>'Facebook\LeadController@getClientName']);
		Route::get('client_roi_status',	['as'=>'client_roi_status',	'uses'=>'Facebook\LeadController@getClientROIStatus']);
		Route::post('lead_sold',	['as'=>'lead_sold',	'uses'=>'Facebook\LeadController@postLeadSold']);
		Route::post('client_lead_sold',	['as'=>'client_lead_sold',	'uses'=>'Facebook\LeadController@postClientLeadSold']);
		Route::get('lead_re-assign_client',	['as'=>'lead_re-assign_client',	'uses'=>'Facebook\LeadController@getLeadReAssignClient']);
		Route::post('lead_re-assign',	['as'=>'lead_re-assign',	'uses'=>'Facebook\LeadController@postLeadReAssign']);
		Route::get('lead_re-sync',	['as'=>'lead_re-sync',	'uses'=>'Facebook\LeadController@getLeadReSync']);
		Route::post('multiple-lead-re-sync',	['as'=>'multiple-lead-re-sync',	'uses'=>'Facebook\LeadController@postMultipleLeadReSync']);
		Route::post('user_lead_allot',	['as'=>'user_lead_allot',	'uses'=>'Facebook\LeadController@postUserLeadAllot']);
		Route::post('client_user_lead_allot',	['as'=>'client_user_lead_allot',	'uses'=>'Facebook\LeadController@postClientUserLeadAllot']);
		Route::post('legal_lead',	['as'=>'legal_lead',	'uses'=>'Facebook\LeadController@postLegalLead']);
		Route::post('default_lead',	['as'=>'default_lead',	'uses'=>'Facebook\LeadController@postDefaultLead']);
		Route::post('client_default_lead',	['as'=>'client_default_lead',	'uses'=>'Facebook\LeadController@postClientDefaultLead']);
		Route::post('lead_setting',	['as'=>'lead_setting',	'uses'=>'Facebook\LeadController@postLeadSetting']);
		Route::post('legal_lead_setting',	['as'=>'legal_lead_setting',	'uses'=>'Facebook\LeadController@postLegalLeadSetting']);
		Route::post('appointment_lead_setting',	['as'=>'appointment_lead_setting',	'uses'=>'Facebook\LeadController@postAppointmentLeadSetting']);
		Route::post('lead_add',	['as'=>'lead_add',	'uses'=>'Facebook\LeadController@postLeadAdd']);
		Route::get('lead_edit',	['as'=>'lead_edit',	'uses'=>'Facebook\LeadController@getLeadEdit']);
		Route::post('lead_edit',	['as'=>'lead_edit',	'uses'=>'Facebook\LeadController@postLeadEdit']);
		Route::post('dispute-lead-edit',	['as'=>'dispute-lead-edit',	'uses'=>'Facebook\LeadController@postDisputeLeadEdit']);
		Route::get('dispute-lead-list',	['as'=>'dispute-lead-list',	'uses'=>'Facebook\LeadController@getDisputeLeads']);
		Route::get('phone-number-hide',	['as'=>'phone-number-hide',	'uses'=>'Facebook\LeadController@hidePhoneNumber']);
		
		//Campaigns
		Route::get('campaigns',	['as'=>'campaigns',	'uses'=>'Facebook\CampaignController@getCampaign']);
		Route::get('campaign-ajax',	['as'=>'campaign-ajax',	'uses'=>'Facebook\CampaignController@getCampaignAjax']);
		Route::get('campaign_add',	['as'=>'campaign_add',	'uses'=>'Facebook\CampaignController@getAddCampaign']);
		Route::get('facebook_ads_form',	['as'=>'facebook_ads_form',	'uses'=>'Panel\CommonController@getfacebookAdsForm']);
		Route::get('client_managers',	['as'=>'client_managers',	'uses'=>'Panel\CommonController@getClientManagers']);
		Route::get('client_info',	['as'=>'client_info',	'uses'=>'Panel\CommonController@getClientInfo']);
		Route::post('campaign_add',	['as'=>'campaign_add',	'uses'=>'Facebook\CampaignController@postAddCampaign']);
		Route::post('campaign_add1',	['as'=>'campaign_add1',	'uses'=>'Facebook\CampaignController@postAddCampaign1']);
		Route::get('campaign_edit/{id}',	['as'=>'campaign_edit',	'uses'=>'Facebook\CampaignController@getEditCampaign']);
		Route::post('campaign_edit',	['as'=>'campaign_edit',	'uses'=>'Facebook\CampaignController@postEditCampaign']);
		Route::post('campaign_edit1',	['as'=>'campaign_edit1',	'uses'=>'Facebook\CampaignController@postEditCampaign1']);
		Route::get('campaign_status',	['as'=>'campaign_status',	'uses'=>'Facebook\CampaignController@changeCampaignStatus']);
		Route::post('campaign_delete',	['as'=>'campaign_delete',	'uses'=>'Facebook\CampaignController@postCampaignDelete']);
		Route::post('campaign_clone',	['as'=>'campaign_clone',	'uses'=>'Facebook\CampaignController@postCampaignClone']);
		Route::get('campaign_lead/{id}',	['as'=>'campaign_lead',	'uses'=>'Facebook\CampaignController@getCampaignLead']);
		Route::get('crm_contact/{id}',	['as'=>'crm_contact',	'uses'=>'Facebook\CampaignController@getCRMAccountLead']);
		Route::get('create-test-adf-xml',	['as'=>'create-test-adf-xml',	'uses'=>'Facebook\CampaignController@createTestADFXML']);
		Route::get('create-manual-adf-xml',	['as'=>'create-manual-adf-xml',	'uses'=>'Facebook\CampaignController@createManualADFXML']);
		Route::get('campaign-publish',	['as'=>'campaign-publish',	'uses'=>'Facebook\CampaignController@publishCampaign']);
		Route::get('campaign-publish-success',	['as'=>'campaign-publish-success',	'uses'=>'Facebook\CampaignController@publishCampaignSuccess']);
		Route::get('user-email-exists',	['as'=>'user-email-exists',	'uses'=>'Facebook\CampaignController@checkUserEmail']);
		Route::get('user-email-update',	['as'=>'user-email-update',	'uses'=>'Facebook\CampaignController@updateUserEmail']);
		Route::get('user-details',	['as'=>'user-details',	'uses'=>'Facebook\CampaignController@getUserdetails']);
		
		//Reports
		Route::get('reports',	['as'=>'reports',	'uses'=>'Panel\ReportController@getReport']);
		Route::get('profit-reports',	['as'=>'profit-reports',	'uses'=>'Panel\ReportController@getProfitReport']);
		Route::get('clients-roi',	['as'=>'clients-roi',	'uses'=>'Panel\ReportController@getClientROIReport']);
		Route::get('reports-ajax',	['as'=>'reports-ajax',	'uses'=>'Panel\ReportController@getReportFilter']);
		Route::get('sold_lead_edit',	['as'=>'sold_lead_edit',	'uses'=>'Panel\ReportController@getLeadSold']);
		Route::post('sold_lead_edit',	['as'=>'sold_lead_edit',	'uses'=>'Panel\ReportController@postLeadSold']);
		Route::get('sold_client_roi_status',	['as'=>'sold_client_roi_status',	'uses'=>'Panel\ReportController@getSoldClientROIStatus']);
		
		Route::get('test',	['as'=>'test',	'uses'=>'Facebook\TestController@getTest']);
		Route::get('test1',	['as'=>'test1',	'uses'=>'Facebook\TestController@getTest1']);
		Route::get('test2',	['as'=>'test2',	'uses'=>'Facebook\TestController@getTest2']);
		Route::get('table_visible_column',	['as'=>'table_visible_column',	'uses'=>'Panel\CommonController@setTableVisibleColumn']);
		Route::get('remove_facebook_account',	['as'=>'remove_facebook_account',	'uses'=>'Panel\CommonController@removeFacebookAccount']);
		
		//User
		Route::get('user',	['as'=>'user',	'uses'=>'Panel\UserController@getAdminUser']);
		Route::get('admin-user-ajax',	['as'=>'admin-user-ajax',	'uses'=>'Panel\UserController@getAdminUserAjax']);
		Route::post('user_add_validation',	['as'=>'user_add_validation',	'uses'=>'Panel\UserController@postAdminUserAddValidation']);
		Route::get('user_add',	['as'=>'user_add',	'uses'=>'Panel\UserController@getAdminUserAdd']);
		Route::post('user_add',	['as'=>'user_add',	'uses'=>'Panel\UserController@postAdminUserAdd']);
		Route::post('user_edit_validation',	['as'=>'user_edit_validation',	'uses'=>'Panel\UserController@postAdminUserEditValidation']);
		Route::get('user_edit/{id}',	['as'=>'user_edit',	'uses'=>'Panel\UserController@getAdminUserEdit']);
		Route::post('user_edit',	['as'=>'user_edit',	'uses'=>'Panel\UserController@postAdminUserEdit']);
		Route::get('vuser',	['as'=>'vuser',	'uses'=>'Panel\UserController@getVendorUser']);
		Route::get('vendor-user-ajax',	['as'=>'vendor-user-ajax',	'uses'=>'Panel\UserController@getVendorUserAjax']);
		Route::get('vuser_add',	['as'=>'vuser_add',	'uses'=>'Panel\UserController@getVendorUserAdd']);
		Route::post('vuser_add',	['as'=>'vuser_add',	'uses'=>'Panel\UserController@postVendorUserAdd']);
		Route::get('vuser_edit/{id}',	['as'=>'vuser_edit',	'uses'=>'Panel\UserController@getVendorUserEdit']);
		Route::post('vuser_edit',	['as'=>'vuser_edit',	'uses'=>'Panel\UserController@postVendorUserEdit']);
		Route::get('cuser',	['as'=>'cuser',	'uses'=>'Panel\UserController@getClientUser']);
		Route::get('client-user-ajax',	['as'=>'client-user-ajax',	'uses'=>'Panel\UserController@getClientUserAjax']);
		Route::get('cuser_add',	['as'=>'cuser_add',	'uses'=>'Panel\UserController@getClientUserAdd']);
		Route::post('cuser_add',	['as'=>'cuser_add',	'uses'=>'Panel\UserController@postClientUserAdd']);
		Route::get('cuser_edit/{id}',	['as'=>'cuser_edit',	'uses'=>'Panel\UserController@getClientUserEdit']);
		Route::post('cuser_edit',	['as'=>'cuser_edit',	'uses'=>'Panel\UserController@postClientUserEdit']);
		Route::get('create_team',	['as'=>'create_team',	'uses'=>'Panel\CommonController@createTeam']);
		Route::get('create_job_title',	['as'=>'create_job_title',	'uses'=>'Panel\CommonController@createJobTitle']);
		Route::get('create_department',	['as'=>'create_department',	'uses'=>'Panel\CommonController@createDepartment']);
		Route::get('job_title_menu',	['as'=>'job_title_menu',	'uses'=>'Panel\CommonController@getJobTitleMenu']);
		Route::get('job_title_permission',	['as'=>'job_title_permission',	'uses'=>'Panel\CommonController@getJobTitlePermission']);
		Route::get('create_expense_category',	['as'=>'create_expense_category',	'uses'=>'Panel\CommonController@createExpenseCategory']);
		
		Route::get('delete-user',	['as'=>'delete-user',	'uses'=>'Panel\UserController@deleteUser']);
		Route::get('active-user',	['as'=>'active-user',	'uses'=>'Panel\UserController@activeUser']);
		Route::post('active-bulk-user',	['as'=>'active-bulk-user',	'uses'=>'Panel\UserController@activeBulkUser']);
		Route::post('inactive-bulk-user',	['as'=>'inactive-bulk-user',	'uses'=>'Panel\UserController@inactiveBulkUser']);
		Route::get('inactive-user',	['as'=>'inactive-user',	'uses'=>'Panel\UserController@inactiveUser']);
		
		
		
		Route::get('user-lead-notifier',	['as'=>'user-lead-notifier',	'uses'=>'Panel\SettingController@postUserLeadNotifier']);
		Route::get('all-user-lead-notifier',	['as'=>'all-user-lead-notifier',	'uses'=>'Panel\SettingController@postAllUserLeadNotifier']);
		
		Route::get('user-appointment-notifier',	['as'=>'user-appointment-notifier',	'uses'=>'Panel\SettingController@postUserAppointmentNotifier']);
		Route::get('all-user-appointment-notifier',	['as'=>'all-user-appointment-notifier',	'uses'=>'Panel\SettingController@postAllUserAppointmentNotifier']);
		
		Route::get('edit-user',	['as'=>'edit-user',	'uses'=>'Panel\UserController@editUser']);
		Route::post('user-edit-profile',	['as'=>'user-edit-profile',	'uses'=>'Panel\UserController@editUserProfile']);
		Route::post('user-change-password',	['as'=>'user-change-password',	'uses'=>'Panel\UserController@changePassword']);
		
		//FTP Account
		Route::get('ftp-account',	['as'=>'ftp-account',	'uses'=>'Facebook\FTPController@getFTPAccount']);
		Route::get('add-ftp-account',	['as'=>'add-ftp-account',	'uses'=>'Facebook\FTPController@getAddFTPAccount']);
		Route::post('validate-add-ftp-account',	['as'=>'validate-add-ftp-account',	'uses'=>'Facebook\FTPController@validateAddFTPAccount']);
		Route::post('add-ftp-account',	['as'=>'add-ftp-account',	'uses'=>'Facebook\FTPController@postAddFTPAccount']);
		Route::get('edit-ftp-account/{id}',	['as'=>'edit-ftp-account',	'uses'=>'Facebook\FTPController@getEditFTPAccount']);
		Route::post('edit-ftp-account',	['as'=>'edit-ftp-account',	'uses'=>'Facebook\FTPController@postEditFTPAccount']);
		Route::get('delete-ftp-account',	['as'=>'delete-ftp-account',	'uses'=>'Facebook\FTPController@deleteFTPAccount']);
		Route::get('active-ftp-account',	['as'=>'active-ftp-account',	'uses'=>'Facebook\FTPController@activeFTPAccount']);
		Route::get('inactive-ftp-account',	['as'=>'inactive-ftp-account',	'uses'=>'Facebook\FTPController@inactiveFTPAccount']);
		
		//Expense
		Route::get('expenses',	['as'=>'expenses',	'uses'=>'Panel\ExpenseController@getExpense']);
		Route::get('delete-expense',	['as'=>'delete-expense',	'uses'=>'Panel\ExpenseController@deleteExpense']);
		Route::post('validate-add-expense',	['as'=>'validate-add-expense',	'uses'=>'Panel\ExpenseController@validateAddExpense']);
		Route::post('add-expense',	['as'=>'add-expense',	'uses'=>'Panel\ExpenseController@postAddExpense']);
		Route::get('edit-expense',	['as'=>'edit-expense',	'uses'=>'Panel\ExpenseController@getEditExpense']);
		Route::post('edit-expense',	['as'=>'edit-expense',	'uses'=>'Panel\ExpenseController@postEditExpense']);
        
        Route::group(['middleware'=>['CheckAndAllowLogins:admin,vendor']],function(){
            
        //invoice
		Route::get('invoice',	['as'=>'invoice',	'uses'=>'Panel\InvoiceController@getInvoice']);
        Route::post('get_clients_invoice',	['as'=>'get_clients_invoice',	'uses'=>'Panel\InvoiceController@getClientsInvoice']);
		Route::post('invoice',	['as'=>'invoice',	'uses'=>'Panel\InvoiceController@postInvoice']);
        Route::post('download-invoice-pdf',	['as'=>'download-invoice-pdf',	'uses'=>'Panel\InvoiceController@downloadInvoiceAsPdf']);
        Route::post('delete-tmp-file',	['as'=>'delete-tmp-file',	'uses'=>'Panel\InvoiceController@deleteTmpFile']);
        Route::post('save-invoice-draft',	['as'=>'save-invoice-draft',	'uses'=>'Panel\InvoiceController@saveInvoiceAsDraft']);
        Route::post('update-invoice-draft',	['as'=>'update-invoice-draft',	'uses'=>'Panel\InvoiceController@updateInvoiceDraft']);
        Route::get('manage-invoice',	['as'=>'manage-invoice',	'uses'=>'Panel\InvoiceController@manageInvoice']);
        Route::get('draft-invoice/{id}',	['as'=>'draft-invoice',	'uses'=>'Panel\InvoiceController@draftInvoiceContinue']);
        Route::get('view-invoice/{type}/{id}',	['as'=>'view-invoice',	'uses'=>'Panel\InvoiceController@ViewInvoice']);
        Route::get('delete-saved-item/{id}',	['as'=>'delete-saved-item',	'uses'=>'Panel\InvoiceController@deleteSavedItem']);
        Route::post('get-client-info',	['as'=>'get-client-info',	'uses'=>'Panel\InvoiceController@getClientInfo']);
        Route::post('update-recurring-status',	['as'=>'update-recurring-status',	'uses'=>'Panel\InvoiceController@updateRecurringStatus']);
        Route::post('update-recurring-invoice',	['as'=>'update-recurring-invoice',	'uses'=>'Panel\InvoiceController@updateRecurringInvoice']);
        Route::get('invoice-report',	['as'=>'invoice-report',	'uses'=>'Panel\InvoiceController@invoiceReport']);
        Route::post('filter-invoices',	['as'=>'filter-invoices',	'uses'=>'Panel\InvoiceController@filterInvoices']);
        Route::post('filter-sales-by-day',	['as'=>'filter-sales-by-day',	'uses'=>'Panel\InvoiceController@filterSalesByDay']);
        Route::post('filter-sales-by-month',	['as'=>'filter-sales-by-month',	'uses'=>'Panel\InvoiceController@filterSalesByMonth']);
        Route::post('filter-sales-by-quarter',	['as'=>'filter-sales-by-quarter',	'uses'=>'Panel\InvoiceController@filterSalesByQuarter']);
        Route::post('filter-sales-by-year',	['as'=>'filter-sales-by-year',	'uses'=>'Panel\InvoiceController@filterSalesByYear']);
        Route::post('filter-sales-by-customer',	['as'=>'filter-sales-by-customer',	'uses'=>'Panel\InvoiceController@filterSalesByCustomer']);
        Route::post('filter-sales-by-item',	['as'=>'filter-sales-by-item',	'uses'=>'Panel\InvoiceController@filterSalesByItem']);
        Route::post('get-saved-item',	['as'=>'get-saved-item',	'uses'=>'Panel\InvoiceController@getSavedItems']);
        Route::post('mark-invoice-paid',	['as'=>'mark-invoice-paid',	'uses'=>'Panel\InvoiceController@markInvoicePaid']);
        Route::post('load-charts',	['as'=>'load-charts',	'uses'=>'Panel\InvoiceController@loadChartsType']);
        Route::post('invoice-send-reminder',	["as"=>"invoice-send-reminder",	"uses"=>"Panel\InvoiceController@sendReminder"]);
        Route::post('update-payment-tnc',	["as"=>"update-payment-tnc",	"uses"=>"Panel\SettingController@updatePaymentTNC"]);
        Route::post('update-invoice-setting',	["as"=>"update-invoice-setting",	"uses"=>"Panel\SettingController@updateInvoiceSetting"]);
        Route::get('edit-sent-invoice/{id}',	['as'=>'edit-sent-invoice',	'uses'=>'Panel\InvoiceController@editSentInvoice']);
        Route::post('delete-invoice',	['as'=>'delete-invoice',	'uses'=>'Panel\InvoiceController@deleteInvoice']);
        Route::post('clone-invoice',	['as'=>'clone-invoice',	'uses'=>'Panel\InvoiceController@cloneInvoice']);
        Route::get('mark_read_notification/{type}/{id}',['as'=>'mark_read_notification','uses'=>'Panel\InvoiceController@MarkReadNotification']);
        
		});
        
        //Closed Deals        
        Route::get('closed-deals',	['as'=>'closed-deals',	'uses'=>'Panel\FTPCSVRWController@showClosedDeals']);
        Route::get('get-closed-deals/{type}',	['as'=>'get-closed-deals',	'uses'=>'Panel\FTPCSVRWController@getClosedDeals']);
        Route::get('totals-closed-deals',	['as'=>'totals-closed-deals',	'uses'=>'Panel\FTPCSVRWController@getDepartmentWiseData']);
        Route::post('fetch-closed-deals',  ['as'=>'fetch-closed-deals',	'uses'=>'Panel\SettingController@fetchClosedDeals']);
        Route::post('update-closed-deals-setting',	['as'=>'update-closed-deals-setting',	'uses'=>'Panel\SettingController@updateClosedDealsSetting']);
        Route::get('view-closed-deal/{id}',	['as'=>'view-closed-deal',	'uses'=>'Panel\FTPCSVRWController@viewClosedDeal']);
        
		//Appointment
        Route::get('appointments',	['as'=>'appointments',	'uses'=>'Panel\AppointmentController@getAppointments']);
		Route::get('appointment-client',	['as'=>'appointment-client',	'uses'=>'Panel\AppointmentController@getClientAppointment']);
		Route::get('appointment-summary',	['as'=>'appointment-summary',	'uses'=>'Panel\AppointmentController@getAppointmentSummary']);
		Route::get('appointment-delete',	['as'=>'appointment-delete',	'uses'=>'Panel\AppointmentController@deleteAppointment']);
		Route::get('appointment-cancel',	['as'=>'appointment-cancel',	'uses'=>'Panel\AppointmentController@cancelAppointment']);
		Route::get('appointment-sold',	['as'=>'appointment-sold',	'uses'=>'Panel\AppointmentController@soldAppointment']);
		Route::get('appointment-shown',	['as'=>'appointment-shown',	'uses'=>'Panel\AppointmentController@shownAppointment']);
		Route::get('appointment-reschedule',	['as'=>'appointment-reschedule',	'uses'=>'Panel\AppointmentController@rescheduleAppointment']);
		Route::get('reschedule-appointment',	['as'=>'reschedule-appointment',	'uses'=>'Panel\AppointmentController@appointmentReschedule']);
		Route::post('event-add',	["as"=>"event-add",	"uses"=>"Panel\AppointmentController@postEvents"]);
		Route::get('event-appointment',	["as"=>"event-appointment",	"uses"=>"Panel\AppointmentController@postAppointment"]);
		Route::get('update-appointment',	["as"=>"update-appointment",	"uses"=>"Panel\AppointmentController@updateAppointment"]);
		Route::get('event-remove',	["as"=>"event-remove",	"uses"=>"Panel\AppointmentController@removeEvent"]);
		Route::get('remove-re-sync-error',	["as"=>"remove-re-sync-error",	"uses"=>"Panel\AppointmentController@removeResyncError"]);
		Route::get('event-lead-search',	["as"=>"event-lead-search",	"uses"=>"Panel\AppointmentController@searchEventLeads"]);
		Route::get('lead-details',	["as"=>"lead-details",	"uses"=>"Panel\AppointmentController@getLeadDetails"]);
        
        Route::group(['middleware'=>'CheckAndAllowLogins:admin','namespace'=>'Panel'],function(){
            
        //Manage Questions
        Route::get('questions',	['as'=>'questions',	'uses'=>'QuestionsController@ManageQuestions']);
        Route::post('add-question',	['as'=>'add-question',	'uses'=>'QuestionsController@AddQuestion']);
        Route::post('delete-question',	['as'=>'delete-question',	'uses'=>'QuestionsController@DeleteQuestion']);
        Route::post('edit-question',	['as'=>'edit-question',	'uses'=>'QuestionsController@EditQuestion']);
        Route::post('enable-question',	['as'=>'enable-question',	'uses'=>'QuestionsController@EnableQuestion']);
        Route::post('change-question-sequence',	['as'=>'change-question-sequence',	'uses'=>'QuestionsController@ChangeQuestionSequence']);
        
        //Manage Answers
        Route::get('answers',	['as'=>'answers',	'uses'=>'QuestionsController@ManageAnswers']);
        Route::post('add-answer',	['as'=>'add-answer',	'uses'=>'QuestionsController@AddAnswer']);
        Route::post('delete-answer',	['as'=>'delete-answer',	'uses'=>'QuestionsController@DeleteAnswer']);
        Route::post('edit-answer',	['as'=>'edit-answer',	'uses'=>'QuestionsController@EditAnswer']);
        Route::post('enable-answer',	['as'=>'enable-answer',	'uses'=>'QuestionsController@EnableAnswer']);
        Route::post('check-question-duplicate',	['as'=>'check-question-duplicate',	'uses'=>'QuestionsController@CheckQuestionDuplicate']);
        
        //Manage Bot
        Route::get('manage-bot',	['as'=>'manage-bot',	'uses'=>'BotController@ManageBot']);
        Route::post('start-bot',	['as'=>'start-bot',	'uses'=>'BotController@StartBot']);
        Route::get('stop-bot/{bot_id}/{new_status}',	['as'=>'stop-bot',	'uses'=>'BotController@StopBot']);
        Route::get('bot-report',	['as'=>'bot-report',	'uses'=>'BotController@BotReport']);
        Route::post('filter-bot-reports',	['as'=>'filter-bot-reports',	'uses'=>'BotController@FilterBotReport']);
        
        //Manage Unanswered Queries
        Route::get('unanswered-queries',	['as'=>'unanswered-queries',	'uses'=>'BotController@UnansweredQueries']);
        Route::post('delete-unanswered-query',	['as'=>'delete-unanswered-query',	'uses'=>'BotController@DeleteQueries']);
        
        });
	});
	// Invoice Make Payment
	Route::get('confirm-invoice/{id}',	['as'=>'confirm-invoice',	'uses'=>'Panel\InvoiceController@confirmAddInvoice']);
	Route::get('invoice-details/{id}',	['as'=>'invoice-details',	'uses'=>'Panel\InvoiceController@invoiceDetails']);
    Route::post('invoice-details/{id}',	['as'=>'invoice-details',	'uses'=>'Panel\InvoiceController@SubmitInvoiceDetails']);
    Route::get('payment-method/{id}',	['as'=>'payment-method',	'uses'=>'Panel\InvoiceController@paymentMethod']);		
	Route::post('payment-method/{id}',	['as'=>'payment-method',	'uses'=>'Panel\InvoiceController@SubmitPaymentMethod']);
    Route::post('/payment-method-bankaccount/{id}',	['as'=>'/payment-method-bankaccount',	'uses'=>'Panel\InvoiceController@SubmitPaymentMethodACH']);
	Route::get('payment-verification/{id}',	['as'=>'payment-verification',	'uses'=>'Panel\InvoiceController@paymentVerification']);
    Route::post('payment-verification/{id}',	['as'=>'payment-verification',	'uses'=>'Panel\InvoiceController@SubmitPaymentVerification']);	
	Route::get('payment-confirmation/{id}',	['as'=>'payment-confirmation',	'uses'=>'Panel\InvoiceController@paymentConfirmation']);
    
    //Crone Job
    Route::get('send-invoice-auto',	'Panel\InvoiceController@send_invoice_auto');
    Route::get('reminder',	'Panel\InvoiceController@tmp');
