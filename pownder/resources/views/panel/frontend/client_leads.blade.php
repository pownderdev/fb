@extends('layouts/default')

{{-- Page title --}}
@section('title')
Leads
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datedropper/datedropper.css')}}">
<!--<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/timedropper/css/timedropper.css')}}">-->
<link rel="stylesheet" type="text/css" href="{{asset('assets/js/sumoselect/sumoselect.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-timepicker.css')}}">
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	
	.date-picker-wrapper{z-index: 999999;}
	
	#AppointmentSetting {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#AppointmentSetting .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#AddLeadManaulModal {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#AddLeadManaulModal .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#EditLeadManaulModal {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#EditLeadManaulModal .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#AllotUserLead {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#AllotUserLead .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	.SumoSelect {width: 100% !important; color: #000 !important;} 
	
	.SumoSelect .select-all {height: 35px !important;}
	
	.options{max-height: 150px !important;}
	
	.SelectBox {padding: 7px 12px !important;}
	
	#dispute_model {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#dispute_model .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#AutomotiveSetting {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#AutomotiveSetting .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#LegalSetting {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#LegalSetting .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	.export.btn-group {*display:none!important;}
	.fixed-table-toolbar {*width:60%;}
	.fixed-table-toolbar .search {width: Calc( 100% - 430px );}
	
	@media screen and (max-width: 767px) {
	.fixed-table-toolbar {width: 100%;}
	.fixed-table-toolbar {width:100%;}
	.fixed-table-toolbar .search {width: Calc( 100% );}
	}
	
	.help-block {color: #fa5a46  !important;}
	
	#sold_modal .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#legal_modal {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#legal_modal .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#default_modal {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#default_modal .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	.fixed-table-container {height: 500px !important; }
	
	#search_lead_type {
    display: block;
    width: 100px;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.428571429;
    color: #555555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
	}
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="#">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">
			<a href="#"> Leads</a>
		</li>
	</ol>
</section>
<!-- Main content -->

<section class="content">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-md-12">
			<div class="row tiles-row">
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
					<div class="canvas-interactive-wrapper1">
						<canvas id="canvas-interactive1" class="grad1"></canvas>
						<a href="#"  onClick="AppointmentSearch('Appointment', '')"> 
							<div class="cta-wrapper1">
								<div class="widget">
									<div class="item">
										<div class="widget-icon pull-left icon-color animation-fadeIn">
											<i class="fa fa-fw fa-calendar-o fa-size"></i>
										</div>
									</div>
									<div class="widget-count panel-white">
										<div class="item-label text-center">
											<div id="TotalAppointment" class="count-box">{{ number_format($TotalAppointment,0) }}</div>
											<span class="title">Appointment</span>
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
					<div class="widget">
						<div class="canvas-interactive-wrapper2">
							<canvas id="canvas-interactive2" class="grad1"></canvas>
							<a href="#" onClick="AppointmentSearch('Appointment', 'Confirmed')"> 
								<div class="cta-wrapper2">
									<div class="item">
										<div class="widget-icon pull-left icon-color animation-fadeIn">
											<i class="fa fa-fw fa-calendar-check-o fa-size"></i>
										</div>
									</div>
									<div class="widget-count panel-white">
										<div class="item-label text-center">
											<div class="count-box" id="ConfirmedAppointment">
												@if($TotalAppointment == 0)
												0%
												@else
												{{ number_format(($ConfirmedAppointment/$TotalAppointment)*100,2) }}%
												@endif
											</div>
											<span class="title">Confirmed</span>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
					<div class="widget">
						<div class="canvas-interactive-wrapper3">
							<canvas id="canvas-interactive3" class="grad1"></canvas>
							<a href="#" onClick="AppointmentSearch('Appointment', 'Shown')"> 
								<div class="cta-wrapper3">
									<div class="item">
										<div class="widget-icon pull-left icon-color animation-fadeIn">
											<i class="fa fa-fw fa-calendar fa-size"></i>
										</div>
									</div>
									<div class="widget-count panel-white">
										<div class="item-label text-center">
											<div class="count-box" id="ShownAppointment">
												@if($TotalAppointment == 0)
												0%
												@else
												{{ number_format(($ShownAppointment/$TotalAppointment)*100,2) }}%
												@endif
											</div>
											<span class="title">Shown</span>
										</div>
									</div>
								</div>
							</a> 
						</div>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
					<div class="widget">
						<div class="canvas-interactive-wrapper4">
							<canvas id="canvas-interactive4" class="grad1"></canvas>
							<a href="#"  onClick="AppointmentSearch('Appointment', 'Canceled')"> 
								<div class="cta-wrapper4">
									<div class="item">
										<div class="widget-icon pull-left icon-color animation-fadeIn">
											<i class="fa fa-fw fa-calendar-times-o fa-size"></i>
										</div>
									</div>
									<div class="widget-count panel-white">
										<div class="item-label text-center">
											<div class="count-box" id="CanceledAppointment">
												@if($TotalAppointment == 0)
												0%
												@else
												{{ number_format(($CanceledAppointment/$TotalAppointment)*100,2) }}%
												@endif
											</div>
											<span class="title">Canceled</span>
										</div>
									</div>
								</div>
							</a> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="content">
	<!--fourth table start-->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i>  Leads
					</h3>
				</div>
				<div class="panel-body">
					<input type="hidden" id="search_setting" name="search_setting" value="">
					<input type="hidden" id="search_appointment_status" name="search_appointment_status" value="">
					<input type="hidden" id="search_phone_number_hide" name="search_phone_number_hide" value="No">
					
					@if(app('request')->exists('email'))
					<input type="hidden" id="search_email" name="search_email" value="{{ app('request')->email }}">
					@else
					<input type="hidden" id="search_email" name="search_email">
					@endif
					
					@if(app('request')->exists('phone_number'))
					<input type="hidden" id="search_phone_number" name="search_phone_number" value="{{ app('request')->phone_number }}">
					@else
					<input type="hidden" id="search_phone_number" name="search_phone_number">
					@endif
					
					<table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-query-params="queryParams" data-pagination="true" data-id-field="id" data-page-list="[10, 20, 40, ALL]" data-side-pagination="server" data-show-footer="false"  data-toggle="table" data-url="{{ route('client-lead-search') }}">
						<thead>
                            <tr>
								<th data-field="" @if(in_array("0",$ClientLeadColumns)) data-visible="false" @endif><input type="checkbox" class="checkall" /></th>
                                <th data-field="#" @if(in_array("1",$ClientLeadColumns)) data-visible="false" @endif>#</th>
                                <th data-field="Created Time" data-sortable="true" @if(in_array("2",$ClientLeadColumns)) data-visible="false" @endif>Created Time</th>
								<th data-field="Appt. Date & Time" data-visible="false" data-sortable="true" @if(in_array("3",$ClientLeadColumns)) data-visible="false" @endif>Appt. Date & Time</th>
								<th data-field="Full Name" data-sortable="true" @if(in_array("4",$ClientLeadColumns)) data-visible="false" @endif>Full Name</th>
								<th data-field="Type" data-sortable="true" @if(in_array("5",$ClientLeadColumns)) data-visible="false" @endif>Type</th>
                                <th data-field="Email" data-sortable="true" @if(in_array("6",$ClientLeadColumns)) data-visible="false" @endif>Email</th>
                                <th data-field="Phone #" data-sortable="true" @if(in_array("7",$ClientLeadColumns)) data-visible="false" @endif>Phone #</th>
                                <th data-field="City" data-sortable="true" @if(in_array("8",$ClientLeadColumns)) data-visible="false" @endif>City</th>
								@if(Session::get('user_category')=='user')
								<th data-field="Action" @if(in_array("9",$ClientLeadColumns)) data-visible="false" @endif>Action</th>
								@else
								<th data-field="User" data-sortable="true" @if(in_array("9",$ClientLeadColumns)) data-visible="false" @endif>User</th>
								<th data-field="Action" @if(in_array("10",$ClientLeadColumns)) data-visible="false" @endif>Action</th>
								@endif
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>	
	</div>
	<!--fourth table end-->
	
	<!-- AddLeadManaulModal -->
	<div id="AddLeadManaulModal" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header modal_heading_bg">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Add Lead</h4>
				</div>
				<form class="form-horizontal" role="form" id="manual_lead_add">
					<div class="modal-body form-body">
						<input type="hidden" name="client_name" id="client_name" value="{{ $lead_client_id }}">
						<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="first_name">First Name</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="first_name" id="first_name" placeholder="First Name" class="form-control">
									<small class="text-danger animated first_name fadeInUp manual_lead_add"></small>
								</div>
							</div>
							<div class="col-md-6">
								<label for="last_name">Last Name</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="last_name" id="last_name" placeholder="Last Name" class="form-control">
									<small class="text-danger animated last_name fadeInUp manual_lead_add"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="phone">Phone #</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="phone" id="phone" placeholder="Phone #" class="form-control">
									<small class="text-danger animated phone fadeInUp manual_lead_add"></small>
								</div>
							</div>
							<div class="col-md-6">
								<label for="email">Email</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="email" id="email" placeholder="Email" class="form-control">
									<small class="text-danger animated email fadeInUp manual_lead_add"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="full_address">Full Address</label>
								<div class="input-group" style="width: 100%;">
									<textarea name="full_address" id="full_address" placeholder="Full Address" rows="2" class="form-control"></textarea>
									<small class="text-danger animated full_address fadeInUp manual_lead_add"></small>
								</div>
							</div>
							<div class="col-md-6">
								<label for="zip">Zip</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="zip" id="zip" placeholder="Zip" class="form-control" maxlength="5" minlength="5" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
									<small class="text-danger animated zip fadeInUp manual_lead_add"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="city">City</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="city" id="city" placeholder="City" class="form-control">
									<small class="text-danger animated city fadeInUp manual_lead_add"></small>
								</div>
							</div>
							<div class="col-md-6">
								<label for="state">State</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="state" id="state" placeholder="State" class="form-control">
									<small class="text-danger animated state fadeInUp manual_lead_add"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="lead_type">Lead Type</label>
								<div class="input-group" style="width: 100%;">
									<label class="radio-inline"><input type="radio" name="lead_type" value="Pownder™ Call"  checked="checked"> Pownder™ Call</label><br />
									<label class="radio-inline"><input type="radio" name="lead_type" value="Pownder™ Messenger"> Pownder™ Messenger</label>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<div class="input-group" style="width: 100%;">
									<label><input type="checkbox" class="messageCheckbox" id="messageCheckbox" value="Checkbox Checked"> Sold</label><br />
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-default addButton" style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c !important;"> Submit</button>
						<button type="reset" class="btn btn-default manual_lead_add_clear">Reset</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- #/AddLeadManaulModal -->
	
	<!-- EditLeadManaulModal -->
	<div id="EditLeadManaulModal" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header modal_heading_bg">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Edit Lead</h4>
				</div>
				<form class="form-horizontal" role="form" id="manual_lead_edit">
					<div class="modal-body form-body">
						<input type="hidden" name="client_name" id="edit_client_name" value="{{ $lead_client_id }}">
						<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="edit_facebook_ads_lead_user_id" id="edit_facebook_ads_lead_user_id">
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="edit_first_name">First Name</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="first_name" id="edit_first_name" placeholder="First Name" class="form-control">
									<small class="text-danger animated edit_first_name fadeInUp manual_lead_edit"></small>
								</div>
							</div>
							<div class="col-md-6">
								<label for="edit_last_name">Last Name</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="last_name" id="edit_last_name" placeholder="Last Name" class="form-control">
									<small class="text-danger animated edit_last_name fadeInUp manual_lead_edit"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="edit_phone">Phone #</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="phone" id="edit_phone" placeholder="Phone #" class="form-control">
									<small class="text-danger animated edit_phone fadeInUp manual_lead_edit"></small>
								</div>
							</div>
							<div class="col-md-6">
								<label for="edit_email">Email</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="email" id="edit_email" placeholder="Email" class="form-control">
									<small class="text-danger animated edit_email fadeInUp manual_lead_edit"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="edit_full_address">Full Address</label>
								<div class="input-group" style="width: 100%;">
									<textarea name="full_address" id="edit_full_address" placeholder="Full Address" rows="2" class="form-control"></textarea>
									<small class="text-danger animated edit_full_address fadeInUp manual_lead_edit"></small>
								</div>
							</div>
							<div class="col-md-6">
								<label for="edit_zip">Zip</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="zip" id="edit_zip" placeholder="Zip" class="form-control" maxlength="5" minlength="5" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
									<small class="text-danger animated edit_zip fadeInUp manual_lead_edit"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="edit_city">City</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="city" id="edit_city" placeholder="City" class="form-control">
									<small class="text-danger animated edit_city fadeInUp manual_lead_edit"></small>
								</div>
							</div>
							<div class="col-md-6">
								<label for="edit_state">State</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="state" id="edit_state" placeholder="State" class="form-control">
									<small class="text-danger animated edit_state fadeInUp manual_lead_edit"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="edit_lead_type">Lead Type</label>
								<div class="input-group" style="width: 100%;">
									<label class="radio-inline"><input type="radio" name="edit_lead_type" value="Pownder™ Call"> Pownder™ Call</label><br />
									<label class="radio-inline"><input type="radio" name="edit_lead_type" value="Pownder™ Messenger"> Pownder™ Messenger</label>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-default editButton" style=" color:#fff; background-color: #ffbe18!important;border-color: #e4a70c !important;"> Submit</button>
						<button type="reset" class="btn btn-default">Reset</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- #/EditLeadManaulModal -->
	
	<!-- AllotUserLead -->
	<div id="AllotUserLead" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background: linear-gradient(154deg, #00c7be 0, #007091 50%, #ffc902 100%)!important;">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title AllotUserLeadTitle">Allot Lead to User</h4>
				</div>
				<form action="{{ route('client_user_lead_allot') }}" class="form-horizontal" role="form" method="post" id="user-lead-allot-validation">
					<div class="modal-body">
						<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="AllotUserLeadId" id="AllotUserLeadId">
						<input type="hidden" name="Action" id="Action">
						<div class="row m-t-10">
							<div class="col-md-12">
								<label for="manager_id">Select User</label>
								<div class="input-group" style="width: 100%;">
									<select name="manager_id" id="manager_id" class="form-control">
										<option value="">Select User</option>
										@foreach($Managers as $Manager)
										<option value="{{ $Manager->id }}">{{ $Manager->first_name }} {{ $Manager->last_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-default" style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c !important;">Submit</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- /#AllotUserLead -->
</section>

<div id="dispute_model" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(154deg, #00c7be 0, #007091 50%, #ffc902 100%)!important;">
				<button type="button" class="close" data-dismiss="modal">x</button>
				<h4 class="modal-title">Dispute</h4>
			</div>
			<form action="{{ route('dispute_lead') }}" method="post">	
				<div class="modal-body">
					<div class="row" style="margin: 0px 30px">
						<div class="col-lg-12">
							<div class="form-body">
								<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
								<input type="hidden" name="client_ads_lead_user_id" id="client_ads_lead_user_id" value="" />
								<div class="form-group" style="height: 30px;">
									<div class="col-lg-9" style="padding-left: 0px">
										<label class="radio-inline"><input type="radio" name="optradio" value="Customer Looking For Another Make" checked> Customer Looking For Another Make</label>
									</div>
									<div class="col-lg-3" style="padding-left: 0px">
										<label class="radio-inline"><input type="radio" name="optradio" value="Dispute"> Dispute</label>
									</div>
								</div>
								<div class="form-group Dispute" style="display:none; margin-left: 20px;">
									<label><input type="checkbox" name="Dispute[]" value="Bad Lead"> Bad Lead</label><br><br>
									
									<label><input type="checkbox" name="Dispute[]" value="Bad Email"> Bad Email</label><br><br>
									
									<label><input type="checkbox" name="Dispute[]" value="Bad Phone #"> Bad Phone #</label><br>
									
									<small class="text-danger animated fadeInUp DisputeCheckBox dispute_lead_error"></small>
								</div>
								<div class="form-group AnotherMake" style="margin-left: 20px;">
									<label class="radio-inline"><input type="radio" name="AnotherMake" value="New" checked> New</label>
									<label class="radio-inline"><input type="radio" name="AnotherMake" value="Used"> Used</label>
								</div>
								<div class="form-group AnotherMake" style="margin-left: 20px;">
									<div class="year_between" style="display:none">
										<label for="year_between">Year Between</label>
										@php  $SoldYear = date('Y');  @endphp
										@if(date('n') >= 4)
										@php  $SoldYear++;  @endphp
										@endif
										<div class="input-group" style="width: 100%;">
											<select multiple="multiple" name="year_between[]" id="year_between" placeholder="Select Year" class="select1 SumoUnder">
												@for($Y=$SoldYear; $Y>=1990; $Y--)
												<option value="{{ $Y }}">{{ $Y }}</option>
												@endfor
											</select>
										</div>
									</div>
								</div>
								<div class="form-group AnotherMake" style="margin-left: 20px;">
									<label for="dispute_group_sub_category_id" class="control-label">@if($displayModel=='Yes') Make @else Sub-Category @endif</label>
									<select name="dispute_group_sub_category_id" id="dispute_group_sub_category_id" title="Select Group Category" class="form-control">
										<option value="">Select @if($displayModel=='Yes') Make @else Sub-Category @endif</option>
										@foreach($GroupSubCategories as $GroupSubCategory)
										<option value="{{ $GroupSubCategory->id }}">{{ $GroupSubCategory->name }}</option>
										@endforeach
									</select>
									
									<small class="text-danger animated fadeInUp dispute_group_sub_category_id dispute_lead_error"></small>
								</div>
								@if($displayModel=='Yes')
								<div class="form-group AnotherMake" style="margin-left: 20px;">
									<label for="disputeModel" class="control-label">Model</label>
									<input type="text" name="disputeModel" id="disputeModel" placeholder="Model" class="form-control">
									<small class="text-danger animated fadeInUp disputeModel dispute_lead_error"></small>
								</div>
								@endif
								<div class="form-group" style="margin-left: 20px;">
									<label for="dispute_comment" class="control-label">Comment</label>
									<textarea name="dispute_comment" id="dispute_comment" placeholder="Comment" class="form-control"></textarea>
									<input type="hidden" name="displayModel" id="displayModel" value="{{ $displayModel }}">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="add_dispute_lead" style="display:none";></button>
					<button type="button" class="btn btn-default addDisputeLead" style=" color:#fff; background-color: #ffbe18 !important; border-color: #e4a70c !important;"> Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- sold-modal -->
<div id="sold_modal" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(154deg, #00c7be 0, #007091 50%, #ffc902 100%)!important;">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Sold</h4>
			</div>
			<form action="{{ route('client_lead_sold') }}" class="form-horizontal" role="form" method="post" id="lead-sold-validation">
				<div class="modal-body">
					<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="sold_facebook_ads_lead_user_id" id="sold_facebook_ads_lead_user_id">
					<div class="row m-t-10">
						<div class="col-md-6">
							<label for="sold_date">Sold Date</label>
							<div class="input-group" style="width: 100%;">
								<div class="input-group-addon">
									<i class="fa fa-fw fa-calendar"></i>
								</div>
								<input type="text" name="sold_date" id="sold_date" placeholder="MM-DD-YYYY" class="form-control" style="background-color:#FFF">
							</div>
						</div>
						<div class="col-md-6">
							<label  for="stock">Stock#</label>
							<div class="input-group" style="width: 100%;">
								<span class="input-group-addon">
									<i class="fa fa-fw fa-file-text-o"></i>
								</span>
								<input type="text" name="stock" id="stock" placeholder="Stock#" maxlength="10" class="form-control">
							</div>
						</div>
					</div>
					<div class="row m-t-10">
						<div class="col-md-4">
							<label for="sold_year">Year</label>
							@php  $SoldYear = date('Y');  @endphp
							@if(date('n') >= 4)
							@php  $SoldYear++;  @endphp
							@endif
							<div class="input-group" style="width: 100%;">
								<select name="sold_year" id="sold_year" placeholder="Select Year" class="form-control">
									<option value="">Select Year</option>
									@for($Y=$SoldYear; $Y>=1990; $Y--)
									<option value="{{ $Y }}">{{ $Y }}</option>
									@endfor
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<label  for="group_sub_category_id">Make</label>
							<div class="input-group" style="width: 100%;">
								<select name="group_sub_category_id" id="group_sub_category_id" placeholder="Select Make" class="form-control">
									<option value="">Select Make</option>
									@foreach($AutomotiveSubCategories as $AutomotiveSubCategory)
									<option value="{{ $AutomotiveSubCategory->id }}">{{ $AutomotiveSubCategory->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<label  for="model">Model</label>
							<div class="input-group" style="width: 100%;">
								<input type="text" name="model" id="model" placeholder="Model" class="form-control">
							</div>
						</div>
					</div>
					<div class="row m-t-10">
						<div class="col-md-6">
							<label for="total_profit">Total Profit $</label>
							<div class="input-group" style="width: 100%;">
								<div class="input-group-addon">
									<i class="fa fa-fw fa-usd"></i>
								</div>
								<input type="text" name="total_profit" id="total_profit" placeholder="Total Profit" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<input type="hidden" name="profit_total" id="profit_total" placeholder="Total Profit" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<input type="hidden" name="roi_automation" id="roi_automation">
							</div>
						</div>
						<div class="col-md-6">
							<label for="real_buyer_name">Co-Buyer</label>
							<div class="input-group" style="width: 100%;">
								<input type="text" name="real_buyer_name" id="real_buyer_name" placeholder="Real Buyer Name" class="form-control" style="display:inline; width:70%; background-color:#FFF;display:none" readonly="readonly"> 
								<button type="button" class="btn btn-info add_buyer" title="Add Co-Buyer" style="margin-left: 10px;">
									<i class="fa fa-plus" aria-hidden="true"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default" style=" color:#fff; background-color: #ffbe18!important;
					border-color: #e4a70c !important;"> Submit</button>
					<button type="reset" class="btn btn-default form_reset">Reset</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- #/sold-modal -->

<div id="add_real_buyer" class="modal fade animated" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(154deg, #00c7be 0, #007091 50%, #ffc902 100%)!important;">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Add Co-Buyer</h4>
			</div>
			<div class="modal-body">
				<div class="row m-t-10">
					<div class="col-md-12">
						<div class="form-group">
							<label class="sr-only" for="buyer_name">Name</label>
							<input type="text" name="buyer_name" id="buyer_name" placeholder="Name" class="form-control m-t-10">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success add_real_buyer">Add</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="AutomotiveSetting" class="modal fade animated" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(154deg, #00c7be 0, #007091 50%, #ffc902 100%)!important;">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Setting</h4>
			</div>
			<form role="form" id="leadSettingForm">
				<div class="modal-body">
					<div class="form-body">
						<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="setting_facebook_ads_lead_user_id" id="setting_facebook_ads_lead_user_id">
						<div class="form-group">
							<label><input type="radio" name="lead_setting" value="Sold" class="default_lead_setting" checked="checked"> Sold</label><br /><br />
							<label><input type="radio" name="lead_setting" value="Need CO-X"> Need CO-X</label><br /><br />
							<label><input type="radio" name="lead_setting" value="Undecided"> Undecided</label><br /><br />
							<label><input type="radio" name="lead_setting" value="Bad Credit"> Bad Credit</label><br /><br />
							<label><input type="radio" name="lead_setting" value="Pre-Check"> Pre-Check</label><br /><br />
							<label><input type="radio" name="lead_setting" value="Appointment"> Appointment</label>
						</div>
						<div class="form-group Appointment" style="display:none;min-height: 60px;">
							<div class="col-md-6">
								<label for="appointment_date">Appointment Date</label>
								<div class="input-group" style="width: 100%;">
									<div class="input-group-addon">
										<i class="fa fa-fw fa-calendar"></i>
									</div>
									<input type="text" class="form-control" id="appointment_date" name="appointment_date" placeholder="MM-DD-YYYY" style="background:#FFF" readonly="readonly">
								</div>
								<small class="text-danger animated appointment_date fadeInUp"></small><br />
							</div>
							<div class="col-md-6">
								<label for="appointment_time">Appointment Time</label>
								<div class="input-group" style="width: 100%;">
									<div class="input-group-addon">
										<i class="fa fa-fw fa-clock-o"></i>
									</div>
									<input type="text" class="form-control  bootstrap-timepicker timepicker td-input" id="appointment_time" name="appointment_time" placeholder="HH:MM" style="background:#FFF" readonly="">
								</div>
								<small class="text-danger animated appointment_time fadeInUp"></small><br />
							</div>
						</div>
						<div class="form-group other_comment" style="display:none">
							<label for="setting_comment">Comment</label>
							<div class="input-group" style="width: 100%;">
								<textarea class="form-control" id="setting_comment" name="setting_comment"></textarea>
								<small class="text-danger animated setting_comment fadeInUp"></small>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default SettingSubmit" style=" color:#fff; background-color: #ffbe18!important;border-color: #e4a70c !important;"> Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Legal-modal -->
<div id="legal_modal" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(154deg, #00c7be 0, #007091 50%, #ffc902 100%)!important;">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Legal</h4>
			</div>
			<form action="{{ route('legal_lead') }}" class="form-horizontal" role="form" method="post" id="lead-legal-validation">
				<div class="modal-body">
					<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="legal_facebook_ads_lead_user_id" id="legal_facebook_ads_lead_user_id">
					<div class="row m-t-10">
						<div class="col-md-6">
							<label for="case_date">Case Date</label>
							<div class="input-group" style="width: 100%;">
								<div class="input-group-addon">
									<i class="fa fa-fw fa-calendar"></i>
								</div>
								<input type="text" name="case_date" id="case_date" placeholder="MM-DD-YYYY" class="form-control" style="background-color:#FFF">
							</div>
						</div>
						<div class="col-md-6">
							<label  for="case_number">Case #</label>
							<div class="input-group" style="width: 100%;">
								<span class="input-group-addon">
									<i class="fa fa-fw fa-file-text-o"></i>
								</span>
								<input type="text" name="case_number" id="case_number" placeholder="Case #" maxlength="10" class="form-control">
							</div>
						</div>
					</div>
					<div class="row m-t-10">
						<div class="col-md-4">
							<label for="legal_group_sub_category_id">Type Of Case</label>
							<div class="input-group" style="width: 100%;">
								<select name="legal_group_sub_category_id" id="legal_group_sub_category_id" placeholder="Select Case Type" class="form-control">
									<option value="">Select Case Type</option>
									@foreach($LegalSubCategories as $LegalSubCategory)
									<option value="{{ $LegalSubCategory->id }}">{{ $LegalSubCategory->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-4">
							<label  for="down_payment">Down Payment</label>
							<div class="input-group" style="width: 100%;">
								<div class="input-group-addon">
									<i class="fa fa-fw fa-usd"></i>
								</div>
								<input type="text" name="down_payment" id="down_payment" placeholder="Down Payment" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
							</div>
						</div>
						<div class="col-md-4" id="legal_client_roi_check" style="display:none">
							<label  for="legal_total">Total $</label>
							<div class="input-group" style="width: 100%;">
								<div class="input-group-addon">
									<i class="fa fa-fw fa-usd"></i>
								</div>
								<input type="text" name="legal_total" id="legal_total" placeholder="Total" class="form-control legal_total" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<input type="hidden" name="legal_client_roi_automation" id="legal_client_roi_automation">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default" style=" color:#fff; background-color: #ffbe18!important;
					border-color: #e4a70c !important;"> Submit</button>
					<button type="reset" class="btn btn-default legal_form_reset">Reset</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- #/legal-modal -->

<!-- default-modal -->
<div id="default_modal" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(154deg, #00c7be 0, #007091 50%, #ffc902 100%)!important;">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Other</h4>
			</div>
			<form action="{{ route('client_default_lead') }}" class="form-horizontal" role="form" method="post" id="lead-default-validation">
				<div class="modal-body">
					<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="default_facebook_ads_lead_user_id" id="default_facebook_ads_lead_user_id">
					<div class="row m-t-10">
						<div class="col-md-6">
							<label for="conversion_date">Conversion Date</label>
							<div class="input-group" style="width: 100%;">
								<div class="input-group-addon">
									<i class="fa fa-fw fa-calendar"></i>
								</div>
								<input type="text" name="conversion_date" id="conversion_date" placeholder="MM-DD-YYYY" class="form-control" style="background-color:#FFF">
							</div>
						</div>
						<div class="col-md-6" id="default_client_roi_check" style="display:none">
							<label  for="default_total_profit">Total Profit $</label>
							<div class="input-group" style="width: 100%;">
								<div class="input-group-addon">
									<i class="fa fa-fw fa-usd"></i>
								</div>
								<input type="text" name="default_total_profit" id="default_total_profit" placeholder="Total Profit" class="form-control default_total_profit" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<input type="hidden" name="default_client_roi_automation" id="default_client_roi_automation">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default" style=" color:#fff; background-color: #ffbe18!important;
					border-color: #e4a70c !important;"> Submit</button>
					<button type="reset" class="btn btn-default default_form_reset">Reset</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- #/default-modal -->

<div id="LegalSetting" class="modal fade animated" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(154deg, #00c7be 0, #007091 50%, #ffc902 100%)!important;">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Setting</h4>
			</div>
			<form role="form" id="LegalLeadSettingForm">
				<div class="modal-body">
					<div class="form-body">
						<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="legal_setting_facebook_ads_lead_user_id" id="legal_setting_facebook_ads_lead_user_id">
						<div class="form-group">
							<label><input type="radio" name="legal_lead_setting" value="Closed" class="default_lead_setting" checked="checked"> Closed</label><br /><br />
							<label><input type="radio" name="legal_lead_setting" value="Doesn't Qualify"> Doesn't Qualify</label><br /><br />
							<label><input type="radio" name="legal_lead_setting" value="Undecided"> Undecided</label><br /><br />
							<label><input type="radio" name="legal_lead_setting" value="Appointment"> Appointment</label>
						</div>
						<div class="form-group LegalAppointment" style="display:none;min-height: 60px;">
							<div class="col-md-6">
								<label for="legal_appointment_date">Appointment Date</label>
								<div class="input-group" style="width: 100%;">
									<div class="input-group-addon">
										<i class="fa fa-fw fa-calendar"></i>
									</div>
									<input type="text" class="form-control" id="legal_appointment_date" name="legal_appointment_date" placeholder="MM-DD-YYYY" style="background:#FFF" readonly="readonly">
								</div>
								<small class="text-danger animated legal_appointment_date fadeInUp"></small><br />
							</div>
							<div class="col-md-6">
								<label for="legal_appointment_time">Appointment Time</label>
								<div class="input-group" style="width: 100%;">
									<div class="input-group-addon">
										<i class="fa fa-fw fa-clock-o"></i>
									</div>
									<input type="text" class="form-control  bootstrap-timepicker timepicker td-input" id="legal_appointment_time" name="legal_appointment_time" placeholder="HH:MM" style="background:#FFF" readonly="">
								</div>
								<small class="text-danger animated legal_appointment_time fadeInUp"></small><br />
							</div>
						</div>
						<div class="form-group legal_other_comment" style="display:none">
							<label for="legal_setting_comment">Comment</label>
							<div class="input-group" style="width: 100%;">
								<textarea class="form-control" id="legal_setting_comment" name="legal_setting_comment"></textarea>
								<small class="text-danger animated legal_setting_comment fadeInUp"></small>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default LegalSettingSubmit" style=" color:#fff; background-color: #ffbe18 !important; border-color: #e4a70c !important;"> Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="AppointmentSetting" class="modal fade animated" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(154deg, #00c7be 0, #007091 50%, #ffc902 100%) !important;">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Appointment Status</h4>
			</div>
			<form role="form" id="AppointmentLeadSettingForm">
				<div class="modal-body">
					<div class="form-body">
						<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="appointment_facebook_ads_lead_user_id" id="appointment_facebook_ads_lead_user_id">
						<div class="form-group">
							<label><input type="radio" name="appointment_lead_setting" value="Confirmed" class="default_appointment_lead_setting" checked="checked"> Confirmed</label><br /><br />
							<label><input type="radio" name="appointment_lead_setting" value="Reschedule"> Reschedule</label><br /><br />
							<label><input type="radio" name="appointment_lead_setting" value="Canceled"> Canceled</label><br /><br />
							<label><input type="radio" name="appointment_lead_setting" value="Shown"> Shown</label><br /><br />
							<label><input type="radio" name="appointment_lead_setting" value="Sold"> Sold</label>
						</div>
						<div class="form-group AppointmentDiv" style="display:none;min-height: 60px;">
							<div class="col-md-6">
								<label for="reschedule_appointment_date">Appointment Date</label>
								<div class="input-group" style="width: 100%;">
									<div class="input-group-addon">
										<i class="fa fa-fw fa-calendar"></i>
									</div>
									<input type="text" class="form-control" id="reschedule_appointment_date" name="reschedule_appointment_date" placeholder="MM-DD-YYYY" style="background:#FFF" readonly="readonly">
								</div>
								<small class="text-danger animated reschedule_appointment_date fadeInUp"></small><br />
							</div>
							<div class="col-md-6">
								<label for="reschedule_appointment_time">Appointment Time</label>
								<div class="input-group" style="width: 100%;">
									<div class="input-group-addon">
										<i class="fa fa-fw fa-clock-o"></i>
									</div>
									<input type="text" class="form-control  bootstrap-timepicker timepicker td-input" id="reschedule_appointment_time" name="reschedule_appointment_time" placeholder="HH:MM" style="background:#FFF" readonly="">
								</div>
								<small class="text-danger animated reschedule_appointment_time fadeInUp"></small><br />
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default AppointmentSettingSubmit" style="color:#fff; background-color: #ffbe18 !important; border-color: #e4a70c !important;"> Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

<!-- InputMask -->
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<!-- bootstrap time picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}" ></script>
<!--<script  type="text/javascript" src="{{asset('assets/vendors/timedropper/js/timedropper.js')}}" ></script>-->
<script type="text/javascript" src="{{asset('assets/js/bootstrap-timepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/lead_sold_validation.js')}}"></script>
<script src="{{asset('assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/sumoselect/jquery.sumoselect.min.js')}}" type="text/javascript" charset="utf-8"></script>
<script>
	$( document ).ready(function() {
	var extra_features = '<select name="search_lead_type" id="search_lead_type" title="Search Lead Type" style="display: inline;float: left; margin-right: 5px;"><option value="All">All</option><option value="Pownder™ Call">Calls</option><option value="Pownder™ Messenger">DM</option><option value="Pownder™ Lead">Internet</option></select><button class="btn btn-default add_manual_lead" title="Add New" ><i class="fa fa-fw fa-plus-square"></i></button><button class="btn btn-default LeadResync" title="Leads Re-Sync" ><i class="fa fa-fw fa-refresh"></i></button><button class="btn btn-default DNC" type="button" name="DNC" title="DNC List"><i class="fa fa-fw fa-bell-slash dnc_icon" style="color:#e01800"></i></button><button class="btn btn-default" type="button" name="mail" title="Mail" ><i class="fa fa-fw fa-envelope"></i></button><button class="btn btn-default" type="button" name="print" title="Print" ><i class="fa fa-fw fa-print"></i></button>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
		
		$("#phone").inputmask('(999) 999-9999');
		$("#edit_phone").inputmask('(999) 999-9999');
		
		@if(app('request')->exists('date_range'))
		$('#date-range-header').val("{{ app('request')->date_range }}");
		@endif
		
		$(document).on("click",".timepicker",function(){
			var d = new Date();
			var current_time = moment(d).format('hh:mm A'); // time format with moment.js
			$(this).timepicker('setTime', current_time); // setting timepicker to current time
			$(this).val(current_time); // setting input field to current time
		});
		
		$('#appointment_time').timepicker().on('hide.timepicker', function(e) {
			var start_time = '09:00 AM'; //start time
			var input_value = e.time.value; //appointment time
			var end_time = '09:00 PM'; //end time
			
			//convert time into timestamp
			var input = new Date("May 26, 1991 " + input_value);
			input = input.getTime();
			
			var stt = new Date("May 26, 1991 " + start_time);
			stt = stt.getTime();
			
			var endt = new Date("May 26, 1991 " + end_time);
			endt = endt.getTime();
			
			if(stt <= input && input <= endt) {
				$('.appointment_time').html('');
			}
			else
			{
				$('#appointment_time').val('');
				$('.appointment_time').html('Time between 9:00 AM to 9:00 PM');
			}
		});
		
		$('#legal_appointment_time').timepicker().on('hide.timepicker', function(e) {
			var start_time = '09:00 AM'; //start time
			var input_value = e.time.value; //appointment time
			var end_time = '09:00 PM'; //end time
			
			//convert time into timestamp
			var input = new Date("May 26, 1991 " + input_value);
			input = input.getTime();
			
			var stt = new Date("May 26, 1991 " + start_time);
			stt = stt.getTime();
			
			var endt = new Date("May 26, 1991 " + end_time);
			endt = endt.getTime();
			
			if(stt <= input && input <= endt) {
				$('.legal_appointment_time').html('');
			}
			else
			{
				$('#legal_appointment_time').val('');
				$('.legal_appointment_time').html('Time between 9:00 AM to 9:00 PM');
			}
		});
		
		$('#reschedule_appointment_time').timepicker().on('hide.timepicker', function(e) {
			var start_time = '09:00 AM'; //start time
			var input_value = e.time.value; //appointment time
			var end_time = '09:00 PM'; //end time
			
			//convert time into timestamp
			var input = new Date("May 26, 1991 " + input_value);
			input = input.getTime();
			
			var stt = new Date("May 26, 1991 " + start_time);
			stt = stt.getTime();
			
			var endt = new Date("May 26, 1991 " + end_time);
			endt = endt.getTime();
			
			if(stt <= input && input <= endt) {
				$('.reschedule_appointment_time').html('');
			}
			else
			{
				$('#reschedule_appointment_time').val('');
				$('.reschedule_appointment_time').html('Time between 9:00 AM to 9:00 PM');
			}
		});
		
		var options1 = {format: "m-d-Y", dropPrimaryColor: "#428bca"};
		$('#appointment_date').dateDropper($.extend({}, options1));
		$('#legal_appointment_date').dateDropper($.extend({}, options1));
		$('#reschedule_appointment_date').dateDropper($.extend({}, options1));
		
		$('.button-select').click(function () {
			$.get("{{ route('filter-appointment') }}",{dateRange: $('#date-range-header').val()},function(data){ 
				$('#TotalAppointment').html(data.TotalAppointment);
				$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
				$('#ShownAppointment').html(data.ShownAppointment);
				$('#CanceledAppointment').html(data.CanceledAppointment);
			});
			$('#table4').bootstrapTable('refresh');
		});
		
		$('.button-clear').click(function () {
			$.get("{{ route('filter-appointment') }}",{dateRange: ''},function(data){ 
				$('#TotalAppointment').html(data.TotalAppointment);
				$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
				$('#ShownAppointment').html(data.ShownAppointment);
				$('#CanceledAppointment').html(data.CanceledAppointment);
			});
			$('#table4').bootstrapTable('refresh');
		});
	});
</script>

<script>
	function queryParams(params) {
		params.dateRange = $('#date-range-header').val(); // add param1
		params.search = $('.fixed-table-toolbar .search input[type=text]').val();
		params.search_appointment_status = $('#search_appointment_status').val();
		params.search_phone_number_hide = $('#search_phone_number_hide').val();
		params.search_setting = $('#search_setting').val();
		params.email = $('#search_email').val();
		params.search_lead_type = $('#search_lead_type').val();
		params.phone_number = $('#search_phone_number').val();
		// console.log(JSON.stringify(params));
		// {"limit":10,"offset":0,"order":"asc","your_param1":1,"your_param2":2}
		return params;
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".form-body").find('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});  
		
		$('#year_between').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
	});
</script>

<script>
	$( document ).ready(function() {
		
		
		$(".add_manual_lead").click(function() {
			$('.manual_lead_add').html('');
			$('.manual_lead_add_clear').trigger('click');
			//$('.messageCheckbox[value="Checkbox Checked"]').iCheck('uncheck');
			$('#messageCheckbox').iCheck('uncheck');
			$('#AddLeadManaulModal').modal('show');
		});
	});
	
	function DisputeLead(id)
	{
		$('#client_ads_lead_user_id').val(id);
		$('.dispute_lead_error').html('');
		//	$('.Dispute').css('display', 'block');
		//	$("#brand").prop('required',false);
		$("#disputeModel").val('');
		$("#dispute_group_sub_category_id").val('');
		$('#dispute_model').modal("show");
	}
</script>

<script type="text/javascript">
	$(document).ready(function(){
		$("input[name='optradio']").on('ifChecked', function(event){
			var radioValue = $(this).val();
			if(radioValue=='Dispute')
			{
				$('.AnotherMake').css('display', 'none');
				$('.Dispute').css('display', 'block');
			}
			else if(radioValue=='Customer Looking For Another Make')
			{
				$('.AnotherMake').css('display', 'block');
				$('.Dispute').css('display', 'none');
			}
		});
		
		$("input[name='AnotherMake']").on('ifChecked', function(event){
			var AnotherMake = $(this).val();
			if(AnotherMake=='Used')
			{
				$('.year_between').css('display', 'block');
			}
			else
			{
				$('.year_between').css('display', 'none');
			}
		});
	});
</script>	

<script>
	$(document).ready(function(){
		$('.dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Client Lead Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$('#sold_modal').on('hidden.bs.modal', function () {
            $('#lead-sold-validation').bootstrapValidator('resetForm', true);
		});
		
		$('#AllotUserLead').on('hidden.bs.modal', function () {
			$('#user-lead-allot-validation').bootstrapValidator('resetForm', true);
		});
		
		$('#sold_date').dateRangePicker({
			singleDate: true,
			showShortcuts: false,
			singleMonth: true,
			format: 'MM-DD-YYYY'
		});
		
		$('.date-picker-wrapper').on('click', function(e) {
			$('#lead-sold-validation').bootstrapValidator('revalidateField', 'sold_date');
		});
		
		$('#case_date').dateRangePicker({
			singleDate: true,
			showShortcuts: false,
			singleMonth: true,
			format: 'MM-DD-YYYY'
		});
		
		$('.date-picker-wrapper').on('click', function(e) {
			$('#lead-legal-validation').bootstrapValidator('revalidateField', 'case_date');
		});
		
		$('#conversion_date').dateRangePicker({
			singleDate: true,
			showShortcuts: false,
			singleMonth: true,
			format: 'MM-DD-YYYY'
		});
		
		$('.date-picker-wrapper').on('click', function(e) {
			$('#lead-default-validation').bootstrapValidator('revalidateField', 'conversion_date');
		});
		
		$(".add_buyer").click(function() {
			$('#buyer_name').val('');
			$('#add_real_buyer').modal('show');
		});
		
		$(".add_real_buyer").click(function() {
			if($('#buyer_name').val()!='')
			{
				$('#real_buyer_name').val($('#buyer_name').val());
				$('#real_buyer_name').css('display','block');
			}
			else
			{
				$('#real_buyer_name').val('');
				$('#real_buyer_name').css('display','none');
			}
			$('#add_real_buyer').modal('hide');
		});
		
		$(".addDisputeLead").click(function() {
			var error='No';
			$('.dispute_lead_error').html('');
			var optradio = $("input[name='optradio']:checked").val();
			var disputeModel = $('#disputeModel').val();
			var displayModel = $('#displayModel').val();
			var dispute_group_sub_category_id = $('#dispute_group_sub_category_id').val()
			var Dispute = $("input[name='Dispute[]']:checked").val()
			//alert(Dispute);
			if(optradio=='Dispute')
			{
				if(Dispute==undefined || Dispute=='undefined')
				{
					error='Yes';
					$('.DisputeCheckBox').html('The dispute reason field is required.');
				}
			}
			else
			{
				if(displayModel=='Yes')
				{
					if(disputeModel=='')
					{
						error='Yes';
						$('.disputeModel').html('The model field is required.');
					}
				}
				
				if(dispute_group_sub_category_id=='')
				{
					error='Yes';
					$('.dispute_group_sub_category_id').html('The Make field is required.');
				}
			}
			
			if(error=='No')
			{
				$( ".add_dispute_lead" ).trigger( "click" );
			}
		});
	});	
</script>

<script>
	function Sold(facebook_ads_lead_user_id)
	{
		$('.form_reset').click();
		$('#real_buyer_name').val('');
		$('#real_buyer_name').css('display','none');
		$('#sold_modal').modal("show");
		$('#sold_modal').on('shown.bs.modal', function () {
            $('#lead-sold-validation').bootstrapValidator('resetForm', true);
		});
		$('#sold_facebook_ads_lead_user_id').val(facebook_ads_lead_user_id);
		$.get("{{ route('client_roi_status') }}",{facebook_ads_lead_user_id: facebook_ads_lead_user_id},function(data){ 
			if(data=='Yes')
			{
				$('#roi_automation').val(data);
				$('#profit_total').attr('type','text');
				$('#profit_total').val('');
				$('#total_profit').attr('type','hidden');
				$('#total_profit').val('0');
				$('.help-block').css('display','none');
			}
			else
			{
				$('#roi_automation').val(data);
				$('#total_profit').attr('type','text');
				$('#total_profit').val('');
				$('#profit_total').attr('type','hidden');
				$('#profit_total').val('0');
				$('.help-block').css('display','none');
			}
		});
		$('#sold_modal').modal("show");
	}
	
	function Legal(facebook_ads_lead_user_id)
	{
		$('.legal_form_reset').click();
		
		$('#lead-legal-validation').bootstrapValidator('resetForm', true);
		
		$('#legal_facebook_ads_lead_user_id').val(facebook_ads_lead_user_id);
		$('#legal_client_roi_check').css('display', 'none');
		$.get("{{ route('client_roi_status') }}",{facebook_ads_lead_user_id: facebook_ads_lead_user_id},function(data){ 
			if(data=='Yes')
			{
				$('#legal_client_roi_check').css('display', 'none');
				$('#legal_client_roi_automation').val(data);
				$('.legal_total').attr('id','total_legal');
			}
			else
			{
				$('#legal_client_roi_check').css('display', 'block');
				$('#legal_client_roi_automation').val(data);
				$('.legal_total').attr('id','legal_total');
			}
		});
		$('#legal_modal').modal("show");
	}
	
	function Default(facebook_ads_lead_user_id)
	{
		$('.default_form_reset').click();
		$('#lead-default-validation').bootstrapValidator('resetForm', true);
		
		$('#default_facebook_ads_lead_user_id').val(facebook_ads_lead_user_id);
		$('#default_client_roi_check').css('display', 'none');
		
		$.get("{{ route('client_roi_status') }}",{facebook_ads_lead_user_id: facebook_ads_lead_user_id},function(data){ 
			if(data=='Yes')
			{
				$('#default_client_roi_check').css('display', 'none');
				$('#default_client_roi_automation').val(data);
				$('.default_total_profit').attr('id','default_profite_total');
			}
			else
			{
				$('#default_client_roi_check').css('display', 'block');
				$('#default_client_roi_automation').val(data);
				$('.default_total_profit').attr('id','default_total_profit');
			}
		});
		$('#default_modal').modal("show");
	}
	
	function LeadSetting(facebook_ads_lead_user_id)
	{
		$('.default_lead_setting').iCheck('check');
		$('#setting_facebook_ads_lead_user_id').val(facebook_ads_lead_user_id);
		$('#setting_comment').val('');
		$('.setting_comment').html('');
		$('#AutomotiveSetting').modal("show");
	}
	
	function ReSync(facebook_ads_lead_user_id)
	{
		$('#date-loader').show();
		$.get("{{ route('lead_re-sync') }}",{facebook_ads_lead_user_id: facebook_ads_lead_user_id},function(data){ 
			$('#date-loader').hide(); 
		});
	}
	
	function LegalLeadSetting(facebook_ads_lead_user_id)
	{
		$('.default_lead_setting').iCheck('check');
		$('#legal_setting_facebook_ads_lead_user_id').val(facebook_ads_lead_user_id);
		$('#legal_setting_comment').val('');
		$('.legal_setting_comment').html('');
		$('#LegalSetting').modal("show");
	}
	
	function AppointmentLeadSetting(facebook_ads_lead_user_id, status, appointment_date, appointment_time)
	{
		if(status == '' || status == undefined)
		{
			$('.default_appointment_lead_setting').iCheck('check');
			$('#reschedule_appointment_date').val('');
			$('#reschedule_appointment_time').val('');
		}
		else if(status == 'Reschedule')
		{
			$('input[value='+status).iCheck('check');
			$('#reschedule_appointment_date').val(appointment_date);
			$('#reschedule_appointment_time').val(appointment_time);
		}
		else
		{
			$('input[value='+status).iCheck('check');
			$('#reschedule_appointment_date').val('');
			$('#reschedule_appointment_time').val('');
		}
		
		$('#appointment_facebook_ads_lead_user_id').val(facebook_ads_lead_user_id);
		$('#AppointmentSetting').modal("show");
	}
	
	function EditLead(id)
	{
		$('.manual_lead_edit').html('');
		$.get("{{ route('lead_edit') }}",{id: id},function(data){ 
			$('#edit_facebook_ads_lead_user_id').val(id);
			$('#edit_first_name').val(data.first_name);
			$('#edit_last_name').val(data.last_name);
			$('#edit_phone').val(data.phone_number);
			$('#edit_email').val(data.email);
			$('#edit_full_address').val(data.street_address);
			$('#edit_zip').val(data.post_code);
			$('#edit_city').val(data.city);
			$('#edit_state').val(data.state);
			if(data.lead_type=='Pownder™ Call')
			{
				$('#EditLeadManaulModal').find(':radio[name=edit_lead_type][value="Pownder™ Call"]').iCheck('check');
			}
			else
			{
				$('#EditLeadManaulModal').find(':radio[name=edit_lead_type][value="Pownder™ Messenger"]').iCheck('check');
			}
		});
		$(".editButton").prop('disabled', false);
		$('#EditLeadManaulModal').modal("show");
	}
	
	function UserLeadAllot(facebook_ads_lead_user_id)
	{
		$('#manager_id').val('');
		$('#Action').val('Allot');
		$('.AllotUserLeadTitle').html('Allot Lead to User');
		$('#AllotUserLeadId').val(facebook_ads_lead_user_id);
		$('#AllotUserLead').modal("show");
	}
	
	function UserLeadReAllot(facebook_ads_lead_user_id, manager_id)
	{
		$('#manager_id').val(manager_id);
		$('#Action').val('Re-Allot');
		$('.AllotUserLeadTitle').html('Re-Allot Lead to another User');
		$('#AllotUserLeadId').val(facebook_ads_lead_user_id);
		$('#AllotUserLead').modal("show");
	}
</script>

<script>
	$(document).ready(function() {
		$('input[name=lead_setting]').on('ifChecked', function() {
			if($(this).val()=='Need CO-X' || $(this).val()=='Undecided' || $(this).val()=='Bad Credit' || $(this).val()=='Pre-Check')
			{
				$('.other_comment').css('display','block');
				$('#setting_comment').val('');
				$('.setting_comment').html('');
			}
			else
			{
				$('.other_comment').css('display','none');
				$('#setting_comment').val('');
				$('.setting_comment').html('');
			}
			
			if($(this).val()=='Appointment')
			{
				$('.Appointment').css('display','block');
				$('#appointment_date').val('');
				$('.appointment_date').html('');
				$('#appointment_time').val('');
				$('.appointment_time').html('');
			}
			else
			{
				$('.Appointment').css('display','none');
				$('#appointment_date').val('');
				$('.appointment_date').html('');
				$('#appointment_time').val('');
				$('.appointment_time').html('');
			}
		});
		
		$('input[name=legal_lead_setting]').on('ifChecked', function() {
			if($(this).val()=="Doesn't Qualify" || $(this).val()=='Undecided')
			{
				$('.legal_other_comment').css('display','block');
				$('#legal_setting_comment').val('');
				$('.legal_setting_comment').html('');
			}
			else
			{
				$('.legal_other_comment').css('display','none');
				$('#legal_setting_comment').val('');
				$('.legal_setting_comment').html('');
			}
			
			if($(this).val()=='Appointment')
			{
				$('.LegalAppointment').css('display','block');
				$('#legal_appointment_date').val('');
				$('.legal_appointment_date').html('');
				$('#legal_appointment_time').val('');
				$('.legal_appointment_time').html('');
			}
			else
			{
				$('.LegalAppointment').css('display','none');
				$('#legal_appointment_date').val('');
				$('.legal_appointment_date').html('');
				$('#legal_appointment_time').val('');
				$('.legal_appointment_time').html('');
			}
		});
		
		$('input[name=appointment_lead_setting]').on('ifChecked', function() {
			if($(this).val()=='Reschedule')
			{
				$('.AppointmentDiv').css('display','block');
				$('#reschedule_appointment_date').val('');
				$('.reschedule_appointment_date').html('');
				$('#reschedule_appointment_time').val('');
				$('.reschedule_appointment_time').html('');
			}
			else
			{
				$('.AppointmentDiv').css('display','none');
				$('#reschedule_appointment_date').val('');
				$('.reschedule_appointment_date').html('');
				$('#reschedule_appointment_time').val('');
				$('.reschedule_appointment_time').html('');
			}
		});
		
		$("#leadSettingForm").on("submit", function( event ) {
			event.preventDefault();
			$(".SettingSubmit").prop('disabled', true);
			$('.appointment_date').html('');
			$('.appointment_time').html('');
			var facebook_ads_lead_user_id=$('#setting_facebook_ads_lead_user_id').val();
			var lead_setting=$('input[name=lead_setting]:checked').val();
			var setting_comment=$('#setting_comment').val();
			var appointment_date=$('#appointment_date').val();
			var appointment_time=$('#appointment_time').val();
			
			if(lead_setting=='Appointment')
			{
				if(appointment_date=='' || appointment_time=='')
				{
					if(appointment_date=='')
					{
						$('.appointment_date').html('The appointment date field is required.');
					}
					
					if(appointment_time=='')
					{
						$('.appointment_time').html('The appointment time field is required.');
					}
					
					$(".SettingSubmit").prop('disabled', false);
					return false;
				}
			}
			
			$.post("{{ route('lead_setting') }}", $(this).serialize(), function (data) {
				//After Success
				$('#AutomotiveSetting').modal("hide");
				$('.default_lead_setting').iCheck('check');
				$('#setting_comment').val('');
				$('#appointment_date').val('');
				$('#appointment_time').val('');
				$(".SettingSubmit").prop('disabled', false);
				if(data=='Sold')
				{
					Sold(facebook_ads_lead_user_id);
				}	
				
				$('#table4').bootstrapTable('refresh');
			});
		});
		
		$("#AppointmentLeadSettingForm").on("submit", function( event ) {
			event.preventDefault();
			$(".AppointmentSettingSubmit").prop('disabled', true);
			$('.reschedule_appointment_date').html('');
			$('.reschedule_appointment_time').html('');
			var facebook_ads_lead_user_id=$('#appointment_facebook_ads_lead_user_id').val();
			var lead_setting=$('input[name=appointment_lead_setting]:checked').val();
			var appointment_date=$('#reschedule_appointment_date').val();
			var appointment_time=$('#reschedule_appointment_time').val();
			
			if(lead_setting=='Reschedule')
			{
				if(appointment_date=='' || appointment_time=='')
				{
					if(appointment_date=='')
					{
						$('.reschedule_appointment_date').html('The appointment date field is required.');
					}
					
					if(appointment_time=='')
					{
						$('.reschedule_appointment_time').html('The appointment time field is required.');
					}
					
					$(".AppointmentSettingSubmit").prop('disabled', false);
					return false;
				}
			}
			
			$.post("{{ route('appointment_lead_setting') }}", $(this).serialize(), function (data) {
				//After Success
				$('#AppointmentSetting').modal("hide");
				$('.default_appointment_lead_setting').iCheck('check');
				$('#reschedule_appointment_date').val('');
				$('#reschedule_appointment_time').val('');
				$(".AppointmentSettingSubmit").prop('disabled', false);
				if(data=='Sold')
				{
					Sold(facebook_ads_lead_user_id);
				}	
				
				$('#table4').bootstrapTable('refresh');
			});
		});
		
		$("#LegalLeadSettingForm").on("submit", function( event ) {
			event.preventDefault();
			$(".SettingSubmit").prop('disabled', true);
			$('.legal_appointment_date').html('');
			$('.legal_appointment_time').html('');
			var facebook_ads_lead_user_id=$('#legal_setting_facebook_ads_lead_user_id').val();
			var lead_setting=$('input[name=legal_lead_setting]:checked').val();
			var setting_comment=$('#legal_setting_comment').val();
			var appointment_date=$('#legal_appointment_date').val();
			var appointment_time=$('#legal_appointment_time').val();
			
			if(lead_setting=='Appointment')
			{
				if(appointment_date=='' || appointment_time=='')
				{
					if(appointment_date=='')
					{
						$('.legal_appointment_date').html('The appointment date field is required.');
					}
					
					if(appointment_time=='')
					{
						$('.legal_appointment_time').html('The appointment time field is required.');
					}
					
					$(".SettingSubmit").prop('disabled', false);
					return false;
				}
			}
			
			$.post("{{ route('legal_lead_setting') }}", $(this).serialize(), function (data) {
				//After Success
				$('#LegalSetting').modal("hide");
				$('.default_lead_setting').iCheck('check');
				$('#legal_setting_comment').val('');
				$('#legal_appointment_date').val('');
				$('#legal_appointment_time').val('');
				$(".SettingSubmit").prop('disabled', false);
				if(data=='Closed')
				{
					Legal(facebook_ads_lead_user_id);
				}	
				
				$('#table4').bootstrapTable('refresh');
			});
		});
		
		$("#manual_lead_add").on("submit", function( event ) {
			event.preventDefault();
			$(".addButton").prop('disabled', true);
			var checkedValue = $('.messageCheckbox:checked').val();
			$('.manual_lead_add').html('');
			var data=$(this).serialize();
			$.ajax({
				url: "{{ route('lead_add') }}",
				type:'POST',
				data: data,
				success: function(data) {
					var str = data.group_category_id;
					str1=str.replace("'","");
					group_category_id=str1.replace("'","");
					
					var str2 = data.facebook_ads_lead_user_id;
					str3=str2.replace("'","");
					facebook_ads_lead_user_id=str3.replace("'","");
					
					$(".addButton").prop('disabled', false);
					$('#AddLeadManaulModal').modal('hide');
					swal('Success!', 'Lead added successfully.', 'success');
					if(checkedValue=='Checkbox Checked')
					{
						if(group_category_id=='6')
						{
							Sold(facebook_ads_lead_user_id);
						}
						else if(group_category_id=='18')
						{	
							Legal(facebook_ads_lead_user_id);
						}
						else
						{
							Default(facebook_ads_lead_user_id);
						}
					}
					
					$('#table4').bootstrapTable('refresh');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$("."+key).html(value);
					});
					$(".addButton").prop('disabled', false);
				}       
			});
		});
		
		$("#manual_lead_edit").on("submit", function( event ) {
			event.preventDefault();
			
			$(".editButton").prop('disabled', true);
			
			$('.manual_lead_edit').html('');
			var data=$(this).serialize();
			
			$.ajax({
				url: "{{ route('lead_edit') }}",
				type:'POST',
				data: data,
				success: function(data) {
					$(".editButton").prop('disabled', false);
					$('#EditLeadManaulModal').modal('hide');
					swal('Success!', 'Lead updated successfully.', 'success');
					$('#table4').bootstrapTable('refresh');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$(".edit_"+key).html(value);
					});
					$(".editButton").prop('disabled', false);
				}       
			});
		});		
	});
	
	function AppointmentSearch(setting, status)
	{
		$('#search_setting').val(setting);
		$('#search_appointment_status').val(status);
		if($('[data-field="Appt. Date & Time" ]').prop("checked") == false){
			$('input[data-field="Appt. Date & Time"]').trigger('click');
		}
		
		$('#table4').bootstrapTable('refresh');
	}
	
	function hidePhoneNumber(facebook_ads_lead_user_id, phone_number_hide)
	{
		if(phone_number_hide == 'Yes')
		{
			swal({
				title: 'Are you sure?',
				text: "You want to activate DNC for this lead?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#22D69D',
				cancelButtonColor: '#FB8678',
				confirmButtonText: 'Yes, activate it!',
				cancelButtonText: 'No, cancel!',
				confirmButtonClass: 'btn',
				cancelButtonClass: 'btn'
				}).then(function () {
				$.get("{{ route('phone-number-hide') }}",{'id':facebook_ads_lead_user_id.toString(), 'phone_number_hide':phone_number_hide}).then(function(response){
					//swal('Deleted!', 'Leads re-sync successfully.', 'success');
					//window.location.reload(true);
					$('#table4').bootstrapTable('refresh');
				});
				}, function (dismiss) {
				// dismiss can be 'cancel', 'overlay',
				// 'close', and 'timer'
				if (dismiss === 'cancel') {
					//swal('Cancelled', 'Leads re-sync cancelled successfully.', 'error');
				}
			});
		}
		else if(phone_number_hide == 'No')
		{
			swal({
				title: 'Are you sure?',
				text: "You want to deactivate DNC for this lead?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#22D69D',
				cancelButtonColor: '#FB8678',
				confirmButtonText: 'Yes, deactivate it!',
				cancelButtonText: 'No, cancel!',
				confirmButtonClass: 'btn',
				cancelButtonClass: 'btn'
				}).then(function () {
				$.get("{{ route('phone-number-hide') }}",{'id':facebook_ads_lead_user_id.toString(), 'phone_number_hide':phone_number_hide}).then(function(response){
					//swal('Deleted!', 'Leads re-sync successfully.', 'success');
					//window.location.reload(true);
					$('#table4').bootstrapTable('refresh');
				});
				}, function (dismiss) {
				// dismiss can be 'cancel', 'overlay',
				// 'close', and 'timer'
				if (dismiss === 'cancel') {
					//swal('Cancelled', 'Leads re-sync cancelled successfully.', 'error');
				}
			});
		}
	}
</script>

<script>
	$(document).ready(function()
	{
		$('.checkall').click(function() {
			var checked = $(this).prop('checked');
			$('#table4').find('.LeadId').prop('checked', checked);
		});
		
		var _token = "{{ csrf_token() }}";
		$(".LeadResync").click(function() {
			var checkedRows = []
			$("input[class='LeadId']:checked").each(function ()
			{
				checkedRows.push($(this).val());
			});
			
			if(checkedRows.length==0)
			{
				swal('Error', 'Please select leads.', 'error');
			}
			else
			{
				swal({
					title: 'Are you sure?',
					text: "You want to re-sync selected leads?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, re-sync it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.post("{{ route('multiple-lead-re-sync') }}",{'id':checkedRows, _token:_token}).then(function(response){
						//swal('Deleted!', 'Leads re-sync successfully.', 'success');
						//window.location.reload(true);
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Leads re-sync cancelled successfully.', 'error');
					}
				});
			}
		});
		
		$(".DNC").click(function() {
			var phone_number_hide = $('#search_phone_number_hide').val();
			if(phone_number_hide == 'No')
			{
				$('#search_phone_number_hide').val('Yes');
				$(".dnc_icon").removeClass("fa-bell-slash");
				$(".dnc_icon").addClass("fa-bell");
				$(".dnc_icon").css("color", "#32CD32");
				$('#table4').bootstrapTable('refresh');
			}
			else if(phone_number_hide == 'Yes')
			{
				$('#search_phone_number_hide').val('No');
				$(".dnc_icon").removeClass("fa-bell");
				$(".dnc_icon").addClass("fa-bell-slash");
				$(".dnc_icon").css("color", "#e01800");
				$('#table4').bootstrapTable('refresh');
			}
		});
		
		$('#search_lead_type').change(function(){
			$('#table4').bootstrapTable('refresh');
		});
	});
</script>
<!-- end of page level js -->
@stop				