@extends('layouts/default')

{{-- Page title --}}
@section('title')
FTP Connect List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	
	.export.btn-group {*display:none!important;}
	.fixed-table-toolbar {*width:60%;}
	.fixed-table-toolbar .search {width: Calc( 100% - 152px );}
	
	@media screen and (max-width: 767px) {
	.fixed-table-toolbar {width: 100%;}
	.fixed-table-toolbar {width:100%;}
	.fixed-table-toolbar .search {width: Calc( 100% );}
	}
	
	.fixed-table-container {height: 500px !important; }
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
			</li>
		<li class="active">
			FTP Connect List
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content p-l-r-15">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> FTP Connect List
					</h3>
				</div>
				<div class="panel-body">
					<table id="FTPAccountList" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20, 40, ALL]" data-show-footer="false" data-click-to-select="true">
						<thead>
							<tr>
								<th data-field="Name" data-sortable="true" @if(in_array("0",$TableColumn)) data-visible="false" @endif>Name</th>
								<th data-field="Provider" data-sortable="true" @if(in_array("1",$TableColumn)) data-visible="false" @endif>Provider</th>
								<th data-field="Use SFTP" data-sortable="true" @if(in_array("2",$TableColumn)) data-visible="false" @endif>Use SFTP</th>
								<th data-field="FTP Host" data-sortable="true" @if(in_array("3",$TableColumn)) data-visible="false" @endif>FTP Host</th>
								<th data-field="FTP Port" data-sortable="true" @if(in_array("4",$TableColumn)) data-visible="false" @endif>FTP Port</th>
								<th data-field="FTP Username" data-sortable="true" @if(in_array("5",$TableColumn)) data-visible="false" @endif>FTP Username</th>
								<th data-field="FTP Password" data-sortable="true" @if(in_array("6",$TableColumn)) data-visible="false" @endif>FTP Password</th>
								<th data-field="Status" data-sortable="true" @if(in_array("7",$TableColumn)) data-visible="false" @endif>Status</th>
								<th data-field="Actions" data-sortable="true" @if(in_array("8",$TableColumn)) data-visible="false" @endif>Actions</th>
							</tr>
						</thead>
						<tbody>
							@php $i=1; @endphp
							@foreach($ftp_accounts as $ftp_account)
							<tr>
								<td>{{ $ftp_account->name }}</td>
								<td>{{ $ftp_account->provider }}</td>
								<td>{{ $ftp_account->use_sftp }}</td>
								<td>{{ $ftp_account->ftp_host }}</td>
								<td>{{ $ftp_account->ftp_port }}</td>
								<td>{{ $ftp_account->username }}</td>
								<td>{{ $ftp_account->password }}</td>
								<td>@if($ftp_account->status==1) Active @else Inactive @endif</td>
								<td>
									<a href="edit-ftp-account/{{ $ftp_account->id }}" title="Edit"><i class="fa fa-fw fa-pencil-square-o" aria-hidden="true"></i></a> 
									
									<a href="#" id="{{ $ftp_account->id }}" class="delete_ftp_account">
										<i class="fa fa-fw fa-times text-danger actions_icon" title="Delete FTP Account"></i>
									</a> 
									@if($ftp_account->status==1)
									<a href="#" inactive_id="{{ $ftp_account->id }}" class="inactive_ftp_account">
										<i class="fa fa-fw fa-star text-danger actions_icon" title="Inactive FTP Account"></i>
									</a>
									@else
									<a href="#" active_id="{{ $ftp_account->id }}" class="active_ftp_account">
										<i class="fa fa-fw fa-star text-success actions_icon" title="Active FTP Account"></i>
									</a>
									@endif
									
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<script>
	$(document).ready(function(){
		$('#FTPAccountList').bootstrapTable();
		
		var extra_features = '<a href="add-ftp-account" class="btn btn-default" title="New FTP Connect" ><i class="fa fa-fw fa-plus-square"></i></a>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
		
		$('.dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='FTP Account List Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$('a.delete_ftp_account').each(function(){ 
			var btn=$(this);  
			$(this).click(function(e){
				swal({
					title: 'Are you sure?',
					text: "You want to delete this FTP account!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, delete it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.get('delete-ftp-account',{'id':btn.attr('id')}).then(function(){
						swal('Deleted!', 'Your FTP account has been deleted.', 'success');
						window.location.reload(true); 
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Your FTP account is safe.', 'error');
					}
				})
			});
		});
		
		$('a.active_ftp_account').each(function(){ 
			var btn=$(this);  
			$(this).click(function(e){
				swal({
					title: 'Are you sure?',
					text: "You want to active this FTP account!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, active it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.get('active-ftp-account',{'id':btn.attr('active_id')}).then(function(){
						
						swal('Active!', 'Your FTP account has been active.', 'success');
						window.location.reload(true); 
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Your FTP account is inactive.', 'error');
					}
				})
			});
		});
		
		$('a.inactive_ftp_account').each(function(){ 
			var btn=$(this);  
			$(this).click(function(e){
				swal({
					title: 'Are you sure?',
					text: "You want to inactive this FTP account!",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, inactive it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.get('inactive-ftp-account',{'id':btn.attr('inactive_id')}).then(function(){
						
						swal('Inactive!', 'Your FTP account has been inactive.', 'success');
						window.location.reload(true); 
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Your FTP account is active.',	'error');
					}
				})
			});
		});
	});
</script>
@stop