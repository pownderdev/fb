@extends('layouts/default')
 
{{-- Page title --}} 
@section('title') 
Invoice Report @parent 
@stop 

{{-- page level styles --}} 
@section('header_styles')
<!--start page level css -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" type="text/css"  href="assets/css/custom_css/chartjs-charts.css" >
<link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-table/css/bootstrap-table.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/custom_css/bootstrap_tables.css">
<!--end page level css-->
@stop 
{{-- Page content --}}
@section('content')
<?php 
//Money Format
 function money_format1($number)
{
  setlocale(LC_MONETARY, 'en_US'); 
  return money_format('%!.2i',$number); 
}
function divide($sales,$qty)
{
  return $sales/$qty;  
}

?>
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
			<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
        <li>
           <a href="{{ route('invoice') }}">
			 Invoice
			</a>
        </li>
		<li class="active">
		  Invoice Report
		</li>
	</ol>
</section>
<!-- Main content -->
  
<section class="main-content" id="invoice_report_body">
 
	<div class="col-md-12">
		<div class="row  invoice_report">
        
			     <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" />
                 <input type="hidden" name="bar_chart_data" id="bar_chart_data" value="<?php echo implode(',',array_values($total_paid_amount_month_wise)); ?>" />
			     <input type="hidden" name="pie_chart_data" id="pie_chart_data" value="<?php if(count($top2highlypaid)==2){echo $top2highlypaid[0]->client_name.'<br/>'.$top2highlypaid[0]->balance.'*@*'.$top2highlypaid[1]->client_name.'<br/>'.$top2highlypaid[1]->balance.'*@*'.'Other<br/>'.$overallamount;}else if(count($top2highlypaid)==1){echo $top2highlypaid[0]->client_name.'<br/>'.$top2highlypaid[0]->balance.'*@*'.'Other<br/>'.$overallamount;}else { echo 'Other<br/>'.$overallamount; } ?>" />
                 <input type="hidden" name="pie_chart_data1" id="pie_chart_data1" value="<?php if(count($top2highlypaid)==2){echo $top2highlypaid[0]->balance.'*@*'.$top2highlypaid[1]->balance.'*@*'.$overallamount;}else if(count($top2highlypaid)==1){echo $top2highlypaid[0]->balance.'*@*'.$overallamount;}else { echo $overallamount; } ?>" />
                 <input type="hidden" name="bar_chart_data_type" id="bar_chart_data_type" value="overall"  />
                 <input type="hidden" name="bar_chart_no_of_month" id="bar_chart_no_of_month" value="{{date('M')}}"  />
                 
                 <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 open-table" id="Day">
						<div class="widget" data-count=".num" data-from="0" data-to="512" data-duration="3">
							<div class="canvas-interactive-wrapper2">
								<canvas id="canvas-interactive2" class="grad1"></canvas>
								<a href="javascript:;"> 
									<div class="cta-wrapper2">
										
										<div class="widget-count panel-white">
											<div class="item-label text-center">
											    <span class="title">Sales By</span>
												<div id="" class="count-box" style="">Day</div>
												
											</div>
										</div>
										<div class="item">
											<div class="widget-icon pull-right icon-color animation-fadeIn">
												<i class="fa fa-fw fa-usd black fa-size"></i>
											</div>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 open-table" id="Month">
						<div class="widget" data-count=".num" data-from="0" data-to="512" data-duration="3">
							<div class="canvas-interactive-wrapper2">
								<canvas id="canvas-interactive2" class="grad1"></canvas>
								<a href="javascript:;"> 
									<div class="cta-wrapper2">
										
										<div class="widget-count panel-white">
											<div class="item-label text-center">
											    <span class="title">Sales By</span>
												<div id="" class="count-box" style="">Month</div>
												
											</div>
										</div>
										<div class="item">
											<div class="widget-icon pull-right icon-color animation-fadeIn">
												<i class="fa fa-fw fa-usd black fa-size"></i>
											</div>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 open-table" id="Quarter">
						<div class="widget" data-count=".num" data-from="0" data-to="512" data-duration="3">
							<div class="canvas-interactive-wrapper2">
								<canvas id="canvas-interactive2" class="grad1"></canvas>
								<a href="javascript:;"> 
									<div class="cta-wrapper2">
										
										<div class="widget-count panel-white">
											<div class="item-label text-center">
											    <span class="title">Sales By</span>
												<div id="" class="count-box" style="">Quarter</div>
												
											</div>
										</div>
										<div class="item">
											<div class="widget-icon pull-right icon-color animation-fadeIn">
												<i class="fa fa-fw fa-usd black fa-size"></i>
											</div>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 open-table" id="Year">

						<div class="widget" data-count=".num" data-from="0" data-to="512" data-duration="3">
							<div class="canvas-interactive-wrapper2">
								<canvas id="canvas-interactive2" class="grad1"></canvas>
								<a href="javascript:;"> 
									<div class="cta-wrapper2">
										
										<div class="widget-count panel-white">
											<div class="item-label text-center">
											    <span class="title">Sales By</span>
												<div id="" class="count-box" style="">Year</div>
												
											</div>
										</div>
										<div class="item">
											<div class="widget-icon pull-right icon-color animation-fadeIn">
												<i class="fa fa-fw fa-usd black fa-size"></i>
											</div>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 open-table" id="Customer">
						<div class="widget" data-count=".num" data-from="0" data-to="512" data-duration="3">
							<div class="canvas-interactive-wrapper2">
								<canvas id="canvas-interactive2" class="grad1"></canvas>
								<a href="javascript:;"> 
									<div class="cta-wrapper2">
										
										<div class="widget-count panel-white">
											<div class="item-label text-center">
											    <span class="title">Sales By</span>
												<div id="" class="count-box" style="">Customer</div>
												
											</div>
										</div>
										<div class="item">
											<div class="widget-icon pull-right icon-color animation-fadeIn">
												<i class="fa fa-fw fa-user-o  black fa-size"></i>
											</div>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					  
					
					<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 open-table" id="Item">
						<div class="widget" data-count=".num" data-from="0" data-to="512" data-duration="3">
							<div class="canvas-interactive-wrapper2">
								<canvas id="canvas-interactive2" class="grad1"></canvas>
								<a href="javascript:;"> 
									<div class="cta-wrapper2">
										
										<div class="widget-count panel-white">
											<div class="item-label text-center">
											    <span class="title">Sales By</span>
												<div id="" class="count-box" style="">Items</div>
												
											</div>
										</div>
										<div class="item">
											<div class="widget-icon pull-right icon-color animation-fadeIn">
												<i class="fa fa-fw fa-dropbox black fa-size"></i>
											</div>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
			
			
			
					
		</div>
	</div>
	
	
	<!--tabel section-->
	<section  class="table-sec container_sales_report_day">
	
				<div class="col-lg-12">
					<div class="panel panel-success filterable">
						<div class="panel-heading">
							<h3 class="panel-title">
								<i class="fa fa-fw fa-th-large"></i> Sales By Day
							</h3>
						</div>
						<div class="panel-body panel_sales_report_day">
                            <div class="row  clearfix text-center">
                              <div class="col-md-6 pull-right">
                                <div class="table-responsive">
                                   <table class="table table-bordered table-condensed summary_day">
                                      <tr>
                                        <th class="text-center" rowspan="2" style="vertical-align: middle;">Total</th>
                                        <th class="text-center">Sales</th>
                                        <th class="text-center">Paid</th>
                                        <th class="text-center">Owed</th>
                                        <th class="text-center">Tax</th>
                                      </tr>
                                      <?php $sum_total_unpaid=array_sum(array_column($sales_by_day,"unpaid"));$sum_total_paid=array_sum(array_column($sales_by_day,"paid"));$sum_tax_overall=array_sum(array_column($sales_by_day,"tax")); ?>
                                      <tr>
                                        <td>${{money_format1($sum_total_unpaid+$sum_total_paid)}}</td>
                                        <td>${{money_format1($sum_total_paid)}}</td>
                                        <td>${{money_format1($sum_total_unpaid)}}</td>
                                        <td>${{money_format1($sum_tax_overall)}}</td>
                                      </tr>
                                   </table>
                                  </div> 
                                </div>
						   </div>
							
							<table id="table_sales_report_day" data-toolbar="#toolbar" data-search="true" data-show-refresh="false"
							data-show-toggle="true" data-show-columns="true" data-show-export="true"
							data-detail-view="true" data-detail-formatter="detailFormatter"
							data-minimum-count-columns="2" data-show-pagination-switch="true"
							data-pagination="true" data-id-field="id" data-page-list="[10, 20,40, ALL]"
							data-show-footer="false" data-height="503">
								<thead>
									<tr>
										<th data-field="Day" data-sortable="true" >Day</th>
										<th data-field="Sales" data-sortable="true" >Sales</th>
										<th data-field="Paid" data-sortable="true" >Paid</th>
										<th data-field="Owed" data-sortable="true" >Owed</th>
										<th data-field="Tax" data-sortable="true" >Tax</th>
									</tr>
								</thead>
								<tbody id="body_sales_report_day">
																		
								</tbody>
                                
							</table>
                            <table class="no-display" id="backup_sales_report_day">
									@foreach($sales_by_day as $key=>$value)
									<tr>
										<td><a target="_blank" title="View Invoices" href="manage-invoice?filter={{$key}}&type=day">{{date('m/d/Y',strtotime($key))}}</a></td>
										<td>${{money_format1($value["unpaid"]+$value["paid"])}}</td>
										<td>${{money_format1($value["paid"])}}</td>
										<td>${{money_format1($value["unpaid"])}}</td>
										<td>${{money_format1($value["tax"])}}</td>
									</tr>    
                                    @endforeach  									
							</table>
						</div>
					</div>
				</div>
			<!--fourth table end-->
	</section>
    <!--- Sale by month-->
    <section  class="table-sec container_sales_report_month">
	
				<div class="col-lg-12">
					<div class="panel panel-success filterable">
						<div class="panel-heading">
							<h3 class="panel-title">
								<i class="fa fa-fw fa-th-large"></i> Sales By Month
							</h3>
						</div>
						<div class="panel-body panel_sales_report_month">
                        
                           <div class="row  clearfix text-center">
                              <div class="col-md-6 pull-right">
                                <div class="table-responsive">
                                   <table class="table table-bordered table-condensed summary_month">
                                      <tr>
                                        <th class="text-center" rowspan="2" style="vertical-align: middle;">Total</th>
                                        <th class="text-center">Sales</th>
                                        <th class="text-center">Paid</th>
                                        <th class="text-center">Owed</th>
                                        <th class="text-center">Tax</th>
                                      </tr>
                                      <?php $sum_total_overall=array_sum(array_column($sales_by_month,"overall"));$sum_total_paid=array_sum(array_column($sales_by_month,"paid"));$sum_tax_overall=array_sum(array_column($sales_by_month,"tax")); ?>
                                      <tr>
                                        <td>${{money_format1($sum_total_overall)}}</td>
                                        <td>${{money_format1($sum_total_paid)}}</td>
                                        <td>${{money_format1($sum_total_overall-$sum_total_paid)}}</td>
                                        <td>${{money_format1($sum_tax_overall)}}</td>
                                      </tr>
                                   </table>
                                  </div> 
                                </div>
						   </div>						
							
							<table id="table_sales_report_month" data-toolbar="#toolbar" data-search="true" data-show-refresh="false"
							data-show-toggle="true" data-show-columns="true" data-show-export="true"
							data-detail-view="true" data-detail-formatter="detailFormatter"
							data-minimum-count-columns="2" data-show-pagination-switch="true"
							data-pagination="true" data-id-field="id" data-page-list="[10, 20,40, ALL]"
							data-show-footer="false" data-height="503">
								<thead>
									<tr>
										<th data-field="Month" data-sortable="true" >Month</th>
										<th data-field="Sales" data-sortable="true" >Sales</th>
										<th data-field="Paid" data-sortable="true" >Paid</th>
										<th data-field="Owed" data-sortable="true" >Owed</th>
										<th data-field="Tax" data-sortable="true" >Tax</th>
									</tr>
								</thead>
								<tbody id="body_sales_report_month">
																		
								</tbody>                                
							</table>
                            <table class="no-display" id="backup_sales_report_month">
									@foreach($sales_by_month as $key=>$value)
									<tr>
										<td><a target="_blank" title="View Invoices" href="manage-invoice?filter={{date('Y-m',strtotime($key))}}&type=month">{{date('F Y',strtotime($key))}}</a></td>
										<td>${{money_format1($value["overall"])}}</td>
										<td>${{money_format1($value["paid"])}}</td>
										<td>${{money_format1($value["overall"]-$value["paid"])}}</td>
										<td>${{money_format1($value["tax"])}}</td>
									</tr>    
                                    @endforeach  									
							</table>
						</div>
					</div>
				</div>
			<!--fourth table end-->
	</section>
    <!-- Sales Report By Quarter -->
	<section  class="table-sec container_sales_report_quarter">
	
				<div class="col-lg-12">
					<div class="panel panel-success filterable">
						<div class="panel-heading">
							<h3 class="panel-title">
								<i class="fa fa-fw fa-th-large"></i> Sales By Quarter
							</h3>
						</div>
						<div class="panel-body panel_sales_report_quarter">
						   
                            <div class="row  clearfix text-center">
                              <div class="col-md-6 pull-right">
                                <div class="table-responsive">
                                   <table class="table table-bordered table-condensed summary_quarter">
                                      <tr>
                                        <th class="text-center" rowspan="2" style="vertical-align: middle;">Total</th>
                                        <th class="text-center">Sales</th>
                                        <th class="text-center">Paid</th>
                                        <th class="text-center">Owed</th>
                                        <th class="text-center">Tax</th>
                                      </tr>
                                      <?php $sum_total_overall=array_sum(array_column($sales_by_quarter,"overall"));$sum_total_paid=array_sum(array_column($sales_by_quarter,"paid"));$sum_tax_overall=array_sum(array_column($sales_by_quarter,"tax")); ?>
                                      <tr>
                                        <td>${{money_format1($sum_total_overall)}}</td>
                                        <td>${{money_format1($sum_total_paid)}}</td>
                                        <td>${{money_format1($sum_total_overall-$sum_total_paid)}}</td>
                                        <td>${{money_format1($sum_tax_overall)}}</td>
                                      </tr>
                                   </table>
                                  </div> 
                                </div>
						   </div>
							
							<table id="table_sales_report_quarter" data-toolbar="#toolbar" data-search="true" data-show-refresh="false"
							data-show-toggle="true" data-show-columns="true" data-show-export="true"
							data-detail-view="true" data-detail-formatter="detailFormatter"
							data-minimum-count-columns="2" data-show-pagination-switch="true"
							data-pagination="true" data-id-field="id" data-page-list="[10, 20,40, ALL]"
							data-show-footer="false" data-height="503">
								<thead>
									<tr>
										<th data-field="Quarter" data-sortable="true" >Quarter</th>
										<th data-field="Sales" data-sortable="true" >Sales</th>
										<th data-field="Paid" data-sortable="true" >Paid</th>
										<th data-field="Owed" data-sortable="true" >Owed</th>
										<th data-field="Tax" data-sortable="true" >Tax</th>
									</tr>
								</thead>
								<tbody id="body_sales_report_quarter">
																		
								</tbody>
                                
							</table>
                            <table class="no-display" id="backup_sales_report_quarter">
									@foreach($sales_by_quarter as $key=>$value)
									<tr>
										<td><a target="_blank" title="View Invoices" href="manage-invoice?filter={{$key}}&type=quarter">{{ucfirst(str_replace("_"," ",$key))}}</a></td>
										<td>${{money_format1($value["overall"])}}</td>
										<td>${{money_format1($value["paid"])}}</td>
										<td>${{money_format1($value["overall"]-$value["paid"])}}</td>
										<td>${{money_format1($value["tax"])}}</td>
									</tr>    
                                    @endforeach  									
						   </table>
						</div>
					</div>
				</div>
			<!--fourth table end-->
	</section>
    <section  class="table-sec container_sales_report_year">
	
				<div class="col-lg-12">
					<div class="panel panel-success filterable">
						<div class="panel-heading">
							<h3 class="panel-title">
								<i class="fa fa-fw fa-th-large"></i> Sales By Year
							</h3>
						</div>
						<div class="panel-body panel_sales_report_year">
						  
                            <div class="row  clearfix text-center">
                              <div class="col-md-6 pull-right">
                                <div class="table-responsive">
                                   <table class="table table-bordered table-condensed summary_year">
                                      <tr>
                                        <th class="text-center" rowspan="2" style="vertical-align: middle;">Total</th>
                                        <th class="text-center">Sales</th>
                                        <th class="text-center">Paid</th>
                                        <th class="text-center">Owed</th>
                                        <th class="text-center">Tax</th>
                                      </tr>
                                      <?php $sum_total_overall=array_sum(array_column($sales_by_year,"overall"));$sum_total_paid=array_sum(array_column($sales_by_year,"paid"));$sum_tax_overall=array_sum(array_column($sales_by_year,"tax")); ?>
                                      <tr>
                                        <td>${{money_format1($sum_total_overall)}}</td>
                                        <td>${{money_format1($sum_total_paid)}}</td>
                                        <td>${{money_format1($sum_total_overall-$sum_total_paid)}}</td>
                                        <td>${{money_format1($sum_tax_overall)}}</td>
                                      </tr>
                                   </table>
                                  </div> 
                                </div>
						    </div>
							
							<table id="table_sales_report_year" data-toolbar="#toolbar" data-search="true" data-show-refresh="false"
							data-show-toggle="true" data-show-columns="true" data-show-export="true"
							data-detail-view="true" data-detail-formatter="detailFormatter"
							data-minimum-count-columns="2" data-show-pagination-switch="true"
							data-pagination="true" data-id-field="id" data-page-list="[10, 20,40, ALL]"
							data-show-footer="false" data-height="503">
								<thead>
									<tr>
										<th data-field="Year" data-sortable="true" >Year</th>
										<th data-field="Sales" data-sortable="true" >Sales</th>
										<th data-field="Paid" data-sortable="true" >Paid</th>
										<th data-field="Owed" data-sortable="true" >Owed</th>
										<th data-field="Tax" data-sortable="true" >Tax</th>
									</tr>
								</thead>
								<tbody id="body_sales_report_year">
									 									
								</tbody>                                
							</table>
                            <table class="no-display" id="backup_sales_report_year">
									@foreach($sales_by_year as $key=>$value)
									<tr>
										<td><a target="_blank" title="View Invoices" href="manage-invoice?filter={{$key}}&type=year">{{$key}}</a></td>
										<td>${{money_format1($value["overall"])}}</td>
										<td>${{money_format1($value["paid"])}}</td>
										<td>${{money_format1($value["overall"]-$value["paid"])}}</td>
										<td>${{money_format1($value["tax"])}}</td>
									</tr>    
                                    @endforeach  									
							</table>
						</div>
					</div>
				</div>
			<!--fourth table end-->
	</section>
    
    <section  class="table-sec container_sales_report_customer">
	
				<div class="col-lg-12">
					<div class="panel panel-success filterable">
						<div class="panel-heading">
							<h3 class="panel-title">
								<i class="fa fa-fw fa-th-large"></i> Sales By Customer
							</h3>
						</div>
						<div class="panel-body panel_sales_report_customer">					
						   <input type="hidden" name="trigger_client" id="trigger_client" value="@if(session('user')->category=='vendor'){{'1'}}@else{{'0'}}@endif" />
                           
                           <div class="row  clearfix text-center">
                              <div class="col-md-6 pull-right">
                                <div class="table-responsive">
                                   <table class="table table-bordered table-condensed summary_customer">
                                      <tr>
                                        <th class="text-center" rowspan="2" style="vertical-align: middle;">Total</th>
                                        <th class="text-center">Sales</th>
                                        <th class="text-center">Paid</th>
                                        <th class="text-center">Owed</th>
                                        <th class="text-center">Tax</th>
                                      </tr>
                                      <?php $sum_total_overall=array_sum(array_column($sales_by_customer,"overall"));$sum_total_paid=array_sum(array_column($sales_by_customer,"paid"));$sum_tax_overall=array_sum(array_column($sales_by_customer,"tax")); ?>
                                      <tr>
                                        <td>${{money_format1($sum_total_overall)}}</td>
                                        <td>${{money_format1($sum_total_paid)}}</td>
                                        <td>${{money_format1($sum_total_overall-$sum_total_paid)}}</td>
                                        <td>${{money_format1($sum_tax_overall)}}</td>
                                      </tr>
                                   </table>
                                  </div> 
                                </div>
						    </div>
    
							<table id="table_sales_report_customer" data-toolbar="#toolbar" data-search="true" data-show-refresh="false"
							data-show-toggle="true" data-show-columns="true" data-show-export="true"
							data-detail-view="true" data-detail-formatter="detailFormatter"
							data-minimum-count-columns="2" data-show-pagination-switch="true"
							data-pagination="true" data-id-field="id" data-page-list="[10, 20,40, ALL]"
							data-show-footer="false" data-height="503">
								<thead>
									<tr>
										<th data-field="Customer" data-sortable="true" >Customer</th>
										<th data-field="Sales" data-sortable="true" >Sales</th>
										<th data-field="Paid" data-sortable="true" >Paid</th>
										<th data-field="Owed" data-sortable="true" >Owed</th>
										<th data-field="Tax" data-sortable="true" >Tax</th>
									</tr>
								</thead>
								<tbody id="body_sales_report_customer">
																		
								</tbody>                                
							</table>
                            <table class="no-display" id="backup_sales_report_customer">
									@foreach($sales_by_customer as $key=>$value)
									<tr class="@if($value['client_type']!='client') active @endif">
										<td><a target="_blank" title="View Invoices" href="manage-invoice?filter={{urlencode($key)}}&type=customer">{{$value["name"]}}</a></td>
										<td>${{money_format1($value["overall"])}}</td>
										<td>${{money_format1($value["paid"])}}</td>
										<td>${{money_format1($value["overall"]-$value["paid"])}}</td>
										<td>${{money_format1($value["tax"])}}</td>
									</tr>    
                                    @endforeach  									
							</table>
						</div>
					</div>
				</div>
			<!--fourth table end-->
	</section>
    <!-- Sales by items -->
    <section  class="table-sec container_sales_report_item">
	
				<div class="col-lg-12">
					<div class="panel panel-success filterable">
						<div class="panel-heading">
							<h3 class="panel-title">
								<i class="fa fa-fw fa-th-large"></i> Sales By Items
							</h3>
						</div>
						<div class="panel-body panel_sales_report_item">
                        
                           <div class="row  clearfix text-center">
                              <div class="col-md-6 pull-right">
                                <div class="table-responsive">
                                   <table class="table table-bordered table-condensed summary_item">
                                      <tr>
                                        <th class="text-center" rowspan="2" style="vertical-align: middle;">Total</th>
                                        <th class="text-center">Sales</th>
                                        <th class="text-center">Quantity</th>
                                        <th class="text-center">Avg Rate</th>
                                        <th class="text-center">Tax</th>
                                      </tr>
                                      <?php $overallsales=array_column($sales_by_item,"overall");$overallqty=array_column($sales_by_item,"qty");$overallavgrate=array_map("divide",$overallsales,$overallqty); $sum_total_sales=array_sum($overallsales);$sum_total_qty=array_sum($overallqty);$sum_total_rate=array_sum($overallavgrate);$sum_tax_overall=array_sum(array_column($sales_by_item,"tax")); ?>
                                      <tr>
                                        <td>${{money_format1($sum_total_sales)}}</td>
                                        <td>{{money_format1($sum_total_qty)}}</td>
                                        <td>${{money_format1($sum_total_rate)}}</td>
                                        <td>${{money_format1($sum_tax_overall)}}</td>
                                      </tr>
                                   </table>
                                  </div> 
                                </div>
						    </div>					
						  
							<table id="table_sales_report_item" data-toolbar="#toolbar" data-search="true" data-show-refresh="false"
							data-show-toggle="true" data-show-columns="true" data-show-export="true"
							data-detail-view="true" data-detail-formatter="detailFormatter"
							data-minimum-count-columns="2" data-show-pagination-switch="true"
							data-pagination="true" data-id-field="id" data-page-list="[10, 20,40, ALL]"
							data-show-footer="false" data-height="503">
								<thead>
									<tr>
										<th data-field="Item" data-sortable="true" >Item</th>
										<th data-field="Sales" data-sortable="true" >Sales</th>
										<th data-field="Quantity" data-sortable="true" >Quantity</th>
										<th data-field="Avg_Rate" data-sortable="true" >Avg Rate</th>
										<th data-field="Tax" data-sortable="true" >Tax</th>
									</tr>
								</thead>
								<tbody id="body_sales_report_item">
									  									
								</tbody>                                
							</table>
                            <table class="no-display" id="backup_sales_report_item">                                    
									@foreach($sales_by_item as $key=>$value)
                                    <?php $item_name=($value["item_name"] && $key) ?$value["item_name"] :'Others(Deleted Items)'; 
                                          $item_name_key=($value["item_name"] && $key) ? $key :'other';
                                    ?>                                    
									<tr>
										<td title="@if(!$value['item_name']){{str_replace('*@*',' , ',$value['item_name1'])}}@endif">
                                        <a target="_blank" title="View Invoices" href="manage-invoice?filter={{urlencode($item_name_key)}}&type=item">{{$item_name}}</a>                                        
                                        </td>
										<td>${{money_format1($value["overall"])}}</td>
										<td>{{money_format1($value["qty"])}}</td>
										<td>${{money_format1($value["overall"]/$value["qty"])}}</td>
										<td>${{money_format1($value["tax"])}}</td>
									</tr>    
                                    @endforeach  									
							</table>
						</div>
					</div>
				</div>
			<!--fourth table end-->
	</section>
	<!--graph section-->
	<section  class="after-table-sec">
		<div class="col-md-12">
			<div class="row ">
                <a href="manage-invoice?filter=outstanding">
				<div class="load-charts-outstanding col-md-3 col-sm-4 col-xs-12 col-md-offset-3 col-sm-offset-2">
					<div class="wrapper_sec ">
					  <div class="details_wrapper outstanding">
						   <div class="top-content">
                           <div class="big-font">
                             <h3>
                                <big>{{$count_total_sent_invoices}} </big>
                                <span class="black">${{$amount_total_sent_invoices}}
                                <br />
                                OUTSTANDING
                                </span> 
                             </h3>
                             
                           </div>
                           </div>						   
					  </div>
					</div>
				</div>
                </a>
                <a href="manage-invoice?filter=overdue">                                
				<div class="load-charts-overdue col-md-3 col-sm-4 col-xs-12">
					<div class="wrapper_sec">
					  <div class="details_wrapper overdue">
						   <div class="top-content">
                            <div class="big-font">
                                <h3>
                                    <big>{{$count_overdue_invoices}} </big>
                                    <span class="black">${{$amount_overdue_invoices}}</span>
                                    <br />
                                    <span class="red">OVERDUE</span>
                                </h3>
                            </div>
                         </div>						   
					  </div>
					</div>
				</div>
                </a>                
			</div>
		</div>
		
		
		<div class="col-md-12">
		 <!-- row -->
				<div class="row">
					<div class="col-lg-6">
						<!-- Basic charts strats here-->
						<div class="panel panel-info">
							<div class="panel-heading">
								<h4 class="panel-title">
                                    <span class="load-charts-overall invisible" ></span>
									<i class="fa fa-fw fa-bar-chart-o"></i> Sales (<i class="fa fa-usd"></i>): <span  id="date_range_filter_bar_chart"><?php echo  'Jan '.date('Y').' - '.date('M Y'); ?></span>
								</h4>
                                <div id="daterange_picker_filter_bar_chart" title="Filter Chart" class="pull-right" >                                    
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                    <span></span> <b class="caret"></b>
                                    <input type="hidden" id="range_filter_bar_chart" />
                                </div>

								<!-- <span class="pull-right">
										<i class="fa fa-fw fa-chevron-up clickable"></i>
										<i class="fa fa-fw fa-times removepanel clickable"></i>
									</span>-->
							</div>
							<div class="panel-body">
								<div id="bar-chart">
									
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<!-- Basic charts strats here-->
						<div class="panel panel-info">
							<div class="panel-heading">
								<h4 class="panel-title">
									<i class="fa fa-fw fa-pie-chart"></i> Top Billed Clients
								</h4>
								 <!--<span class="pull-right">
										<i class="fa fa-fw fa-chevron-up clickable"></i>
										<i class="fa fa-fw fa-times removepanel clickable"></i>
									</span>-->
							</div>
							<div class="panel-body">
								<div id="pie-chart">
									
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>
	
	</section>
	
</section>





<!-- /.content -->
@stop 

{{-- page level scripts --}} 
@section('footer_scripts')
<!-- begining of page level js -->
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- date-range-picker -->
<script type="text/javascript" src="assets/vendors/daterangepicker/js/daterangepicker.js"></script>
<!-- begining of page level js -->
<script src="assets/vendors/chartjs/js/invoice_report_charts.js"></script>
<!-- begining of page level js -->
<script type="text/javascript" src="assets/vendors/editable-table/js/mindmup-editabletable.js"></script>
<script type="text/javascript" src="assets/vendors/bootstrap-table/js/bootstrap-table.min.js"></script>
<script type="text/javascript" src="assets/vendors/tableExport.jquery.plugin/tableExport.min.js"></script>
<script type="text/javascript" src="assets/js/custom_js/bootstrap_tables.js"></script>
<script type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/custom.js')}}"></script>


@stop