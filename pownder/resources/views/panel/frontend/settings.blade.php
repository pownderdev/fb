@extends('layouts/default')

{{-- Page title --}}
@section('title')
Settings
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/form_layouts.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/advbuttons.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/js/sumoselect/sumoselect.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/toolbar/css/jquery.toolbar.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/tab.css')}}">
<link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="{{asset('assets/css/custom_css/switches.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datedropper/datedropper.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/bootstrap3-wysihtml5-bower/css/bootstrap3-wysihtml5.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/form_editors.css')}}"/>
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	.fixed-table-toolbar .search {width: Calc( 100% - 108px );}
	
	optgroup {background-color: #fb9f98; color: #000;}
	
	.export.btn-group {*display:none !important;}
	
	#faq-cat-4 .fixed-table-toolbar {*width:60%;}
	#faq-cat-4 .fixed-table-toolbar .search {width: Calc( 100% - 236px );}
	
	@media  screen and (max-width: 767px) {
	.fixed-table-toolbar {width: 100%;}
	.fixed-table-toolbar {width:100%;}
	.fixed-table-toolbar .search {width: Calc( 100% );}
	}
	
	.SumoSelect {width: 100% !important; color: #000 !important;} 
	
	.SumoSelect .select-all {height: 35px !important;}
	
	.options{max-height: 150px !important;}
	
	.SelectBox {padding: 7px 12px !important;}
	
	SumoSelect .no-match {display: none !important; padding: 6px;}
	
	.faq-cat-content .panel-heading:hover {background-color: #1dd4cb;} 
	
	.table-summary {
	display:inline-block;
	border: 1px solid #ccc;
	margin-top: 15px;
	}
	
	.table-summary .wrapp {
	float:left;
	text-align:center;
	width:200px;
	}
	
	.table-summary .wrapp .title{
	padding: 5px 0;
	font-weight: 800;
	border-bottom: 1px solid #ccc;
	border-left: 1px solid #ccc;
	}
    .blue_btn 
    {
    background-color: #008fac;
    border-color: #008fac;
    }	
	.table-summary .wrapp .details{
	padding: 5px;
	border-left: 1px solid #ccc;
	}
	
	.nopadding{padding: 0;}
	
	.fixed-table-container {height: 500px !important; } 
	
	/* ============================================================
	COMMON
	============================================================ */
	.cmn-toggle {
	position: absolute;
	margin-left: -9999px;
	visibility: hidden;
	}
	.cmn-toggle + label {
	/*display: block;*/
	position: relative;
	cursor: pointer;
	outline: none;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	}
	
	/* ============================================================
	SWITCH 1 - ROUND
	============================================================ */
	input.cmn-toggle-round + label {
	padding: 2px;
	width: 40px;
	height: 20px;
	background-color: #dddddd;
	-webkit-border-radius: 60px;
	-moz-border-radius: 60px;
	-ms-border-radius: 60px;
	-o-border-radius: 60px;
	border-radius: 60px;
	}
	input.cmn-toggle-round + label:before, input.cmn-toggle-round + label:after {
	display: block;
	position: absolute;
	top: 1px;
	left: 1px;
	bottom: 1px;
	content: "";
	}
	input.cmn-toggle-round + label:before {
	right: 1px;
	background-color: #f1f1f1;
	-webkit-border-radius: 60px;
	-moz-border-radius: 60px;
	-ms-border-radius: 60px;
	-o-border-radius: 60px;
	border-radius: 60px;
	-webkit-transition: background 0.4s;
	-moz-transition: background 0.4s;
	-o-transition: background 0.4s;
	transition: background 0.4s;
	}
	input.cmn-toggle-round + label:after {
	width: 19px;
	background-color: #fff;
	-webkit-border-radius: 100%;
	-moz-border-radius: 100%;
	-ms-border-radius: 100%;
	-o-border-radius: 100%;
	border-radius: 100%;
	-webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
	box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
	-webkit-transition: margin 0.4s;
	-moz-transition: margin 0.4s;
	-o-transition: margin 0.4s;
	transition: margin 0.4s;
	}
	input.cmn-toggle-round:checked + label:before {
	background-color: #8ce196;
	}
	input.cmn-toggle-round:checked + label:after {
	margin-left: 20px;
	}
</style>

<!--end of page level css-->
@stop
{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">	Settings</li>
	</ol>
</section>
<?php
	$errorarr=$errors;   
	function showerrormsg($key,$errors)
	{    
		$msg='';
		if(count($errors->get($key))>0)
		{ 
			$msg.='';
			foreach($errors->get($key) as $error)
			{
				if($key=="alert_success")
				{
					$msg.=$error;
				}
				else
				{
					$msg.='<small class="text-danger animated fadeInUp">'.$error.'</small>' ; 
				}
			}
		} 
		return $msg;     
	}
?>
<!-- Main content -->
<section class="content p-l-r-15">
    @include('panel.includes.status')
	@php 
	$appointment_notifier = '';
	$lead_notifier = '';
	@endphp
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<!--tab starts-->
			<!-- Nav tabs category -->
			<ul class="nav nav-tabs" style="margin-top:0px">
				<li class="active"><a href="#faq-cat-1" id="Group-Tab" data-toggle="tab">Group</a></li>
				<li><a href="#faq-cat-2" id="Package-Tab" data-toggle="tab"> @if(Session::get('user_category')=='admin'  || ManagerHelper::ManagerCategory()=='admin') {{ "Clients Package" }} @else {{ 'Package' }} @endif </a></li>
				@if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
				<li><a href="#faq-cat-3" id="Vendor-Package-Tab" data-toggle="tab">Vendors Package</a></li>
				@endif
				@if(Session::get('user_category')=='admin' || Session::get('user_category')=='vendor')
				<li><a href="#faq-cat-4" id="FB-Ads-Account-Tab" data-toggle="tab">Facebook Ad Accounts</a></li>
				<li><a href="#faq-cat-5" id="Email-Notification-Tab" data-toggle="tab">Email Notification</a></li>
                <li><a href="#faq-cat-6"  data-toggle="tab">Invoice Settings</a></li>
				@endif
                @if(Session::get('user_category')=='admin')
                <li><a href="#faq-cat-7"  data-toggle="tab">Closed Deals</a></li>
				@endif
				
				@if(Session::get('user_category')=='admin' || Session::get('user_category')=='vendor')
				<li><a href="#faq-cat-8" id="Block-Pages-Forms-Tab" data-toggle="tab">Block FB Pages & Forms</a></li>
				@endif
			</ul>
			<!-- Tab panes -->
			<div class="tab-content faq-cat-content">
				<div class="tab-pane active in fade" id="faq-cat-1">
					<div class="panel-group" id="accordion-cat-1">
						@if($WritePermission=='Yes')
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-file-text-o"></i> Create Group
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-1-sub-1" class="panel-collapse collapse in">
								<div class="panel-body">
									<form action="{{ route('add-group-submit') }}" class="form-horizontal" method="post" id="form-validation" >
										<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
										<div class="form-body">
											<div class="form-group">
												<label for="name" class="col-md-3 control-label"> Group Name</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<input value="{{ old('name') }}" id="name" type="text" name="name" class="form-control"
														placeholder="Name">
														<input value="{{ old('id') }}" id="id" type="hidden" name="id">
													</div>
													<?php echo showerrormsg('name',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-group">
												<label for="group_category_id" class="col-md-3 control-label"> Category</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<div id="div_group_category_id">
															<select id="group_category_id" name="group_category_id" class="form-control">
																<option value="">Select Category</option>
																@foreach($GroupCategories as $GroupCategory)
																<option value="{{ $GroupCategory->id }}" @if('6'==$GroupCategory->id) selected  @endif>{{ $GroupCategory->name }}</option>
																@endforeach
															</select>
														</div>
													</div>
													<?php echo showerrormsg('group_category_id',$errorarr); ?>
												</div>
												@if(Session::get('user_category')=='admin')
												<div class="col-md-2" style="padding:0px">
													<button type="button" class="btn btn-info add_group_category" data-toggle="modal" data-target="#add_group_category" title="Add new category">
														<i class="fa fa-plus" aria-hidden="true"></i>
													</button>
												</div>
												@endif
											</div>
											
											<div class="form-group">
												<label for="group_sub_category_id" class="col-md-3 control-label"> Sub-Category</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<div id="div_group_sub_category_id">
															<select multiple="multiple" placeholder="Select Sub-Category" name="group_sub_category_id[]" id="group_sub_category_id" class="select1 SumoUnder">
																@foreach($GroupSubCategories as $GroupSubCategory)
																<option value="{{ $GroupSubCategory->id }}">{{ $GroupSubCategory->name }}</option>
																@endforeach
															</select>
														</div>
													</div>
													<?php echo showerrormsg('group_sub_category_id',$errorarr); ?>
												</div>
												@if(Session::get('user_category')=='admin')
												<div class="col-md-2" style="padding:0px">
													<button type="button" class="btn btn-info add_group_sub_category" data-toggle="modal" data-target="#add_group_sub_category" title="Add new sub-category">
														<i class="fa fa-plus" aria-hidden="true"></i>
													</button>
												</div>
												@endif
											</div>
											
											<div class="form-group">
												<label for="zipcode" class="col-md-3 control-label"> Zip code</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<input value="{{ old('zipcode') }}" type="text" placeholder="Zip code" id="zipcode" name="zipcode" class="form-control"/>
													</div>
													<?php echo showerrormsg('zipcode', $errorarr); ?>
												</div>
											</div>
											<div class="form-group">
												<label for="milesradius" class="col-md-3 control-label"> Miles Radius</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<input value="{{ old('milesradius') }}" type="text" id="milesradius" name="milesradius" placeholder="Miles Radius" class="form-control"/>        
													</div>
													<?php echo showerrormsg('milesradius',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-group">
												<label for="client_list" class="col-md-3 control-label"> Client</label>
												<div class="col-md-6">
													<div  id="client_list_hide">
														<div class="input-group">
															<span class="input-group-addon">
																<i class="fa fa-fw fa-edit"></i>
															</span>
															<select multiple="multiple" placeholder="Select Client" name="client_list[]" id="client_list" class="select1 SumoUnder">
																@foreach($Clients as $Client)
																<option value="{{ $Client->id }}">{{ $Client->name }} ({{ $Client->email }})</option>
																@endforeach
															</select>
														</div>
													</div>
													<?php echo showerrormsg('client_list',$errorarr); ?>
												</div>
												<div class="col-md-2" style="padding:0px">
													@if($ClientPermission=='Yes')
													<button type="button" class="btn btn-info add_client" title="Add new client">
														<i class="fa fa-plus" aria-hidden="true"></i>
													</button>
													@endif
												</div>
											</div>
											
											<div class="form-group">
												<label for="group_type" class="col-md-3 control-label"> Group Type</label>
												<div class="col-md-6 form_field">
													<label class="radio-inline"><input type="radio" name="group_type" value="Page Engagements"> Page Engagements</label>
													<label class="radio-inline"><input type="radio" name="group_type" value="From Ad Forms" checked> From Ad Forms</label><br>
													<?php echo showerrormsg('group_type',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-group div_lead_form">
												<label for="lead_form" class="col-md-3 control-label"> Lead Form</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<select multiple="multiple" placeholder="Select Lead Form" name="lead_form[]" id="lead_form"  class="select1 SumoUnder">
															@foreach($LeadForms as $LeadForm)
															<option value="{{ $LeadForm->id }}">{{ $LeadForm->name }}</option>
															@endforeach
														</select>
													</div>
													<?php echo showerrormsg('lead_form',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-group div_page_engagements">
												<label for="lead_form" class="col-md-3 control-label"> Page Engagements</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<select multiple="multiple" placeholder="Select Page Engagements" name="page_engagements[]" id="page_engagements"  class="select1 SumoUnder">
															@foreach($facebook_pages as $facebook_page)
															<option value="{{ $facebook_page->id }}">{{ $facebook_page->name }}</option>
															@endforeach
														</select>
													</div>
													<?php echo showerrormsg('page_engagements',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-actions">
												<div class="row">
													<div class="col-md-12 text-center">
														<button type="submit" class="btn btn-primary"> Submit</button>
														&nbsp;
														<button type="reset" class="btn btn-default bttn_reset"> Reset</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						@endif
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-th-large"></i> Group List
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-1-sub-2" class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="row">		
										<div class="col-lg-12">
											<table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20,40,ALL]" data-show-footer="false">
												<thead>
													<tr>
														<th data-field="#" @if(in_array("0",$GroupColumns)) data-visible="false" @endif>#</th>
														<th data-field="Create Date" data-sortable="true" @if(in_array("1",$GroupColumns)) data-visible="false" @endif>Created Time</th>
														<th data-field="Name" data-sortable="true" @if(in_array("2",$GroupColumns)) data-visible="false" @endif>Name</th>
														<th data-field="Category" data-sortable="true" @if(in_array("3",$GroupColumns)) data-visible="false" @endif>Category</th>
														<th data-field="Sub-Category" data-sortable="true" @if(in_array("4",$GroupColumns)) data-visible="false" @endif>Sub-Category</th>
														<th data-field="Zip code" data-sortable="true" data-halign="left" data-align="right" @if(in_array("5",$GroupColumns)) data-visible="false" @endif>Zip code</th>
														<th data-field="Miles Radius" data-sortable="true" data-halign="left" data-align="right" @if(in_array("6",$GroupColumns)) data-visible="false" @endif>Miles Radius</th>
														@if($WritePermission=='Yes')<th data-field="Action" @if(in_array("7",$GroupColumns)) data-visible="false" @endif>Action</th>@endif
													</tr>
												</thead>
												<tbody>  
													@php $i=1; @endphp
													
													@foreach($Groups as $Group)
													<tr>
														<td>{{ $i++ }}</td>
														<td>{{ date('m-d-Y h:i:s A',strtotime($Group->created_at)) }}</td>
														<td>{{ $Group->name }}</td>
														<td>{{ $Group->group_category_name }}</td>
														<!--<td>{{ $Group->group_sub_category_name }}</td>-->
														<td>{{ Helper::SubCategoryName($Group->id) }}</td>
														<td>{{ $Group->zipcode }}</td>
														<td>{{ $Group->milesradius }}</td>
														@if($WritePermission=='Yes')
														<td>
															<div class="ui-group-buttons">
																<form><input type="hidden" name="id" value="{{ $Group->id }}" /></form>
																<a href="{{ route('group') }}?id={{ $Group->id }}" title="Edit" class="btn btn-success" role="button">
																	<span class="glyphicon glyphicon-edit"></span>
																</a>                                    
																<button id="{{ $Group->id }}" type="button" title="Delete"  class="btn btn-danger delete" role="button">
																	<span class="glyphicon glyphicon-trash"></span>
																</button>
															</div>
														</td>                       
														@endif
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="tab-pane fade" id="faq-cat-2">
					<div class="panel-group" id="accordion-cat-2">
						@if($WritePermission=='Yes')
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-file-text-o"></i> @if(app('request')->exists('package_id')) Edit @else Create @endif @if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin') {{ "Clients" }} @endif Package
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-2-sub-1" class="panel-collapse collapse in">
								<div class="panel-body">
									<form action="@if(app('request')->exists('package_id')){{ route('edit-package-submit') }}@else {{ route('add-package-submit') }} @endif" class="form-horizontal" role="form" method="post" id="package-form-validation">
										<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
										<div class="form-body">
											<div class="form-group m-t-10">
												<label for="package_name" class="col-md-3 control-label">
													Package  Name
												</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<input value="@if(app('request')->exists('package_id') && !old('package_name')){{ $EditPackage->name }}@else{{ old('package_name') }}@endif" id="package_name" type="text" name="package_name" class="form-control"
														placeholder="Name">
														<input value="@if(app('request')->exists('package_id')){{ $EditPackage->id }}@endif" id="package_id" type="hidden" name="package_id">
													</div>
													<small class="text-danger animated fadeInUp package_name client_package_validate"></small>
													<?php echo showerrormsg('package_name',$errorarr); ?>
												</div>
											</div>
											<div class="form-group">
												<label for="package_type" class="col-md-3 control-label">Package Type</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<select id="package_type" name="package_type" class="form-control">
															<option value="Monthly" @if(app('request')->exists('package_id') && !old('package_type') and $EditPackage->package_type=='Monthly') selected @elseif(old('package_type')=='Monthly') selected  @endif>Monthly</option>
															<option value="Lead" @if(app('request')->exists('package_id') && !old('package_type') and $EditPackage->package_type=='Lead') selected @elseif(old('package_type')=='Lead') selected  @endif>Lead</option>
															<option value="Sold" @if(app('request')->exists('package_id') && !old('package_type') and $EditPackage->package_type=='Sold') selected @elseif(old('package_type')=='Sold') selected  @endif>Sold</option>
														</select>
													</div>
													<small class="text-danger animated fadeInUp package_type client_package_validate"></small>
													<?php echo showerrormsg('package_type',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-group lead_value_hide" @if(app('request')->exists('package_id') && $EditPackage->package_type=='Lead') @else style="display:none" @endif>
												
												<label for="per_lead_value" class="col-md-3 control-label">
													Per Lead Value
												</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-usd"></i>
														</span>
														<input type="text" id="per_lead_value" name="per_lead_value" value="@if(app('request')->exists('package_id') && !old('per_lead_value')){{ $EditPackage->per_lead_value }}@else{{ old('per_lead_value') }}@endif" placeholder="Lead Value" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
													</div>
													<small class="text-danger animated fadeInUp per_lead_value client_package_validate"></small>
													<?php echo showerrormsg('per_lead_value',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-group">
												<label for="monthly_lead_quantity" class="col-md-3 control-label">
													Monthly Lead Quantity
												</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<input type="text" id="monthly_lead_quantity" name="monthly_lead_quantity" value="@if(app('request')->exists('package_id') && !old('monthly_lead_quantity')){{ $EditPackage->monthly_lead_quantity }}@else{{ old('monthly_lead_quantity') }}@endif" placeholder="Monthly Lead Quantity" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" class="form-control"/>
													</div>
													<small class="text-danger animated fadeInUp monthly_lead_quantity client_package_validate"></small>
													<?php echo showerrormsg('monthly_lead_quantity',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-group package_amount_hide"  @if(app('request')->exists('package_id') && $EditPackage->package_type=='Lead')  style="display:none" @endif>
												<label for="amount" class="col-md-3 control-label">
													Package Value (Amount $)
												</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-usd"></i>
														</span>
														<input type="text" id="amount" name="amount" value="@if(app('request')->exists('package_id') && !old('amount')){{ $EditPackage->amount }}@else{{ old('amount') }}@endif" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" placeholder="Amount" class="form-control"/>
													</div>
													<small class="text-danger animated fadeInUp amount client_package_validate"></small>
													<?php echo showerrormsg('amount',$errorarr); ?>
												</div>
											</div>
										</div>
										<div class="form-actions">
											<div class="row">
												<div class="col-md-12 text-center">
													<button type="submit" class="btn btn-primary" id="submit_client_package">Submit</button>                                                                
													&nbsp;
													<button type="reset" class="btn btn-default">
														Reset
													</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						@endif
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-th-large"></i> @if(Session::get('user_category')=='admin') {{ "Clients" }} @endif Package List
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-2-sub-2" class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="row">		
										<div class="col-lg-12">
											<table id="Package-Table" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20,40,ALL]" data-show-footer="false" data-side-pagination="server" data-toggle="table" data-url="{{ route('client_package_list') }}">
												<thead>
													<tr>
														<th data-field="#" @if(in_array("0",$ClientPackageColumns)) data-visible="false" @endif>#</th>
														<th data-field="Created Time" data-sortable="true" @if(in_array("1",$ClientPackageColumns)) data-visible="false" @endif>Created Time</th>
														<th data-field="Name" data-sortable="true" @if(in_array("2",$ClientPackageColumns)) data-visible="false" @endif>Name</th>
														<th data-field="Type" data-sortable="true" @if(in_array("3",$ClientPackageColumns)) data-visible="false" @endif>Type</th>
														<th data-field="Per Lead Value" data-sortable="true" data-halign="left" data-align="right" @if(in_array("4",$ClientPackageColumns)) data-visible="false" @endif>Per Lead Value</th>
														<th data-field="Monthly Lead Quantity" data-sortable="true" data-halign="left" data-align="right" @if(in_array("5",$ClientPackageColumns)) data-visible="false" @endif>Monthly Lead Quantity</th>
														<th data-field="Package Value (Amount $)" data-sortable="true" @if(in_array("6",$ClientPackageColumns)) data-visible="false" @endif data-halign="left" data-align="right">Package Value (Amount $)</th>
														@if($WritePermission=='Yes')
														<th data-field="Action" @if(in_array("7",$ClientPackageColumns)) data-visible="false" @endif>Action</th>
														@endif
													</tr>
												</thead>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				@if(Session::get('user_category')=='admin'  || ManagerHelper::ManagerCategory()=='admin')
				<div class="tab-pane fade" id="faq-cat-3">
					<div class="panel-group" id="accordion-cat-3">
						@if($WritePermission=='Yes')
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-file-text-o"></i> @if(app('request')->exists('vendor_package_id')) Edit @else Create @endif Vendors Package
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-3-sub-1" class="panel-collapse collapse in">
								<div class="panel-body">
									<form action="@if(app('request')->exists('vendor_package_id')){{ route('edit-vendor-package-submit') }}@else {{ route('add-vendor-package-submit') }} @endif" class="form-horizontal" method="post" id="vendor-package-form-validation">
										<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
										<div class="form-body">
											<div class="form-group m-t-10">
												<label for="vendor_package_name" class="col-md-3 control-label">
													Package Name
												</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<input value="@if(app('request')->exists('vendor_package_id') && !old('vendor_package_name')){{ $EditVendorPackage->name }}@else{{ old('vendor_package_name') }}@endif" id="vendor_package_name" type="text" name="vendor_package_name" class="form-control"
														placeholder="Name">
														<input value="@if(app('request')->exists('vendor_package_id')){{ $EditVendorPackage->id }}@endif" id="vendor_package_id" type="hidden" name="vendor_package_id">
													</div>
													<?php echo showerrormsg('vendor_package_name',$errorarr); ?>
												</div>
											</div>
											<div class="form-group">
												<label for="vendor_client_limits" class="col-md-3 control-label">
													Clients Limits
												</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<select name="vendor_client_limits" id="vendor_client_limits" class="form-control">
															<option value="">Select Clients Limits</option>
															<option value="Limited" @if(app('request')->exists('vendor_package_id') && !old('vendor_client_limits') and $EditVendorPackage->client_limit=='Limited') selected @elseif(old('vendor_client_limits')=='Limited') selected  @endif>Limited</option>
															<option value="Unlimited" @if(app('request')->exists('vendor_package_id') && !old('vendor_client_limits') and $EditVendorPackage->client_limit=='Unlimited') selected @elseif(old('vendor_client_limits')=='Unlimited') selected  @endif>Unlimited</option>												
														</select>
													</div>
													<?php echo showerrormsg('vendor_client_limits',$errorarr); ?>
												</div>
												<div class="col-md-2 vendor_client_limit_integer" style="padding:0px">
													<input value="@if(app('request')->exists('vendor_package_id')){{ $EditVendorPackage->client_limits }}@endif" name="vendor_client_limit_integer" id="vendor_client_limit_integer" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
												</div>
											</div>
											<div class="form-group">
												<label for="vendor_ads_accounts" class="col-md-3 control-label">
													Ads Accounts
												</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<select name="vendor_ads_accounts" id="vendor_ads_accounts" class="form-control">
															<option value="">Select Ads Accounts</option>
															<option value="Limited" @if(app('request')->exists('vendor_package_id') && !old('vendor_ads_accounts') and $EditVendorPackage->ads_account=='Limited') selected @elseif(old('vendor_ads_accounts')=='Limited') selected  @endif>Limited</option>
															<option value="Unlimited" @if(app('request')->exists('vendor_package_id') && !old('vendor_ads_accounts') and $EditVendorPackage->ads_account=='Unlimited') selected @elseif(old('vendor_ads_accounts')=='Unlimited') selected  @endif>Unlimited</option>													
														</select>
													</div>
													<?php echo showerrormsg('vendor_ads_accounts',$errorarr); ?>
												</div>
												<div class="col-md-2 vendor_ads_account_integer" style="padding:0px">
													<input value="@if(app('request')->exists('vendor_package_id')){{ $EditVendorPackage->ads_accounts }}@endif" name="vendor_ads_account_integer" id="vendor_ads_account_integer" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
												</div>
											</div>
											<div class="form-group">
												<label for="vendor_campaigns" class="col-md-3 control-label">
													Campaigns
												</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<select name="vendor_campaigns" id="vendor_campaigns" class="form-control">
															<option value="">Select Campaigns</option>
															<option value="Limited" @if(app('request')->exists('vendor_package_id') && !old('vendor_campaigns') and $EditVendorPackage->campaign=='Limited') selected @elseif(old('vendor_campaigns')=='Limited') selected  @endif>Limited</option>
															<option value="Unlimited" @if(app('request')->exists('vendor_package_id') && !old('vendor_campaigns') and $EditVendorPackage->campaign=='Unlimited') selected @elseif(old('vendor_campaigns')=='Unlimited') selected  @endif>Unlimited</option>													
														</select>
													</div>
													<?php echo showerrormsg('vendor_campaigns',$errorarr); ?>
												</div>
												<div class="col-md-2 vendor_campaign_integer" style="padding:0px">
													<input value="@if(app('request')->exists('vendor_package_id')){{ $EditVendorPackage->campaigns }}@endif" name="vendor_campaign_integer" id="vendor_campaign_integer" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
												</div>
											</div>
											<div class="form-group">
												<label for="vendor_leads_amount" class="col-md-3 control-label">
													Leads Amount
												</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<select name="vendor_leads_amount" id="vendor_leads_amount" class="form-control">
															<option value="">Select Leads Amount</option>
															<option value="Limited" @if(app('request')->exists('vendor_package_id') && !old('vendor_leads_amount') and $EditVendorPackage->leads_amount=='Limited') selected @elseif(old('vendor_leads_amount')=='Limited') selected  @endif>Limited</option>
															<option value="Unlimited" @if(app('request')->exists('vendor_package_id') && !old('vendor_leads_amount') and $EditVendorPackage->leads_amount=='Unlimited') selected @elseif(old('vendor_leads_amount')=='Unlimited') selected  @endif>Unlimited</option>														
														</select>
													</div>
													<?php echo showerrormsg('vendor_leads_amount',$errorarr); ?>
												</div>
												<div class="col-md-2 vendor_leads_amount_integer" style="padding:0px">
													<input value="@if(app('request')->exists('vendor_package_id')){{ $EditVendorPackage->leads_amounts }}@endif" name="vendor_leads_amount_integer" id="vendor_leads_amount_integer" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
												</div>
											</div>
											<div class="form-group">
												<label for="vendor_fb_messenger" class="col-md-3 control-label">
													FB Messenger
												</label>
												<div class="col-md-6">
													<div class="input-group">
														<span class="input-group-addon">
															<i class="fa fa-fw fa-edit"></i>
														</span>
														<select name="vendor_fb_messenger" id="vendor_fb_messenger" class="form-control">
															<option value="">Select FB Messenger</option>
															<option value="No" @if(app('request')->exists('vendor_package_id') && !old('vendor_fb_messenger') and $EditVendorPackage->fb_messenger=='No') selected @elseif(old('vendor_fb_messenger')=='No') selected  @endif>No</option>
															<option value="Yes" @if(app('request')->exists('vendor_package_id') && !old('vendor_fb_messenger') and $EditVendorPackage->fb_messenger=='Yes') selected @elseif(old('vendor_fb_messenger')=='Yes') selected  @endif>Yes</option>															
														</select>
													</div>
													<?php echo showerrormsg('vendor_fb_messenger',$errorarr); ?>
												</div>
											</div>
										</div>
										<div class="form-actions">
											<div class="row">
												<div class="col-md-12 text-center">
													<button type="submit" class="btn btn-primary">Submit
													</button>                                                                
													&nbsp;
													<button type="reset" class="btn btn-default">
														Reset
													</button>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						@endif
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-th-large"></i> Vendors Package List
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-3-sub-2" class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="row">		
										<div class="col-lg-12">
											<table id="Vendor-Package-Table" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20,40,ALL]" data-show-footer="false">
												<thead>
													<tr>
														<th data-field="#" @if(in_array("0",$VendorPackageColumns)) data-visible="false" @endif>#</th>
														<th data-field="Create Date" data-sortable="true" @if(in_array("1",$VendorPackageColumns)) data-visible="false" @endif>Created Time</th>
														<th data-field="Name" data-sortable="true" @if(in_array("2",$VendorPackageColumns)) data-visible="false" @endif>Name</th>
														<th data-field="Clients Limits" data-sortable="true" @if(in_array("3",$VendorPackageColumns)) data-visible="false" @endif>Clients Limits</th>
														<th data-field="Ads Accounts" data-sortable="true" @if(in_array("4",$VendorPackageColumns)) data-visible="false" @endif>Ads Accounts</th>
														<th data-field="Campaigns" data-sortable="true" @if(in_array("5",$VendorPackageColumns)) data-visible="false" @endif>Campaigns</th>
														<th data-field="Leads Amount" data-sortable="true" @if(in_array("6",$VendorPackageColumns)) data-visible="false" @endif>Leads Amount</th>
														<th data-field="FB Messenger" data-sortable="true" @if(in_array("7",$VendorPackageColumns)) data-visible="false" @endif>FB Messenger</th>
														@if($WritePermission=='Yes')<th data-field="Action" @if(in_array("8",$VendorPackageColumns)) data-visible="false" @endif>Action</th>@endif
													</tr>
												</thead>
												<tbody>  
													@php $k=1; @endphp
													
													@foreach($VendorPackages as $VendorPackage)
													<tr>
														<td>{{ $k++ }}</td>
														<td>{{ date('m-d-Y h:i:s A',strtotime($VendorPackage->created_at)) }}</td>
														<td>{{ $VendorPackage->name }}</td>
														<td>@if($VendorPackage->client_limit=='Unlimited'){{ $VendorPackage->client_limit }}@else{{ number_format($VendorPackage->client_limits,0) }}@endif</td>
														<td>@if($VendorPackage->ads_account=='Unlimited'){{ $VendorPackage->ads_account }}@else{{ number_format($VendorPackage->ads_accounts,0) }}@endif</td>
														<td>@if($VendorPackage->campaign=='Unlimited'){{ $VendorPackage->campaign }}@else{{ number_format($VendorPackage->campaigns,0) }}@endif</td>
														<td>@if($VendorPackage->leads_amount=='Unlimited'){{ $VendorPackage->leads_amount }}@else{{ number_format($VendorPackage->leads_amounts,0) }}@endif</td>
														<td>{{ $VendorPackage->fb_messenger }}</td>
														@if($WritePermission=='Yes')
														<td>
															<div class="ui-group-buttons">
																<form><input type="hidden" name="vendor_package_id" value="{{ $VendorPackage->id }}" /></form>
																<button type="button" title="Edit"  class="btn btn-success vendor-package-edit" role="button">
																	<span class="glyphicon glyphicon-edit"></span>
																</button>                                    
																<button id="{{ $VendorPackage->id }}" type="button" title="Delete"  class="btn btn-danger vendor-package-delete" role="button">
																	<span class="glyphicon glyphicon-trash"></span>
																</button>
															</div>
														</td>
														@endif
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endif
				
				@if(Session::get('user_category')=='admin'  || Session::get('user_category')=='vendor')
				<div class="tab-pane fade" id="faq-cat-4">
					<div class="panel-group" id="accordion-cat-4">
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-th-large"></i> Facebook Ad Accounts List
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-4-sub-1" class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="row">		
										<div class="col-lg-12">
											<div class="col-md-12 nopadding" style="" align="right">
												<div class="wrapp table-summary">         
													<div class="wrapp">       
														<div class="title">Total Ad Accounts</div>         
														<div class="details">{{ count($facebook_ads_accounts) }}</div>       
													</div>   	
												</div>
											</div>
											
											<table id="FB-Ads-Account-Table" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20,40,ALL]" data-show-footer="false">
												<thead>
													<tr>
														<th data-field="#" @if(in_array("0",$FacebookAdsAccountTable)) data-visible="false" @endif>#</th>
														<th data-field="Date Created" data-sortable="true" @if(in_array("1",$FacebookAdsAccountTable)) data-visible="false" @endif>Date Created</th>
														<th data-field="Clients" data-sortable="true" @if(in_array("2",$FacebookAdsAccountTable)) data-visible="false" @endif>Clients</th>
														<th data-field="FB Ad Account" data-halign="center" data-align="right" data-sortable="true" @if(in_array("3",$FacebookAdsAccountTable)) data-visible="false" @endif>FB Ad Account</th>
														<th data-field="FB User" data-sortable="true" @if(in_array("4",$FacebookAdsAccountTable)) data-visible="false" @endif>FB User</th>
														<th data-field="Import Campaigns" data-sortable="true" @if(in_array("5",$FacebookAdsAccountTable)) data-visible="false" @endif>Import Campaigns</th>
														<th data-field="Sync" data-halign="center" data-align="center" data-sortable="true" @if(in_array("6",$FacebookAdsAccountTable)) data-visible="false" @endif>Sync</th>
														<th data-field="Delete" data-halign="center" data-align="center" data-sortable="true" @if(in_array("7",$FacebookAdsAccountTable)) data-visible="false" @endif>Delete</th>
													</tr>
												</thead>
												<tbody>  
													@php $k=1; @endphp
													
													@foreach($facebook_ads_accounts as $facebook_ads_account)
													<tr>
														<td>{{ $k++ }}</td>
														<td>{{ date('m-d-Y h:i:s A',strtotime($facebook_ads_account->created_at)) }}</td>
														<td></td>
														<td>{{ $facebook_ads_account->account_id }}</td>
														<td>{{ $facebook_ads_account->facebook_account_name }}</td>
														<td></td>
														<td>
															<div class="ui-group-buttons">
																<button  type="button" title="Sync"  class="btn btn-info" role="button">
																	<span class="glyphicon glyphicon-refresh"></span>
																</button>
															</div>
															<br />
															{{ Helper::FacebooklastSyncLeadDate($facebook_ads_account->facebook_account_id)}}
														</td>
														<td>
															<div class="ui-group-buttons">
																<button id="{{ $facebook_ads_account->id }}" type="button" title="Delete"  class="btn btn-danger FB-Ads-Account-delete" role="button">
																	<span class="glyphicon glyphicon-trash"></span>
																</button>
															</div>
														</td>
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="tab-pane fade" id="faq-cat-5">
					<div class="panel-group" id="accordion-cat-5">
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-th-large"></i> Email Notification
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-5-sub-1" class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="row">		
										<div class="col-lg-12">
											<label class="switch ib switch-custom">
												<input type="checkbox" value="Lead Appointment Notification" name="email_notification[]" class="email_notification" id="email_notification1" @if(Helper::EmailNotification('Lead Appointment Notification')=='Yes' || Helper::EmailNotification('Lead Appointment Notification')=='') checked @endif/>
												<label for="email_notification1" data-on="YES" data-off="NO"></label>
												<span>Lead Appointment Notification</span>
											</label>
											
											<label class="switch ib switch-custom">
												<input type="checkbox" value="New Lead Notification" name="email_notification[]" class="email_notification" id="email_notification2" @if(Helper::EmailNotification('New Lead Notification')=='Yes' || Helper::EmailNotification('New Lead Notification')=='') checked @endif/>
												<label for="email_notification2" data-on="YES" data-off="NO"></label>
												<span>New Lead Notification</span>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="panel panel-success  panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title"  style="color: #fff;">
									<i class="fa fa-fw fa-th-large"></i> <span id="lead_title" style="font-size: 16px;">Client's User Email Notification (Please select client from the filter above)</span>
								</h4>
							</div>
							<div id="faq-cat-5-sub-2" class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-12">
											<table id="UserEmailNotification" data-toggle="table" data-query-params="queryParams" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-page-list="[10, 20,40, ALL]" data-show-footer="false" data-side-pagination="server" data-sort-order="ASC" data-url="{{ route('user-email-notification') }}">
												<thead>
													<tr>
														<th data-field="#" data-halign="center" data-align="center">#</th>
														<th data-field="User Name" data-sortable="true">User Name</th>
														<th data-field="Appointment Notifier" data-visible="true" data-halign="center" data-align="center">Appointment Notifier</th>
														<th data-field="Lead Notifier" data-visible="true" data-halign="center" data-align="center">Lead Notifier</th>
													</tr>
												</thead>
												<tbody>
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="tab-pane fade" id="faq-cat-6">
					<div class="panel-group" id="accordion-cat-6">
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-info"></i> Payment Terms & Conditions
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-6-sub-1" class="panel-collapse collapse in">
								<div class="panel-body">
									<form action="{{ route('update-payment-tnc') }}" class="form-horizontal" method="post" id="form-validation" >
										<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
										<div class="form-body">
											<div class="form-group">
												<label for="payment_tnc" class="col-md-3 control-label">
													Payment Terms & Conditions
												</label>
												<div class="col-md-6">
													<textarea rows="6" name="payment_tnc" id="payment_tnc" required="" class="form-control" placeholder="Payment page terms and conditions">@if(count($invoice_settings)>0){{$invoice_settings->payment_tnc}}@endif</textarea>
													<?php echo showerrormsg('payment_tnc',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-actions">
												<div class="row">
													<div class="col-md-12 text-center">
														<button type="submit" class="btn btn-primary">Update
														</button>                                                                
														&nbsp;
														<button type="reset" class="btn btn-default bttn_reset">
															Reset
														</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>    
						@if(session('user')->category=="admin")
						
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-info"></i>  Invoice Page Terms & Conditions
									<span class="pull-right"></span>
								</h4>
							</div>
							
							<div id="faq-cat-6-sub-2" class="panel-collapse collapse in">
								<div class="panel-body">
									<form action="{{ route('update-invoice-setting') }}" class="form-horizontal" method="post" id="form-validation" >
										<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="type" value="invoice_tnc" />
										<div class="form-body">											
											
											<div class="form-group">
												<label for="invoice_tnc" class="col-md-3 control-label">
													Terms & Conditions
												</label>
												<div class="col-md-9">
													<textarea rows="5"  name="invoice_tnc" id="invoice_tnc" required="" class="editor-cls"   placeholder="Invoice Terms & Conditions">@if(count($invoice_settings_common_for_all)>0){{str_replace("?"," ",$invoice_settings_common_for_all->invoice_tnc)}}@endif</textarea>
													<?php echo showerrormsg('invoice_tnc',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-actions">
												<div class="row">
													<div class="col-md-12 text-center">
														<button type="submit" class="btn btn-primary">Update
														</button>                                                                
														&nbsp;
														<button type="reset" class="btn btn-default bttn_reset">
															Reset
														</button>
													</div>                                                    
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>                           
						</div>
						
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-credit-card"></i> Transactional Fee
									<span class="pull-right"></span>
								</h4>
							</div>
							
							<div id="faq-cat-6-sub-2" class="panel-collapse collapse in">
								<div class="panel-body">
									<form action="{{ route('update-invoice-setting') }}" class="form-horizontal" method="post" id="form-validation" >
										<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="type" value="cc_charge" />
										<div class="form-body">											
											
											<div class="form-group">
												<label for="cc_charge" class="col-md-3 control-label">
													Credit Card Processing Fee(%)
												</label>
												<div class="col-md-6">
													<input type="number" name="cc_charge" id="cc_charge" required="" class="form-control" value="@if(count($invoice_settings_common_for_all)>0){{$invoice_settings_common_for_all->cc_charge}}@endif" step="0.01" placeholder="Transactional Fee"/>
													<?php echo showerrormsg('cc_charge',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-actions">
												<div class="row">
													<div class="col-md-12 text-center">
														<button type="submit" class="btn btn-primary">Update
														</button>                                                                
														&nbsp;
														<button type="reset" class="btn btn-default bttn_reset">
															Reset
														</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>                           
						</div>
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-envelope"></i>  Invoice Email
									<span class="pull-right"></span>
								</h4>
							</div>
							
							<div id="faq-cat-6-sub-2" class="panel-collapse collapse in">
								<div class="panel-body">
									<form action="{{ route('update-invoice-setting') }}" class="form-horizontal" method="post" id="form-validation" >
										<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="type" value="invoice_email" />
										<div class="form-body">											
											
											<div class="form-group">
												<label for="invoice_email" class="col-md-3 control-label">
													Invoice Email
												</label>
												<div class="col-md-6">
													<input type="email" name="invoice_email" id="invoice_email" required="" class="form-control" value="@if(count($invoice_settings_common_for_all)>0){{$invoice_settings_common_for_all->invoice_email}}@endif"  placeholder="Invoice From Email"/>
													<?php echo showerrormsg('invoice_email',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-actions">
												<div class="row">
													<div class="col-md-12 text-center">
														<button type="submit" class="btn btn-primary">Update
														</button>                                                                
														&nbsp;
														<button type="reset" class="btn btn-default bttn_reset">
															Reset
														</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>                           
						</div>
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-envelope"></i>  Invoice Subject
									<span class="pull-right"></span>
								</h4>
							</div>
							
							<div id="faq-cat-6-sub-2" class="panel-collapse collapse in">
								<div class="panel-body">
									<form action="{{ route('update-invoice-setting') }}" class="form-horizontal" method="post" id="form-validation" >
										<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="type" value="invoice_subject" />
										<div class="form-body">											
											
											<div class="form-group">
												<label for="invoice_subject" class="col-md-3 control-label">
													Invoice Subject
												</label>
												<div class="col-md-6">
													<input type="text" name="invoice_subject" id="invoice_subject" required="" class="form-control" value="@if(count($invoice_settings_common_for_all)>0){{$invoice_settings_common_for_all->invoice_subject}}@endif"  placeholder="Invoice Email Subject"/>
													<?php echo showerrormsg('invoice_subject',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-actions">
												<div class="row">
													<div class="col-md-12 text-center">
														<button type="submit" class="btn btn-primary">Update
														</button>                                                                
														&nbsp;
														<button type="reset" class="btn btn-default bttn_reset">
															Reset
														</button>
													</div>
													<div class="col-md-12">
														<small class="text-danger">[name] = Client/Vendor name</small> ,
														<small class="text-danger">[invoice_num] = Invoice Number</small>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>                           
						</div>
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-envelope"></i>  Invoice Email Text
									<span class="pull-right"></span>
								</h4>
							</div>
							
							<div id="faq-cat-6-sub-2" class="panel-collapse collapse in">
								<div class="panel-body">
									<form action="{{ route('update-invoice-setting') }}" class="form-horizontal" method="post" id="form-validation" >
										<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="type" value="invoice_msg" />
										<div class="form-body">											
											
											<div class="form-group">
												<label for="invoice_msg" class="col-md-3 control-label">
													Invoice Message
												</label>
												<div class="col-md-9">
													<textarea rows="5"  name="invoice_msg" id="invoice_msg" required="" class="editor-cls"   placeholder="Invoice Email Text">@if(count($invoice_settings_common_for_all)>0){{$invoice_settings_common_for_all->invoice_msg}}@endif</textarea>
													<?php echo showerrormsg('invoice_msg',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-actions">
												<div class="row">
													<div class="col-md-12 text-center">
														<button type="submit" class="btn btn-primary">Update
														</button>                                                                
														&nbsp;
														<button type="reset" class="btn btn-default bttn_reset">
															Reset
														</button>
													</div>
													<div class="col-md-12">
														<small class="text-danger">[name] = Client/Vendor name</small> ,
														<small class="text-danger">[invoice_num] = Invoice Number</small>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>                           
						</div>
                        <div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-bell"></i>  Reminder Email Template
									<span class="pull-right"></span>
								</h4>
							</div>
							
							<div id="faq-cat-6-sub-2" class="panel-collapse collapse in">
								<div class="panel-body">
									<form action="{{ route('update-invoice-setting') }}" class="form-horizontal" method="post" id="form-validation" >
										<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="type" value="invoice_reminder" />
										<div class="form-body">	
											
											<div class="form-group">
												<label for="reminder_subject" class="col-md-3 control-label">
													Subject
												</label>
												<div class="col-md-6">
													<input type="text" name="reminder_subject" id="reminder_subject" required="" class="form-control" value="@if(count($invoice_settings_common_for_all)>0){{$invoice_settings_common_for_all->reminder_subject}}@endif"  placeholder="Invoice Reminder Subject"/>
													<?php echo showerrormsg('reminder_subject',$errorarr); ?>
												</div>
											</div>										
											
											<div class="form-group">
												<label for="reminder_msg" class="col-md-3 control-label">
													Message
												</label>
												<div class="col-md-9">
													<textarea rows="5"  name="reminder_msg" id="reminder_msg" required="" class="editor-cls"   placeholder="Invoice Reminder Email Text">@if(count($invoice_settings_common_for_all)>0){{$invoice_settings_common_for_all->reminder_msg}}@endif</textarea>
													<?php echo showerrormsg('reminder_msg',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-actions">
												<div class="row">
													<div class="col-md-12 text-center">
														<button type="submit" class="btn btn-primary">Update
														</button>                                                                
														&nbsp;
														<button type="reset" class="btn btn-default bttn_reset">
															Reset
														</button>
													</div>
													<div class="col-md-12">
														<small class="text-danger">[name] = Client/Vendor name</small> ,
														<small class="text-danger">[invoice_num] = Invoice Number</small>
                                                        <small class="text-danger">[due_date] =    Due Date</small>
                                                        <small class="text-danger">[Amount] = Balance Amount</small>
                                                        <small class="text-danger">[Due_Days] = Invoice due in / due by days </small>
                                                        <small class="text-danger">[Company_Name] = Company Name</small>
                                                        <small class="text-danger">[Company_Address] = Company Address</small>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>                           
						</div>
						@endif 
						
					</div>
				</div>
				@endif
				@if(session('user')->category=="admin")   
				<div class="tab-pane fade" id="faq-cat-7">
					<div class="panel-group" id="accordion-cat-7">
						
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-search"></i> Fetch New Closed Deals
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-6-sub-1" class="panel-collapse collapse in">
								@if(!$closed_deals_cron || !$closed_deals_cron->status)
								<div class="panel-body text-justify">                                    
									<form action="{{route('fetch-closed-deals')}}" method="post">
										{{csrf_field()}}
										<div class="form-group">
											<label>Click & Fetch Data</label> 
											<button type="submit" class="btn btn-flat btn-primary blue_btn">Fetch Data</button>
										</div>
									</form>
								</div>   	
								@else
								<div class="panel-body">   
									<i class="fa fa-fw fa-2x text-danger fa-spin fa-pulse fa-spinner"></i> <span class="text-danger"> Fetching Data</span>
								</div>
								<div class="panel-footer">    
									<p class="help-block">Note: please refresh page to check status.</p>
								</div>   
								@endif
								
							</div>
						</div>
						
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-clock-o"></i> Set Data Fetching Times
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-6-sub-1" class="panel-collapse collapse in">
								<div class="panel-body">
									<form action="{{ route('update-closed-deals-setting') }}" class="form-horizontal" method="post" id="form-validation" >
										<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
										<input type="hidden" name="type" value="time" />
										<div class="form-body">											
											
											<div class="form-group">
												<label for="time1" class="col-md-3 control-label">
													Fetch Time 1 :  
												</label>
												<div class="col-md-6">
													<input type="time" name="time1" id="time1" required="" class="form-control" value="@if(count($closed_deals_settings)>0){{date('H:i',strtotime($closed_deals_settings->fetch_time1))}}@endif"  placeholder="Data Fetching Time 1 "/>
													<?php echo showerrormsg('time1',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-group">
												<label for="time2" class="col-md-3 control-label">
													Fetch Time 2 :  
												</label>
												<div class="col-md-6">
													<input type="time" name="time2" id="time2" required="" class="form-control" value="@if(count($closed_deals_settings)>0){{date('H:i',strtotime($closed_deals_settings->fetch_time2))}}@endif"  placeholder="Data Fetching Time 2 "/>
													<?php echo showerrormsg('time2',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-group">
												<label for="time3" class="col-md-3 control-label">
													Fetch Time 3 :  
												</label>
												<div class="col-md-6">
													<input type="time" name="time3" id="time3" required="" class="form-control" value="@if(count($closed_deals_settings)>0){{date('H:i',strtotime($closed_deals_settings->fetch_time3))}}@endif"  placeholder="Data Fetching Time 3 "/>
													<?php echo showerrormsg('time3',$errorarr); ?>
												</div>
											</div>
											
											<div class="form-actions">
												<div class="row">
													<div class="col-md-12 text-center">
														<button type="submit" class="btn btn-primary">Update
														</button>                                                                
														&nbsp;
														<button type="reset" class="btn btn-default bttn_reset">
															Reset
														</button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				@endif 
				
				@if(Session::get('user_category')=='admin'  || Session::get('user_category')=='vendor')
				<div class="tab-pane fade" id="faq-cat-8">
					<div class="panel-group" id="accordion-cat-8">
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-th-large"></i> Facebook Page List
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-8-sub-1" class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="row">		
										<div class="col-lg-12">
											<table id="FacebookPageTable" data-toggle="table" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-page-list="[10, 20,40, ALL]" data-show-footer="false" data-side-pagination="server" data-url="{{ route('facebook-page-list') }}">
												<thead>
													<tr>
														<th data-field="#" data-halign="center" data-align="center" @if(in_array("0",$FacebookPageTable)) data-visible="false" @endif>#</th>
														<th data-field="Page Name" data-sortable="true" @if(in_array("1",$FacebookPageTable)) data-visible="false" @endif>Page Name</th>
														<th data-field="Action" data-halign="center" data-align="center" @if(in_array("2",$FacebookPageTable)) data-visible="false" @endif>Action</th>
													</tr>
												</thead>
												<tbody>  
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-th-large"></i>Facebook Ad Form List
									<span class="pull-right"></span>
								</h4>
							</div>
							<div id="faq-cat-8-sub-2" class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="row">		
										<div class="col-lg-12">
											<table id="facebookAdFormTable" data-toggle="table" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-page-list="[10, 20,40, ALL]" data-show-footer="false" data-side-pagination="server" data-url="{{ route('lead-form-list') }}">
												<thead>
													<tr>
														<th data-field="#" data-halign="center" data-align="center" @if(in_array("0",$AdFormTable)) data-visible="false" @endif>#</th>
														<th data-field="Form Name" data-sortable="true" @if(in_array("1",$AdFormTable)) data-visible="false" @endif>Form Name</th>
														<th data-field="Action" data-halign="center" data-align="center" @if(in_array("2",$AdFormTable)) data-visible="false" @endif>Action</th>
													</tr>
												</thead>
												<tbody>  
													
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endif
			</div>
			<!--tab ends-->
		</div>
	</div>
	
	<div id="add_group_category" class="modal fade animated" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: rgb(79, 193, 233);">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Add New Group Category</h4>
				</div>
				<div class="modal-body">
					<div class="row m-t-10">
						<div class="col-md-12">
							<div class="form-group">
								<label class="sr-only" for="group_category_name">Group Category Name</label>
								<input type="text" name="group_category_name" id="group_category_name" placeholder="Group Category Name" class="form-control m-t-10">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" id="save_group_category">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">
						Close
					</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="add_group_sub_category" class="modal fade animated" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: rgb(79, 193, 233);">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Add New Group Sub-Category</h4>
				</div>
				<div class="modal-body">
					<div class="row m-t-10">
						<div class="col-md-12">
							<div class="form-group">
								<label class="sr-only" for="group_sub_category_name">Group Sub-Category Name</label>
								<input type="text" name="group_sub_category_name" id="group_sub_category_name" placeholder="Group Sub-Category Name" class="form-control m-t-10">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success" id="save_group_sub_category">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">
						Close
					</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- Modals Section-->
	<div id="add_client_modal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" style="color:#fff;opacity: 0.8;">&times;</button>
					<h4 class="modal-title">Add Client</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_name">Name</label>
								<input type="text" class="form-control" id="client_name" name="client_name" placeholder="Name">
								<small class="text-danger animated fadeInUp client_name add_client_error"></small>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_address">Address</label>
								<input type="text" class="form-control" id="client_address" name="client_address" placeholder="Address">
								<small class="text-danger animated fadeInUp client_address add_client_error"></small>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_zip">Zip</label>
								<input type="text" class="form-control" id="client_zip" name="client_zip" placeholder="Zip">
								<small class="text-danger animated fadeInUp client_zip add_client_error"></small>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_mobile">Phone</label>
								<input type="text" class="form-control" id="client_mobile" name="client_mobile" placeholder="Phone" data-inputmask='"mask": "(999) 999-9999"' data-mask/>
								<small class="text-danger animated fadeInUp client_mobile add_client_error"></small>
							</div>
							<div class="col-md-4 col-sm-12  form_field form-group">
								<label for="client_email">Email</label>
								<input type="email" class="form-control" id="client_email" name="client_email" placeholder="Email">
								<small class="text-danger animated fadeInUp client_email add_client_error"></small>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_lead_form">Facebook Lead Form</label>
								<select multiple="multiple" placeholder="Select Lead Form" name="client_lead_form[]" id="client_lead_form" class="select1 SumoUnder">
									@foreach($FacebookAdsLeads as $FacebookAdsLead)
									<option  value="{{ $FacebookAdsLead->id }}">{{ $FacebookAdsLead->name }}</option>
									@endforeach
								</select>
								<small class="text-danger animated fadeInUp client_lead_form add_client_error"></small>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_contact_name">Contact Name</label>
								<input type="text" class="form-control" id="client_contact_name" name="client_contact_name" placeholder="Contact Name">
								<small class="text-danger animated fadeInUp client_contact_name add_client_error"></small>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_package_id">Package</label>
								<select id="client_package_id" name="client_package_id" class="form_select">
									<option value="">Select Package</option>
									@foreach($Packages as $Package)
									<option value="{{ $Package->id }}" @if($Package->id == '3') selected @endif>{{ $Package->name }}</option>
									@endforeach
								</select>
								<input id="client_package_type" name="client_package_type" type="hidden">
								<small class="text-danger animated fadeInUp client_package_id add_client_error"></small>
							</div>
							
							<div class="col-md-4 col-sm-12 form_field form-group ClientPerSoldValue" style="display:none">
								<label for="client_per_sold_value">Per Sold Value</label>
								
								<input id="client_per_sold_value" name="client_per_sold_value" type="text" class="form-control" placeholder="Per Sold Value" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<small class="text-danger animated fadeInUp client_per_sold_value"></small>
							</div>
							
							<div class="col-md-4 col-sm-12 form_field form-group ClientDiscount">
								<label for="client_discount">Discount (Amount $)</label>
								<input id="client_discount" name="client_discount" type="text" class="form-control" placeholder="Discount Amount" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<small class="text-danger animated fadeInUp client_discount add_client_error"></small>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_url">URL</label>
								<input type="url" class="form-control" id="client_url" name="client_url" placeholder="Url">
								<small class="text-danger animated fadeInUp client_url add_client_error"></small>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_group_category_id">Group Category</label>
								<select id="client_group_category_id" name="client_group_category_id" class="form-control" placeholder="Select Group Category">
									<option value="">Select Group Category</option> 
									@foreach($GroupCategories as $GroupCategory)
									<option value="{{ $GroupCategory->id }}" @if($GroupCategory->id == '6') selected @endif>{{ $GroupCategory->name }}</option>
									@endforeach
								</select>
								<small class="text-danger animated fadeInUp client_group_category_id add_client_error"></small>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_group_sub_category_id">Group Sub-Category</label>
								<select id="client_group_sub_category_id" name="client_group_sub_category_id" class="form-control" placeholder="Select Group Sub-Category">
									<option value="">Select Group Sub-Category</option> 
									@foreach($GroupSubCategories as $GroupSubCategory)
									<option value="{{ $GroupSubCategory->id }}">{{ $GroupSubCategory->name }}</option>
									@endforeach
								</select>
								<small class="text-danger animated fadeInUp client_group_sub_category_id add_client_error"></small>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_is_override">Override</label>
								<select id="client_is_override" name="client_is_override" class="form_select">
									<option value="No">No</option>
									<option value="Yes" selected>Yes</option>
								</select>
								<small class="text-danger animated fadeInUp client_is_override add_client_error"></small>
							</div>
							
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_crm_email">CRM Email</label>
								<input id="client_crm_email" name="client_crm_email" type="email" class="form-control" placeholder="CRM Email">
								<small class="text-danger animated fadeInUp client_crm_email add_client_error"></small>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_photo">Upload Avatar ( 180x180 )</label>
								<input id="client_photo" name="client_photo" type="file" class="avatar" accept="image/*">
								<small class="text-danger animated fadeInUp client_photo add_client_error"></small>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-3 col-sm-12 form_field form-group">
								<label for="client_start_date">Start Date</label>
								<input value="{{ date('m-d-Y') }}" id="client_start_date" name="client_start_date" type="text" class="form-control" placeholder="Start Date" readonly style="background-color:#FFF">
								<small class="text-danger animated fadeInUp client_start_date add_client_error"></small>
							</div>
							<div class="col-md-3 col-sm-12 form_field form-group">
								<label for="client_roi_id">ROI ID</label>
								<input id="client_roi_id" name="client_roi_id" type="text" class="form-control" placeholder="ROI ID">
								<small class="text-danger animated fadeInUp client_roi_id add_client_error"></small>
							</div>
							<div class="col-md-3 col-sm-12 form_field form-group">
								<label for="client_roi_automation">ROI Automation</label><br>
								<label class="radio-inline"><input type="radio" name="client_roi_automation" value="No" checked> No </label>
								<label class="radio-inline"><input type="radio" name="client_roi_automation" value="Yes"> Yes </label><br>
								<small class="text-danger animated fadeInUp client_roi_automation add_client_error"></small>
							</div>
							<div class="col-md-3 col-sm-12 form_field form-group">
								<label for="client_pro_rate">Pro Rate</label><br>
								<label class="radio-inline"><input type="radio" name="client_pro_rate" value="No" checked> No </label>
								<label class="radio-inline"><input type="radio" name="client_pro_rate" value="Yes"> Yes </label><br>
								<small class="text-danger animated fadeInUp client_pro_rate add_client_error"></small>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">							
							<div class="col-md-3 col-sm-12  form_field form-group">
								<label>Page Engagements</label><br/>
								<label class="radio-inline"><input type="radio" name="page_engagements" value="On"   /> On</label>
								<label class="radio-inline"><input type="radio" name="page_engagements" value="Off" checked /> Off</label><br>
							</div>
							<div class="col-md-3 col-sm-12 hidden form_field form-group facebook_pages_container">
								<label for="facebook_pages">Select Page</label>
								<select multiple="multiple" placeholder="Select Facebook Pages" name="facebook_pages[]" id="facebook_pages" class="select1 SumoUnder">
									@foreach($facebook_pages as $facebook_page)
									<option value="{{ $facebook_page->id }}">{{ $facebook_page->name }}</option>
									@endforeach
								</select>								
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-default"  style=" color:#fff; background-color: #ffbe18! important; border-color: #e4a70c !important;" id="AddClient"> Add Client</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"> Cancel</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="client_package_delete" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: rgb(255, 182, 95);">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Warning</h4>
				</div>
				<div class="modal-body">
					<p style="font-weight:500; font-size:14px"><span style="color:red; font-weight:600" id="client_response"></span> Clients are assigned to this package, remove all clients from this package before delete.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="package_client_detail">Details</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="client_package_notification" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: rgb(79, 193, 233);">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Client List</h4>
				</div>
				<div class="modal-body" id="client_warning_notification" style="overflow: scroll; height: 306px;">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="vendor_package_delete" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: rgb(255, 182, 95);">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Warning</h4>
				</div>
				<div class="modal-body">
					<p style="font-weight:500; font-size:14px"><span style="color:red; font-weight:600" id="vendor_response"></span> Vendors are assigned to this package, remove all vendors from this package before delete.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="package_vendor_detail">Details</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="vendor_package_notification" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: rgb(79, 193, 233);">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Vendor List</h4>
				</div>
				<div class="modal-body" id="vendor_warning_notification" style="overflow: scroll; height: 306px;">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close
					</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="client_group_delete" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: rgb(255, 182, 95);">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Warning</h4>
				</div>
				<div class="modal-body">
					<p style="font-weight:500; font-size:14px"><span style="color:red; font-weight:600" id="group_response"></span> Clients are assigned to this group, remove all clients from this group before delete.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="group_client_detail">Details</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="group_client_notification" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: rgb(79, 193, 233);">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Client List</h4>
				</div>
				<div class="modal-body" id="group_warning_notification" style="overflow: scroll; height: 306px;">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close
					</button>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- InputMask -->
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.date.extensions.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.extensions.js')}}" type="text/javascript"></script>
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<script src="{{asset('assets/js/sumoselect/jquery.sumoselect.min.js')}}" type="text/javascript" charset="utf-8"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.all.min.js')}}" ></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap3-wysihtml5-bower/js/bootstrap3-wysihtml5.min.js')}}"></script>

<script>
	"use strict";
	$( document ).ready(function() {    
		$('#Package-Table').bootstrapTable();
		$('#Vendor-Package-Table').bootstrapTable();
		$('#FB-Ads-Account-Table').bootstrapTable();
		$('#FacebookPageTable').bootstrapTable();
		$('#facebookAdFormTable').bootstrapTable();
		$('#UserEmailNotification').bootstrapTable();
		var extra_features = '<br><div class="pull-right" style="padding: 5px;"><b style="font-size: 14px;">Appointment Notifier</b> <div title="Appointment Notifier" class="switch11" style="display: inline-flex;" title="Switch to select all"><input id="cmn-toggle-10" type="checkbox" value="All" class="cmn-toggle cmn-toggle-round"><label for="cmn-toggle-10"></label></div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b style="font-size: 14px;">Lead Notifier</b> <div title="Lead Notifier" class="switch22" style="display: inline-flex;" title="Switch to select all"><input id="cmn-toggle-20" type="checkbox" value="All" class="cmn-toggle cmn-toggle-round"><label for="cmn-toggle-20"></label></div></div>';
		$("#faq-cat-5 .fixed-table-toolbar").append(extra_features);
		
		$("textarea.editor-cls").wysihtml5();
		
		$("[data-mask]").inputmask();
		
		var options1 = {format: "m-d-Y", dropPrimaryColor: "#428bca"};
		$('#client_start_date').dateDropper($.extend({}, options1));
		
		$('#group_sub_category_id').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		$('#client_list').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		$('#page_engagements').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		$('#lead_form').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		$('#client_lead_form').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		
		$(".bttn_reset").click(function(){
			$('#client_list')[0].sumo.unSelectAll();
			$('#lead_form')[0].sumo.unSelectAll();
		});
		
		$(".add_group_category").click(function(){
			$('#group_category_name').val('');
		});
		
		$(".add_group_sub_category").click(function(){
			$('#group_sub_category_name').val('');
		});
		
		$("#save_group_category").click(function(){
			
			if($('#group_category_name').val()=='')
			{
				swal('The category name field is required');
				return false;
			}
			
			$.get( "{{ route('create_group_category') }}", { group_category_name: $('#group_category_name').val() } )
			.done(function( data ) {
				var innerhtm='<select multiple="multiple" placeholder="Select Sub-Category" name="group_sub_category_id[]" id="group_sub_category_id" class="select1 SumoUnder">';
				
				for(var i=0;i<data.length;i++)
				{
					innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				innerhtm +='</select>';
				$('#div_group_sub_category_id').html(innerhtm);
				$('#group_sub_category_id').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
				$(".add_group_category").click();
			});
		});
		
		$("#group_category_id").change(function(){
			if($(this).val()=='')
			{
				$('.add_group_sub_category').css('display','none');
			}
			else
			{
				$('.add_group_sub_category').css('display','block');
			}
			
			$.get( "{{ route('get_group_sub_category') }}", { group_category_id: $(this).val() } )
			.done(function( data ) {
				var innerhtm='<select multiple="multiple" placeholder="Select Sub-Category" name="group_sub_category_id[]" id="group_sub_category_id" class="select1 SumoUnder">';
				
				for(var i=0;i<data.length;i++)
				{
					innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				innerhtm +='</select>';
				$('#div_group_sub_category_id').html(innerhtm);
				$('#group_sub_category_id').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
			});
		});
		
		$(".reset_lead").click(function () {
			$('.reset_lead').addClass('btn-primary');
			$('.reset_lead').removeClass('btn-warning');
			$(".client-list ul li").removeClass("active");
			$('#lead_title').html('Client\'s User Email Notification (Please select client from the filter above)');
			$('#UserEmailNotification').bootstrapTable('refresh');
			$.get("{{ route('check-email-notification') }}", { 'client_id': $(".client-list ul li.active").attr("user_client_id")}, function(data) {
				if (data.lead_notifier == 'On') 
				{
					$('#cmn-toggle-20').prop('checked', true);
				}
				else
				{
					$('#cmn-toggle-20').prop('checked', false);
				}
				
				if (data.appointment_notifier == 'On') 
				{
					$('#cmn-toggle-10').prop('checked', true);
				}
				else
				{
					$('#cmn-toggle-10').prop('checked', false);
				}
			});
		});
		
		@if(Session::get('client_id') > 0)
		$.get("{{ route('get_client_name') }}",{client_id: "{{ Session::get('client_id') }}"},function(data){ 
			$('#lead_title').html(data+' User Email Notification (Please select client from the filter above)');
		});
		@endif
		
		$(".client-list ul li").click(function() {
			var client_id = $(this).attr('user_client_id');
			if(client_id==undefined)
			{
				$('#UserEmailNotification').bootstrapTable('refresh');
				$('#lead_title').html('Client\'s User Email Notification (Please select client from the filter above)');
				$('.reset_lead').addClass('btn-primary');
				$('.reset_lead').removeClass('btn-warning');
			}
			else
			{
				$.get("{{ route('get_client_name') }}",{client_id: client_id},function(data){ 
					$('#lead_title').html(data+' User Email Notification (Please select client from the filter above)');
				});
				$('.reset_lead').addClass('btn-warning');
				$('.reset_lead').removeClass('btn-primary');
				
				$('#UserEmailNotification').bootstrapTable('refresh');
			}
			
			$.get("{{ route('check-email-notification') }}", { 'client_id': $(".client-list ul li.active").attr("user_client_id")}, function(data) {
				if (data.lead_notifier == 'On') 
				{
					$('#cmn-toggle-20').prop('checked', true);
				}
				else
				{
					$('#cmn-toggle-20').prop('checked', false);
				}
				
				if (data.appointment_notifier == 'On') 
				{
					$('#cmn-toggle-10').prop('checked', true);
				}
				else
				{
					$('#cmn-toggle-10').prop('checked', false);
				}
			});
		});
		
		$(".switch11 input[type='checkbox']").on('change', function (event, state) {
			var client_id = $(".client-list ul li.active").attr("user_client_id");
			var checked = $(this).prop('checked');
			$('#UserEmailNotification').find('.appointment_notifier').prop('checked', checked);
			
			$('#date-loader').show();
			if($(this).is(':checked')) 
			{
				$.get("{{ route('all-user-appointment-notifier') }}", { 'client_id':client_id, 'appointment_notifier':'On' }, function(data) {
					//alert('On');
					$('#date-loader').hide();
				});			
			} 
			else 
			{
				$.get("{{ route('all-user-appointment-notifier') }}", { 'client_id':client_id, 'appointment_notifier':'Off' }, function(data) {
					//alert('Off');
					$('#date-loader').hide();
				});			
			}
		});
		
		$(".switch22 input[type='checkbox']").on('change', function (event, state) {
			var client_id = $(".client-list ul li.active").attr("user_client_id");
			var checked = $(this).prop('checked');
			$('#UserEmailNotification').find('.lead_notifier').prop('checked', checked);
			
			$('#date-loader').show();
			if($(this).is(':checked')) 
			{
				$.get("{{ route('all-user-lead-notifier') }}", { 'client_id':client_id, 'lead_notifier':'On' }, function(data) {
					//alert('On');
					$('#date-loader').hide();
				});			
			} 
			else 
			{
				$.get("{{ route('all-user-lead-notifier') }}", { 'client_id':client_id, 'lead_notifier':'Off' }, function(data) {
					//alert('Off');
					$('#date-loader').hide();
				});			
			}
		});
		
		$("#save_group_sub_category").click(function(){
			
			if($('#group_category_id').val()=='')
			{
				swal('The group category name field is required');
				return false;
			}
			else if($('#group_sub_category_name').val()=='')
			{
				swal('The sub category name field is required');
				return false;
			}
			
			$.get( "{{ route('create_group_sub_category') }}", { group_category_id: $('#group_category_id').val(), group_sub_category_name: $('#group_sub_category_name').val() } )
			.done(function( data ) {
				var innerhtm='<option value="">Select Sub-Category</option>';
				
				for(var i=0;i<data.length;i++)
				{
					innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				
				$('#group_sub_category_id').html(innerhtm);
				
				$(".add_group_sub_category").click();
			});
		});
		
		<?php if(app('request')->exists('package_id')) { ?>
			$("#Package-Tab").trigger("click");
		<?php }?>
		
		<?php if(app('request')->exists('vendor_package_id')) { ?>
			$("#Vendor-Package-Tab").trigger("click");
		<?php }?>
	});	
</script>

<script>
	$(document).ready(function(){
		$("#client_group_category_id").change(function(){
			$.get( "{{ route('get_group_sub_category') }}", { group_category_id: $(this).val() } )
			.done(function( data ) {
				var innerhtm='<option value="">Select Group Sub-Category</option>';
				for(var i=0;i<data.length;i++)
				{
					innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				$('#client_group_sub_category_id').html(innerhtm);
			});
		});
		
		$(".vendor_client_limit_integer").css('display','none');
		$(".vendor_ads_account_integer").css('display','none');
		$(".vendor_campaign_integer").css('display','none');
		$(".vendor_leads_amount_integer").css('display','none');
		
		@if(app('request')->exists('vendor_package_id'))
		
		@if($EditVendorPackage->client_limit=='Limited')
		$(".vendor_client_limit_integer").css('display','block');
		@endif
		
		@if($EditVendorPackage->ads_account=='Limited')
		$(".vendor_ads_account_integer").css('display','block');
		@endif
		
		@if($EditVendorPackage->campaign=='Limited')
		$(".vendor_campaign_integer").css('display','block');
		@endif
		
		@if($EditVendorPackage->leads_amount=='Limited')
		$(".vendor_leads_amount_integer").css('display','block');
		@endif
		
		@endif
		
		$("#submit_client_package").click(function(){
			$('.client_package_validate').html('');
			var package_name=$('#package_name').val();
			var package_type=$('#package_type').val();
			var per_lead_value=$('#per_lead_value').val();
			var monthly_lead_quantity=$('#monthly_lead_quantity').val();
			var amount=$('#amount').val();
			var error='no';
			if(package_name=='')
			{
				error='yes';
				$('.package_name').html('The package name is required');
			}
			
			if(package_type=='Monthly')
			{
				if(monthly_lead_quantity=='')
				{
					error='yes';
					$('.monthly_lead_quantity').html('The monthly lead quantity is required');
				}
				
				if(amount=='')
				{
					error='yes';
					$('.amount').html('The amount is required');
				}
			}
			else if(package_type=='Lead')
			{
				if(per_lead_value=='')
				{
					error='yes';
					$('.per_lead_value').html('The lead value is required');
				}
			}
			
			if(error=='yes')
			{
				return false;
			}
		});
		
		$("#vendor_client_limits").change(function(){
			if($(this).val()=='Limited')
			{
				$('.vendor_client_limit_integer').css('display','block');
			}
			else
			{
				$('.vendor_client_limit_integer').css('display','none');
				$('#vendor_client_limit_integer').val('0');
			}
		});
		
		$("#vendor_ads_accounts").change(function(){
			if($(this).val()=='Limited')
			{
				$('.vendor_ads_account_integer').css('display','block');
			}
			else
			{
				$('.vendor_ads_account_integer').css('display','none');
				$('#vendor_ads_account_integer').val('0');
			}
		});
		
		$("#vendor_campaigns").change(function(){
			if($(this).val()=='Limited')
			{
				$('.vendor_campaign_integer').css('display','block');
			}
			else
			{
				$('.vendor_campaign_integer').css('display','none');
				$('#vendor_campaign_integer').val('0');
			}
		});
		
		$("#vendor_leads_amount").change(function(){
			if($(this).val()=='Limited')
			{
				$('.vendor_leads_amount_integer').css('display','block');
			}
			else
			{
				$('.vendor_leads_amount_integer').css('display','none');
				$('#vendor_leads_amount_integer').val('0');
			}
		});
		
		$("#package_type").change(function(){
			if($(this).val()=='Lead')
			{
				$('.lead_value_hide').css('display','block');
				$('.package_amount_hide').css('display','none');
			}
			else if($(this).val()=='Monthly')
			{
				$('.lead_value_hide').css('display','none');
				$('.package_amount_hide').css('display','block');
				$('#per_lead_value').val('');
			}
			else
			{
				$('.lead_value_hide').css('display','none');
				$('.package_amount_hide').css('display','none');
				$('#per_lead_value').val('');
				$('#amount').val('');
			}
		});
	});
</script>

<script>
	$( document ).ready(function() {
		$('.add_client').click( function(){
			$('#add_client_modal').modal("show");
		});
		
		$('#client_package_id').change(function(){
			var package_id=$(this).val();
			if(package_id!='')
			{
				$.get("{{ route('check-package-type') }}",{package_id: package_id},function(data){ 
					if(data=='Sold')
					{
						$('.ClientPerSoldValue').css('display','block');
						$('.ClientDiscount').css('display','none');
						$('#client_package_type').val(data);
						$('#client_discount').val('');
					}
					else
					{
						$('.ClientDiscount').css('display','block');
						$('.ClientPerSoldValue').css('display','none');
						$('#client_package_type').val(data);
						$('#client_per_sold_value').val('');
					}
				});
			}
			else
			{
				$('.ClientPerSoldValue').css('display','none');
				$('.ClientDiscount').css('display','none');
				$('#client_package_type').val('');
				$('#client_discount').val('');
				$('#client_per_sold_value').val('');
			}
		});
		
		$('#AddClient').click( function(){
			error='No';
			$('.add_client_error').html('');
			$('.client_per_sold_value').html('');
			var client_package_id=$("#client_package_id").val();
			var client_package_type=$('#client_package_type').val();
			var client_discount=$('#client_discount').val();
			var client_per_sold_value=$('#client_per_sold_value').val();					
			
			if ($('input[name="page_engagements"]').prop('checked') && $('input[name="page_engagements"]').val() == "On" && !$('#facebook_pages').val())
			{
				$('.facebook_pages_container').addClass('has-error').children('small').remove();
				$('.facebook_pages_container').append('<small class="help-block" data-bv-validator="notEmpty" data-bv-for="facebook_pages[]" data-bv-result="INVALID" style="display: block;">Select atleast one page</small>');
				$('#facebook_pages').focus();
				return;
			}
			else
			{
				$('.facebook_pages_container').removeClass('has-error').addClass('has-success').children('small').remove();    			
			}
			
			var client_name=$("#client_name").val();
			var client_address=$("#client_address").val();
			var client_zip=$("#client_zip").val();
			var client_mobile=$("#client_mobile").val();
			var client_email=$("#client_email").val();
			var client_lead_form=$("#client_lead_form").val();
			var client_contact_name=$("#client_contact_name").val();
			var client_is_override=$("#client_is_override").val();
			var client_url=$("#client_url").val();
			var client_group_category_id = $("#client_group_category_id").val();
			var client_group_sub_category_id = $("#client_group_sub_category_id").val();
			var client_crm_email=$("#client_crm_email").val();
			var client_start_date=$("#client_start_date").val();
			var client_roi_id=$("#client_roi_id").val();
			var client_roi_automation=$('input[name=client_roi_automation]:checked').val();
			var client_pro_rate=$('input[name=client_pro_rate]:checked').val();
			var page_engagements=$('input[name=page_engagements]:checked').val();
			var facebook_pages=$('#facebook_pages').val();
			var _token="{{ csrf_token() }}";
			var client_photo=$('#client_photo')[0].files[0];
			
			var form = new FormData();
			form.append('_token', _token);
			form.append('client_name', client_name);
			form.append('client_address', client_address);
			form.append('client_zip', client_zip);
			form.append('client_mobile', client_mobile);
			form.append('client_email', client_email);
			form.append('client_lead_form', JSON.stringify(client_lead_form));
			form.append('client_contact_name', client_contact_name);
			form.append('client_package_id', client_package_id);
			form.append('client_per_sold_value', client_per_sold_value);
			form.append('client_discount', client_discount);
			form.append('client_is_override', client_is_override);
			form.append('client_url', client_url);
			form.append('client_group_category_id', client_group_category_id);
			form.append('client_group_sub_category_id', client_group_sub_category_id);
			form.append('client_crm_email', client_crm_email);
			form.append('client_start_date', client_start_date);
			form.append('client_pro_rate', client_pro_rate);
			form.append('page_engagements', page_engagements);
			form.append('facebook_pages[]', facebook_pages);
			form.append('client_roi_id', client_roi_id);
			form.append('client_roi_automation', client_roi_automation);
			form.append('client_photo', client_photo);
			
			if(client_package_id!='')
			{
				if(client_package_type=='Sold')
				{
					if(client_per_sold_value=='')
					{
						$('.client_per_sold_value').html('The per sold value is required');
					}
					else
					{
						$.ajax({
							url: "{{ route('create_group_client') }}",
							data: form,
							cache: false,
							contentType: false,
							processData: false,
							type: 'POST',
							success:function(response) {
								$("#client_name").val('');
								$("#client_address").val('');
								$("#client_zip").val('');
								$("#client_mobile").val('');
								$("#client_email").val('');
								$("#client_lead_form").val('');
								$("#client_contact_name").val('');
								$("#client_package_id").val('');
								$("#client_per_sold_value").val('');
								$("#client_discount").val('');
								$("#client_is_override").val('');
								$("#client_url").val('');
								$("#client_brand").val('');
								$("#client_crm_email").val('');
								$('input[name="page_engagements"]').iCheck('uncheck');
								$('.facebook_pages_container').removeClass('hidden');
								$('#facebook_pages')[0].sumo.unSelectAll();
								$('.facebook_pages_container').addClass('hidden');
								$("#client_start_date").val("{{ date('m-d-Y') }}");
								
								$.get( "{{ route('get_group_client') }}").done(function( data ) {
									var innerhtm='<div class="input-group"><span class="input-group-addon"><i class="fa fa-fw fa-edit"></i></span><select multiple="multiple" placeholder="Select Client" name="client_list[]" id="client_list" class="select1 SumoUnder">';
									
									for(var i=0;i<data.length;i++)
									{
										innerhtm +='<option value="'+data[i].id+'">'+data[i].name+' ('+data[i].email+')</option>';
									}
									innerhtm +='</select></div>';
									
									$('#client_list_hide').html(innerhtm);
									$('#client_list').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
								});
								
								$('#add_client_modal').modal("hide");
							},
							error: function (textStatus, errorThrown) {
								var obj = textStatus.responseJSON;
								$.each(obj, function(k, v) {
									//display the key and value pair
									$('.'+k).html(v);
								});
							}
						});
					}
				}
				else
				{
					$.get("{{ route('check-client-discount') }}",{package_id: client_package_id,discount_amount: client_discount},function(data){ 
						if(data!='No')
						{
							$('.client_discount').html(data);
						}
						else
						{
							$.ajax({
								url: "{{ route('create_group_client') }}",
								data: form,
								cache: false,
								contentType: false,
								processData: false,
								type: 'POST',
								success:function(response) {
									$("#client_name").val('');
									$("#client_address").val('');
									$("#client_zip").val('');
									$("#client_mobile").val('');
									$("#client_email").val('');
									$("#client_lead_form").val('');
									$("#client_contact_name").val('');
									$("#client_package_id").val('');
									$("#client_per_sold_value").val('');
									$("#client_discount").val('');
									$("#client_is_override").val('');
									$("#client_url").val('');
									$("#client_brand").val('');
									$("#client_crm_email").val('');
									$('input[name="page_engagements"]').iCheck('uncheck');
									$('.facebook_pages_container').removeClass('hidden');
									$('#facebook_pages')[0].sumo.unSelectAll();
									$('.facebook_pages_container').addClass('hidden');
									
									$("#client_start_date").val("{{ date('m-d-Y') }}");
									
									$.get( "{{ route('get_group_client') }}").done(function( data ) {
										var innerhtm='<div class="input-group"><span class="input-group-addon"><i class="fa fa-fw fa-edit"></i></span><select multiple="multiple" placeholder="Select Client" name="client_list[]" id="client_list" class="select1 SumoUnder">';
										
										for(var i=0;i<data.length;i++)
										{
											innerhtm +='<option value="'+data[i].id+'">'+data[i].name+' ('+data[i].email+')</option>';
										}
										innerhtm +='</select></div>';
										
										$('#client_list_hide').html(innerhtm);
										$('#client_list').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
									});
									
									$('#add_client_modal').modal("hide");
								},
								error: function (textStatus, errorThrown) {
									var obj = textStatus.responseJSON;
									$.each(obj, function(k, v) {
										//display the key and value pair
										$('.'+k).html(v);
									});
								}
							});
						}
					});
				}
			}
			else
			{
				$.ajax({
					url: "{{ route('create_group_client') }}",
					data: form,
					cache: false,
					contentType: false,
					processData: false,
					type: 'POST',
					success:function(response) {
						$("#client_name").val('');
						$("#client_address").val('');
						$("#client_zip").val('');
						$("#client_mobile").val('');
						$("#client_email").val('');
						$("#client_lead_form").val('');
						$("#client_contact_name").val('');
						$("#client_package_id").val('');
						$("#client_per_sold_value").val('');
						$("#client_discount").val('');
						$("#client_is_override").val('');
						$("#client_url").val('');
						$("#client_brand").val('');
						$("#client_crm_email").val('');
						$('input[name="page_engagements"]').iCheck('uncheck');
						$('.facebook_pages_container').removeClass('hidden');
						$('#facebook_pages')[0].sumo.unSelectAll();
						$('.facebook_pages_container').addClass('hidden');
						
						$("#client_start_date").val("{{ date('m-d-Y') }}");
						
						$.get( "{{ route('get_group_client') }}").done(function( data ) {
							var innerhtm='<div class="input-group"><span class="input-group-addon"><i class="fa fa-fw fa-edit"></i></span><select multiple="multiple" placeholder="Select Client" name="client_list[]" id="client_list" class="select1 SumoUnder">';
							
							for(var i=0;i<data.length;i++)
							{
								innerhtm +='<option value="'+data[i].id+'">'+data[i].name+' ('+data[i].email+')</option>';
							}
							innerhtm +='</select></div>';
							
							$('#client_list_hide').html(innerhtm);
							$('#client_list').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
						});
						
						$('#add_client_modal').modal("hide");
					},
					error: function (textStatus, errorThrown) {
						var obj = textStatus.responseJSON;
						$.each(obj, function(k, v) {
							//display the key and value pair
							$('.'+k).html(v);
						});
					}
				});
			}
		});
	});
</script>

<script>
	$(document).ready(function(){
		$('#faq-cat-1 .dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Setting Group Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$('#faq-cat-2 .dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Setting Client Package Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$('#faq-cat-3 .dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Setting Vendor Package Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$('#faq-cat-4 .dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Setting Facebook Ads Account Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$('#faq-cat-8-sub-1 .dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name = 'Block Facebook Page Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$('#faq-cat-8-sub-2 .dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name = 'Block Facebook Ad Form Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
	});	
</script>
<script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/inner_pages_validation.js')}}"></script>
<script src="{{asset('assets/vendors/toolbar/js/jquery.toolbar.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/tabs_accordions.js')}}"></script>
<!-- end of page level js -->
<script src="{{asset('assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".form_field").find('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
		
		var extra_features = '<button class="btn btn-default" type="button" name="mail" title="Create"><i class="fa fa-user-plus"></i></button><button class="btn btn-default" type="button" name="mail" title="Setting" ><i class="fa fa-fw fa-cogs"></i></button><button class="btn btn-default" type="button" name="print" title="Print" ><i class="fa fa-fw fa-print"></i></button>';
		$("#faq-cat-4 .fixed-table-toolbar .columns").prepend(extra_features);
		
		$('.email_notification').change(function(){  
			var noyification_type = $(this).val();
			var noyification_status = '';
			if($(this).prop('checked'))
			{
				noyification_status = 'Yes';
			}
			else
			{
				noyification_status = 'No';
			}
			
			$.get('email-notification',{'noyification_type':noyification_type,'noyification_status':noyification_status}).then(function(response){
				//alert('Done');
			});
		});
		
		if ('{{ $appointment_notifier }}' == 'yes') 
		{
			$('#cmn-toggle-10').prop('checked', false);
		}
		else
		{
			$('#cmn-toggle-10').prop('checked', true);
		}
		
		if ('{{ $lead_notifier }}' == 'yes') 
		{
			$('#cmn-toggle-20').prop('checked', false);
		}
		else
		{
			$('#cmn-toggle-20').prop('checked', true);
		}
		
		$('.sumo_lead_form > .okCancelInMulti > .MultiControls > .btnOk').click(function() {
			$('#date-loader').show();
			var facebook_ads_lead_id = $('#lead_form').val();
			var client_id = $('#client_list').val();
			if(facebook_ads_lead_id == null || client_id == null)
			{
				$('#date-loader').hide();
			}
			else
			{
				$.get("{{ route('check-client-lead-form') }}",{client_id: client_id.toString(), facebook_ads_lead_id: facebook_ads_lead_id.toString() }, function(data){
					if(data == 0)
					{
						$('#date-loader').hide();
					}
					else
					{
						var num = $('#lead_form > option').length;
						
						for(var i=0; i<num; i++)
						{
							$('#lead_form')[0].sumo.unSelectItem(i);
						}
						
						$('#date-loader').hide();
						swal('Error !', 'Some selected lead form assigned to another client which are not selected in group.', 'error');
					}
				}); 
			}
		});
		
		$('.sumo_client_list > .okCancelInMulti > .MultiControls > .btnOk').click(function() {
			$('#date-loader').show();
			var facebook_ads_lead_id = $('#lead_form').val();
			var client_id = $('#client_list').val();
			if(facebook_ads_lead_id == null || client_id == null)
			{
				$('#date-loader').hide();
			}
			else
			{
				$.get("{{ route('check-client-lead-form') }}",{client_id: client_id.toString(), facebook_ads_lead_id: facebook_ads_lead_id.toString()}, function(data){
					if(data == 0)
					{
						$('#date-loader').hide();
					}
					else
					{
						var num = $('#lead_form > option').length;
						
						for(var i=0; i<num; i++)
						{
							$('#lead_form')[0].sumo.unSelectItem(i);
						}
						
						$('#date-loader').hide();
						
						swal('Error !', 'Some selected lead form assigned to another client which are not selected in group.', 'error');
					}
				}); 
			}
		});
		
		$(".switch01 input[type='checkbox']").on('change', function (event, state) {
			var facebook_ads_lead_id = $(this).val();
			var $section = $(this);
			if($(this).is(':checked')) 
			{
				var is_blocked = 0;
				swal({
					title: 'Are you sure?',
					text: "Our system will fetch leads from this Ad from if unblocked",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, unblock it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.get("{{ route('block-lead-form') }}",{'id':facebook_ads_lead_id, 'is_blocked':is_blocked}).then(function(response){
						swal('Unblocked!', 'Ad form unblock successfully.', 'success');
						//window.location.reload(true);
						//$('#facebookAdFormTable').bootstrapTable('refresh');
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Ad form unblock cancelled successfully.', 'error');
						$section.prop("checked", false);
					}
				}); 
			} 
			else 
			{
				var is_blocked = 1;
				swal({
					title: 'Are you sure?',
					text: "Our system will not fetch leads from this Ad from if blocked & it will get unassign from Clients/Groups.",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, block it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.get("{{ route('block-lead-form') }}",{'id':facebook_ads_lead_id, 'is_blocked':is_blocked}).then(function(response){
						swal('Blocked!', 'Form blocked successfully.', 'success');
						//window.location.reload(true);
						//$('#facebookAdFormTable').bootstrapTable('refresh');
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Ad form block cancelled successfully.', 'error');
						$section.prop("checked", true);
					}
				});
			}
		});
		
		
		setInterval(function(){
			$(".switch01 input[type='checkbox']").on('change', function (event, state) {
				var facebook_ads_lead_id = $(this).val();
				var $section = $(this);
				if($(this).is(':checked')) 
				{
					var is_blocked = 0;
					swal({
						title: 'Are you sure?',
						text: "Our system will fetch leads from this Ad from if unblocked",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#22D69D',
						cancelButtonColor: '#FB8678',
						confirmButtonText: 'Yes, unblock it!',
						cancelButtonText: 'No, cancel!',
						confirmButtonClass: 'btn',
						cancelButtonClass: 'btn'
						}).then(function () {
						$.get("{{ route('block-lead-form') }}",{'id':facebook_ads_lead_id, 'is_blocked':is_blocked}).then(function(response){
							swal('Unblocked!', 'Form unblocked successfully.', 'success');
							//window.location.reload(true);
							//$('#facebookAdFormTable').bootstrapTable('refresh');
						});
						}, function (dismiss) {
						// dismiss can be 'cancel', 'overlay',
						// 'close', and 'timer'
						if (dismiss === 'cancel') {
							swal('Cancelled', 'Ad form unblock cancelled successfully.', 'error');
							$section.prop("checked", false);
						}
					}); 
				} 
				else 
				{
					var is_blocked = 1;
					swal({
						title: 'Are you sure?',
						text: "Our system will not fetch leads from this Ad from if blocked & it will get unassign from Clients/Groups.",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#22D69D',
						cancelButtonColor: '#FB8678',
						confirmButtonText: 'Yes, block it!',
						cancelButtonText: 'No, cancel!',
						confirmButtonClass: 'btn',
						cancelButtonClass: 'btn'
						}).then(function () {
						$.get("{{ route('block-lead-form') }}",{'id':facebook_ads_lead_id, 'is_blocked':is_blocked}).then(function(response){
							swal('Blocked!', 'Ad form block successfully.', 'success');
							//window.location.reload(true);
							//$('#facebookAdFormTable').bootstrapTable('refresh');
						});
						}, function (dismiss) {
						// dismiss can be 'cancel', 'overlay',
						// 'close', and 'timer'
						if (dismiss === 'cancel') {
							swal('Cancelled', 'Ad form block cancelled successfully.', 'error');
							$section.prop("checked", true);
						}
					});
				}
			});
		}, 1000);
		
		/////////Facebook page////////////////
		$(".switch02 input[type='checkbox']").on('change', function (event, state) {
			var facebook_ads_lead_id = $(this).val();
			var $section = $(this);
			if($(this).is(':checked')) 
			{
				var is_blocked = 0;
				swal({
					title: 'Are you sure?',
					text: "Our system show this facebook page.",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, show it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.get("{{ route('facebook-page-hide') }}",{'id':facebook_ads_lead_id, 'is_blocked':is_blocked}).then(function(response){
						swal('Show!', 'facebook page show successfully.', 'success');
						//window.location.reload(true);
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'facebook page show cancelled successfully.', 'error');
						$section.prop("checked", false);
					}
				}); 
			} 
			else 
			{
				var is_blocked = 1;
				swal({
					title: 'Are you sure?',
					text: "Our system hide this facebook page & it will get unassign from Clients/Groups.",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, hide it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.get("{{ route('facebook-page-hide') }}",{'id':facebook_ads_lead_id, 'is_blocked':is_blocked}).then(function(response){
						swal('Hide!', 'Facebook page hide successfully.', 'success');
						//window.location.reload(true);
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Facebook page hide cancelled successfully.', 'error');
						$section.prop("checked", true);
					}
				});
			}
		});
		
		setInterval(function(){
			$(".switch02 input[type='checkbox']").on('change', function (event, state) {
				var facebook_ads_lead_id = $(this).val();
				var $section = $(this);
				if($(this).is(':checked')) 
				{
					var is_blocked = 0;
					swal({
						title: 'Are you sure?',
						text: "Our system show this facebook page.",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#22D69D',
						cancelButtonColor: '#FB8678',
						confirmButtonText: 'Yes, show it!',
						cancelButtonText: 'No, cancel!',
						confirmButtonClass: 'btn',
						cancelButtonClass: 'btn'
						}).then(function () {
						$.get("{{ route('facebook-page-hide') }}",{'id':facebook_ads_lead_id, 'is_blocked':is_blocked}).then(function(response){
							swal('Show!', 'facebook page show successfully.', 'success');
							//window.location.reload(true);
						});
						}, function (dismiss) {
						// dismiss can be 'cancel', 'overlay',
						// 'close', and 'timer'
						if (dismiss === 'cancel') {
							swal('Cancelled', 'facebook page show cancelled successfully.', 'error');
							$section.prop("checked", false);
						}
					}); 
				} 
				else 
				{
					var is_blocked = 1;
					swal({
						title: 'Are you sure?',
						text: "Our system hide this facebook page & it will get unassign from Clients/Groups.",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#22D69D',
						cancelButtonColor: '#FB8678',
						confirmButtonText: 'Yes, hide it!',
						cancelButtonText: 'No, cancel!',
						confirmButtonClass: 'btn',
						cancelButtonClass: 'btn'
						}).then(function () {
						$.get("{{ route('facebook-page-hide') }}",{'id':facebook_ads_lead_id, 'is_blocked':is_blocked}).then(function(response){
							swal('Hide!', 'Facebook page hide successfully.', 'success');
							//window.location.reload(true);
						});
						}, function (dismiss) {
						// dismiss can be 'cancel', 'overlay',
						// 'close', and 'timer'
						if (dismiss === 'cancel') {
							swal('Cancelled', 'Facebook page hide cancelled successfully.', 'error');
							$section.prop("checked", true);
						}
					});
				}
			});
		}, 1000);
		
		$("#zipcode, #client_zip").inputmask('99999');
		
		//Post Engagements
		$('#facebook_pages').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		$('input[name="page_engagements"]').on('ifChanged',function(){ 
			if($(this).val()=="On")
			{ 
				$('.facebook_pages_container').removeClass('hidden');
				$('#facebook_pages')[0].sumo.unSelectAll();
			}
			else
			{ 
				$('.facebook_pages_container').addClass('hidden');                 
			}            
		});
		
		$('.div_page_engagements').addClass('hidden');
		
		$('input[name="group_type"]').on('ifChanged',function(){ 
			if($(this).val() == "Page Engagements")
			{ 
				$('.div_page_engagements').removeClass('hidden');
				$('.div_lead_form').addClass('hidden');
				$('#lead_form')[0].sumo.unSelectAll();
				$('#page_engagements')[0].sumo.unSelectAll(); 
			}
			else if($(this).val() == "From Ad Forms")
			{ 
				$('.div_lead_form').removeClass('hidden');
				$('.div_page_engagements').addClass('hidden');
				$('#page_engagements')[0].sumo.unSelectAll();  
				$('#lead_form')[0].sumo.unSelectAll();
			}            
		});
	});
	
	function deleteClientPackage(id)
	{
		swal({
			title: 'Are you sure?',
			text: "You will not be able to recover this package!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#22D69D',
			cancelButtonColor: '#FB8678',
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn',
			cancelButtonClass: 'btn'
			}).then(function () {
			$.post('delete-package',{'_token':$('#_token').val(),'id':id}).then(function(response){
				if(response=='Yes')
				{
					swal('Deleted!', 'Your package has been deleted.', 'success');
					window.location.reload(true); 
				}
				else
				{
					client_package_delete(id,response);
				}
			});
			}, function (dismiss) {
			// dismiss can be 'cancel', 'overlay',
			// 'close', and 'timer'
			if (dismiss === 'cancel') {
				swal('Cancelled', 'Your package is safe.', 'error');
			}
		})
	}
	
	function client_package_delete(id, clients)
	{
		$('#client_response').html(clients);
		$('#package_client_detail').attr('onclick', 'client_package_notification('+id+')');
		$('#client_package_delete').modal("show");
	}
	
	function client_package_notification(id)
	{
		$.get("{{ route('package-client-detail') }}",{id: id},function(data){
			var innerhtm='<ul class="list-group">';
			
			for(var i=0;i<data.length;i++)
			{
				innerhtm +='<li class="list-group-item">'+data[i].name+' ('+data[i].email+')</li>';
			}
			
			innerhtm +='</ul>';
			
			$('#client_warning_notification').html(innerhtm);
		});
		$('#client_package_delete').modal("hide");
		$('#client_package_notification').modal("show");
	}
	
	function vendor_package_delete(id, vendors)
	{
		$('#vendor_response').html(vendors);
		$('#vendor_package_delete').modal("show");
		$('#package_vendor_detail').attr('onclick', 'vendor_package_notification('+id+')');
	}
	
	function vendor_package_notification(id)
	{
		$.get("{{ route('package-vendor-detail') }}",{id: id},function(data){
			var innerhtm='<ul class="list-group">';
			
			for(var i=0;i<data.length;i++)
			{
				innerhtm +='<li class="list-group-item">'+data[i].name+' ('+data[i].email+')</li>';
			}
			
			innerhtm +='</ul>';
			
			$('#vendor_warning_notification').html(innerhtm);
		});
		$('#vendor_package_delete').modal("hide");
		$('#vendor_package_notification').modal("show");
	}
	
	function client_group_delete(id, clients)
	{
		$('#group_response').html(clients);
		$('#client_group_delete').modal("show");
		$('#group_client_detail').attr('onclick', 'group_client_notification('+id+')');
	}
	
	function group_client_notification(id)
	{
		$.get("{{ route('group-client-detail') }}",{id: id},function(data){
			var innerhtm='<ul class="list-group">';
			
			for(var i=0;i<data.length;i++)
			{
				innerhtm +='<li class="list-group-item">'+data[i].name+' ('+data[i].email+')</li>';
			}
			
			innerhtm +='</ul>';
			
			$('#group_warning_notification').html(innerhtm);
		});
		$('#client_group_delete').modal("hide");
		$('#group_client_notification').modal("show");
	}
	
	function queryParams(params) {
		params.client_id = $(".client-list ul li.active").attr("user_client_id"); // add param1
		// console.log(JSON.stringify(params));
		// {"limit":10,"offset":0,"order":"asc","your_param1":1,"your_param2":2}
		return params;
	}
	
	function AppointmentNotifier(manager_id, appointment_notifier) {
		$('#date-loader').show();
		$.get("{{ route('user-appointment-notifier') }}", { 'id':manager_id, 'appointment_notifier':appointment_notifier }, function(data) {
			$('#UserEmailNotification').bootstrapTable('refresh');
			$('#date-loader').hide();
			if ($('.abcdef:checked').length == $('.abcdef').length) 
			{
				$('#cmn-toggle-10').prop('checked', true);
			}
			else
			{
				$('#cmn-toggle-10').prop('checked', false);
			}
		});	
	}
	
	function LeadNotifier(manager_id, lead_notifier) {
		$('#date-loader').show();
		$.get("{{ route('user-lead-notifier') }}", { 'id':manager_id, 'lead_notifier':lead_notifier }, function(data) {
			$('#UserEmailNotification').bootstrapTable('refresh');
			$('#date-loader').hide();
			if ($('.ghijkl:checked').length == $('.ghijkl').length) 
			{
				$('#cmn-toggle-20').prop('checked', true);
			}
			else
			{
				$('#cmn-toggle-20').prop('checked', false);
			}
		});	
	}
	
	$.get("{{ route('check-email-notification') }}", { 'client_id': $(".client-list ul li.active").attr("user_client_id")}, function(data) {
		if (data.lead_notifier == 'On') 
		{
			$('#cmn-toggle-20').prop('checked', true);
		}
		else
		{
			$('#cmn-toggle-20').prop('checked', false);
		}
		
		if (data.appointment_notifier == 'On') 
		{
			$('#cmn-toggle-10').prop('checked', true);
		}
		else
		{
			$('#cmn-toggle-10').prop('checked', false);
		}
	});			
</script>
@stop																																										