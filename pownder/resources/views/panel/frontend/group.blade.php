@extends('layouts/default')

{{-- Page title --}}
@section('title')
Settings
@parent
@stop
{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/js/sumoselect/sumoselect.css')}}">
<link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datedropper/datedropper.css')}}">
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	.fixed-table-toolbar .search {width: Calc( 100% - 108px );}
	
	optgroup {background-color: #fb9f98; color: #000;}
	
	.export.btn-group {*display:none !important;}
	
	@media  screen and (max-width: 767px) {
	.fixed-table-toolbar {width: 100%;}
	.fixed-table-toolbar {width:100%;}
	.fixed-table-toolbar .search {width: Calc( 100% );}
	}
	
	.SumoSelect {width: 100% !important; color: #000 !important;} 
	
	.SumoSelect .select-all {height: 35px !important;}
	
	.options {max-height: 150px !important;}
	
	.SelectBox {padding: 7px 12px !important;}
	
	SumoSelect .no-match {display: none !important; padding: 6px;}
	
	.faq-cat-content .panel-heading:hover {background-color: #1dd4cb;}
	.fixed-table-container {height: 500px !important; }
</style>

<!--end of page level css-->
@stop
{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">	Settings</li>
	</ol>
</section>
<?php
	$errorarr=$errors;   
	function showerrormsg($key,$errors)
	{    
		$msg='';
		if(count($errors->get($key))>0)
		{ 
			$msg.='';
			foreach($errors->get($key) as $error)
			{
				if($key=="alert_success")
				{
					$msg.=$error;
				}
				else
				{
					$msg.='<small class="text-danger animated fadeInUp">'.$error.'</small>' ; 
				}
			}
		} 
		return $msg;     
	}
?>
<!-- Main content -->
<section class="content p-l-r-15">
    @include('panel.includes.status')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel-group" id="accordion-cat-1">
				@if($WritePermission=='Yes')
				<div class="panel panel-success panel-faq">
					<div class="panel-heading">
						<h4 class="panel-title" style="color: #fff;">
							<i class="fa fa-fw fa-file-text-o"></i> Edit Create Group
							<span class="pull-right"></span>
						</h4>
					</div>
					<div id="faq-cat-1-sub-1" class="panel-collapse collapse in">
						<div class="panel-body">
							<form action="{{ route('edit-group-submit') }}" class="form-horizontal" method="post" id="form-validation">
								<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
								<div class="form-body">
									<div class="form-group">
										<label for="name" class="col-md-3 control-label"> Group Name</label>
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-fw fa-edit"></i>
												</span>
												<input value="{{ $group->name }}" id="name" type="text" name="name" class="form-control"
												placeholder="Name">
												<input value="{{ $group->id }}" id="id" type="hidden" name="id">
											</div>
											<?php echo showerrormsg('name',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group">
										<label for="group_category_id" class="col-md-3 control-label"> Category</label>
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-fw fa-edit"></i>
												</span>
												<div id="div_group_category_id">
													<select id="group_category_id" name="group_category_id" class="form-control">
														<option value="">Select Category</option>
														@foreach($GroupCategories as $GroupCategory)
														<option value="{{ $GroupCategory->id }}" @if($group->group_category_id == $GroupCategory->id) selected @endif>{{ $GroupCategory->name }}</option>
														@endforeach
													</select>
												</div>
											</div>
											<?php echo showerrormsg('group_category_id',$errorarr); ?>
										</div>
										@if(Session::get('user_category')=='admin')
										<div class="col-md-2" style="padding:0px">
											<button type="button" class="btn btn-info add_group_category" data-toggle="modal" data-target="#add_group_category" title="Add new category">
												<i class="fa fa-plus" aria-hidden="true"></i>
											</button>
										</div>
										@endif
									</div>
									
									<div class="form-group">
										<label for="group_sub_category_id" class="col-md-3 control-label"> Sub-Category</label>
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-fw fa-edit"></i>
												</span>
												<div id="div_group_sub_category_id">
													<select multiple="multiple" placeholder="Select Sub-Category" name="group_sub_category_id[]" id="group_sub_category_id" class="select1 SumoUnder">
														<?php $SubCategory_Array=explode(',',$group->group_sub_category_id); ?>
														@foreach($GroupSubCategories as $GroupSubCategory)
														<option value="{{ $GroupSubCategory->id }}" @if(in_array($GroupSubCategory->id, $SubCategory_Array)) {{ 'selected' }} @endif>{{ $GroupSubCategory->name }}</option>
														@endforeach
													</select>
												</div>
											</div>
											<?php echo showerrormsg('group_sub_category_id',$errorarr); ?>
										</div>
										@if(Session::get('user_category')=='admin')
										<div class="col-md-2" style="padding:0px">
											<button type="button" class="btn btn-info add_group_sub_category" data-toggle="modal" data-target="#add_group_sub_category" style="display:none" title="Add new sub-category">
												<i class="fa fa-plus" aria-hidden="true"></i>
											</button>
										</div>
										@endif
									</div>
									
									<div class="form-group">
										<label for="zipcode" class="col-md-3 control-label">
											Zip code
										</label>
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-fw fa-edit"></i>
												</span>
												<input value="{{ $group->zipcode }}" type="text" placeholder="Zip code"
												id="zipcode" name="zipcode" class="form-control"/>
											</div>
											<?php echo showerrormsg('zipcode',$errorarr); ?>
										</div>
									</div>
									<div class="form-group">
										<label for="milesradius" class="col-md-3 control-label"> Miles Radius</label>
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-fw fa-edit"></i>
												</span>
												<input value="{{ $group->milesradius }}" type="text" id="milesradius" name="milesradius" placeholder="Miles Radius"
												class="form-control"/>        
											</div>
											<?php echo showerrormsg('milesradius',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group">
										<label for="client_list" class="col-md-3 control-label"> Client</label>
										<div class="col-md-6">
											<div  id="client_list_hide">
												<div class="input-group">
													<span class="input-group-addon">
														<i class="fa fa-fw fa-edit"></i>
													</span>
													<select multiple="multiple" placeholder="Select Client" name="client_list[]" id="client_list" class="select1 SumoUnder">
														<?php $Client_Array=explode(',', $group->client_id); ?>
														@foreach($Clients as $Client)
														<option value="{{ $Client->id }}" @if(in_array($Client->id, $Client_Array)) {{ 'selected' }} @endif>{{ $Client->name }} ({{ $Client->email }})</option>
														@endforeach
													</select>
												</div>
											</div>
											<?php echo showerrormsg('client_list',$errorarr); ?>
										</div>
										<div class="col-md-2" style="padding:0px">
											@if($ClientPermission=='Yes')
											<button type="button" class="btn btn-info add_client" title="Add new client">
												<i class="fa fa-plus" aria-hidden="true"></i>
											</button>
											@endif
										</div>
									</div>
									
									<div class="form-group">
										<label for="group_type" class="col-md-3 control-label"> Group Type</label>
										<div class="col-md-6 form_field">
											<label class="radio-inline"><input type="radio" name="group_type" value="Page Engagements" @if($group->group_type == 'Page Engagements') checked @endif> Page Engagements</label>
											<label class="radio-inline"><input type="radio" name="group_type" value="From Ad Forms" @if($group->group_type == 'From Ad Forms') checked @endif> From Ad Forms</label><br>
											<?php echo showerrormsg('group_type',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group div_lead_form">
										<label for="lead_form" class="col-md-3 control-label"> Lead Form</label>
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-fw fa-edit"></i>
												</span>
												<select multiple="multiple" placeholder="Select Lead Form" name="lead_form[]" id="lead_form"  class="select1 SumoUnder">
													<?php $LeadForm_Array=explode(',',$group->facebook_ads_lead_id); ?>
													@foreach($LeadForms as $LeadForm)
													<option value="{{ $LeadForm->id }}" @if(in_array($LeadForm->id, $LeadForm_Array)) {{ 'selected' }} @endif>{{ $LeadForm->name }}</option>
													@endforeach
												</select>
											</div>
											<?php echo showerrormsg('lead_form',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group div_page_engagements">
										<label for="lead_form" class="col-md-3 control-label"> Page Engagements</label>
										<div class="col-md-6">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="fa fa-fw fa-edit"></i>
												</span>
												<select multiple="multiple" placeholder="Select Page Engagements" name="page_engagements[]" id="page_engagements"  class="select1 SumoUnder">
													<?php $Page_Array=explode(',',$group->facebook_page_id); ?>
													@foreach($page_engagements as $page_engagement)
													<option value="{{ $page_engagement->id }}" @if(in_array($page_engagement->id, $Page_Array)) {{ 'selected' }} @endif>{{ $page_engagement->name }}</option>
													@endforeach
												</select>
											</div>
											<?php echo showerrormsg('page_engagements',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-actions">
										<div class="row">
											<div class="col-md-12 text-center">
												<button type="submit" class="btn btn-primary"> Submit</button> 
												<button type="reset" class="btn btn-default bttn_reset"> Reset</button>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				@endif
				<div class="panel panel-success panel-faq">
					<div class="panel-heading">
						<h4 class="panel-title" style="color: #fff;">
							<i class="fa fa-fw fa-th-large"></i> Group Client List
							<span class="pull-right"></span>
						</h4>
					</div>
					<div id="faq-cat-1-sub-2" class="panel-collapse collapse in">
						<div class="panel-body">
							<div class="row">		
								<div class="col-lg-12">
									<table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-query-params="queryParams" data-pagination="true"  data-page-list="[10, 20, 40, ALL]" data-show-footer="false"  data-toggle="table">
										<thead>
											<tr>
												<th data-field="ID" data-sortable="true" @if(in_array("0",$ClientColumns)) data-visible="false" @endif>ID</th>
												<th data-field="Date" data-sortable="true" @if(in_array("1",$ClientColumns)) data-visible="false" @endif>Date</th>
												<th data-field="Status" data-sortable="true" @if(in_array("2",$ClientColumns)) data-visible="false" @endif>Status</th>
												<th data-field="Client" data-sortable="true" @if(in_array("3",$ClientColumns)) data-visible="false" @endif>Client</th>
												
												@if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
												
												<th data-field="Owner" data-sortable="true" @if(in_array("4",$ClientColumns)) data-visible="false" @endif>Owner</th>
												<th data-field="Package" data-sortable="true" @if(in_array("5",$ClientColumns)) data-visible="false" @endif>Package</th>
												<th data-field="Ads" data-sortable="true" data-align="right" @if(in_array("6",$ClientColumns)) data-visible="false" @endif>Ads</th>
												<th data-field="CPs" data-sortable="true" data-align="right" @if(in_array("7",$ClientColumns)) data-visible="false" @endif>CPs</th>
												<th data-field="LDs" data-sortable="true" data-align="right" @if(in_array("8",$ClientColumns)) data-visible="false" @endif>LDs</th>
												
												@else
												
												<th data-field="Package" data-sortable="true" @if(in_array("4",$ClientColumns)) data-visible="false" @endif>Package</th>
												<th data-field="Ads" data-sortable="true" data-align="right" @if(in_array("5",$ClientColumns)) data-visible="false" @endif>Ads</th>
												<th data-field="CPs" data-sortable="true" data-align="right" @if(in_array("6",$ClientColumns)) data-visible="false" @endif>CPs</th>
												<th data-field="LDs" data-sortable="true" data-align="right" @if(in_array("7",$ClientColumns)) data-visible="false" @endif>LDs</th>
												
												@endif
											</tr>
										</thead>
										<tbody>
											@foreach($group_clients as $row)
											<tr>
												<td>{{ $row->client_no }}</td>
												<td>{{ date('m-d-Y',strtotime($row->created_at)) }}</td>
												<td>@if($row->status == 1) Active @else Inactive @endif</td>
												<td>{{ $row->name }}</td>
												
												@if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin')
												<td>@if($row->vendor_name==null || $row->vendor_name=='') Admin @else {{ $row->vendor_name }} @endif</td>
												@endif
												
												<td>{{ $row->package_name }}</td>
												<td>{{ $row->Ads }}</td>
												<td>{{ $row->CPs }}</td>
												<td>@if($LeadList=='Yes') <a href="{{ route('leads') }}?client_id={{ $row->id }}&page={{ $group->facebook_page_id }}&form={{ $group->facebook_ads_lead_id }}">{{ $row->LDs }}</a> @else {{ $row->LDs }} @endif</td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->

<div id="add_group_category" class="modal fade animated" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color: rgb(79, 193, 233);">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title"> Add New Group Category</h4>
			</div>
			<div class="modal-body">
				<div class="row m-t-10">
					<div class="col-md-12">
						<div class="form-group">
							<label class="sr-only" for="group_category_name"> Group Category Name</label>
							<input type="text" name="group_category_name" id="group_category_name" placeholder="Group Category Name" class="form-control m-t-10">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="save_group_category"> Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"> Close</button>
			</div>
		</div>
	</div>
</div>

<div id="add_group_sub_category" class="modal fade animated" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color: rgb(79, 193, 233);">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title"> Add New Group Sub-Category</h4>
			</div>
			<div class="modal-body">
				<div class="row m-t-10">
					<div class="col-md-12">
						<div class="form-group">
							<label class="sr-only" for="group_sub_category_name"> Group Sub-Category Name</label>
							<input type="text" name="group_sub_category_name" id="group_sub_category_name" placeholder="Group Sub-Category Name" class="form-control m-t-10">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" id="save_group_sub_category"> Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">	Close</button>
			</div>
		</div>
	</div>
</div>

<!-- Modals Section-->
<div id="add_client_modal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" style="color:#fff;opacity: 0.8;">&times;</button>
				<h4 class="modal-title">Add Client</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_name">Name</label>
							<input type="text" class="form-control" id="client_name" name="client_name" placeholder="Name">
							<small class="text-danger animated fadeInUp client_name add_client_error"></small>
						</div>
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_address">Address</label>
							<input type="text" class="form-control" id="client_address" name="client_address" placeholder="Address">
							<small class="text-danger animated fadeInUp client_address add_client_error"></small>
						</div>
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_zip">Zip</label>
							<input type="text" class="form-control" id="client_zip" name="client_zip" placeholder="Zip">
							<small class="text-danger animated fadeInUp client_zip add_client_error"></small>
						</div>
					</div>
					<div class="col-md-12 col-sm-12">
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_mobile">Phone</label>
							<input type="text" class="form-control" id="client_mobile" name="client_mobile" placeholder="Phone" data-inputmask='"mask": "(999) 999-9999"' data-mask/>
							<small class="text-danger animated fadeInUp client_mobile add_client_error"></small>
						</div>
						<div class="col-md-4 col-sm-12  form_field form-group">
							<label for="client_email">Email</label>
							<input type="email" class="form-control" id="client_email" name="client_email" placeholder="Email">
							<small class="text-danger animated fadeInUp client_email add_client_error"></small>
						</div>
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_lead_form">Facebook Lead Form</label>
							<select multiple="multiple" placeholder="Select Lead Form" name="client_lead_form[]" id="client_lead_form" class="select1 SumoUnder">
								@foreach($FacebookAdsLeads as $FacebookAdsLead)
								<option  value="{{ $FacebookAdsLead->id }}">{{ $FacebookAdsLead->name }}</option>
								@endforeach
							</select>
							<small class="text-danger animated fadeInUp client_lead_form add_client_error"></small>
						</div>
					</div>
					<div class="col-md-12 col-sm-12">
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_contact_name">Contact Name</label>
							<input type="text" class="form-control" id="client_contact_name" name="client_contact_name" placeholder="Contact Name">
							<small class="text-danger animated fadeInUp client_contact_name add_client_error"></small>
						</div>
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_package_id">Package</label>
							<select id="client_package_id" name="client_package_id" class="form_select">
								<option value="">Select Package</option>
								@foreach($Packages as $Package)
								<option value="{{ $Package->id }}" @if($Package->id == '3') selected @endif>{{ $Package->name }}</option>
								@endforeach
							</select>
							<input id="client_package_type" name="client_package_type" type="hidden">
							<small class="text-danger animated fadeInUp client_package_id add_client_error"></small>
						</div>
						
						<div class="col-md-4 col-sm-12 form_field form-group ClientPerSoldValue" style="display:none">
							<label for="client_per_sold_value">Per Sold Value</label>
							
							<input id="client_per_sold_value" name="client_per_sold_value" type="text" class="form-control" placeholder="Per Sold Value" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
							<small class="text-danger animated fadeInUp client_per_sold_value"></small>
						</div>
						
						<div class="col-md-4 col-sm-12 form_field form-group ClientDiscount">
							<label for="client_discount">Discount (Amount $)</label>
							<input id="client_discount" name="client_discount" type="text" class="form-control" placeholder="Discount Amount" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
							<small class="text-danger animated fadeInUp client_discount add_client_error"></small>
						</div>
					</div>
					<div class="col-md-12 col-sm-12">
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_url">URL</label>
							<input type="url" class="form-control" id="client_url" name="client_url" placeholder="Url">
							<small class="text-danger animated fadeInUp client_url add_client_error"></small>
						</div>
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_group_category_id">Group Category</label>
							<select id="client_group_category_id" name="client_group_category_id" class="form-control" placeholder="Select Group Category">
								<option value="">Select Group Category</option> 
								@foreach($GroupCategories as $GroupCategory)
								<option value="{{ $GroupCategory->id }}" @if($group->group_category_id == $GroupCategory->id) selected @endif>{{ $GroupCategory->name }}</option>
								@endforeach
							</select>
							<small class="text-danger animated fadeInUp client_group_category_id add_client_error"></small>
						</div>
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_group_sub_category_id">Group Sub-Category</label>
							<select id="client_group_sub_category_id" name="client_group_sub_category_id" class="form-control" placeholder="Select Group Sub-Category">
								<option value="">Select Group Sub-Category</option>
								@foreach($GroupSubCategories as $GroupSubCategory)
								<option value="{{ $GroupSubCategory->id }}">{{ $GroupSubCategory->name }}</option>
								@endforeach
							</select>
							<small class="text-danger animated fadeInUp client_group_sub_category_id add_client_error"></small>
						</div>
					</div>
					<div class="col-md-12 col-sm-12">
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_is_override">Override</label>
							<select id="client_is_override" name="client_is_override" class="form_select">
								<option value="No">No</option>
								<option value="Yes" selected>Yes</option>
							</select>
							<small class="text-danger animated fadeInUp client_is_override add_client_error"></small>
						</div>
						
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_crm_email">CRM Email</label>
							<input id="client_crm_email" name="client_crm_email" type="email" class="form-control" placeholder="CRM Email">
							<small class="text-danger animated fadeInUp client_crm_email add_client_error"></small>
						</div>
						<div class="col-md-4 col-sm-12 form_field form-group">
							<label for="client_photo">Upload Avatar ( 180x180 )</label>
							<input id="client_photo" name="client_photo" type="file" class="avatar" accept="image/*">
							<small class="text-danger animated fadeInUp client_photo add_client_error"></small>
						</div>
					</div>
					<div class="col-md-12 col-sm-12">
						<div class="col-md-3 col-sm-12 form_field form-group">
							<label for="client_start_date">Start Date</label>
							<input value="{{ date('m-d-Y') }}" id="client_start_date" name="client_start_date" type="text" class="form-control" placeholder="Start Date" readonly style="background-color:#FFF">
							<small class="text-danger animated fadeInUp client_start_date add_client_error"></small>
						</div>
						<div class="col-md-3 col-sm-12 form_field form-group">
							<label for="client_roi_id">ROI ID</label>
							<input id="client_roi_id" name="client_roi_id" type="text" class="form-control" placeholder="ROI ID">
							<small class="text-danger animated fadeInUp client_roi_id add_client_error"></small>
						</div>
						<div class="col-md-3 col-sm-12 form_field form-group">
							<label for="client_roi_automation">ROI Automation</label><br>
							<label class="radio-inline"><input type="radio" name="client_roi_automation" value="No" checked> No </label>
							<label class="radio-inline"><input type="radio" name="client_roi_automation" value="Yes"> Yes </label><br>
							<small class="text-danger animated fadeInUp client_roi_automation add_client_error"></small>
						</div>
						<div class="col-md-3 col-sm-12 form_field form-group">
							<label for="client_pro_rate">Pro Rate</label><br>
							<label class="radio-inline"><input type="radio" name="client_pro_rate" value="No" checked> No </label>
							<label class="radio-inline"><input type="radio" name="client_pro_rate" value="Yes"> Yes </label><br>
							<small class="text-danger animated fadeInUp client_pro_rate add_client_error"></small>
						</div>
					</div>
					<div class="col-md-12 col-sm-12">							
						<div class="col-md-3 col-sm-12  form_field form-group">
							<label>Page Engagements</label><br/>
							<label class="radio-inline"><input type="radio" name="page_engagements" value="On"   /> On</label>
							<label class="radio-inline"><input type="radio" name="page_engagements" value="Off" checked /> Off</label><br>
						</div>
						<div class="col-md-3 col-sm-12 hidden form_field form-group facebook_pages_container ">
							<label for="facebook_pages">Select Page</label>
							<select multiple="multiple" placeholder="Select Facebook Pages" name="facebook_pages[]" id="facebook_pages" class="select1 SumoUnder">
								@foreach($facebook_pages as $facebook_page)
								<option value="{{ $facebook_page->id }}">{{ $facebook_page->name }}</option>
								@endforeach
							</select>								
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-default"  style=" color:#fff; background-color: #ffbe18! important; border-color: #e4a70c !important;" id="AddClient"> Add Client</button>
				<button type="button" class="btn btn-default" data-dismiss="modal"> Cancel</button>
			</div>
		</div>
	</div>
</div>

<div id="client_package_delete" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color: rgb(255, 182, 95);">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Warning</h4>
			</div>
			<div class="modal-body">
				<p style="font-weight:500; font-size:14px"><span style="color:red; font-weight:600" id="client_response"></span> Clients are assigned to this package, remove all clients from this package before delete.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="package_client_detail">Details</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="client_package_notification" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color: rgb(79, 193, 233);">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Client List</h4>
			</div>
			<div class="modal-body" id="client_warning_notification" style="overflow: scroll; height: 306px;">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="vendor_package_delete" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color: rgb(255, 182, 95);">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Warning</h4>
			</div>
			<div class="modal-body">
				<p style="font-weight:500; font-size:14px"><span style="color:red; font-weight:600" id="vendor_response"></span> Vendors are assigned to this package, remove all vendors from this package before delete.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="package_vendor_detail">Details</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="vendor_package_notification" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color: rgb(79, 193, 233);">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Vendor List</h4>
			</div>
			<div class="modal-body" id="vendor_warning_notification" style="overflow: scroll; height: 306px;">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close
				</button>
			</div>
		</div>
	</div>
</div>

<div id="client_group_delete" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color: rgb(255, 182, 95);">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Warning</h4>
			</div>
			<div class="modal-body">
				<p style="font-weight:500; font-size:14px"><span style="color:red; font-weight:600" id="group_response"></span> Clients are assigned to this group, remove all clients from this group before delete.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" id="group_client_detail">Details</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="group_client_notification" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background-color: rgb(79, 193, 233);">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Client List</h4>
			</div>
			<div class="modal-body" id="group_warning_notification" style="overflow: scroll; height: 306px;">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close
				</button>
			</div>
		</div>
	</div>
</div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- InputMask -->
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.date.extensions.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.extensions.js')}}" type="text/javascript"></script>
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<script src="{{asset('assets/js/sumoselect/jquery.sumoselect.min.js')}}" type="text/javascript" charset="utf-8"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}"></script>
<script src="{{asset('assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
<script>
	"use strict";
	$( document ).ready(function() {    		
		$("[data-mask]").inputmask();
		
		$(".form_field,.form_field1").find('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});  
		
		var options1 = {format: "m-d-Y", dropPrimaryColor: "#428bca"};
		$('#client_start_date').dateDropper($.extend({}, options1));
		
		$('#group_sub_category_id').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		$('#client_list').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		$('#page_engagements').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		$('#lead_form').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		$('#client_lead_form').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		
		$(".bttn_reset").click(function(){
			$('#client_list')[0].sumo.unSelectAll();
			$('#lead_form')[0].sumo.unSelectAll();
		});
		
		$(".add_group_category").click(function(){
			$('#group_category_name').val('');
		});
		
		$(".add_group_sub_category").click(function(){
			$('#group_sub_category_name').val('');
		});
		
		$("#save_group_category").click(function(){
			
			if($('#group_category_name').val()=='')
			{
				swal('The category name field is required');
				return false;
			}
			
			$.get( "{{ route('create_group_category') }}", { group_category_name: $('#group_category_name').val() } )
			.done(function( data ) {
				var innerhtm='<select multiple="multiple" placeholder="Select Sub-Category" name="group_sub_category_id[]" id="group_sub_category_id" class="select1 SumoUnder">';
				
				for(var i=0;i<data.length;i++)
				{
					innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				innerhtm +='</select>';
				$('#div_group_sub_category_id').html(innerhtm);
				$('#group_sub_category_id').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
				$(".add_group_category").click();
			});
		});
		
		$("#group_category_id").change(function(){
			if($(this).val()=='')
			{
				$('.add_group_sub_category').css('display','none');
			}
			else
			{
				$('.add_group_sub_category').css('display','block');
			}
			
			$.get( "{{ route('get_group_sub_category') }}", { group_category_id: $(this).val() } )
			.done(function( data ) {
				var innerhtm='<select multiple="multiple" placeholder="Select Sub-Category" name="group_sub_category_id[]" id="group_sub_category_id" class="select1 SumoUnder">';
				
				for(var i=0;i<data.length;i++)
				{
					innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				innerhtm +='</select>';
				$('#div_group_sub_category_id').html(innerhtm);
				$('#group_sub_category_id').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
			});
		});
		
		$("#save_group_sub_category").click(function(){
			
			if($('#group_category_id').val()=='')
			{
				swal('The group category name field is required');
				return false;
			}
			else if($('#group_sub_category_name').val()=='')
			{
				swal('The sub category name field is required');
				return false;
			}
			
			$.get( "{{ route('create_group_sub_category') }}", { group_category_id: $('#group_category_id').val(), group_sub_category_name: $('#group_sub_category_name').val() } )
			.done(function( data ) {
				var innerhtm='<option value="">Select Sub-Category</option>';
				
				for(var i=0;i<data.length;i++)
				{
					innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				
				$('#group_sub_category_id').html(innerhtm);
				
				$(".add_group_sub_category").click();
			});
		});
	});	
</script>

<script>
	$(document).ready(function(){
		$("#client_group_category_id").change(function(){
			$.get( "{{ route('get_group_sub_category') }}", { group_category_id: $(this).val() } )
			.done(function( data ) {
				var innerhtm='<option value="">Select Group Sub-Category</option>';
				for(var i=0;i<data.length;i++)
				{
					innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				$('#client_group_sub_category_id').html(innerhtm);
			});
		});
	});
</script>

<script>
	$( document ).ready(function() {
		$('.add_client').click( function(){
			$('#add_client_modal').modal("show");
		});
		
		$('#client_package_id').change(function(){
			var package_id=$(this).val();
			if(package_id!='')
			{
				$.get("{{ route('check-package-type') }}",{package_id: package_id},function(data){ 
					if(data=='Sold')
					{
						$('.ClientPerSoldValue').css('display','block');
						$('.ClientDiscount').css('display','none');
						$('#client_package_type').val(data);
						$('#client_discount').val('');
					}
					else
					{
						$('.ClientDiscount').css('display','block');
						$('.ClientPerSoldValue').css('display','none');
						$('#client_package_type').val(data);
						$('#client_per_sold_value').val('');
					}
				});
			}
			else
			{
				$('.ClientPerSoldValue').css('display','none');
				$('.ClientDiscount').css('display','none');
				$('#client_package_type').val('');
				$('#client_discount').val('');
				$('#client_per_sold_value').val('');
			}
		});
		
		$('#AddClient').click( function(){
			error='No';
			$('.add_client_error').html('');
			$('.client_per_sold_value').html('');
			var client_package_id=$("#client_package_id").val();
			var client_package_type=$('#client_package_type').val();
			var client_discount=$('#client_discount').val();
			var client_per_sold_value=$('#client_per_sold_value').val();
			
			if ($('input[name="page_engagements"]').prop('checked') && $('input[name="page_engagements"]').val() == "On" && !$('#facebook_pages').val())
    		{
    			$('.facebook_pages_container').addClass('has-error').children('small').remove();
    			$('.facebook_pages_container').append('<small class="help-block" data-bv-validator="notEmpty" data-bv-for="facebook_pages[]" data-bv-result="INVALID" style="display: block;">Select atleast one page</small>');
    			$('#facebook_pages').focus();
                return;
			}
    		else
    		{
    			$('.facebook_pages_container').removeClass('has-error').addClass('has-success').children('small').remove();    			
			}
			
			var client_name=$("#client_name").val();
			var client_address=$("#client_address").val();
			var client_zip=$("#client_zip").val();
			var client_mobile=$("#client_mobile").val();
			var client_email=$("#client_email").val();
			var client_lead_form=$("#client_lead_form").val();
			var client_contact_name=$("#client_contact_name").val();
			var client_is_override=$("#client_is_override").val();
			var client_url=$("#client_url").val();
			var client_group_category_id = $("#client_group_category_id").val();
			var client_group_sub_category_id = $("#client_group_sub_category_id").val();
			var client_crm_email=$("#client_crm_email").val();
			var client_start_date=$("#client_start_date").val();
			var client_roi_id=$("#client_roi_id").val();
			var client_roi_automation=$('input[name=client_roi_automation]:checked').val();
			var client_pro_rate=$('input[name=client_pro_rate]:checked').val();
			var page_engagements=$('input[name=page_engagements]:checked').val();
            var facebook_pages=$('#facebook_pages').val();
			var _token="{{ csrf_token() }}";
			var client_photo=$('#client_photo')[0].files[0];
			
			var form = new FormData();
			form.append('_token', _token);
			form.append('client_name', client_name);
			form.append('client_address', client_address);
			form.append('client_zip', client_zip);
			form.append('client_mobile', client_mobile);
			form.append('client_email', client_email);
			form.append('client_lead_form', JSON.stringify(client_lead_form));
			form.append('client_contact_name', client_contact_name);
			form.append('client_package_id', client_package_id);
			form.append('client_per_sold_value', client_per_sold_value);
			form.append('client_discount', client_discount);
			form.append('client_is_override', client_is_override);
			form.append('client_url', client_url);
			form.append('client_group_category_id', client_group_category_id);
			form.append('client_group_sub_category_id', client_group_sub_category_id);
			form.append('client_crm_email', client_crm_email);
			form.append('client_start_date', client_start_date);
			form.append('client_pro_rate', client_pro_rate);
			form.append('page_engagements', page_engagements);
            form.append('facebook_pages[]', facebook_pages);
			form.append('client_roi_id', client_roi_id);
			form.append('client_roi_automation', client_roi_automation);
			form.append('client_photo', client_photo);
			
			if(client_package_id!='')
			{
				if(client_package_type=='Sold')
				{
					if(client_per_sold_value=='')
					{
						$('.client_per_sold_value').html('The per sold value is required');
					}
					else
					{
						$.ajax({
							url: "{{ route('create_group_client') }}",
							data: form,
							cache: false,
							contentType: false,
							processData: false,
							type: 'POST',
							success:function(response) {
								$("#client_name").val('');
								$("#client_address").val('');
								$("#client_zip").val('');
								$("#client_mobile").val('');
								$("#client_email").val('');
								$("#client_lead_form").val('');
								$("#client_contact_name").val('');
								$("#client_package_id").val('');
								$("#client_per_sold_value").val('');
								$("#client_discount").val('');
								$("#client_is_override").val('');
								$("#client_url").val('');
								$("#client_brand").val('');
								$("#client_crm_email").val('');
								$('input[name="page_engagements"]').iCheck('uncheck');
                                $('.facebook_pages_container').removeClass('hidden');
                                $('#facebook_pages')[0].sumo.unSelectAll();
                                $('.facebook_pages_container').addClass('hidden');
								$("#client_start_date").val("{{ date('m-d-Y') }}");
								
								$.get( "{{ route('get_group_client') }}").done(function( data ) {
									var innerhtm='<div class="input-group"><span class="input-group-addon"><i class="fa fa-fw fa-edit"></i></span><select multiple="multiple" placeholder="Select Client" name="client_list[]" id="client_list" class="select1 SumoUnder">';
									
									for(var i=0;i<data.length;i++)
									{
										innerhtm +='<option value="'+data[i].id+'">'+data[i].name+' ('+data[i].email+')</option>';
									}
									innerhtm +='</select></div>';
									
									$('#client_list_hide').html(innerhtm);
									$('#client_list').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
								});
								
								$('#add_client_modal').modal("hide");
							},
							error: function (textStatus, errorThrown) {
								var obj = textStatus.responseJSON;
								$.each(obj, function(k, v) {
									//display the key and value pair
									$('.'+k).html(v);
								});
							}
						});
					}
				}
				else
				{
					$.get("{{ route('check-client-discount') }}",{package_id: client_package_id,discount_amount: client_discount},function(data){ 
						if(data!='No')
						{
							$('.client_discount').html(data);
						}
						else
						{
							$.ajax({
								url: "{{ route('create_group_client') }}",
								data: form,
								cache: false,
								contentType: false,
								processData: false,
								type: 'POST',
								success:function(response) {
									$("#client_name").val('');
									$("#client_address").val('');
									$("#client_zip").val('');
									$("#client_mobile").val('');
									$("#client_email").val('');
									$("#client_lead_form").val('');
									$("#client_contact_name").val('');
									$("#client_package_id").val('');
									$("#client_per_sold_value").val('');
									$("#client_discount").val('');
									$("#client_is_override").val('');
									$("#client_url").val('');
									$("#client_brand").val('');
									$("#client_crm_email").val('');
									$('input[name="page_engagements"]').iCheck('uncheck');
									$('.facebook_pages_container').removeClass('hidden');
									$('#facebook_pages')[0].sumo.unSelectAll();
									$('.facebook_pages_container').addClass('hidden');
									$("#client_start_date").val("{{ date('m-d-Y') }}");
									
									$.get( "{{ route('get_group_client') }}").done(function( data ) {
										var innerhtm='<div class="input-group"><span class="input-group-addon"><i class="fa fa-fw fa-edit"></i></span><select multiple="multiple" placeholder="Select Client" name="client_list[]" id="client_list" class="select1 SumoUnder">';
										
										for(var i=0;i<data.length;i++)
										{
											innerhtm +='<option value="'+data[i].id+'">'+data[i].name+' ('+data[i].email+')</option>';
										}
										innerhtm +='</select></div>';
										
										$('#client_list_hide').html(innerhtm);
										$('#client_list').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
									});
									
									$('#add_client_modal').modal("hide");
								},
								error: function (textStatus, errorThrown) {
									var obj = textStatus.responseJSON;
									$.each(obj, function(k, v) {
										//display the key and value pair
										$('.'+k).html(v);
									});
								}
							});
						}
					});
				}
			}
			else
			{
				$.ajax({
					url: "{{ route('create_group_client') }}",
					data: form,
					cache: false,
					contentType: false,
					processData: false,
					type: 'POST',
					success:function(response) {
						$("#client_name").val('');
						$("#client_address").val('');
						$("#client_zip").val('');
						$("#client_mobile").val('');
						$("#client_email").val('');
						$("#client_lead_form").val('');
						$("#client_contact_name").val('');
						$("#client_package_id").val('');
						$("#client_per_sold_value").val('');
						$("#client_discount").val('');
						$("#client_is_override").val('');
						$("#client_url").val('');
						$("#client_brand").val('');
						$("#client_crm_email").val('');
						$('input[name="page_engagements"]').iCheck('uncheck');
						$('.facebook_pages_container').removeClass('hidden');
						$('#facebook_pages')[0].sumo.unSelectAll();
						$('.facebook_pages_container').addClass('hidden');
						$("#client_start_date").val("{{ date('m-d-Y') }}");
						
						$.get( "{{ route('get_group_client') }}").done(function( data ) {
							var innerhtm='<div class="input-group"><span class="input-group-addon"><i class="fa fa-fw fa-edit"></i></span><select multiple="multiple" placeholder="Select Client" name="client_list[]" id="client_list" class="select1 SumoUnder">';
							
							for(var i=0;i<data.length;i++)
							{
								innerhtm +='<option value="'+data[i].id+'">'+data[i].name+' ('+data[i].email+')</option>';
							}
							innerhtm +='</select></div>';
							
							$('#client_list_hide').html(innerhtm);
							$('#client_list').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
						});
						
						$('#add_client_modal').modal("hide");
					},
					error: function (textStatus, errorThrown) {
						var obj = textStatus.responseJSON;
						$.each(obj, function(k, v) {
							//display the key and value pair
							$('.'+k).html(v);
						});
					}
				});
			}
		});
		
		//Post Enagements
		$('#facebook_pages').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		$('input[name="page_engagements"]').on('ifChanged',function(){ 
			if($(this).val()=="On")
			{ 
				$('.facebook_pages_container').removeClass('hidden');
				$('#facebook_pages')[0].sumo.unSelectAll();
			}
			else
			{ 
				$('.facebook_pages_container').addClass('hidden');                 
			}            
		});
	});
	
	function client_package_delete(id, clients)
	{
		$('#client_response').html(clients);
		$('#package_client_detail').attr('onclick', 'client_package_notification('+id+')');
		$('#client_package_delete').modal("show");
	}
	
	function client_package_notification(id)
	{
		$.get("{{ route('package-client-detail') }}",{id: id},function(data){
			var innerhtm='<ul class="list-group">';
			
			for(var i=0;i<data.length;i++)
			{
				innerhtm +='<li class="list-group-item">'+data[i].name+' ('+data[i].email+')</li>';
			}
			
			innerhtm +='</ul>';
			
			$('#client_warning_notification').html(innerhtm);
		});
		$('#client_package_delete').modal("hide");
		$('#client_package_notification').modal("show");
	}
	
	function vendor_package_delete(id, vendors)
	{
		$('#vendor_response').html(vendors);
		$('#vendor_package_delete').modal("show");
		$('#package_vendor_detail').attr('onclick', 'vendor_package_notification('+id+')');
	}
	
	function vendor_package_notification(id)
	{
		$.get("{{ route('package-vendor-detail') }}",{id: id},function(data){
			var innerhtm='<ul class="list-group">';
			
			for(var i=0;i<data.length;i++)
			{
				innerhtm +='<li class="list-group-item">'+data[i].name+' ('+data[i].email+')</li>';
			}
			
			innerhtm +='</ul>';
			
			$('#vendor_warning_notification').html(innerhtm);
		});
		$('#vendor_package_delete').modal("hide");
		$('#vendor_package_notification').modal("show");
	}
	
	function client_group_delete(id, clients)
	{
		$('#group_response').html(clients);
		$('#client_group_delete').modal("show");
		$('#group_client_detail').attr('onclick', 'group_client_notification('+id+')');
	}
	
	function group_client_notification(id)
	{
		$.get("{{ route('group-client-detail') }}",{id: id},function(data){
			var innerhtm='<ul class="list-group">';
			
			for(var i=0;i<data.length;i++)
			{
				innerhtm +='<li class="list-group-item">'+data[i].name+' ('+data[i].email+')</li>';
			}
			
			innerhtm +='</ul>';
			
			$('#group_warning_notification').html(innerhtm);
		});
		$('#client_group_delete').modal("hide");
		$('#group_client_notification').modal("show");
	}
</script>

<script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/inner_pages_validation.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Group Client Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$("#zipcode,#client_zip").inputmask('99999');
		
		$('.sumo_lead_form > .okCancelInMulti > .MultiControls > .btnOk').click(function() {
			$('#date-loader').show();
			var facebook_ads_lead_id = $('#lead_form').val();
			var client_id = $('#client_list').val();
			if(facebook_ads_lead_id == null || client_id == null)
			{
				$('#date-loader').hide();
			}
			else
			{
				$.get("{{ route('check-client-lead-form') }}",{client_id: client_id.toString(), facebook_ads_lead_id: facebook_ads_lead_id.toString() }, function(data){
					if(data == 0)
					{
						$('#date-loader').hide();
					}
					else
					{
						var num = $('#lead_form > option').length;
						
						for(var i=0; i<num; i++)
						{
							$('#lead_form')[0].sumo.unSelectItem(i);
						}
						
						$('#date-loader').hide();
						swal('Error !', 'Some selected lead form assigned to another client which are not selected in group.', 'error');
					}
				}); 
			}
		});
		
		$('.sumo_client_list > .okCancelInMulti > .MultiControls > .btnOk').click(function() {
			$('#date-loader').show();
			var facebook_ads_lead_id = $('#lead_form').val();
			var client_id = $('#client_list').val();
			if(facebook_ads_lead_id == null || client_id == null)
			{
				$('#date-loader').hide();
			}
			else
			{
				$.get("{{ route('check-client-lead-form') }}",{client_id: client_id.toString(), facebook_ads_lead_id: facebook_ads_lead_id.toString()}, function(data){
					if(data == 0)
					{
						$('#date-loader').hide();
					}
					else
					{
						var num = $('#lead_form > option').length;
						
						for(var i=0; i<num; i++)
						{
							$('#lead_form')[0].sumo.unSelectItem(i);
						}
						
						$('#date-loader').hide();
						
						swal('Error !', 'Some selected lead form assigned to another client which are not selected in group.', 'error');
					}
				}); 
			}
		});
		
		@if($group->group_type == "Page Engagements")
		$('.div_page_engagements').removeClass('hidden');
		$('.div_lead_form').addClass('hidden');
		@elseif($group->group_type == "From Ad Forms")
		$('.div_lead_form').removeClass('hidden');
		$('.div_page_engagements').addClass('hidden');
		@else
		$('.div_lead_form').addClass('hidden');
		$('.div_page_engagements').addClass('hidden');
		@endif
		
		$('input[name="group_type"]').on('ifChanged',function(){ 
		if($(this).val() == "Page Engagements")
		{ 
			$('.div_page_engagements').removeClass('hidden');
			$('.div_lead_form').addClass('hidden');
			$('#lead_form')[0].sumo.unSelectAll();
			$('#page_engagements')[0].sumo.unSelectAll(); 
		}
		else if($(this).val() == "From Ad Forms")
		{ 
			$('.div_lead_form').removeClass('hidden');
			$('.div_page_engagements').addClass('hidden');
			$('#page_engagements')[0].sumo.unSelectAll();  
			$('#lead_form')[0].sumo.unSelectAll();
		}            
	});
	});
</script>
@stop																																										