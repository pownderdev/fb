@extends('layouts/default')

{{-- Page title --}}
@section('title')
Add User
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet" type="text/css"/>
<style>
	optgroup {background-color: #fb9f98; color: #000;}
	
	#NewDepartment {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#NewDepartment .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#NewJobTitle {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#NewJobTitle .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#NewTeam {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4);	*bottom: auto;}
	#NewTeam .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		
		<li class="active">
			Add User
		</li>
	</ol>
</section>


<!-- Main content -->
<section class="content p-l-r-15">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-lg-12">
			<form method="POST" action="{{ route('user_add') }}" role="form" id="add_user" enctype="multipart/form-data">
				<div class="panel panel-success filterable">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-fw fa-th-large"></i> Add User
						</h3>
					</div>
					<div class="panel-body form-body">
						<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
						<div class="row m-t-10">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="first_name">First Name</label>
									<input type="text" id="first_name" name="first_name" placeholder="First Name" class="form-control">
									<small class="text-danger animated first_name fadeInUp add_user"></small>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="last_name">Last Name</label>
									<input type="text" id="last_name" name="last_name" placeholder="Last Name" class="form-control">
									<small class="text-danger animated last_name fadeInUp add_user"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="department">Department</label>
									<div class="input-group">
										<select id="department" name="department" class="form-control">
											<option value="">Select Department</option>
											@foreach($Departments as $Department)
											<option value="{{ $Department->id }}" @if($Department->id == 8) selected @endif>{{ $Department->name }}</option>
											@endforeach
										</select>
										<span class="input-group-btn">
											<button class="btn btn-success NewDepartment" data-toggle="modal" data-target="#NewDepartment" type="button" title="Add New Department"><i class="fa fa-fw fa-plus" aria-hidden="true"></i></button>
										</span>
									</div>
									<small class="text-danger animated department fadeInUp add_user"></small>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="job_title">Job Title</label>
									<div class="input-group">
										<select id="job_title" name="job_title" class="form-control">
											<option value="">Select Job Title</option>
											@foreach($JobTitles as $JobTitle)
											<option value="{{ $JobTitle->id }}">{{ $JobTitle->name }}</option>
											@endforeach
										</select>
										<span class="input-group-btn">
											<button class="btn btn-success NewJobTitle" data-toggle="modal" data-target="#NewJobTitle" type="button" title="Add New Job Title"><i class="fa fa-fw fa-plus" aria-hidden="true"></i></button>
										</span>
									</div>
									<small class="text-danger animated job_title fadeInUp add_user"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="team">Team</label>
									<div class="input-group">
										<select id="team" name="team" class="form-control">
											<option value="">Select Team</option>
											@foreach($Teams as $Team)
											<option value="{{ $Team->id }}" @if($Team->id == 5) selected @endif>{{ $Team->name }}</option>
											@endforeach
										</select>
										<span class="input-group-btn">
											<button class="btn btn-success NewTeam" data-toggle="modal" data-target="#NewTeam" type="button" title="Add New Team"><i class="fa fa-fw fa-plus" aria-hidden="true"></i></button>
										</span>
									</div>
									<small class="text-danger animated team fadeInUp add_user"></small>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="email_address">Email Address</label>
									<input type="text" id="email_address" name="email_address" placeholder="Email" class="form-control">
									<small class="text-danger animated email_address fadeInUp add_user"></small>
								</div>
							</div>
						</div>
						<div class="row  m-t-10">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="username">Username</label>
									<input type="text" id="username" name="username" placeholder="Username" class="form-control">
									<small class="text-danger animated username fadeInUp add_user"></small>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="employee_number">Employee Number</label>
									<input type="text" id="employee_number" name="employee_number" placeholder="Employee Number" class="form-control">
									<small class="text-danger animated employee_number fadeInUp add_user"></small>
								</div>
							</div>
						</div>
						
						<div class="row m-t-10">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="cell_number">Cell Number</label>
									<input type="text" id="cell_number" name="cell_number" placeholder="Cell Number" class="form-control">
									<small class="text-danger animated cell_number fadeInUp add_user"></small>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="phone_number">Phone number</label>
									<input type="text" id="phone_number" name="phone_number" placeholder="Phone number" class="form-control">
									<small class="text-danger animated phone_number fadeInUp add_user"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="user_type">User Type</label>
									<select id="user_type" name="user_type" placeholder="User Type" class="form-control">
										<option value="admin">Select User Type</option>
										<option value="client" selected>Client</option>
										<option value="vendor">Vendor</option>
									</select>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label user_type_lable" for="client_vendor_id">Select Client/Vendor</label>
									<select id="admin_id" name="admin_id" class="form-control" style="display:none">
										<option value="0">Select Client/Vendor</option>
									</select>
									
									<select id="vendor_id" name="vendor_id" class="form-control" style="display:none">
										<option value="">Select Vendor</option>
										@foreach($Vendors as $Vendor)
										<option value="{{ $Vendor->vendor_id }}">{{ $Vendor->name }}</option>
										@endforeach
									</select>
									
									<select id="client_id" name="client_id" class="form-control">
										<option value="">Select Client</option>
										<optgroup label="Own Client"></optgroup>
										@foreach($Clients as $Client)
										<option value="{{ $Client->id }}">{{ $Client->name }}</option>
										@endforeach
										@foreach($Vendors as $Vendor)
										<optgroup label="{{ $Vendor->name }} Client"></optgroup>
										@foreach(VendorHelper::CampaignVendorClients($Vendor->id) as $Client)
										<option value="{{ $Client->id }}">{{ $Client->name }}</option>
										@endforeach
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row  m-t-10">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="picture">Select Picture</label><br>
									<input id="picture" type="file" name="picture" accept="image/*" style="display:inline"/>
									<img src="http://big.pownder.com/pownder/storage/app/uploads/default.png" id="profile-img-tag" width="180px" height="180px" style="display:inline" />
									<small class="text-danger animated picture fadeInUp add_user"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<h4 style="background-color:#c6efce;padding: 10px; margin: 10px 15px;">User Permission</h4>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<label><input type="checkbox" name="menu[]" id="DashboardMenu" value="Dashboard" class="access_menu"> Dashboard</label>
								<div id="DashboardMenuHide" style="padding-left: 25px;display:none">
									<label><input type="radio" name="dashboard_permission" id="DashboardRead" value="Dashboard Read"> Read</label><br />
									<label><input type="radio" name="dashboard_permission" id="DashboardReadWrite" value="Dashboard Read Write"> Read & Write</label>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<label><input type="checkbox" name="menu[]" id="CampaignsMenu" value="Campaigns" class="access_menu"> Campaigns</label>
								<div id="CampaignsMenuHide" style="padding-left: 25px;display:none">
									<label><input type="radio" name="campaigns_permission" id="CampaignsRead" value="Campaigns Read"> Read</label><br />
									<label><input type="radio" name="campaigns_permission" id="CampaignsReadWrite" value="Campaigns Read Write"> Read & Write</label>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<label><input type="checkbox" name="menu[]" id="LeadsMenu" value="Leads" class="access_menu"> Leads</label>
								<div id="LeadsMenuHide" style="padding-left: 25px;display:none">
									<label><input type="radio" name="lead_permission" id="AllLeads" value="All Leads"> All Leads</label><br />
									<label><input type="radio" name="lead_permission" id="AllottedLeads" value="Allotted Leads"> Allotted Leads</label>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<label><input type="checkbox" name="menu[]" id="CallTrackMenu" value="Call Track" class="access_menu"> Call Track</label>
								<div id="CallTrackMenuHide" style="padding-left: 25px;display:none">
									<label><input type="radio" name="calltrack_permission" id="CallTrackRead" value="Call Track Read"> Read</label><br />
									<label><input type="radio" name="calltrack_permission" id="CallTrackReadWrite" value="Call Track Read Write"> Read & Write</label>
								</div>
							</div>
						</div>
						
						<div class="row m-t-10">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<label><input type="checkbox" name="menu[]" id="FBMessengerMenu" value="FB Messenger" class="access_menu"> FB Messenger</label>
								<div id="FBMessengerMenuHide" style="padding-left: 25px;display:none">
									<label><input type="radio" name="fbmassenger_permission" id="FBMessengerRead" value="FB Messenger Read"> Read</label><br />
									<label><input type="radio" name="fbmassenger_permission" id="FBMessengerReadWrite" value="FB Messenger Read Write"> Read & Write</label>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<label><input type="checkbox" name="menu[]" id="ReportsMenu" value="Reports" class="access_menu"> Reports</label>
								<div id="ReportsMenuHide" style="padding-left: 25px;display:none">
									<label><input type="radio" name="reports_permission" id="ReportsRead" value="Reports Read"> Read</label><br />
									<label><input type="radio" name="reports_permission" id="ReportsReadWrite" value="Reports Read Write"> Read & Write</label>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<label><input type="checkbox" name="menu[]" id="ClientsMenu" value="Clients" class="access_menu"> Clients</label>
								<div class="ClientsMenuHide" style="padding-left: 25px;display:none">
									<label><input type="radio" name="clients_permission" id="ClientsRead" value="Clients Read"> Read</label><br />
									<label><input type="radio" name="clients_permission" id="ClientsReadWrite" value="Clients Read Write"> Read & Write</label>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<div class="ClientsMenuHide" style="padding-left: 25px;display:none">
									<br /><br />
									<label><input type="radio" name="client_view_permission" id="AllClients" value="All Clients"> All Clients</label><br />
									<label><input type="radio" name="client_view_permission" id="AllottedClients" value="Allotted Clients"> Allotted Clients</label>										
								</div>
							</div>
						</div>
						
						<div class="row m-t-10">
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<label><input type="checkbox" name="menu[]" id="VendorsMenu" value="Vendors" class="access_menu"> Vendors</label>
								<div id="VendorsMenuHide" style="padding-left: 25px;display:none">
									<label><input type="radio" name="vendors_permission" id="VendorsRead" value="Vendors Read"> Read</label><br />
									<label><input type="radio" name="vendors_permission" id="VendorsReadWrite" value="Vendors Read Write"> Read & Write</label>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<label><input type="checkbox" name="menu[]" id="InvoiceMenu" value="Invoice" class="access_menu"> Invoice</label>
								<div id="InvoiceMenuHide" style="padding-left: 25px;display:none">
									<label><input type="radio" name="invoice_permission" id="InvoiceRead" value="Invoice Read"> Read</label><br />
									<label><input type="radio" name="invoice_permission" id="InvoiceReadWrite" value="Invoice Read Write"> Read & Write</label>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<label><input type="checkbox" name="menu[]" id="ExpenseMenu" value="Expense" class="access_menu"> Expense</label>
								<div id="ExpenseMenuHide" style="padding-left: 25px;display:none">
									<label><input type="radio" name="expense_permission" id="ExpenseRead" value="Expense Read"> Read</label><br />
									<label><input type="radio" name="expense_permission" id="ExpenseReadWrite" value="Expense Read Write"> Read & Write</label>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
								<label><input type="checkbox" name="menu[]" id="SettingMenu" value="Setting" class="access_menu"> Setting</label>
								<div id="SettingMenuHide" style="padding-left: 25px;display:none">
									<label><input type="radio" name="setting_permission" id="SettingRead" value="Setting Read"> Read</label><br />
									<label><input type="radio" name="setting_permission" id="SettingReadWrite" value="Setting Read Write"> Read & Write</label>
								</div>
							</div>
						</div>
						
						<div class="row m-t-10">
							<center>
								<hr />
								<button type="button" class="btn btn-default addButton" style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c !important;"> Submit</button>
								<button type="submit" class="btn btn-default addSubmit" style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c !important; display:none"> Submit</button>
							</center>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	
	<div id="NewDepartment" class="modal fade animated" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header gred_2">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Add New Department</h4>
				</div>
				<div class="modal-body">
					<div class="row m-t-10">
						<div class="col-md-12">
							<div class="form-group">
								<label class="sr-only" for="department_name">Department Name</label>
								<input type="text" name="department_name" id="department_name" placeholder="Department Name" class="form-control m-t-10">
								<small class="text-danger animated department_name fadeInUp"></small>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c!important;" id="save_department">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="NewJobTitle" class="modal fade animated" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header gred_2">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Add New Job Title</h4>
				</div>
				<div class="modal-body">
					<div class="row m-t-10">
						<div class="col-md-12">
							<div class="form-group">
								<label class="sr-only" for="job_title_name">Job Title Name</label>
								<input type="text" name="job_title_name" id="job_title_name" placeholder="Job Title Name" class="form-control m-t-10">
								<small class="text-danger animated job_title_name fadeInUp"></small>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c!important;" id="save_job_title">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	<div id="NewTeam" class="modal fade animated" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header gred_2">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Add New Team</h4>
				</div>
				<div class="modal-body">
					<div class="row m-t-10">
						<div class="col-md-12">
							<div class="form-group">
								<label class="sr-only" for="team_name">Team Name</label>
								<input type="text" name="team_name" id="team_name" placeholder="Team Name" class="form-control m-t-10">
								<small class="text-danger animated team_name fadeInUp"></small>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c!important;" id="save_team">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script src="{{asset('assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>

<script>
	$(document).ready(function(){
		$(".form-body").find('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
		
		$("#cell_number").inputmask('(999) 999-9999');
		$("#phone_number").inputmask('(999) 999-9999');
		
		$("#picture").change(function(){
			readURL(this);
		});
		
		$(".NewDepartment").click(function(){
			$('#department_name').val('');
			$('.department_name').html('');
		});
		
		$(".NewJobTitle").click(function(){
			$('#job_title_name').val('');
			$('.job_title_name').html('');
		});
		
		$(".NewTeam").click(function(){
			$('#team_name').val('');
			$('.team_name').html('');
		});
		
		//Dashboard Menu
		$('#DashboardMenu').on('ifChecked', function (event){
			$('#DashboardMenuHide').css('display', 'block');
			$('#DashboardRead').iCheck('check');
		});
		$('#DashboardMenu').on('ifUnchecked', function (event) {
			$('#DashboardMenuHide').css('display', 'none');
			$('#DashboardRead').iCheck('uncheck');
			$('#DashboardReadWrite').iCheck('uncheck');
		});
		
		//Campaigns Menu
		$('#CampaignsMenu').on('ifChecked', function (event){
			$('#CampaignsMenuHide').css('display', 'block');
			$('#CampaignsRead').iCheck('check');
		});
		$('#CampaignsMenu').on('ifUnchecked', function (event) {
			$('#CampaignsMenuHide').css('display', 'none');
			$('#CampaignsRead').iCheck('uncheck');
			$('#CampaignsReadWrite').iCheck('uncheck');
		});
		
		//Leads Menu
		$('#LeadsMenu').on('ifChecked', function (event){
			$('#LeadsMenuHide').css('display', 'block');
			$('#AllLeads').iCheck('check');
		});
		$('#LeadsMenu').on('ifUnchecked', function (event) {
			$('#LeadsMenuHide').css('display', 'none');
			$('#AllLeads').iCheck('uncheck');
			$('#AllottedLeads').iCheck('uncheck');
		});
		
		//Call Track Menu
		$('#CallTrackMenu').on('ifChecked', function (event){
			$('#CallTrackMenuHide').css('display', 'block');
			$('#CallTrackRead').iCheck('check');
		});
		$('#CallTrackMenu').on('ifUnchecked', function (event) {
			$('#CallTrackMenuHide').css('display', 'none');
			$('#CallTrackRead').iCheck('uncheck');
			$('#CallTrackReadWrite').iCheck('uncheck');
		});
		
		//FB Messenger Menu
		$('#FBMessengerMenu').on('ifChecked', function (event){
			$('#FBMessengerMenuHide').css('display', 'block');
			$('#FBMessengerRead').iCheck('check');
		});
		$('#FBMessengerMenu').on('ifUnchecked', function (event) {
			$('#FBMessengerMenuHide').css('display', 'none');
			$('#FBMessengerRead').iCheck('uncheck');
			$('#FBMessengerReadWrite').iCheck('uncheck');
		});
		
		//Reports Menu
		$('#ReportsMenu').on('ifChecked', function (event){
			$('#ReportsMenuHide').css('display', 'block');
			$('#ReportsRead').iCheck('check');
		});
		$('#ReportsMenu').on('ifUnchecked', function (event) {
			$('#ReportsMenuHide').css('display', 'none');
			$('#ReportsRead').iCheck('uncheck');
			$('#ReportsReadWrite').iCheck('uncheck');
		});
		
		//Clients Menu
		$('#ClientsMenu').on('ifChecked', function (event){
			$('.ClientsMenuHide').css('display', 'block');
			$('#ClientsRead').iCheck('check');
			$('#AllClients').iCheck('check');
		});
		$('#ClientsMenu').on('ifUnchecked', function (event) {
			$('.ClientsMenuHide').css('display', 'none');
			$('#ClientsRead').iCheck('uncheck');
			$('#ClientsReadWrite').iCheck('uncheck');
			$('#AllClients').iCheck('uncheck');
			$('#AllottedClients').iCheck('uncheck');
		});
		
		//Vendors Menu
		$('#VendorsMenu').on('ifChecked', function (event){
			$('#VendorsMenuHide').css('display', 'block');
			$('#VendorsRead').iCheck('check');
		});
		$('#VendorsMenu').on('ifUnchecked', function (event) {
			$('#VendorsMenuHide').css('display', 'none');
			$('#VendorsRead').iCheck('uncheck');
			$('#VendorsReadWrite').iCheck('uncheck');
		});	
		
		//Invoice Menu
		$('#InvoiceMenu').on('ifChecked', function (event){
			$('#InvoiceMenuHide').css('display', 'block');
			$('#InvoiceRead').iCheck('check');
		});
		$('#InvoiceMenu').on('ifUnchecked', function (event) {
			$('#InvoiceMenuHide').css('display', 'none');
			$('#InvoiceRead').iCheck('uncheck');
			$('#InvoiceReadWrite').iCheck('uncheck');
		});
		
		//Expense Menu
		$('#ExpenseMenu').on('ifChecked', function (event){
			$('#ExpenseMenuHide').css('display', 'block');
			$('#ExpenseRead').iCheck('check');
		});
		$('#ExpenseMenu').on('ifUnchecked', function (event) {
			$('#ExpenseMenuHide').css('display', 'none');
			$('#ExpenseRead').iCheck('uncheck');
			$('#ExpenseReadWrite').iCheck('uncheck');
		});
		
		//Setting Menu
		$('#SettingMenu').on('ifChecked', function (event){
			$('#SettingMenuHide').css('display', 'block');
			$('#SettingRead').iCheck('check');
		});
		$('#SettingMenu').on('ifUnchecked', function (event) {
			$('#SettingMenuHide').css('display', 'none');
			$('#SettingRead').iCheck('uncheck');
			$('#SettingReadWrite').iCheck('uncheck');
		});
		
		$(".addButton").click(function( event ) {
			event.preventDefault();
			
			$(".addButton").prop('disabled', true);
			
			$('.add_user').html('');
			var data=$('#add_user').serialize();
			
			$.ajax({
				url: "{{ route('user_add_validation') }}",
				type:'POST',
				data: data,
				success: function(data) {
					//swal('Success!', 'Manager added successfully.', 'success');
					//window.location.reload(true);
					$(".addSubmit").trigger('click');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$("."+key).html(value);
					});
					$(".addButton").prop('disabled', false);
				}       
			});
		});
		
		$("#save_department").click(function(){
			$("#save_department").prop('disabled', true);
			$('.department_name').html('');
			if($('#department_name').val()=='')
			{
				$("#save_department").prop('disabled', false);
				$('.department_name').html('The department name field is required');
				return false;
			}
			var data = 'department_name='+$('#department_name').val();
			$.ajax({
				url: "{{ route('create_department') }}",
				type:'GET',
				data: data,
				success: function(data) {
					var innerhtm='<option value="">Select Department</option>';
					
					for(var i=0;i<data.length;i++)
					{
						innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
					}
					
					$('#department').html(innerhtm);
					$("#save_department").prop('disabled', false);
					$(".NewDepartment").click();						
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$("."+key).html(value);
					});
					$("#save_department").prop('disabled', false);
				}       
			});
		});
		
		$("#save_job_title").click(function(){
			$("#save_job_title").prop('disabled', true);
			$('.job_title_name').html('');
			if($('#job_title_name').val()=='')
			{
				$("#save_job_title").prop('disabled', false);
				$('.job_title_name').html('The job title name field is required');
				return false;
			}
			var data = 'job_title_name='+$('#job_title_name').val();
			$.ajax({
				url: "{{ route('create_job_title') }}",
				type:'GET',
				data: data,
				success: function(data) {
					var innerhtm='<option value="">Select Job Title</option>';
					
					for(var i=0;i<data.length;i++)
					{
						innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
					}
					
					$('#job_title').html(innerhtm);
					$("#save_job_title").prop('disabled', false);
					$(".NewJobTitle").click();						
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$("."+key).html(value);
					});
					$("#save_job_title").prop('disabled', false);
				}       
			});
		});
		
		$("#save_team").click(function(){
			$("#save_team").prop('disabled', true);
			$('.team_name').html('');
			if($('#team_name').val()=='')
			{
				$("#save_team").prop('disabled', false);
				$('.team_name').html('The team name field is required');
				return false;
			}
			var data = 'team_name='+$('#team_name').val();
			$.ajax({
				url: "{{ route('create_team') }}",
				type:'GET',
				data: data,
				success: function(data) {
					var innerhtm='<option value="">Select Team</option>';
					
					for(var i=0;i<data.length;i++)
					{
						innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
					}
					
					$('#team').html(innerhtm);
					$("#save_team").prop('disabled', false);
					$(".NewTeam").click();						
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$("."+key).html(value);
					});
					$("#save_team").prop('disabled', false);
				}       
			});
		});
	});
	
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
			}
            reader.readAsDataURL(input.files[0]);
		}
	}
</script>
<script>
	$(document).ready(function(){
		$("#job_title").change(function(){
			var job_title_id = $(this).val();
			$('.access_menu').iCheck('uncheck');
			if(job_title_id!='')
			{				
				$.get("{{ route('job_title_menu') }}",{ job_title_id: job_title_id }, function(data){
					for(var i=0;i<data.length;i++)
					{
						$("input[value='"+data[i].menu+"']").iCheck('check');
					}
				});
				
				$.get("{{ route('job_title_permission') }}",{ job_title_id: job_title_id }, function(data){
					for(var i=0;i<data.length;i++)
					{
						$("input[value='"+data[i].permission+"']").iCheck('check');
					}
				});
			}
		});
		
		$("#job_title").change(function(){
			var job_title_id = $(this).val();
			$('.access_menu').iCheck('uncheck');
			if(job_title_id!='')
			{				
				$.get("{{ route('job_title_menu') }}",{ job_title_id: job_title_id }, function(data){
					for(var i=0;i<data.length;i++)
					{
						$("input[value='"+data[i].menu+"']").iCheck('check');
					}
				});
				
				$.get("{{ route('job_title_permission') }}",{ job_title_id: job_title_id }, function(data){
					for(var i=0;i<data.length;i++)
					{
						$("input[value='"+data[i].permission+"']").iCheck('check');
					}
					
					var user_type = $('#user_type').val();
					if(user_type == 'admin')
					{	
						$('.user_type_lable').html('Select Client/Vendor');
						$('#admin_id').css('display', 'block');
						$('#client_id').css('display', 'none');
						$('#vendor_id').css('display', 'none');
						$('#DashboardMenu, #CampaignsMenu, #LeadsMenu, #CallTrackMenu, #FBMessengerMenu, #ReportsMenu, #ClientsMenu, #VendorsMenu, #InvoiceMenu, #ExpenseMenu, #SettingMenu').iCheck('enable');
					}
					else if(user_type == 'vendor')
					{	
						$('.user_type_lable').html('Select Vendor');
						$('#admin_id').css('display', 'none');
						$('#client_id').css('display', 'none');
						$('#vendor_id').css('display', 'block');
						$('#DashboardMenu, #CampaignsMenu, #LeadsMenu, #CallTrackMenu, #FBMessengerMenu, #ReportsMenu, #ClientsMenu, #InvoiceMenu, #ExpenseMenu, #SettingMenu').iCheck('enable');				
						$('#VendorsMenu').iCheck('uncheck');
						$('#VendorsMenu').iCheck('disable');
					}
					else if(user_type == 'client')
					{				
						$('.user_type_lable').html('Select Client');
						$('#admin_id').css('display', 'none');
						$('#client_id').css('display', 'block');
						$('#vendor_id').css('display', 'none');
						$('#DashboardMenu, #LeadsMenu, #CallTrackMenu, #FBMessengerMenu, #ReportsMenu').iCheck('enable');
						$('#CampaignsMenu, #ClientsMenu, #VendorsMenu, #InvoiceMenu, #ExpenseMenu, #SettingMenu').iCheck('uncheck');
						$('#CampaignsMenu, #ClientsMenu, #VendorsMenu, #InvoiceMenu, #ExpenseMenu, #SettingMenu').iCheck('disable');
					}
				});
			}
		});
		
		$("#user_type").change(function(){
			var user_type = $(this).val();
			if(user_type == 'admin')
			{	
				$('.user_type_lable').html('Select Client/Vendor');
				$('#admin_id').css('display', 'block');
				$('#client_id').css('display', 'none');
				$('#vendor_id').css('display', 'none');
				$('#DashboardMenu, #CampaignsMenu, #LeadsMenu, #CallTrackMenu, #FBMessengerMenu, #ReportsMenu, #ClientsMenu, #VendorsMenu, #InvoiceMenu, #ExpenseMenu, #SettingMenu').iCheck('enable');
			}
			else if(user_type == 'vendor')
			{	
				$('.user_type_lable').html('Select Vendor');
				$('#admin_id').css('display', 'none');
				$('#client_id').css('display', 'none');
				$('#vendor_id').css('display', 'block');
				
				$('#DashboardMenu, #CampaignsMenu, #LeadsMenu, #CallTrackMenu, #FBMessengerMenu, #ReportsMenu, #ClientsMenu, #InvoiceMenu, #ExpenseMenu, #SettingMenu').iCheck('enable');				
				$('#VendorsMenu').iCheck('uncheck');
				$('#VendorsMenu').iCheck('disable');
			}
			else if(user_type == 'client')
			{				
				$('.user_type_lable').html('Select Client');
				$('#admin_id').css('display', 'none');
				$('#client_id').css('display', 'block');
				$('#vendor_id').css('display', 'none');
				
				$('#DashboardMenu, #LeadsMenu, #CallTrackMenu, #FBMessengerMenu, #ReportsMenu').iCheck('enable');
				$('#CampaignsMenu, #ClientsMenu, #VendorsMenu, #InvoiceMenu, #SettingMenu').iCheck('uncheck');
				$('#CampaignsMenu, #ClientsMenu, #VendorsMenu, #InvoiceMenu, #SettingMenu').iCheck('disable');
			}
		});
	});
</script>
@stop