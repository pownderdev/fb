@extends('layouts/default')

{{-- Page title --}}
@section('title')
Expense List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datedropper/datedropper.css')}}">
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	
	.export.btn-group {*display:none!important;}
	
	.fixed-table-toolbar {*width:60%;}
	.fixed-table-toolbar .search {width: Calc( 100% - 152px );}
	
	@media screen and (max-width: 767px) {
	.fixed-table-toolbar {width: 100%;}
	.fixed-table-toolbar {width:100%;}
	.fixed-table-toolbar .search {width: Calc( 100% );}
	}
	
	optgroup {background-color: #fb9f98; color: #000;}
	
	#AddExpense {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#AddExpense .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#EditExpense {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#EditExpense .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#NewCategory {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#NewCategory .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	.fixed-table-container {height: 500px !important; }
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active"> Expense List</li>
	</ol>
</section>

<!-- Main content -->
<section class="content p-l-r-15">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Expense List
					</h3>
				</div>
				<div class="panel-body">
					<table id="FTPAccountList" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20, 40, ALL]" data-show-footer="false" data-click-to-select="true">
						<thead>
							<tr>
								<th data-field="Added Date" data-sortable="true" @if(in_array("0",$TableColumn)) data-visible="false" @endif>Added Date</th>
								<th data-field="Expense Date" data-sortable="true" @if(in_array("1",$TableColumn)) data-visible="false" @endif>Expense Date</th>
								<th data-field="Total Amount" data-sortable="true" @if(in_array("2",$TableColumn)) data-visible="false" @endif>Total Amount</th>
								<th data-field="Category" data-sortable="true" @if(in_array("3",$TableColumn)) data-visible="false" @endif>Category</th>
								<th data-field="Description" data-sortable="true" @if(in_array("4",$TableColumn)) data-visible="false" @endif>Description</th>
								<th data-field="Company Name" data-sortable="true" @if(in_array("5",$TableColumn)) data-visible="false" @endif>Company Name</th>
								<th data-field="Client" data-sortable="true" @if(in_array("6",$TableColumn)) data-visible="false" @endif>Client</th>
								@if($WritePermission=='Yes')
								<th data-field="Actions" data-sortable="true" @if(in_array("7",$TableColumn)) data-visible="false" @endif>Actions</th>
								@endif
							</tr>
						</thead>
						<tbody>
							@php $i=1; @endphp
							@foreach($expenses as $expense)
							<tr>
								<td>{{ date('m-d-Y h:i A',strtotime($expense->created_at)) }}</td>
								<td>{{ date('m-d-Y',strtotime($expense->expense_date)) }}</td>
								<td>{{ number_format($expense->total_amount,2) }}</td>
								<td>{{ $expense->expense_category_name }}</td>
								<td>{{ $expense->description }}</td>
								<td>{{ $expense->company_name }}</td>
								<td>{{ $expense->client_name }}</td>
								@if($WritePermission=='Yes')
								<td>
									<i class="fa fa-fw fa-pencil-square-o actions_icon"  onClick="EditExpense('{{ $expense->id }}')" title="Edit"></i> 
									
									<i class="fa fa-fw fa-times text-danger actions_icon" onClick="DeleteExpense('{{ $expense->id }}')" title="Delete Expense"></i>
								</td>
								@endif
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->

<div id="NewCategory" class="modal fade animated" role="dialog" style="z-index: 999999;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gred_2">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Add New Category</h4>
			</div>
			<div class="modal-body">
				<div class="row m-t-10">
					<div class="col-md-12">
						<div class="form-group">
							<label class="sr-only" for="category_name">Category Name</label>
							<input type="text" name="category_name" id="category_name" placeholder="Category Name" class="form-control m-t-10">
							<small class="text-danger animated category_name fadeInUp"></small>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" style=" color:#fff; background-color: #ffbe18 !important; border-color: #e4a70c!important;" id="save_category">Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="AddExpense" class="modal fade animated" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gred_2">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Add Expense</h4>
			</div>
			<form method="POST" action="{{ route('add-expense') }}" role="form" id="validate_add_expense">
				<div class="modal-body form-body">
					<div class="row m-t-10">
						<div class="col-md-12">
							<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
							<div class="row m-t-10">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label class="control-label" for="company_name">Company Name</label>
										<input type="text" id="company_name" name="company_name" placeholder="Company Name" class="form-control">
										<small class="text-danger animated company_name fadeInUp validate_add_expense"></small>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label class="control-label" for="date">Date</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
											<input type="text" id="date" name="date" placeholder="MM-DD-YYYY" class="form-control" readonly style="background-color:#FFF">
										</div>
										<small class="text-danger animated date fadeInUp validate_add_expense"></small>
									</div>
								</div>
							</div>
							<div class="row m-t-10">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label class="control-label" for="total_amount">Total Amount</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span>
											<input type="text" id="total_amount" name="total_amount" placeholder="Total Amount" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
										</div>
										<small class="text-danger animated total_amount fadeInUp validate_add_expense"></small>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label class="control-label" for="category">Category</label>
										<div class="input-group">
											<select id="category" name="category" class="form-control">
												<option value="">Select Category</option>
												@foreach($expense_categories as $expense_category)
												<option value="{{ $expense_category->id }}">{{ $expense_category->name }}</option>
												@endforeach
											</select>
											<span class="input-group-btn">
												<button class="btn btn-success NewCategory" data-toggle="modal" data-target="#NewCategory" type="button" title="Add New Category"><i class="fa fa-fw fa-plus" aria-hidden="true"></i></button>
											</span>
										</div>
										<small class="text-danger animated category fadeInUp validate_add_expense"></small>
									</div>
								</div>
							</div>
							<div class="row m-t-10">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label class="control-label" for="client">Client</label>
										<select id="client" name="client" class="form-control">
											<option value="">Select Client</option>
											<optgroup label="Own Client"></optgroup>
											@foreach($Clients as $Client)
											<option value="{{ $Client->id }}">{{ $Client->name }}</option>
											@endforeach
											@foreach($Vendors as $Vendor)
											<optgroup label="{{ $Vendor->name }} Client"></optgroup>
											@foreach(VendorHelper::CampaignVendorClients($Vendor->id) as $Client)
											<option value="{{ $Client->id }}">{{ $Client->name }}</option>
											@endforeach
											@endforeach
										</select>
										<small class="text-danger animated client fadeInUp validate_add_expense"></small>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label class="control-label" for="description">Description</label>
										<textarea id="description" name="description" placeholder="Description" class="form-control"></textarea>
										<small class="text-danger animated description fadeInUp validate_add_expense"></small>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default addButton" style=" color:#fff; background-color: #ffbe18 !important; border-color: #e4a70c !important;"> Submit</button>
					<button type="submit" class="addSubmit" style="display:none"> Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="EditExpense" class="modal fade animated" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gred_2">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title">Edit Expense</h4>
			</div>
			<form method="POST" action="{{ route('edit-expense') }}" role="form" id="validate_edit_expense">
				<div class="modal-body form-body">
					<div class="row m-t-10">
						<div class="col-md-12">
							<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
							<input type="hidden" name="id" id="id"/>
							<div class="row m-t-10">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label class="control-label" for="edit_company_name">Company Name</label>
										<input type="text" id="edit_company_name" name="company_name" placeholder="Company Name" class="form-control">
										<small class="text-danger animated edit_company_name fadeInUp validate_edit_expense"></small>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label class="control-label" for="edit_date">Date</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
											<input type="text" id="edit_date" name="date" placeholder="MM-DD-YYYY" class="form-control" readonly style="background-color:#FFF">
										</div>
										<small class="text-danger animated edit_date fadeInUp validate_edit_expense"></small>
									</div>
								</div>
							</div>
							<div class="row m-t-10">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label class="control-label" for="edit_total_amount">Total Amount</label>
										<div class="input-group">
											<span class="input-group-addon"><i class="fa fa-usd" aria-hidden="true"></i></span>
											<input type="text" id="edit_total_amount" name="total_amount" placeholder="Total Amount" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
										</div>
										<small class="text-danger animated edit_total_amount fadeInUp validate_edit_expense"></small>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label class="control-label" for="edit_category">Category</label>
										<div class="input-group">
											<select id="edit_category" name="category" class="form-control">
												<option value="">Select Category</option>
												@foreach($expense_categories as $expense_category)
												<option value="{{ $expense_category->id }}">{{ $expense_category->name }}</option>
												@endforeach
											</select>
											<span class="input-group-btn">
												<button class="btn btn-success NewCategory" data-toggle="modal" data-target="#NewCategory" type="button" title="Add New Category"><i class="fa fa-fw fa-plus" aria-hidden="true"></i></button>
											</span>
										</div>
										<small class="text-danger animated edit_category fadeInUp validate_edit_expense"></small>
									</div>
								</div>
							</div>
							<div class="row m-t-10">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label class="control-label" for="edit_client">Client</label>
										<select id="edit_client" name="client" class="form-control">
											<option value="">Select Client</option>
											<optgroup label="Own Client"></optgroup>
											@foreach($Clients as $Client)
											<option value="{{ $Client->id }}">{{ $Client->name }}</option>
											@endforeach
											@foreach($Vendors as $Vendor)
											<optgroup label="{{ $Vendor->name }} Client"></optgroup>
											@foreach(VendorHelper::CampaignVendorClients($Vendor->id) as $Client)
											<option value="{{ $Client->id }}">{{ $Client->name }}</option>
											@endforeach
											@endforeach
										</select>
										<small class="text-danger animated edit_client fadeInUp validate_edit_expense"></small>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label class="control-label" for="edit_description">Description</label>
										<textarea id="edit_description" name="description" placeholder="Description" class="form-control"></textarea>
										<small class="text-danger animated edit_description fadeInUp validate_edit_expense"></small>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default editButton" style=" color:#fff; background-color: #ffbe18 !important; border-color: #e4a70c !important;"> Submit</button>
					<button type="submit" class="editSubmit" style="display:none"> Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}"></script>
<script>
	$(document).ready(function(){
		$('#FTPAccountList').bootstrapTable();
		
		@if($WritePermission=='Yes')
		var extra_features = '<a href="#" onClick="AddExpense()" class="btn btn-default" title="New Expense" ><i class="fa fa-fw fa-plus-square"></i></a>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
		@endif
		
		$('.dropdown-menu input[type=checkbox]').change(function(){
			var column_value = $(this).val();
			var table_name = 'Expense List Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		var options1 = {format: "m-d-Y", dropPrimaryColor: "#428bca"};
		$('#date').dateDropper($.extend({}, options1));
		$('#edit_date').dateDropper($.extend({}, options1));
		
		$(".addButton").click(function( event ) {
			event.preventDefault();
			
			$(".addButton").prop('disabled', true);
			
			$('.validate_add_expense').html('');
			var data=$('#validate_add_expense').serialize();
			
			$.ajax({
				url: "{{ route('validate-add-expense') }}",
				type:'POST',
				data: data,
				success: function(data) {
					$(".addSubmit").trigger('click');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$("."+key).html(value);
					});
					$(".addButton").prop('disabled', false);
				}       
			});
		});
		
		$(".editButton").click(function( event ) {
			event.preventDefault();
			
			$(".editButton").prop('disabled', true);
			
			$('.validate_edit_expense').html('');
			var data=$('#validate_edit_expense').serialize();
			
			$.ajax({
				url: "{{ route('validate-add-expense') }}",
				type:'POST',
				data: data,
				success: function(data) {
					$(".editSubmit").trigger('click');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$(".edit_"+key).html(value);
					});
					$(".editButton").prop('disabled', false);
				}       
			});
		});
		
		$("#save_category").click(function(){
			$("#save_category").prop('disabled', true);
			$('.category_name').html('');
			if($('#category_name').val()=='')
			{
				$("#save_category").prop('disabled', false);
				$('.category_name').html('The category name field is required');
				return false;
			}
			var data = 'category_name='+$('#category_name').val();
			$.ajax({
				url: "{{ route('create_expense_category') }}",
				type:'GET',
				data: data,
				success: function(data) {
					var innerhtm='<option value="">Select Category</option>';
					
					for(var i=0;i<data.length;i++)
					{
						innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
					}
					
					$('#category').html(innerhtm);
					$('#edit_category').html(innerhtm);
					$("#save_category").prop('disabled', false);
					$('#category_name').val('')
					$(".NewCategory").click();						
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$("."+key).html(value);
					});
					$("#save_category").prop('disabled', false);
				}       
			});
		});
	});
	
	function DeleteExpense(id)
	{
		swal({
			title: 'Are you sure?',
			text: "You want to delete this expense!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#22D69D',
			cancelButtonColor: '#FB8678',
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn',
			cancelButtonClass: 'btn'
			}).then(function () {
			$.get("{{ route('delete-expense') }}",{'id': id}).then(function(){
				swal('Deleted!', 'Your expense has been deleted.', 'success');
				window.location.reload(true); 
			});
			}, function (dismiss) {
			if (dismiss === 'cancel') {
				swal('Cancelled', 'Your expense is safe.', 'error');
			}
		});
	}
	
	function EditExpense(id)
	{
		$('.validate_edit_expense').html('');
		$.get("{{ route('edit-expense') }}",{id: id},function(data){ 
			$('#id').val(id);
			$('#edit_company_name').val(data.company_name);
			$('#edit_date').val(data.expense_date);
			$('#edit_category').val(data.expense_category_id);
			$('#edit_total_amount').val(data.total_amount);
			$('#edit_client').val(data.client_id);
			$('#edit_description').val(data.description);
		});
		$(".editButton").prop('disabled', false);
		$('#EditExpense').modal("show");
	}
	
	function AddExpense()
	{
		$('.validate_add_expense').html('');
		$('#company_name').val('Pownder™');
		$('#date').val("{{ date('m-d-Y') }}");
		$('#category').val('');
		$('#total_amount').val('');
		$('#client').val('');
		$('#description').val('');
		$(".addButton").prop('disabled', false);
		$('#AddExpense').modal("show");
	}
</script>
@stop