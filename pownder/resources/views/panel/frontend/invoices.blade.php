@extends('layouts/default')
 
{{-- Page title --}} 
@section('title') 
Invoice @parent 
@stop 

{{-- page level styles --}} 
@section('header_styles')
<!--start page level css -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<!--<link rel="stylesheet" href="{{asset('assets/vendors/iCheck/css/all.css')}}"/>-->
<link rel="stylesheet" href="{{asset('assets/css/custom_css/confirm_invoice.css')}}" />
<!--end page level css-->
@stop 
{{-- Page content --}}
@section('content')
<?php 
function showErrorMsg($error)
{
    if(count($error)>0)
    {
        $msg='';
        foreach($error as $val)
        {
         $msg.='<small class="help-block animated fadeInUp text-danger" style="color: #FB8678;">'.$val.'</small>';    
        }
        return $msg;        
    }
}
//Money Format
 function money_format1($number)
{
  setlocale(LC_MONETARY, 'en_US'); 
  return money_format('%!.2i',$number); 
}
?>
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
			<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">
		  Create Invoice
		</li>
	</ol>
</section>
<?php 
 $msg1='';
                $continue_old_invoice=((Session::has('draft_continue')) ? 1 : (Session::has('invoice_edit')) ?1 :0);
                $clients_list=($continue_old_invoice and Session::has('clients_list')) ?session('clients_list') :[];
                $invoice_data=((Session::has('draft_continue')) ?session('draft_data') :((Session::has('invoice_edit')) ?session('invoice_data') :'')); 
                $invoice_items=((Session::has('draft_continue')) ?session('draft_items') :((Session::has('invoice_edit')) ?session('invoice_items') :''));               
    
?>
<!-- Main content -->
<form method="post" enctype="multipart/form-data" class="invoice_form">
<section class="content p-l-r-15">
    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}"  />
    <input type="hidden" name="trigger_client" id="trigger_client" value="@if(!Session::has('draft_continue') && !Session::has('invoice_edit')){{'1'}}@else{{'0'}}@endif" />
    <input type="hidden" name="invoice_edit" id="invoice_edit" value="{{Session::has('invoice_edit')}}" />
    <input type="hidden" name="invoice_edit_invoice_date" id="invoice_edit_invoice_date" value="@if(Session::has('invoice_edit')){{$invoice_data->invoice_date}}@endif" />
    <input type="hidden" name="login_type" id="login_type" value="{{session('user')->category}}" />
    <input type="hidden" name="mark_paid" id="mark_paid" value="off"  />
    @include('panel.includes.status')   
    
	<div class="row">
		<div class="panel">
          <div class="panel-body">
          <?php echo showErrorMsg($errors->all());$manage_invoice_position='';       ?>                    
          <div class="row">
             <div class="col-md-1">
                <label class="m-t-8" for="client_type">Bill To:</label>
             </div>
             @if(session('user')->category=="admin")
             <div class="col-md-3">
               <select class="form-control client_type" name="client_type" id="client_type">
                <option value=""  disabled="">Select Recipient Type</option>                
                <option value="vendor" @if($continue_old_invoice and $invoice_data->client_type=="vendor"){{ 'selected=""' }}@endif>Vendor</option>                
                <option value="client" @if($continue_old_invoice and $invoice_data->client_type=="client"){{ 'selected=""' }}@elseif(!$continue_old_invoice){{ 'selected=""' }}@endif>Client</option>
              </select>
             </div>
             <?php $manage_invoice_position='col-md-5'; ?>
             @elseif(session('user')->category=="vendor")
             <input type="hidden" class="client_type" name="client_type" id="client_type" value="client" />
             <?php $manage_invoice_position='col-md-8'; ?>
             @endif
             @if(Session::has('invoice_edit'))
             <input type="hidden" class="client_type" name="client_type" id="client_type" value="{{$invoice_data->client_type}}" />
             @endif
             <div class="col-md-3">
               <select class="form-control client" name="client" id="client">                
                <?php               
                 
                if(count($clients_list)>0)
                {
                $own='';$own1='';$vendor='';$vendor_name='';
                
                foreach($clients_list as $client) 
                {
                 if($client->login_id==$client->created_user_id && $client->vendor_id==0 && $client->type=="double")
                 {
                      $own.='<option ';if($invoice_data->client_id == $client->id){$own.='selected';} $own.=' value="'.$client->id.'">'.$client->name."</option>";
                 }
                 else   if($client->login_id==$client->created_user_id && $client->type=="single")
                 {
                      $own1.='<option ';if($invoice_data->client_id == $client->id){$own1.='selected';} $own1.=' value="'.$client->id.'">'.$client->name."</option>";
                 }
                 else   if(session('user')->category=="vendor")
                 {
                      $own1.='<option ';if($invoice_data->client_id == $client->id){$own1.='selected';} $own1.=' value="'.$client->id.'">'.$client->name."</option>";
                 }
                 else
                 {
                    if($vendor_name!=$client->created_user_name && $vendor_name=="")
                    {
                      $vendor_name=$client->created_user_name;
                      $vendor.='<optgroup label="'.$client->created_user_name.'">';
                      $vendor.='<option ';if($invoice_data->client_id == $client->id){$vendor.='selected';} $vendor.='  value="'.$client->id.'">'.$client->name."</option>";    
                    }
                    else if($vendor_name==$client->created_user_name && $vendor_name!="")
                    {                
                      $vendor.='<option ';if($invoice_data->client_id == $client->id){$vendor.='selected';} $vendor.='  value="'.$client->id.'">'.$client->name."</option>";               
                    }
                    else if($vendor_name!=$client->created_user_name && $vendor_name!="")
                    {
                      $vendor_name=$client->created_user_name;   
                      $vendor.='</optgroup><optgroup label="'.$client->created_user_name.'">';    
                      $vendor.='<option ';if($invoice_data->client_id == $client->id){$vendor.='selected';} $vendor.='  value="'.$client->id.'">'.$client->name."</option>";  
                    }
                 }     
                }
                if(session('user')->category=="vendor")
                {    
                $msg='<option value="" selected="">Select Client</option>';
                }
                else
                {    
                $msg='<option value="" selected="">Select Client/Vendor</option>';
                }
                if($clients_list[0]->type=="single")
                {                 
                    $msg.=$own1;                    
                }
                else
                {
                 $msg.='<optgroup label="Own Clients">';
                 $msg.=$own;
                 $msg.='</optgroup>';
                 //msg.='<optgroup label="Vendor\'s Clients">';
                 $msg.=$vendor;
                 //msg.='</optgroup>';
                }
                $msg1=$msg;
                }
                else
                {
                    $msg1='<option value="" selected="" disabled="">Select Client</option>';
                }
                echo $msg1;    
                ?>
                            
              </select>              
             </div>
             @if(Session::has('invoice_edit'))
             <input type="hidden" class="client" name="client" id="client" value="{{$invoice_data->client_id}}" />
             @endif
             <div class="{{$manage_invoice_position}} text-right">
                <a href="{{route('invoice-report')}}">                
                <button type="button" class="btn btn-warning  pownder_yellow">Invoice Report</button>
                </a>
                <a href="{{route('manage-invoice')}}">                
                <button type="button" class="btn btn-success  pownder_green">Manage Invoice</button>
                </a>
             </div>
           </div>          
          </div>
         </div>  
      </div>
      <div class="row">
      <div class="col-md-4 no-padding">
	  <section class="invoice-option">
			<div class="option-wrapper">
				
				<div class="panel-heading  gred_2" style="padding: 8px 15px;">
					<h3 class="panel-title">
						Invoice Details
					</h3>
				</div>
                <div class="form-group">
					<label class="control-label" for="client_email" >
						Client Email:
					</label>
					<input type="text" name="client_email" id="client_email" value="@if($continue_old_invoice and $invoice_data->client_email){{$invoice_data->client_email}}@endif" class="form-control client_email"   />					
				</div>
                <div class="form-group">
					<label class="control-label" for="cc_email">
						Cc Email:
					</label>
					<input type="text" name="cc_email" id="cc_email" value="@if($continue_old_invoice and $invoice_data->cc_email){{$invoice_data->cc_email}}@endif" class="form-control cc_email"  />					
				</div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-5">
                      <label> Recurring:</label>
                    </div>
                    <div class="col-md-7">                           
                      <label class="switch ib switch-custom">                    
        					<input type="checkbox" name="recurring_on" id="recurring_on" @if($continue_old_invoice and $invoice_data->recurring_on==1) checked="" @endif />
        					<label for="recurring_on" data-on="ON" data-off="OFF"></label>					
        		      </label>
                    </div>
                    
                   </div>
                </div>
                <div class="form-group">
					<label class="control-label" for="company_logo">
						Company Logo:
					</label>
					<input type="file" name="company_logo" id="company_logo" class="form-control company_logo" accept="image/*" />					
				</div>
                
                <div class="form-group">
					<label class="control-label" for="company_name">
						Company Name:
					</label>
					<input type="text" name="company_name" id="company_name" value="@if($continue_old_invoice and $invoice_data->company_name){{$invoice_data->company_name}}@else{{session('user')->name}}@endif" class="form-control company_name"  />				
				</div>
                <div class="form-group">
					<label class="control-label" for="company_address">
						Company Address:
					</label>
                    <textarea name="company_address" class="form-control company_address" id="company_address" placeholder=""><?php if($continue_old_invoice and $invoice_data->company_address){echo $invoice_data->company_address;}else{echo session('user')->name."\r\n".session('user')->address."\r\n".session('user')->city.' '.session('user')->state.' '.session('user')->zip."\r\n".session('user')->mobile."\r\n".trim(str_replace('https://','',str_replace('http://','',session('user')->url)),'/')."\r\n";if(session('user')->category=="admin"){echo $INVOICEEMAILID;}else{echo session('user')->email;} }?></textarea>									
				</div>                
				<div class="form-group">
					<label class="control-label" for="bill_to">
						Bill To:
					</label>
					<textarea name="bill_to" class="form-control bill_to" id="bill_to" placeholder="">@if($continue_old_invoice and $invoice_data->bill_to){{$invoice_data->bill_to}}@endif</textarea>
				</div>
                <div class="form-group">
					<label class="control-label" for="invoice_date">
						Invoice Date
					</label>
					<input type="text" name="invoice_date" id="invoice_date" value="@if($continue_old_invoice and $invoice_data->invoice_date!='0000-00-00'){{date('m/d/Y',strtotime($invoice_data->invoice_date))}}@else{{date('m/d/Y')}}@endif" class="form-control invoice_date cursor-pointer" placeholder="MM/DD/YYYY" readonly="readonly"/>
				</div>
				<div class="form-group">
                    <!--<label class="control-label" for="due_date">
						Due On Receipt
					</label>-->
                    <select class="form-control due_date" name="due_date" id="due_date">
                      <option value="0" @if($continue_old_invoice and $invoice_data->due_on_receipt=='0') selected="" @endif>Due on receipt</option>                                          
                      <option value="3" @if($continue_old_invoice and $invoice_data->due_on_receipt=='3') selected="" @endif>3 Days</option>
                      <option value="7" @if($continue_old_invoice and $invoice_data->due_on_receipt=='7') selected="" @endif>7 Days</option>
                      <option value="14" @if($continue_old_invoice and $invoice_data->due_on_receipt=='14') selected="" @endif>14 Days</option>
                      <option value="21" @if($continue_old_invoice and $invoice_data->due_on_receipt=='21') selected="" @endif>21 Days</option>
                      <option value="30" @if($continue_old_invoice and $invoice_data->due_on_receipt=='30') selected="" @endif>30 Days</option>
                      <option value="45" @if($continue_old_invoice and $invoice_data->due_on_receipt=='45') selected="" @endif>45 Days</option>
                      <option value="60" @if($continue_old_invoice and $invoice_data->due_on_receipt=='60') selected="" @endif>60 Days</option>
                      <option value="180" @if($continue_old_invoice and $invoice_data->due_on_receipt=='180') selected="" @endif>180 Days</option>
                    </select>					
				</div>                
                
				<div class="form-group">
					<label class="control-label" for="recurring_date">
						Recurring Date
					</label>
					<input type="text" name="recurring_date" id="recurring_date" value="@if($continue_old_invoice and $invoice_data->recurring_date!='0000-00-00 00:00:00'){{date('m/d/Y',strtotime($invoice_data->recurring_date))}}@endif" class="form-control recurring_date cursor-pointer" placeholder="MM/DD/YYYY" @if($continue_old_invoice and $invoice_data->recurring_on==1)  @else disabled="" @endif readonly="readonly" />
				</div>
                
                
				<div class="form-group">
					<label class="control-label" for="tax">
						Tax(%)
					</label>
					<input type="number" name="tax" id="tax" value="@if($continue_old_invoice and $invoice_data->tax_perc){{$invoice_data->tax_perc}}@elseif($tax!='NA'){{$tax}}@else{{'0.00'}}@endif" class="form-control tax" placeholder="%" min="0" step="0.01" @if($continue_old_invoice and $invoice_data->tax_perc and $invoice_data->tax) @else readonly="" @endif />
				    <br />
                    <label class="switch ib switch-custom">
					<input type="checkbox" name="include_tax" id="include_tax" @if($continue_old_invoice and $invoice_data->tax_perc and $invoice_data->tax) checked="" @endif />
					<label for="include_tax" data-on="YES" data-off="NO"></label>
					<span class="switch_label">Include tax</span>
					</label>                    
                </div>
				<hr>
				<div class="panel-heading  gred_2" style="padding: 0px 15px;">
					<h4 style="float:left; margin: 9px 0;">
						Add Items
					</h4>
					<input type="button" name="" value="+" class="btn form-control add_existing" id="add_existing" />
					<div class="clear" style=" clear: both;">
					</div>
				</div>
				<div class="row">
                    <div class="col-md-12">
						<div class="form-group">
							<label class="control-label" for="item">
								Item
							</label>
							<input type="text"   class="form-control item" id="item"/><!--name="item"-->
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label" for="description">
								Description
							</label>
							<textarea  name="" class="form-control a_des" id="description"></textarea>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label class="control-label" for="price">
								Rate
							</label>
                            <div class="input-group">
                            <span class="input-group-addon text-black">$</span>
							<input type="number" name="" class="form-control a_price" id="price" step="0.01" />
                            </div>
						</div>
					</div>
					<div class="col-md-4 col-sm-8 col-xs-7">
						<div class="form-group">
							<label class="control-label" for="quantity">
								Qnt.
							</label>
							<input type="number" name="" class="form-control a_quantity" id="quantity" step="0.01" min="1"/>
						</div>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-5">
						<div class="form-group">
							<label class="control-label" for="addto">
								<br>
							</label>
							<button type="button" class="btn add-btn" id="addto" data_id=""><i class="fa fa-arrow-circle-right"></i></button>
						</div>
					</div>
				</div>
			</div>
		</section>
        </div>
        <div class="col-md-8 p-r-0">
			<!--header area-->
			<div class="table-responsive invoice-table">
				<table class="table table-condensed">				
					<tbody>
						<tr>
							<td colspan="2">
								<img id="company_logo_field" class="invoice-logo" src="@if($continue_old_invoice){{asset('pownder/storage/app/uploads/invoice/company_logo/'.$invoice_data->company_logo)}}@else{{asset('assets/img/favicon.png')}}@endif"/>
								<h2  class="title m-t-10 m-b-10 main-title">
									Invoice
								</h2>
								<h4 id="company_name_field" class="title m-t-10 m-b-10 sub-title">
									@if($continue_old_invoice and $invoice_data->company_name){{$invoice_data->company_name}}@else{{session('user')->name}}@endif
								</h4>
							</td>
							<td>
							</td>
							<td class="align-bottom" colspan="2">
								<p id="company_address_field" class="text-right">
									<?php if($continue_old_invoice and $invoice_data->company_address){echo nl2br($invoice_data->company_address);}else{ echo session('user')->name.'<br/>'.session('user')->address.'<br/>'.session('user')->city.' '.session('user')->state.' '.session('user')->zip.'<br/>'.session('user')->mobile.'<br/>'.trim(str_replace('https://','',str_replace('http://','',session('user')->url)),'/').'<br/>';if(session('user')->category=="admin"){echo $INVOICEEMAILID;}else{echo session('user')->email;} } ?>
								</p>
							</td>
						</tr>
					</tbody>
				</table>
				<!--header bottom area-->
				<div class="invoice-header-bottom">
					<table class="col-100">					
						<tbody>
							<tr>
								<td class="col-10 align-top"  rowspan="4">
									<p class="m-0 title">
										Bill To:
									</p>
								</td>
								<td class="col-40 align-top" colspan="3" rowspan="4">
									<p class="content align-top" id="bill_to_field">
										<?php if($continue_old_invoice and $invoice_data->bill_to){ echo nl2br($invoice_data->bill_to); }else { echo 'Name <br /> Address <br/>City State Zip<br />Phone<br />Website<br/>Email'; } ?>
									</p>
								</td>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Invoice Num
									</p>
								</td>
								<td class="col-20 align-top" colspan="2">
									<p class="content align-top right">										
										<span id="time">
                                         @if(Session::has('invoice_edit')) {{session('invoice_data')->invoice_num}} @else {{$invoice_id}} @endif
										</span>
									</p>
								</td>
							</tr>
							<tr>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Date
									</p>
								</td>
								<td class="col-20 align-top" colspan="2">
									<p class="content align-top right" id="invoice_date_field">
										@if($continue_old_invoice and $invoice_data->invoice_date!='0000-00-00'){{date('m/d/Y',strtotime($invoice_data->invoice_date))}} @else {{date('m/d/Y')}} @endif
									</p>
								</td>
							</tr>
							<tr>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Due Date
									</p>
								</td>
								<td class="col-20 align-top" colspan="2">
									<p class="content align-top right" id="due_date_field">
                                    @if($continue_old_invoice and $invoice_data->due_date!='0000-00-00'){{date('m/d/Y',strtotime($invoice_data->due_date))}}@else{{date('m/d/Y')}}@endif
									</p>
								</td>
							</tr>
                            
							<tr class="recurring_date_fcontainer @if($continue_old_invoice and $invoice_data->recurring_on){{'visible'}}@else{{'invisible'}}@endif">
								<td class="col-20 align-top">
									<p class="title align-top right">
										Recurring Date
									</p>
								</td>
								<td class="col-20 align-top" colspan="2">
									<p class="content align-top right" id="recurring_date_field">
                                    @if($continue_old_invoice and $invoice_data->recurring_date!='0000-00-00 00:00:00'){{date('m/d/Y',strtotime($invoice_data->recurring_date))}}@endif
									</p>
								</td>
							</tr>
       
                           <!-- <span id="recurring_date_field" class="invisible"></span>-->
						</tbody>
					</table>
				</div>
				<!--header title area-->
				<div class="invoice-title-section">
					<table class="col-100">
						<tbody>
							<tr>
								<td class="col-30 align-top" colspan="2">
									<p class="title align-top left">
										Item
									</p>
								</td>
                                <td class="col-20 align-top">
									<p class="title align-top left">
										Description
									</p>
								</td>
								<td class="col-15 align-top">
									<p class="title align-top right">
										Quantity
									</p>
								</td>
								<td class="col-15 align-top">
									<p class="title align-top right">
										Rate
									</p>
								</td>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Amount
									</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--header content area-->
				<div class="invoice-details-section">
					<div class="item-details-section">
						<table class="col-100" style="position: relative;">
							<tbody class="append_items">
							 @if($continue_old_invoice)
                             @foreach($invoice_items as $item)
                              <tr class="border-b">
                              <td class="col-10 align-top item-option"><i class="fa fa-times delete" ></i><i class="fa fa-pencil edit"></i></td>
                              <td class="col-20 align-top" ><p class="content align-top left item_name">{{$item->item}}</p><input type="hidden" name="item[]" value="{{$item->item}}" /></td>
                              <td class="col-20 align-top"><p class="content align-top left des"><?php echo nl2br($item->description);?></p><input type="hidden" value="{{$item->description}}" name="description[]" /></td>
                              <td class="col-15 align-top"><p class="content align-top right qnt">{{money_format1($item->qty)}}</p><input type="hidden" value="{{$item->qty}}" name="qty[]" /></td>
                              <td class="col-15 align-top"><p class="content align-top right "> $<span class="rate">{{money_format1($item->rate)}}</span></p><input type="hidden" value="{{$item->rate}}" name="rate[]" /></td>
                              <td class="col-20 align-top"><p class="content align-top right item_total">$<span>{{money_format1($item->amount)}}</span></p><input type="hidden" value="{{$item->amount}}" name="amount[]" /></td>
                              </tr>
                             @endforeach
                             @endif
							</tbody>
						</table>
					</div>
					<table class="col-100">
						<tbody>
							<!--bill section-->
							<tr>
								<td class="col-60 align-top" rowspan="5" colspan="3">                                
								</td>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Subtotal
									</p>
								</td>
								<td class="col-20 align-top">
                                    <input type="hidden" name="sub_total" value="@if($continue_old_invoice){{$invoice_data->sub_total}}@else{{'0'}}@endif" />
									<p class="title align-top right calculation_sub_total">                                        
										$@if($continue_old_invoice){{money_format1($invoice_data->sub_total)}}@else{{'0'}}@endif
									</p>
								</td>
							</tr>
							<tr>
							
								<td class="col-20 align-top">
                                   <input type="hidden" name="tax_perc" value="@if($continue_old_invoice){{$invoice_data->tax_perc}}@else{{'0'}}@endif" />
									<p class="title align-top right">
										Tax(
										<span class="calculation_tax_perc" id="tax_field">
                                        @if($continue_old_invoice){{money_format1($invoice_data->tax_perc)}}@else{{'0.00'}}@endif  
                                        <?php /*elseif($tax!='NA'){{money_format1($tax)}} elseif($tax!='NA'){{$tax}}*/ ?>                                        
										</span>                                        
										%)
									</p>
								</td>
								<td class="col-20 align-top">
                                    <input type="hidden" name="tax_value" value="@if($continue_old_invoice){{$invoice_data->tax}}@else{{'0'}}@endif" />
									<p class="title align-top right calculation_tax">                                        
										$@if($continue_old_invoice){{money_format1($invoice_data->tax)}}@else{{'0'}}@endif
									</p>
								</td>
							</tr>
							<tr>
							
								<td class="col-20 align-top">
									<p class="title align-top right">
										Total
									</p>
								</td>
								<td class="col-20 align-top">
                                    <input type="hidden" name="total" value="@if($continue_old_invoice){{$invoice_data->total}}@else{{'0'}}@endif" />
									<p class="title align-top right calculation_total">                                        
										$@if($continue_old_invoice){{money_format1($invoice_data->total)}}@else{{'0'}}@endif
									</p>
								</td>
							</tr>
							<tr>
							
								<td class="col-20 align-top">
									<p class="title align-top right">
										Paid
									</p>
								</td>
								<td class="col-20 align-top">
                                   <input type="hidden" name="paid" value="@if($continue_old_invoice){{$invoice_data->paid}}@else{{'0'}}@endif" />
									<p class="title align-top right calculation_paid">                                        
										$@if($continue_old_invoice){{money_format1($invoice_data->paid)}}@else{{'0'}}@endif
									</p>
								</td>
							</tr>
							<tr>
							
								<td class="col-20 align-top final-due">
									<p class="title align-top right">
										Balance Due
									</p>
								</td>
								<td class="col-20 align-top final-due">
                                    <input type="hidden" name="balance" value="@if($continue_old_invoice){{$invoice_data->balance}}@else{{'0'}}@endif" />
									<p class="title align-top right calculation_balance">                                        
										$@if($continue_old_invoice){{money_format1($invoice_data->balance)}}@else{{'0'}}@endif
									</p>
								</td>
							</tr>
                            @if(Session::has('draft_continue'))
                            <input type="hidden" name="draft_id" value="{{session('draft_data')->id}}" />
                            <input type="hidden" name="company_logo_org" value="{{session('draft_data')->company_logo}}" />
                            @endif
                            @if(Session::has('invoice_edit'))
                            <input type="hidden" name="invoice_id" value="{{session('invoice_data')->invoice_id}}" />
                            <input type="hidden" name="invoice_num" value="{{session('invoice_data')->invoice_num}}" />
                            <input type="hidden" name="company_logo_org" value="{{session('invoice_data')->company_logo}}" />
                            @endif                             
						</tbody>
					</table>
					
                    @if($INVOICETNC)
                    <hr/>  
                    <div class="row">
                      <div class="col-md-12 invoice_tnc_con">
                      
                        <?php echo $INVOICETNC; ?> 
                        
                      </div>
                    
                    </div>
                     <br />                                                           
                    @endif				
				</div> 
                               
			</div>            
		
        <br />
        <div class="row">
                        <div class="col-md-12 text-right">           
                                 <button type="button" class="btn btn-default  download-invoice-pdf">Download PDF</button>                                 
                                 @if(Session::has('draft_continue'))
                                 <button type="button" class="btn btn-primary  update-invoice-draft blue_btn">Update Draft</button>
                                 @else
                                 <button type="button" class="btn btn-primary  save-invoice-draft blue_btn">Save As Draft</button>
                                 @endif
                                 <button type="button" class="btn btn-warning mark_invoice_paid  pownder_yellow">Mark As Paid</button>
                                 @if(Session::has('invoice_edit'))                                            
                                 <button type="submit" class="btn btn-success save_and_email green_btn">Update and Email</button>
                                 @else
                                 <button type="submit" class="btn btn-success save_and_email green_btn">Save and Email</button>
                                 @endif
                                                                                                
                      </div>
            </div>        
       </div>
	</div>
</section>
</form>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header gred_2">
				<button type="button" class="close" data-dismiss="modal">
					&times;
				</button>
				<h4 class="modal-title" style="color:#fff;">
					Item List
				</h4>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table item-list-table">
						<thead>
							<tr>
                                <td><input type="checkbox" class="checkall" /></td>
								<td class="title">
									Item
								</td>
                                <td class="title">
									Description
								</td>
                                <td class="title">
                                  Qty
                                </td>
                                <td class="title">
                                  Rate
                                </td>
                                <td class="title"></td>
							</tr>
						</thead>
						<tbody>
                            @foreach($saved_items as $item)                            
							<tr>          
                                <td>
									<input type="checkbox" class="saved_item_id  checkitem add_items_to_invoice" value="{{$item->id}}" />                                    
						        </td>                      
                                <td class="i_item">
									{{$item->item}}
								</td> 
								<td class="i_des">
									<?php echo nl2br($item->description); ?>                                    
								</td>
                                <td class="i_qty">
                                  1
                                </td>
                                <td class="i_rate">
                                $0.00
                                </td>                                
                                <td>
									<button title="Remove Item" class="btn btn-sm bg-danger remove-saved-item i_add_button i_add pull-right">
										<i class="fa fa-close"></i>
									</button>
								</td>
							</tr>
                            @endforeach
							
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">              
				<button type="button" class="btn btn-default" data-dismiss="modal" style=" background: #ffbe18; color:#fff;">
					Close
				</button>
			</div>
		</div>
	</div>
</div>
<!-- /.content -->
<!--Upload cheque images -->
<div class="modal fade" id="upload_cheque" role="dialog">
   <form  method="post" class="form-horizontal form_upload_cheque" enctype="multipart/form-data"  >   
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mark Invoice As Paid</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
             <label class="col-xs-12">
             Do you want to upload Bank Cheque Images??
             </label>
            </div>
            <div class="form-group">
             <label class="col-xs-12">            
             <button type="button" class="btn btn-primary yes_upload">Yes</button>
             <button type="button" class="btn btn-default no_upload">No</button>
             </label>
            </div>
            <div class="form-group hidden upload_images">
              <label class="control-label col-xs-3">Upload Images</label>
              <div class="col-xs-9">
                <input  type="file" class="form-control" name="cheque[]" id="cheque" multiple="" accept="image/*"   />
              </div>
            </div> 
                       
          </div>
          <div class="modal-footer">     
            <div class="alert alert-danger hidden error_msgs text-justify "></div>       
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success submit_mark_paid">Mark As Paid</button>
          </div>
        </div>
    </div>
  </form>  
</div>
<!--Upload cheque images -->

@stop 

{{-- page level scripts --}} 
@section('footer_scripts')
<!-- begining of page level js -->
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- date-range-picker -->
<!--<script type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/iCheck/js/icheck.js')}}"></script>-->
<script type="text/javascript" src="{{asset('assets/js/custom_js/invoice.js')}}"></script>
@stop