@extends('layouts/default')
 
{{-- Page title --}} 
@section('title') 
Manage Invoice @parent 
@stop 

{{-- page level styles --}} 
@section('header_styles')
<!--start page level css -->

<style>
	 
	  .date_range {
		         margin: 1% 0;
                 margin-top: 10px;
                 float: right;
                 width: 5%;
	  }
      
	  .fixed-table-toolbar {width:95%;}
      .fixed-table-toolbar .search {width: Calc( 100% - 293px );}
	  
      .input-group .form-control.date_range_picker { width: 230px; }      
	  
	  @media screen and (max-width: 767px) {
		  .date_range {
				float: left;
				width: 98%;
			}
			.fixed-table-toolbar {
				width: 100%;
			}
			.fixed-table-toolbar {width:100%;}
            .fixed-table-toolbar .search {width: Calc( 100% );}
       }
       .lead
       {
        font-weight: bold;        
        font-size: 15px;
       }
       .bg-white
       {
        background-color: white;    
        height: 50px;
        width: 50px;
        border-radius: 50%;
        padding: 6px 0px;
        font-weight: bold;
        border-width: 2px;
       }
       .text-muted.small
       {
        color: gray !important
       }
       .text-black
       {
        color: #000 !important;
       }
       .text-info
       {
        color: #008fac !important;
       }
       .text-primary
       {
        color: #428BCA !important;
       }
       .text-success
       {
        color: #22D69D !important;
       }
       .text-danger
       {
        color: #FB8678 !important;
       }
       button.bg-white
       {
        line-height: 13px !important;
       }
       .fixed_width
       {
        width: 80px;
        margin: auto;
       }
       .btn.bg-white:hover
       {
        color:#fff !important;
       }
       .m-l-10
       {
        margin-left: 10px !important;
       }
       .m-l-5
       {
        margin-left: 5px !important;
       }
       .table > thead > tr > th, .table > thead > tr > td, .table > tbody > tr > th, .table > tbody > tr > td, .table > tfoot > tr > th, .table > tfoot > tr > td { vertical-align: middle !important; }
	</style>
<!--end page level css-->
@stop 
{{-- Page content --}}
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
			<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
        <li>
			<a href="{{ route('invoice') }}">
			 Invoice
			</a>
		</li>
		<li class="active">
		  Manage Invoice
		</li>
	</ol>
</section>
<?php 
//Money Format
 function money_format1($number)
{
  setlocale(LC_MONETARY, 'en_US'); 
  return money_format('%!.2i',$number); 
}
?>
<!-- Main content -->
<section class="content p-l-r-15">
    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}"  />
    <input type="hidden" name="trigger_filter" id="trigger_filter" value="@if(Request::has('filter')){{Request::get('filter')}}@endif" />
    <input type="hidden" name="filter_type" id="filter_type" value="@if(Request::has('type')){{Request::get('type')}}@endif" />
    @include('panel.includes.status')   
    
	<div class="row">
      <div class="col-lg-12">
		<div class="panel panel-success filterable">
          <div class="panel-heading">
             <h3 class="panel-title">
              <i class="fa fa-fw fa-th-large"></i> Invoice
             </h3>
          </div>
          <div class="panel-body">
					
	    				<div class="form-group date_range">
                        <div class="input-group">
                        <!--<span class="input-group-addon"><i class="fa fa-calendar"></i></span>-->          
                        <!--
                        <input type="text" class="form-control pull-right date_range_picker" id="date-range0" placeholder="MM-DD-YYYY to MM-DD-YYYY"  />
                        <div class="input-group-btn">
                          <button type="button" class="btn btn-default filter_invoice_date_range" title="Filter Data Between Date Range"><i class="fa fa-search"></i></button>
                          <a href="{{route('manage-invoice')}}">
                          <button type="button" class="btn btn-default m-l-5" title="Refresh Data"><i class="fa fa-refresh"></i></button>
                          </a> 
                        </div>
                        -->
                        
                        </div> 
                        <a href="{{route('manage-invoice')}}">
                          <button type="button" class="btn btn-default m-l-5" title="Refresh Data"><i class="fa fa-refresh"></i></button>
                          </a>    
                        </div>
                        <table id="invoices" data-toolbar="#toolbar" data-search="true"  data-show-columns="true" 
                             data-pagination="true" data-id-field="Status" data-page-list="[10, 20,40,ALL]"
                                data-height="503">
                            <thead>
                            <tr>
                                <th data-field="Status" data-sortable="true">Status</th>
                                <th data-field="Bill To" data-sortable="true">Bill To</th>
                                <th data-field="Email To" data-sortable="true">Email To</th>
                                <th data-field="Amount" data-sortable="true">Amount</th>
                                <th data-field="Recurring" data-sortable="true">Recurring</th>
                                <th data-field="Sent By" data-sortable="true" @if(session('user')->category=="admin") data-visible="false" @endif>Sent By</th>
                                <th data-field="Action"  class="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody id="invoice_data">
                            <?php $k=0; ?>
                            @foreach($invoices as $invoice)
                            <?php 
                            $duein='-';
                            if($invoice->due_date!="0000-00-00")
                            {
                            $date1=date_create(date('Y-m-d'));
                            $date2=date_create($invoice->due_date);    
                            $diff=date_diff($date1,$date2); 
                            $duein=$diff->format('%r%a');
                            }
                            ?>
                            <tr>
                                <td>
                                 <?php $balance_color='';$invoice_num='';$due_status='';?>
                                 @if($invoice->invoice_type=="draft")
                                    <button class="btn btn-info bg-white text-info  small"><small>Draft</small></button>
                                    <?php $balance_color='text-info';$invoice_num='DRFT-'.sprintf('%03d',$invoice->invoice_num);$due_status=($duein>=0) ? ' - Due in '.abs($duein).' days' : ' - overdue by '.abs($duein).' days'; ?>
                                 @elseif($invoice->invoice_type=="sent")   
                                    <button class="btn btn-success bg-white text-success small"><small>Sent</small></button>
                                    <?php $balance_color='text-success';$invoice_num=$invoice->invoice_num;$due_status=' - Due in '.abs($duein).' days'; ?>
                                 @elseif($invoice->invoice_type=="paid")   
                                    <button class="btn btn-primary bg-white text-primary  small"><small>Paid</small></button> 
                                    <?php $balance_color='text-primary';$invoice_num=$invoice->invoice_num;$due_status=' - '.sprintf('CNF%05d',$invoice->id); ?>
                                 @elseif($invoice->invoice_type=="overdue")   
                                    <button class="btn btn-danger bg-white text-danger  small"><small>Over<br/>due</small></button> 
                                    <?php $balance_color='text-danger';$invoice_num=$invoice->invoice_num;$due_status=' - overdue by '.abs($duein).' days'; ?>   
                                 @endif     
                                </td>
                                <td><strong class="bill_to lead text-black">{{$invoice->client_name}}</strong>
                                <br />
                                <span class="text-muted small">
                                {{date('F d, Y',strtotime($invoice->created_at))}} 
                                 {{$due_status}} 
                                </span>
                                </td>
                                <td>{{$invoice->client_email}}</td>
                                <td>
                                <span><strong class="lead {{$balance_color}}">${{money_format1($invoice->balance)}}</strong></span>
                                <br /><span class="text-muted small">{{$invoice_num}}</span>
                                </td>
                                <td>
                                   @if($invoice->recurring_on!="na" and $invoice->has_recurred!=1)
                                    <label class="switch ib switch-custom text-center">
                					<input type="checkbox" class="recurring-status" id="include_tax{{++$k}}" @if($invoice->recurring_on) checked="" @endif />
                					<label for="include_tax{{$k}}" data-on="ON" data-off="OFF" data-num="{{$invoice->invoice_num}}" data-id="{{$invoice->invoice_id}}"></label>
                					<span class="switch_label">Recurring</span>
    					           </label>                                     
                                   @endif
                                </td>
                                <td>
                                  @if(session('user')->category!="admin" and $invoice->created_user_id!=session('user')->id)
                                 <label class="badge">By Admin</label>
                                 @endif
                                </td>
                                <td class="text-center">                                
                                 @if($invoice->invoice_type=="draft")
                                    <a href="#" data-id="{{$invoice->id}}" data-invoice-type="draft" class="delete-invoice" title="Delete Invoice"><i class="fa fa-trash-o text-info"></i></a>
                                    <a href="{{route('draft-invoice',['id'=>$invoice->id])}}"><i class="fa fa-repeat text-info" title="Continue Invoice"></i></a>
                                 @elseif($invoice->invoice_type=="sent")
                                    <a href="{{route('edit-sent-invoice',['id'=>$invoice->invoice_id])}}"><i class="fa fa-edit text-success" title="Edit Invoice"></i></a>                                                                  
                                    <a href="#" data-num="{{$invoice->invoice_num}}" data-id="{{$invoice->invoice_id}}" data-email="{{$invoice->client_email}}" data-ccemail="{{$invoice->cc_email}}" class="send-payment-reminder-direct" title="Send Payment Reminder"><i class="fa fa-envelope text-success"></i></a>  
                                    <a href="#" data-num="{{$invoice->invoice_num}}" data-id="{{$invoice->invoice_id}}" class="mark-as-paid" title="Mark Invoice As Paid"><i class="fa fa-check-square-o text-success"></i></a>                                  
                                    <a href="#" data-id="{{$invoice->invoice_id}}" data-invoice-type="sent" class="delete-invoice" title="Delete Invoice"><i class="fa fa-trash-o text-success"></i></a>
                                    <a href="{{route('view-invoice',['id'=>$invoice->id,'type'=>$invoice->invoice_type])}}"><i class="fa fa-file-pdf-o text-success" title="View Invoice"></i></a>                                     
                                 @elseif($invoice->invoice_type=="paid")
                                   <a href="#" data-num="{{$invoice->invoice_num}}" data-id="{{$invoice->invoice_id}}" data-invoice-type="paid" class="clone-invoice" title="Clone Invoice"><i class="fa fa-clone text-primary"></i></a>
                                   <a href="#" data-id="{{$invoice->invoice_id}}" data-invoice-type="paid" class="delete-invoice" title="Delete Invoice"><i class="fa fa-trash-o text-primary"></i></a>
                                   <a href="{{route('view-invoice',['id'=>$invoice->id,'type'=>$invoice->invoice_type])}}"><i class="fa fa-file-pdf-o text-primary" title="View Invoice"></i></a>
                                 @elseif($invoice->invoice_type=="overdue")
                                 <a href="{{route('edit-sent-invoice',['id'=>$invoice->invoice_id])}}"><i class="fa fa-edit text-danger" title="Edit Invoice"></i></a>
                                 <a href="#" data-num="{{$invoice->invoice_num}}" data-id="{{$invoice->invoice_id}}" data-email="{{$invoice->client_email}}" data-ccemail="{{$invoice->cc_email}}" class="send-payment-reminder-direct" title="Send Payment Reminder"><i class="fa fa-envelope text-danger"></i></a>                                 
                                 <a href="#" data-num="{{$invoice->invoice_num}}" data-id="{{$invoice->invoice_id}}" class="mark-as-paid" title="Mark Invoice As Paid"><i class="fa fa-check-square-o text-danger"></i></a>
                                 <a href="#" data-id="{{$invoice->invoice_id}}" data-invoice-type="sent" class="delete-invoice" title="Delete Invoice"><i class="fa fa-trash-o text-danger"></i></a>                                  
                                 <a href="{{route('view-invoice',['id'=>$invoice->id,'type'=>$invoice->invoice_type])}}"><i class="fa fa-file-pdf-o text-danger" title="View Invoice"></i></a>  
                                 @endif                                 
                                </td>
                            </tr>
                            @endforeach                           
                            </tbody>
                        </table>
                    </div>
        </div>
		
	  </div>	
	</div>
</section>

<!-- /.content -->
<div class="modal fade animated" id="send_reminder_modal" role="dialog">
    <form  method="post" class="form-horizontal send_reminder_form">
    <input type="hidden" id="send_reminder_invoice_id" name="invoice_id" />
    <input type="hidden" id="send_reminder_invoice_num" name="invoice_num" />
    <input type="hidden" id="send_reminder_invoice_billto" name="billto" />
    {{csrf_field()}}
    <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Send Payment Reminder</h4>              
           </div>
           <div class="modal-body">
          
              <div class="form-group">
                 <label class="control-label col-xs-2">To:</label>
                 <div class="col-xs-10 p-t-7">
                   <span class="form-control-staic send_reminder_emailto" ></span> 
                   <span class="form-control-staic send_reminder_emailid" ></span>
                 </div>
              </div>
              <div class="form-group hidden">
                 <label class="control-label col-xs-2">Cc:</label>
                 <div class="col-xs-10 p-t-7">
                   <span class="form-control-staic send_reminder_ccemail" ></span>
                 </div>
              </div>
              <div class="form-group">
                 <label class="control-label col-xs-2 text-hide">Sub:</label>
                 <div class="col-xs-10 p-t-7">
                   <div class="input-group">
                      <span class="input-group-addon">Reminder:</span>
                      <!--Your payment for this invoice is due-->
                      <input type="text" class="form-control" id="send_reminder_subject" value="" name="send_reminder_subject"  placeholder="Subject" />
                   </div>
                 </div>
              </div>
              <div class="form-group">
                 <label class="control-label col-xs-2 text-hide">Msg:</label>
                 <div class="col-xs-10 p-t-7">
                      <textarea class="form-control" id="send_reminder_msg" name="send_reminder_msg"  placeholder="Add personal note to recipient" ></textarea>
                 </div>
              </div>           
           </div>
           <div class="modal-footer">
             <button type="submit" class="btn btn-primary blue_btn">Send Reminder</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
           </div> 
         </div>
    </div>
    </form>
</div>

<!--Upload cheque images -->
<div class="modal fade" id="upload_cheque" role="dialog">
   <form  method="post" class="form-horizontal form_upload_cheque" enctype="multipart/form-data"  >
   {{csrf_field()}}
   <input type="hidden" name="invoice_num" id="map_invoice_num" />
   <input type="hidden" name="invoice_id" id="map_invoice_id" />
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Mark Invoice As Paid</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
             <label class="col-xs-12">
             Do you want to upload Bank Cheque Images??
             </label>
            </div>
            <div class="form-group">
             <label class="col-xs-12">            
             <button type="button" class="btn btn-primary yes_upload">Yes</button>
             <button type="button" class="btn btn-default no_upload">No</button>
             </label>
            </div>
            <div class="form-group hidden upload_images">
              <label class="control-label col-xs-3">Upload Images</label>
              <div class="col-xs-9">
                <input  type="file" class="form-control" name="cheque[]" id="cheque" multiple="" accept="image/*"   />
              </div>
            </div> 
                       
          </div>
          <div class="modal-footer">     
            <div class="alert alert-danger hidden error_msgs text-justify "></div>       
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success submit_mark_paid">Mark As Paid</button>
          </div>
        </div>
    </div>
  </form>  
</div>
<!--Upload cheque images -->

@stop 

{{-- page level scripts --}} 
@section('footer_scripts')
<!-- begining of page level js -->
    <!--Custom JS -->
    
    <script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/tableExport/tableExport.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
    <!-- date-range-picker -->
	<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}" ></script>
	<!-- bootstrap time picker -->
	<script  type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" ></script>

	<script  type="text/javascript" src="{{asset('assets/vendors/jquerydaterangepicker/js/jquery.daterangepicker.min.js')}}" ></script>
	<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}" ></script>
	<script  type="text/javascript" src="{{asset('assets/vendors/timedropper/js/timedropper.js')}}" ></script>
    <script>
	//$('#date-range0').dateRangePicker({
//		autoClose: true,
//		format: 'MM/DD/YYYY',
//        getValue: function () {
//            var range = $(this).val();
//			//alert(range);
//        }
//    });
//	
	</script>
    <script type="text/javascript" src="{{asset('assets/js/custom_js/custom.js')}}"></script>
	

@stop