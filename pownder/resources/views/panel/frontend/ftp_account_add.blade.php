@extends('layouts/default')

{{-- Page title --}}
@section('title')
FTP Connect
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">
			FTP Connect
		</li>
	</ol>
</section>


<!-- Main content -->
<section class="content p-l-r-15">
	@include('panel.includes.status')
	<?php
		$errorarr=$errors;   
		function showerrormsg($key,$errors)
		{    
			$msg='';
			if(count($errors->get($key))>0)
			{ 
				$msg.='';
				foreach($errors->get($key) as $error)
				{
					if($key=="alert_success")
					{
						$msg.=$error;
					}
					else
					{
						$msg.='<small class="text-danger animated fadeInUp">'.$error.'</small>' ; 
					}
				}
			} 
			return $msg;     
		}
	?>
	<div class="row">
		<div class="col-lg-12">
			<form method="POST" action="{{ route('add-ftp-account') }}" role="form" id="validate_add_ftp_account">
				<div class="panel panel-success filterable">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-fw fa-th-large"></i> Add FTP Account
						</h3>
					</div>
					<div class="panel-body form-body">
						<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
						<div class="row m-t-10">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="name">Name</label>
									<input type="text" id="name" name="name" placeholder="Name" value="{{ old('name') }}" class="form-control">
									<small class="text-danger animated name fadeInUp validate_add_ftp_account"></small>
									<?php echo showerrormsg('name',$errorarr); ?>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="provider">Provider</label>
									<input type="text" id="provider" name="provider" value="{{ old('provider') }}" placeholder="Provider" class="form-control">
									<small class="text-danger animated provider fadeInUp validate_add_ftp_account"></small>
									<?php echo showerrormsg('provider',$errorarr); ?>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="use_sftp">Use SFTP? (Secure FTP)</label>
									<select id="use_sftp" name="use_sftp" class="form-control">
										<option value="No" @if(old('use_sftp')=='No') selected @endif>No</option>
										<option value="Yes"  @if(old('use_sftp')=='Yes') selected @endif>Yes</option>
									</select>
									<small class="text-danger animated use_sftp fadeInUp validate_add_ftp_account"></small>
									<?php echo showerrormsg('use_sftp',$errorarr); ?>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="ftp_host">FTP Host</label>
									<input type="text" id="ftp_host" name="ftp_host" value="{{ old('ftp_host') }}" placeholder="FTP Host" class="form-control">
									<small class="text-danger animated ftp_host fadeInUp validate_add_ftp_account"></small>
									<?php echo showerrormsg('ftp_host',$errorarr); ?>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="ftp_port">FTP Port</label>
									<input type="text" id="ftp_port" name="ftp_port" value="{{ old('ftp_port') }}" placeholder="FTP Port" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
									<small class="text-danger animated ftp_port fadeInUp validate_add_ftp_account"></small>
									<?php echo showerrormsg('ftp_port',$errorarr); ?>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="ftp_username">FTP Username</label>
									<input type="text" id="ftp_username" name="ftp_username" value="{{ old('ftp_username') }}" placeholder="FTP Username" class="form-control">
									<small class="text-danger animated ftp_username fadeInUp validate_add_ftp_account"></small>
									<?php echo showerrormsg('ftp_username',$errorarr); ?>
								</div>
							</div>
						</div>
						<div class="row  m-t-10">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="ftp_password">FTP Password</label>
									<input type="password" id="ftp_password" name="ftp_password" value="{{ old('ftp_password') }}" placeholder="FTP Password" class="form-control">
									<small class="text-danger animated ftp_password fadeInUp validate_add_ftp_account"></small>
									<?php echo showerrormsg('ftp_password',$errorarr); ?>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
								<div class="form-group">
									<label class="control-label" for="directory">Which directory to search for files?</label>
									<input type="text" id="directory" name="directory" value="{{ old('directory') }}" placeholder="Which directory to search for files?" class="form-control">
									<small class="text-danger animated directory fadeInUp validate_add_ftp_account"></small>
									<?php echo showerrormsg('directory',$errorarr); ?>
								</div>
							</div>
						</div>
						
						<div class="row m-t-10">
							<center>
								<hr />
								<button type="button" class="btn btn-default addButton" style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c !important;"> Submit</button>
								<button type="submit" class="addSubmit" style="display:none"> Submit</button>
							</center>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<!-- /.content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script>
	$(document).ready(function(){
		$(".addButton").click(function( event ) {
			event.preventDefault();
			
			$(".addButton").prop('disabled', true);
			
			$('.validate_add_ftp_account').html('');
			var data=$('#validate_add_ftp_account').serialize();
			
			$.ajax({
				url: "{{ route('validate-add-ftp-account') }}",
				type:'POST',
				data: data,
				success: function(data) {
					$(".addSubmit").trigger('click');
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$("."+key).html(value);
					});
					$(".addButton").prop('disabled', false);
				}       
			});
		});
	});
</script>		
@stop			