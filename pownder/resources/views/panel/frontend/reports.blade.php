@extends('layouts/default')

{{-- Page title --}}
@section('title')
Reports
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	
	@if(Session::get('user_category')=='client' || ManagerHelper::ManagerCategory()=='client')
	#client_roi_section .fixed-table-toolbar {*width:60%;}
	#client_roi_section .fixed-table-toolbar .search {width: Calc( 100% - 195px );}
	#client_profit_section .fixed-table-toolbar .search {width: Calc( 100% - 500px );}
	@else
	#client_roi_section .fixed-table-toolbar {*width:60%;}
	#client_roi_section .fixed-table-toolbar .search {width: Calc( 100% - 195px );}
	#client_profit_section .fixed-table-toolbar .search {width: Calc( 100% - 600px );}
	@endif
	
	#client_profit_section .fixed-table-toolbar .columns-right {margin-top: 80px}
	#client_profit_section .fixed-table-toolbar .search {margin-top: 80px}
	
	.date-picker-wrapper{ z-index: 999999;}
	
	.export.btn-group {*display:none !important;}
	
	.help-block {color: #fa5a46  !important;}
	
	#sold_modal .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	.table-summary {display:inline-block; border: 1px solid #ccc; margin-top: 15px; }
	.table-summary .wrapp {float:left; text-align:center; width:100px;}
	.table-summary .wrapp .title{padding: 5px 0px; font-weight: 800; border-bottom: 1px solid #ccc;border-left: 1px solid #ccc;}
	.table-summary .wrapp .details{padding: 5px; border-left: 1px solid #ccc;}
	.fixed-table-container {height: 500px !important; }
	.nopadding{padding: 0;}
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li><a href="{{ route('/') }}"><i class="fa fa-fw fa-home"></i> Dashboard</a></li>
		<li class="active"><a href="#"> Reports</a>	</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> 
						@if(Session::get('client_id') > 0)
						<span id="profit_title">{{ $Client->name }} Profit</span>
						@else
						<span id="profit_title">Client Profit</span>
						@endif
					</h3>
				</div>
				<div class="panel-body" id="client_profit_section">
					<div class="col-md-12 nopadding" id="toolbar" style="float:left">
						<div class="wrapp table-summary" style="margin-top:35px">  
							@if(Session::get('user_category')!='client' && ManagerHelper::ManagerCategory()!='client')
							<div class="wrapp">       
								<div class="title" style="padding:0px">ROI</div>         
								<div class="details TotalROI" style="padding:0px">0</div>       
							</div>
							@endif
							<div class="wrapp">       
								<div class="title" style="padding:0px">Leads</div>         
								<div class="details" style="padding:0px">0</div>       
							</div>
							<div class="wrapp">       
								<div class="title" style="padding:0px">Sold</div>       
								<div class="details SoldTotals" style="padding:0px">{{ number_format($SoldTotals,0) }}</div>       
							</div> 
							<div class="wrapp">       
								<div class="title" style="padding:0px">Total Profit</div> 
								<div class="details SoldTotalProfits" style="padding:0px">${{ number_format($SoldTotalProfit,0) }}</div>       
							</div> 	
						</div>
					</div>
					
					<table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20, 40, ALL]" data-show-footer="false" data-toggle="table" data-side-pagination="server" data-query-params="queryParams" data-url="{{ route('profit-reports') }}">
						<thead>
                            <tr>
								@if(Session::get('user_category')=='client' || ManagerHelper::ManagerCategory()=='client')
                                <th data-field="#" data-sortable="true" @if(in_array("0",$ClientProfitColumns)) data-visible="false" @endif>#</th>
                                <th data-field="Create Date" data-sortable="true" @if(in_array("1",$ClientProfitColumns)) data-visible="false" @endif>Create Date</th>
                                <th data-field="Sold Date" data-sortable="true" @if(in_array("2",$ClientProfitColumns)) data-visible="false" @endif>Sold Date</th>
                                <th data-field="Stock #" data-sortable="true" @if(in_array("3",$ClientProfitColumns)) data-visible="false" @endif>Stock #</th>
                                <th data-field="Full Name" data-sortable="true" @if(in_array("4",$ClientProfitColumns)) data-visible="false" @endif>Full Name</th>
								<th data-field="Email" data-sortable="true" @if(in_array("5",$ClientProfitColumns)) data-visible="false" @endif>Email</th>
								<th data-field="Phone #" data-sortable="true" @if(in_array("6",$ClientProfitColumns)) data-visible="false" @endif>Phone #</th>
                                <th data-field="Vehicle Info" data-sortable="true" @if(in_array("7",$ClientProfitColumns)) data-visible="false" @endif>Vehicle Info</th>
                                <th data-field="Total Profit" data-sortable="true" data-halign="left" data-align="right" @if(in_array("8",$ClientProfitColumns)) data-visible="false" @endif>Total Profit</th>
								@if($WritePermission=='Yes')<th data-field="Action" data-halign="left" data-align="center" @if(in_array("9",$ClientProfitColumns)) data-visible="false" @endif>Action</th>@endif
								
								@else
								
								<th data-field="#" data-sortable="true" @if(in_array("0",$ClientProfitColumns)) data-visible="false" @endif>#</th>
                                <th data-field="Create Date" data-sortable="true" @if(in_array("1",$ClientProfitColumns)) data-visible="false" @endif>Create Date</th>
								<th data-field="Client" data-sortable="true" @if(in_array("2",$ClientProfitColumns)) data-visible="false" @endif>Client</th>
                                <th data-field="Sold Date" data-sortable="true" @if(in_array("3",$ClientProfitColumns)) data-visible="false" @endif>Sold Date</th>
                                <th data-field="Stock #" data-sortable="true" @if(in_array("4",$ClientProfitColumns)) data-visible="false" @endif>Stock #</th>
                                <th data-field="Full Name" data-sortable="true" @if(in_array("5",$ClientProfitColumns)) data-visible="false" @endif>Full Name</th>
								<th data-field="Email" data-sortable="true" @if(in_array("6",$ClientProfitColumns)) data-visible="false" @endif>Email</th>
								<th data-field="Phone #" data-sortable="true" @if(in_array("7",$ClientProfitColumns)) data-visible="false" @endif>Phone #</th>
                                <th data-field="Vehicle Info" data-sortable="true" @if(in_array("8",$ClientProfitColumns)) data-visible="false" @endif>Vehicle Info</th>
                                <th data-field="Total Profit" data-sortable="true" data-halign="left" data-align="right" @if(in_array("9",$ClientProfitColumns)) data-visible="false" @endif>Total Profit</th>
								@if($WritePermission=='Yes')<th data-field="Action" data-halign="left" data-align="center" @if(in_array("10",$ClientProfitColumns)) data-visible="false" @endif>Action</th>@endif
								@endif
							</tr>
						</thead>
						<tbody>
							
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	@if(Session::get('user_category')=='admin' || Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='admin' || ManagerHelper::ManagerCategory()=='vendor')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Client's ROI
					</h3>
				</div>
				<div class="panel-body" id="client_roi_section">
					<div class="col-md-12 nopadding" style="" align="right">
						<div class="wrapp table-summary">         
							<div class="wrapp" style="padding: 20px; font-weight:800"> Total</div>
							<?php
								$TotalLDs = 0;
								$LeadPlusMinus = 0;
								$TotalCost = 0;
								$TotalProfit = 0;
								$TotalROI = 0;
								
								foreach($Clients as $Client)
								{ 
									$TotalLDs = $TotalLDs + (ClientHelper::ClientLDs($Client->id, '', '')); 
									$LeadPlusMinus = $LeadPlusMinus + (ClientHelper::ClientLDs($Client->id, '', '') - $Client->monthly_lead_quantity);
									
									if($Client->package_type == 'Monthly')
									{
										$Cost = $Client->package_amount - $Client->discount;
									}
									elseif($Client->package_type == 'Lead')
									{
										$Cost = (ClientHelper::ClientLDs($Client->id, '', '') * $Client->per_lead_value) - $Client->discount; 
									}
									elseif($Client->package_type == 'Sold')
									{
										$Cost = ClientHelper::ClientSold($Client->id, '', '') * $Client->per_sold_value;
									}
									else
									{
										$Cost = 0; 
									}
									
									$TotalCost = $TotalCost + ($Cost);
									
									$TotalProfit = $TotalProfit + (ClientHelper::ClientProfit($Client->id, '', ''));
								}
								
								if($TotalCost == 0 || $TotalProfit == 0)
								{
									$TotalROI = 0;
								}
								else
								{
									$TotalROI = ($TotalProfit / $TotalCost) * 100;
								}
							?>
							<div class="wrapp">       
								<div class="title">LD's</div>         
								<div class="details TotalLDs">{{ number_format($TotalLDs,0) }}</div>       
							</div> 
							<div class="wrapp">       
								<div class="title">+/-</div>       
								<div class="details LeadPlusMinus">{{ number_format($LeadPlusMinus,0) }}</div>       
							</div> 
							<div class="wrapp">       
								<div class="title">Cost</div>       
								<div class="details TotalCost">${{ number_format($TotalCost,2) }}</div>       
							</div> 
							<div class="wrapp">       
								<div class="title">Profit</div>       
								<div class="details TotalProfit">${{ number_format($TotalProfit,2) }}</div>       
							</div> 
							<div class="wrapp">       
								<div class="title">ROI</div>       
								<div class="details TotalROI">{{ number_format($TotalROI,2) }}%</div>       
							</div> 
						</div>
					</div>
					
					<table id="table5" data-toolbar="#toolbar1" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20, 40, ALL]" data-show-footer="false" data-toggle="table" data-side-pagination="server" data-query-params="queryParams1" data-url="{{ route('clients-roi') }}">
						<thead>
							<tr>
								<th data-sortable="true" data-field="Date" @if(in_array("0",$ClientROIColumns)) data-visible="false" @endif>Date</th>
								<th data-sortable="true" data-field="Client" @if(in_array("1",$ClientROIColumns)) data-visible="false" @endif>Client</th>
								<th data-sortable="true" data-field="Package" @if(in_array("2",$ClientROIColumns)) data-visible="false" @endif>Package</th>
								<th data-sortable="true" data-halign="left" data-align="right" data-field="LD's" @if(in_array("3",$ClientROIColumns)) data-visible="false" @endif>LD's</th>
								<th data-sortable="true" data-halign="left" data-align="right" data-field="+/-" @if(in_array("4",$ClientROIColumns)) data-visible="false" @endif>+/-</th>
								<th data-sortable="true" data-halign="left" data-align="right" data-field="Cost" @if(in_array("5",$ClientROIColumns)) data-visible="false" @endif>Cost</th>
								<th data-sortable="true" data-halign="left" data-align="right" data-field="Profit" @if(in_array("6",$ClientROIColumns)) data-visible="false" @endif>Profit</th>
								<th data-sortable="true" data-halign="left" data-align="right" data-field="ROI" @if(in_array("7",$ClientROIColumns)) data-visible="false" @endif>ROI</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	@endif
	
	<!-- sold-modal -->
	<div id="sold_modal" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header modal_heading_bg">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Sold</h4>
				</div>
				<form action="{{ route('sold_lead_edit') }}" class="form-horizontal" role="form" method="post" id="lead-sold-validation">
					<div class="modal-body">
						<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="sold_lead_id" id="sold_lead_id">
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="sold_date">Sold Date</label>
								<div class="input-group" style="width: 100%;">
									<div class="input-group-addon">
										<i class="fa fa-fw fa-calendar"></i>
									</div>
									<input type="text" name="sold_date" id="sold_date" placeholder="MM-DD-YYYY" class="form-control" style="background-color: #FFF">
								</div>
							</div>
							<div class="col-md-6">
								<label  for="stock">Stock#</label>
								<div class="input-group" style="width: 100%;">
									<span class="input-group-addon">
										<i class="fa fa-fw fa-file-text-o"></i>
									</span>
									<input type="text" name="stock" id="stock" placeholder="Stock#" maxlength="10" class="form-control">
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-4">
								<label for="sold_year">Year</label>
								@php  $SoldYear = date('Y');  @endphp
								@if(date('n') >= 4)
								@php  $SoldYear++;  @endphp
								@endif
								<div class="input-group" style="width: 100%;">
									<select name="sold_year" id="sold_year" placeholder="Select Year" class="form-control">
										<option value="">Select Year</option>
										@for($Y=$SoldYear; $Y>=1990; $Y--)
										<option value="{{ $Y }}">{{ $Y }}</option>
										@endfor
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<label  for="group_sub_category_id">Make</label>
								<div class="input-group" style="width: 100%;">
									<select name="group_sub_category_id" id="group_sub_category_id" placeholder="Select Make" class="form-control">
										<option value="">Select Make</option>
										@foreach($AutomotiveSubCategories as $AutomotiveSubCategory)
										<option value="{{ $AutomotiveSubCategory->id }}">{{ $AutomotiveSubCategory->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<label  for="model">Model</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="model" id="model" placeholder="Model" class="form-control">
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="total_profit">Total Profit $</label>
								<div class="input-group" style="width: 100%;">
									<div class="input-group-addon">
										<i class="fa fa-fw fa-usd"></i>
									</div>
									<input type="text" name="total_profit" id="total_profit" placeholder="Total Profit" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
									<input type="hidden" name="profit_total" id="profit_total" placeholder="Total Profit" class="form-control" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
									<input type="hidden" name="roi_automation" id="roi_automation">
								</div>
							</div>
							<div class="col-md-6">
								<label for="real_buyer_name">Co-Buyer</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="real_buyer_name" id="real_buyer_name" placeholder="Real Buyer Name" class="form-control" style=" width:70%; background-color:#FFF;display:none" readonly="readonly"> 
									<button type="button" class="btn btn-info add_buyer" title="Add Co-Buyer" style="margin-left: 10px;">
										<i class="fa fa-plus" aria-hidden="true"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-default" style=" color:#fff; background-color: #ffbe18!important;
						border-color: #e4a70c !important;"> Submit</button>
						<button type="reset" class="btn btn-default form_reset">Reset</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- #/sold-modal -->
	
	<div id="add_real_buyer" class="modal fade animated" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background: linear-gradient(154deg, #00c7be 0, #007091 50%, #ffc902 100%)!important;">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Add Co-Buyer</h4>
				</div>
				<div class="modal-body">
					<div class="row m-t-10">
						<div class="col-md-12">
							<div class="form-group">
								<label class="sr-only" for="buyer_name">Name</label>
								<input type="text" name="buyer_name" id="buyer_name" placeholder="Name" class="form-control m-t-10">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success add_real_buyer">Add</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</section>
@stop
{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/lead_sold_validation.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<script>
	$(document).ready(function() {	
		$('#table5').bootstrapTable();
		
		@if(Session::get('user_category')!='client' && ManagerHelper::ManagerCategory()!='client')
		$('.TotalROI').html('{{ number_format($TotalROI,2) }}%');
		@endif
		
		$('#sold_modal').on('hidden.bs.modal', function () {
            $('#lead-sold-validation').bootstrapValidator('resetForm', true);
		});
		
		$('#sold_date').dateRangePicker({
			singleDate: true,
			showShortcuts: false,
			singleMonth: true,
			format: 'MM-DD-YYYY'
		});
		
		$('.date-picker-wrapper').on('click', function(e) {
			$('#lead-sold-validation').bootstrapValidator('revalidateField', 'sold_date');
		});
		
		var extra_features = '<button class="btn btn-default" type="button" name="mail" title="Mail" ><i class="fa fa-fw fa-envelope"></i></button><button class="btn btn-default" type="button" name="print" title="Print" ><i class="fa fa-fw fa-print"></i></button>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
	});
</script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<!-- bootstrap time picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/timedropper/js/timedropper.js')}}"></script>
<script>
	$(document).ready(function(){
		$('#client_profit_section .dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Client Profit Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$('#client_roi_section .dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Client ROI Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$(".add_buyer").click(function() {
			$('#buyer_name').val('');
			$('#add_real_buyer').modal('show');
		});
		
		$(".add_real_buyer").click(function() {
			if($('#buyer_name').val()!='')
			{
				$('#real_buyer_name').val($('#buyer_name').val());
				$('#real_buyer_name').css('display','block');
			}
			else
			{
				$('#real_buyer_name').val('');
				$('#real_buyer_name').css('display','none');
			}
			$('#add_real_buyer').modal('hide');
		});
		
		$('.button-select').click(function () {
			$('#date-loader').show();
			$('#table4').bootstrapTable('refresh', {
				query: {
					dateRange: $('#date-range-header').val(),
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('#client_profit_section .fixed-table-toolbar .search input[type=text]').val()
				}
			});
			
			$('#table5').bootstrapTable('refresh', {
				query: {
					dateRange: $('#date-range-header').val(),
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('#client_roi_section .fixed-table-toolbar .search input[type=text]').val()
				}
			});
			
			$.get("{{ route('reports-ajax') }}",{dateRange: $('#date-range-header').val(), client_id: $(".client-list ul li.active").attr("user_client_id"), search: $('#client_profit_section .fixed-table-toolbar .search input[type=text]').val(), search1: $('#client_roi_section .fixed-table-toolbar .search input[type=text]').val()},function(data){ 
				$('.TotalLDs').html(data.TotalLDs);
				$('.LeadPlusMinus').html(data.LeadPlusMinus);
				$('.TotalCost').html(data.TotalCost);
				$('.TotalProfit').html(data.TotalProfit);
				$('.TotalROI').html(data.TotalROI);
				$('.SoldTotals').html(data.SoldTotals);
				$('.SoldTotalProfits').html(data.SoldTotalProfits);
				$('#date-loader').hide(); 
			});
		});
		
		$('.button-clear').click(function () {
			$('#date-loader').show();
			$('#table4').bootstrapTable('refresh', {
				query: {
					dateRange: '',
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('#client_profit_section .fixed-table-toolbar .search input[type=text]').val()
				}
			});
			
			$('#table5').bootstrapTable('refresh', {
				query: {
					dateRange: '',
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('#client_roi_section .fixed-table-toolbar .search input[type=text]').val()
				}
			});
			
			$.get("{{ route('reports-ajax') }}",{dateRange: $('#date-range-header').val(), client_id: $(".client-list ul li.active").attr("user_client_id"), search: $('#client_profit_section .fixed-table-toolbar .search input[type=text]').val(), search1: $('#client_roi_section .fixed-table-toolbar .search input[type=text]').val()},function(data){ 
				$('.TotalLDs').html(data.TotalLDs);
				$('.LeadPlusMinus').html(data.LeadPlusMinus);
				$('.TotalCost').html(data.TotalCost);
				$('.TotalProfit').html(data.TotalProfit);
				$('.TotalROI').html(data.TotalROI);
				$('.SoldTotals').html(data.SoldTotals);
				$('.SoldTotalProfits').html(data.SoldTotalProfits);
				$('#date-loader').hide(); 
			});
		});
		
		$(".reset_lead").click(function () {
			$('#date-loader').show();
			$('.reset_lead').addClass('btn-primary');
			$('.reset_lead').removeClass('btn-warning');
			$('#profit_title').html('Client Profit');
			$(".client-list ul li").removeClass("active");

			$('#client_profit_section .fixed-table-toolbar .search input[type=text]').val('');
			$('#table4').bootstrapTable('refresh', {
				query: {
					dateRange: $('#date-range-header').val(),
					client_id: '',
					search: ''
				}
			});
			
			$('#table5').bootstrapTable('refresh', {
				query: {
					dateRange: $('#date-range-header').val(),
					client_id: '',
					search: ''
				}
			});
			
			$.get("{{ route('reports-ajax') }}",{dateRange: $('#date-range-header').val(), client_id: $(".client-list ul li.active").attr("user_client_id"), search: $('#client_profit_section .fixed-table-toolbar .search input[type=text]').val(), search1: $('#client_roi_section .fixed-table-toolbar .search input[type=text]').val()},function(data){ 
				$('.TotalLDs').html(data.TotalLDs);
				$('.LeadPlusMinus').html(data.LeadPlusMinus);
				$('.TotalCost').html(data.TotalCost);
				$('.TotalProfit').html(data.TotalProfit);
				$('.TotalROI').html(data.TotalROI);
				$('.SoldTotals').html(data.SoldTotals);
				$('.SoldTotalProfits').html(data.SoldTotalProfits);
				$('#date-loader').hide(); 
			});
		});
		
		$(".client-list ul li").click(function() {
			$('#date-loader').show();
			var client_id = $(this).attr('user_client_id');
			if(client_id==undefined)
			{
				$('#profit_title').html('Client Profit');
				$('.reset_lead').addClass('btn-primary');
				$('.reset_lead').removeClass('btn-warning');
			}
			else
			{
				$.get("{{ route('get_client_name') }}",{client_id: client_id},function(data){ 
					$('#profit_title').html(data+' Profit');
				});
				$('.reset_lead').addClass('btn-warning');
				$('.reset_lead').removeClass('btn-primary');
			}
			
			$('#table4').bootstrapTable('refresh', {
				query: {
					dateRange: $('#date-range-header').val(),
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('#client_profit_section .fixed-table-toolbar .search input[type=text]').val()
				}
			});
			
			$('#table5').bootstrapTable('refresh', {
				query: {
					dateRange: $('#date-range-header').val(),
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('#client_roi_section .fixed-table-toolbar .search input[type=text]').val()
				}
			});
			
			$.get("{{ route('reports-ajax') }}",{dateRange: $('#date-range-header').val(), client_id: $(".client-list ul li.active").attr("user_client_id"), search: $('#client_profit_section .fixed-table-toolbar .search input[type=text]').val(), search1: $('#client_roi_section .fixed-table-toolbar .search input[type=text]').val()},function(data){ 
				$('.TotalLDs').html(data.TotalLDs);
				$('.LeadPlusMinus').html(data.LeadPlusMinus);
				$('.TotalCost').html(data.TotalCost);
				$('.TotalProfit').html(data.TotalProfit);
				$('.TotalROI').html(data.TotalROI);
				$('.SoldTotals').html(data.SoldTotals);
				$('.SoldTotalProfits').html(data.SoldTotalProfits);
				$('#date-loader').hide(); 
			});
		});
		
		@if(Session::get('client_id') > 0)
			$.get("{{ route('reports-ajax') }}",{dateRange: $('#date-range-header').val(), client_id: {{ Session::get('client_id') }}, search: $('#client_profit_section .fixed-table-toolbar .search input[type=text]').val(), search1: $('#client_roi_section .fixed-table-toolbar .search input[type=text]').val()},function(data){ 
				$('.TotalLDs').html(data.TotalLDs);
				$('.LeadPlusMinus').html(data.LeadPlusMinus);
				$('.TotalCost').html(data.TotalCost);
				$('.TotalProfit').html(data.TotalProfit);
				$('.TotalROI').html(data.TotalROI);
				$('.SoldTotals').html(data.SoldTotals);
				$('.SoldTotalProfits').html(data.SoldTotalProfits);
				$('#date-loader').hide(); 
			});
			@endif
	});	
</script>
<script>
	function queryParams(params) {
		params.search = $('#client_profit_section .fixed-table-toolbar .search input[type=text]').val();
		params.dateRange = $('#date-range-header').val();
		params.client_id = $(".client-list ul li.active").attr("user_client_id");
		// console.log(JSON.stringify(params));
		// {"limit":10,"offset":0,"order":"asc","your_param1":1,"your_param2":2}
		return params;
	}
	
	function queryParams1(params) {
		params.search = $('#client_roi_section .fixed-table-toolbar .search input[type=text]').val();
		params.dateRange = $('#date-range-header').val();
		params.client_id = $(".client-list ul li.active").attr("user_client_id");
		// console.log(JSON.stringify(params));
		// {"limit":10,"offset":0,"order":"asc","your_param1":1,"your_param2":2}
		return params;
	}
	
	function Sold(sold_lead_id,client_id)
	{
		$('.form_reset').click();
		$('#sold_lead_id').val(sold_lead_id);
		$.get("{{ route('sold_client_roi_status') }}",{client_id: client_id},function(data){ 
			if(data=='Yes')
			{
				$('#roi_automation').val(data);
				$('#profit_total').attr('type','text');
				$('#profit_total').val('');
				$('#total_profit').attr('type','hidden');
				$('#total_profit').val('0');
				$('.help-block').css('display','none');
			}
			else
			{
				$('#roi_automation').val(data);
				$('#total_profit').attr('type','text');
				$('#total_profit').val('');
				$('#profit_total').attr('type','hidden');
				$('#profit_total').val('0');
				$('.help-block').css('display','none');
			}
		});
		
		$.get("{{ route('sold_lead_edit') }}",{sold_lead_id: sold_lead_id},function(data){ 
			if(data.length==1)
			{
				var edit_sold_date = data[0].sold_date;
				var result = edit_sold_date.split('-');
				$('#sold_modal').modal("show");
				$('#sold_date').val(result[1]+'-'+result[2]+'-'+result[0]);
				$('#stock').val(data[0].stock);
				$('#sold_year').val(data[0].sold_year);
				$('#group_sub_category_id').val(data[0].group_sub_category_id);
				$('#model').val(data[0].model);
				$('#total_profit').val(data[0].total_profit);
				$('#profit_total').val(data[0].total_profit);
				if(data[0].real_buyer_name==null)
				{
					$('#real_buyer_name').val('');
					$('#real_buyer_name').css('display','none');
				}
				else
				{
					$('#real_buyer_name').val(data[0].real_buyer_name);
					$('#real_buyer_name').css('display','block');
				}
			}
		});
	}
</script>
<!-- end of page level js -->
@stop