@extends('layouts/default')

{{-- Page title --}}
@section('title')
Dispute Leads
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	
	.export.btn-group {*display:none!important;}
	
	#EditLeadManaulModal {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#EditLeadManaulModal .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	.fixed-table-toolbar {*width:60%;}
	.fixed-table-toolbar .search {width: Calc( 100% - 110px );}
	
	@media screen and (max-width: 767px) {
	.fixed-table-toolbar {width: 100%;}
	.fixed-table-toolbar {width:100%;}
	.fixed-table-toolbar .search {width: Calc( 100% );}
	}
	
	.fixed-table-container {height: 500px !important; }
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
        <li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active"> Dispute Leads</li>
	</ol>
</section>

<!-- Main content -->
<section class="content p-l-r-15">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Dispute Lead List
					</h3>
				</div>
				<div class="panel-body">
					<table id="DisputeLeadTable" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-page-list="[10, 20, 40, ALL]" data-show-footer="false">
						<thead>
							<tr>
								<th data-field="#" data-sortable="true" @if(in_array("0",$Columns)) data-visible="false" @endif>#</th>
								<th data-field="Created Time" data-sortable="true" @if(in_array("1",$Columns)) data-visible="false" @endif>Created Time</th>
								<th data-field="Campaign" data-sortable="true" @if(in_array("2",$Columns)) data-visible="false" @endif>Campaign</th>
								<th data-field="Ad Set Name" data-sortable="true" @if(in_array("3",$Columns)) data-visible="false" @endif>Ad Set Name</th>
								<th data-field="Ad Name" data-sortable="true" @if(in_array("4",$Columns)) data-visible="false" @endif>Ad Name</th>
								<th data-field="Full Name" data-sortable="true" @if(in_array("5",$Columns)) data-visible="false" @endif>Full Name</th>
								<th data-field="Type" data-sortable="true" @if(in_array("6",$Columns)) data-visible="false" @endif>Type</th>
								<th data-field="Email" data-sortable="true" @if(in_array("7",$Columns)) data-visible="false" @endif>Email</th>
								<th data-field="Phone #" data-sortable="true" @if(in_array("8",$Columns)) data-visible="false" @endif>Phone #</th>
								<th data-field="City" data-sortable="true" @if(in_array("9",$Columns)) data-visible="false" @endif>City</th>
								<th data-field="State" data-sortable="true" @if(in_array("10",$Columns)) data-visible="false" @endif>State</th>
								<th data-field="Zip" data-sortable="true" @if(in_array("11",$Columns)) data-visible="false" @endif>Zip</th>
								<th data-field="Action" data-sortable="true" @if(in_array("12",$Columns)) data-visible="false" @endif>Action</th>
								</tr>
						</thead>
						<tbody>
							@php $i=1; @endphp
							@foreach($Leads as $Lead)
							<tr>
								<td>{{ $i++ }}</td>
								<td>{{ date('m-d-Y h:i A', strtotime($Lead->created_time)) }}</td>
								<td>{{ $Lead->campaign_name }}</td>
								<td>{{ $Lead->adset_name }}</td>
								<td>{{ $Lead->ad_name }}</td>
								<td>{{ title_case($Lead->full_name) }}</td>
								<td>{{ $Lead->lead_type }}</td>
								<td>{{ $Lead->email }}</td>
								<td>{{ $Lead->phone_number }}</td>
								<td>{{ title_case($Lead->city) }}</td>
								<td>{{ $Lead->state }}</td>
								<td>{{ $Lead->post_code }}</td>
								<td style="text-align: center">
									<i class="fa fa-pencil text-primary actions_icon" onclick="EditLead('{{ $Lead->id }}');" title="Edit"></i>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<!-- EditLeadManaulModal -->
	<div id="EditLeadManaulModal" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header modal_heading_bg">
					<button type="button" class="close" data-dismiss="modal">×</button>
					<h4 class="modal-title">Edit Lead</h4>
				</div>
				<form class="form-horizontal" role="form" id="manual_lead_edit">
					<div class="modal-body form-body">
						<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="edit_facebook_ads_lead_user_id" id="edit_facebook_ads_lead_user_id">
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="edit_first_name">First Name</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="first_name" id="edit_first_name" placeholder="First Name" class="form-control">
									<small class="text-danger animated edit_first_name fadeInUp manual_lead_edit"></small>
								</div>
							</div>
							<div class="col-md-6">
								<label for="edit_last_name">Last Name</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="last_name" id="edit_last_name" placeholder="Last Name" class="form-control">
									<small class="text-danger animated edit_last_name fadeInUp manual_lead_edit"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="edit_phone">Phone #</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="phone" id="edit_phone" placeholder="Phone #" class="form-control">
									<small class="text-danger animated edit_phone fadeInUp manual_lead_edit"></small>
								</div>
							</div>
							<div class="col-md-6">
								<label for="edit_email">Email</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="email" id="edit_email" placeholder="Email" class="form-control">
									<small class="text-danger animated edit_email fadeInUp manual_lead_edit"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="edit_full_address">Full Address</label>
								<div class="input-group" style="width: 100%;">
									<textarea name="full_address" id="edit_full_address" placeholder="Full Address" rows="2" class="form-control"></textarea>
									<small class="text-danger animated edit_full_address fadeInUp manual_lead_edit"></small>
								</div>
							</div>
							<div class="col-md-6">
								<label for="edit_zip">Zip</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="zip" id="edit_zip" placeholder="Zip" class="form-control" />                                    
									<small class="text-danger animated edit_zip fadeInUp manual_lead_edit"></small>
								</div>
							</div>
						</div>
						<div class="row m-t-10">
							<div class="col-md-6">
								<label for="edit_city">City</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="city" id="edit_city" placeholder="City" class="form-control">
									<small class="text-danger animated edit_city fadeInUp manual_lead_edit"></small>
								</div>
							</div>
							<div class="col-md-6">
								<label for="edit_state">State</label>
								<div class="input-group" style="width: 100%;">
									<input type="text" name="state" id="edit_state" placeholder="State" class="form-control">
									<small class="text-danger animated edit_state fadeInUp manual_lead_edit"></small>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-default editButton" style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c !important;"> Submit</button>
						<button type="reset" class="btn btn-default manual_lead_add_clear">Reset</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- #/EditLeadManaulModal -->
</section>
<!-- /.content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>

<script>
	$(document).ready(function(){
		$('#DisputeLeadTable').bootstrapTable();
		
		$('.dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Dispute Lead List Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$("#manual_lead_edit").on("submit", function( event ) {
			event.preventDefault();
			
			$(".editButton").prop('disabled', true);
			
			$('.manual_lead_edit').html('');
			var data=$(this).serialize();
			
			$.ajax({
				url: "{{ route('dispute-lead-edit') }}",
				type:'POST',
				data: data,
				success: function(data) {
					$(".editButton").prop('disabled', false);
					$('#EditLeadManaulModal').modal('hide');
					swal('Success!', 'Lead updated successfully.', 'success');
					window.location.reload(true);
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$(".edit_"+key).html(value);
					});
					$(".editButton").prop('disabled', false);
				}       
			});
		});
	});
	
	function EditLead(id)
	{
		$('.manual_lead_edit').html('');
		$.get("{{ route('lead_edit') }}",{id: id},function(data){ 
			$('#edit_facebook_ads_lead_user_id').val(id);
			if(data.first_name == '')
			{
				$('#edit_first_name').val(data.full_name);
			}
			else
			{
				$('#edit_first_name').val(data.first_name);
			}
			
			$('#edit_last_name').val(data.last_name);
			$('#edit_phone').val(data.phone_number);
			$('#edit_email').val(data.email);
			$('#edit_full_address').val(data.street_address);
			$('#edit_zip').val(data.post_code);
			$('#edit_city').val(data.city);
			$('#edit_state').val(data.state);
		});
		$(".editButton").prop('disabled', false);
		$('#EditLeadManaulModal').modal("show");
	}
   $("#edit_zip").inputmask('99999');
</script>
@stop