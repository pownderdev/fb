@extends('layouts/default')

{{-- Page title --}}
@section('title')
Appointments
@parent
@stop
{{-- page level styles --}} 
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/fullcalendar_new/css/fullcalendar.css')}}"/>
<link rel="stylesheet" media='print' type="text/css" href="{{asset('assets/vendors/fullcalendar_new/css/fullcalendar.print.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/iCheck/css/all.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/calendar_custom.css')}}"/>
<link href="http://big.pownder.com/assets/date_time/bootstrap-datetimepicker.css" rel="stylesheet">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}" />
<style>
    .text-black { color:#000 !important;}
	.ui-autocomplete { z-index: 99999; overflow-y: scroll; height:250px }
	.ui-menu-item-wrapper { padding:5px 0px; margin:10px 0px; }
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content') 

<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{route('/')}}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		
		<li class="active">
			Appointments
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		
		<div class="col-md-9">
			<div class="box">
				<div class="box-body">
					<div id="calendar"></div>
				</div>
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
		
		<div class="col-md-3">
			<div class="box">
				<div class="box-title">
					<h3>Appointment With</h3>
					<div class="pull-right box-toolbar">
						<a href="#" class="btn btn-link btn-xs" data-toggle="modal" data-target="#myModal">
							<i class="fa fa-plus"></i>
						</a>
					</div>
				</div>
				<div class="box-body">
					<div id='external-events'>
						@foreach($Events as $Event)
						<div class='external-event fc-event' data-event="{{$Event->id}}" data-facebook="{{$Event->facebook_ads_lead_user_id}}" data-url="{{$Event->event_url}}" style="background-color:{{$Event->event_type}}; border-color:{{$Event->event_type}}">{{ $Event->event_name }}</div>
						@endforeach
						<p class="well no-border no-radius">
							<input type='checkbox' class="custom_icheck" id='drop-remove'/>
							<label for='drop-remove'>remove after drop</label>
						</p>
					</div>
				</div>
				<div class="box-footer">
					<a href="#" class="btn btn-success btn-block" data-toggle="modal" data-target="#myModal">Create Appointment</a>
				</div>
			</div>
			<!-- /.box -->
		</div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<form method="post" id="addEventForm">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-content">
                    <div class="modal-header gred_2">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">
                            <i class="fa fa-plus"></i> Create Appointment
						</h4>
					</div>
                    
                    <div class="modal-body">
						<div class="form-group">
							<label for="new-event">Appointment With</label>
							<div class="input-group">
								<div class="ui-widget">
									<input type="text" id="new-event" name="event" class="form-control" placeholder="Appointment With">
								</div>
								<input type="hidden" id="event_type" name="event_type" value="#4FC1E9">
								<input type="hidden" id="facebook_ads_lead_user_id" name="facebook_ads_lead_user_id" value="">
								<div class="input-group-btn">
									<button type="button" id="color-chooser-btn" name="event_type" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
										Select
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu pull-right" id="color-chooser">
										<li>
											<a class="palette-primary" href="#">Primary</a>
										</li>
										<li>
											<a class="palette-success" href="#">Success</a>
										</li>
										<li>
											<a class="palette-info" href="#">Info</a>
										</li>
										<li>
											<a class="palette-warning" href="#">warning</a>
										</li>
										<li>
											<a class="palette-danger" href="#">Danger</a>
										</li>
										<li>
											<a class="palette-default" href="#">Default</a>
										</li>
									</ul>
								</div>
								<!-- /btn-group -->
							</div>
							<small class="text-danger animated event fadeInUp add-event"></small>
						</div>   
                        <!-- /input-group -->
                        <div class="form-group">
							<label for="event_url">Appointment URL</label>  
							<div class="input-group"> 
								<span class="input-group-addon text-black">http://</span>                        
								<input type="url" class="form-control pull-right" id="event_url" name="url"  placeholder="Enter The URL related to appointment (optional)"/>                       
							</div>
						</div>
                        
                        <!--<div class="form-group">
							<label for="date-range0">Date and Time</label>
							<div class='input-group date' id='datetimepicker1'>
							<input type='text' class="form-control" id="time" name="time" placeholder="Select Date and time For Event" />
							<span class="input-group-addon">
							<span class="fa fa-calendar text-black"></span>
							</span>
							</div>
							<small class="text-danger animated time fadeInUp add-event"></small>
						</div>-->
					</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-right" id="close_calendar_event" data-dismiss="modal">
                            Close
                            <i class="fa fa-times"></i>
						</button>
                        <button type="submit" class="btn btn-success pull-left" id="add-new-event">
                            <i class="fa fa-plus"></i> Add
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<!-- /.content -->

<div id="calendarModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gred_2">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
				<h4 class="modal-title">Appointment Details</h4>
			</div>
			<div class="modal-body"> 
				<table class="table table-bordered">
					<tr>
						<td>Name</td>
						<th id="lead_name"></th>
					</tr>
					<tr>
						<td>Client's Name</td>
						<th id="client_name"></th>
					</tr>
					<tr>
						<td>Appointment DateTime</td>
						<th id="appt_datetime"></th>
					</tr>
					<tr>
						<td>Url</td>
						<th id="appt_url"></th>
					</tr>
					<tr>
						<td>Email</td>
						<th id="appt_email"></th>
					</tr>
					<tr>
						<td>Phone #</td>
						<th id="appt_phone"></th>
					</tr>
					<tr>
						<td>City</td>
						<th id="appt_city"></th>
					</tr>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@stop

{{-- page level scripts --}} 
@section('footer_scripts')
<!-- begining of page level js -->
<script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/fullcalendar_new/js/fullcalendar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/iCheck/js/icheck.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/date_time/bootstrap-datetimepicker.js')}}"></script>

<script type="text/javascript">
    $(function () {
		/* initialize the external events
		-----------------------------------------------------------------*/
		
		$('#external-events .fc-event').each(function() {
			
			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});
			
			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});
			
		});
		
		//Date for the calendar events (dummy data)
		$('#calendar').fullCalendar(
		{
			header: {left: 'title', center: '', right: 'prevYear,year,nextYear  prev,today,next  month,agendaWeek,agendaDay'},
			defaultTimedEventDuration: '01:00:00',
			defaultView: "agendaWeek",
			buttonText: {prev: "Prev", next: "Next", today: 'Today', month: 'Month', week: 'Week', year:'Year', day: 'Day'},
			buttonIcons: {prev: "left-double-arrow", next: "right-double-arrow", prevYear: "left-double-arrow", nextYear: "right-double-arrow"},
			
			//Random events
			@php $i=0; @endphp
			events: [
			@foreach($Appointments as $Appointment)
			@if(count($Appointments) == $i)
			{title: '{{ $Appointment->full_name }}', start: '{{ $Appointment->appointment_date}}T{{ date("H:i:s",strtotime($Appointment->appointment_time))}}', backgroundColor: '{{ $Appointment->backgroundColor }}', borderColor: '{{ $Appointment->borderColor }}', id: '{{ $Appointment->id }}', className: "appointmentHide"}
			@else
			{title: '{{ $Appointment->full_name }}', start: '{{ $Appointment->appointment_date}}T{{ date("H:i:s",strtotime($Appointment->appointment_time))}}', backgroundColor: '{{ $Appointment->backgroundColor }}', borderColor: '{{ $Appointment->borderColor }}', id: '{{ $Appointment->id }}', className: "appointmentHide"},
			@endif
			@php $i++; @endphp
			@endforeach
			],
			
			eventClick: function(event) {
				//alert(event.id);
				if(event.id != '' && event.id != undefined)
				{$('#date-loader').show();
					$.get("{{ route('lead-details') }}",{id: event.id},function(data){ 
						$('#lead_name').html(data.full_name);
						$('#client_name').html(data.client_name);
						$('#appt_datetime').html(data.appointment_date+' '+data.appointment_time);
						if(data.appointment_url == null)
						{
							$('#appt_url').html('');
						}
						else
						{
							$('#appt_url').html('<a href="'+data.appointment_url+'" target="_blank">'+data.appointment_url+'</a>');
						}
						$('#appt_email').html(data.email);
						$('#appt_phone').html(data.phone_number);
						$('#appt_city').html(data.city);
						$('#date-loader').hide();
						$('#calendarModal').modal();
					});
				}
			}, 	
			
			editable: true,
			droppable: true,
			navLinks: true,
			// dragRevertDuration: 0,
			eventDrop : function( event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view ) 
            {
				$('.fc-time-grid-event').hide();
				$('.appointmentHide').show();
				var date = dayDelta+' ';
				var facebook_ads_lead_user_id = event.id;
				if(event.id != '' && event.id != undefined)
				{
					$.get("{{ route('update-appointment') }}",{facebook_ads_lead_user_id: facebook_ads_lead_user_id.toString(), dayDelta:date.toString()},function(data){
						//After Success
					});
				}
			},
			
			// this allows things to be dropped onto the calendar !!!
			drop: function(date, allDay)
			{ 
				// this function is called when something is dropped
				// retrieve the dropped element's stored Event Object
				var originalEventObject = $(this).data('eventObject');
				// we need to copy it, so that multiple events don't have a reference to the same object
				var copiedEventObject = $.extend(
				{
				}, originalEventObject);
				// assign it the date that was reported
				copiedEventObject.start = date;
				copiedEventObject.id = $(this).data("facebook").toString();
				copiedEventObject.title = $(this).html().toString();
				copiedEventObject.allDay = allDay;
				copiedEventObject.backgroundColor = $(this).css("background-color");
				copiedEventObject.borderColor = $(this).css("border-color");
				copiedEventObject.className = "appointmentHide";
				// render the event on the calendar 
				//alert(copiedEventObject.id);
				//alert(copiedEventObject.title);
				// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
				$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
				$(".badge1").text(parseInt($(".badge1").text()) + 1);  
				//alert(date.toString());
				$('.fc-time-grid-event').hide();
				$('.appointmentHide').show();
				//alert(date);
				if($(this).data("facebook") > 0){
					$.get("{{ route('event-appointment') }}", {url: $(this).data("url").toString(), facebook_ads_lead_user_id: $(this).data("facebook").toString(), borderColor: $(this).css("border-color"), backgroundColor: $(this).css("background-color"), datetime: date.toString()}, function (data) {
						//After Success
					});
				}
				
				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked'))
				{
					$.get("{{ route('event-remove') }}", {id: $(this).data("event")}, function (data) {
						// if so, remove the element from the "Draggable Events" list
						$(this).remove();
					});
				}
				//$(this).remove();
				
				var displayed = $('.fc-time-grid-event').filter(function() {
					var element = $(this);
					$('.fc-time-grid-event').css('margin-right', '0px');
					if(element.css('z-index') == '2') {
						element.remove();
						return false;
					}
					
					return true;
				});
			}
		}); /* ADDING EVENTS */
		
		var currColor = "#4FC1E9"; //default
		//Color chooser button
		var colorChooser = $("#color-chooser-btn");
		
		$("#color-chooser").find('li a').on('click', function(e)
		{
			e.preventDefault();
			//Save color
			currColor = $(this).css("background-color");
			//Add color effect to button
			colorChooser.css({"background-color": currColor, "border-color": currColor}).html($(this).text() + ' <span class="caret"></span>');
			$('#event_type').val(currColor);
		});
		
		$("#addEventForm").on("submit", function( e ) {
			e.preventDefault();
			$('.event').html('');
			
			//Get value and make sure it is not null
			var val = $("#new-event").val();
			if(val == '')
			{
				$('.event').html('The event title is required');
				return false;
			}
			
			if($("#facebook_ads_lead_user_id").val() != '')
			{
				var facebook_ads_lead_user_id = $("#facebook_ads_lead_user_id").val();
				var url = $("#event_url").val();
				$.post("{{ route('event-add') }}", $(this).serialize(), function (data) {
					//After Success
					if(data.response != '')
					{
						//Create event
						var event = $("<div />");
						event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event fc-event");
						//event.data({"event": data.response, "facebook": facebook_ads_lead_user_id});
						event.attr('data-event', data.response);
						event.attr('data-facebook', facebook_ads_lead_user_id);
						event.attr('data-url', url);
						event.html(val);
						$('#external-events').prepend(event);
						//Add draggable funtionality
						
						/* initialize the external events
						-----------------------------------------------------------------*/
						
						$('#external-events .fc-event').each(function() {
							
							// store data so the calendar knows to render an event upon drop
							$(this).data('event', {
								title: $.trim($(this).text()), // use the element's text as the event title
								stick: true // maintain when user navigates (see docs on the renderEvent method)
							});
							
							// make the event draggable using jQuery UI
							$(this).draggable({
								zIndex: 999,
								revert: true,      // will cause the event to go back to its
								revertDuration: 0  //  original position after the drag
							});
							
						});
					}
					
					//Remove event from text input
					$("#new-event,#event_url,#facebook_ads_lead_user_id").val("");
					$('#myModal').modal('hide');
				});
			}
			else
			{
				swal ( "Oops" ,  "Lead not exists!" ,  "error" );
			}
		}); 
		
		$('#myModal').on('hidden.bs.modal', function (e) {
			$('.event').html('');
			$("#new-event,#event_url,#facebook_ads_lead_user_id").val("");
		})
		
		$('#new-event').autocomplete({
			source:"{{ route('event-lead-search') }}",
			minlength:1,
			autoFocus:true,
			select: function (event, ui) {
				// Prevent value from being put in the input:
				this.value = ui.item.label;
				
				// Set the next input's value to the "value" of the item.
				$(this).next("input").val(ui.item.value);
				var id = ui.item.id;
				id = id.replace("'", "");
				id = id.replace("'", "");
				$("#facebook_ads_lead_user_id").val(id); // display the selected text
				
				event.preventDefault();
			}
		});
	});
</script>
<!-- end of page level js -->
@stop