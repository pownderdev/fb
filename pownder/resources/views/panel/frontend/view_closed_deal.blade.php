@extends('layouts/default')
 
{{-- Page title --}} 
@section('title') 
View Closed Deal @parent 
@stop 

{{-- page level styles --}} 
@section('header_styles')
<!--start page level css -->
<style>
.bg-grey
{
 background-color: #e8e8e8 !important;
}
.bg-green
{
 background-color: #00c3bc !important;
 color:#fff !important;
}
.hightlight
{
    font-weight: bold !important;
    font-size: 14px;
}
</style>
<!--end page level css-->
@stop 
{{-- Page content --}}
@section('content')
<?php 
function showErrorMsg($error)
{
    if(count($error)>0)
    {
        $msg='';
        foreach($error as $val)
        {
         $msg.='<small class="help-block animated fadeInUp text-danger" style="color: #FB8678;">'.$val.'</small>';    
        }
        return $msg;        
    }
}
//Money Format
 function money_format1($number)
{ 
          
          $number=str_replace(",","",$number);
          settype($number,"float");      
          setlocale(LC_MONETARY, 'en_US'); 
          $f_number=money_format('%!.2i',abs( $number ));     
          $f_number=($number<0) ? '-$'.$f_number : '$'.$f_number;
          return $f_number;
}
?>
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
			<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
        <li>
			<a href="{{ route('closed-deals') }}">
			 Closed Deals
			</a>
		</li>
		<li class="active">
		  View Closed Deal
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content p-l-r-15">
 
  @include('panel.includes.status') 
    
	<div class="row">
      <div class="col-lg-12">
		<div class="panel panel-success filterable">
          <div class="panel-heading">
             <h3 class="panel-title">
              <i class="fa fa-fw fa-th-large"></i> View Closed Deal
             </h3>             
          </div>
          
          <div class="panel-body">
			  
                <div class="row clearfix">
            	<div class="col-md-6 col-md-offset-0">            	 
            	  
                  <div class="table-responsive">
                                    
                      <table class="table table-bordered table-hover">
                      
                      <tbody>
                         
                         <tr>                         
                          <th class="active">Deal #</th>
                          <td>{{$data->col5}}</td>
                         </tr>
                         
                         <tr>                         
                          <th class="active">Dealership Name</th>
                          <td>{{$data->client_name}}</td>
                         </tr>
                         
                         <tr>                         
                          <th class="active">Group</th>
                          <td>{{$data->group_category}}</td>
                         </tr>
                      
                      </tbody>
                      
                      </table>
                  
                  </div>	                     
            	 
            	</div>
            </div>
            <div class="row clearfix">
            	<div class="col-md-12 col-md-offset-0">            	 
            	  
                  <div class="table-responsive">
                                    
                      <table class="table table-bordered table-hover">
                      
                      <tbody>
                         
                         <tr class="bg-grey">                         
                          <th>Customer Info</th>
                          <th>Source</th>
                          <th colspan="2">Call Tracking</th>
                         </tr>
                         <tr>
                         <td><i class="fa fa-user" title="Buyer Name"></i> {{$data->col7}}</td> 
                         <td>Source: {{$data->source}}</td>
                         <td>Incoming Calls	</td>
                         <td>0</td>                        
                         </tr>
                         <!--
                         <tr>                         
                         <td><b>Customer #</b> {{$data->col6}}</td>
                         </tr>
                         -->
                         <tr> 
                         <?php $city=$data->col15;$country=$data->col18; if($city==$country){$country='';}else{ $country=' , '.$country; } ?>                        
                         <td><i class="fa fa-home" title="Buyer Address"></i> {{$data->col13.' '.$data->col14.' , '.$city.' , '.$data->col16.$country.' , '.$data->col17}}</td>
                         <td>Date: </td>
                         <td>Outgoing Calls</td>
                         <td>0</td>
                         </tr>
                         <tr>
                         <td>
                          @if($data->col19) <i class="fa fa-phone" title="Home Phone"></i> {{$data->col19}} @endif
                          @if($data->col20) <i class="fa fa-mobile" title="Cell Phone"></i> {{$data->col20}} @endif
                          @if($data->col21) <i class="fa fa-phone" title="Work Phone"></i> {{$data->col21}} @endif
                         </td>
                         <td>Lead Status: </td>
                         <td>Total Duration	</td>
                         <td>00:00:00</td>                         
                         </tr>
                         
                         <tr>
                         <td><i class="fa fa-envelope" title="Email 1"></i> {{trim($data->col23.' , '. $data->col24.' , '.$data->col25,', ')}}</td>                         
                         <td></td>
                         <td>SMS</td>
                         <td>0</td>
                         </tr>
                         
                         <tr>
                         <td>Co-Buyer:	 {{$data->col36}}</td>
                         <td></td>
                         <td></td>
                         <td></td>                         
                         </tr>
                         
                         
                      </tbody>
                      
                      </table>
                  
                  </div>	                     
            	 
            	</div>
            </div>
            
            <div class="row clearfix">
            	<div class="col-md-12 col-md-offset-0">            	 
            	  
                  <div class="table-responsive">
                                    
                      <table class="table table-bordered table-hover">
                      
                      <tbody>
                         
                         <tr class="bg-grey">                         
                          <th colspan="2">Deal</th>
                          <th colspan="2">Vehicle</th>
                         </tr>
                         <tr>
                         <th class="bg-green">Sold Date</th> 
                         <td>{{$data->col153}}</td>
                         <th class="bg-green">Stock #</th> 
                         <td>{{$data->col70}}</td>
                         
                         </tr>
                         
                         <tr>
                         <th class="bg-green">Deal Category</th> 
                         <td>
                         <?php
                         switch($data->col158)
                         {
                            case "F": echo 'Retail '; $rate=$data->col164;  break;
                            case "R": echo 'Retail '; $rate=$data->col164;  break;
                            case "L" :echo 'Lease';   $rate=$data->col181;break;
                            case "W" :echo 'Wholesale';$rate='';break;
                            default:echo 'Other ('.$data->col158.')'; $rate=''; break;
                         }
                         ?>
                         </td>
                         <th class="bg-green">Vehicle</th> 
                         <td>{{$data->col69}}</td>                     
                         </tr>
                         
                         <tr>
                         <th class="bg-green"><i class="fa fa-usd" title="Sale Price"></i> Sale Price</th> 
                         <td>{{money_format1($data->col119)}}</td>
                         <th class="bg-green">Year Make Model</th> 
                         <td>{{$data->col62.' '.$data->col63.' '.$data->col64}}</td>
                                   
                         </tr>
                         
                         <tr>
                         <th class="bg-green"><i class="fa fa-usd" title="Down Payment"></i> Down Payment</th> 
                         <td>{{money_format1($data->col171)}}</td>
                         <th class="bg-green">VIN</th> 
                         <td>{{$data->col61}}</td>
                                
                         </tr>
                         
                         <tr>
                         <th class="bg-green"><i class="fa fa-usd" title="Down Payment"></i> Amount Financed</th>
                         <td>{{money_format1($data->col163)}}</td>
                         <th class="bg-green">Exterior Color</th> 
                         <td>{{$data->col68}}</td>
                                
                         </tr>
                         
                         <tr>
                         <th class="bg-green"><i class="fa fa-usd" title="Rebates"></i> Rebates</th> 
                         <td>{{money_format1($data->col167)}}</td>
                         <th class="bg-green">Rate</th> 
                         <td>@if($rate){{number_format($rate,2).'%'}}@endif</td>
                                           
                         </tr>
                         
                         <tr>
                         <th colspan="2" class="bg-grey">Profit:</th>
                         <th class="bg-green">Monthly Payment</th> 
                         <td>{{money_format1($data->col165)}}</td>     
                         </tr>
                         
                         <tr>
                         <th class="bg-green">Reserve</th>
                         <td>{{money_format1($data->col141)}}</td>   
                         <th class="bg-green">Term</th> 
                         <td>{{$data->col162}}</td>
                                
                         </tr>
                         
                         <tr>
                         <th class="bg-green">Gross Payable</th>
                         <td>{{money_format1($data->col148)}}</td>       
                         <th class="bg-green">Financial Institute</th> 
                         <td>{{$data->col160}}</td>
                                 
                         </tr>
                         
                         <tr>
                         <th class="bg-green">Front End Gross</th>
                         <td>{{money_format1($data->col135)}}</td>        
                         <th class="bg-green">Salesperson</th> 
                         <?php 
                         $sales1=$sales2=$sales3='';
                         if($data->col106)
                         {
                         $sales1=preg_split('/\s+/',$data->col106); $firstname1=implode(' ',array_slice($sales1,0,-1)); $lastname_F1=str_split(array_slice($sales1,-1,1)[0],1)[0]; 
                         $sales1= ucwords(strtolower($firstname1.' '.$lastname_F1));
                         }
                         if($data->col108)
                         {
                         $sales2=preg_split('/\s+/',$data->col108); $firstname2=implode(' ',array_slice($sales2,0,-1)); $lastname_F2=str_split(array_slice($sales2,-1,1)[0],1)[0]; 
                         $sales2= ucwords(strtolower($firstname2.' '.$lastname_F2));
                         }
                         if($data->col110)
                         {
                         $sales3=preg_split('/\s+/',$data->col110); $firstname3=implode(' ',array_slice($sales3,0,-1)); $lastname_F3=str_split(array_slice($sales3,-1,1)[0],1)[0]; 
                         $sales3= ucwords(strtolower($firstname3.' '.$lastname_F3));
                         }
                         ?>
                         <td>{{trim($sales1.' / '.$sales2.' / '.$sales3,' /')}}</td>
                                 
                         </tr>
                         
                         <tr>
                         <th class="bg-green">Back End Gross</th>
                         <td>{{money_format1($data->col145)}}</td>  
                         <th class="bg-green">F & I Manager</th> 
                         <td>{{$data->col114}}</td>
                         
                         </tr>
                         
                         <tr>
                         <?php $profit=money_format1($data->col146);if(strpos($profit,'-$')===false){$textcolor='text-success';}else{$textcolor='text-danger';} ?>
                         <th class="bg-green text-primary text-bold">Total Profit</th>
                         <td class="hightlight {{$textcolor}}">{{$profit}}</td>   
                         <th class="bg-green">Closing Manger</th> 
                         <td>{{$data->col112}}</td>
                         
                         </tr>
                         
                         
                         <tr>
                         <th class="bg-green"></th> 
                         <td></td>
                         <th class="bg-green">Sales Manager</th> 
                         <td>{{$data->col116}}</td>
                         </tr>
                         
                         
                         
                      </tbody>
                      
                      </table>
                  
                  </div>	                     
            	 
            	</div>
            </div>
            <br />
            
     </div>
        
        </div>
		
	  </div>	
	</div>
 
</section>


<!-- /.content -->
@stop 

{{-- page level scripts --}} 
@section('footer_scripts')

@stop