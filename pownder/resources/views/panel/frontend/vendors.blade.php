@extends('layouts/default')

{{-- Page title --}}
@section('title')
Vendors list
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datedropper/datedropper.css')}}">
<link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet" type="text/css"/>
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	.fixed-table-container {height: 500px !important; }
	.export.btn-group {*display:none!important;}
	
	@if($WritePermission=='Yes')
	.fixed-table-toolbar {*width:60%;}
	.fixed-table-toolbar .search {width: Calc( 100% - 427px );}
	@else
	.fixed-table-toolbar {*width:60%;}
	.fixed-table-toolbar .search {width: Calc( 100% - 300px );}
	@endif
	
	@media screen and (max-width: 767px) {
	.fixed-table-toolbar {width: 100%;}
	.fixed-table-toolbar {width:100%;}
	.fixed-table-toolbar .search {width: Calc( 100% );}
	}
	
	#filter_status {
    display: block;
    width: 100px;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.428571429;
    color: #555555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
	}
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">
			Vendors list
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content"> 
    @include('panel.includes.status')       
	<?php
		$errorarr=$errors;   
		function showerrormsg($key,$errors)
		{    
			$msg='';
			if(count($errors->get($key))>0)
			{ 
				$msg.='';
				foreach($errors->get($key) as $error)
				{
					if($key=="alert_success")
					{
						$msg.=$error;
					}
					else
					{
						$msg.='<small class="text-danger animated fadeInUp"  >'.$error.'</small>' ; 
					}
				}
			} 
			
			return $msg;     
		}
	?>
	<!--fourth table start-->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Vendors List
					</h3>
				</div>
				<div class="panel-body">
					<table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-query-params="queryParams" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20, 40, ALL]" data-show-footer="false" data-toggle="table" data-side-pagination="server" data-url="{{ route('vendor-list-ajax') }}">
						<thead>
                            <tr>
								<th data-field="" @if(in_array("0",$VendorColumns)) data-visible="false" @endif><input type="checkbox" class="checkall" /></th>
                                <th data-field="#" @if(in_array("1",$VendorColumns)) data-visible="false" @endif>#</th>
                                <th data-field="Date" data-sortable="true" @if(in_array("2",$VendorColumns)) data-visible="false" @endif>Date</th>
                                <th data-field="Status" data-sortable="true" @if(in_array("3",$VendorColumns)) data-visible="false" @endif>Status</th>
                                <th data-field="Vendor Name" data-sortable="true" @if(in_array("4",$VendorColumns)) data-visible="false" @endif>Vendor Name</th>
                                <th data-field="Package" data-sortable="true" @if(in_array("5",$VendorColumns)) data-visible="false" @endif>Package</th>
                                <th data-field="Clients" data-sortable="true" data-align="right" @if(in_array("6",$VendorColumns)) data-visible="false" @endif>Clients</th>
                                <th data-field="Ads" data-align="right" @if(in_array("7",$VendorColumns)) data-visible="false" @endif>Ads</th>
                                <th data-field="CP's" data-align="right" @if(in_array("8",$VendorColumns)) data-visible="false" @endif>CP's</th>
                                <th data-field="LD's" data-align="right" @if(in_array("9",$VendorColumns)) data-visible="false" @endif>LD's</th>
                                <th data-field="FB Lists" data-align="right" @if(in_array("10",$VendorColumns)) data-visible="false" @endif>FB Lists</th>
                                <th data-field="FB Messages" data-align="right" @if(in_array("11",$VendorColumns)) data-visible="false" @endif>FB Messages</th>
                                @if($WritePermission=='Yes')<th data-field="Login"  @if(in_array("12",$VendorColumns)) data-visible="false" @endif>Login</th>@endif
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!--fourth table end-->
</section>

<!-- Modals Section-->
<div id="add_vendor_modal" class="modal fade" role="dialog">
	<form action="{{ route('add-vendor-submit') }}" method="post" id="add_vendor" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" style="color:#fff;opacity: 0.8;">&times;</button>
					<h4 class="modal-title">Add Vendor</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="name">Name</label>
								<input value="{{ old('name') }}" type="text" class="form-control" id="name" name="name" placeholder="Name">
								<?php echo showerrormsg('name',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="address">Address</label>
								<input value="{{ old('address') }}" type="text" class="form-control" id="address" name="address" placeholder="Address">
								<?php echo showerrormsg('address',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="zip">Zip</label>
								<input value="{{ old('zip') }}" type="text" class="form-control" id="zip" name="zip" placeholder="Zip" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<?php echo showerrormsg('zip',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="mobile">Phone</label>
								<input value="{{ old('mobile') }}" type="text" class="form-control" id="mobile" name="mobile" placeholder="Phone" data-inputmask='"mask": "(999) 999-9999"' data-mask/>
								<?php echo showerrormsg('mobile',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12  form_field form-group">
								<label for="email">Email</label>
								<input value="{{ old('email') }}" type="email" class="form-control" id="email" name="email" placeholder="Email">
								<?php echo showerrormsg('email',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="url">URL</label>
								<input value="{{ old('url') }}" type="url" class="form-control" id="url" name="url" placeholder="Url">
								<?php echo showerrormsg('url',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="contact_name">Contact Name</label>
								<input value="{{ old('contact_name') }}" type="text" class="form-control" id="contact_name" name="contact_name" placeholder="Contact Name">
								<?php echo showerrormsg('contact_name',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="vendor_package_id">Package</label>
								<select id="vendor_package_id" name="vendor_package_id" class="form_select">
									<option value="">Select Package</option>
									@foreach($VendorPackages as $VendorPackage)
									<option value="{{ $VendorPackage->id }}" @if(old('vendor_package_id')==$VendorPackage->id) selected @endif>{{ $VendorPackage->name }}</option>
									@endforeach
								</select>
								<?php echo showerrormsg('vendor_package_id',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="start_date">Start Date</label>
								<input value="{{ date('m-d-Y') }}" id="start_date" name="start_date" type="text" class="form-control" placeholder="Start Date" style="background-color:#FFF">
								<?php echo showerrormsg('start_date',$errorarr); ?>
							</div>
						</div>
						
						<div class="col-md-12 col-sm-12">
                            <div class="col-md-4 col-sm-12 form_field form-group">
								<label for="invoice_email">Account Payable Email</label>
								<input value="{{ old('invoice_email') }}" id="invoice_email" name="invoice_email" type="email" class="form-control" placeholder="Account Payable Email"/>
								<?php echo showerrormsg('invoice_email',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="photo">Upload Avatar ( 180x180 )</label>
								<input id="photo" name="photo" type="file" class="avatar" accept="image/*">
								<?php echo showerrormsg('photo',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="pro_rate">Pro Rate</label><br>
								<label class="radio-inline"><input type="radio" name="pro_rate" value="No" checked> No </label>
								<label class="radio-inline"><input type="radio" name="pro_rate" value="Yes"> Yes </label><br>
								<?php echo showerrormsg('pro_rate',$errorarr); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default"  style=" color:#fff; background-color: #ffbe18!important;
					border-color: #e4a70c!important;"> Add Vendor</button>
					<button type="button" class="btn btn-default" data-dismiss="modal" > Cancel</button>
				</div>
			</div>
		</div>
	</form>
</div>

@if(app('request')->exists('id'))
<!-- Modals Section-->
<div id="edit_vendor_modal" class="modal fade" role="dialog">
	<form action="{{ route('edit-vendor-submit') }}" method="post" id="edit_vendor" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" style="color:#fff;opacity: 0.8;">&times;</button>
					<h4 class="modal-title">Edit Vendor</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="vendor_name">Name</label>
								<input value="@if(app('request')->exists('id')){{ $Vendor->name }}@endif" type="text" class="form-control" id="vendor_name" name="vendor_name" placeholder="Name">
								<input value="@if(app('request')->exists('id')){{ $Vendor->id }}@endif" id="vendor_id" type="hidden" name="vendor_id">
								<?php echo showerrormsg('vendor_name',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="vendor_address">Address</label>
								<input value="@if(app('request')->exists('id')){{ $Vendor->address }}@endif" type="text" class="form-control" id="vendor_address" name="vendor_address" placeholder="Address">
								<?php echo showerrormsg('vendor_address',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="vendor_zip">Zip</label>
								<input value="@if(app('request')->exists('id')){{ $Vendor->zip }}@endif" type="text" class="form-control" id="vendor_zip" name="vendor_zip" placeholder="Zip" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<?php echo showerrormsg('vendor_zip',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="vendor_mobile">Phone</label>
								<input value="@if(app('request')->exists('id')){{ $Vendor->mobile }}@endif" type="text" class="form-control" id="vendor_mobile" name="vendor_mobile" placeholder="Phone" data-inputmask='"mask": "(999) 999-9999"' data-mask/>
								<?php echo showerrormsg('vendor_mobile',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12  form_field form-group">
								<label for="vendor_email">Email</label>
								<input value="@if(app('request')->exists('id')){{ $Vendor->email }}@endif" type="email" class="form-control" id="vendor_email" name="vendor_email" placeholder="Email">
								<?php echo showerrormsg('vendor_email',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="vendor_url">URL</label>
								<input value="@if(app('request')->exists('id')){{ $Vendor->url }}@endif" type="url" class="form-control" id="vendor_url" name="vendor_url" placeholder="Url">
								<?php echo showerrormsg('vendor_url',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="vendor_contact_name">Contact Name</label>
								<input value="@if(app('request')->exists('id')){{ $Vendor->contact_name }}@endif" type="text" class="form-control" id="vendor_contact_name" name="vendor_contact_name" placeholder="Contact Name">
								<?php echo showerrormsg('vendor_contact_name',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="vendor_package">Package</label>
								<select id="vendor_package" name="vendor_package" class="form_select">
									<option value="">Select Package</option>
									@foreach($VendorPackages as $VendorPackage)
									<option value="{{ $VendorPackage->id }}" @if(app('request')->exists('id') && $Vendor->vendor_package_id==$VendorPackage->id) selected  @endif>{{ $VendorPackage->name }}</option>
									@endforeach
								</select>
								<?php echo showerrormsg('vendor_package',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="vendor_start_date">Start Date</label>
								<input value="@if(app('request')->exists('id')){{ date('m-d-Y',strtotime($Vendor->start_date)) }}@endif" id="vendor_start_date" name="vendor_start_date" type="text" class="form-control" placeholder="Start Date" style="background-color:#FFF">
								<?php echo showerrormsg('vendor_start_date',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
                            <div class="col-md-4 col-sm-12 form_field form-group">
								<label for="vendor_invoice_email">Account Payable Email</label>
								<input value="@if(app('request')->exists('id')){{ $Vendor->invoice_email }}@endif" id="vendor_invoice_email" name="invoice_email" type="email" class="form-control" placeholder="Account Payable Email"/>
								<?php echo showerrormsg('invoice_email',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="vendor_photo">Upload Avatar ( 180x180 )</label>
								<input id="vendor_photo" name="vendor_photo" type="file" class="avatar" accept="image/*">
								<?php echo showerrormsg('vendor_photo',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="vendor_pro_rate">Pro Rate</label><br>
								<label class="radio-inline"><input type="radio" name="vendor_pro_rate" value="No" @if(app('request')->exists('id') && $Vendor->pro_rate=="No") checked @endif> No </label>
								<label class="radio-inline"><input type="radio" name="vendor_pro_rate" value="Yes" @if(app('request')->exists('id') && $Vendor->pro_rate=="Yes") checked @endif> Yes </label><br>
								<?php echo showerrormsg('vendor_pro_rate',$errorarr); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default"  style=" color:#fff; background-color: #ffbe18!important;
					border-color: #e4a70c!important;"> Update Vendor</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"> Cancel</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endif

@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<script src="{{asset('assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
<script>
	$( document ).ready(function() {
		@if($WritePermission=='Yes')
		var extra_features = '<select name="filter_status" id="filter_status" style="display: inline;float: left; margin-right: 5px;"><option value="1">Active</option><option value="0">Inactive</option></select><button class="btn btn-default add_vendor" type="button" name="mail" title="Create"  ><i class="fa fa-user-plus"></i></button><button class="btn btn-default" type="button" name="mail" title="Setting" ><i class="fa fa-fw fa-cogs"></i></button><button class="btn btn-default" type="button" name="print" title="Print" ><i class="fa fa-fw fa-print"></i></button><button class="btn btn-default ActiveBulkVendor" type="button" title="Active Vendors" ><i class="fa fa-fw fa-check-circle-o"></i></button><button class="btn btn-default InactiveBulkVendor" type="button" title="Inactive Vendors" ><i class="fa fa-fw fa-ban"></i></button>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
		@else
		var extra_features = '<select name="filter_status" id="filter_status" style="display: inline;float: left; margin-right: 5px;"><option value="1">Active</option><option value="0">Inactive</option></select><button class="btn btn-default" type="button" name="mail" title="Mail" ><i class="fa fa-fw fa-cogs"></i></button><button class="btn btn-default" type="button" name="print" title="Print" ><i class="fa fa-fw fa-print"></i></button>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
		@endif
		
		$('.add_vendor').click( function(){
			$('#add_vendor_modal').modal("show")
		});	
		
		$("#filter_status").change(function(){
			$('#table4').bootstrapTable('refresh', {
				query: {
					status: $(this).val(),
					dateRange: $('#date-range-header').val(),
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
		
		$('.button-select').click(function () {
			$('#table4').bootstrapTable('refresh', {
				query: {
					status: $('#filter_status').val(),
					dateRange: $('#date-range-header').val(),
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
		
		$('.button-clear').click(function () {
			$('#table4').bootstrapTable('refresh', {
				query: {
					status: $('#filter_status').val(),
					dateRange: '',
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
		
		$(".form_field").find('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});  
		
		<?php if(app('request')->exists('id')) { ?>
			$('#edit_vendor_modal').modal("show");
			<?php }elseif(count($errors->all())>0) { ?>
			$('.add_vendor').trigger('click');
		<?php } ?>
	});	
</script>
<!-- InputMask -->
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.date.extensions.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.extensions.js')}}" type="text/javascript"></script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}" ></script>
<!-- bootstrap time picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/timedropper/js/timedropper.js')}}" ></script>
<script>
	$(document).ready(function(){
		$('.dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Vendor Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$("[data-mask]").inputmask();
		
		var options1 = {
			format: "m-d-Y",
			dropPrimaryColor: "#428bca"
		};
		$('#start_date').dateDropper($.extend({}, options1));
		$('#vendor_start_date').dateDropper($.extend({}, options1));
	});	
	
	function queryParams(params) {
		params.status = $('#filter_status').val(); // add param1
		params.search = $('.fixed-table-toolbar .search input[type=text]').val();
		params.dateRange = $('#date-range-header').val();
		// console.log(JSON.stringify(params));
		// {"limit":10,"offset":0,"order":"asc","your_param1":1,"your_param2":2}
		return params;
	}
	
	function VendorDelete(id)
	{
		swal({
			title: 'Are you sure?',
			text: "You want to delete this vendor!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#22D69D',
			cancelButtonColor: '#FB8678',
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn',
			cancelButtonClass: 'btn'
			}).then(function () {
			$.get('delete-vendor',{'id':id}).then(function(){
				swal('Deleted!', 'Your vendor has been deleted.', 'success');
				window.location.reload(true); 
			});
			}, function (dismiss) {
			if (dismiss === 'cancel') {
				swal('Cancelled', 'Your vendor is safe.', 'error');
			}
		});
	}
    $("#zip,#vendor_zip").inputmask('99999');
</script>
<script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/inner_pages_validation.js')}}"></script>
<!-- end of page level js -->
<script>
	$(document).ready(function()
	{
		var _token = "{{ csrf_token() }}";
		
		$('.checkall').click(function() {
			var checked = $(this).prop('checked');
			$('#table4').find('.vendor_id').prop('checked', checked);
		});
		
		$(".ActiveBulkVendor").click(function() {
			var checkedRows = []
			$("input[class='vendor_id']:checked").each(function ()
			{
				checkedRows.push($(this).val());
			});
			
			if(checkedRows.length==0)
			{
				swal('Error', 'Please select vendor.', 'error');
			}
			else
			{
				swal({
					title: 'Are you sure?',
					text: "You want to active selected vendors?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, active it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.post("{{ route('active-bulk-vendor') }}",{'id':checkedRows, _token:_token}).then(function(response){
						swal('Active!', 'Vendors active successfully.', 'success');
						window.location.reload(true);
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Vendors active cancelled successfully.', 'error');
					}
				});
			}
		});
		
		$(".InactiveBulkVendor").click(function() {
			var checkedRows = []
			$("input[class='vendor_id']:checked").each(function ()
			{
				checkedRows.push($(this).val());
			});
			
			if(checkedRows.length==0)
			{
				swal('Error', 'Please select vendor.', 'error');
			}
			else
			{
				swal({
					title: 'Are you sure?',
					text: "You want to inactive selected vendors?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, inactive it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.post("{{ route('inactive-bulk-vendor') }}",{'id':checkedRows, _token:_token}).then(function(response){
						swal('Inactive!', 'Vendors inactive successfully.', 'success');
						window.location.reload(true);
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Vendors inactive cancelled successfully.', 'error');
					}
				});
			}
		});
	});
</script>
@stop