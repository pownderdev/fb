@extends('layouts/default')

{{-- Page title --}}
@section('title')
FB Messenger
@parent
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li> Pages</li>
		<li class="active">
			FB Messenger
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content p-l-r-15">
	@include('layouts.right_sidebar')
</section>
<!-- /.content -->
@stop