@extends('layouts/default')

{{-- Page title --}}
@section('title')
Dashboard
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/leaflet/css/leaflet.css')}}"/>
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	
	.export.btn-group {*display:none!important;}
	
	.animation-hatch small{font-size:45%;}
	
	.animation-hatch {*cursor:pointer;}
	.fixed-table-toolbar .search {width: Calc( 100% - 195px );
	.top-bar .multiselect.dropdown-toggle.btn.btn-default{ width: 160px; }
	.fixed-table-container {height: 500px !important; }
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
@php $TotalLeadText = 0 @endphp
@if(count(Helper::FacebookAccounts())>0)
<?php if(Session::has('FirstTimeOauth')) {?>
	<section class="content">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel">
					<div class="panel-body tasks_list">
						<div class="panel-body">
							<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
								<h4 style="text-align:center">We are gathering the leads.... this process might take up to an hour.</h4>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- ./row -->
	</section>
	
	<?php }else{ ?>
	
	<section class="content-header gred_2" style="margin-top: -3px;">
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<div class="header-data">
					<h1>Dashboard</h1>
					<p>Welcome to Pownder <small class="tm">TM</small> &nbsp , {{ Session::get('user_name') }}</p>
				</div>
			</div>
			
			<div class="col-md-6 col-sm-6 col-xs-12 pull-right header-top">
				<div class="row text-center">
					<div class="col-xs-6 col-sm-3">
						<a href="#"> 
							<h2 class="animation-hatch">
								<strong style="color: #fff;">
									{{ Helper::NumberFormat(number_format($Likes,2)) }}
								</strong>
								<br>
								<small>
									<i class="fa fa-fw fa-thumbs-o-up"></i>
									Likes
								</small>
							</h2>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3">
						<a href="#">
							<h2 class="animation-hatch">
								<strong style="color: #fff;">
									{{ Helper::NumberFormat(number_format($Reach,2)) }}
								</strong>
								<br>
								<small>
									<i class="fa fa-heart-o"></i>
									Reach
								</small>
							</h2>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3">
						<a href="{{ route('leads') }}">
							<h2 class="animation-hatch">
								<strong id="total_lead"  style="color: #fff;">
									{{ Helper::NumberFormat(number_format($TotalLead,2)) }}
									@php $TotalLeadText = Helper::NumberFormat(number_format($TotalLead,2)); @endphp
								</strong>	
								<br>
								<small>
									<i class="fa fa-fw fa-trophy"></i>
									Results
								</small>
							</h2>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3">
						<a href="{{ route('clients') }}">
							<h2 class="animation-hatch">
								<strong style="color: #fff;">{{ Helper::NumberFormat(number_format($clients,2)) }}</strong>
								<br>
								<small>
									<i class="fa fa-fw fa-suitcase black"></i>
									Clients
								</small>
							</h2>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="content">
		@include('panel.includes.status')
		@if($UnassignLeads > 0)
		<div class="alert alert-warning alert-dismissable" style="color:#fff; background-color: #ffbe18 !important; border-color: #ffbe18 !important;">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
			<strong><a href="leads?Unassign=show" style="color: #fff;">{{ Helper::NumberFormat(number_format($UnassignLeads,2)) }}</a></strong> Leads is not assigned yet, please perform action.
		</div>
		@endif
		<div class="row">
			<div class="col-md-12">
				<div class="row tiles-row">
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
						<div class="canvas-interactive-wrapper1">
							<canvas id="canvas-interactive1" class="grad1"></canvas>
							<a href="{{ route('leads') }}?date_range={{ date('Y-m-d') }} to {{ date('Y-m-d') }}" id="lead_url">
								<div class="cta-wrapper1">
									<div class="widget" data-count=".num" data-from="0" data-to="99.9" data-suffix="%" data-duration="2">
										<div class="item">
											<div class="widget-icon pull-left icon-color animation-fadeIn">
												<i class="menu-icon fa fa-fw fa-line-chart fa-size"></i>
											</div>
										</div>
										<div class="widget-count panel-white">
											<div class="item-label text-center">
												<div id="month_lead" class="count-box">
													{{ Helper::NumberFormat(number_format($TodayLead,2)) }}
												</div>
												<span class="title">Leads</span>
											</div>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
					
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
						<div class="widget" data-count=".num" data-from="0" data-to="512" data-duration="3">
							<div class="canvas-interactive-wrapper2">
								<canvas id="canvas-interactive2" class="grad1"></canvas>
								<a href="#"> 
									<div class="cta-wrapper2">
										<div class="item">
											<div class="widget-icon pull-left icon-color animation-fadeIn">
												<i class="fa fa-fw fa-usd fa-size"></i>
											</div>
										</div>
										<div class="widget-count panel-white">
											<div class="item-label text-center">
												<div class="count-box" id="CPA">
													@if($Spend == 0 || $facebook_ads_lead_users == 0)
													$0
													@else
													${{ Helper::NumberFormat(number_format($Spend/$facebook_ads_lead_users,2)) }}
													@endif
												</div>
												<span class="title">CPA</span>
											</div>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
					
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
						<div class="widget" data-suffix="k" data-count=".num" data-from="0" data-to="310" data-duration="4" data-easing="false">
							<div class="canvas-interactive-wrapper3">
								<canvas id="canvas-interactive3" class="grad1"></canvas>
								<a href="#"> 
									<div class="cta-wrapper3">
										<div class="item">
											<div class="widget-icon pull-left icon-color animation-fadeIn">
												<i class="fa fa-bar-chart-o fa-size"></i>
											</div>
										</div>
										<div class="widget-count panel-white">
											<div class="item-label text-center">
												<div class="count-box" id="LGR">
													@if($Click == 0 || $facebook_ads_lead_users == 0)
													0%
													@else
													{{ Helper::NumberFormat(number_format(($facebook_ads_lead_users/$Click)*100,2)) }}%
													@endif
												</div>
												<span class="title">Lead Generation Rate</span>
											</div>
										</div>
									</div>
								</a> 
							</div>
						</div>
					</div>
					
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
						<div class="widget">
							<div class="canvas-interactive-wrapper4">
								<canvas id="canvas-interactive4" class="grad1"></canvas>
								<a href="#"> 
									<div class="cta-wrapper4">
										<div class="item">
											<div class="widget-icon pull-left icon-color animation-fadeIn">
												<i class="fa fa-fw fa-money fa-size"></i>
											</div>
										</div>
										<div class="widget-count panel-white">
											<div class="item-label text-center">
												<div class="count-box" id="Spend">
													${{ Helper::NumberFormat(number_format($Spend,2)) }}
												</div>
												<span class="title">Ad Spent</span>
											</div>
										</div>
									</div>
								</a> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel">
					<div class="panel-heading">
						<h3 class="heading_text panel-title">Leads</h3>
						<span class="pull-right line-bar-charts">
							<button class="btn btn-sm btn-link chart_switch" data-chart="bar">Bar Chart</button>
							<button class="btn btn-sm btn-default chart_switch" data-chart="line">Line Chart
							</button>
						</span>
					</div>
					<div class="panel-body">
						<div id="sales-line-bar" style="height:400px"></div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-md-12 col-xs-12" style="padding:0px !important;" id="mainMap">
					<div id="map" style="width: 100%; height: 400px;"></div>
				</div>
			</div>
		</div>
		
		<div class="row" style="margin-top:20px">
			<div class="col-lg-12 col-sm-12 col-xs-12">
				<div class="panel">
					<div class="panel-body">
						<table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20, 40, ALL]" data-show-footer="false" data-toggle="table" data-side-pagination="server" data-query-params="queryParams" data-url="{{ route('vendor-dashboard-client') }}" data-sort-name="Leads" data-sort-order="desc">
							<thead>
								<tr>
									<th data-field="#" data-sortable="true" @if(in_array("0",$Columns)) data-visible="false" @endif>#</th>
									<th data-field="Active?" data-sortable="true" @if(in_array("1",$Columns)) data-visible="false" @endif>Active?</th>
									<th data-field="Clients" data-sortable="true" @if(in_array("2",$Columns)) data-visible="false" @endif>Clients</th>
									<th data-field="Ad Set Name" data-sortable="true" @if(in_array("3",$Columns)) data-visible="false" @endif>Ad Set Name</th>
									<th data-field="Leads" data-sortable="true" @if(in_array("4",$Columns)) data-visible="false" @endif>Leads</th>
									<th data-field="Calls" data-sortable="true" @if(in_array("5",$Columns)) data-visible="false" @endif>Calls</th>
									<th data-field="Clicks" data-sortable="true" @if(in_array("6",$Columns)) data-visible="false" @endif>Clicks</th>
									<th data-field="LGR %" data-sortable="true" @if(in_array("7",$Columns)) data-visible="false" @endif>LGR %</th>
									<th data-field="CPA $" data-sortable="true" @if(in_array("8",$Columns)) data-visible="false" @endif>CPA $</th>
									<th data-field="Budget" data-sortable="true" @if(in_array("9",$Columns)) data-visible="false" @endif>Budget </th>
									<th data-field="Spent $" data-sortable="true" @if(in_array("10",$Columns)) data-visible="false" @endif>Spent $</th>
									<th data-field="CTR %" data-sortable="true" @if(in_array("11",$Columns)) data-visible="false" @endif>CTR %</th>
									<th data-field="CPM $" data-sortable="true" @if(in_array("12",$Columns)) data-visible="false" @endif>CPM $</th>
									<th data-field="Freq" data-sortable="true" @if(in_array("13",$Columns)) data-visible="false" @endif>Freq</th>
									<th data-field="Reach" data-sortable="true" @if(in_array("14",$Columns)) data-visible="false" @endif>Reach</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php }?>

@else

<section class="content">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel">
				<div class="panel-body tasks_list">
					<div class="panel-body">
						<div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
							<h1 style="text-align:center">Welcome, {{ session('user')->name }}</h1>
							<h3 style="text-align:center">Connect your Facebook account and start optimizing your campaigns!</h3>
							<p style="text-align:center; margin-top:20px">
								<a href="https://www.facebook.com/v2.10/dialog/oauth?client_id=246735105809032&state=45c2c5f62d6c955c813aa2e987072db6&response_type=code&sdk=php-sdk-5.5.0&redirect_uri=http%3A%2F%2Fbig.pownder.com%2Ffblogin%2Fcallback.php&scope=email%2Cpages_show_list%2Cmanage_pages%2Cads_read%2Cads_management%2Cuser_likes%2Cpublish_actions%2Cpublish_pages%2Cpages_messaging" class="button button-rounded button-action-flat hvr-sink"  style="background-color: #3b5998; font-weight: 600; padding:0px 20px 0px 0px"> 
									<span class="button" style="background-color: #3b5998; padding:0px 10px; color:#FFF">
									<i class="fa fa-fw fa-facebook fa-2" aria-hidden="true" style="font-size: 20px !important;"></i></span> Add Facebook Account
								</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- ./row -->
</section>

@endif

@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCerha5x7NxLWIlKdBytL4JjjCWTTTHHCM&sensor=false" type="text/javascript"></script>
<script type="text/javascript">
    var locations = [
	@php $i = 1; @endphp
	@foreach($CityLeads as $CityLead)
	@if($i == count($CityLeads))
	["{{ title_case($CityLead->city) }} - {{ $CityLead->Leads }} Leads", {{ number_format($CityLead->latitude,7) }}, {{ number_format($CityLead->longitude,7) }}, {{ $i++ }}]
	@else
	["{{ title_case($CityLead->city) }} - {{ $CityLead->Leads }} Leads", {{ number_format($CityLead->latitude,7) }}, {{ number_format($CityLead->longitude,7) }}, {{ $i++ }}],
	@endif
	@endforeach
    ];
	
    var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 10,
		center: new google.maps.LatLng(34.0522342, -118.2436849),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});
	
    var infowindow = new google.maps.InfoWindow();
	
	map.setOptions({ minZoom: 2, maxZoom: 30 });
	
    var marker, i;
	
    for (i = 0; i < locations.length; i++) {  
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			map: map
		});
		
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				infowindow.setContent(locations[i][0]);
				infowindow.open(map, marker);
			}
		})(marker, i));
	}
</script>

<script>
	$(document).ready(function () {
		$('#count-box').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 2
		});
		$('#count-like').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 2
		});
		$('#count-leads').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 2
		});
		$('#count-ads').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 2
		});
		$('#count-result').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 2
		});
		$('#count-box2').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 5
		});
		$('#count-box3').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 7
		});
		$('#count-box4').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 10
		});
		$('#count-pro').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 10
		});
		
		$(".client-list ul li").click(function() {
			jQuery('#date-loader').css("display","block");
			var client_id = $(this).attr('user_client_id');
			$('.reset_lead').addClass('btn-warning');
			$('.reset_lead').removeClass('btn-primary');
			var dateString = $('#date-range-header').val();
			if(dateString == '')
			{
				var date_start = "{{ date('Y-m-d') }}";
				var date_stop = "{{ date('Y-m-d') }}";
			}
			else
			{
				var dateArray = dateString.split(' to ');
				var date_start = dateArray[0];
				var date_stop = dateArray[1];
			}
			
			if(client_id == undefined && dateString == '')
			{
				$("#lead_url").attr('href', "{{ route('leads') }}?date_range={{ date('Y-m-d') }} to {{ date('Y-m-d') }}");
			}
			else if(client_id != undefined && dateString != '')
			{
				var start = date_start.split('-');
				var stop = date_stop.split('-');
				
				$("#lead_url").attr('href', "{{ route('leads') }}?client_id="+client_id+"&date_range="+dateString);
			}
			else if(client_id != undefined)
			{
				$("#lead_url").attr('href', "{{ route('leads') }}?date_range={{ date('Y-m-d') }} to {{ date('Y-m-d') }}&client_id="+client_id);
			}
			else if(dateString != '')
			{
				var start = date_start.split('-');
				var stop = date_stop.split('-');
				
				$("#lead_url").attr('href', "{{ route('leads') }}?date_range="+dateString);
			}
			if(client_id==undefined)
			{
				$('#total_lead').html("{{ $TotalLeadText }}");
				
				$.get("{{ route('vendor-dashboard-ajax') }}",{date_start: date_start, date_stop:date_stop, client_id:client_id},function(data){ 
					$('#CPA').html(data.CPA);
					$('#Spend').html(data.Spend);
					$('#LGR').html(data.LGR);
					$('#month_lead').html(data.Leads);
					
					$("#mainMap").html('<div id="map" style="width: 100%; height: 400px;"></div>');
					
					var j = 1;
					var locations = [];
					var CityLeads = data.CityLeads;
					for(var i=0;i<CityLeads.length;i++)
					{
						var MapLocation = [];
						MapLocation.push(CityLeads[i].city+' - '+CityLeads[i].Leads+' Leads');
						MapLocation.push(CityLeads[i].latitude);
						MapLocation.push(CityLeads[i].longitude);
						MapLocation.push(j);
						locations.push(MapLocation); 
						j++;
					} 
					
					var map = new google.maps.Map(document.getElementById('map'), {
						zoom: 10,
						center: new google.maps.LatLng(34.0522342, -118.2436849),
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});
					
					var infowindow = new google.maps.InfoWindow();
					map.setOptions({ minZoom: 2, maxZoom: 30 });
					
					var marker, i;
					for (i = 0; i < locations.length; i++) { 
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(locations[i][1], locations[i][2]),
							map: map
						});
						
						google.maps.event.addListener(marker, 'click', (function(marker, i) {
							return function() {
								infowindow.setContent(locations[i][0]);
								infowindow.open(map, marker);
							}
						})(marker, i));
					}
					
					/////////////////////////////////////////////Graph/////////////////////////////////////
					var facebook_campaign_values = data.facebook_campaign_values;
					var GraphCPA = data.GraphCPA;
					var GraphCTR = data.GraphCTR;
					var GraphClick = data.GraphClick;
					var GraphFreq = data.GraphFreq;
					var GraphLGR = data.GraphLGR;
					var GraphLead = data.GraphLead;
					var GraphSpent = data.GraphSpent;
					$("#display_graph").html('<div id="sales-line-bar" style="height:400px"></div>');
					var x_date = ['x'];
					var campaign_clicks = ['Clicks'];
					var campaign_cpa = ['CPA ($)'];
					var campaign_ctr = ['CTR (%)'];
					var campaign_frequency = ['Frequency'];
					var campaign_lgr = ['LGR (%)'];
					var campaign_leads = ['Leads'];
					var campaign_spend = ['Spent ($)'];
					
					for(var ai=0;ai<facebook_campaign_values.length;ai++)
					{ 
						var ctr = GraphCTR[ai];
						campaign_ctr.push(parseFloat(ctr).toFixed(2));
						
						var spend = GraphSpent[ai];
						campaign_spend.push(parseFloat(spend).toFixed(2));
						
						var frequency = GraphFreq[ai];
						campaign_frequency.push(parseFloat(frequency).toFixed(2));
						
						var cpa = GraphCPA[ai];
						campaign_cpa.push(parseFloat(cpa).toFixed(2));
						
						var lgr = GraphLGR[ai];
						campaign_lgr.push(parseFloat(lgr).toFixed(2));
						
						campaign_leads.push(GraphLead[ai]);
						campaign_clicks.push(GraphClick[ai]);
						x_date.push(facebook_campaign_values[ai].campaign_date);
					}
					
					var core_chart1 = {        
						main_dashboard: function () {            
							if ($('#sales-line-bar').length) {                
								var sales_line_bar_chart = c3.generate({                    
									bindto: '#sales-line-bar',                    
									data: {                        
										x: 'x',                        
										columns: [x_date, campaign_clicks, campaign_cpa, campaign_ctr, campaign_frequency, campaign_leads, campaign_lgr, campaign_spend],                 
										types: {'Clicks': 'area', 'CPA ($)': 'area', 'CTR (%)': 'area', 'Frequency': 'area', 'LGR (%)': 'area', 'Leads': 'area', 'Spent ($)': 'area', 'Clicks': 'line', 'CPA ($)': 'line', 'CTR (%)': 'line', 'Frequency': 'line', 'LGR (%)': 'line', 'Leads': 'line', 'Spent ($)': 'line'}                    
									},                    
									axis: {                        
										x: { type: 'timeseries', tick: { culling: false, fit: true, format: "%d%b " } },                 
										y: { tick: { format: d3.format("") } }                
									},              
									point: { r: '4', focus: { expand: { r: '5' } } },          
									bar: { width: { ratio: 0.5 } },            
									grid: {         
										x: { show: true	},               
										y: { show: true }              
									},             
									color: {       
									pattern: ['#428bca', '#ffb65f', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']                    }               
								});               
								$('.chart_switch').on('click', function () {   
									if ($(this).data('chart') == 'line') {     
										sales_line_bar_chart.transform('line');
										} else if ($(this).data('chart') == 'bar') {       
										sales_line_bar_chart.transform('bar');              
									}                  
									if (!$(this).hasClass("btn-default")) {      
										$('.chart_switch').toggleClass('btn-default btn-link');      
									}            
								});            
								$(window).on("debouncedresize", function () {       
									sales_line_bar_chart.resize();            
								});                             
								$("[data-toggle='offcanvas']").click(function (e) {           
									sales_line_bar_chart.resize();             
								});        
							}      
						}  
					};   
					core_chart1.main_dashboard();
					////////////////////////////////////////////Graph//////////////////////////////////////
					
					jQuery('#date-loader').css("display", "none");
				});
			}
			else
			{
				$.get("{{ route('client-total-leads') }}",{client_id: client_id},function(data){ 
					$('#total_lead').html(data);
				});
				$.get("{{ route('vendor-dashboard-ajax') }}",{date_start: date_start, date_stop:date_stop, client_id:client_id},function(data){ 
					$('#CPA').html(data.CPA);
					$('#Spend').html(data.Spend);
					$('#LGR').html(data.LGR);
					$('#month_lead').html(data.Leads);
					
					$("#mainMap").html('<div id="map" style="width: 100%; height: 400px;"></div>');
					
					var j = 1;
					var locations = [];
					var CityLeads = data.CityLeads;
					for(var i=0;i<CityLeads.length;i++)
					{
						var MapLocation = [];
						MapLocation.push(CityLeads[i].city+' - '+CityLeads[i].Leads+' Leads');
						MapLocation.push(CityLeads[i].latitude);
						MapLocation.push(CityLeads[i].longitude);
						MapLocation.push(j);
						locations.push(MapLocation); 
						j++;
					} 
					
					var map = new google.maps.Map(document.getElementById('map'), {
						zoom: 10,
						center: new google.maps.LatLng(34.0522342, -118.2436849),
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});
					
					var infowindow = new google.maps.InfoWindow();
					map.setOptions({ minZoom: 2, maxZoom: 30 });
					
					var marker, i;
					for (i = 0; i < locations.length; i++) { 
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(locations[i][1], locations[i][2]),
							map: map
						});
						
						google.maps.event.addListener(marker, 'click', (function(marker, i) {
							return function() {
								infowindow.setContent(locations[i][0]);
								infowindow.open(map, marker);
							}
						})(marker, i));
					}
					
					/////////////////////////////////////////////Graph/////////////////////////////////////
					var facebook_campaign_values = data.facebook_campaign_values;
					var GraphCPA = data.GraphCPA;
					var GraphCTR = data.GraphCTR;
					var GraphClick = data.GraphClick;
					var GraphFreq = data.GraphFreq;
					var GraphLGR = data.GraphLGR;
					var GraphLead = data.GraphLead;
					var GraphSpent = data.GraphSpent;
					$("#display_graph").html('<div id="sales-line-bar" style="height:400px"></div>');
					var x_date = ['x'];
					var campaign_clicks = ['Clicks'];
					var campaign_cpa = ['CPA ($)'];
					var campaign_ctr = ['CTR (%)'];
					var campaign_frequency = ['Frequency'];
					var campaign_lgr = ['LGR (%)'];
					var campaign_leads = ['Leads'];
					var campaign_spend = ['Spent ($)'];
					
					for(var ai=0;ai<facebook_campaign_values.length;ai++)
					{ 
						var ctr = GraphCTR[ai];
						campaign_ctr.push(parseFloat(ctr).toFixed(2));
						
						var spend = GraphSpent[ai];
						campaign_spend.push(parseFloat(spend).toFixed(2));
						
						var frequency = GraphFreq[ai];
						campaign_frequency.push(parseFloat(frequency).toFixed(2));
						
						var cpa = GraphCPA[ai];
						campaign_cpa.push(parseFloat(cpa).toFixed(2));
						
						var lgr = GraphLGR[ai];
						campaign_lgr.push(parseFloat(lgr).toFixed(2));
						
						campaign_leads.push(GraphLead[ai]);
						campaign_clicks.push(GraphClick[ai]);
						x_date.push(facebook_campaign_values[ai].campaign_date);
					}
					
					var core_chart1 = {        
						main_dashboard: function () {            
							if ($('#sales-line-bar').length) {                
								var sales_line_bar_chart = c3.generate({                    
									bindto: '#sales-line-bar',                    
									data: {                        
										x: 'x',                        
										columns: [x_date, campaign_clicks, campaign_cpa, campaign_ctr, campaign_frequency, campaign_leads, campaign_lgr, campaign_spend],                 
										types: {'Clicks': 'area', 'CPA ($)': 'area', 'CTR (%)': 'area', 'Frequency': 'area', 'LGR (%)': 'area', 'Leads': 'area', 'Spent ($)': 'area', 'Clicks': 'line', 'CPA ($)': 'line', 'CTR (%)': 'line', 'Frequency': 'line', 'LGR (%)': 'line', 'Leads': 'line', 'Spent ($)': 'line'}                    
									},                    
									axis: {                        
										x: { type: 'timeseries', tick: { culling: false, fit: true, format: "%d%b " } },                 
										y: { tick: { format: d3.format("") } }                
									},              
									point: { r: '4', focus: { expand: { r: '5' } } },          
									bar: { width: { ratio: 0.5 } },            
									grid: {         
										x: { show: true	},               
										y: { show: true }              
									},             
									color: {       
									pattern: ['#428bca', '#ffb65f', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']                    }               
								});               
								$('.chart_switch').on('click', function () {   
									if ($(this).data('chart') == 'line') {     
										sales_line_bar_chart.transform('line');
										} else if ($(this).data('chart') == 'bar') {       
										sales_line_bar_chart.transform('bar');              
									}                  
									if (!$(this).hasClass("btn-default")) {      
										$('.chart_switch').toggleClass('btn-default btn-link');      
									}            
								});            
								$(window).on("debouncedresize", function () {       
									sales_line_bar_chart.resize();            
								});                             
								$("[data-toggle='offcanvas']").click(function (e) {           
									sales_line_bar_chart.resize();             
								});        
							}      
						}  
					};   
					core_chart1.main_dashboard();
					////////////////////////////////////////////Graph//////////////////////////////////////
					
					jQuery('#date-loader').css("display", "none");
				});
			}
			
			$('#table4').bootstrapTable('refresh', {
				query: {
					dateRange: $('#date-range-header').val(),
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
		
		$(".reset_lead").click(function () {
			jQuery('#date-loader').css("display","block");
			$('#total_lead').html("{{ $TotalLeadText }}");
			var dateString = $('#date-range-header').val();
			if(dateString == '')
			{
				var date_start = "{{ date('Y-m-d') }}";
				var date_stop = "{{ date('Y-m-d') }}";
				$("#lead_url").attr('href', "{{ route('leads') }}?date_range={{ date('Y-m-d') }} to {{ date('Y-m-d') }}");
			}
			else
			{
				var dateArray = dateString.split(' to ');
				var date_start = dateArray[0];
				var date_stop = dateArray[1];
				
				var start = date_start.split('-');
				var stop = date_stop.split('-');
				
				$("#lead_url").attr('href', "{{ route('leads') }}?date_range="+dateString);
			}
			var client_id = '';
			$.get("{{ route('vendor-dashboard-ajax') }}",{date_start: date_start, date_stop:date_stop, client_id:client_id},function(data){ 
				$('#CPA').html(data.CPA);
				$('#Spend').html(data.Spend);
				$('#LGR').html(data.LGR);
				$('#month_lead').html(data.Leads);
				
				$("#mainMap").html('<div id="map" style="width: 100%; height: 400px;"></div>');
				
				var j = 1;
				var locations = [];
				var CityLeads = data.CityLeads;
				for(var i=0;i<CityLeads.length;i++)
				{
					var MapLocation = [];
					MapLocation.push(CityLeads[i].city+' - '+CityLeads[i].Leads+' Leads');
					MapLocation.push(CityLeads[i].latitude);
					MapLocation.push(CityLeads[i].longitude);
					MapLocation.push(j);
					locations.push(MapLocation); 
					j++;
				} 
				
				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 10,
					center: new google.maps.LatLng(34.0522342, -118.2436849),
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				
				var infowindow = new google.maps.InfoWindow();
				map.setOptions({ minZoom: 2, maxZoom: 30 });
				
				var marker, i;
				for (i = 0; i < locations.length; i++) { 
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(locations[i][1], locations[i][2]),
						map: map
					});
					
					google.maps.event.addListener(marker, 'click', (function(marker, i) {
						return function() {
							infowindow.setContent(locations[i][0]);
							infowindow.open(map, marker);
						}
					})(marker, i));
				}
				
				/////////////////////////////////////////////Graph/////////////////////////////////////
				var facebook_campaign_values = data.facebook_campaign_values;
				var GraphCPA = data.GraphCPA;
				var GraphCTR = data.GraphCTR;
				var GraphClick = data.GraphClick;
				var GraphFreq = data.GraphFreq;
				var GraphLGR = data.GraphLGR;
				var GraphLead = data.GraphLead;
				var GraphSpent = data.GraphSpent;
				$("#display_graph").html('<div id="sales-line-bar" style="height:400px"></div>');
				var x_date = ['x'];
				var campaign_clicks = ['Clicks'];
				var campaign_cpa = ['CPA ($)'];
				var campaign_ctr = ['CTR (%)'];
				var campaign_frequency = ['Frequency'];
				var campaign_lgr = ['LGR (%)'];
				var campaign_leads = ['Leads'];
				var campaign_spend = ['Spent ($)'];
				
				for(var ai=0;ai<facebook_campaign_values.length;ai++)
				{ 
					var ctr = GraphCTR[ai];
					campaign_ctr.push(parseFloat(ctr).toFixed(2));
					
					var spend = GraphSpent[ai];
					campaign_spend.push(parseFloat(spend).toFixed(2));
					
					var frequency = GraphFreq[ai];
					campaign_frequency.push(parseFloat(frequency).toFixed(2));
					
					var cpa = GraphCPA[ai];
					campaign_cpa.push(parseFloat(cpa).toFixed(2));
					
					var lgr = GraphLGR[ai];
					campaign_lgr.push(parseFloat(lgr).toFixed(2));
					
					campaign_leads.push(GraphLead[ai]);
					campaign_clicks.push(GraphClick[ai]);
					x_date.push(facebook_campaign_values[ai].campaign_date);
				}
				
				var core_chart1 = {        
					main_dashboard: function () {            
						if ($('#sales-line-bar').length) {                
							var sales_line_bar_chart = c3.generate({                    
								bindto: '#sales-line-bar',                    
								data: {                        
									x: 'x',                        
									columns: [x_date, campaign_clicks, campaign_cpa, campaign_ctr, campaign_frequency, campaign_leads, campaign_lgr, campaign_spend],                 
									types: {'Clicks': 'area', 'CPA ($)': 'area', 'CTR (%)': 'area', 'Frequency': 'area', 'LGR (%)': 'area', 'Leads': 'area', 'Spent ($)': 'area', 'Clicks': 'line', 'CPA ($)': 'line', 'CTR (%)': 'line', 'Frequency': 'line', 'LGR (%)': 'line', 'Leads': 'line', 'Spent ($)': 'line'}                    
								},                    
								axis: {                        
									x: { type: 'timeseries', tick: { culling: false, fit: true, format: "%d%b " } },                 
									y: { tick: { format: d3.format("") } }                
								},              
								point: { r: '4', focus: { expand: { r: '5' } } },          
								bar: { width: { ratio: 0.5 } },            
								grid: {         
									x: { show: true	},               
									y: { show: true }              
								},             
								color: {       
								pattern: ['#428bca', '#ffb65f', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']                    }               
							});               
							$('.chart_switch').on('click', function () {   
								if ($(this).data('chart') == 'line') {     
									sales_line_bar_chart.transform('line');
									} else if ($(this).data('chart') == 'bar') {       
									sales_line_bar_chart.transform('bar');              
								}                  
								if (!$(this).hasClass("btn-default")) {      
									$('.chart_switch').toggleClass('btn-default btn-link');      
								}            
							});            
							$(window).on("debouncedresize", function () {       
								sales_line_bar_chart.resize();            
							});                             
							$("[data-toggle='offcanvas']").click(function (e) {           
								sales_line_bar_chart.resize();             
							});        
						}      
					}  
				};   
				core_chart1.main_dashboard();
				////////////////////////////////////////////Graph//////////////////////////////////////
				
				$('.reset_lead').addClass('btn-primary');
				$('.reset_lead').removeClass('btn-warning');
				$(".client-list ul li").removeClass("active");
				jQuery('#date-loader').css("display", "none");
			});
			
			$('#table4').bootstrapTable('refresh', {
				query: {
					dateRange: $('#date-range-header').val(),
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
	});
	
	function queryParams(params) {
		params.search = $('.fixed-table-toolbar .search input[type=text]').val();
		params.dateRange = $('#date-range-header').val();
		params.client_id = $(".client-list ul li.active").attr("user_client_id");
		// console.log(JSON.stringify(params));
		// {"limit":10,"offset":0,"order":"asc","your_param1":1,"your_param2":2}
		return params;
	}
</script>

<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-multiselect/js/bootstrap-multiselect.js')}}"></script>
<script>
	$( document ).ready(function() {
	    var extra_features = '<button class="btn btn-default" type="button" name="mail" title="Mail" ><i class="fa fa-fw fa-envelope"></i></button><button class="btn btn-default" type="button" name="print" title="Print" ><i class="fa fa-fw fa-print"></i></button>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
		
        $("#multiselect1").multiselect();
		
        $("#multiselect1").change(function(){
			var set_val = $(this).val();
			//alert(set_val);
			if(set_val == "This Month" || set_val == "Last Month" || set_val == "This Year" || set_val == "Life Time"){
				$(".date_range").css({ "width": "263px", "display": "block"});
				}else{
				$(".date_range").css({ "width": "0px", "display": "none"});
			}
		});
		
		var x_date = ['x'];
		var campaign_clicks = ['Clicks'];
		var campaign_cpa = ['CPA ($)'];
		var campaign_ctr = ['CTR (%)'];
		var campaign_frequency = ['Frequency'];
		var campaign_lgr = ['LGR (%)'];
		var campaign_leads = ['Leads'];
		var campaign_spend = ['Spent ($)'];
		
		@php $imp = 0; @endphp
		@foreach($facebook_campaign_values as $facebook_campaign_value)
		var ctr = {{ $GraphCTR[$imp] }};
		campaign_ctr.push(parseFloat(ctr).toFixed(2));
		
		var spend = {{ $GraphSpent[$imp] }};
		campaign_spend.push(parseFloat(spend).toFixed(2));
		
		var frequency = {{ $GraphFreq[$imp] }};
		campaign_frequency.push(parseFloat(frequency).toFixed(2));
		
		var cpa = {{ $GraphCPA[$imp] }};
		campaign_cpa.push(parseFloat(cpa).toFixed(2));
		
		var lgr = {{ $GraphLGR[$imp] }};
		campaign_lgr.push(parseFloat(lgr).toFixed(2));
		
		campaign_leads.push({{ $GraphLead[$imp] }});
		campaign_clicks.push({{ $GraphClick[$imp] }});
		x_date.push('{{ $facebook_campaign_value->campaign_date }}');
		@php $imp++; @endphp
		@endforeach
		
		var core_chart1 = {        
			main_dashboard: function () {            
				if ($('#sales-line-bar').length) {                
					var sales_line_bar_chart = c3.generate({                    
						bindto: '#sales-line-bar',                    
						data: {                        
							x: 'x',                        
							columns: [x_date, campaign_clicks, campaign_cpa, campaign_ctr, campaign_frequency, campaign_leads, campaign_lgr, campaign_spend],
							types: {'Clicks': 'area', 'CPA ($)': 'area', 'CTR (%)': 'area', 'Frequency': 'area', 'LGR (%)': 'area', 'Leads': 'area', 'Spent ($)': 'area', 'Clicks': 'line', 'CPA ($)': 'line', 'CTR (%)': 'line', 'Frequency': 'line', 'LGR (%)': 'line', 'Leads': 'line', 'Spent ($)': 'line'}
						},                    
						axis: {                        
							x: {                            
								type: 'timeseries',                        
								tick: {                               
									culling: false,                  
									fit: true,                       
									format: "%d %b"                     
								}                  
							},                 
							y: {                
								tick: {          
									format: d3.format("")  
								}                   
							}                
						},              
						point: {        
							r: '4',       
							focus: {        
								expand: { r: '5' }             
							}             
						},          
						bar: {        
							width: { ratio: 0.5 }             
						},            
						grid: {         
							x: { show: true	},               
							y: { show: true }              
						},             
						color: {       
						pattern: ['#428bca', '#ffb65f', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']                    }               
					});               
					$('.chart_switch').on('click', function () {   
						if ($(this).data('chart') == 'line') {     
							sales_line_bar_chart.transform('line');  
							} else if ($(this).data('chart') == 'bar') {       
							sales_line_bar_chart.transform('bar');              
						}                  
						if (!$(this).hasClass("btn-default")) {      
							$('.chart_switch').toggleClass('btn-default btn-link');      
						}            
					});            
					$(window).on("debouncedresize", function () {       
						sales_line_bar_chart.resize();            
					});                             
					$("[data-toggle='offcanvas']").click(function (e) {           
						sales_line_bar_chart.resize();             
					});        
				}      
			}  
		};   
		core_chart1.main_dashboard();
		
		$('.button-select').click(function(evt){
			jQuery('#date-loader').css("display","block");
			var dateString = $('#date-range-header').val();
			if(dateString == '')
			{
				var date_start = "{{ date('Y-m-d') }}";
				var date_stop = "{{ date('Y-m-d') }}";
			}
			else
			{
				var dateArray = dateString.split(' to ');
				var date_start = dateArray[0];
				var date_stop = dateArray[1];
			}
			var client_id = $(".client-list ul li.active").attr("user_client_id");
			if(client_id == undefined && dateString == '')
			{
				$("#lead_url").attr('href', "{{ route('leads') }}?date_range={{ date('Y-m-d') }} to {{ date('Y-m-d') }}");
			}
			else if(client_id != undefined && dateString != '')
			{
				var start = date_start.split('-');
				var stop = date_stop.split('-');
				
				$("#lead_url").attr('href', "{{ route('leads') }}?client_id="+client_id+"&date_range="+dateString);
			}
			else if(client_id != undefined)
			{
				$("#lead_url").attr('href', "{{ route('leads') }}?date_range={{ date('Y-m-d') }} to {{ date('Y-m-d') }}&client_id="+client_id);
			}
			else if(dateString != '')
			{
				var start = date_start.split('-');
				var stop = date_stop.split('-');
				
				$("#lead_url").attr('href', "{{ route('leads') }}?date_range="+dateString);
			}
			$.get("{{ route('vendor-dashboard-ajax') }}",{date_start: date_start, date_stop:date_stop, client_id:client_id},function(data){ 
				$('#CPA').html(data.CPA);
				$('#Spend').html(data.Spend);
				$('#LGR').html(data.LGR);
				$('#month_lead').html(data.Leads);
				
				$("#mainMap").html('<div id="map" style="width: 100%; height: 400px;"></div>');
				
				var j = 1;
				var locations = [];
				var CityLeads = data.CityLeads;
				for(var i=0;i<CityLeads.length;i++)
				{
					var MapLocation = [];
					MapLocation.push(CityLeads[i].city+' - '+CityLeads[i].Leads+' Leads');
					MapLocation.push(CityLeads[i].latitude);
					MapLocation.push(CityLeads[i].longitude);
					MapLocation.push(j);
					locations.push(MapLocation); 
					j++;
				} 
				
				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 10,
					center: new google.maps.LatLng(34.0522342, -118.2436849),
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				
				var infowindow = new google.maps.InfoWindow();
				map.setOptions({ minZoom: 2, maxZoom: 30 });
				
				var marker, i;
				for (i = 0; i < locations.length; i++) { 
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(locations[i][1], locations[i][2]),
						map: map
					});
					
					google.maps.event.addListener(marker, 'click', (function(marker, i) {
						return function() {
							infowindow.setContent(locations[i][0]);
							infowindow.open(map, marker);
						}
					})(marker, i));
				}
				
				/////////////////////////////////////////////Graph/////////////////////////////////////
				var facebook_campaign_values = data.facebook_campaign_values;
				var GraphCPA = data.GraphCPA;
				var GraphCTR = data.GraphCTR;
				var GraphClick = data.GraphClick;
				var GraphFreq = data.GraphFreq;
				var GraphLGR = data.GraphLGR;
				var GraphLead = data.GraphLead;
				var GraphSpent = data.GraphSpent;
				$("#display_graph").html('<div id="sales-line-bar" style="height:400px"></div>');
				var x_date = ['x'];
				var campaign_clicks = ['Clicks'];
				var campaign_cpa = ['CPA ($)'];
				var campaign_ctr = ['CTR (%)'];
				var campaign_frequency = ['Frequency'];
				var campaign_lgr = ['LGR (%)'];
				var campaign_leads = ['Leads'];
				var campaign_spend = ['Spent ($)'];
				
				for(var ai=0;ai<facebook_campaign_values.length;ai++)
				{ 
					var ctr = GraphCTR[ai];
					campaign_ctr.push(parseFloat(ctr).toFixed(2));
					
					var spend = GraphSpent[ai];
					campaign_spend.push(parseFloat(spend).toFixed(2));
					
					var frequency = GraphFreq[ai];
					campaign_frequency.push(parseFloat(frequency).toFixed(2));
					
					var cpa = GraphCPA[ai];
					campaign_cpa.push(parseFloat(cpa).toFixed(2));
					
					var lgr = GraphLGR[ai];
					campaign_lgr.push(parseFloat(lgr).toFixed(2));
					
					campaign_leads.push(GraphLead[ai]);
					campaign_clicks.push(GraphClick[ai]);
					x_date.push(facebook_campaign_values[ai].campaign_date);
				}
				
				var core_chart1 = {        
					main_dashboard: function () {            
						if ($('#sales-line-bar').length) {                
							var sales_line_bar_chart = c3.generate({                    
								bindto: '#sales-line-bar',                    
								data: {                        
									x: 'x',                        
									columns: [x_date, campaign_clicks, campaign_cpa, campaign_ctr, campaign_frequency, campaign_leads, campaign_lgr, campaign_spend],                 
									types: {'Clicks': 'area', 'CPA ($)': 'area', 'CTR (%)': 'area', 'Frequency': 'area', 'LGR (%)': 'area', 'Leads': 'area', 'Spent ($)': 'area', 'Clicks': 'line', 'CPA ($)': 'line', 'CTR (%)': 'line', 'Frequency': 'line', 'LGR (%)': 'line', 'Leads': 'line', 'Spent ($)': 'line'}                    
								},                    
								axis: {                        
									x: { type: 'timeseries', tick: { culling: false, fit: true, format: "%d%b " } },                 
									y: { tick: { format: d3.format("") } }                
								},              
								point: { r: '4', focus: { expand: { r: '5' } } },          
								bar: { width: { ratio: 0.5 } },            
								grid: {         
									x: { show: true	},               
									y: { show: true }              
								},             
								color: {       
								pattern: ['#428bca', '#ffb65f', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']                    }               
							});               
							$('.chart_switch').on('click', function () {   
								if ($(this).data('chart') == 'line') {     
									sales_line_bar_chart.transform('line');
									} else if ($(this).data('chart') == 'bar') {       
									sales_line_bar_chart.transform('bar');              
								}                  
								if (!$(this).hasClass("btn-default")) {      
									$('.chart_switch').toggleClass('btn-default btn-link');      
								}            
							});            
							$(window).on("debouncedresize", function () {       
								sales_line_bar_chart.resize();            
							});                             
							$("[data-toggle='offcanvas']").click(function (e) {           
								sales_line_bar_chart.resize();             
							});        
						}      
					}  
				};   
				core_chart1.main_dashboard();
				////////////////////////////////////////////Graph//////////////////////////////////////
				
				jQuery('#date-loader').css("display", "none");
			});
			
			$('#table4').bootstrapTable('refresh', {
				query: {
					dateRange: $('#date-range-header').val(),
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
		
		$('.button-clear').click(function(evt){
			jQuery('#date-loader').css("display","block");
			var date_start = "{{ date('Y-m-d') }}";
			var date_stop = "{{ date('Y-m-d') }}";
			var client_id = $(".client-list ul li.active").attr("user_client_id");
			if(client_id == undefined)
			{
				$("#lead_url").attr('href', "{{ route('leads') }}?date_range={{ date('Y-m-d') }} to {{ date('Y-m-d') }}");
			}
			else
			{
				$("#lead_url").attr('href', "{{ route('leads') }}?date_range={{ date('Y-m-d') }} to {{ date('Y-m-d') }}&client_id="+client_id);
			}
			$.get("{{ route('vendor-dashboard-ajax') }}",{date_start: date_start, date_stop:date_stop, client_id:client_id},function(data){ 
				$('#CPA').html(data.CPA);
				$('#Spend').html(data.Spend);
				$('#LGR').html(data.LGR);
				$('#month_lead').html(data.Leads);
				
				$("#mainMap").html('<div id="map" style="width: 100%; height: 400px;"></div>');
				
				var j = 1;
				var locations = [];
				var CityLeads = data.CityLeads;
				for(var i=0;i<CityLeads.length;i++)
				{
					var MapLocation = [];
					MapLocation.push(CityLeads[i].city+' - '+CityLeads[i].Leads+' Leads');
					MapLocation.push(CityLeads[i].latitude);
					MapLocation.push(CityLeads[i].longitude);
					MapLocation.push(j);
					locations.push(MapLocation); 
					j++;
				} 
				
				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 10,
					center: new google.maps.LatLng(34.0522342, -118.2436849),
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				
				var infowindow = new google.maps.InfoWindow();
				map.setOptions({ minZoom: 2, maxZoom: 30 });
				
				var marker, i;
				for (i = 0; i < locations.length; i++) { 
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(locations[i][1], locations[i][2]),
						map: map
					});
					
					google.maps.event.addListener(marker, 'click', (function(marker, i) {
						return function() {
							infowindow.setContent(locations[i][0]);
							infowindow.open(map, marker);
						}
					})(marker, i));
				}
				
				/////////////////////////////////////////////Graph/////////////////////////////////////
				var facebook_campaign_values = data.facebook_campaign_values;
				var GraphCPA = data.GraphCPA;
				var GraphCTR = data.GraphCTR;
				var GraphClick = data.GraphClick;
				var GraphFreq = data.GraphFreq;
				var GraphLGR = data.GraphLGR;
				var GraphLead = data.GraphLead;
				var GraphSpent = data.GraphSpent;
				$("#display_graph").html('<div id="sales-line-bar" style="height:400px"></div>');
				var x_date = ['x'];
				var campaign_clicks = ['Clicks'];
				var campaign_cpa = ['CPA ($)'];
				var campaign_ctr = ['CTR (%)'];
				var campaign_frequency = ['Frequency'];
				var campaign_lgr = ['LGR (%)'];
				var campaign_leads = ['Leads'];
				var campaign_spend = ['Spent ($)'];
				
				for(var ai=0;ai<facebook_campaign_values.length;ai++)
				{ 
					var ctr = GraphCTR[ai];
					campaign_ctr.push(parseFloat(ctr).toFixed(2));
					
					var spend = GraphSpent[ai];
					campaign_spend.push(parseFloat(spend).toFixed(2));
					
					var frequency = GraphFreq[ai];
					campaign_frequency.push(parseFloat(frequency).toFixed(2));
					
					var cpa = GraphCPA[ai];
					campaign_cpa.push(parseFloat(cpa).toFixed(2));
					
					var lgr = GraphLGR[ai];
					campaign_lgr.push(parseFloat(lgr).toFixed(2));
					
					campaign_leads.push(GraphLead[ai]);
					campaign_clicks.push(GraphClick[ai]);
					x_date.push(facebook_campaign_values[ai].campaign_date);
				}
				
				var core_chart1 = {        
					main_dashboard: function () {            
						if ($('#sales-line-bar').length) {                
							var sales_line_bar_chart = c3.generate({                    
								bindto: '#sales-line-bar',                    
								data: {                        
									x: 'x',                        
									columns: [x_date, campaign_clicks, campaign_cpa, campaign_ctr, campaign_frequency, campaign_leads, campaign_lgr, campaign_spend],                 
									types: {'Clicks': 'area', 'CPA ($)': 'area', 'CTR (%)': 'area', 'Frequency': 'area', 'LGR (%)': 'area', 'Leads': 'area', 'Spent ($)': 'area', 'Clicks': 'line', 'CPA ($)': 'line', 'CTR (%)': 'line', 'Frequency': 'line', 'LGR (%)': 'line', 'Leads': 'line', 'Spent ($)': 'line'}                    
								},                    
								axis: {                        
									x: { type: 'timeseries', tick: { culling: false, fit: true, format: "%d%b " } },                 
									y: { tick: { format: d3.format("") } }                
								},              
								point: { r: '4', focus: { expand: { r: '5' } } },          
								bar: { width: { ratio: 0.5 } },            
								grid: {         
									x: { show: true	},               
									y: { show: true }              
								},             
								color: {       
								pattern: ['#428bca', '#ffb65f', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']                    }               
							});               
							$('.chart_switch').on('click', function () {   
								if ($(this).data('chart') == 'line') {     
									sales_line_bar_chart.transform('line');
									} else if ($(this).data('chart') == 'bar') {       
									sales_line_bar_chart.transform('bar');              
								}                  
								if (!$(this).hasClass("btn-default")) {      
									$('.chart_switch').toggleClass('btn-default btn-link');      
								}            
							});            
							$(window).on("debouncedresize", function () {       
								sales_line_bar_chart.resize();            
							});                             
							$("[data-toggle='offcanvas']").click(function (e) {           
								sales_line_bar_chart.resize();             
							});        
						}      
					}  
				};   
				core_chart1.main_dashboard();
				////////////////////////////////////////////Graph//////////////////////////////////////
				
				jQuery('#date-loader').css("display", "none");
			});
			$('#table4').bootstrapTable('refresh', {
				query: {
					dateRange: $('#date-range-header').val(),
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
		
		$('.dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Dashboard Client Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
	});
</script>
<!-- InputMask -->
<script  type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}" ></script>
<!-- bootstrap time picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/jquerydaterangepicker/js/jquery.daterangepicker.min.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/timedropper/js/timedropper.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/Buttons/js/buttons.js')}}" ></script>
@stop	
<?php Session::forget('FirstTimeOauth'); ?>