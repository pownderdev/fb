@extends('layouts/default')

{{-- Page title --}}
@section('title')
Campaigns
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	.fixed-table-container {height: 500px !important; }
	.export.btn-group {*display:none!important;}
	
	@if($WritePermission=='Yes')
	.fixed-table-toolbar {*width:60%;}
	.fixed-table-toolbar .search {width: Calc( 100% - 345px );}
	@else
	.fixed-table-toolbar {*width:60%;}
	.fixed-table-toolbar .search {width: Calc( 100% - 215px );}
	@endif
	
	@media screen and (max-width: 767px) {
	.fixed-table-toolbar {width: 100%;}
	.fixed-table-toolbar {width:100%;}
	.fixed-table-toolbar .search {width: Calc( 100% );}
	}
	
	/* ============================================================
	COMMON
	============================================================ */
	.cmn-toggle {
	position: absolute;
	margin-left: -9999px;
	visibility: hidden;
	}
	.cmn-toggle + label {
	/*display: block;*/
	position: relative;
	cursor: pointer;
	outline: none;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	}
	
	/* ============================================================
	SWITCH 1 - ROUND
	============================================================ */
	input.cmn-toggle-round + label {
	padding: 2px;
	width: 40px;
	height: 20px;
	background-color: #dddddd;
	-webkit-border-radius: 60px;
	-moz-border-radius: 60px;
	-ms-border-radius: 60px;
	-o-border-radius: 60px;
	border-radius: 60px;
	}
	input.cmn-toggle-round + label:before, input.cmn-toggle-round + label:after {
	display: block;
	position: absolute;
	top: 1px;
	left: 1px;
	bottom: 1px;
	content: "";
	}
	input.cmn-toggle-round + label:before {
	right: 1px;
	background-color: #f1f1f1;
	-webkit-border-radius: 60px;
	-moz-border-radius: 60px;
	-ms-border-radius: 60px;
	-o-border-radius: 60px;
	border-radius: 60px;
	-webkit-transition: background 0.4s;
	-moz-transition: background 0.4s;
	-o-transition: background 0.4s;
	transition: background 0.4s;
	}
	input.cmn-toggle-round + label:after {
	width: 19px;
	background-color: #fff;
	-webkit-border-radius: 100%;
	-moz-border-radius: 100%;
	-ms-border-radius: 100%;
	-o-border-radius: 100%;
	border-radius: 100%;
	-webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
	box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
	-webkit-transition: margin 0.4s;
	-moz-transition: margin 0.4s;
	-o-transition: margin 0.4s;
	transition: margin 0.4s;
	}
	input.cmn-toggle-round:checked + label:before {background-color: #8ce196;}
	input.cmn-toggle-round:checked + label:after {margin-left: 20px;}
	#filter_status {
    display: block;
    width: 100px;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.428571429;
    color: #555555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
	}
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">
			Campaigns
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Campaign List
					</h3>
				</div>
				<div class="panel-body">
					<table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-query-params="queryParams" data-pagination="true" data-id-field="id" data-page-list="[10, 20, 40, ALL]" data-side-pagination="server" data-show-footer="false"  data-toggle="table" data-url="{{ route('campaign-ajax') }}">
						<thead>
                            <tr>
								@if($WritePermission=='Yes')<th data-field="" @if(in_array("0",$CampaignColumns)) data-visible="false" @endif><input type="checkbox" class="checkall" /></th>@endif
                                <th data-field="Date" data-sortable="true" @if(in_array("1",$CampaignColumns)) data-visible="false" @endif>Date</th>
                                <th data-field="Clients" data-sortable="true" @if(in_array("2",$CampaignColumns)) data-visible="false" @endif>Clients</th>
								<th data-field="Campaign Name" data-sortable="true" @if(in_array("3",$CampaignColumns)) data-visible="false" @endif>Campaign Name</th>
                                <th data-field="CRM" data-sortable="true" @if(in_array("4",$CampaignColumns)) data-visible="false" @endif>CRM</th>
                                <th data-field="Leads" data-sortable="true" data-align="right" @if(in_array("5",$CampaignColumns)) data-visible="false" @endif>Leads</th>
                                <th data-field="Last Lead" data-sortable="true" @if(in_array("6",$CampaignColumns)) data-visible="false" @endif>Last Lead</th>
                                @if($WritePermission=='Yes')<th data-field="Action" data-sortable="true" @if(in_array("7",$CampaignColumns)) data-visible="false" @endif>Action</th>@endif
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<script>
	$(document).ready(function(){
		$('.checkall').click(function() {
			var checked = $(this).prop('checked');
			$('#table4').find('.campaign_id').prop('checked', checked);
		});
		
		$(".switch1 input[type='checkbox']").on('change', function (event, state) {
			var campaign_id = $(this).val();
			var $section = $(this);
			if($(this).is(':checked')) 
			{
				var is_active = 0;
				swal({
					title: 'Are you sure?',
					text: "You want to active this campaign?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, active it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.get("{{ route('campaign_status') }}",{'id':campaign_id, 'is_active':is_active}).then(function(response){
						swal('Activated!', 'Campaign activated successfully.', 'success');
						//window.location.reload(true);
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Campaign activation cancelled successfully.', 'error');
						$section.prop("checked", false);
					}
				});
			} 
			else 
			{
				var is_active = 1;
				swal({
					title: 'Are you sure?',
					text: "You want to pause this campaign?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, pause it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.get("{{ route('campaign_status') }}",{'id':campaign_id, 'is_active':is_active}).then(function(response){
						swal('Paused!', 'Campaign paused successfully.', 'success');
						//window.location.reload(true);
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Campaign pause cancelled successfully.', 'error');
						$section.prop("checked", true);
					}
				});
			}
		});
		
		setInterval(function(){
			$(".switch1 input[type='checkbox']").on('change', function (event, state) {
				var campaign_id = $(this).val();
				var $section = $(this);
				if($(this).is(':checked')) 
				{
					var is_active = 0;
					swal({
						title: 'Are you sure?',
						text: "You want to active this campaign?",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#22D69D',
						cancelButtonColor: '#FB8678',
						confirmButtonText: 'Yes, active it!',
						cancelButtonText: 'No, cancel!',
						confirmButtonClass: 'btn',
						cancelButtonClass: 'btn'
						}).then(function () {
						$.get("{{ route('campaign_status') }}",{'id':campaign_id, 'is_active':is_active}).then(function(response){
							swal('Activated!', 'Campaign activated successfully.', 'success');
							//window.location.reload(true);
						});
						}, function (dismiss) {
						// dismiss can be 'cancel', 'overlay',
						// 'close', and 'timer'
						if (dismiss === 'cancel') {
							swal('Cancelled', 'Campaign activation cancelled successfully.', 'error');
							$section.prop("checked", false);
						}
					});
				} 
				else 
				{
					var is_active = 1;
					swal({
						title: 'Are you sure?',
						text: "You want to pause this campaign?",
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#22D69D',
						cancelButtonColor: '#FB8678',
						confirmButtonText: 'Yes, pause it!',
						cancelButtonText: 'No, cancel!',
						confirmButtonClass: 'btn',
						cancelButtonClass: 'btn'
						}).then(function () {
						$.get("{{ route('campaign_status') }}",{'id':campaign_id, 'is_active':is_active}).then(function(response){
							swal('Paused!', 'Campaign paused successfully.', 'success');
							//window.location.reload(true);
						});
						}, function (dismiss) {
						// dismiss can be 'cancel', 'overlay',
						// 'close', and 'timer'
						if (dismiss === 'cancel') {
							swal('Cancelled', 'Campaign pause cancelled successfully.', 'error');
							$section.prop("checked", true);
						}
					});
				}
			});
		}, 1000);
		
		@if($WritePermission=='Yes')
		var extra_features = '<select name="filter_status" id="filter_status" style="display: inline;float: left; margin-right: 5px;"><option value="0">Active</option><option value="1">Inactive</option></select><a href="campaign_add" class="btn btn-default" title="Add New" ><i class="fa fa-fw fa-plus-square"></i></a><button class="btn btn-default campaign_clone" type="button" title="Clone" ><i class="fa fa-fw fa-clone"></i></button><button class="btn btn-default delete_campaign" type="button"  title="Delete" ><i class="fa fa-fw fa-trash"></i></button>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
		@else
		var extra_features = '<select name="filter_status" id="filter_status" style="display: inline;float: left; margin-right: 5px;"><option value="0">Active</option><option value="1">Inactive</option></select>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
		@endif
		
		$('.dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Campaign Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
	});
</script>

<script>
	$(document).ready(function()
	{
		var _token = "{{ csrf_token() }}";
		$(".delete_campaign").click(function() {
			var checkedRows = []
			$("input[class='campaign_id']:checked").each(function ()
			{
				checkedRows.push($(this).val());
			});
			
			if(checkedRows.length==0)
			{
				swal('Error', 'Please select campaign.', 'error');
			}
			else
			{
				swal({
					title: 'Are you sure?',
					text: "You want to delete selected campaign?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, delete it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.post("{{ route('campaign_delete') }}",{'id':checkedRows, _token:_token}).then(function(response){
						swal('Deleted!', 'Campaign deleted successfully.', 'success');
						window.location.reload(true);
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Campaign delete cancelled successfully.', 'error');
					}
				});
			}
		});
		
		$(".campaign_clone").click(function() {
			var checkedRows = [];
			$("input[class='campaign_id']:checked").each(function ()
			{
				checkedRows.push($(this).val());
			});
			if(checkedRows.length==1)
			{
				swal({
					title: 'Are you sure?',
					text: "You want to create clone selected campaign?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, create it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.post("{{ route('campaign_clone') }}",{'id':checkedRows, _token:_token}).then(function(response){
						swal('Created!', 'Campaign clone created successfully.', 'success');
						window.location.reload(true);
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Campaign create clone cancelled successfully.', 'error');
					}
				});
			}
			else
			{
				if(checkedRows.length==0)
				{
					swal('Error', 'Please select campaign.', 'error');
				}
				else
				{
					swal('Error', 'Please select ony one campaign.', 'error');
				}
			}
		});
		
		$("#filter_status").change(function(){
			$('#table4').bootstrapTable('refresh', {
				query: {
					status: $(this).val(),
					dateRange: $('#date-range-header').val(),
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
		
		$('.button-select').click(function () {
			$('#table4').bootstrapTable('refresh', {
				query: {
					status: $('#filter_status').val(),
					dateRange: $('#date-range-header').val(),
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
		
		$('.button-clear').click(function () {
			$('#table4').bootstrapTable('refresh', {
				query: {
					status: $('#filter_status').val(),
					dateRange: '',
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
		
		@if(app('request')->exists('client_id'))
		var client_id = "{{ app('request')->client_id }}";
		if(client_id==undefined)
		{
			$('.reset_lead').addClass('btn-primary');
			$('.reset_lead').removeClass('btn-warning');
		}
		else
		{
			$('.reset_lead').addClass('btn-warning');
			$('.reset_lead').removeClass('btn-primary');
		}
		@endif
		
		$(".reset_lead").click(function () {
			$('.reset_lead').addClass('btn-primary');
			$('.reset_lead').removeClass('btn-warning');
			$(".client-list ul li").removeClass("active");
			
			$('#filter_status').val('0');
			$('.fixed-table-toolbar .search input[type=text]').val('');
			$('#table4').bootstrapTable('refresh', {
				query: {
					status: 0,
					dateRange: $('#date-range-header').val(),
					client_id: '',
					search: ''
				}
			});
		});
		
		$(".client-list ul li").click(function() {
			var client_id = $(this).attr('user_client_id');
			if(client_id==undefined)
			{
				$('.reset_lead').addClass('btn-primary');
				$('.reset_lead').removeClass('btn-warning');
			}
			else
			{
				$('.reset_lead').addClass('btn-warning');
				$('.reset_lead').removeClass('btn-primary');
			}
			
			$('#table4').bootstrapTable('refresh', {
				query: {
					status: $('#filter_status').val(),
					dateRange: $('#date-range-header').val(),
					client_id: $(".client-list ul li.active").attr("user_client_id"),
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
	});
	
	function queryParams(params) {
		params.status = $('#filter_status').val(); // add param1
		params.search = $('.fixed-table-toolbar .search input[type=text]').val();
		params.client_id = $(".client-list ul li.active").attr("user_client_id"),
		params.dateRange = $('#date-range-header').val();
		// console.log(JSON.stringify(params));
		// {"limit":10,"offset":0,"order":"asc","your_param1":1,"your_param2":2}
		return params;
	}
</script>
@stop