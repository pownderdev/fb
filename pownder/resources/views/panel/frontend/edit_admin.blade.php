@extends('layouts/default')

{{-- Page title --}}
@section('title')
Edit Profile
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_css/wizard.css')}}">
<link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet" type="text/css"/>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li>
			<a href="#"> Profile</a>
		</li>
		<li class="active">
			Edit Profile
		</li>
	</ol>
</section>

<?php
	$errorarr=$errors;   
	function showerrormsg($key,$errors)
	{    
		$msg='';
		if(count($errors->get($key))>0)
		{ 
			$msg.='';
			foreach($errors->get($key) as $error)
			{
				if($key=="alert_success")
				{
					$msg.=$error;
				}
				else
				{
					$msg.='<small class="text-danger animated fadeInUp"  >'.$error.'</small>' ; 
				}
			}
		} 
		
		return $msg;     
	}
?>

<!-- Main content -->
<section class="content">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff"
						data-loop="true"></i> Edit Profile
					</h3>
				</div>
				<div class="panel-body">
					<div class="bs-example" style="margin-top: 20px;">
						<ul class="nav nav-tabs" style="margin-bottom: 15px;">
							<li class="active">
								<a href="#home" data-toggle="tab" aria-expanded="true">Edit Profile</a>
							</li>
							<li class="">
								<a href="#profile" data-toggle="tab" aria-expanded="false">Change Password</a>
							</li>
						</ul>
						<div id="myTabContent" class="tab-content">
							<div class="tab-pane fade active in" id="home">
								<form action="{{ route('admin-edit-profile') }}" method="post" id="admin-edit-profile" enctype="multipart/form-data">
									{{ csrf_field() }}
									<div class="form-group">
										<label for="first_name" class="col-sm-3 control-label">Name *</label>
										<div class="col-sm-9">
											<input id="name" name="name" type="text" placeholder="Name" class="form-control" value="{{ $admin->name }}"/>
											<?php echo showerrormsg('name',$errorarr); ?>
										</div>
									</div>
									<div class="form-group" style="padding-top: 40px;">
										<label for="address" class="col-sm-3 control-label">Address *</label>
										<div class="col-sm-9">
											<input id="address" name="address" type="text" placeholder="Address" class="form-control" 	value="{{ $admin->address }}"/>
											<?php echo showerrormsg('address', $errorarr); ?>
										</div>
									</div>
									<div class="form-group" style="padding-top: 30px;">
										<label for="zip" class="col-sm-3 control-label">Zip *</label>
										<div class="col-sm-9">
											<input id="zip" name="zip" placeholder="Zip" type="text" class="form-control" value="{{ $admin->zip }}"/>
											<?php echo showerrormsg('zip', $errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="email" class="col-sm-3 control-label">Email *</label>
										<div class="col-sm-9">
											<input type="email" value="{{ $admin->email }}" id="email" name="email" placeholder="Email"  class="form-control"/>
											<?php echo showerrormsg('email', $errorarr); ?>
										</div>
									</div>
									<div class="form-group" style="padding-top: 30px;">
										<label for="appointment_option" class="col-sm-3 control-label">Appointment Email</label>
										<div class="col-sm-2">
											<label class="radio-inline"><input type="radio" name="appointment_option" value="No" @if($admin->appointment_option == 'No') checked @endif> No</label>
											<label class="radio-inline"><input type="radio" name="appointment_option" value="Yes" @if($admin->appointment_option == 'Yes') checked @endif> Yes</label>
											<?php echo showerrormsg('appointment_option', $errorarr); ?>
										</div>
										<div class="col-sm-7">
											<input type="email" value="{{ $admin->appointment_email }}" id="appointment_email" name="appointment_email" placeholder="Appointment Email"  class="form-control"/>
											<?php echo showerrormsg('appointment_email', $errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="invoice_option" class="col-sm-3 control-label">Invoice Email</label>
										<div class="col-sm-2">
											<label class="radio-inline"><input type="radio" name="invoice_option" value="No" @if($admin->invoice_option == 'No') checked @endif> No</label>
											<label class="radio-inline"><input type="radio" name="invoice_option" value="Yes" @if($admin->invoice_option == 'Yes') checked @endif> Yes</label>
											<?php echo showerrormsg('invoice_option', $errorarr); ?>
										</div>
										<div class="col-sm-7">
											<input type="email" value="{{ $admin->invoice_email }}" id="invoice_email" name="invoice_email" placeholder="Invoice Email"  class="form-control"/>
											<?php echo showerrormsg('invoice_email',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="new_lead_option" class="col-sm-3 control-label">New Lead Email</label>
										<div class="col-sm-2">
											<label class="radio-inline"><input type="radio" name="new_lead_option" value="No" @if($admin->new_lead_option == 'No') checked @endif> No</label>
											<label class="radio-inline"><input type="radio" name="new_lead_option" value="Yes" @if($admin->new_lead_option == 'Yes') checked @endif> Yes</label>
											<?php echo showerrormsg('new_lead_option', $errorarr); ?>
										</div>
										<div class="col-sm-7">
											<input type="email" value="{{ $admin->new_lead_email }}" id="new_lead_email" name="new_lead_email" placeholder="New Lead Email"  class="form-control"/>
											<?php echo showerrormsg('new_lead_email',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="close_deal_option" class="col-sm-3 control-label">Closed Deal Email</label>
										<div class="col-sm-2">
											<label class="radio-inline"><input type="radio" name="close_deal_option" value="No" @if($admin->close_deal_option == 'No') checked @endif> No</label>
											<label class="radio-inline"><input type="radio" name="close_deal_option" value="Yes" @if($admin->close_deal_option == 'Yes') checked @endif> Yes</label>
											<?php echo showerrormsg('close_deal_option', $errorarr); ?>
										</div>
										<div class="col-sm-7">
											<input type="email" value="{{ $admin->close_deal_email }}" id="close_deal_email" name="close_deal_email" placeholder="Closed Deal Email"  class="form-control"/>
											<?php echo showerrormsg('close_deal_email',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="other_option" class="col-sm-3 control-label">Other Email</label>
										<div class="col-sm-2">
											<label class="radio-inline"><input type="radio" name="other_option" value="No" @if($admin->other_option == 'No') checked @endif> No</label>
											<label class="radio-inline"><input type="radio" name="other_option" value="Yes" @if($admin->other_option == 'Yes') checked @endif> Yes</label>
											<?php echo showerrormsg('other_option', $errorarr); ?>
										</div>
										<div class="col-sm-7">
											<input type="email" value="{{ $admin->other_email }}" id="other_email" name="other_email" placeholder="Other Email"  class="form-control"/>
											<?php echo showerrormsg('other_email',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="receive_notification" class="col-sm-3 control-label">Receive Notification</label>
										<div class="col-sm-9">
											<label class="radio-inline"><input type="radio" name="receive_notification" value="Everytime" @if($admin->receive_notification == 'Everytime') checked @endif> Everytime</label>
											<label class="radio-inline"><input type="radio" name="receive_notification" value="Hourly" @if($admin->receive_notification == 'Hourly') checked @endif> Hourly</label>
											<label class="radio-inline"><input type="radio" name="receive_notification" value="Daily" @if($admin->receive_notification == 'Daily') checked @endif> Daily</label>
											<label class="radio-inline"><input type="radio" name="receive_notification" value="Weekly" @if($admin->receive_notification == 'Weekly') checked @endif> Weekly</label>
											<label class="radio-inline"><input type="radio" name="receive_notification" value="Semi-monthly" @if($admin->receive_notification == 'Semi-monthly') checked @endif> Semi-monthly</label>
											<label class="radio-inline"><input type="radio" name="receive_notification" value="Monthly" @if($admin->receive_notification == 'Monthly') checked @endif> Monthly</label>
											<?php echo showerrormsg('receive_notification',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="mobile" class="col-sm-3 control-label">Phone *</label>
										<div class="col-sm-9">
											<input type="text" value="{{ $admin->mobile }}" id="mobile" name="mobile" placeholder="Phone"  class="form-control"/>
											<?php echo showerrormsg('mobile',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="photo" class="col-sm-3 control-label">Upload Avatar ( 180x180 )</label>
										<div class="col-sm-9">
											<input id="photo" name="photo" type="file" class="avatar" accept="image/*">
											<?php echo showerrormsg('photo',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<center><button type="submit" name="Save" id="Save" class="btn btn-success">Save</button></center>
									</div>
								</form>
							</div>
							
							<div class="tab-pane fade" id="profile">
								<form action="{{ route('admin-change-password') }}" method="post" id="admin-change-password" >
									{{ csrf_field() }}
									<div class="form-group">
										<label for="password" class="col-sm-3 control-label">Password *</label>
										<div class="col-sm-9">
											<input id="password" name="password" type="password" value="{{ old('password') }}" placeholder="Password" class="form-control required"/>
											<?php echo showerrormsg('password',$errorarr); ?>
										</div>
									</div>
									<div class="form-group" style="padding-top:60px">
										<label for="password_confirmation" class="col-sm-3 control-label">Confirm Password*</label>
										<div class="col-sm-9">
											<input id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}" type="password" placeholder="Confirm Password " class="form-control required"/>
											<?php echo showerrormsg('password_confirmation',$errorarr); ?>
										</div>
									</div>
									<div class="form-group" style="padding-top:40px">
										<button type="submit" style="margin-left:100px" name="change_password" id="change_password" class="btn btn-success">Change Password</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- row -->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" ></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/adminEdit.js')}}"></script>
<script src="{{asset('assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>
<!-- end of page level js -->
<script>
	$(document).ready(function(){
		//$("[data-mask]").inputmask();
		$("#mobile").inputmask('(999) 999-9999');
		
		$(".form-group").find('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
		
		@if(Session::get('EmailChange')=='Yes')
		swal('Password will get reset now and will send to their new email');
		@php
		Session::put('EmailChange','No');
		@endphp
		@endif
        
		@if($admin->appointment_option == 'No')  
		$('#appointment_email').prop('readonly', true);
		@endif
		
		@if($admin->invoice_option == 'No')  
		$('#invoice_email').prop('readonly', true);
		@endif
		
		@if($admin->new_lead_option == 'No')  
		$('#new_lead_email').prop('readonly', true);
		@endif
		
		@if($admin->close_deal_option == 'No')  
		$('#close_deal_email').prop('readonly', true);
		@endif
		
		@if($admin->other_option == 'No')  
		$('#other_email').prop('readonly', true);
		@endif
		
		$('input[name="appointment_option"]').on('ifChanged',function(){ 
            if($(this).val()=="Yes")
            { 
				$('#appointment_email').prop('readonly', false);
			}
            else
            { 
				$('#appointment_email').prop('readonly', true);
			}            
		});
		
		$('input[name="invoice_option"]').on('ifChanged',function(){ 
            if($(this).val()=="Yes")
            { 
				$('#invoice_email').prop('readonly', false);
			}
            else
            { 
				$('#invoice_email').prop('readonly', true);
			}            
		});
		
		$('input[name="new_lead_option"]').on('ifChanged',function(){ 
            if($(this).val()=="Yes")
            { 
				$('#new_lead_email').prop('readonly', false);
			}
            else
            { 
				$('#new_lead_email').prop('readonly', true);
			}            
		});
		
		$('input[name="close_deal_option"]').on('ifChanged',function(){ 
            if($(this).val()=="Yes")
            { 
				$('#close_deal_email').prop('readonly', false);
			}
            else
            { 
				$('#close_deal_email').prop('readonly', true);
			}            
		});
		
		$('input[name="other_option"]').on('ifChanged',function(){ 
            if($(this).val()=="Yes")
            { 
				$('#other_email').prop('readonly', false);
			}
            else
            { 
				$('#other_email').prop('readonly', true);
			}            
		});
		
        $("#zip").inputmask('99999');
	});
</script>
@stop
