@extends('layouts/default')

{{-- Page title --}} 
@section('title') 
Questions @parent 
@stop 

{{-- page level styles --}} 
@section('header_styles')
<!--start page level css -->
<style>
	.m-t-5
	{
    margin-top: 5px;
	}
</style>
<!--end page level css-->
@stop 
{{-- Page content --}}
@section('content')
<?php 
	function showErrorMsg($error)
	{
		if(count($error)>0)
		{
			$msg='';
			foreach($error as $val)
			{
				$msg.='<small class="help-block animated fadeInUp text-danger" style="color: #FB8678;">'.$val.'</small>';    
			}
			return $msg;        
		}
	}
	//Money Format
	function money_format1($number)
	{
		setlocale(LC_MONETARY, 'en_US'); 
		return money_format('%!.2i',$number); 
	}
?>
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
        <li>
			<a href="{{ route('manage-bot') }}">
				Manage Bot
			</a>
		</li>
		<li class="active">
          <a href="{{ route('questions') }}">
			Manage Questions
          </a>   
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content p-l-r-15">
    <?php $url='add-question'; if(Request::has('edit')) {$url='edit-question';} ?> 
	<form action="{{route($url)}}" method="post" class="form-horizontal">
		<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" />
                
        @if(Request::has('edit'))
        <input type="hidden" name="question_id" id="question_id" value="{{$edit_question->id}}" />
        @endif
        
		@include('panel.includes.status')
        
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-success filterable">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-fw fa-th-large">
							</i>
							@if(Request::has('edit')) Edit @else Add @endif Question
						</h3>
					</div>
					<div class="panel-body">
                        <?php echo showErrorMsg($errors->all()); ?>
						<div class="form-group">
							<label class="control-label col-sm-3">
								Question (English)
							</label>
							<div class="col-sm-9">
								<textarea name="question_en" required="" class="form-control" placeholder="Enter Question In English">@if(Request::has('edit')){{$edit_question->question_en}}@endif</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">
								Question (Spanish)
							</label>
							<div class="col-sm-9">
								<textarea name="question_sp" required="" class="form-control" placeholder="Enter Question In Spanish">@if(Request::has('edit')){{$edit_question->question_sp}}@endif</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">
								Input Format
							</label>
							<div class="col-sm-9">
								<select name="input_format" required="" class="form-control">
									<option value="" disabled="">
										select format
									</option>
									<option @if(Request::has('edit') and $edit_question->input_format=="any") selected="" @endif value="any">
										Alphanumeric
									</option>
									<option @if(Request::has('edit') and $edit_question->input_format=="digits") selected="" @endif value="digits">
										Digits
									</option>
									<option @if(Request::has('edit') and $edit_question->input_format=="email") selected="" @endif value="email">
										Email
									</option>
									<option @if(Request::has('edit') and $edit_question->input_format=="phone_number") selected="" @endif value="phone_number">
										Phone #
									</option>
                                    <option @if(Request::has('edit') and $edit_question->input_format=="address") selected="" @endif value="address">
										Address
									</option>
                                    <option @if(Request::has('edit') and $edit_question->input_format=="zip-code") selected="" @endif value="zip-code">
										Zip Code
									</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">
								Required
							</label>
							<div class="col-sm-9">
								<label class="switch ib switch-custom m-t-5">
									<input type="checkbox" @if(Request::has('edit') and !$edit_question->required)  @else checked="" @endif name="required" id="required" />
									<label for="required" data-on="Yes" data-off="No">
									</label>
								</label>
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="control-label col-sm-3">								
							</label>
							<div class="col-sm-9">
								<button type="submit" class="btn btn-success">@if(Request::has('edit')) Update @else Add @endif Question</button>
							</div>
						</div>
                        <div class="form-group">
							<small class="col-sm-3 text-danger">
								{First_Name} = Person's First Name
							</small>
                        </div>    
                        
					</div>
				</div>
			</div>
		</div>
   </form>     
        <div class="row">
			<div class="col-lg-12">
				<div class="panel panel-success filterable">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-fw fa-th-large">
							</i>
							All Questions
						</h3>
					</div>
					<div class="panel-body">
						<table id="questions" data-toggle="table"  data-search="true" data-show-columns="true" 
						data-pagination="true" data-page-list="[10, 20,40,ALL]">
                            <thead>
								<tr>
									<th>Question (English)</th>
									<th>Question (Spanish)</th>
									<th>Input Format</th>
									<th>Required</th>
                                    <th>Enabled</th>
                                    <th>Sequence</th>
									<th>Date</th> 
									<th class="text-center">Action</th>
								</tr>
							</thead>
                            <tbody>
                                <?php $i=1; ?>
								@foreach($questions as $question)
                                
                                <?php $input_format='';
                                 
                                 switch($question->input_format)
                                 {
                                    case "any" :$input_format='Alphanumeric';break;
                                    case "digits" :$input_format='Digits';break;
                                    case "email" :$input_format='Email';break;
                                    case "phone_number" :$input_format='Phone #';break;
                                    case "address" :$input_format='Address';break;
                                    case "zip-code" :$input_format='Zip Code';break;
                                 }
                                 
                                 ?>
								<tr>
									<td><?php echo nl2br($question->question_en); ?></td>
									<td><?php echo nl2br($question->question_sp); ?></td>
									<td>{{$input_format}}</td>
									<td>@if($question->required){{'Yes'}}@else {{'No'}} @endif</td>
                                    <td>
                                      <label class="switch ib switch-custom m-t-5">
        									<input type="checkbox" @if($question->status) checked="" @endif class="status" id="status{{$i}}" data-id="{{$question->id}}" data-status="{{$question->status}}"   />
        									<label for="status{{$i++}}" data-on="Yes" data-off="No">
        									</label>
 								      </label>
                                    </td>
                                    <td>
                                    <?php if($question->status) { ?>
                                    <select class="form-control input-sm sequence_change">
                                      <?php for($k=1;$k<=$active_questions;$k++) { ?>
                                      <option @if($question->id.'***'.$question->sequence==$question->id.'***'.$k) selected="" @endif value="{{$question->id.'***'.$k}}">{{$k}}</option>
                                      <?php } ?>
                                    </select>
                                    <?php } ?>
                                    </td>
									<td>{{date('m/d/Y h:i A',strtotime($question->updated_at))}}</td>
									<td>
										<a href="#" data-id="{{$question->id}}" class="delete-question"><i class="fa fa-trash" title="Delete"></i></a>
										<a href="?edit={{$question->id}}"><i class="fa fa-edit" title="Edit"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>	
</section>


<!-- /.content -->
@stop 

{{-- page level scripts --}} 
@section('footer_scripts')
<script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/custom_questions.js')}}"></script>
@stop