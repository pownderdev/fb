@extends('layouts/default')

{{-- Page title --}} 
@section('title') 
Manage Bot @parent 
@stop 

{{-- page level styles --}} 
@section('header_styles')
<!--start page level css -->
<style>
	.m-t-5
	{
    margin-top: 5px;
	}
    .cursor-pointer
    {
        cursor: pointer;
    }
    .bg-transparent
    {
        background-color: transparent !important;
    }
    .with-no-border
    {
        border: 0px solid transparent !important;
        box-shadow: none !important;
    }
</style>
<!--end page level css-->
@stop 
{{-- Page content --}}
@section('content')
<?php 
	function showErrorMsg($error)
	{
		if(count($error)>0)
		{
			$msg='';
			foreach($error as $val)
			{
				$msg.='<small class="help-block animated fadeInUp text-danger" style="color: #FB8678;">'.$val.'</small>';    
			}
			return $msg;        
		}
	}
	//Money Format
	function money_format1($number)
	{
		setlocale(LC_MONETARY, 'en_US'); 
		return money_format('%!.2i',$number); 
	}
?>
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">
			Manage Bot
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content p-l-r-15">    
	     @include('panel.includes.status')  
        <div class="row">
        <div class="col-lg-12">
		<div class="panel bg-transparent with-no-border">
          <div class="panel-body">
          
          <div class="row">
          <div class="col-lg-6">
           <a href="{{route('bot-report')}}">                
                <button type="button" title="Bot Report" class="btn btn-warning  green_btn"><i class="fa fa-bar-chart"></i> Bot Report</button>
           </a>
          </div>
          <div class="col-lg-6 text-right"> 
          
           <a href="{{route('questions')}}">                
                <button type="button" title="Manage Questions" class="btn btn-warning  pownder_yellow">Manage Questions</button>
           </a>
           <a href="{{route('answers')}}">                
                <button type="button" title="Manage Answers" class="btn btn-success  pownder_green">Manage Answers</button>
           </a>
           <a href="{{route('unanswered-queries')}}">                
                <button type="button" title="Unanswered Queries" class="btn btn-success  blue_btn">Unanswered Queries</button>
           </a>
           
           </div>
           
           </div>

          </div>
         </div>
         
				<div class="panel panel-success filterable">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-fw fa-th-large">
							</i>
							All Facebook Pages
						</h3>
					</div>
					<div class="panel-body">
						<table id="questions" data-toggle="table"  data-search="true" data-show-columns="true" 	data-pagination="true" data-page-list="[10, 20,40,ALL]">
                            <thead>
								<tr>
									<th>Page Name</th>
									<th>Page Category</th>
									<th>Bot Activated</th>
								</tr>
							</thead>
                            <tbody>
                                <?php $i=1; ?>
								@foreach($fb_pages as $page)
								<tr>
									<td>{{$page->name}}</td>
									<td>{{$page->category}}</td>									
                                    <td> 
                                          @if($page->BotStatus==1)
        							      <a href="{{route('stop-bot',['bot_id'=>$page->BotId,'new_status'=>0])}}"><button type="button"  class="btn btn-success">Yes</button></a>		
                                          <!--<input type="hidden" value='$page_json' class="page_json" />-->
                                          @elseif(!is_null($page->BotStatus))                                          
        							      <a href="{{route('stop-bot',['bot_id'=>$page->BotId,'new_status'=>1])}}"><button type="button"  class="btn btn-danger">Disabled</button></a>		
                                          <!--<input type="hidden" value='$page_json' class="page_json" />-->
                                          @else
                                          <button type="button"  class="btn btn-default activate_bot"   data-page_name="{{$page->name}}" data-page_id="{{$page->id}}"  >No</button>
                                          @endif  
                                    </td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>	
</section>

<!-- Start Bot -->
<div class="modal fade" id="bot_activation" role="dialog">
   <form  method="post" class="form-horizontal form_activate_bot" autocomplete="off"  >
   {{csrf_field()}}
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Bot Activation On <span class="cursor-pointer" title="Click To Copy To Clipboard" id="fb_page_name"></span></h4>
          </div>
          <div class="modal-body">
            <!--<div class="form-group">
              <label class="col-xs-4">Facebook Page ID</label>
              <div class="col-xs-8">
                <input title="Click To Copy To Clipboard" type="text" class="form-control cursor-pointer" name="fb_page_id" id="fb_page_id"  readonly="" />
              </div>
            </div>
            --> 
            <input  type="hidden" name="fb_page_id" id="fb_page_id"  />
            
            <div class="form-group">
              <label class="col-xs-4">Callback URL</label>
              <div class="col-xs-8">
                <input  title="Click To Copy To Clipboard" type="url" class="form-control cursor-pointer" name="dialogflow_callback_url" id="dialogflow_callback_url" value="https://bots.dialogflow.com/facebook/2b0ade83-c4cc-4543-8f55-28e3d65bb5b7/webhook" readonly=""  />
             </div>
            </div>
                         
            <div class="form-group">
              <label class="col-xs-4">Verify Token</label>
              <div class="col-xs-8">
                <input title="Click To Copy To Clipboard" type="text" class="form-control cursor-pointer" name="verify_token"  id="verify_token" value="870fec35-d05e-48b3-b5e3-e44efdff0ed5" readonly="" />
              </div>
            </div> 
            <!--           
            <div class="form-group">
              <label class="col-xs-4">Webhook URL</label>
              <div class="col-xs-8">
                <input title="Click To Copy To Clipboard" type="url" class="form-control cursor-pointer" value="http://big.pownder.com/bot-test/webhook/test.php" name="webhook_url"  id="webhook_url" readonly="" />
              </div>
            </div>
            -->
            <input  type="hidden" name="webhook_url"  id="webhook_url" />
           
            <div class="form-group">
              <label class="col-xs-4">FB Page Access Token</label>
              <div class="col-xs-8">
                <input title="Paste Facebook Page Access Token Here" type="text" placeholder="Paste Facebook Page Access Token Here"  class="form-control" name="fb_page_token" id="fb_page_token"   />
              </div>
            </div>
            
             
            
            <!--
            <div class="form-group">
              <label class="col-xs-4">Client access token</label>
              <div class="col-xs-8"> -->
                <input  type="hidden"   name="dialogflow_client_token" id="dialogflow_client_token"  />
            <!--  </div>
            </div> -
            <div class="form-group">
              <label class="col-xs-4">Developer Access Token</label>
              <div class="col-xs-8">-->
                <input  type="hidden" name="dialogflow_developer_token" id="dialogflow_developer_token"  />
             <!-- </div>
            </div>-->           
          </div>
          <div class="modal-footer">     
            <div class="alert alert-danger hidden bot_errors text-justify "></div> 
            <p class="notifications hidden btn btn-info btn-sm"></p>      
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-success start_bot_btn">Start Bot</button>
          </div>
        </div>
    </div>
  </form>  
</div>
<!--Start Bot -->


<!-- Stop Bot -->
<div class="modal fade" id="bot_deactivation" role="dialog">
   <form  method="post" class="form-horizontal form_deactivate_bot" autocomplete="off"  >
   {{csrf_field()}}
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"><span id="header_text">Bot is Active On</span> <span class="cursor-pointer" title="Double Click To Copy To Clipboard" id="de_fb_page_name"></span></h4>
          </div>
          <div class="modal-body">
            <!--<div class="form-group">
              <label class="col-xs-4">Facebook Page ID</label>
              <div class="col-xs-8">
                <input title="Double Click To Copy To Clipboard" type="text" class="form-control cursor-pointer" name="fb_page_id" id="fb_page_id"  readonly="" />
              </div>
            </div>
            --> 
            <input  type="hidden" name="fb_page_id" id="de_fb_page_id"  />
            <input  type="hidden" name="bot_id" id="bot_id"  />
            <input  type="hidden" name="bot_status" id="bot_status"  />
                         
            <div class="form-group">
              <label class="col-xs-4">Verify Token</label>
              <div class="col-xs-8">
                <input title="Double Click To Copy To Clipboard" type="text" class="form-control cursor-pointer" name="verify_token"  id="de_verify_token" readonly="" />
              </div>
            </div>            
            <div class="form-group">
              <label class="col-xs-4">Webhook URL</label>
              <div class="col-xs-8">
                <input title="Double Click To Copy To Clipboard" type="url" class="form-control cursor-pointer" value="http://big.pownder.com/bot-test/webhook/test.php" name="webhook_url"  id="de_webhook_url" readonly="" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-xs-4">FB Page Access Token</label>
              <div class="col-xs-8">
                <input title="Paste Facebook Page Access Token Here" type="text"  class="form-control" name="fb_page_token" id="de_fb_page_token" readonly=""   />
              </div>
            </div>
            <!--
            <div class="form-group">
              <label class="col-xs-4">Callback URL</label>
              <div class="col-xs-8"> -->
                <input type="hidden"  name="dialogflow_callback_url" id="de_dialogflow_callback_url"  />
            <!--  </div>
            </div> -->
            <!--
            <div class="form-group">
              <label class="col-xs-4">Client access token</label>
              <div class="col-xs-8">-->
                <input  type="hidden"   name="dialogflow_client_token" id="de_dialogflow_client_token" readonly=""  />
            <!--  </div>
            </div>-->
            <div class="form-group">
              <label class="col-xs-4">Developer Access Token</label>
              <div class="col-xs-8">
                <input title="Paste Dialogflow Developer Access Token Here" type="text"  class="form-control" name="dialogflow_developer_token" id="de_dialogflow_developer_token" readonly=""  />
              </div>
            </div>           
          </div>
          <div class="modal-footer">     
            <div class="alert alert-danger hidden bot_errors text-justify "></div>       
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-danger stop_bot_btn">Stop Bot</button>
          </div>
        </div>
    </div>
  </form>  
</div>
<!--Stop Bot -->

<!-- /.content -->
@stop 

{{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/custom_manage_bot.js')}}"></script>
@stop