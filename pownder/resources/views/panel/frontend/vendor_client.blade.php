@extends('layouts/default')

{{-- Page title --}}
@section('title')
Clients list
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/js/sumoselect/sumoselect.css')}}">
<link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet" type="text/css"/>
<!--<link href="{{asset('assets/css/menubarfold.css')}}" rel="stylesheet">-->

<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	.fixed-table-container {height: 500px !important; }
	.export.btn-group {*display:none!important;}
	.date_range {
	margin: 1% 0;
	margin-top: 10px;
	float: right;
	width: 262px;
	}
	.fixed-table-toolbar {*width:60%;}
	.fixed-table-toolbar .search {width: Calc( 100% - 500px );}
	
	#crm_modal {
	background: rgba(0, 0, 0, 0.701961);
	box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4);
	*bottom: auto;
	}
	
	#crm_modal .modal-dialog {
    top: 50%;
    margin: 0 0 0 50%;
    transform: translate(-50%, -50%);
	}
	
	@media screen and (max-width: 767px) {
	.date_range {
	float: left;
	width: 98%;
	}
	.fixed-table-toolbar {
	width: 100%;
	}
	.fixed-table-toolbar {width:100%;}
	.fixed-table-toolbar .search {width: Calc( 100% );}
	}
	
	.SumoSelect {
    width: 100% !important;
	color: #000 !important;
	}    
	
	.SelectBox {
    padding: 7px 12px !important;
	}
	
	.options{
	max-height: 150px !important;
	}
	
	.SumoSelect .select-all {
    height: 35px !important;
	}
</style>
<style>
	/* Absolute Center Spinner */
	.loading {
	position: fixed;
	z-index: 99999 !important;
	height: 2em;
	width: 2em;
	overflow: show;
	margin: auto;
	top: 0;
	left: 0;
	bottom: 0;
	right: 0;
	}
	
	/* Transparent Overlay */
	.loading:before {
	content: '';
	display: block;
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background-color: rgba(0,0,0,0.3);
	}
	
	/* :not(:required) hides these rules from IE9 and below */
	.loading:not(:required) {
	/* hide "loading..." text */
	font: 0/0 a;
	color: transparent;
	text-shadow: none;
	background-color: transparent;
	border: 0;
	}
	
	.loading:not(:required):after {
	content: '';
	display: block;
	font-size: 10px;
	width: 1em;
	height: 1em;
	margin-top: -0.5em;
	-webkit-animation: spinner 1500ms infinite linear;
	-moz-animation: spinner 1500ms infinite linear;
	-ms-animation: spinner 1500ms infinite linear;
	-o-animation: spinner 1500ms infinite linear;
	animation: spinner 1500ms infinite linear;
	border-radius: 0.5em;
	-webkit-box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.5) -1.5em 0 0 0, rgba(0, 0, 0, 0.5) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
	box-shadow: rgba(0, 0, 0, 0.75) 1.5em 0 0 0, rgba(0, 0, 0, 0.75) 1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) 0 1.5em 0 0, rgba(0, 0, 0, 0.75) -1.1em 1.1em 0 0, rgba(0, 0, 0, 0.75) -1.5em 0 0 0, rgba(0, 0, 0, 0.75) -1.1em -1.1em 0 0, rgba(0, 0, 0, 0.75) 0 -1.5em 0 0, rgba(0, 0, 0, 0.75) 1.1em -1.1em 0 0;
	}
	
	/* Animation */
	
	@-webkit-keyframes spinner {
	0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
	}
	100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
	}
	}
	@-moz-keyframes spinner {
	0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
	}
	100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
	}
	}
	@-o-keyframes spinner {
	0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
	}
	100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
	}
	}
	@keyframes spinner {
	0% {
    -webkit-transform: rotate(0deg);
    -moz-transform: rotate(0deg);
    -ms-transform: rotate(0deg);
    -o-transform: rotate(0deg);
    transform: rotate(0deg);
	}
	100% {
    -webkit-transform: rotate(360deg);
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
	}
	}
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">
			Clients list
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content"> 
    @include('panel.includes.status')
	
	<?php
		$errorarr=$errors;   
		function showerrormsg($key,$errors)
		{    
			$msg='';
			if(count($errors->get($key))>0)
			{ 
				$msg.='';
				foreach($errors->get($key) as $error)
				{
					if($key=="alert_success")
					{
						$msg.=$error;
					}
					else
					{
						$msg.='<small class="text-danger animated fadeInUp">'.$error.'</small>' ; 
					}
				}
			} 
			return $msg;     
		}
	?>
	<!--fourth table start-->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Clients List
					</h3>
				</div>
				<div class="panel-body">
					<div class="form-group date_range" style="">
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-fw fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right " id="date-range0"
							placeholder="MM-DD-YYYY to MM-DD-YYYY"  />
						</div>
					</div>
					<div class="loading" style="display:none">Loading&#8230;</div>
					<table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20, 40, ALL]" data-show-footer="false">
						<thead>
                            <tr>
                                <th data-field="ID" data-sortable="true" @if(in_array("0",$ClientColumns)) data-visible="false" @endif>ID</th>
                                <th data-field="Date" data-sortable="true" @if(in_array("1",$ClientColumns)) data-visible="false" @endif>Date</th>
                                <th data-field="Status" data-sortable="true" @if(in_array("2",$ClientColumns)) data-visible="false" @endif>Status</th>
                                <th data-field="Client" data-sortable="true" @if(in_array("3",$ClientColumns)) data-visible="false" @endif>Client</th>
                                <th data-field="Package" data-sortable="true" @if(in_array("5",$ClientColumns)) data-visible="false" @endif>Package</th>
                                <th data-field="Ads" data-sortable="true" @if(in_array("6",$ClientColumns)) data-visible="false" @endif>Ads</th>
                                <th data-field="CPs" data-sortable="true" @if(in_array("7",$ClientColumns)) data-visible="false" @endif>CPs</th>
                                <th data-field="LDs" data-sortable="true" @if(in_array("8",$ClientColumns)) data-visible="false" @endif>LDs</th>
                                <th data-field="CRM" @if(in_array("9",$ClientColumns)) data-visible="false" @endif>CRM</th>
								<th data-field="Action" @if(in_array("10",$ClientColumns)) data-visible="false" @endif>Action</th>
							</tr>
						</thead>
						<tbody>
                            <?php $i=1; ?>
                            @foreach($clientsdata as $data)
							<tr>
                                <td>{{ $data->id }}</td>
                                <td>{{ date('m-d-Y',strtotime($data->created_at)) }}</td>
                                <td>@if($data->status==1) Active @else Inactive @endif</td>
                                <td>{{ $data->name }}</td>
								<td>{{ $data->package_name }}</td>
                                <td>{{ ClientHelper::ClientAds($data->id) }}</td>
                                <td>{{ ClientHelper::ClientCPs($data->id) }}</td>
                                <td><a href="leads?client_id={{ $data->id }}">{{ ClientHelper::ClientLDs($data->id) }}</a></td>
								<td>
									@if(ClientHelper::ClientCampaign($data->id)!=0)
									<i class="fa fa-pencil-square-o actions_icon" aria-hidden="true" title="Create" onClick="CreateCRM('{{ $data->id }}')"></i>
									<a href="crm_account/{{ $data->id }}" title="View">
										<i class="fa fa-file-text-o" aria-hidden="true"></i>
									</a>
									@endif
								</td>
                                <td>
									<a href="vclients?id={{ $data->id }}">
										<i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit Client"></i>
									</a>
									<a href="#" id="{{ $data->id }}" class="delete_vendor_client">
										<i class="fa fa-fw fa-times text-danger actions_icon" title="Delete Client"></i>
									</a>
									@if($data->status==1)
									<a href="#" inactive_id="{{ $data->id }}" class="inactive_vendor_client">
										<i class="fa fa-fw fa-star text-danger actions_icon" title="Inactive Client"></i>
									</a>
									@else
									<a href="#" active_id="{{ $data->id }}" class="active_vendor_client">
										<i class="fa fa-fw fa-star text-success actions_icon" title="Active Client"></i>
									</a>
									@endif
								</td>
							</tr> 
							@endforeach                                                                                    
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!--fourth table end-->
</section>

<!-- Modals Section-->
<div id="add_client_modal" class="modal fade" role="dialog">
	<form action="{{ route('add_vendor_client') }}" method="post" id="add_client" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" style="color:#fff;opacity: 0.8;">&times;</button>
					<h4 class="modal-title">Add Client</h4>
				</div>
				
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="name">Name</label>
								<input value="{{ old('name') }}" type="text" class="form-control" id="name" name="name" placeholder="Name">
								<?php echo showerrormsg('name',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="address">Address</label>
								<input value="{{ old('address') }}" type="text" class="form-control" id="address" name="address" placeholder="Address">
								<?php echo showerrormsg('address',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="zip">Zip</label>
								<input value="{{ old('zip') }}" type="text" class="form-control" id="zip" name="zip" placeholder="Zip" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<?php echo showerrormsg('zip',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="mobile">Phone</label>
								<input value="{{ old('mobile') }}" type="text" class="form-control" id="mobile" name="mobile" placeholder="Phone" data-inputmask='"mask": "(999) 999-9999"' data-mask/>
								<?php echo showerrormsg('mobile',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12  form_field form-group">
								<label for="email">Email</label>
								<input value="{{ old('email') }}" type="email" class="form-control" id="email" name="email" placeholder="Email">
								<?php echo showerrormsg('email',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="lead_form">Facebook Lead Form</label>
								<select multiple="multiple" placeholder="Select Lead Form" name="lead_form[]" id="lead_form" class="select1 SumoUnder">
									@foreach($FacebookAdsLeads as $FacebookAdsLead)
									<option  value="{{ $FacebookAdsLead->id }}">{{ $FacebookAdsLead->name }}</option>
									@endforeach
								</select>
								<?php echo showerrormsg('lead_form',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="contact_name">Contact Name</label>
								<input value="{{ old('contact_name') }}" type="text" class="form-control" id="contact_name" name="contact_name" placeholder="Contact Name">
								<?php echo showerrormsg('contact_name',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="package_id">Package</label>
								<select id="package_id" name="package_id" class="form_select">
									<option value="">Select Package</option>
									@foreach($packages as $package)
									<option value="{{ $package->id }}">{{ $package->name }}</option>
									@endforeach
								</select>
								<?php echo showerrormsg('package_id',$errorarr); ?>
							</div>
							
							<div class="col-md-4 col-sm-12 form_field form-group PerSoldValue" style="display:none">
								<label for="per_sold_value">Per Sold Value</label>
								<input value="{{ old('per_sold_value') }}" id="per_sold_value" name="per_sold_value" type="text" class="form-control" placeholder="Per Sold Value" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<input id="package_type" name="package_type" type="hidden">
								<small class="text-danger animated fadeInUp per_sold_value"></small>
							</div>
							
							<div class="col-md-4 col-sm-12 form_field form-group Discount">
								<label for="discount">Discount (Amount $)</label>
								<input value="{{ old('discount') }}" id="discount" name="discount" type="text" class="form-control" placeholder="Discount Amount" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<small class="text-danger animated fadeInUp discount_amount"></small>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="url">URL</label>
								<input value="{{ old('url') }}" type="url" class="form-control" id="url" name="url" placeholder="Url">
								<?php echo showerrormsg('url',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="group_category_id">Group Category</label>
								<select id="group_category_id" name="group_category_id" class="form-control" placeholder="Select Group Category">
									<option value="">Select Group Category</option> 
									@foreach($GroupCategories as $GroupCategory)
									<option value="{{ $GroupCategory->id }}">{{ $GroupCategory->name }}</option>
									@endforeach
								</select>
								<?php echo showerrormsg('group_category_id',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="group_sub_category_id">Group Sub-Category</label>
								<select id="group_sub_category_id" name="group_sub_category_id" class="form-control" placeholder="Select Group Sub-Category">
									<option value="">Select Group Sub-Category</option> 
								</select>
								<?php echo showerrormsg('group_sub_category_id',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="is_override">Override</label>
								<select id="is_override" name="is_override" class="form_select">
									<option value="No" @if(old('is_override')=="No") selected  @endif>No</option>
									<option value="Yes" @if(old('is_override')=="Yes") selected  @endif>Yes</option>
								</select>
								<?php echo showerrormsg('is_override',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="crm_email">CRM Email</label>
								<input value="{{ old('crm_email') }}" id="crm_email" name="crm_email" type="email" class="form-control" placeholder="CRM Email">
								<?php echo showerrormsg('crm_email',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="photo">Upload Avatar ( 180x180 )</label>
								<input id="photo" name="photo" type="file" class="avatar" accept="image/*">
								<?php echo showerrormsg('photo',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="roi_automation">ROI Automation</label><br>
								<label class="radio-inline"><input type="radio" name="roi_automation" value="No" checked> No </label>
								<label class="radio-inline"><input type="radio" name="roi_automation" value="Yes"> Yes </label><br>
								<?php echo showerrormsg('roi_automation',$errorarr); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="add_client_submit" style="display:none";></button>
					<button class="btn btn-default addClientNew"  style=" color:#fff; background-color: #ffbe18!important;
					border-color: #e4a70c!important;"> Add Client</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"> Cancel</button>
				</div>
			</div>
		</div>
	</form>
</div>

@if(app('request')->exists('id'))
<!-- Modals Section-->
<div id="edit_client_modal" class="modal fade" role="dialog">
	<form action="{{ route('edit_vendor_client') }}" method="post" id="edit_client" enctype="multipart/form-data">
		{{ csrf_field() }}
		<div class="modal-dialog modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" style="color:#fff;opacity: 0.8;">&times;</button>
					<h4 class="modal-title">Edit Client</h4>
				</div>
				
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_name">Name</label>
								<input value="@if(app('request')->exists('id')){{ $editdata->name }}@endif" type="text" class="form-control" id="client_name" name="client_name" placeholder="Name">
								<input value="@if(app('request')->exists('id')){{ $editdata->id }}@endif" id="client_id" type="hidden" name="client_id">
								<?php echo showerrormsg('client_name',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_address">Address</label>
								<input value="@if(app('request')->exists('id')){{ $editdata->address }}@endif" type="text" class="form-control" id="client_address" name="client_address" placeholder="Address">
								<?php echo showerrormsg('client_address',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_zip">Zip</label>
								<input value="@if(app('request')->exists('id')){{ $editdata->zip }}@endif" type="text" class="form-control" id="client_zip" name="client_zip" placeholder="Zip" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<?php echo showerrormsg('client_zip',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_mobile">Phone</label>
								<input value="@if(app('request')->exists('id')){{ $editdata->mobile }}@endif" type="text" class="form-control" id="client_mobile" name="client_mobile" placeholder="Phone" data-inputmask='"mask": "(999) 999-9999"' data-mask/>
								<?php echo showerrormsg('client_mobile',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12  form_field form-group">
								<label for="client_email">Email</label>
								<input value="@if(app('request')->exists('id')){{ $editdata->email }}@endif" type="email" class="form-control" id="client_email" name="client_email" placeholder="Email">
								<?php echo showerrormsg('client_email',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_lead_form">Facebook Ads Lead</label>
								<select multiple="multiple" placeholder="Select Lead Form" name="client_lead_form[]" id="client_lead_form" class="select1 SumoUnder">
									<?php
										if(app('request')->exists('id')){
											$LeadForm_Array=explode(',',$editdata->facebook_ads_lead_id);
										}
										else{
											$LeadForm_Array=array();
										}
									?>
									@foreach($EditFacebookAdsLeads as $FacebookAdsLead)
									<option  value="{{ $FacebookAdsLead->id }}" @if(in_array($FacebookAdsLead->id, $LeadForm_Array)) {{ 'selected' }} @endif>{{ $FacebookAdsLead->name }}</option>
									@endforeach
								</select>
								<?php echo showerrormsg('client_lead_form',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_contact_name">Contact Name</label>
								<input value="@if(app('request')->exists('id')){{ $editdata->contact_name }}@endif" type="text" class="form-control" id="client_contact_name" name="client_contact_name" placeholder="Contact Name">
								<?php echo showerrormsg('client_contact_name',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_package_id">Package</label>
								<select id="client_package_id" name="client_package_id" class="form_select">
									<option value="">Select Package</option>
									@foreach($packages as $package)
									<option value="{{ $package->id }}" @if(app('request')->exists('id') && $editdata->package_id==$package->id) selected  @endif>{{ $package->name }}</option>
									@endforeach
								</select>
								<?php echo showerrormsg('client_package_id',$errorarr); ?>
							</div>
							
							<div class="col-md-4 col-sm-12 form_field form-group ClientPerSoldValue" style="display:none">
								<label for="client_per_sold_value">Per Sold Value</label>
								<input value="@if(app('request')->exists('id')){{ $editdata->per_sold_value }}@endif" id="client_per_sold_value" name="client_per_sold_value" type="text" class="form-control" placeholder="Per Sold Value" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<input id="client_package_type" name="client_package_type" type="hidden">
								<small class="text-danger animated fadeInUp client_per_sold_value"></small>
							</div>
							
							<div class="col-md-4 col-sm-12 form_field form-group ClientDiscount" style="display:none">
								<label for="client_discount">Discount (Amount $)</label>
								<input value="@if(app('request')->exists('id')){{ $editdata->discount }}@endif" id="client_discount" name="client_discount" type="text" class="form-control" placeholder="Discount Amount" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
								<small class="text-danger animated fadeInUp client_discount_amount"></small>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_url">URL</label>
								<input value="@if(app('request')->exists('id')){{ $editdata->url }}@endif" type="url" class="form-control" id="client_url" name="client_url" placeholder="Url">
								<?php echo showerrormsg('client_url',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_group_category_id">Group Category</label>
								<select id="client_group_category_id" name="client_group_category_id" class="form-control" placeholder="Select Group Category">
									<option value="">Select Group Category</option> 
									@foreach($GroupCategories as $GroupCategory)
									<option value="{{ $GroupCategory->id }}" @if(app('request')->exists('id') && $editdata->group_category_id == $GroupCategory->id) selected @endif>{{ $GroupCategory->name }}</option>
									@endforeach
								</select>
								<?php echo showerrormsg('client_group_category_id',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_group_sub_category_id">Group Sub-Category</label>
								<select id="client_group_sub_category_id" name="client_group_sub_category_id" class="form-control" placeholder="Select Group Sub-Category">
									<option value="">Select Group Sub-Category</option> 
									@foreach($GroupSubCategories as $GroupSubCategory)
									<option value="{{ $GroupSubCategory->id }}" @if(app('request')->exists('id') && $editdata->group_sub_category_id == $GroupSubCategory->id) selected @endif>{{ $GroupSubCategory->name }}</option>
									@endforeach
								</select>
								<?php echo showerrormsg('client_group_sub_category_id',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_is_override">Override</label>
								<select id="client_is_override" name="client_is_override" class="form_select">
									<option value="No" @if(app('request')->exists('id') && $editdata->is_override=="No") selected @endif>No</option>
									<option value="Yes" @if(app('request')->exists('id') && $editdata->is_override=="Yes") selected @endif>Yes</option>
								</select>
								<?php echo showerrormsg('client_is_override',$errorarr); ?>
							</div>
							
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_crm_email">CRM Email</label>
								<input value="@if(app('request')->exists('id')){{ $editdata->crm_email }}@endif" id="client_crm_email" name="client_crm_email" type="email" class="form-control" placeholder="CRM Email">
								<?php echo showerrormsg('client_crm_email',$errorarr); ?>
							</div>
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_photo">Upload Avatar ( 180x180 )</label>
								<input id="client_photo" name="client_photo" type="file" class="avatar" accept="image/*">
								<?php echo showerrormsg('client_photo',$errorarr); ?>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="col-md-4 col-sm-12 form_field form-group">
								<label for="client_roi_automation">ROI Automation</label><br>
								<label class="radio-inline"><input type="radio" name="client_roi_automation" value="No" @if(app('request')->exists('id') && $editdata->roi_automation=="No") checked @endif> No </label>
								<label class="radio-inline"><input type="radio" name="client_roi_automation" value="Yes" @if(app('request')->exists('id') && $editdata->roi_automation=="Yes") checked @endif> Yes </label><br>
								<?php echo showerrormsg('client_roi_automation',$errorarr); ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="modal-footer">
					<button type="submit" class="edit_client" style="display:none";></button>
					<button  class="btn btn-default editClientExisting"  style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c!important;"> Update Client</button>
					<button type="button" class="btn btn-default" data-dismiss="modal"> Cancel</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endif

<!-- crm_modal -->
<div id="crm_modal" class="modal fade animated" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gred_2">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Create CRM</h4>
			</div>
			<form role="form" id="crm_form">
				<div class="modal-body">
					<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="crm_client_id" id="crm_client_id">
					<strong class="text-danger animated create_crm fadeInUp crm_error"></strong>
					<div class="row m-t-10">
						<div class="col-md-12">
							<div class="input-group" style="width: 100%;">
								<label for="campaign">Campaign</label>
								<select name="campaign" id="campaign" class="form-control">
								</select>
								<br /><small class="text-danger animated campaign fadeInUp crm_error"></small>
							</div>
						</div>
					</div>
					<div class="row m-t-10" id="hide_crm_account">
						<div class="col-md-12">
							<div class="input-group" style="width: 100%;">
								<label for="crm_account">CRM Account</label>
								<select name="crm_account" id="crm_account" class="form-control">
								</select>
								<br /><small class="text-danger animated crm_account fadeInUp crm_error"></small>
							</div>
						</div>
					</div>
					<div class="row m-t-10" id="agilecrm" style="display:none">
						<div class="col-md-12">
							<label for="crm_domain">CRM Domain</label>
							<div class="input-group" style="width: 100%;">
								<input type="text" name="crm_domain" id="crm_domain"
								placeholder="CRM Domain" class="form-control">
								<span class="input-group-addon">.agilecrm.com</span>
							</div>
							<small class="text-danger animated crm_domain fadeInUp crm_error"></small>
						</div>
					</div>
					<div class="row m-t-10">
						<div class="col-md-12">
							<div class="input-group" style="width: 100%;">
								<label for="crm_username">CRM Username</label>
								<input type="text" name="crm_username" id="crm_username"
								placeholder="CRM Username" class="form-control">
								<br /><small class="text-danger animated crm_username fadeInUp crm_error"></small>
							</div>
						</div>
					</div>
					<div class="row m-t-10">
						<div class="col-md-12">
							<div class="input-group" style="width: 100%;">
								<label for="crm_password">CRM Password</label>
								<input type="text" name="crm_password" id="crm_password"
								placeholder="CRM Password" class="form-control">
								<br /><small class="text-danger animated crm_password fadeInUp crm_error"></small>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-succes crm-submit">Submit</button>
					<button type="reset" class="btn btn-default">Reset</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- form-modal end -->

@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<script src="{{asset('assets/js/sumoselect/jquery.sumoselect.min.js')}}" type="text/javascript" charset="utf-8"></script>

<script>
	$( document ).ready(function() {
		var extra_features = '<button class="btn btn-default add_client " type="button" name="mail" title="Create"  ><i class="fa fa-user-plus"></i></button><button class="btn btn-default" type="button" name="mail" title="Mail" ><i class="fa fa-fw fa-envelope"></i></button><button class="btn btn-default" type="button" name="print" title="Print" ><i class="fa fa-fw fa-print"></i></button>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
		
		$('.add_client').click( function(){
			//alert("ok");
			$('#add_client_modal').modal("show");
		});
		
		$('#lead_form').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		$('#client_lead_form').SumoSelect({search: true, okCancelInMulti:true, selectAll:true });
		
		<?php if(app('request')->exists('id')) { ?>
			$('#edit_client_modal').modal("show");
			<?php }elseif(count($errors->all())>0) { ?>
			$('.add_client').trigger('click');
		<?php } ?>
	});	
</script>

<!-- InputMask -->
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.date.extensions.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.extensions.js')}}" type="text/javascript"></script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<!-- bootstrap time picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/timedropper/js/timedropper.js')}}"></script>

<script>
	$('#date-range0').dateRangePicker({
		autoClose: true,
		format: 'MM-DD-YYYY',
		getValue: function () {
			var range = $(this).val();
			//alert(range);
		}
	});
</script>

<script>
	$(document).ready(function(){
		$('.dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Client Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$('#package_id').change(function(){
			var package_id=$(this).val();
			if(package_id!='')
			{
				$.get("{{ route('check-package-type') }}",{package_id: package_id},function(data){  
					if(data=='Sold')
					{
						$('.PerSoldValue').css('display','block');
						$('.Discount').css('display','none');
						$('#package_type').val(data);
						$('#discount').val('');
					}
					else
					{
						$('.PerSoldValue').css('display','none');
						$('.Discount').css('display','block');
						$('#package_type').val(data);
						$('#per_sold_value').val('');
					}
				});
			}
			else
			{
				$('.PerSoldValue').css('display','none');
				$('.Discount').css('display','none');
				$('#package_type').val('');
				$('#discount').val('');
				$('#per_sold_value').val('');
			}
		});
		
		$('#client_package_id').change(function(){
			var package_id=$(this).val();
			if(package_id!='')
			{
				$.get("{{ route('check-package-type') }}",{package_id: package_id},function(data){ 
					if(data=='Sold')
					{
						$('.ClientPerSoldValue').css('display','block');
						$('.ClientDiscount').css('display','none');
						$('#client_package_type').val(data);
						$('#client_discount').val('');
					}
					else
					{
						$('.ClientDiscount').css('display','block');
						$('.ClientPerSoldValue').css('display','none');
						$('#client_package_type').val(data);
						$('#client_per_sold_value').val('');
					}
				});
			}
			else
			{
				$('.ClientPerSoldValue').css('display','none');
				$('.ClientDiscount').css('display','none');
				$('#client_package_type').val(data);
				$('#client_discount').val('');
				$('#client_per_sold_value').val('');
			}
		});
		
		@if(app('request')->exists('id'))
		@if($editdata->package_id!='')
		$.get("{{ route('check-package-type') }}",{package_id: "{{ $editdata->package_id }}"},function(data){ 
			if(data=='Sold')
			{
				$('.ClientPerSoldValue').css('display','block');
				$('.ClientDiscount').css('display','none');
				$('#client_package_type').val(data);
			}
			else
			{
				$('.ClientDiscount').css('display','block');
				$('.ClientPerSoldValue').css('display','none');
				$('#client_package_type').val(data);
			}
		});
		@else
		$('.ClientPerSoldValue').css('display','none');
		$('#client_package_type').val(data);
		$('#client_discount').val('');
		$('#client_per_sold_value').val('');
		@endif
		@endif
		
		$("[data-mask]").inputmask();
	});	
</script>

<script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/inner_pages_validation.js')}}"></script>
<script src="{{asset('assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".form_field").find('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});  
		
		$("#group_category_id").change(function(){
			$.get( "{{ route('get_group_sub_category') }}", { group_category_id: $(this).val() } )
			.done(function( data ) {
				var innerhtm='<option value="">Select Group Sub-Category</option>';
				
				for(var i=0;i<data.length;i++)
				{
					innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				
				$('#group_sub_category_id').html(innerhtm);
			});
		});
		
		$("#client_group_category_id").change(function(){
			$.get( "{{ route('get_group_sub_category') }}", { group_category_id: $(this).val() } )
			.done(function( data ) {
				var innerhtm='<option value="">Select Group Sub-Category</option>';
				
				for(var i=0;i<data.length;i++)
				{
					innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				
				$('#client_group_sub_category_id').html(innerhtm);
			});
		});
		
		$("#crm_account").change(function(){
			crm_account= $(this).val();
			if(crm_account=='Agile')
			{
				$('#agilecrm').css('display','block');
			}
			else
			{
				$('#agilecrm').css('display','none');
			}
		});
		
		$(".addClientNew").click(function(event){
			event.preventDefault();
			$('.loading').css('display', 'block');
			$(".addClientNew").prop('disabled', true);
			var error='no';
			$('.per_sold_value').html('');
			$('.discount_amount').html('');
			var package_id=$('#package_id').val();
			var package_type=$('#package_type').val();
			var per_sold_value=$('#per_sold_value').val();
			var discount=$('#discount').val();
			if(package_id!='')
			{
				if(package_type=='Sold')
				{
					if(per_sold_value=='')
					{
						error='yes';
						$('.per_sold_value').html('The per sold value is required');
						$(".addClientNew").prop('disabled', false);
					}
					else
					{
						$( ".add_client_submit" ).trigger('click');
						$(".addClientNew").prop('disabled', false);
					}
				}
				else
				{
					$.get("{{ route('check-client-discount') }}",{package_id: package_id,discount_amount: discount},function(data){ 
						if(data!='No')
						{
							error='yes';
							$('.discount_amount').html(data);
							$(".addClientNew").prop('disabled', false);
						}
						else
						{
							$( ".add_client_submit" ).trigger('click');
							$(".addClientNew").prop('disabled', false);
						}
					});
				}
			}
			else
			{
				$( ".add_client_submit" ).trigger('click');
				$(".addClientNew").prop('disabled', false);
			}
			$('.loading').css('display', 'none');
		});
		
		$(".editClientExisting").click(function(event){
			event.preventDefault();
			$('.loading').css('display', 'block');
			$(".editClientExisting").prop('disabled', true);
			
			var error='no';
			$('.client_per_sold_value').html('');
			$('.client_discount_amount').html('');
			
			var client_package_id=$('#client_package_id').val();
			var client_discount=$('#client_discount').val();
			
			var client_package_type=$('#client_package_type').val();
			var client_per_sold_value=$('#client_per_sold_value').val();
			
			if(client_package_id!='')
			{
				if(client_package_type=='Sold')
				{
					if(client_per_sold_value=='')
					{
						error='yes';
						$('.client_per_sold_value').html('The per sold value is required');
						$(".editClientExisting").prop('disabled', false);
					}
					else{
						$( ".edit_client" ).trigger('click');
						$(".editClientExisting").prop('disabled', false);
					}
				}
				else
				{
					$.get("{{ route('check-client-discount') }}",{package_id: client_package_id,discount_amount: client_discount},function(data){ 
						if(data!='No')
						{
							error='yes';
							$('.client_discount_amount').html(data);
							$(".editClientExisting").prop('disabled', false);
						}
						else{
							$( ".edit_client" ).trigger('click');
							$(".editClientExisting").prop('disabled', false);
						}
					});
				}
			}
			else
			{
				$( ".edit_client" ).trigger('click');
				$(".editClientExisting").prop('disabled', false);
			}
			
			$('.loading').css('display', 'none');
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
	    $("#crm_form").on("submit", function( event ) {
			event.preventDefault();
			$(".crm-submit").prop('disabled', true);
			$('.crm_error').html('');
			var data=$(this).serialize();
			
	        $.ajax({
	            url: "{{ route('add_crm_account') }}",
	            type:'POST',
	            data: data,
	            success: function(data) {
	                if(data=='success')
					{
						$('#crm_modal').modal("hide");
						$(".crm-submit").prop('disabled', false);
						swal('Success!', 'CRM account created successfully.', 'success');
					}
					else
					{
						$(".create_crm").html(data);
						$(".crm-submit").prop('disabled', false);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$("."+key).html(value);
					});
					$(".crm-submit").prop('disabled', false);
				}       
			});
		}); 
	});
	
	function CreateCRM(client_id)
	{
		$('.crm_error').html('');
		$('#crm_client_id').val(client_id);
		$('#crm_account').html('<option value="">Select CRM Account</option>');
		$('#crm_domain').val('');
		$('#crm_username').val('');
		$('#crm_password').val('');
		$('#agilecrm').css('display','none');
		
		$.get("{{ route('client_campaign') }}",
		{
			client_id: client_id
		},
		function(data){
			var innerhtm='<option value="">Select Campaign</option>';
			for(var i=0;i<data.length;i++)
			{
				innerhtm +='<option value="'+data[i].id+'">'+data[i].campaign_name+'</option>';
			}
			$('#campaign').html(innerhtm);
		});
		$('#crm_modal').modal("show");
	}
	
	$("#campaign").change(function() {
		var campaign_id=$(this).val();
		if(campaign_id!='')
		{
			$.get("{{ route('client_crm_account') }}",
			{
				campaign_id: campaign_id
			},
			function(data){
				if(data[0].crm_account==null || data[0].crm_account=='')
				{
					$('#crm_account').html('<option value="">Select CRM Account</option> <option value="Agile">Agile</option> <option value="Zoho">Zoho</option>');
				}
				else
				{
					$('#crm_account').html('<option value="">Select CRM Account</option><option value="'+data[0].crm_account+'">'+data[0].crm_account+'</option>');
				}
			});
		}
		else
		{
			$('#crm_account').html('<option value="">Select CRM Account</option>');
			$('#crm_domain').val('');
			$('#agilecrm').css('display','none');			
		}
	});
</script>

@stop								