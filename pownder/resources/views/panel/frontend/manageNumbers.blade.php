@extends('layouts/default')

{{-- Page title --}}
@section('title')
Leads
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/bootstrap-table/css/bootstrap-table.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_css/bootstrap_tables.css')}}">


<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/daterangepicker/css/daterangepicker.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/jquerydaterangepicker/css/daterangepicker.min.css')}}">


<!--<link href="{{asset('assets/css/menubarfold.css')}}" rel="stylesheet">-->

<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	
	.export.btn-group {
	*display:none!important;
	}
	.date_range {
	margin: 1%;
	margin-top: 10px;
	float: right;
	width: 38%;
	}
	.table-summary {
	display:inline-block;
	border: 1px solid #ccc;
	margin-top: 15px;
	
	}
	
	.table-summary .wrapp {
	float:left;
	text-align:center;
	width:100px;
	
	}
	.table-summary .wrapp .title{
	padding: 5px 0;
	font-weight: 800;
	border-bottom: 1px solid #ccc;
	border-left: 1px solid #ccc;
	}
	.table-summary .wrapp .details{
	padding: 5px;
	border-left: 1px solid #ccc;
	}
	.nopadding{padding: 0;}
</style>

<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="">
			<a href="#"> Call Track</a>
		</li>
		<li class="active">
			<a href="#"> Manage Numbers</a>
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	
	
	<!--fourth table start-->
	<div class="row">
		
		
		
		
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i>  Manage Numbers
					</h3>
				</div>
				<div class="panel-body">
					
					<div class="col-md-12 nopadding" style="" align="right">
						
						<!--<div class="wrapp" >
							<a href="#"><button class="btn btn-default" type="button"  >Generate New Numbers</button></a>
							</div> 
						-->							
						
					</div>
					
					
					<table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false"
					data-show-toggle="true" data-show-columns="true" data-show-export="true"
					data-detail-view="true" data-detail-formatter="detailFormatter"
					data-minimum-count-columns="2" data-show-pagination-switch="true"
					data-pagination="true" data-id-field="id" data-page-list="[10, 20,40,ALL]"
					data-show-footer="false" data-height="503">
						<thead>
                            <tr>
                                <th data-field="Create Time" data-sortable="true">Number</th>
                                <th data-field="Campaign" data-sortable="true">Friendly Name</th>
                                <th data-field="Ad Set Name" data-sortable="true">Clients Names </th>
                                <th data-field="Ad Name" data-sortable="true">Campaign Name</th>
                                <th data-field="Full Name" data-sortable="true">Action</th>
                                <th data-field="Email" data-sortable="true">Reports</th>
							</tr>
						</thead>
						<tbody>
                            <tr>
                                <td>(562) 334-1110</td>
                                <td>Facebook Profile</td>
                                <td>Alhambra Nissan </td>
                                <td>Alhambra Nissan - SUV</td>
                                <td>Release</td>
                                <td>Stats</td>
							</tr>
							<tr>
                                <td>(562) 334-1111</td>
                                <td>English Ad</td>
                                <td>Alhambra Nissan </td>
                                <td>Cheap Compact Cars</td>
                                <td>Release</td>
                                <td>Stats</td>
							</tr>
							<tr>
                                <td>(562) 334-1112</td>
                                <td>Mix Ads</td>
                                <td>Alhambra Nissan </td>
                                <td>All SUV's Must Go!</td>
                                <td>Release</td>
                                <td>Stats</td>
							</tr>
							<tr>
                                <td>(562) 334-1113</td>
                                <td>Store Number </td>
                                <td>Glendale Toyota</td>
                                <td>Summer Event</td>
                                <td>Release</td>
                                <td>Stats</td>
							</tr>
							<tr>
                                <td>(562) 334-1113</td>
                                <td>Sevice # </td>
                                <td>Glendale Toyota </td>
                                <td>Memorial Day Weekend</td>
                                <td>Release</td>
                                <td>Stats</td>
							</tr>
							<tr>
                                <td>(562) 334-1110</td>
                                <td>Facebook Profile</td>
                                <td>Alhambra Nissan </td>
                                <td>Alhambra Nissan - SUV</td>
                                <td>Release</td>
                                <td>Stats</td>
							</tr>
							<tr>
                                <td>(562) 334-1111</td>
                                <td>English Ad</td>
                                <td>Alhambra Nissan </td>
                                <td>Cheap Compact Cars</td>
                                <td>Release</td>
                                <td>Stats</td>
							</tr>
							<tr>
                                <td>(562) 334-1112</td>
                                <td>Mix Ads</td>
                                <td>Alhambra Nissan </td>
                                <td>All SUV's Must Go!</td>
                                <td>Release</td>
                                <td>Stats</td>
							</tr>
							<tr>
                                <td>(562) 334-1113</td>
                                <td>Store Number </td>
                                <td>Glendale Toyota</td>
                                <td>Summer Event</td>
                                <td>Release</td>
                                <td>Stats</td>
							</tr>
							<tr>
                                <td>(562) 334-1113</td>
                                <td>Sevice # </td>
                                <td>Glendale Toyota </td>
                                <td>Memorial Day Weekend</td>
                                <td>Release</td>
                                <td>Stats</td>
							</tr>
							<tr>
                                <td>(562) 334-1113</td>
                                <td>Sevice # </td>
                                <td>Glendale Toyota </td>
                                <td>Memorial Day Weekend</td>
                                <td>Release</td>
                                <td>Stats</td>
							</tr>
							
							
                            
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		
		
		
		
		
		
		
		
		
	</div>
	<!--fourth table end-->
	<!--@include('layouts.right_sidebar')  -->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>

<script>
	
	
	
	
	$( document ).ready(function() {
	    var extra_features = '<a href="#"><button class="btn btn-default" type="button" style="margin: 10px 0;" >Generate New Numbers</button></a>';
		$(".fixed-table-toolbar").append(extra_features);
	});	
	
</script>
<!--
<div class="input-group"><div class="input-group-addon"><i class="fa fa-fw fa-calendar"></i></div><input type="text" class="form-control pull-right" id="date-range0" placeholder="YYYY-MM-DD to YYYY-MM-DD"/></div>-->


<!-- bootstrap time picker -->

<!-- InputMask -->
<script  type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}" ></script>
<!-- bootstrap time picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" ></script>

<script  type="text/javascript" src="{{asset('assets/vendors/jquerydaterangepicker/js/jquery.daterangepicker.min.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/timedropper/js/timedropper.js')}}" ></script>
<!--<script  type="text/javascript" src="{{asset('assets/js/custom_js/datepickers.js')}}" ></script> -->


<script>
	$('#date-range0').dateRangePicker({
		autoClose: true,
		format: 'MM-DD-YYYY',
        getValue: function () {
            var range = $(this).val();
			//alert(range);
		}
	});
	
	
</script>



<!-- end of page level js -->
@stop
