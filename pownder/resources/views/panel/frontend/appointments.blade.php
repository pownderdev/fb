@extends('layouts/default')

{{-- Page title --}}
@section('title')
Appointments
@parent
@stop
{{-- page level styles --}} 
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/fullcalendar_new/css/fullcalendar.css')}}"/>
<link rel="stylesheet" media='print' type="text/css" href="{{asset('assets/vendors/fullcalendar_new/css/fullcalendar.print.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/iCheck/css/all.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/calendar_custom.css')}}"/>
<link href="http://big.pownder.com/assets/date_time/bootstrap-datetimepicker.css" rel="stylesheet">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datedropper/datedropper.css')}}">
<!--<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/timedropper/css/timedropper.css')}}">-->
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap-timepicker.css')}}">
<style>
    .text-black { color:#000 !important;}
	.ui-autocomplete { z-index: 99999; overflow-y: scroll; height:250px }
	.ui-menu-item-wrapper { padding:5px 0px; margin:10px 0px; }
	
	#myModal {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#myModal .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#calendarModal {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#calendarModal .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	#rescheduleModel {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#rescheduleModel .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	.fc-myCustomButton-button {color:#fff; background-color: #ffbe18 !important; font-weight: bold; border: 2px solid #fff; box-shadow: 4px 3px 10px rgba(0, 0, 0, 0.50);font-size: 14px !important;}
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content') 

<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li><a href="{{route('/')}}"> <i class="fa fa-fw fa-home"></i> Dashboard</a></li>
		<li class="active"> Appointments</li>
	</ol>
</section>

<section class="content">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-md-12">
			<div class="row tiles-row">
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
					<div class="canvas-interactive-wrapper1">
						<canvas id="canvas-interactive1" class="grad1"></canvas>
						<a href="#" onClick="AppointmentSearch('')"> 
							<div class="cta-wrapper1">
								<div class="widget">
									<div class="item">
										<div class="widget-icon pull-left icon-color animation-fadeIn">
											<i class="fa fa-fw fa-calendar-o fa-size"></i>
										</div>
									</div>
									<div class="widget-count panel-white">
										<div class="item-label text-center">
											<div id="TotalAppointment" class="count-box">{{ number_format($TotalAppointment,0) }}</div>
											<span class="title">Appointment</span>
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
					<div class="widget">
						<div class="canvas-interactive-wrapper2">
							<canvas id="canvas-interactive2" class="grad1"></canvas>
							<a href="#" onClick="AppointmentSearch('Confirmed')"> 
								<div class="cta-wrapper2">
									<div class="item">
										<div class="widget-icon pull-left icon-color animation-fadeIn">
											<i class="fa fa-fw fa-calendar-check-o fa-size"></i>
										</div>
									</div>
									<div class="widget-count panel-white">
										<div class="item-label text-center">
											<div class="count-box" id="ConfirmedAppointment">
												@if($TotalAppointment == 0)
												0%
												@else
												{{ number_format(($ConfirmedAppointment/$TotalAppointment)*100,2) }}%
												@endif
											</div>
											<span class="title">Confirmed</span>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
					<div class="widget">
						<div class="canvas-interactive-wrapper3">
							<canvas id="canvas-interactive3" class="grad1"></canvas>
							<a href="#" onClick="AppointmentSearch('Shown')"> 
								<div class="cta-wrapper3">
									<div class="item">
										<div class="widget-icon pull-left icon-color animation-fadeIn">
											<i class="fa fa-fw fa-calendar fa-size"></i>
										</div>
									</div>
									<div class="widget-count panel-white">
										<div class="item-label text-center">
											<div class="count-box" id="ShownAppointment">
												@if($TotalAppointment == 0)
												0%
												@else
												{{ number_format(($ShownAppointment/$TotalAppointment)*100,2) }}%
												@endif
											</div>
											<span class="title">Shown</span>
										</div>
									</div>
								</div>
							</a> 
						</div>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
					<div class="widget">
						<div class="canvas-interactive-wrapper4">
							<canvas id="canvas-interactive4" class="grad1"></canvas>
							<a href="#" onClick="AppointmentSearch('Canceled')"> 
								<div class="cta-wrapper4">
									<div class="item">
										<div class="widget-icon pull-left icon-color animation-fadeIn">
											<i class="fa fa-fw fa-calendar-times-o fa-size"></i>
										</div>
									</div>
									<div class="widget-count panel-white">
										<div class="item-label text-center">
											<div class="count-box" id="CanceledAppointment">
												@if($TotalAppointment == 0)
												0%
												@else
												{{ number_format(($CanceledAppointment/$TotalAppointment)*100,2) }}%
												@endif
											</div>
											<span class="title">Canceled</span>
										</div>
									</div>
								</div>
							</a> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-body">
					<input type="hidden" id="appointment_status" name="appointment_status" value="">
					<div id="calendar"></div>
				</div>
			</div>
			<!-- /.box -->
		</div>
		<!-- /.col -->
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<form method="post" id="addEventForm">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="modal-content">
                    <div class="modal-header gred_2">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">
                            <i class="fa fa-plus"></i> Create Appointment
						</h4>
					</div>
					
					<div class="modal-body" style="height: 200px;">
						<div class="form-group">
							<label for="new-event">Appointment With</label>
							<div class="input-group">
								<div class="ui-widget">
									<input type="text" id="new-event" name="event" class="form-control" placeholder="Appointment With">
								</div>
								<input type="hidden" id="event_type" name="event_type" value="#4FC1E9">
								<input type="hidden" id="facebook_ads_lead_user_id" name="facebook_ads_lead_user_id" value="">
								<div class="input-group-btn">
									<button type="button" id="color-chooser-btn" name="event_type" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
										Select
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu pull-right" id="color-chooser">
										<li>
											<a class="palette-primary" href="#">Primary</a>
										</li>
										<li>
											<a class="palette-success" href="#">Success</a>
										</li>
										<li>
											<a class="palette-info" href="#">Info</a>
										</li>
										<li>
											<a class="palette-warning" href="#">warning</a>
										</li>
										<li>
											<a class="palette-danger" href="#">Danger</a>
										</li>
										<li>
											<a class="palette-default" href="#">Default</a>
										</li>
									</ul>
								</div>
								<!-- /btn-group -->
							</div>
							<small class="text-danger animated event fadeInUp add-event"></small>
						</div>
						
						<div class="form-group">
							<div class="col-md-6" style="padding: 0px 10px 0px 0px">
								<label for="create_appointment_date">Appointment Date</label>
								<div class="input-group" style="width: 100%;">
									<div class="input-group-addon">
										<i class="fa fa-fw fa-calendar"></i>
									</div>
									<input type="text" class="form-control" id="create_appointment_date" name="create_appointment_date" placeholder="MM-DD-YYYY" style="background:#FFF" readonly="readonly">
								</div>
								<small class="text-danger animated create_appointment_date fadeInUp"></small><br />
							</div>
							<div class="col-md-6" style="padding: 0px 0px 0px 10px">
								<label for="create_appointment_time">Appointment Time</label>
								<div class="input-group" style="width: 100%;">
									<div class="input-group-addon">
										<i class="fa fa-fw fa-clock-o"></i>
									</div>
									<input type="text" class="form-control bootstrap-timepicker timepicker" id="create_appointment_time" name="create_appointment_time" placeholder="HH:MM" style="background:#FFF" readonly="readonly">
								</div>
								<small class="text-danger animated create_appointment_time fadeInUp"></small><br />
							</div>
						</div>
                        <!-- /input-group -->
					</div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger pull-right" id="close_calendar_event" data-dismiss="modal">
                            Close
                            <i class="fa fa-times"></i>
						</button>
                        <button type="submit" class="btn btn-success pull-left" id="add-new-event">
                            <i class="fa fa-plus"></i> Add
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<!-- /.content -->

<div id="calendarModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gred_2">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
				<h4 class="modal-title">Appointment Details</h4>
			</div>
			<div class="modal-body"> 
				<table class="table table-bordered">
					<tr>
						<td>Name</td>
						<th id="lead_name"></th>
					</tr>
					<tr>
						<td>Client's Name</td>
						<th id="client_name"></th>
					</tr>
					<tr>
						<td>Appointment DateTime</td>
						<th id="appt_datetime"></th>
					</tr>
					<tr>
						<td>Email</td>
						<th id="appt_email"></th>
					</tr>
					<tr>
						<td>Phone #</td>
						<th id="appt_phone"></th>
					</tr>
					<tr>
						<td>City</td>
						<th id="appt_city"></th>
					</tr>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger cancelAppointment" data-facebook="0">Cancel</button>
				<button type="button" class="btn btn-warning deleteAppointment" data-facebook="0">Delete</button>
				<button type="button" class="btn btn-success rescheduleAppointment" data-facebook="0">Reschedule</button>
				<button type="button" class="btn btn-info shownAppointment" data-facebook="0">Shown</button>
				<button type="button" class="btn btn-primary soldAppointment" data-facebook="0">Sold</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div id="rescheduleModel" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header gred_2">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
				<h4 class="modal-title">Appointment Reschedule</h4>
			</div>
			<div class="modal-body" style="height: 100px;"> 
				<div class="form-group">
					<input type="hidden" name="appointment_facebook_ads_lead_user_id" id="appointment_facebook_ads_lead_user_id">
					<div class="col-md-6">
						<label for="appointment_date">Appointment Date</label>
						<div class="input-group" style="width: 100%;">
							<div class="input-group-addon">
								<i class="fa fa-fw fa-calendar"></i>
							</div>
							<input type="text" class="form-control" id="appointment_date" name="appointment_date" placeholder="MM-DD-YYYY" style="background:#FFF" readonly="readonly">
						</div>
						<small class="text-danger animated appointment_date fadeInUp"></small><br />
					</div>
					<div class="col-md-6">
						<label for="appointment_time">Appointment Time</label>
						<div class="input-group" style="width: 100%;">
							<div class="input-group-addon">
								<i class="fa fa-fw fa-clock-o"></i>
							</div>
							<input type="text" class="form-control bootstrap-timepicker timepicker" id="appointment_time" name="appointment_time" placeholder="HH:MM" style="background:#FFF" readonly="readonly">
						</div>
						<small class="text-danger animated appointment_time fadeInUp"></small><br />
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success saveAppointment">Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@stop

{{-- page level scripts --}} 
@section('footer_scripts')
<!-- begining of page level js -->
<script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/vendors/fullcalendar_new/js/fullcalendar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/iCheck/js/icheck.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/date_time/bootstrap-datetimepicker.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}" ></script>
<!--<script  type="text/javascript" src="{{asset('assets/vendors/timedropper/js/timedropper.js')}}" ></script>-->
<script type="text/javascript" src="{{asset('assets/js/bootstrap-timepicker.js')}}"></script>
<script type="text/javascript">
    $(function () {
		/* initialize the external events
		-----------------------------------------------------------------*/
		
		$('#external-events .fc-event').each(function() {
			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});
			
			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});
		});
		
		//Date for the calendar events (dummy data)
		$('#calendar').fullCalendar(
		{
			customButtons: {
				myCustomButton: {
					text: 'Create Appointment',
					click: function() {
						$('#myModal').modal();
					}
				}
			},
			header: {left: 'title', center: 'myCustomButton', right: 'prevYear,year,nextYear  prev,today,next  month,agendaWeek,agendaDay'},
			defaultTimedEventDuration: '01:00:00',
			defaultView: "agendaWeek",
			buttonText: {prev: "Prev", next: "Next", today: 'Today', month: 'Month', week: 'Week', year:'Year', day: 'Day'},
			buttonIcons: {prev: "left-double-arrow", next: "right-double-arrow", prevYear: "left-double-arrow", nextYear: "right-double-arrow"},
			
			//Random events
			@php $i=0; @endphp
			events: [
			@foreach($Appointments as $Appointment)
			@if(count($Appointments) == $i)
			{title: '{{ $Appointment->full_name }}', start: '{{ $Appointment->appointment_date}}T{{ date("H:i:s",strtotime($Appointment->appointment_time))}}', backgroundColor: '{{ $Appointment->backgroundColor }}', borderColor: '{{ $Appointment->borderColor }}', id: '{{ $Appointment->id }}', className: "appointmentHide"}
			@else
			{title: '{{ $Appointment->full_name }}', start: '{{ $Appointment->appointment_date}}T{{ date("H:i:s",strtotime($Appointment->appointment_time))}}', backgroundColor: '{{ $Appointment->backgroundColor }}', borderColor: '{{ $Appointment->borderColor }}', id: '{{ $Appointment->id }}', className: "appointmentHide"},
			@endif
			@php $i++; @endphp
			@endforeach
			],
			
			eventClick: function(event) {
				//alert(event.id);
				if(event.id != '' && event.id != undefined)
				{
					$('#date-loader').show();
					$.get("{{ route('lead-details') }}",{id: event.id},function(data){ 
						$('#lead_name').html(data.full_name);
						$('#client_name').html(data.client_name);
						$('#appt_datetime').html(data.appointment_date+' '+data.appointment_time);
						$('#appt_email').html(data.email);
						$('#appt_phone').html(data.phone_number);
						$('#appt_city').html(data.city);
						$('.cancelAppointment').data('facebook', event.id);
						$('.rescheduleAppointment').data('facebook', event.id);
						$('.shownAppointment').data('facebook', event.id);
						$('.soldAppointment').data('facebook', event.id);
						
						if(data.appointment_cal == 'Yes')
						{
							$('.deleteAppointment').data('facebook', event.id);
							$('.deleteAppointment').show();
						}
						else
						{
							$('.deleteAppointment').data('facebook', '');
							$('.deleteAppointment').hide();
						}
						
						$('#date-loader').hide();
						$('#calendarModal').modal();
					});
				}
			}, 	
			
			editable: false,
			minTime: "09:00:00",
			maxTime: "22:00:00",
			droppable: false,
			//navLinks: true,
			// dragRevertDuration: 0,
			eventDrop : function( event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view ) 
            {
				$('.fc-draggable').hide();
				$('.appointmentHide').show();
				var date = dayDelta+' ';
				var facebook_ads_lead_user_id = event.id;
				if(event.id != '' && event.id != undefined)
				{
					$.get("{{ route('update-appointment') }}",{facebook_ads_lead_user_id: facebook_ads_lead_user_id.toString(), dayDelta:date.toString()},function(data){
						//After Success
					});
				}
			},
			
			// this allows things to be dropped onto the calendar !!!
			drop: function(date, allDay)
			{ 
				// this function is called when something is dropped
				// retrieve the dropped element's stored Event Object
				var originalEventObject = $(this).data('eventObject');
				// we need to copy it, so that multiple events don't have a reference to the same object
				var copiedEventObject = $.extend(
				{
				}, originalEventObject);
				// assign it the date that was reported
				copiedEventObject.start = date;
				copiedEventObject.id = $(this).data("facebook").toString();
				copiedEventObject.title = $(this).html().toString();
				copiedEventObject.allDay = allDay;
				copiedEventObject.backgroundColor = $(this).css("background-color");
				copiedEventObject.borderColor = $(this).css("border-color");
				copiedEventObject.className = "appointmentHide";
				// render the event on the calendar 
				
				// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
				$('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
				$(".badge1").text(parseInt($(".badge1").text()) + 1);  
				
				$('.fc-draggable').hide();
				$('.appointmentHide').css('z-index', '9999');
				$('.appointmentHide').css('margin-right', '0px');
				$('.appointmentHide').css('left', '0%');
				$('.appointmentHide').css('right', '0%');
				$('.appointmentHide').show();
				
				if($(this).data("facebook") > 0){
					$.get("{{ route('event-appointment') }}", {facebook_ads_lead_user_id: $(this).data("facebook").toString(), borderColor: $(this).css("border-color"), backgroundColor: $(this).css("background-color"), datetime: date.toString()}, function (data) {
						//After Success
					});
				}
				
				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked'))
				{
					//alert($(this).data("eventid").toString());
					$.get("{{ route('event-remove') }}", {id: $(this).data("eventid").toString()}, function (data) {
						// if so, remove the element from the "Draggable Events" list
						$(this).remove();
					});
					$(this).remove();
				}
				
				var displayed = $('.fc-draggable').filter(function() {
					var element = $(this);
					$('.appointmentHide').css('margin-right', '0px');
					if(element.css('z-index') == '2') {
						element.remove();
						return false;
					}
					
					return true;
				});
			}
		}); /* ADDING EVENTS */
		
		var currColor = "#4FC1E9"; //default
		//Color chooser button
		var colorChooser = $("#color-chooser-btn");
		
		$("#color-chooser").find('li a').on('click', function(e)
		{
			e.preventDefault();
			//Save color
			currColor = $(this).css("background-color");
			//Add color effect to button
			colorChooser.css({"background-color": currColor, "border-color": currColor}).html($(this).text() + ' <span class="caret"></span>');
			$('#event_type').val(currColor);
		});
		
		$("#addEventForm").on("submit", function( e ) {
			e.preventDefault();
			$('.event, .create_appointment_date, .create_appointment_time').html('');
			
			var facebook_ads_lead_user_id = $("#facebook_ads_lead_user_id").val();
			var appointment_date = $("#create_appointment_date").val();
			var appointment_time = $("#create_appointment_time").val();
			var borderColor = $("#event_type").val();
			var backgroundColor = $("#event_type").val();
			
			var error = 0;
			
			//Get value and make sure it is not null
			var val = $("#new-event").val();
			if(val == '')
			{
				$('.event').html('The event title is required');
				error = 1;
			}
			
			if(appointment_date == '')
			{
				$('.create_appointment_date').html('The event date is required');
				error = 1;
			}
			
			if(appointment_time == '')
			{
				$('.create_appointment_time').html('The event time is required');
				error = 1;
			}
			
			if(error == 1)
			{
				return false;
			}
			else if($("#facebook_ads_lead_user_id").val() != '')
			{
				$('#date-loader').show();
				$.get("{{ route('event-appointment') }}", {facebook_ads_lead_user_id: facebook_ads_lead_user_id.toString(), borderColor: borderColor, backgroundColor: backgroundColor, appointment_date: appointment_date, appointment_time: appointment_time}, function (data) {
					//After Success
					$('#calendar').fullCalendar('removeEvents', facebook_ads_lead_user_id.toString());
					
					var myEvent = {
						title: data.title, 
						start: data.start, 
						backgroundColor: data.backgroundColor, 
						borderColor: data.borderColor, 
						id: facebook_ads_lead_user_id.toString(), 
						className: data.className
					};
					
					$('#calendar').fullCalendar('renderEvent', myEvent, true);
					
					$("#new-event, #facebook_ads_lead_user_id, #create_appointment_date, #create_appointment_time").val("");
					$('#myModal').modal('hide');
					$('#date-loader').hide();
					
					$.get("{{ route('appointment-summary') }}",{client_id: $(".client-list ul li.active").attr("user_client_id"), dateRange: $('#date-range-header').val(), appointment_status: $('#appointment_status').val()},function(data){ 
						$('#TotalAppointment').html(data.TotalAppointment);
						$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
						$('#ShownAppointment').html(data.ShownAppointment);
						$('#CanceledAppointment').html(data.CanceledAppointment);
					});
				});
			}
			else
			{
				swal ( "Oops" ,  "Lead not exists!" ,  "error" );
			}
		}); 
		
		$('#myModal').on('hidden.bs.modal', function (e) {
			$('.event, .create_appointment_date, .create_appointment_time').html('');
			$("#new-event, #facebook_ads_lead_user_id, #create_appointment_date, #create_appointment_time").val("");
		})
		
		$('#new-event').autocomplete({
			source:"{{ route('event-lead-search') }}",
			minlength:1,
			autoFocus:true,
			select: function (event, ui) {
				// Prevent value from being put in the input:
				this.value = ui.item.label;
				
				// Set the next input's value to the "value" of the item.
				$(this).next("input").val(ui.item.value);
				var id = ui.item.id;
				id = id.replace("'", "");
				id = id.replace("'", "");
				$("#facebook_ads_lead_user_id").val(id); // display the selected text
				
				event.preventDefault();
			}
		});
		
		$(".deleteAppointment").click(function(){
			var facebook_ads_lead_user_id = $('.deleteAppointment').data("facebook");
			//alert(facebook_ads_lead_user_id);
			$('#date-loader').show();
			$.get("{{ route('appointment-delete') }}", {facebook_ads_lead_user_id: $('.deleteAppointment').data("facebook").toString()}, function (data) {
				//After Success
				$('#calendar').fullCalendar('removeEvents', $('.deleteAppointment').data("facebook").toString());
				$('.fc-draggable').hide();
				$('.appointmentHide').show();
				$('#date-loader').hide();
				$('#calendarModal').modal('hide');
				
				$.get("{{ route('appointment-summary') }}",{client_id: $(".client-list ul li.active").attr("user_client_id"), dateRange: $('#date-range-header').val(), appointment_status: $('#appointment_status').val()},function(data){ 
					$('#TotalAppointment').html(data.TotalAppointment);
					$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
					$('#ShownAppointment').html(data.ShownAppointment);
					$('#CanceledAppointment').html(data.CanceledAppointment);
				});
			});
		});
		
		$(".cancelAppointment").click(function(){
			var facebook_ads_lead_user_id = $('.cancelAppointment').data("facebook");
			//alert(facebook_ads_lead_user_id);
			$('#date-loader').show();
			$.get("{{ route('appointment-cancel') }}", {facebook_ads_lead_user_id: $('.cancelAppointment').data("facebook").toString()}, function (data) {
				//After Success
				if(data.id != '')
				{
					$('#calendar').fullCalendar('removeEvents', facebook_ads_lead_user_id.toString());
					var myEvent = {
						title: data.title, 
						start: data.start, 
						backgroundColor: data.backgroundColor, 
						borderColor: data.borderColor, 
						id: facebook_ads_lead_user_id.toString(), 
						className: data.className
					};
					
					$('#calendar').fullCalendar('renderEvent', myEvent, true);
					$('#date-loader').hide();
					$('#calendarModal').modal('hide');
				}
				
				$.get("{{ route('appointment-summary') }}",{client_id: $(".client-list ul li.active").attr("user_client_id"), dateRange: $('#date-range-header').val(), appointment_status: $('#appointment_status').val()},function(data){ 
					$('#TotalAppointment').html(data.TotalAppointment);
					$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
					$('#ShownAppointment').html(data.ShownAppointment);
					$('#CanceledAppointment').html(data.CanceledAppointment);
				});
			});
		});
		
		$(".soldAppointment").click(function(){
			var facebook_ads_lead_user_id = $('.soldAppointment').data("facebook");
			//alert(facebook_ads_lead_user_id);
			$('#date-loader').show();
			$.get("{{ route('appointment-sold') }}", {facebook_ads_lead_user_id: $('.soldAppointment').data("facebook").toString()}, function (data) {
				//After Success
				$('#calendar').fullCalendar('removeEvents', $('.soldAppointment').data("facebook").toString());
				//$('.fc-draggable').hide();
				//$('.appointmentHide').show();
				$('#date-loader').hide();
				$('#calendarModal').modal('hide');
				$.get("{{ route('appointment-summary') }}",{client_id: $(".client-list ul li.active").attr("user_client_id"), dateRange: $('#date-range-header').val(), appointment_status: $('#appointment_status').val()},function(data){ 
					$('#TotalAppointment').html(data.TotalAppointment);
					$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
					$('#ShownAppointment').html(data.ShownAppointment);
					$('#CanceledAppointment').html(data.CanceledAppointment);
				});
			});
		});
		
		$(".shownAppointment").click(function(){
			var facebook_ads_lead_user_id = $('.shownAppointment').data("facebook");
			//alert(facebook_ads_lead_user_id);
			$('#date-loader').show();
			$.get("{{ route('appointment-shown') }}", {facebook_ads_lead_user_id: $('.shownAppointment').data("facebook").toString()}, function (data) {
				//After Success
				if(data.id != '')
				{
					$('#calendar').fullCalendar('removeEvents', facebook_ads_lead_user_id.toString());
					var myEvent = {
						title: data.title, 
						start: data.start, 
						backgroundColor: data.backgroundColor, 
						borderColor: data.borderColor, 
						id: facebook_ads_lead_user_id.toString(), 
						className: data.className
					};
					
					$('#calendar').fullCalendar('renderEvent', myEvent, true);
					$('#date-loader').hide();
					$('#calendarModal').modal('hide');
				}
				$.get("{{ route('appointment-summary') }}",{client_id: $(".client-list ul li.active").attr("user_client_id"), dateRange: $('#date-range-header').val(), appointment_status: $('#appointment_status').val()},function(data){ 
					$('#TotalAppointment').html(data.TotalAppointment);
					$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
					$('#ShownAppointment').html(data.ShownAppointment);
					$('#CanceledAppointment').html(data.CanceledAppointment);
				});
			});
		});
		
		$(".rescheduleAppointment").click(function(){
			var facebook_ads_lead_user_id = $('.rescheduleAppointment').data("facebook");
			//alert(facebook_ads_lead_user_id);
			$('#date-loader').show();
			$.get("{{ route('appointment-reschedule') }}", {facebook_ads_lead_user_id: $('.rescheduleAppointment').data("facebook").toString()}, function (data) {
				//After Success
				$('.appointment_date').html('');
				$('.appointment_time').html('');
				$('#appointment_date').val(data.appointment_date);
				$('#appointment_time').val(data.appointment_time);
				$('#appointment_facebook_ads_lead_user_id').val($('.rescheduleAppointment').data("facebook").toString());
				$('#date-loader').hide();
				$('#calendarModal').modal('hide');
				$('#rescheduleModel').modal();
				$.get("{{ route('appointment-summary') }}",{client_id: $(".client-list ul li.active").attr("user_client_id"), dateRange: $('#date-range-header').val(), appointment_status: $('#appointment_status').val()},function(data){ 
					$('#TotalAppointment').html(data.TotalAppointment);
					$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
					$('#ShownAppointment').html(data.ShownAppointment);
					$('#CanceledAppointment').html(data.CanceledAppointment);
				});
			});
		});
		
		$("#close_calendar_event").on('click', function(e){
			$('.event, .create_appointment_date, .create_appointment_time').html('');
			$("#new-event, #facebook_ads_lead_user_id, #create_appointment_date, #create_appointment_time").val("");
		});
		
		$('input[type="checkbox"].custom_icheck, input[type="radio"].custom_radio').iCheck(
		{
			checkboxClass: 'icheckbox_minimal-blue',
			radioClass: 'iradio_minimal-blue',
			increaseArea: '20%'
		});
		
		var options1 = {format: "m-d-Y", dropPrimaryColor: "#428bca"};
		$('#appointment_date').dateDropper($.extend({}, options1));
		$('#create_appointment_date').dateDropper($.extend({}, options1));
		//$("#appointment_time").timeDropper({primaryColor: "#428bca", format: "hh:mm A"});
		//$("#create_appointment_time").timeDropper({primaryColor: "#428bca", format: "hh:mm A"});
		
		//$('#appointment_time').timepicker({minuteStep: 15, showInputs: false});
		//$('#create_appointment_time').timepicker({minuteStep: 15, showInputs: false});
		
		$(document).on("click",".timepicker",function(){
			var d = new Date();
			var current_time = moment(d).format('hh:mm A'); // time format with moment.js
			$(this).timepicker('setTime', current_time); // setting timepicker to current time
			$(this).val(current_time); // setting input field to current time
		});
		
		$('#appointment_time').timepicker().on('hide.timepicker', function(e) {
			var start_time = '09:00 AM'; //start time
			var input_value = e.time.value; //appointment time
			var end_time = '09:00 PM'; //end time
			
			//convert time into timestamp
			var input = new Date("May 26, 1991 " + input_value);
			input = input.getTime();
			
			var stt = new Date("May 26, 1991 " + start_time);
			stt = stt.getTime();
			
			var endt = new Date("May 26, 1991 " + end_time);
			endt = endt.getTime();
			
			if(stt <= input && input <= endt) {
				$('.appointment_time').html('');
			}
			else
			{
				$('#appointment_time').val('');
				$('.appointment_time').html('Time between 9:00 AM to 9:00 PM');
			}
		});
		
		$('#create_appointment_time').timepicker().on('hide.timepicker', function(e) {
			var start_time = '09:00 AM'; //start time
			var input_value = e.time.value; //appointment time
			var end_time = '09:00 PM'; //end time
			
			//convert time into timestamp
			var input = new Date("May 26, 1991 " + input_value);
			input = input.getTime();
			
			var stt = new Date("May 26, 1991 " + start_time);
			stt = stt.getTime();
			
			var endt = new Date("May 26, 1991 " + end_time);
			endt = endt.getTime();
			
			if(stt <= input && input <= endt) {
				$('.create_appointment_time').html('');
			}
			else
			{
				$('#create_appointment_time').val('');
				$('.create_appointment_time').html('Time between 9:00 AM to 9:00 PM');
			}
		});
		
		$(".saveAppointment").on("click", function( event ) {
			event.preventDefault();
			$(".saveAppointment").prop('disabled', true);
			$('.appointment_date').html('');
			$('.appointment_time').html('');
			var facebook_ads_lead_user_id=$('#appointment_facebook_ads_lead_user_id').val();
			var appointment_date=$('#appointment_date').val();
			var appointment_time=$('#appointment_time').val();
			
			if(appointment_date=='' || appointment_time=='')
			{
				if(appointment_date=='')
				{
					$('.appointment_date').html('The appointment date field is required.');
				}
				
				if(appointment_time=='')
				{
					$('.appointment_time').html('The appointment time field is required.');
				}
				
				$(".saveAppointment").prop('disabled', false);
				return false;
			}
			$('#date-loader').show();
			$.get("{{ route('reschedule-appointment') }}", {facebook_ads_lead_user_id: facebook_ads_lead_user_id.toString(), appointment_time:appointment_time, appointment_date:appointment_date}, function (data) {
				//After Success
				$('#appointment_date').val('');
				$('#appointment_time').val('');
				$('#appointment_facebook_ads_lead_user_id').val('')
				$(".saveAppointment").prop('disabled', false);
				if(data.id != '')
				{
					$('#calendar').fullCalendar('removeEvents', facebook_ads_lead_user_id.toString());
					var myEvent = {
						title: data.title, 
						start: data.start, 
						backgroundColor: data.backgroundColor, 
						borderColor: data.borderColor, 
						id: facebook_ads_lead_user_id.toString(), 
						className: data.className
					};
					
					$('#calendar').fullCalendar('renderEvent', myEvent, true);
					$('#rescheduleModel').modal("hide");
					$('#date-loader').hide();
				}
				
				$.get("{{ route('appointment-summary') }}",{client_id: $(".client-list ul li.active").attr("user_client_id"), dateRange: $('#date-range-header').val(), appointment_status: $('#appointment_status').val()},function(data){ 
					$('#TotalAppointment').html(data.TotalAppointment);
					$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
					$('#ShownAppointment').html(data.ShownAppointment);
					$('#CanceledAppointment').html(data.CanceledAppointment);
				});
			});
		});
		
		$("#create_appointment_date, #create_appointment_time").val("");
		
		//-----------------------------------------------------------------------------------------------------------------
		
		$(".reset_lead").click(function () {
			$('.reset_lead').addClass('btn-primary');
			$('.reset_lead').removeClass('btn-warning');
			$(".client-list ul li").removeClass("active");
			
			$('#date-loader').show();
			$.get("{{ route('appointment-client') }}",{client_id: '', dateRange: $('#date-range-header').val(), appointment_status: $('#appointment_status').val()},function(data){ 
				$('#calendar').fullCalendar('removeEvents');
				$('#TotalAppointment').html(data.TotalAppointment);
				$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
				$('#ShownAppointment').html(data.ShownAppointment);
				$('#CanceledAppointment').html(data.CanceledAppointment);
				var data = data.data;
				var eventArray = [];
				for(var ij = 0; ij < data.length; ij++)
				{
					var str = data[ij].id;
					var facebook_ads_lead_user_id = str.slice(1, -1);
					
					var myEvent = {
						title: data[ij].title, 
						start: data[ij].start, 
						backgroundColor: data[ij].backgroundColor, 
						borderColor: data[ij].borderColor, 
						id: facebook_ads_lead_user_id, 
						className: 'appointmentHide'
					};
					
					eventArray.push(myEvent);
				}
				$('#calendar').fullCalendar('renderEvents', eventArray, true);
				$('#date-loader').hide();
			});
		});
		
		$(".client-list ul li").click(function() {
			var client_id = $(this).attr('user_client_id');
			if(client_id==undefined)
			{
				$('.reset_lead').addClass('btn-primary');
				$('.reset_lead').removeClass('btn-warning');
			}
			else
			{
				$('.reset_lead').addClass('btn-warning');
				$('.reset_lead').removeClass('btn-primary');
			}
			$('#date-loader').show();
			$.get("{{ route('appointment-client') }}",{client_id: client_id, dateRange: $('#date-range-header').val(), appointment_status: $('#appointment_status').val()},function(data){ 
				$('#calendar').fullCalendar('removeEvents');
				$('#TotalAppointment').html(data.TotalAppointment);
				$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
				$('#ShownAppointment').html(data.ShownAppointment);
				$('#CanceledAppointment').html(data.CanceledAppointment);
				var data = data.data;
				var eventArray = [];
				for(var ij = 0; ij < data.length; ij++)
				{
					var str = data[ij].id;
					var facebook_ads_lead_user_id = str.slice(1, -1);
					
					var myEvent = {
						title: data[ij].title, 
						start: data[ij].start, 
						backgroundColor: data[ij].backgroundColor, 
						borderColor: data[ij].borderColor, 
						id: facebook_ads_lead_user_id, 
						className: 'appointmentHide'
					};
					eventArray.push(myEvent);
				}
				//console.log(eventArray);
				$('#calendar').fullCalendar('renderEvents', eventArray, true);
				$('#date-loader').hide();
			});
		});
		
		$('.button-select').click(function () {
			$('#date-loader').show();
			$.get("{{ route('appointment-client') }}",{client_id: $(".client-list ul li.active").attr("user_client_id"), dateRange: $('#date-range-header').val(), appointment_status: $('#appointment_status').val()},function(data){ 
				$('#calendar').fullCalendar('removeEvents');
				$('#TotalAppointment').html(data.TotalAppointment);
				$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
				$('#ShownAppointment').html(data.ShownAppointment);
				$('#CanceledAppointment').html(data.CanceledAppointment);
				var data = data.data;
				var eventArray = [];
				for(var ij = 0; ij < data.length; ij++)
				{
					var str = data[ij].id;
					var facebook_ads_lead_user_id = str.slice(1, -1);
					
					var myEvent = {
						title: data[ij].title, 
						start: data[ij].start, 
						backgroundColor: data[ij].backgroundColor, 
						borderColor: data[ij].borderColor, 
						id: facebook_ads_lead_user_id, 
						className: 'appointmentHide'
					};
					eventArray.push(myEvent);
				}
				$('#calendar').fullCalendar('renderEvents', eventArray, true);
				$('#date-loader').hide();
			});
		});
		
		$('.button-clear').click(function () {
			$('#date-loader').show();
			$.get("{{ route('appointment-client') }}",{client_id: $(".client-list ul li.active").attr("user_client_id"), dateRange: '', appointment_status: $('#appointment_status').val()},function(data){ 
				$('#calendar').fullCalendar('removeEvents');
				$('#TotalAppointment').html(data.TotalAppointment);
				$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
				$('#ShownAppointment').html(data.ShownAppointment);
				$('#CanceledAppointment').html(data.CanceledAppointment);
				var data = data.data;
				var eventArray = [];
				for(var ij = 0; ij < data.length; ij++)
				{
					var str = data[ij].id;
					var facebook_ads_lead_user_id = str.slice(1, -1);
					
					var myEvent = {
						title: data[ij].title, 
						start: data[ij].start, 
						backgroundColor: data[ij].backgroundColor, 
						borderColor: data[ij].borderColor, 
						id: facebook_ads_lead_user_id, 
						className: 'appointmentHide'
					};
					eventArray.push(myEvent);
				}
				$('#calendar').fullCalendar('renderEvents', eventArray, true);
				$('#date-loader').hide();
			});
		});
		
		$(".fc-myCustomButton-button").removeClass("fc-button fc-state-default");
		//$(".fc-myCustomButton-button").addClass("btn-warning");
	});
	
	function AppointmentSearch(status)
	{
		$('#appointment_status').val(status);
		$('#date-loader').show();
		$.get("{{ route('appointment-client') }}",{client_id: $(".client-list ul li.active").attr("user_client_id"), dateRange: $('#date-range-header').val(), appointment_status: status},function(data){ 
			$('#calendar').fullCalendar('removeEvents');
			$('#TotalAppointment').html(data.TotalAppointment);
			$('#ConfirmedAppointment').html(data.ConfirmedAppointment);
			$('#ShownAppointment').html(data.ShownAppointment);
			$('#CanceledAppointment').html(data.CanceledAppointment);
			var data = data.data;
			var eventArray = [];
			for(var ij = 0; ij < data.length; ij++)
			{
				var str = data[ij].id;
				var facebook_ads_lead_user_id = str.slice(1, -1);
				
				var myEvent = {
					title: data[ij].title, 
					start: data[ij].start, 
					backgroundColor: data[ij].backgroundColor, 
					borderColor: data[ij].borderColor, 
					id: facebook_ads_lead_user_id, 
					className: 'appointmentHide'
				};
				eventArray.push(myEvent);
			}
			$('#calendar').fullCalendar('renderEvents', eventArray, true);
			$('#date-loader').hide();
		});
	}
</script>
<!-- end of page level js -->
@stop