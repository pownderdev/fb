@extends('layouts/default')

{{-- Page title --}}
@section('title')
Edit Profile
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/select2/css/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/select2/css/select2-bootstrap.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css')}}" >
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/iCheck/css/all.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_css/wizard.css')}}" >
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li>
			<a href="#"> Profile</a>
		</li>
		<li class="active">
			Edit Profile
		</li>
	</ol>
</section>

<?php
	$errorarr=$errors;   
	function showerrormsg($key,$errors)
	{    
		$msg='';
		if(count($errors->get($key))>0)
		{ 
			$msg.='';
			foreach($errors->get($key) as $error)
			{
				if($key=="alert_success")
				{
					$msg.=$error;
				}
				else
				{
					$msg.='<small class="text-danger animated fadeInUp"  >'.$error.'</small>' ; 
				}
			}
		} 
		
		return $msg;     
	}
?>

<!-- Main content -->
<section class="content">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff"
						data-loop="true"></i> Edit Profile
					</h3>
				</div>
				<div class="panel-body">
					<div class="bs-example" style="margin-top: 20px;">
						<ul class="nav nav-tabs" style="margin-bottom: 15px;">
							<li class="active">
								<a href="#home" data-toggle="tab" aria-expanded="true">Edit Profile</a>
							</li>
							<li class="">
								<a href="#profile" data-toggle="tab" aria-expanded="false">Change Password</a>
							</li>
						</ul>
						<div id="myTabContent" class="tab-content">
							<div class="tab-pane fade active in" id="home">
								<form action="{{ route('client-edit-profile') }}" method="post" id="client-edit-profile" enctype="multipart/form-data">
									{{ csrf_field() }}
									<div class="form-group" style="height:1px">
										<label for="first_name" class="col-sm-3 control-label">Name *</label>
										<div class="col-sm-9">
											<input id="name" name="name" type="text" placeholder="Name" class="form-control" value="{{ $client->name }}"/>
											<?php echo showerrormsg('name',$errorarr); ?>
										</div>
									</div>
									<div class="form-group" style="padding-top: 30px;">
										<label for="email" class="col-sm-3 control-label">Email *</label>
										<div class="col-sm-9">
											<input type="email" value="{{ $client->email }}" id="email" name="email" placeholder="Email"  class="form-control"/>
											<?php echo showerrormsg('email',$errorarr); ?>
										</div>
									</div>
									<div class="form-group" style="padding-top: 30px;">
										<label for="address" class="col-sm-3 control-label">Address *</label>
										<div class="col-sm-9">
											<input id="address" name="address" type="text" placeholder="Address" class="form-control" 	value="{{ $client->address }}"/>
											<?php echo showerrormsg('address',$errorarr); ?>
										</div>
									</div>
									<div class="form-group" style="padding-top: 30px;">
										<label for="zip" class="col-sm-3 control-label">Zip *</label>
										<div class="col-sm-9">
											<input id="zip" name="zip" placeholder="Zip" type="text" class="form-control" value="{{ $client->zip }}" />
                                            <!--onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57"-->
											<?php echo showerrormsg('zip',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="mobile" class="col-sm-3 control-label">Phone *</label>
										<div class="col-sm-9">
											<input id="mobile" name="mobile" placeholder="Phone" type="text" class="form-control" value="{{ $client->mobile }}"/>
											<?php echo showerrormsg('mobile',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="url" class="col-sm-3 control-label">URL *</label>
										<div class="col-sm-9">
											<input id="url" name="url" placeholder="URL" type="text" class="form-control" value="{{ $client->url }}"/>
											<?php echo showerrormsg('url',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="contact_name" class="col-sm-3 control-label">Contact Name *</label>
										<div class="col-sm-9">
											<input id="contact_name" name="contact_name" placeholder="Contact Name" type="text" class="form-control" value="{{ $client->contact_name }}"/>
											<?php echo showerrormsg('contact_name',$errorarr); ?>
										</div>
									</div>
                                    
                                    <div class="form-group" style="padding-top: 30px;">
										<label for="invoice_email" class="col-sm-3 control-label">Account Payable Email</label>
										<div class="col-sm-9">
											<input id="invoice_email" name="invoice_email" placeholder="Account Payable Email" type="email" class="form-control" value="{{ $client->invoice_email }}"/>
											<?php echo showerrormsg('invoice_email',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="crm_email" class="col-sm-3 control-label">CRM Email </label>
										<div class="col-sm-9">
											<input id="crm_email" name="crm_email" placeholder="CRM Email" type="email" class="form-control" value="{{ $client->crm_email }}"/>
											<?php echo showerrormsg('crm_email',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<label for="photo" class="col-sm-3 control-label">Upload Avatar ( 180x180 )</label>
										<div class="col-sm-9">
											<input id="photo" name="photo" type="file" class="avatar" accept="image/*">
											<?php echo showerrormsg('photo',$errorarr); ?>
										</div>
									</div>
									
									<div class="form-group" style="padding-top: 30px;">
										<center><button type="submit" name="Save" id="Save" class="btn btn-success">Save</button></center>
									</div>
								</form>
							</div>
							
							<div class="tab-pane fade" id="profile">
								<form action="{{ route('client-change-password') }}" method="post" id="client-change-password" >
									{{ csrf_field() }}
									<div class="form-group">
										<label for="password" class="col-sm-3 control-label">Password *</label>
										<div class="col-sm-9">
											<input id="password" name="password" type="password" value="{{ old('password') }}" placeholder="Password" class="form-control required"/>
											<?php echo showerrormsg('password',$errorarr); ?>
										</div>
									</div>
									<div class="form-group" style="padding-top:60px">
										<label for="password_confirmation" class="col-sm-3 control-label">Confirm Password*</label>
										<div class="col-sm-9">
											<input id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}" type="password" placeholder="Confirm Password " class="form-control required"/>
											<?php echo showerrormsg('password_confirmation',$errorarr); ?>
										</div>
									</div>
									<div class="form-group" style="padding-top:40px">
										<button type="submit" style="margin-left:100px" name="change_password" id="change_password" class="btn btn-success">Change Password</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- row -->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/select2/js/select2.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrapwizard/js/jquery.bootstrap.wizard.js')}}" ></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" ></script>
<script type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/iCheck/js/icheck.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/clientEdit.js')}}"></script>

<!-- InputMask -->
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>
<!-- end of page level js -->

<script>
	$(document).ready(function(){
		//$("[data-mask]").inputmask();
		$("#mobile").inputmask('(999) 999-9999');
		
		@if(Session::get('EmailChange')=='Yes')
		swal('Password will get reset now and will send to their new email');
		@php
		Session::put('EmailChange','No');
		@endphp
		@endif
        
        $("#zip").inputmask('99999');
	});
</script>

@stop
