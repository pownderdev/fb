@extends('layouts/default')

{{-- Page title --}}
@section('title')
Campaign CRM Lead
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet" type="text/css"/>
<!--end of page level css-->
<style>
	#SyncTest {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#SyncTest .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
</style>

@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">
			Campaign CRM Lead
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content p-l-r-15" id="first-step">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Lead Details
					</h3>
				</div>
				<div class="panel-body">
					<table class="table table-striped table-borderd">
						<thead>
							<tr style="background-color: #f9f9f9;">
								<th colspan="2" style="text-align:center"><h4 style="font-weight:600">Lead Ads Form Fields Automatically Matched</h4></th>
							</tr>
							<tr>
								<th style="width:50%">Name</th>
								<th style="width:50%">Match</th>
							</tr>
						</thead>
						<tbody>
							<tr class="po_email">
								<td>Email</td>
								<td>
									<label>
										Automatically recognized &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										@if(is_null($AutometiveCampaign))
										<input type="checkbox" class="automatically_recognized" name="auto_email" id="auto_email" value="Yes" checked> 
										@else
										<input type="checkbox" class="automatically_recognized" name="auto_email" id="auto_email" value="Yes" @if($AutometiveCampaign->auto_email == 'Yes') {{ 'checked'}} @endif>
										@endif
									</label>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<div class="po-markup" style="display: inline;">
										<i class="fa fa-lg fa-question-circle po-link" aria-hidden="true"></i>
										<div class="po-content hidden">
											<div class="po-body">
												<p id="po_email"></p>
											</div>
											<!-- ./po-body -->
										</div>
										<!-- ./po-content -->
									</div>
									<!-- ./po-markup-->
								</td>
							</tr>
							<tr class="po_first_name">
								<td>First Name</td>
								<td>
									<label>
										Automatically recognized &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										@if(is_null($AutometiveCampaign))
										<input type="checkbox" class="automatically_recognized" name="auto_first_name" id="auto_first_name" value="Yes" checked> 
										@else
										<input type="checkbox" class="automatically_recognized" name="auto_first_name" id="auto_first_name" value="Yes" @if($AutometiveCampaign->auto_first_name == 'Yes') {{ 'checked'}} @endif>
										@endif
									</label>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<div class="po-markup" style="display: inline;">
										<i class="fa fa-lg fa-question-circle po-link" aria-hidden="true"></i>
										<div class="po-content hidden">
											<div class="po-body">
												<p id="po_first_name"></p>
											</div>
											<!-- ./po-body -->
										</div>
										<!-- ./po-content -->
									</div>
									<!-- ./po-markup-->
								</td>
							</tr>
							<tr class="po_last_name">
								<td>Last Name</td>
								<td>
									<label>
										Automatically recognized &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										@if(is_null($AutometiveCampaign))
										<input type="checkbox" class="automatically_recognized" name="auto_last_name" id="auto_last_name" value="Yes" checked> 
										@else
										<input type="checkbox" class="automatically_recognized" name="auto_last_name" id="auto_last_name" value="Yes" @if($AutometiveCampaign->auto_last_name == 'Yes') {{ 'checked'}} @endif>
										@endif
									</label>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<div class="po-markup" style="display: inline;">
										<i class="fa fa-lg fa-question-circle po-link" aria-hidden="true"></i>
										<div class="po-content hidden">
											<div class="po-body">
												<p id="po_last_name"></p>
											</div>
											<!-- ./po-body -->
										</div>
										<!-- ./po-content -->
									</div>
									<!-- ./po-markup-->
								</td>
							</tr>
							<tr class="po_phone_number">
								<td>Phone Number</td>
								<td>
									<label>
										Automatically recognized &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										@if(is_null($AutometiveCampaign))
										<input type="checkbox" class="automatically_recognized" name="auto_phone_number" id="auto_phone_number" value="Yes" checked> 
										@else
										<input type="checkbox" class="automatically_recognized" name="auto_phone_number" id="auto_phone_number" value="Yes" @if($AutometiveCampaign->auto_phone_number == 'Yes') {{ 'checked'}} @endif>
										@endif
									</label>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<div class="po-markup" style="display: inline;">
										<i class="fa fa-lg fa-question-circle po-link" aria-hidden="true"></i>
										<div class="po-content hidden">
											<div class="po-body">
												<p id="po_phone_number"></p>
											</div>
											<!-- ./po-body -->
										</div>
										<!-- ./po-content -->
									</div>
									<!-- ./po-markup-->
								</td>
							</tr>
							<tr class="po_city">
								<td>City</td>
								<td>
									<label>
										Automatically recognized &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										@if(is_null($AutometiveCampaign))
										<input type="checkbox" class="automatically_recognized" name="auto_city" id="auto_city" value="Yes" checked> 
										@else
										<input type="checkbox" class="automatically_recognized" name="auto_city" id="auto_city" value="Yes" @if($AutometiveCampaign->auto_city == 'Yes') {{ 'checked'}} @endif>
										@endif
									</label>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<div class="po-markup" style="display: inline;">
										<i class="fa fa-lg fa-question-circle po-link" aria-hidden="true"></i>
										<div class="po-content hidden">
											<div class="po-body">
												<p id="po_city"></p>
											</div>
											<!-- ./po-body -->
										</div>
										<!-- ./po-content -->
									</div>
									<!-- ./po-markup-->
								</td>
							</tr>
							<tr class="po_zip_code">
								<td>Zip Code</td>
								<td>
									<label>
										Automatically recognized &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										@if(is_null($AutometiveCampaign))
										<input type="checkbox" class="automatically_recognized" name="auto_zip_code" id="auto_zip_code" value="Yes" checked> 
										@else
										<input type="checkbox" class="automatically_recognized" name="auto_zip_code" id="auto_zip_code" value="Yes" @if($AutometiveCampaign->auto_zip_code == 'Yes') {{ 'checked'}} @endif>
										@endif
									</label>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<div class="po-markup" style="display: inline;">
										<i class="fa fa-lg fa-question-circle po-link" aria-hidden="true"></i>
										<div class="po-content hidden">
											<div class="po-body">
												<p id="po_zip_code"></p>
											</div>
											<!-- ./po-body -->
										</div>
										<!-- ./po-content -->
									</div>
									<!-- ./po-markup-->
								</td>
							</tr>
							<tr>
								<td>Prospect</td>
								<td>
									@if(is_null($AutometiveCampaign))
									<select class="form-control" name="prospect" id="prospect" style="width:200px">
										<option value="New" selected>New</option>
										<option value="Used">Used</option>									
									</select>
									@else
									<select class="form-control" name="prospect" id="prospect" style="width:200px">
										<option value="New" @if($AutometiveCampaign->prospect == 'New') {{ 'selected' }} @endif>New</option>
										<option value="Used" @if($AutometiveCampaign->prospect == 'Used') {{ 'selected' }} @endif>Used</option>									
									</select>
									@endif
								</td>
							</tr>
							<tr>
								<td>Phone Time</td>
								<td>
									@if(is_null($AutometiveCampaign))
									<select class="form-control" name="phone_time" id="phone_time" style="width:200px">
										<option value="morning">Morning</option>
										<option value="afternoon">Afternoon</option>
										<option value="evening">Evening</option>
										<option value="nopreference" selected>No Preference</option>
										<option value="day">Day</option>
									</select>
									@else
									<select class="form-control" name="phone_time" id="phone_time" style="width:200px">
										<option value="morning" @if($AutometiveCampaign->phone_time == 'morning') {{ 'selected' }} @endif>Morning</option>
										<option value="afternoon" @if($AutometiveCampaign->phone_time == 'afternoon') {{ 'selected' }} @endif>Afternoon</option>
										<option value="evening" @if($AutometiveCampaign->phone_time == 'evening') {{ 'selected' }} @endif>Evening</option>
										<option value="nopreference" @if($AutometiveCampaign->phone_time == 'nopreference') {{ 'selected' }} @endif>No Preference</option>
										<option value="day" @if($AutometiveCampaign->phone_time == 'day') {{ 'selected' }} @endif>Day</option>
									</select>
									@endif
								</td> 
							</tr>
							<tr>
								<td>Phone Type</td>
								<td>
									@if(is_null($AutometiveCampaign))
									<select class="form-control" name="phone_type" id="phone_type" style="width:200px">
										<option value="cellphone">Cell Phone</option>
										<option value="fax">Fax</option>
										<option value="home">Home</option>
										<option value="mobile" selected>Mobile</option>
										<option value="voice">Voice</option>
										<option value="work">Work</option>
									</select>
									@else
									<select class="form-control" name="phone_type" id="phone_type" style="width:200px">
										<option value="cellphone" @if($AutometiveCampaign->phone_type == 'cellphone') {{ 'selected' }} @endif>Cell Phone</option>
										<option value="fax" @if($AutometiveCampaign->phone_type == 'fax') {{ 'selected' }} @endif>Fax</option>
										<option value="home" @if($AutometiveCampaign->phone_type == 'home') {{ 'selected' }} @endif>Home</option>
										<option value="mobile" @if($AutometiveCampaign->phone_type == 'mobile') {{ 'selected' }} @endif>Mobile</option>
										<option value="voice" @if($AutometiveCampaign->phone_type == 'voice') {{ 'selected' }} @endif>Voice</option>
										<option value="work" @if($AutometiveCampaign->phone_type == 'work') {{ 'selected' }} @endif>Work</option>
									</select>
									@endif
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Provider Details
					</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="provider_name">Provider Name</label>
								@if(is_null($AutometiveCampaign))
								<input type="text" name="provider_name" id="provider_name" class="form-control" value="{{ $Campaign->service_provider_name }}" placeholder="Provider Name" />
								@else
								<input type="text" name="provider_name" id="provider_name" class="form-control" value="{{ $AutometiveCampaign->provider_name }}" placeholder="Provider Name" />
								@endif
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="service_name">Service Name</label>
								@if(is_null($AutometiveCampaign))
								<input type="text" name="service_name" id="service_name" value="{{ $Campaign->service_name }}" class="form-control" placeholder="Name of Service" />
								@else
								<input type="text" name="service_name" id="service_name" value="{{ $AutometiveCampaign->service_name }}" class="form-control" placeholder="Name of Service" />
								@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="provider_email">Email</label>
								@if(is_null($AutometiveCampaign))
								<input type="email" name="provider_email" id="provider_email" class="form-control" value="{{ $Campaign->service_provider_email }}" placeholder="Email" />
								@else
								<input type="email" name="provider_email" id="provider_email" class="form-control" value="{{ $AutometiveCampaign->provider_email }}" placeholder="Email" />
								@endif
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="provider_phone">Phone</label>
								@if(is_null($AutometiveCampaign))
								<input type="text" name="provider_phone" id="provider_phone" class="form-control" value="{{ $Campaign->service_provider_phone }}" placeholder="Phone" />
								@else
								<input type="text" name="provider_phone" id="provider_phone" class="form-control" value="{{ $AutometiveCampaign->provider_phone }}" placeholder="Phone" />
								@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="provider_url">URL</label>
								@if(is_null($AutometiveCampaign))
								<input type="url" name="provider_url" id="provider_url" class="form-control" value="{{ $Campaign->service_provider_url }}" placeholder="URL" />
								@else
								<input type="url" name="provider_url" id="provider_url" class="form-control" value="{{ $AutometiveCampaign->provider_url }}" placeholder="URL" />
								@endif
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="provider_zip">Zip</label>
								@if(is_null($AutometiveCampaign))
								<input type="text" name="provider_zip" id="provider_zip" class="form-control" placeholder="Zip" value="{{ $ServiceProvider->zip }}" onkeypress="return isNumberKey(event)"/>
								@else
								<input type="text" name="provider_zip" id="provider_zip" class="form-control" placeholder="Zip" value="{{ $AutometiveCampaign->provider_zip }}" onkeypress="return isNumberKey(event)"/>
								@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="provider_city">City</label>
								@if(is_null($AutometiveCampaign))
								<input type="text" name="provider_city" id="provider_city" class="form-control" placeholder="City" value="{{ $ServiceProvider->city }}"/>
								@else
								<input type="text" name="provider_city" id="provider_city" class="form-control" placeholder="City" value="{{ $AutometiveCampaign->provider_city }}"/>
								@endif
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="provider_state">State</label>
								@if(is_null($AutometiveCampaign))
								<input type="text" name="provider_state" id="provider_state" class="form-control" placeholder="State" value="{{ $ServiceProvider->state }}"/>
								@else
								<input type="text" name="provider_state" id="provider_state" class="form-control" placeholder="State" value="{{ $AutometiveCampaign->provider_state }}"/>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Vendor Details
					</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="vendor_name">Name</label>
								@if(is_null($AutometiveCampaign))
								<input type="text" name="vendor_name" id="vendor_name" class="form-control" value="{{ $Client->name }}" placeholder="Name" />
								@else
								<input type="text" name="vendor_name" id="vendor_name" class="form-control" value="{{ $AutometiveCampaign->vendor_name }}" placeholder="Name" />
								@endif
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="vendor_email">Email</label>
								@if(is_null($AutometiveCampaign))
								<input type="email" name="vendor_email" id="vendor_email" class="form-control" value="{{ $Client->crm_email }}" placeholder="Email" />
								@else
								<input type="email" name="vendor_email" id="vendor_email" class="form-control" value="{{ $AutometiveCampaign->vendor_email }}" placeholder="Email" />
								@endif
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="vendor_phone">Phone</label>
								@if(is_null($AutometiveCampaign))
								<input type="text" name="vendor_phone" id="vendor_phone" class="form-control" value="{{ $Client->mobile }}" placeholder="Phone" />
								@else
								<input type="text" name="vendor_phone" id="vendor_phone" class="form-control" value="{{ $AutometiveCampaign->vendor_phone }}" placeholder="Phone" />
								@endif
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="vendor_url">URL</label>
								@if(is_null($AutometiveCampaign))
								<input type="url" name="vendor_url" id="vendor_url" class="form-control" value="{{ $Client->url }}" placeholder="URL" />
								@else
								<input type="url" name="vendor_url" id="vendor_url" class="form-control" value="{{ $AutometiveCampaign->vendor_url }}" placeholder="URL" />
								@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="vendor_address">Address</label>
								@if(is_null($AutometiveCampaign))
								<input type="text" name="vendor_address" id="vendor_address" value="{{ $Client->address }}" class="form-control" placeholder="Address" />
								@else
								<input type="text" name="vendor_address" id="vendor_address" value="{{ $AutometiveCampaign->vendor_address }}" class="form-control" placeholder="Address" />
								@endif
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="vendor_zip">Zip</label>
								@if(is_null($AutometiveCampaign))
								<input type="text" name="vendor_zip" id="vendor_zip" class="form-control" placeholder="Zip" value="{{ $Client->zip }}" onkeypress="return isNumberKey(event)"/>
								@else
								<input type="text" name="vendor_zip" id="vendor_zip" class="form-control" placeholder="Zip" value="{{ $AutometiveCampaign->vendor_zip }}" onkeypress="return isNumberKey(event)"/>
								@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="vendor_city">City</label>
								@if(is_null($AutometiveCampaign))
								<input type="text" name="vendor_city" id="vendor_city" class="form-control" placeholder="City" value="{{ $Client->city }}"/>
								@else
								<input type="text" name="vendor_city" id="vendor_city" class="form-control" placeholder="City" value="{{ $AutometiveCampaign->vendor_city }}"/>
								@endif
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="control-label" for="vendor_state">State</label>
								@if(is_null($AutometiveCampaign))
								<input type="text" name="vendor_state" id="vendor_state" class="form-control" placeholder="State" value="{{ $Client->state }}"/>
								@else
								<input type="text" name="vendor_state" id="vendor_state" class="form-control" placeholder="State" value="{{ $AutometiveCampaign->vendor_state }}"/>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<button class="btn btn-default btn-lg" style="float:left; margin-bottom: 50px;" onclick="goBack()"><i class="fa fa-chevron-left" aria-hidden="true"></i><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</button>
	<button class="btn btn-lg" style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c !important;float:right; margin-bottom: 50px;" id="GoSecondStep">Next <i class="fa fa-chevron-right" aria-hidden="true"></i><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
</section>

<section class="content p-l-r-15" id="second-step" style="display:none">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Test Connection
					</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<table class="table table-striped table-borderd">
								<thead>
									<tr>
										<th colspan="4" style="text-align:center">
											<span style="font-weight:600; font-size:16px">Click the button to verify the connection work well</span> <button class="btn btn-info btn-md TestNow" data-toggle="modal" data-target="#SyncTest"><i class="fa fa-refresh" aria-hidden="true"></i> Test Now</button>
											<br /><br />
											<span style="font-weight:600; font-size:16px">Or test the connection using some previously collected leads</span>
										</th>
									</tr>
									<tr>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Email</th>
										<th>Phone</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									@foreach($Leads as $Lead)
									<tr>
										<td>@if($Lead->first_name == ''){{ $Lead->full_name }}@else{{ $Lead->first_name }}@endif</td>
										<td>{{ $Lead->last_name }}</td>
										<td>{{ $Lead->email }}</td>
										<td>{{ $Lead->phone_number }}</td>
										<td>
											<button class="btn btn-info btn-md SyncButton" onClick="LeadSync('{{ $Campaign->id}}', '{{ $Lead->id }}')"><i class="fa fa-refresh" aria-hidden="true"></i> Sync</button>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<p style="text-align:center; font-size:14px; font-weight:600">Please read the following suggestions and select the checkboxes to proceed.</p>
							<p>I TESTED at least one lead with <b>{{ $Campaign->custom_campaign_name }}</b> and it was synced with my CRM. <label style="float:right; margin-right: 20px;"><input type="checkbox" name="lead_sync[]" value="Test"></label></p>
							<p>The previously collected lead MUST BE manually synced (this will avoid email deliverability issues). <label style="float:right; margin-right: 20px;"><input type="checkbox"  name="lead_sync[]" value="Manually"></label></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<button class="btn btn-default btn-lg" style="float:left; margin-bottom: 50px;" id="BackFirstStep"><i class="fa fa-chevron-left" aria-hidden="true"></i><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</button>
	<button class="btn btn-lg" style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c !important;float:right; margin-bottom: 50px;"  id="GoThirdStep" disabled>Next <i class="fa fa-chevron-right" aria-hidden="true"></i><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
</section>

<section class="content p-l-r-15" id="third-step" style="display:none">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Summary
					</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<center>
								<h1>CONGRATULATIONS!</h1>
								<h3>Your connection is now completed!</h3>
								<h5>Please click on "Publish" to start syncing and create/active your lead ads campaign from facebook.</h5>
							</center>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<button class="btn btn-default btn-lg" style="float:left; margin-bottom: 50px;" id="BackSecondStep"><i class="fa fa-chevron-left" aria-hidden="true"></i><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</button>
	<button class="btn btn-primary btn-lg CamapignPublish" style="float:right; margin-bottom: 50px;"><i class="fa fa-check-square-o" aria-hidden="true"></i> Publish</button>
</section>
<!-- /.content -->

<div id="SyncTest" class="modal fade animated" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(154deg, #00c7be 0, #007091 50%, #ffc902 100%)!important;">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title"><i class="fa fa-question-circle-o" aria-hidden="true"></i> Test your connection (recommended)</h4>
			</div>
			<div class="modal-body">
				<div class="form-group" style="padding: 20px 0px;">
					<div class="col-md-4">
						<label for="first_name">First Name</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name">
						<small class="text-danger animated vendor_url fadeInUp campaign_add_error"></small>
					</div>
				</div>
				<div class="form-group" style="padding: 20px 0px;">
					<div class="col-md-4">
						<label for="last_name">Last Name</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name">
						<small class="text-danger animated vendor_url fadeInUp campaign_add_error"></small>
					</div>
				</div>
				<div class="form-group" style="padding: 20px 0px;">
					<div class="col-md-4">
						<label for="phone_number">Phone Number</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="Phone Number">
						<small class="text-danger animated vendor_url fadeInUp campaign_add_error"></small>
					</div>
				</div>
				<div class="form-group" style="padding: 20px 0px;">
					<div class="col-md-4">
						<label for="email">Email</label>
					</div>
					<div class="col-md-8">
						<input type="email" name="email" id="email" class="form-control" placeholder="Email">
						<small class="text-danger animated vendor_url fadeInUp campaign_add_error"></small>
					</div>
				</div>
				<div class="form-group" style="padding: 20px 0px;">
					<div class="col-md-4">
						<label for="city">City</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="city" id="city" class="form-control" placeholder="City">
						<small class="text-danger animated vendor_url fadeInUp campaign_add_error"></small>
					</div>
				</div>
				<div class="form-group" style="padding: 20px 0px;">
					<div class="col-md-4">
						<label for="post_code">Zip Code</label>
					</div>
					<div class="col-md-8">
						<input type="text" name="post_code" id="post_code" class="form-control" placeholder="Zip Code" onkeypress="return isNumberKey(event)">
						<small class="text-danger animated vendor_url fadeInUp campaign_add_error"></small>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default SettingSubmit" style=" color:#fff; background-color:#ffbe18 !important; border-color: #e4a70c !important;"> Submit</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

@stop				

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<!-- InputMask -->
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
<!-- end of page level js -->
<script>
	$(document).ready(function(){
		$("#phone_number").inputmask('(999) 999-9999');
		$("#provider_phone").inputmask('(999) 999-9999');
		$("#vendor_phone").inputmask('(999) 999-9999');
		$("#post_code").inputmask('99999');
		
		$("label").find('input').iCheck({
			checkboxClass: 'icheckbox_square-yellow',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});		
		
		$(".automatically_recognized").iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
		
		$(".SyncButton").click(function(){
			$(this).css('background-color', '#ffbe18 !important');
			$(this).css('border-color', '#e4a70c !important');
		});
		
		$(".po_email").hover(function(){
			if ($('#auto_email').is(':checked')) 
			{
				$("#po_email").html('Uncheck to not send the Fields');
			}
			else
			{
				$("#po_email").html('Check to send the fields');
			}
		});
		
		$(".po_first_name").hover(function(){
			if ($('#auto_first_name').is(':checked')) 
			{
				$("#po_first_name").html('Uncheck to not send the Fields');
			}
			else
			{
				$("#po_first_name").html('Check to send the fields');
			}
		});
		
		$(".po_last_name").hover(function(){
			if ($('#auto_last_name').is(':checked')) 
			{
				$("#po_last_name").html('Uncheck to not send the Fields');
			}
			else
			{
				$("#po_last_name").html('Check to send the fields');
			}
		});
		
		$(".po_phone_number").hover(function(){
			if ($('#auto_phone_number').is(':checked')) 
			{
				$("#po_phone_number").html('Uncheck to not send the Fields');
			}
			else
			{
				$("#po_phone_number").html('Check to send the fields');
			}
		});
		
		$(".po_city").hover(function(){
			if ($('#auto_city').is(':checked')) 
			{
				$("#po_city").html('Uncheck to not send the Fields');
			}
			else
			{
				$("#po_city").html('Check to send the fields');
			}
		});
		
		$(".po_zip_code").hover(function(){
			if ($('#auto_zip_code').is(':checked')) 
			{
				$("#po_zip_code").html('Uncheck to not send the Fields');
			}
			else
			{
				$("#po_zip_code").html('Check to send the fields');
			}
		});
		
		/* popover panel fifa js */
		$('.po-markup > .po-link').popover({
			trigger: 'hover',
			
			html: true, // must have if HTML is contained in popover
			// get the title and conent
			title: function() {
				return $(this).parent().find('.po-title').html();
			},
			content: function() {
				return $(this).parent().find('.po-body').html();
			},
			container: 'body',
			placement: 'right'
		});
		
		$('#auto_city').on('ifChecked', function(event){
			$("#po_city").html('Uncheck to not send the Fields');
		});
		
		$('#auto_city').on('ifUnchecked', function(event){
			$("#po_city").html('Check to send the fields');
		});
		
		$('#auto_last_name').on('ifChecked', function(event){
			$("#po_last_name").html('Uncheck to not send the Fields');
		});
		
		$('#auto_last_name').on('ifUnchecked', function(event){
			$("#po_last_name").html('Check to send the fields');
		});
		
		$('#auto_first_name').on('ifChecked', function(event){
			$("#po_first_name").html('Uncheck to not send the Fields');
		});
		
		$('#auto_first_name').on('ifUnchecked', function(event){
			$("#po_first_name").html('Check to send the fields');
		});
		
		$('#auto_email').on('ifChecked', function(event){
			$("#po_email").html('Uncheck to not send the Fields');
		});
		
		$('#auto_email').on('ifUnchecked', function(event){
			$("#po_email").html('Check to send the fields');
		});
		
		$('#auto_zip_code').on('ifChecked', function(event){
			$("#po_zip_code").html('Uncheck to not send the Fields');
		});
		
		$('#auto_zip_code').on('ifUnchecked', function(event){
			$("#po_zip_code").html('Check to send the fields');
		});
		
		$('#auto_phone_number').on('ifChecked', function(event){
			$("#po_phone_number").html('Uncheck to not send the Fields');
		});
		
		$('#auto_phone_number').on('ifUnchecked', function(event){
			$("#po_phone_number").html('Check to send the fields');
		});
		
		$('#GoSecondStep').click(function(){
			$('#first-step').css('display', 'none');
			$('#second-step').css('display', 'block');
		});
		$('#GoThirdStep').click(function(){
			$('#second-step').css('display', 'none');
			$('#third-step').css('display', 'block');
		});
		$('#BackFirstStep').click(function(){
			$('#first-step').css('display', 'block');
			$('#second-step').css('display', 'none');
		});
		$('#BackSecondStep').click(function(){
			$('#second-step').css('display', 'block');
			$('#third-step').css('display', 'none');
		});
		
		$('input[type=checkbox]').on('ifChecked', function(event){
			var checked = []
			$("input[name='lead_sync[]']:checked").each(function ()
			{
				checked.push($(this).val());
			});
			if(checked.length==2)
			{
				$("#GoThirdStep").prop('disabled', false);
			}
			else
			{
				$("#GoThirdStep").prop('disabled', true);
			}
		});
		
		$('input[type=checkbox]').on('ifUnchecked', function(event){
			var checked = []
			$("input[name='lead_sync[]']:checked").each(function ()
			{
				checked.push($(this).val());
			});
			if(checked.length==2)
			{
				$("#GoThirdStep").prop('disabled', false);
			}
			else
			{
				$("#GoThirdStep").prop('disabled', true);
			}
		});
		
		$('.SettingSubmit').click(function(){
			$(".SettingSubmit").prop("disabled", true);
			jQuery('#date-loader').css("display","block");
			
			var campaign_id = "{{ $Campaign->id }}";
			
			var auto_email = '';
			var auto_first_name = '';
			var auto_last_name = '';
			var auto_phone_number = '';
			var auto_city = '';
			var auto_zip_code = '';
			
			if ($('#auto_email').is(":checked")){
				auto_email = 'Yes';
				}else{ 
				auto_email = 'No';
			}
			
			if ($('#auto_first_name').is(":checked")){
				auto_first_name = 'Yes';
				}else{ 
				auto_first_name = 'No';
			}
			
			if ($('#auto_last_name').is(":checked")){
				auto_last_name = 'Yes';
				}else{ 
				auto_last_name = 'No';
			}
			
			if ($('#auto_phone_number').is(":checked")){
				auto_phone_number = 'Yes';
				}else{ 
				auto_phone_number = 'No';
			}
			
			if ($('#auto_city').is(":checked")){
				auto_city = 'Yes';
				}else{ 
				auto_city = 'No';
			}
			
			if ($('#auto_zip_code').is(":checked")){
				auto_zip_code = 'Yes';
				}else{ 
				auto_zip_code = 'No';
			}
			
			var prospect = $('#prospect').val();
			var phone_time = $('#phone_time').val();
			var phone_type = $('#phone_type').val();
			
			var provider_name = $('#provider_name').val();
			var service_name = $('#service_name').val();
			var provider_email = $('#provider_email').val();
			var provider_phone = $('#provider_phone').val();
			var provider_url = $('#provider_url').val();
			var provider_zip = $('#provider_zip').val();
			var provider_city = $('#provider_city').val();
			var provider_state = $('#provider_state').val();
			
			var vendor_name = $('#vendor_name').val();
			var vendor_email = $('#vendor_email').val();
			var vendor_phone = $('#vendor_phone').val();
			var vendor_url = $('#vendor_url').val();
			var vendor_address = $('#vendor_address').val();
			var vendor_zip = $('#vendor_zip').val();
			var vendor_city = $('#vendor_city').val();
			var vendor_state = $('#vendor_state').val();
			
			var first_name = $('#first_name').val();
			var last_name = $('#last_name').val();
			var email = $('#email').val();
			var phone_number = $('#phone_number').val();
			var city = $('#city').val();
			var post_code = $('#post_code').val();
			
			$.get("{{ route('create-test-adf-xml') }}",{campaign_id: campaign_id, provider_name: provider_name, service_name: service_name, provider_email: provider_email, provider_phone: provider_phone, provider_url: provider_url, provider_zip: provider_zip, provider_city: provider_city, provider_state: provider_state, vendor_name: vendor_name, vendor_email: vendor_email, vendor_phone: vendor_phone, vendor_url: vendor_url, vendor_address: vendor_address,vendor_zip: vendor_zip, vendor_city: vendor_city, vendor_state: vendor_state, first_name: first_name, last_name: last_name, email: email, phone_number: phone_number, city: city, post_code: post_code, prospect: prospect, phone_time: phone_time, phone_type: phone_type, auto_email: auto_email, auto_first_name: auto_first_name, auto_last_name: auto_last_name, auto_phone_number: auto_phone_number, auto_city: auto_city, auto_zip_code: auto_zip_code},function(data){ 
				jQuery('#date-loader').css("display","none");
				$('#SyncTest').modal('hide');
				$('.TestNow').css('background-color', '#ffbe18 !important');
				$('.TestNow').css('border-color', '#e4a70c !important');
				$(".SettingSubmit").prop("disabled", false);
			});
		});
		
		$('.CamapignPublish').click(function(){
			$(".CamapignPublish").prop("disabled", true);
			jQuery('#date-loader').css("display","block");
			
			var campaign_id = "{{ $Campaign->id }}";
			
			var auto_email = '';
			var auto_first_name = '';
			var auto_last_name = '';
			var auto_phone_number = '';
			var auto_city = '';
			var auto_zip_code = '';
			
			if ($('#auto_email').is(":checked")){
				auto_email = 'Yes';
				}else{ 
				auto_email = 'No';
			}
			
			if ($('#auto_first_name').is(":checked")){
				auto_first_name = 'Yes';
				}else{ 
				auto_first_name = 'No';
			}
			
			if ($('#auto_last_name').is(":checked")){
				auto_last_name = 'Yes';
				}else{ 
				auto_last_name = 'No';
			}
			
			if ($('#auto_phone_number').is(":checked")){
				auto_phone_number = 'Yes';
				}else{ 
				auto_phone_number = 'No';
			}
			
			if ($('#auto_city').is(":checked")){
				auto_city = 'Yes';
				}else{ 
				auto_city = 'No';
			}
			
			if ($('#auto_zip_code').is(":checked")){
				auto_zip_code = 'Yes';
				}else{ 
				auto_zip_code = 'No';
			}
			
			var prospect = $('#prospect').val();
			var phone_time = $('#phone_time').val();
			var phone_type = $('#phone_type').val();
			
			var provider_name = $('#provider_name').val();
			var service_name = $('#service_name').val();
			var provider_email = $('#provider_email').val();
			var provider_phone = $('#provider_phone').val();
			var provider_url = $('#provider_url').val();
			var provider_zip = $('#provider_zip').val();
			var provider_city = $('#provider_city').val();
			var provider_state = $('#provider_state').val();
			
			var vendor_name = $('#vendor_name').val();
			var vendor_email = $('#vendor_email').val();
			var vendor_phone = $('#vendor_phone').val();
			var vendor_url = $('#vendor_url').val();
			var vendor_address = $('#vendor_address').val();
			var vendor_zip = $('#vendor_zip').val();
			var vendor_city = $('#vendor_city').val();
			var vendor_state = $('#vendor_state').val();
			
			var checked = []
			$("input[name='lead_sync[]']:checked").each(function ()
			{
				checked.push($(this).val());
			});
			
			$.get("{{ route('campaign-publish') }}",{campaign_id: campaign_id, auto_email: auto_email, auto_first_name: auto_first_name, auto_last_name: auto_last_name, auto_phone_number: auto_phone_number, auto_city: auto_city, auto_zip_code: auto_zip_code, provider_name: provider_name, service_name: service_name, provider_email: provider_email, provider_phone: provider_phone, provider_url: provider_url, provider_zip: provider_zip, provider_city: provider_city, provider_state: provider_state, vendor_name: vendor_name, vendor_email: vendor_email, vendor_phone: vendor_phone, vendor_url: vendor_url, vendor_address: vendor_address,vendor_zip: vendor_zip, vendor_city: vendor_city, vendor_state: vendor_state, checked: checked, prospect: prospect, phone_time: phone_time, phone_type: phone_type},function(data){ 
				jQuery('#date-loader').css("display","none");
				if(data == 'success')
				{
					window.location.href = "{{ route('campaign-publish-success') }}";
				}
				else
				{
					$(".CamapignPublish").prop("disabled", false);
				}
			});
		});
	});
	
	function isNumberKey(evt){
		var charCode = (evt.which) ? evt.which : event.keyCode
		if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
		return true;
	}
	
	function LeadSync(campaign_id, facebook_ads_lead_user_id)
	{
		jQuery('#date-loader').css("display","block");
		
		var auto_email = '';
		var auto_first_name = '';
		var auto_last_name = '';
		var auto_phone_number = '';
		var auto_city = '';
		var auto_zip_code = '';
		
		if ($('#auto_email').is(":checked")){
			auto_email = 'Yes';
			}else{ 
			auto_email = 'No';
		}
		
		if ($('#auto_first_name').is(":checked")){
			auto_first_name = 'Yes';
			}else{ 
			auto_first_name = 'No';
		}
		
		if ($('#auto_last_name').is(":checked")){
			auto_last_name = 'Yes';
			}else{ 
			auto_last_name = 'No';
		}
		
		if ($('#auto_phone_number').is(":checked")){
			auto_phone_number = 'Yes';
			}else{ 
			auto_phone_number = 'No';
		}
		
		if ($('#auto_city').is(":checked")){
			auto_city = 'Yes';
			}else{ 
			auto_city = 'No';
		}
		
		if ($('#auto_zip_code').is(":checked")){
			auto_zip_code = 'Yes';
			}else{ 
			auto_zip_code = 'No';
		}
		
		var prospect = $('#prospect').val();
		var phone_time = $('#phone_time').val();
		var phone_type = $('#phone_type').val();
		
		var provider_name = $('#provider_name').val();
		var service_name = $('#service_name').val();
		var provider_email = $('#provider_email').val();
		var provider_phone = $('#provider_phone').val();
		var provider_url = $('#provider_url').val();
		var provider_zip = $('#provider_zip').val();
		var provider_city = $('#provider_city').val();
		var provider_state = $('#provider_state').val();
		
		var vendor_name = $('#vendor_name').val();
		var vendor_email = $('#vendor_email').val();
		var vendor_phone = $('#vendor_phone').val();
		var vendor_url = $('#vendor_url').val();
		var vendor_address = $('#vendor_address').val();
		var vendor_zip = $('#vendor_zip').val();
		var vendor_city = $('#vendor_city').val();
		var vendor_state = $('#vendor_state').val();
		
		$.get("{{ route('create-manual-adf-xml') }}",{campaign_id: campaign_id, facebook_ads_lead_user_id: facebook_ads_lead_user_id, provider_name: provider_name, service_name: service_name, provider_email: provider_email, provider_phone: provider_phone, provider_url: provider_url, provider_zip: provider_zip, provider_city: provider_city, provider_state: provider_state, vendor_name: vendor_name, vendor_email: vendor_email, vendor_phone: vendor_phone, vendor_url: vendor_url, vendor_address: vendor_address,vendor_zip: vendor_zip, vendor_city: vendor_city, vendor_state: vendor_state, prospect: prospect, phone_time: phone_time, phone_type: phone_type, auto_email: auto_email, auto_first_name: auto_first_name, auto_last_name: auto_last_name, auto_phone_number: auto_phone_number, auto_city: auto_city, auto_zip_code: auto_zip_code},function(data){ 
			//swal('Please check your CRM configure email');
			jQuery('#date-loader').css("display","none");
		});
	}
	
	function goBack() {
		window.history.back();
	}
</script>
@stop