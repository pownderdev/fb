@extends('layouts/default')

{{-- Page title --}}
@section('title')
Dashboard
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/leaflet/css/leaflet.css')}}"/>
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	
	.export.btn-group {*display:none!important;}
	
	.animation-hatch small{font-size:45%;}
	
	.animation-hatch {*cursor:pointer;}
	
	.top-bar .multiselect.dropdown-toggle.btn.btn-default{ width: 160px; }
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2" style="margin-top: -3px;">
	<div class="row">
		<div class="col-md-4 col-sm-6">
			<div class="header-data">
				<h1>Dashboard</h1>
				<p>Welcome to Pownder <small class="tm">TM</small> &nbsp , {{ Session::get('user_name') }} </p>
			</div>
		</div>
		
		<div class="col-md-8 col-sm-6 col-xs-12 pull-right header-top">
			<div class="row text-center">
				<div class="col-xs-6 col-sm-2 col-md-offset-6">
					<a href="#"> 
						<h2 class="animation-hatch">
							<strong id="count-like" style="color: #fff;" class="after-k">119</strong>
							<br>
							<small><i class="fa fa-fw fa-thumbs-o-up"></i> Likes</small>
						</h2>
					</a>
				</div>
				<div class="col-xs-6 col-sm-2">
					<a href="#">
						<h2 class="animation-hatch">
							<strong id="count-reach" style="color: #fff;" class="after-k">197</strong>
							<br>
							<small><i class="fa fa-heart-o"></i> Reach</small>
						</h2>
					</a>
				</div>
				<div class="col-xs-6 col-sm-2">
					<a href="{{ route('cleads') }}">
						<h2 class="animation-hatch">
							<strong  style="color: #fff;">{{ Helper::NumberFormat(number_format(ClientHelper::TotalLeads(),2)) }}</strong>
							<br>
							<small><i class="fa fa-fw fa-trophy"></i> Results</small>
						</h2>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row tiles-row">
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
					<div class="canvas-interactive-wrapper1">
						<canvas id="canvas-interactive1" class="grad1"></canvas>
						<a href="{{ route('cleads') }}?date_range={{ date('Y-m-d') }} to {{ date('Y-m-d') }}" id="lead_url">
							<div class="cta-wrapper1">
								<div class="widget" data-count=".num" data-from="0" data-to="99.9" data-suffix="%" data-duration="2">
									<div class="item">
										<div class="widget-icon pull-left icon-color animation-fadeIn">
											<i class="menu-icon fa fa-fw fa-line-chart fa-size"></i>
										</div>
									</div>
									<div class="widget-count panel-white">
										<div class="item-label text-center">
											<div id="month_lead" class="count-box">
												{{ Helper::NumberFormat(number_format($TodayLead,2)) }}
											</div>
											<span class="title">Leads</span>
										</div>
									</div>
								</div>
							</div>
						</a>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
					<div class="widget" data-count=".num" data-from="0" data-to="512" data-duration="3">
						<div class="canvas-interactive-wrapper2">
							<canvas id="canvas-interactive2" class="grad1"></canvas>
							<a href="{{ route('reports') }}"> 
								<div class="cta-wrapper2">
									<div class="item">
										<div class="widget-icon pull-left icon-color animation-fadeIn">
											<i class="fa fa-fw fa-car fa-size"></i>
										</div>
									</div>
									<div class="widget-count panel-white">
										<div class="item-label text-center">
											<div class="count-box" id="Sold">{{ Helper::NumberFormat(number_format($Sold,2)) }}</div>
											<span class="title">Sold</span>
										</div>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
					<div class="widget" data-suffix="k" data-count=".num" data-from="0" data-to="310" data-duration="4" data-easing="false">
						<div class="canvas-interactive-wrapper3">
							<canvas id="canvas-interactive3" class="grad1"></canvas>
							<a href="#"> 
								<div class="cta-wrapper3">
									<div class="item">
										<div class="widget-icon pull-left icon-color animation-fadeIn">
											<i class="fa fa-bar-chart-o fa-size"></i>
										</div>
									</div>
									<div class="widget-count panel-white">
										<div class="item-label text-center">
											<div class="count-box" id="LGR">
												@if($Click == 0 || $facebook_ads_lead_users == 0)
												0%
												@else
												{{ Helper::NumberFormat(number_format(($facebook_ads_lead_users/$Click)*100,2)) }}%
												@endif
											</div>
											<span class="title">Lead Generation Rate</span>
										</div>
									</div>
								</div>
							</a> 
						</div>
					</div>
				</div>
				
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 tile-bottom">
					<div class="widget">
						<div class="canvas-interactive-wrapper4">
							<canvas id="canvas-interactive4" class="grad1"></canvas>
							<a href="#"> 
								<div class="cta-wrapper4">
									<div class="item">
										<div class="widget-icon pull-left icon-color animation-fadeIn">
											<i class="fa fa-fw fa-sticky-note-o fa-size"></i>
										</div>
									</div>
									<div class="widget-count panel-white">
										<div class="item-label text-center">
											<div class="count-box" id="Appointment">{{ Helper::NumberFormat(number_format($Appointment,2)) }}</div>
											<span class="title">Appointment</span>
										</div>
									</div>
								</div>
							</a> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel">
				<div class="panel-heading">
					<h3 class="heading_text panel-title">Sales</h3>
					<span class="pull-right line-bar-charts">
						<button class="btn btn-sm btn-link chart_switch" data-chart="bar">Bar Chart</button>
						<button class="btn btn-sm btn-default chart_switch" data-chart="line">Line Chart
						</button>
					</span>
				</div>
				<div class="panel-body">
					<div id="sales-line-bar" style="height:400px"></div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="col-md-12 col-xs-12" style="padding:0px !important;" id="mainMap">
				<div id="map" style="width: 100%; height: 400px;"></div>
			</div>
		</div>
	</div>
</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script src="http://maps.google.com/maps/api/js?key=AIzaSyCerha5x7NxLWIlKdBytL4JjjCWTTTHHCM&sensor=false" type="text/javascript"></script>
<script type="text/javascript">
    var locations = [
	@php $i = 1; @endphp
	@foreach($CityLeads as $CityLead)
	@if($i == count($CityLeads))
	["{{ title_case($CityLead->city) }} - {{ $CityLead->Leads }} Leads", {{ number_format($CityLead->latitude,7) }}, {{ number_format($CityLead->longitude,7) }}, {{ $i++ }}]
	@else
	["{{ title_case($CityLead->city) }} - {{ $CityLead->Leads }} Leads", {{ number_format($CityLead->latitude,7) }}, {{ number_format($CityLead->longitude,7) }}, {{ $i++ }}],
	@endif
	@endforeach
    ];
	
    var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 10,
		center: new google.maps.LatLng(34.0522342, -118.2436849),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});
	
    var infowindow = new google.maps.InfoWindow();
	
	map.setOptions({ minZoom: 2, maxZoom: 30 });
	
    var marker, i;
	
    for (i = 0; i < locations.length; i++) {  
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			map: map
		});
		
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				infowindow.setContent(locations[i][0]);
				infowindow.open(map, marker);
			}
		})(marker, i));
	}
</script>
<script>
	$(document).ready(function () {
		$('#count-box').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 2
		});
		$('#count-like').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 2
		});
		$('#count-reach').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 2
		});
		$('#count-ads').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 2
		});
		$('#count-result').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 2
		});
		$('#count-box2').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 5
		});
		$('#count-box3').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 7
		});
		$('#count-box4').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 10
		});
		$('#count-pro').CountUpCircle({
			duration: 2500,
			opacity_anim: true,
			step_divider: 10
		});
	});
</script>

<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-multiselect/js/bootstrap-multiselect.js')}}"></script>
<script>
	$( document ).ready(function() {
		var extra_features = '<button class="btn btn-default" type="button" name="mail" title="Mail" ><i class="fa fa-fw fa-envelope"></i></button><button class="btn btn-default" type="button" name="print" title="Print" ><i class="fa fa-fw fa-print"></i></button>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
		
		$("#multiselect1").multiselect();
		
		$("#multiselect1").change(function(){
			var set_val = $(this).val();
			//alert(set_val);
			if(set_val == "This Month" || set_val == "Last Month" || set_val == "This Year" || set_val == "Life Time"){
				$(".date_range").css({ "width": "263px", "display": "block"});
				}else{
				$(".date_range").css({ "width": "0px", "display": "none"});
			}
		});
		
		var x_date = ['x'];
		var campaign_clicks = ['Clicks'];
		var campaign_ctr = ['CTR (%)'];
		var campaign_frequency = ['Frequency'];
		var campaign_lgr = ['LGR (%)'];
		var campaign_leads = ['Leads'];
		
		@php $imp = 0; @endphp
		@foreach($facebook_campaign_values as $facebook_campaign_value)
		var ctr = {{ $GraphCTR[$imp] }};
		campaign_ctr.push(parseFloat(ctr).toFixed(2));
		
		var frequency = {{ $GraphFreq[$imp] }};
		campaign_frequency.push(parseFloat(frequency).toFixed(2));
		
		var lgr = {{ $GraphLGR[$imp] }};
		campaign_lgr.push(parseFloat(lgr).toFixed(2));
		
		campaign_leads.push({{ $GraphLead[$imp] }});
		campaign_clicks.push({{ $GraphClick[$imp] }});
		x_date.push('{{ $facebook_campaign_value->campaign_date }}');
		@php $imp++; @endphp
		@endforeach
		
		var core_chart1 = {        
			main_dashboard: function () {            
				if ($('#sales-line-bar').length) {                
					var sales_line_bar_chart = c3.generate({                    
						bindto: '#sales-line-bar',                    
						data: {                        
							x: 'x',                        
							columns: [x_date, campaign_clicks, campaign_ctr, campaign_frequency, campaign_leads, campaign_lgr],
							types: {'Clicks': 'area', 'CTR (%)': 'area', 'Frequency': 'area', 'LGR (%)': 'area', 'Leads': 'area', 'Clicks': 'line', 'CTR (%)': 'line', 'Frequency': 'line', 'LGR (%)': 'line', 'Leads': 'line'}                    
						},                    
						axis: {                        
							x: {                            
								type: 'timeseries',                        
								tick: {                               
									culling: false,                  
									fit: true,                       
									format: "%d %b"                     
								}                  
							},                 
							y: {                
								tick: {          
									format: d3.format("")  
								}                   
							}                
						},              
						point: {        
							r: '4',       
							focus: {        
								expand: { r: '5' }             
							}             
						},          
						bar: {        
							width: { ratio: 0.5 }             
						},            
						grid: {         
							x: { show: true	},               
							y: { show: true }              
						},             
						color: {       
						pattern: ['#428bca', '#ffb65f', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']                    }               
					});               
					$('.chart_switch').on('click', function () {   
						if ($(this).data('chart') == 'line') {     
							sales_line_bar_chart.transform('line');  
							} else if ($(this).data('chart') == 'bar') {       
							sales_line_bar_chart.transform('bar');              
						}                  
						if (!$(this).hasClass("btn-default")) {      
							$('.chart_switch').toggleClass('btn-default btn-link');      
						}            
					});            
					$(window).on("debouncedresize", function () {       
						sales_line_bar_chart.resize();            
					});                             
					$("[data-toggle='offcanvas']").click(function (e) {           
						sales_line_bar_chart.resize();             
					});        
				}      
			}  
		};   
		core_chart1.main_dashboard();
		
		$('.button-select').click(function(evt){
			jQuery('#date-loader').css("display","block");
			var dateString = $('#date-range-header').val();
			if(dateString == '')
			{
				var date_start = "{{ date('Y-m-d') }}";
				var date_stop = "{{ date('Y-m-d') }}";
				$("#lead_url").attr('href', "{{ route('cleads') }}?date_range={{ date('Y-m-d') }} to {{ date('Y-m-d') }}");
			}
			else
			{
				var dateArray = dateString.split(' to ');
				var date_start = dateArray[0];
				var date_stop = dateArray[1];
				
				var start = date_start.split('-');
				var stop = date_stop.split('-');
				
				$("#lead_url").attr('href', "{{ route('cleads') }}?date_range="+dateString);
			}
			$.get("{{ route('client-dashboard-ajax') }}",{date_start: date_start, date_stop:date_stop},function(data){ 
				$('#Sold').html(data.Sold);
				$('#Appointment').html(data.Appointment);
				$('#LGR').html(data.LGR);
				$('#month_lead').html(data.Leads);
				
				$("#mainMap").html('<div id="map" style="width: 100%; height: 400px;"></div>');
				
				var j = 1;
				var locations = [];
				var CityLeads = data.CityLeads;
				for(var i=0;i<CityLeads.length;i++)
				{
					var MapLocation = [];
					MapLocation.push(CityLeads[i].city+' - '+CityLeads[i].Leads+' Leads');
					MapLocation.push(CityLeads[i].latitude);
					MapLocation.push(CityLeads[i].longitude);
					MapLocation.push(j);
					locations.push(MapLocation); 
					j++;
				} 
				
				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 10,
					center: new google.maps.LatLng(34.0522342, -118.2436849),
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				
				var infowindow = new google.maps.InfoWindow();
				map.setOptions({ minZoom: 2, maxZoom: 30 });
				
				var marker, i;
				for (i = 0; i < locations.length; i++) { 
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(locations[i][1], locations[i][2]),
						map: map
					});
					
					google.maps.event.addListener(marker, 'click', (function(marker, i) {
						return function() {
							infowindow.setContent(locations[i][0]);
							infowindow.open(map, marker);
						}
					})(marker, i));
				}
				
				/////////////////////////////////////////////Graph/////////////////////////////////////
				var facebook_campaign_values = data.facebook_campaign_values;
				var GraphCTR = data.GraphCTR;
				var GraphFreq = data.GraphFreq;
				var GraphLGR = data.GraphLGR;
				var GraphLead = data.GraphLead;
				var GraphClick = data.GraphClick;
				$("#display_graph").html('<div id="sales-line-bar" style="height:400px"></div>');
				var x_date = ['x'];
				var campaign_clicks = ['Clicks'];
				var campaign_ctr = ['CTR (%)'];
				var campaign_frequency = ['Frequency'];
				var campaign_lgr = ['LGR (%)'];
				var campaign_leads = ['Leads'];
				
				for(var ai=0;ai<facebook_campaign_values.length;ai++)
				{ 
					var ctr = GraphCTR[ai];
					campaign_ctr.push(parseFloat(ctr).toFixed(2));
					
					var frequency = GraphFreq[ai];
					campaign_frequency.push(parseFloat(frequency).toFixed(2));
					
					var lgr = GraphLGR[ai];
					campaign_lgr.push(parseFloat(lgr).toFixed(2));
					
					campaign_leads.push(GraphLead[ai]);
					campaign_clicks.push(GraphClick[ai]);
					x_date.push(facebook_campaign_values[ai].campaign_date);
				}
				
				var core_chart1 = {        
					main_dashboard: function () {            
						if ($('#sales-line-bar').length) {                
							var sales_line_bar_chart = c3.generate({                    
								bindto: '#sales-line-bar',                    
								data: {                        
									x: 'x',                        
									columns: [x_date, campaign_clicks, campaign_ctr, campaign_frequency, campaign_leads, campaign_lgr],                 
									types: {'Clicks': 'area', 'CTR (%)': 'area', 'Frequency': 'area', 'LGR (%)': 'area', 'Leads': 'area', 'Clicks': 'line', 'CTR (%)': 'line', 'Frequency': 'line', 'LGR (%)': 'line', 'Leads': 'line'}                    
								},                    
								axis: {                        
									x: { type: 'timeseries', tick: { culling: false, fit: true, format: "%d%b " } },                 
									y: { tick: { format: d3.format("") } }                
								},              
								point: { r: '4', focus: { expand: { r: '5' } } },          
								bar: { width: { ratio: 0.5 } },            
								grid: {         
									x: { show: true	},               
									y: { show: true }              
								},             
								color: {       
								pattern: ['#428bca', '#ffb65f', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']                    }               
							});               
							$('.chart_switch').on('click', function () {   
								if ($(this).data('chart') == 'line') {     
									sales_line_bar_chart.transform('line');
									} else if ($(this).data('chart') == 'bar') {       
									sales_line_bar_chart.transform('bar');              
								}                  
								if (!$(this).hasClass("btn-default")) {      
									$('.chart_switch').toggleClass('btn-default btn-link');      
								}            
							});            
							$(window).on("debouncedresize", function () {       
								sales_line_bar_chart.resize();            
							});                             
							$("[data-toggle='offcanvas']").click(function (e) {           
								sales_line_bar_chart.resize();             
							});        
						}      
					}  
				};   
				core_chart1.main_dashboard();
				////////////////////////////////////////////Graph//////////////////////////////////////
				
				jQuery('#date-loader').css("display", "none");
			});
		});
		
		$('.button-clear').click(function(evt){
			jQuery('#date-loader').css("display","block");
			var date_start = "{{ date('Y-m-d') }}";
			var date_stop = "{{ date('Y-m-d') }}";
			$("#lead_url").attr('href', "{{ route('cleads') }}?date_range={{ date('Y-m-d') }} to {{ date('Y-m-d') }}");
			$.get("{{ route('client-dashboard-ajax') }}",{date_start: date_start, date_stop:date_stop},function(data){ 
				$('#Sold').html(data.Sold);
				$('#Appointment').html(data.Appointment);
				$('#LGR').html(data.LGR);
				$('#month_lead').html(data.Leads);
				
				$("#mainMap").html('<div id="map" style="width: 100%; height: 400px;"></div>');
				
				var j = 1;
				var locations = [];
				var CityLeads = data.CityLeads;
				for(var i=0;i<CityLeads.length;i++)
				{
					var MapLocation = [];
					MapLocation.push(CityLeads[i].city+' - '+CityLeads[i].Leads+' Leads');
					MapLocation.push(CityLeads[i].latitude);
					MapLocation.push(CityLeads[i].longitude);
					MapLocation.push(j);
					locations.push(MapLocation); 
					j++;
				} 
				
				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 10,
					center: new google.maps.LatLng(34.0522342, -118.2436849),
					mapTypeId: google.maps.MapTypeId.ROADMAP
				});
				
				var infowindow = new google.maps.InfoWindow();
				map.setOptions({ minZoom: 2, maxZoom: 30 });
				
				var marker, i;
				for (i = 0; i < locations.length; i++) { 
					marker = new google.maps.Marker({
						position: new google.maps.LatLng(locations[i][1], locations[i][2]),
						map: map
					});
					
					google.maps.event.addListener(marker, 'click', (function(marker, i) {
						return function() {
							infowindow.setContent(locations[i][0]);
							infowindow.open(map, marker);
						}
					})(marker, i));
				}
				
				/////////////////////////////////////////////Graph/////////////////////////////////////
				var facebook_campaign_values = data.facebook_campaign_values;
				var GraphCTR = data.GraphCTR;
				var GraphFreq = data.GraphFreq;
				var GraphLGR = data.GraphLGR;
				var GraphLead = data.GraphLead;
				var GraphClick = data.GraphClick;
				$("#display_graph").html('<div id="sales-line-bar" style="height:400px"></div>');
				var x_date = ['x'];
				var campaign_clicks = ['Clicks'];
				var campaign_ctr = ['CTR (%)'];
				var campaign_frequency = ['Frequency'];
				var campaign_lgr = ['LGR (%)'];
				var campaign_leads = ['Leads'];
				
				for(var ai=0;ai<facebook_campaign_values.length;ai++)
				{ 
					var ctr = GraphCTR[ai];
					campaign_ctr.push(parseFloat(ctr).toFixed(2));
					
					var frequency = GraphFreq[ai];
					campaign_frequency.push(parseFloat(frequency).toFixed(2));
					
					var lgr = GraphLGR[ai];
					campaign_lgr.push(parseFloat(lgr).toFixed(2));
					
					campaign_leads.push(GraphLead[ai]);
					campaign_clicks.push(GraphClick[ai]);
					x_date.push(facebook_campaign_values[ai].campaign_date);
				}
				
				var core_chart1 = {        
					main_dashboard: function () {            
						if ($('#sales-line-bar').length) {                
							var sales_line_bar_chart = c3.generate({                    
								bindto: '#sales-line-bar',                    
								data: {                        
									x: 'x',                        
									columns: [x_date, campaign_clicks, campaign_ctr, campaign_frequency, campaign_leads, campaign_lgr],                 
									types: {'Clicks': 'area', 'CTR (%)': 'area', 'Frequency': 'area', 'LGR (%)': 'area', 'Leads': 'area', 'Clicks': 'line', 'CTR (%)': 'line', 'Frequency': 'line', 'LGR (%)': 'line', 'Leads': 'line'}                    
								},                    
								axis: {                        
									x: { type: 'timeseries', tick: { culling: false, fit: true, format: "%d%b " } },                 
									y: { tick: { format: d3.format("") } }                
								},              
								point: { r: '4', focus: { expand: { r: '5' } } },          
								bar: { width: { ratio: 0.5 } },            
								grid: {         
									x: { show: true	},               
									y: { show: true }              
								},             
								color: {       
								pattern: ['#428bca', '#ffb65f', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']                    }               
							});               
							$('.chart_switch').on('click', function () {   
								if ($(this).data('chart') == 'line') {     
									sales_line_bar_chart.transform('line');
									} else if ($(this).data('chart') == 'bar') {       
									sales_line_bar_chart.transform('bar');              
								}                  
								if (!$(this).hasClass("btn-default")) {      
									$('.chart_switch').toggleClass('btn-default btn-link');      
								}            
							});            
							$(window).on("debouncedresize", function () {       
								sales_line_bar_chart.resize();            
							});                             
							$("[data-toggle='offcanvas']").click(function (e) {           
								sales_line_bar_chart.resize();             
							});        
						}      
					}  
				};   
				core_chart1.main_dashboard();
				////////////////////////////////////////////Graph//////////////////////////////////////
				
				jQuery('#date-loader').css("display", "none");
			});
		});
	});
</script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}" ></script>
<!-- bootstrap time picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/jquerydaterangepicker/js/jquery.daterangepicker.min.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/timedropper/js/timedropper.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/Buttons/js/buttons.js')}}"></script>
@stop	