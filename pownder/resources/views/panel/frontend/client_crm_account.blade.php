@extends('layouts/default')

{{-- Page title --}}
@section('title')
Client CRM Account List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/js/sumoselect/sumoselect.css')}}">
<link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet" type="text/css"/>
<!--<link href="{{asset('assets/css/menubarfold.css')}}" rel="stylesheet">-->

<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	.fixed-table-container {height: 500px !important; }
	.export.btn-group {*display:none!important;}
	.fixed-table-toolbar {*width:60%;}
	.fixed-table-toolbar .search {width: Calc( 100% - 110px );}
	
	#crm_modal {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	
	#crm_modal .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
	
	@media screen and (max-width: 767px) {
	.fixed-table-toolbar {width: 100%;}
	.fixed-table-toolbar {width:100%;}
	.fixed-table-toolbar .search {width: Calc( 100% );}
	}
	
	.SumoSelect {width: 100% !important; color: #000 !important;}    
	
	.SelectBox {padding: 7px 12px !important;}
	
	.options{max-height: 150px !important;}
	
	.SumoSelect .select-all {height: 35px !important;}
</style>

<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
        <li class="active">
			Client CRM Account List
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Client CRM Account List
					</h3>
				</div>
				<div class="panel-body">
					<table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20, 40, ALL]" data-show-footer="false">
						<thead>
                            <tr>
                                <th data-field="#" data-sortable="true" @if(in_array("0",$CRMAccountColumns)) data-visible="false" @endif>#</th>
                                <th data-field="Date" data-sortable="true" @if(in_array("1",$CRMAccountColumns)) data-visible="false" @endif>Date</th>
                                <th data-field="Clients" data-sortable="true" @if(in_array("2",$CRMAccountColumns)) data-visible="false" @endif>Clients</th>
                                <th data-field="CRM" data-sortable="true" @if(in_array("3",$CRMAccountColumns)) data-visible="false" @endif>CRM</th>
                                <th data-field="Leads" data-sortable="true" @if(in_array("4",$CRMAccountColumns)) data-visible="false" @endif>Leads</th>
                                <th data-field="Last Lead" data-sortable="true" @if(in_array("5",$CRMAccountColumns)) data-visible="false" @endif>Last Lead</th>
                                @if($WritePermission=='Yes')<th data-field="Action" data-sortable="true" @if(in_array("6",$CRMAccountColumns)) data-visible="false" @endif>Action</th>@endif
							</tr>
						</thead>
						<tbody>
                            <?php $i=1; ?>
                            @foreach($CRMAccounts as $data)
							<tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ date('m-d-Y',strtotime($data->created_at)) }}</td>
                                <td>{{ $data->name }}</td>
								<td>{{ $data->account }}</td>
                                <td>
									@if(ClientHelper::CRMTotalContact($data->id)>0)
									<a href="../crm_contact/{{ $data->id }}">{{ ClientHelper::CRMTotalContact($data->id) }}</a>
									@endif
								</td>
                                <td>{{ ClientHelper::ContactCreateLastLead($data->id) }}</td>
								@if($WritePermission=='Yes')
                                <td>
									@if($data->is_deleted==0)
									<a href="#" class="inactive_client" title="Pause" onClick="Pause('{{ $data->id }}','1')">
										<i class="fa fa-pause-circle-o fa-lg" aria-hidden="true"></i>
									</a>
									@else
									<a href="#" class="active_client" title="Activate" onClick="Activate('{{ $data->id }}','0')">
										<i class="fa fa-play-circle-o fa-lg" aria-hidden="true"></i>
									</a>
									@endif
								</td>
								@endif
							</tr> 
							@endforeach                                                                                    
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<script>
	$(document).ready(function(){
		$('.dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Client CRM Account Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
	});
</script>
<script>
	function Pause(id,is_deleted)
	{
		swal({
			title: 'Are you sure?',
			text: "You want to pause this CRM account?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#22D69D',
			cancelButtonColor: '#FB8678',
			confirmButtonText: 'Yes, pause it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn',
			cancelButtonClass: 'btn'
			}).then(function () {
			$.get("{{ route('crm_account_status') }}",{'id':id, 'is_deleted':is_deleted}).then(function(response){
				swal('Paused!', 'CRM account paused successfully.', 'success');
				window.location.reload(true);
			});
			}, function (dismiss) {
			// dismiss can be 'cancel', 'overlay',
			// 'close', and 'timer'
			if (dismiss === 'cancel') {
				swal('Cancelled', 'CRM account pause cancelled successfully.', 'error');
			}
		});
	}
	
	function Activate(id,is_deleted)
	{
		swal({
			title: 'Are you sure?',
			text: "You want to active this CRM account?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#22D69D',
			cancelButtonColor: '#FB8678',
			confirmButtonText: 'Yes, active it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn',
			cancelButtonClass: 'btn'
			}).then(function () {
			$.get("{{ route('crm_account_status') }}",{'id':id, 'is_deleted':is_deleted}).then(function(response){
				swal('Activated!', 'CRM account activated successfully.', 'success');
				window.location.reload(true);
			});
			}, function (dismiss) {
			// dismiss can be 'cancel', 'overlay',
			// 'close', and 'timer'
			if (dismiss === 'cancel') {
				swal('Cancelled', 'CRM account activation cancelled successfully.', 'error');
			}
		});
	}
</script>
@stop								