@extends('layouts/default')

{{-- Page title --}}
@section('title')
CRM Account Contact Lead List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	
	.export.btn-group {*display:none!important;}
	.fixed-table-toolbar {*width:60%;}
	.fixed-table-toolbar .search {width: Calc( 100% - 110px );}
	
	@media screen and (max-width: 767px) {
	.fixed-table-toolbar {width: 100%;}
	.fixed-table-toolbar {width:100%;}
	.fixed-table-toolbar .search {width: Calc( 100% );}
	}
</style>

<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">
			CRM Account Contact Lead List
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> {{ $CRMccount->account }} Contact Lead List
					</h3>
				</div>
				<div class="panel-body">
					<table id="CRMAccountContactLeadList" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-page-list="[10, 20, 40, ALL]" data-show-footer="false">
						<thead>
                            <tr>
								<th data-field="#" @if(in_array("0",$CampaignColumns)) data-visible="false" @endif>#</th>
								<th data-field="Created Time" data-sortable="true" @if(in_array("1",$CampaignColumns)) data-visible="false" @endif>Created Time</th>
								<th data-field="Campaign" data-sortable="true" @if(in_array("2",$CampaignColumns)) data-visible="false" @endif>Campaign</th>
								<th data-field="Ad Set Name" data-sortable="true" @if(in_array("3",$CampaignColumns)) data-visible="false" @endif>Ad Set Name</th>
								<th data-field="Ad Name" data-sortable="true" @if(in_array("4",$CampaignColumns)) data-visible="false" @endif>Ad Name</th>
								<th data-field="Full Name" data-sortable="true" @if(in_array("5",$CampaignColumns)) data-visible="false" @endif>Full Name</th>
								<th data-field="Email" data-sortable="true" @if(in_array("6",$CampaignColumns)) data-visible="false" @endif>Email</th>
								<th data-field="Phone #" data-sortable="true" @if(in_array("7",$CampaignColumns)) data-visible="false" @endif>Phone #</th>
								<th data-field="City" data-sortable="true" @if(in_array("8",$CampaignColumns)) data-visible="false" @endif>City</th>
							</tr>
						</thead>
						<tbody>
							@php $i=1; @endphp
							@foreach($CampaignLeads as $CampaignLead)
							<tr>
								<td>{{ $i++ }}</td>
								<td>{{ date('m/d/Y',strtotime($CampaignLead->contact_created_at)) }}</td>
								<td>{{ $CampaignLead->campaign_name }}</td>
								<td>{{ $CampaignLead->adset_name }}</td>
								<td>{{ $CampaignLead->ad_name }}</td>
								<td>{{ $CampaignLead->full_name }}</td>
								<td>{{ $CampaignLead->email }}</td>
								<td>{{ $CampaignLead->phone_number }}</td>
								<td>{{ $CampaignLead->city }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<script>
	$(document).ready(function(){
		$('#CRMAccountContactLeadList').bootstrapTable();
		$('.dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='CRM Account Lead List Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
	});
</script>
@stop