@extends('layouts/default')

{{-- Page title --}} 
@section('title') 
Unanswered Queries @parent 
@stop 

{{-- page level styles --}} 
@section('header_styles')
<!--start page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/iCheck/css/all.css')}}" />

<!--end page level css-->
@stop 
{{-- Page content --}}
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
        <li>
			<a href="{{ route('manage-bot') }}">
				Manage Bot
			</a>
		</li>
		<li class="active">
			<a href="{{ route('unanswered-queries') }}">
				Unanswered Queries
			</a>
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content p-l-r-15">
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" /> 
        <div class="row">
			<div class="col-lg-12">
				<div class="panel panel-success filterable">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-fw fa-th-large">
							</i>
							All Unanswered Queries
						</h3>
					</div>
					<div class="panel-body">
                       
						<table id="questions" data-toggle="table"  data-search="true" data-show-columns="true" data-pagination="true" data-page-list="[10, 20,40,ALL]">
                            <thead>
								<tr>
                                    <th><a href="#" class="add-query-answer"><i class="fa fa-plus fa-fw" title="Click to add answer for selected queries"></i></th>
									<th>Query</th>
									<th>Page</th>
                                    <th>Sender</th>
									<th>Date</th> 
									<th class="text-center">Action</th>
								</tr>
							</thead>
                            <tbody>
                                <?php $i=1; ?>
								@foreach($queries as $query)                              
								<tr>
                                    								
									<td title="Click + icon above to add answer for it"><input  type="checkbox" value="{{$query->query_id}}" class="queries icheck" /></td>
                                    <td><?php echo nl2br($query->query); ?></td>
                                    <td>{{$query->page_name}}</td> 
                                    <td>{{$query->first_name}} {{$query->last_name}}</td>                                
									<td>{{date('m/d/Y H:i:s',strtotime($query->query_created))}}</td>
									<td>
										<a href="#" data-id="{{$query->query_id}}" class="delete-query"><i class="fa fa-trash" title="Delete"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>	
</section>


<!-- /.content -->
@stop 

{{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/vendors/iCheck/js/icheck.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/custom_manage_bot.js')}}"></script>
@stop