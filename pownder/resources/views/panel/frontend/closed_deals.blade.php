@extends('layouts/default')

{{-- Page title --}} 
@section('title') 
Closed Deals @parent 
@stop 

{{-- page level styles --}} 
@section('header_styles')
<!--start page level css -->
<style>
	.fixed-table-toolbar .search {width: Calc( 100% - 108px );}
	#table_new_container .fixed-table-toolbar .search {width: Calc( 100% - 258px );}
	#table_used_container .fixed-table-toolbar .search {width: Calc( 100% - 258px );}
</style>
<!--end page level css-->
@stop 
{{-- Page content --}}
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		
		<li class="active">
			Closed Deals
		</li>
	</ol>
</section>
<?php 
	//Money Format
	function money_format1($number)
	{
		setlocale(LC_MONETARY, 'en_US'); 
		$f_number=money_format('%!.2i',abs($number)); 
		$f_number=($number<0) ? '-$'.$f_number : '$'.$f_number;
		return $f_number;
	}
?>
<!-- Main content -->
<section class="content p-l-r-15">
    <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}"  />
    @include('panel.includes.status')   
    
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Closed Deals
					</h3>
				</div>
				<div class="panel-body">
					<table class="closed_deals_tables" id="table_totals" data-show-columns="true" data-id-field="department" data-query-params="queryParams" data-url="{{route('totals-closed-deals')}}">
						<thead>
                            <tr>
                                <th data-field="department">Departments</th>
                                <th data-field="total_sold">Total Sold</th>
                                <th data-field="total_new">New</th>
                                <th data-field="total_used">Used</th>
                                <th data-field="front_gross">Front-End Gross</th>
                                <th data-field="hold_back">Holdback</th>
                                <th data-field="back_gross">Back-end Gross</th>
                                <th data-field="total_profit">Total Profit</th>
                                <th data-field="hold_check">Hold Check</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>	
	</div>
	
    <div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Totals by City
					</h3>
				</div>
				<div class="panel-body">
					
					<!--
						<div class="row  clearfix text-center">
						<div class="col-md-6 pull-right">
						<div class="table-responsive">                                   
						<table class="closed_deals_tables" id="table_by_city_total"
						data-query-params="queryParams"
						data-url="{{route('get-closed-deals',['type'=>'group_by_city_total'])}}" >
						<thead>
						<tr>      
						<th data-field="city" data-visible="false" >Total Cities</th>                                  
						<th data-field="total_sold" >Total Sold</th>
						<th data-field="avg_gross" >Total Avg Gross</th>
						<th data-field="total_profit"  >Total Profit</th>
						<th data-field="percentage"  >Total Percentage</th>
						</tr>
						</thead>                           
						</table>
						</div> 
						</div>
						</div>
					-->
					
					<table class="closed_deals_tables" id="table_by_city" data-search="true" data-show-toggle="false" data-show-columns="true" data-show-export="true" data-detail-view="false" data-detail-formatter="detailFormatter" data-minimum-count-columns="1" data-show-pagination-switch="false" data-pagination="true" data-id-field="city" data-page-list="[10, 20,40,ALL]" data-show-footer="false" data-side-pagination="server"  data-query-params="queryParams" data-sort-name="total_sold" data-sort-order="desc" data-url="{{route('get-closed-deals',['type'=>'group_by_city'])}}" >
						<thead>
                            <tr>
                                <th data-field="city" data-sortable="true">City</th>
                                <th data-field="total_sold" data-sortable="true">Total Sold</th>
                                <th data-field="avg_gross" data-sortable="true">Avg Gross</th>
                                <th data-field="total_profit" data-sortable="true" >Total Profit</th>
                                <th data-field="percentage"  data-sortable="true" >Percentage</th>
							</tr>
						</thead>                           
					</table>
				</div>
			</div>
		</div>	
	</div>
	
    <div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> New Cars
					</h3>
				</div>
				<div class="panel-body" id="table_new_container">
					<table class="closed_deals_tables" id="table_new" data-search="true" data-show-toggle="false" data-show-columns="true" data-show-export="true" data-detail-view="false" data-detail-formatter="detailFormatter" data-minimum-count-columns="1" data-show-pagination-switch="false"
					data-pagination="true" data-id-field="contract_date" data-page-list="[10, 20,40,ALL]" data-show-footer="false" data-side-pagination="server"  data-query-params="queryParams1" data-sort-name="contract_date" data-sort-order="desc" data-url="{{route('get-closed-deals',['type'=>'NEW'])}}" data-showLoading="true">
						<thead>
                            <tr>
                                <th data-field="contract_date" data-sortable="true">Date</th>
                                <th data-field="deal_number" data-sortable="true">Deal #</th>
                                <th data-field="full_name" data-sortable="true">Name</th>
                                <th data-field="city" data-sortable="true">City</th>
                                <th data-field="front_gross" data-sortable="true">F.End Gross</th>
                                <th data-field="hold_back" data-sortable="true">Holdback</th>
                                <th data-field="back_gross" data-sortable="true"  >B.End Gross</th>
                                <th data-field="total_profit" data-sortable="true" >Total Profit</th>
                                <th data-field="hold_check" data-sortable="true" >Hold Check</th>
                                <th data-field="salesman1" data-sortable="true" >Salesperson</th>
                                <th data-field="source" data-sortable="true" >Source</th>
                                <th data-field="department" data-sortable="true" >Dept.</th>
                                <th data-field="action" >Action</th>
							</tr>
						</thead>                           
					</table>
				</div>
			</div>
		</div>	
	</div>
	
    <div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Used Cars
					</h3>
				</div>
				<div class="panel-body" id="table_used_container">
					<table class="closed_deals_tables" id="table_used" data-search="true" data-show-toggle="false" data-show-columns="true" data-show-export="true" data-detail-view="false" data-detail-formatter="detailFormatter" data-minimum-count-columns="1" data-show-pagination-switch="false"
					data-pagination="true" data-id-field="contract_date" data-page-list="[10, 20,40,ALL]" data-show-footer="false" data-side-pagination="server" data-query-params="queryParams1" data-sort-name="contract_date" data-sort-order="desc" data-url="{{route('get-closed-deals',['type'=>'USED'])}}">
						<thead>
                            <tr>
                                <th data-field="contract_date" data-sortable="true">Date</th>
                                <th data-field="deal_number" data-sortable="true">Deal #</th>
                                <th data-field="full_name" data-sortable="true">Name</th>
                                <th data-field="city" data-sortable="true">City</th>
                                <th data-field="front_gross" data-sortable="true">F.End Gross</th>
                                <th data-field="hold_back" data-sortable="true">Holdback</th>
                                <th data-field="back_gross" data-sortable="true">B.End Gross</th>
                                <th data-field="total_profit" data-sortable="true">Total Profit</th>
                                <th data-field="hold_check" data-sortable="true">Hold Check</th>
                                <th data-field="salesman1" data-sortable="true">Salesperson</th>
                                <th data-field="source" data-sortable="true">Source</th>
                                <th data-field="department" data-sortable="true">Dept.</th>
                                <th data-field="action">Action</th>
							</tr>
						</thead>                           
					</table>
				</div>
			</div>
		</div>	
	</div>
</section>
<!-- /.content -->

@stop 

{{-- page level scripts --}} 
@section('footer_scripts')
<!-- begining of page level js -->
<!--Custom JS -->

<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}" ></script>
<!-- bootstrap time picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" ></script>

<script  type="text/javascript" src="{{asset('assets/vendors/jquerydaterangepicker/js/jquery.daterangepicker.min.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/timedropper/js/timedropper.js')}}" ></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/closed_deals.js')}}"></script>

<script>
	
    function queryParams(params)
    {
        var dont_apply_range=<?php echo $filter=(Request::has('client_id') && Request::has('deal_number') && Request::has('stock_number')) ? 1 :0 ; ?>;
        
        params.range=(dont_apply_range) ?'' :$('#date-range-header').val();
		
        if($(".client-list").length>0 && $(".client-list ul li.active").length>0)
        {
			params.client_id=$(".client-list ul li.active").attr("user_client_id");
		}
        else
        {
			params.client_id='';
		}
        params.deal_number='<?php echo $filter=(Request::has('client_id') && Request::has('deal_number') && Request::has('stock_number')) ? Request::input('deal_number') : '';  ?>';
        params.stock_number='<?php echo $filter=(Request::has('client_id') && Request::has('deal_number') && Request::has('stock_number')) ? Request::input('stock_number') : '';  ?>';
        
        return params;
	}
	
    function queryParams1(params)
    {
		var dont_apply_range=<?php echo $filter=(Request::has('client_id') && Request::has('deal_number') && Request::has('stock_number')) ? 1 :0 ; ?>;
        
        params.range=(dont_apply_range) ?'' :$('#date-range-header').val();
        
        if($(".client-list").length>0 && $(".client-list ul li.active").length>0)
        {
			params.client_id=$(".client-list ul li.active").attr("user_client_id");
		}
        else
        {
			params.client_id='';
		}
        params.deal_number='<?php echo $filter=(Request::has('client_id') && Request::has('deal_number') && Request::has('stock_number')) ? Request::input('deal_number') : '';  ?>';
        params.stock_number='<?php echo $filter=(Request::has('client_id') && Request::has('deal_number') && Request::has('stock_number')) ? Request::input('stock_number') : '';  ?>';
        
        params.filter_new=$('#filter_new').val();
        params.filter_used=$('#filter_used').val();
        return params;
	}        
</script>
@stop