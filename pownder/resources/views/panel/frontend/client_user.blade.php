@extends('layouts/default')

{{-- Page title --}}
@section('title')
User List
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	.fixed-table-container {height: 500px !important; }
	.export.btn-group {*display:none!important;}
	.fixed-table-toolbar {*width:60%;}
	.fixed-table-toolbar .search {width: Calc( 100% - 342px );}
	
	@media screen and (max-width: 767px) {
	.fixed-table-toolbar {width: 100%;}
	.fixed-table-toolbar {width:100%;}
	.fixed-table-toolbar .search {width: Calc( 100% );}
	}
	
	#filter_status {
    display: block;
    width: 100px;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.428571429;
    color: #555555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
	}
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		
		<li class="active">
			User List
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content p-l-r-15">
	@include('panel.includes.status')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> User List
					</h3>
				</div>
				<div class="panel-body">
					<table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-query-params="queryParams" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20, 40, ALL]" data-show-footer="false" data-toggle="table" data-side-pagination="server" data-url="{{ route('client-user-ajax') }}">
						<thead>
							<tr>
								<th data-field="" @if(in_array("0",$TableColumn)) data-visible="false" @endif><input type="checkbox" class="checkall" /></th>
								<th data-field="User Group" data-sortable="true" @if(in_array("1",$TableColumn)) data-visible="false" @endif>User Group</th>
								<th data-field="Job Title" data-sortable="true" @if(in_array("2",$TableColumn)) data-visible="false" @endif>Job Title</th>
								<th data-field="User Name" data-sortable="true" @if(in_array("3",$TableColumn)) data-visible="false" @endif>User Name</th>
								<th data-field="Cell #" data-sortable="true" @if(in_array("4",$TableColumn)) data-visible="false" @endif>Cell #</th>
								<th data-field="Team" data-sortable="true" @if(in_array("5",$TableColumn)) data-visible="false" @endif>Team</th>
								<th data-field="Employee ID" data-sortable="true" @if(in_array("6",$TableColumn)) data-visible="false" @endif>Employee ID</th>
								<th data-field="Status" data-sortable="true" @if(in_array("7",$TableColumn)) data-visible="false" @endif>Status</th>
								<th data-field="Actions" data-sortable="true" @if(in_array("8",$TableColumn)) data-visible="false" @endif>Actions</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<script>
	$(document).ready(function(){
		var extra_features = '<select name="filter_status" id="filter_status" style="display: inline;float: left; margin-right: 5px;"><option value="1">Active</option><option value="0">Inactive</option></select><a href="cuser_add" class="btn btn-default" title="Add User" ><i class="fa fa-fw fa-plus-square"></i></a><button class="btn btn-default ActiveBulkUser" type="button" title="Active Users" ><i class="fa fa-fw fa-check-circle-o"></i></button><button class="btn btn-default InactiveBulkUser" type="button" title="Inactive Users" ><i class="fa fa-fw fa-ban"></i></button>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
		
		$('.dropdown-menu input[type=checkbox]').change(function(){
			var column_value=$(this).val();
			var table_name='Client User List Table';
			$.get("{{ route('table_visible_column') }}",{column_value: column_value, table_name: table_name},function(data){ 
				//alert(data); 
			});
		});
		
		$("#filter_status").change(function(){
			$('#table4').bootstrapTable('refresh', {
				query: {
					status: $(this).val(),
					dateRange: $('#date-range-header').val(),
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
		
		$('.button-select').click(function () {
			$('#table4').bootstrapTable('refresh', {
				query: {
					status: $('#filter_status').val(),
					dateRange: $('#date-range-header').val(),
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
		
		$('.button-clear').click(function () {
			$('#table4').bootstrapTable('refresh', {
				query: {
					status: $('#filter_status').val(),
					dateRange: '',
					search: $('.fixed-table-toolbar .search input[type=text]').val()
				}
			});
		});
	});
	
	function queryParams(params) {
		params.status = $('#filter_status').val(); // add param1
		params.search = $('.fixed-table-toolbar .search input[type=text]').val();
		params.dateRange = $('#date-range-header').val();
		// console.log(JSON.stringify(params));
		// {"limit":10,"offset":0,"order":"asc","your_param1":1,"your_param2":2}
		return params;
	}
	
	function UserDelete(id)
	{
		swal({
			title: 'Are you sure?',
			text: "You want to delete this user!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#22D69D',
			cancelButtonColor: '#FB8678',
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn',
			cancelButtonClass: 'btn'
			}).then(function () {
			$.get('delete-user',{'id':id}).then(function(){
				swal('Deleted!', 'Your user has been deleted.', 'success');
				window.location.reload(true); 
			});
			}, function (dismiss) {
			// dismiss can be 'cancel', 'overlay',
			// 'close', and 'timer'
			if (dismiss === 'cancel') {
				swal('Cancelled', 'Your user is safe.', 'error');
			}
		});
	}
	
	function UserActive(id)
	{
		swal({
			title: 'Are you sure?',
			text: "You want to active this user!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#22D69D',
			cancelButtonColor: '#FB8678',
			confirmButtonText: 'Yes, active it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn',
			cancelButtonClass: 'btn'
			}).then(function () {
			$.get('active-user',{'id':id}).then(function(){
				
				swal('Active!', 'Your user has been active.', 'success');
				window.location.reload(true); 
			});
			}, function (dismiss) {
			// dismiss can be 'cancel', 'overlay',
			// 'close', and 'timer'
			if (dismiss === 'cancel') {
				swal('Cancelled', 'Your user is inactive.', 'error');
			}
		});
	}
	
	function UserInactive(id)
	{
		swal({
			title: 'Are you sure?',
			text: "You want to inactive this user!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#22D69D',
			cancelButtonColor: '#FB8678',
			confirmButtonText: 'Yes, inactive it!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn',
			cancelButtonClass: 'btn'
			}).then(function () {
			$.get('inactive-user',{'id':id}).then(function(){
				
				swal('Inactive!', 'Your user has been inactive.', 'success');
				window.location.reload(true); 
			});
			}, function (dismiss) {
			// dismiss can be 'cancel', 'overlay',
			// 'close', and 'timer'
			if (dismiss === 'cancel') {
				swal('Cancelled', 'Your user is active.',	'error');
			}
		});
	}
</script>
<script>
	$(document).ready(function()
	{
		var _token = "{{ csrf_token() }}";
		
		$('.checkall').click(function() {
			var checked = $(this).prop('checked');
			$('#table4').find('.user_id').prop('checked', checked);
		});
		
		$(".ActiveBulkUser").click(function() {
			var checkedRows = []
			$("input[class='user_id']:checked").each(function ()
			{
				checkedRows.push($(this).val());
			});
			
			if(checkedRows.length==0)
			{
				swal('Error', 'Please select user.', 'error');
			}
			else
			{
				swal({
					title: 'Are you sure?',
					text: "You want to active selected users?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, active it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.post("{{ route('active-bulk-user') }}",{'id':checkedRows, _token:_token}).then(function(response){
						swal('Active!', 'Users active successfully.', 'success');
						window.location.reload(true);
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Users active cancelled successfully.', 'error');
					}
				});
			}
		});
		
		$(".InactiveBulkUser").click(function() {
			var checkedRows = []
			$("input[class='user_id']:checked").each(function ()
			{
				checkedRows.push($(this).val());
			});
			
			if(checkedRows.length==0)
			{
				swal('Error', 'Please select user.', 'error');
			}
			else
			{
				swal({
					title: 'Are you sure?',
					text: "You want to inactive selected users?",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#22D69D',
					cancelButtonColor: '#FB8678',
					confirmButtonText: 'Yes, inactive it!',
					cancelButtonText: 'No, cancel!',
					confirmButtonClass: 'btn',
					cancelButtonClass: 'btn'
					}).then(function () {
					$.post("{{ route('inactive-bulk-user') }}",{'id':checkedRows, _token:_token}).then(function(response){
						swal('Inactive!', 'Users inactive successfully.', 'success');
						window.location.reload(true);
					});
					}, function (dismiss) {
					// dismiss can be 'cancel', 'overlay',
					// 'close', and 'timer'
					if (dismiss === 'cancel') {
						swal('Cancelled', 'Users inactive cancelled successfully.', 'error');
					}
				});
			}
		});
	});
</script>
@stop