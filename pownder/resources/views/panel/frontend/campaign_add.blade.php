@extends('layouts/default')

{{-- Page title --}}
@section('title')
Add Campaign
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--start page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/js/sumoselect/sumoselect.css')}}">
<link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/toolbar/css/jquery.toolbar.css')}}">
<style>
	optgroup {background-color: #fb9f98; color: #000;}
	.group {background-color: #fb9f98; color: #000;}
	.SumoSelect {width: 100% !important; color: #000 !important;}    
	.help-block {color: #fa5a46  !important;}
	.SelectBox {padding: 7px 12px !important;}
	
	.options{max-height: 150px !important;}
	
	.SumoSelect .select-all {height: 35px !important;}
	#UserModal {background: rgba(0, 0, 0, 0.701961); box-shadow: 0 3px 6px -2px rgba(0,0,0,0.4); *bottom: auto;}
	#UserModal .modal-dialog {top: 50%; margin: 0 0 0 50%; transform: translate(-50%, -50%);}
</style>
<!--end page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li><a href="{{ route('campaigns') }}"> Campaigns</a></li>
		<li class="active">	Add Campaign</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Add Campaign
					</h3>
				</div>
				<div class="panel-body">
					<form role="form" id="add_campaign_form">
						{{ csrf_field() }}
						<div class="form-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="facebook_page">Facebook Pages</label>
										<select multiple="multiple" placeholder="Select Facebook Pages" name="facebook_page[]" id="facebook_page" class="select1 SumoUnder">
											@foreach($FacebookPages as $FacebookPage)
											<option value="{{ $FacebookPage->id }}">{{ $FacebookPage->name }}</option>
											@endforeach
											@foreach($Vendors as $Vendor)
											<optgroup label="{{ $Vendor->name }} Facebook Pages" class="otp-group"></optgroup>
											@foreach(VendorHelper::VendorFacebookPages($Vendor->id) as $FacebookPage)
											<option value="{{ $FacebookPage->id }}">{{ $FacebookPage->name }}</option>
											@endforeach
											@endforeach
										</select>
										<small class="text-danger animated facebook_page fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="facebook_ad_form">Facebook Ad Form</label>
										<div id="formHide">
											<select  multiple="multiple" placeholder="Select Facebook Ad Form" name="facebook_ad_form[]" id="facebook_ad_form" class="select1 SumoUnder">
											</select>
										</div>
										<small class="text-danger animated facebook_ad_form fadeInUp campaign_add_error"></small>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="custom_campaign_name">Custom Campaign Name</label>
										<input type="text" class="form-control"
										id="custom_campaign_name" name="custom_campaign_name" placeholder="Custom Campaign Name">
										<small class="text-danger animated custom_campaign_name fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="crm_account">CRM Account</label>
										<select id="crm_account" name="crm_account" class="form-control">
											<option value="">Select CRM Account</option>
											<option value="Pownder™">Pownder™</option>
											<optgroup label="Automotive"></optgroup>
											<option value="Advent">Advent</option>
											<option value="CDK">CDK</option>
											<option value="Dealerpeak">Dealerpeak</option>
											<option value="DealerSocket">DealerSocket</option>
											<option value="DealerTrack">DealerTrack</option>
											<option value="Dominion Dealer Solutions">Dominion Dealer Solutions</option>
											<option value="eflow automotive">eflow automotive</option>
											<option value="eLead CRM">eLead CRM</option>
											<option value="FordDirect">FordDirect</option>
											<option value="HigherGear">HigherGear</option>
											<option value="Momentum">Momentum</option>
											<option value="ProMax Unlimited">ProMax Unlimited</option>
											<option value="Reynolds & Reynolds">Reynolds & Reynolds</option>
											<option value="VinSolutions">VinSolutions</option>
											<option value="Votenza">Votenza</option>
											<optgroup label="Business"></optgroup>
											<option value="Agile">Agile</option>
											<option value="Zoho">Zoho</option>
										</select>
										<small class="text-danger animated crm_account fadeInUp campaign_add_error"></small>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="manual_lead_type">Manual Lead Type</label>
										<select  multiple="multiple" placeholder="Select Manual Lead Type" name="manual_lead_type[]" id="manual_lead_type" class="select1 SumoUnder">
											<option value="Pownder™ Call">Pownder™ Call</option>
											<option value="Pownder™ Messenger">Pownder™ Messenger</option>
										</select>
										<small class="text-danger animated manual_lead_type fadeInUp campaign_add_error"></small>
									</div>
								</div>
								
								<div class="col-md-4">
									<div class="form-group">
										<label for="page_engagement"> Page Engagements</label><br>
										<label class="radio-inline"><input type="radio" name="page_engagement" value="No" checked> No</label>
										<label class="radio-inline"><input type="radio" name="page_engagement" value="Yes"> Yes</label><br>
										<small class="text-danger animated page_engagement fadeInUp campaign_add_error"></small>
									</div>
								</div>
								
								<div class="col-md-4">
									<div class="form-group">
										<label for="lead_sync">
											Lead Sync 
											<div class="po-markup" style="display: inline;">
												<i class="fa fa-lg fa-question-circle po-link" aria-hidden="true"></i>
												<div class="po-content hidden">
													<div class="po-body">
														<p>
															Do you want to Sync All leads into your CRM with new leads or just the New leads? Please select one option.
														</p>
													</div>
													<!-- ./po-body -->
												</div>
												<!-- ./po-content -->
											</div>
											<!-- ./po-markup-->
										</label><br>
										<label class="radio-inline"><input type="radio" name="lead_sync" value="Existing & New"> Existing & New</label>
										<label class="radio-inline"><input type="radio" name="lead_sync" value="Only New" checked> Only New</label><br>
										<small class="text-danger animated lead_sync fadeInUp campaign_add_error"></small>
									</div>
								</div>
							</div>
							<div class="row">
								<h4 style="background-color:#c6efce;padding: 10px; margin: 10px 15px;">Client Info</h4>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="client_id">Client ID</label>
										<input type="text" name="client_id" id="client_id" class="form-control" placeholder="Client ID" readonly="readonly" />
										<small class="text-danger animated client_id fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="client_name">Client Name</label>
										<select id="client_name" name="client_name" class="form-control">
											<option value="">Select Client Name</option>
											<optgroup label="Own Client"></optgroup>
											@foreach($Clients as $Client)
											<option value="{{ $Client->id }}">{{ $Client->name }}</option>
											@endforeach
											@foreach($Vendors as $Vendor)
											<optgroup label="{{ $Vendor->name }} Client"></optgroup>
											@foreach(VendorHelper::CampaignVendorClients($Vendor->id) as $Client)
											<option value="{{ $Client->id }}">{{ $Client->name }}</option>
											@endforeach
											@endforeach
										</select>
										<small class="text-danger animated client_name fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="client_contact">Client Contact</label>
										<input type="text" name="client_contact" id="client_contact" class="form-control" placeholder="Client Contact" />
										<small class="text-danger animated client_contact fadeInUp campaign_add_error"></small>
									</div>
								</div>
							</div>
							
							<div class="row">
								<h4 style="background-color:#c6efce;padding: 10px; margin: 10px 15px;">Email Set Up</h4>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="from">From</label>
										<input type="text" name="from" id="from" value="leads@pownder.com" class="form-control" placeholder="From" />
										<small class="text-danger animated from fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="crm_email">CRM Email? <small>(emails separated by comma)</small></label>
										<input type="text" name="crm_email" id="crm_email" class="form-control" placeholder="CRM Email" />
										<small class="text-danger animated crm_email fadeInUp campaign_add_error"></small>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="cc">Cc? <small>(emails separated by comma)</small></label>
										<input type="text" name="cc" id="cc" class="form-control" placeholder="Cc" />
										<small class="text-danger animated cc fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="bcc">Bcc? <small>(emails separated by comma)</small></label>
										<input type="text" name="bcc" id="bcc" class="form-control" placeholder="Bcc" />
										<small class="text-danger animated bcc fadeInUp campaign_add_error"></small>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="subject">Subject</label>
										<input type="text" name="subject" id="subject" value="New Pownder™ Lead" class="form-control" placeholder="Subject" />
										<small class="text-danger animated subject fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="manager_id">Notify Users</label>
										<div id="userHide">
											<select multiple="multiple" placeholder="Select Users" name="manager_id[]" id="manager_id" class="select1 SumoUnder">
											</select>
										</div>
										<small class="text-danger animated manager_id fadeInUp campaign_add_error"></small>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="user_notification"> User Notification <small>(Round Robin)</small></label><br>
										<label class="radio-inline"><input type="radio" name="user_notification" value="No" checked> No</label>
										<label class="radio-inline"><input type="radio" name="user_notification" value="Yes"> Yes</label><br>
										<small class="text-danger animated user_notification fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="notification"> Notification</label>
										<select class="form-control" id="notification" name="notification" disabled>
											<option value="" selected>None</option>
											<option value="For New Leads">For New Leads</option>
											<option value="For Appointments">For Appointments</option>
											<option value="Both">Both</option>
										</select>
										<small class="text-danger animated notification fadeInUp campaign_add_error"></small>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="lead_notification"> Client Notification</label><br>
										<label class="radio-inline"><input type="radio" name="lead_notification" value="Both" checked> Both</label>
										<label class="radio-inline"><input type="radio" name="lead_notification" value="Only Appointments"> Only Appointments</label>
										<label class="radio-inline"><input type="radio" name="lead_notification" value="Only New Leads"> Only New Leads</label><br>
										<small class="text-danger animated lead_notification fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="email_lead_notification">Lead Notification? <small>(emails separated by comma)</small></label>
										<input type="text" name="email_lead_notification" id="email_lead_notification" class="form-control" placeholder="Lead Notification" />
										<small class="text-danger animated email_lead_notification fadeInUp campaign_add_error"></small>
									</div>
								</div>
							</div>
							
							<div class="row">
								<h4 style="background-color:#c6efce;padding: 10px; margin: 10px 15px;">Service Provider Information</h4>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="provider_name">Provider Name</label>
										<input type="text" name="provider_name" id="provider_name" class="form-control" value="Pownder™" placeholder="Provider Name" />
										<small class="text-danger animated provider_name fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="provider_id">Provider ID</label>
										<input type="text" name="provider_id" id="provider_id" class="form-control" placeholder="Provider ID" @if(Session::get('user_category')=='admin') value="1" @else value="{{ Session::get('vendor_id')+1 }}" @endif />
										<small class="text-danger animated provider_id fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="name_of_service">Name of Service</label>
										<input type="text" name="name_of_service" id="name_of_service" @if(Session::get('user_category')=='admin') value="Facebook" @else value="{{ $ServiceProvider->name }}" @endif class="form-control" placeholder="Name of Service" />
										<small class="text-danger animated name_of_service fadeInUp campaign_add_error"></small>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="email">Email</label>
										<input type="text" name="email" id="email" class="form-control" @if(Session::get('user_category')=='admin') value="info@pownder.com" @else value="{{ $ServiceProvider->email }}" @endif placeholder="Email" />
										<small class="text-danger animated email fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="phone">Phone</label>
										<input type="text" name="phone" id="phone" class="form-control" value="{{ $ServiceProvider->mobile }}" placeholder="Phone" />
										<small class="text-danger animated phone fadeInUp campaign_add_error"></small>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="url">URL</label>
										<input type="text" name="url" id="url" class="form-control" placeholder="URL" value="http://www.pownder.com" />
										<small class="text-danger animated url fadeInUp campaign_add_error"></small>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label" for="prospect_id_source">Prospect ID Source</label>
										<input type="text" name="prospect_id_source" id="prospect_id_source" class="form-control" placeholder="Prospect ID Source" value="{{ Session::get('user_id') }}" />
										<small class="text-danger animated prospect_id_source fadeInUp campaign_add_error"></small>
									</div>
								</div>
							</div>
							<div class="form-group form-actions">
								<center>
									<button type="button" class="btn btn-primary addButton" style="color:#fff; background-color: #ffbe18 !important; border-color: #e4a70c !important;">Save</button>
									<button type="button" class="btn btn-primary SaveTestButton" style="color:#fff; background-color: #ffbe18 !important; border-color: #e4a70c !important;" disabled>Save & Test</button>
									<button type="reset" class="btn btn-default clear-form">Reset</button>
								</center>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>	

<!-- UserModal -->
<div id="UserModal" class="modal fade animated" role="dialog" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" style="background: linear-gradient(154deg, #00c7be 0, #007091 50%, #ffc902 100%)!important;">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h4 class="modal-title UserModalTitle"> Re-assign</h4>
			</div>
			<form class="form-horizontal" role="form" id="user-email-update">
				<div class="modal-body">
					<input type="hidden" name="UserId" id="UserId">
					<div class="row m-t-10">
						<div class="col-md-12">
							<label for="user_email"> Email</label>
							<div class="input-group" style="width: 100%;">
								<input type="text" name="user_email" id="user_email" class="form-control">
								<small class="text-danger animated user_email fadeInUp"></small>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-default modalButton" style=" color:#fff; background-color: #ffbe18!important; border-color: #e4a70c !important;"> Save</button>
					<button type="button" class="btn btn-default modalButton" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- /#UserModal -->
@stop								

{{-- page level scripts --}}
@section('footer_scripts')
<!-- InputMask -->
<script src="{{asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/sumoselect/jquery.sumoselect.min.js')}}" type="text/javascript" charset="utf-8"></script>
<script src="{{asset('assets/vendors/iCheck/js/icheck.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/vendors/toolbar/js/jquery.toolbar.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".form-group").find('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
		
		/* popover panel js */
		$("[data-toggle='popover']").popover();
		
		/* tooltops panel js */
		$(".tooltip-examples a").tooltip({
			placement: 'top'
		});
		
		/* popover panel fifa js */
		$('.po-markup > .po-link').popover({
			trigger: 'hover',
			
			html: true, // must have if HTML is contained in popover
			// get the title and conent
			title: function() {
				return $(this).parent().find('.po-title').html();
			},
			content: function() {
				return $(this).parent().find('.po-body').html();
			},
			container: 'body',
			placement: 'right'
		});
		
		$("#phone").inputmask('(999) 999-9999');
		$('#facebook_ad_form').SumoSelect({okCancelInMulti:true, selectAll:true });
		$('#facebook_page').SumoSelect({okCancelInMulti:true, selectAll:true });
		$('#manual_lead_type').SumoSelect({okCancelInMulti:true, selectAll:true });
		$('#manager_id').SumoSelect({okCancelInMulti:true, selectAll:true });
		
		$(".clear-form").click(function() {
			$('.campaign_add_error').html('');
		});
		
		$('#facebook_page').on('change', function() {
			var facebook_page_id=$(this).val();
			$.get("{{ route('facebook_ads_form') }}",
			{
				facebook_page_id: facebook_page_id
			},
			function(data){
				var innerhtm='<select  multiple="multiple" placeholder="Select Facebook Ad Form" name="facebook_ad_form[]" id="facebook_ad_form" class="select1 SumoUnder">';
				
				for(var i=0;i<data.length;i++)
				{
					innerhtm +='<option value="'+data[i].id+'">'+data[i].name+'</option>';
				}
				
				innerhtm +='</select>';
				
				$('#formHide').html(innerhtm);
				$('#facebook_ad_form').SumoSelect({okCancelInMulti:true, selectAll:true });
			});
		});
		
		$('#client_name').on('change', function() {
			var client_id=$(this).val();
			$.get("{{ route('client_info') }}",{client_id: client_id}, function(data){
				if(data.length==1)
				{
					$('#client_id').val(data[0].client_no);
					$('#client_contact').val(data[0].contact_name);
					$('#crm_email').val(data[0].crm_email);
				}
				else
				{
					$('#client_id').val('');
					$('#client_contact').val('');
					$('#crm_email').val('');
				}
			}); 
			
			$.get("{{ route('client_managers') }}",{client_id: client_id}, function(data){
				var innerhtm='<select multiple="multiple" placeholder="Select Users" name="manager_id[]" id="manager_id" class="select1 SumoUnder">';
				
				for(var i=0;i<data.length;i++)
				{
					innerhtm +='<option value="'+data[i].id+'">'+data[i].full_name+'</option>';
				}
				
				innerhtm +='</select>';
				
				$('#userHide').html(innerhtm);
				$('#manager_id').SumoSelect({okCancelInMulti:true, selectAll:true });
				
				$('.sumo_manager_id > .okCancelInMulti > .MultiControls > .btnOk').click(function() {
					$('#date-loader').show();
					var manager_id = $('#manager_id').val();
					if(manager_id == null)
					{
						$('#date-loader').hide();
					}
					else
					{
						$.get("{{ route('user-email-exists') }}",{ manager_id: manager_id.toString() }, function(data){
							if(data != '')
							{
								$.get("{{ route('user-details') }}",{ id: data }, function(manager){
									$('.UserModalTitle').html('User Name: '+manager.full_name);
									$('#UserId').val(manager.id);
									$('#user_email').val('');
									$('#UserModal').modal("show");
									for(var i=manager_id.length; i>=1; i--)
									{
										$('#manager_id')[0].sumo.unSelectItem(i-1);
									}
									$('#date-loader').hide();
								});
							}
							else
							{
								$('#date-loader').hide();
							}
						}); 
					}
				}); 
			});
		});
		
		$('#crm_account').on('change', function() {
			var crm_account = $(this).val();
			$('#crm_email').prop('readonly', false);
			if(crm_account == '' || crm_account == 'Agile' || crm_account=='Zoho' || crm_account=='Pownder™')
			{
				$(".SaveTestButton").prop('disabled', true);
				if(crm_account=='Pownder™')
				{
					$('#crm_email').val('');
					$('#crm_email').prop('readonly', true);
				}
			}
			else
			{
				$(".SaveTestButton").prop('disabled', false);
			}
		});
		
		$(".addButton").click(function(event){
			event.preventDefault();
			$(".addButton").prop('disabled', true);
			$(".SaveTestButton").prop('disabled', true);
			
			$('.campaign_add_error').html('');
			var data=$("#add_campaign_form").serialize();
			
			$.ajax({
				url: "{{ route('campaign_add') }}",
				type:'POST',
				data: data,
				success: function(data) {
					if(data=='success')
					{
						swal('Success!', 'Campaign added successfully.', 'success');
						window.location.href = "{{ route('campaigns') }}";
					}
					else
					{
						swal('Error!', data, 'error');
						$(".addButton").prop('disabled', false);
						$(".SaveTestButton").prop('disabled', false);
						var crm_account = $('#crm_account').val();
						if(crm_account == '' || crm_account == 'Agile' || crm_account == 'Zoho' || crm_account == 'Pownder™')
						{
							$(".SaveTestButton").prop('disabled', true);
						}
						else
						{
							$(".SaveTestButton").prop('disabled', false);
						}
					}		
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$("."+key).html(value);
					});
					$(".addButton").prop('disabled', false);
					$(".SaveTestButton").prop('disabled', false);
					var crm_account = $('#crm_account').val();
					if(crm_account == '' || crm_account == 'Agile' || crm_account == 'Zoho' || crm_account == 'Pownder™')
					{
						$(".SaveTestButton").prop('disabled', true);
					}
					else
					{
						$(".SaveTestButton").prop('disabled', false);
					}
				}       
			});
		});
		
		$(".SaveTestButton").click(function(event){
			event.preventDefault();
			$(".addButton").prop('disabled', true);
			$(".SaveTestButton").prop('disabled', true);
			
			$('.campaign_add_error').html('');
			var data=$("#add_campaign_form").serialize();
			
			$.ajax({
				url: "{{ route('campaign_add1') }}",
				type:'POST',
				data: data,
				success: function(data) {
					if(data.msg=='success')
					{
						swal('Success!', 'Campaign added successfully.', 'success');
						window.location.href = "{{ route('campaign_crm_leads') }}?campaign="+data.campaign_id;
					}
					else
					{
						swal('Error!', data.msg, 'error');
						$(".addButton").prop('disabled', false);
						$(".SaveTestButton").prop('disabled', false);
						var crm_account = $('#crm_account').val();
						if(crm_account == '' || crm_account == 'Agile' || crm_account == 'Zoho' || crm_account == 'Pownder™')
						{
							$(".SaveTestButton").prop('disabled', true);
						}
						else
						{
							$(".SaveTestButton").prop('disabled', false);
						}
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$("."+key).html(value);
					});
					$(".addButton").prop('disabled', false);
					$(".SaveTestButton").prop('disabled', false);
					var crm_account = $('#crm_account').val();
					if(crm_account == '' || crm_account == 'Agile' || crm_account == 'Zoho' || crm_account == 'Pownder™')
					{
						$(".SaveTestButton").prop('disabled', true);
					}
					else
					{
						$(".SaveTestButton").prop('disabled', false);
					}
				}       
			});
		});
		
 		$('.sumo_manager_id > .okCancelInMulti > .MultiControls > .btnOk').click(function() {
			$('#date-loader').show();
			var manager_id = $('#manager_id').val();
			if(manager_id == null)
			{
				$('#date-loader').hide();
			}
			else
			{
				$.get("{{ route('user-email-exists') }}",{ manager_id: manager_id.toString() }, function(data){
					if(data != '')
					{
						$.get("{{ route('user-details') }}",{ id: data }, function(manager){
							$('.UserModalTitle').html('User Name: '+manager.full_name);
							$('#UserId').val(manager.id);
							$('#user_email').val('');
							$('#UserModal').modal("show");
							
							var num = $('#manager_id > option').length;
							
							for(var i=0; i<num; i++)
							{
								$('#manager_id')[0].sumo.unSelectItem(i);
							}
							$('#date-loader').hide();
						});
					}
					else
					{
						$('#date-loader').hide();
					}
				}); 
			}
		});
		
		$("#user-email-update").on("submit", function( event ) {
			event.preventDefault();
			$(".modalButton").prop('disabled', true);
			$('.user_email').html('');
			$('#date-loader').show();
			var data = $("#user-email-update").serialize();
			
			$.ajax({
				url: "{{ route('user-email-update') }}",
				type: 'GET',
				data: data,
				success: function(data) {
					$('#UserModal').modal("hide");
					$(".modalButton").prop('disabled', false);
					$('#date-loader').hide();
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$(".user_email").html(value);
					});
					$(".modalButton").prop('disabled', false);
					$('#date-loader').hide();
				}       
			});
		}); 
		
		$('input[name="user_notification"]').on('ifChanged',function(){ 
            if($(this).val()=="No")
            { 
				$("#notification").prop('disabled', true);
				$("#notification").val('');
			}
            else
            { 
				$("#notification").prop('disabled', false);
				$("#notification").val('');
			}            
		});
	});
</script>
@stop