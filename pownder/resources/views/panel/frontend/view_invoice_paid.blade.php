@extends('layouts/default')
 
{{-- Page title --}} 
@section('title') 
Paid Invoice @parent 
@stop 

{{-- page level styles --}} 
@section('header_styles')
<!--start page level css -->
<link rel="stylesheet" media="all" type="text/css" href="{{asset('assets/css/custom_css/confirm_invoice.css')}}"  />
<!--end page level css-->
@stop 
{{-- Page content --}}
@section('content')
<?php 
function showErrorMsg($error)
{
    if(count($error)>0)
    {
        $msg='';
        foreach($error as $val)
        {
         $msg.='<small class="help-block animated fadeInUp text-danger" style="color: #FB8678;">'.$val.'</small>';    
        }
        return $msg;        
    }
}
//Money Format
 function money_format1($number)
{
  setlocale(LC_MONETARY, 'en_US'); 
  return money_format('%!.2i',$number); 
}
?>
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
			<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
        <li>
			<a href="{{ route('invoice') }}">
			 Invoice
			</a>
		</li>
        <li>
			<a href="{{ route('manage-invoice') }}">
			 Manage Invoice
			</a>
		</li>
		<li class="active">
		  View Invoice
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content p-l-r-15">
  <form action="{{route('update-recurring-invoice')}}" method="post" class="recurring_invoice_update">
         
   <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}"  />
   @include('panel.includes.status') 
    
	<div class="row">
      <div class="col-lg-12">
		<div class="panel panel-success filterable">
          <div class="panel-heading">
             <h3 class="panel-title">
              <i class="fa fa-fw fa-th-large"></i> Paid Invoice
             </h3>             
          </div>
          
          <div class="panel-body">
			  @if(count($invoice_data)>0)
               <?php echo showErrorMsg($errors->all()); ?> 
                 <input type="hidden" name="invoice_id" value="{{$invoice_data[0]->invoice_id}}" />
                 <input type="hidden" name="invoice_num" value="{{$invoice_data[0]->invoice_num}}" />
                 
                <div class="row clearfix">
            	 <div class="col-md-5 pull-right">
                   <div class="well">
                    <div class="row">
                     <?php 
                        
                        if($invoice_data[0]->pay_method_type=="by_admin")
                        {
                        echo '<div class="col-md-4">';
                        }
                        else
                        {
                            echo '<div class="col-md-12">';
                        }
                        echo '<b>Payment Info</b><br />';
                         switch($invoice_data[0]->pay_method_type)
                         {
                            case "card" : 
                            if($invoice_data[0]->creditcardfee_perc && $invoice_data[0]->creditcardfee_amount)
                            {
                            echo '<small><b>By Credit Card ('.$invoice_data[0]->payment_via.')</b></small>';
                            }
                            else
                            {
                            echo '<small><b>By Debit Card ('.$invoice_data[0]->payment_via.')</b></small>';
                            }
                            break;
                                                        
                            case "bank" : echo '<small><b>By Bank Account ('.$invoice_data[0]->payment_via.')</b></small>'; break;
                            
                            case "by_admin" : echo '<small><b>By Cheque</b></small>';    break;
                            
                         }
                         if($invoice_data[0]->pay_method_type=="by_admin")
                         {
                         echo '</div><div class="col-md-8"><b>Bank Cheque Images</b><br />';                         
                         if($invoice_data[0]->cheque_images)
                         {
                            
                            $files=explode(',',$invoice_data[0]->cheque_images);
                            foreach($files as $file)
                            {
                            echo '<small><a target="_blank" href="'.asset('pownder/storage/app/uploads/invoice/cheque_images/'.$file).'">'.$file.'</a></small><br/>';
                            }
                            
                         }
                         else
                         {
                            echo '<small>N/A</small>';
                         }
                         }
                          echo '</div>';
                     ?>
                     </div>
                   </div>
                 </div>
                </div>  
   	 
                <div class="row clearfix">
            	<div class="col-md-8 col-md-offset-2">            	 
            		<section class="main_wrapper">
            			<!--header area-->
            			<div class="table-responsive invoice-table">
            				<table class="table table-condensed">
            					<thead>
            					</thead>
            					<tbody>
            						<tr>
            							<td class="col-50" colspan="2">
            								<img class="invoice-logo" src="{{asset('pownder/storage/app/uploads/invoice/company_logo/'.$invoice_data[0]->company_logo)}}"/>
            								<h2 class="title m-t-10 m-b-10 main-title">
            									Invoice
            								</h2>
            								<h4 class="title m-t-10 m-b-10 sub-title">
            									{{$invoice_data[0]->company_name}}
            								</h4>
            							</td>
            							<td class="col-10">
            							</td>
            							<td class="col-40 align-bottom" colspan="2">
            								<p class="m-0 right " style="min-height: 50px">
            									<?php echo nl2br($invoice_data[0]->company_address);?>
            								</p>
            							</td>
            						</tr>
            					</tbody>
            				</table>
            				<!--header bottom area-->
            				<div class="invoice-header-bottom">
            					<table class="col-100">
            						<thead>
            						</thead>
            						<tbody>
            							<tr>
            								<td class="col-10 align-top" colspan="1" rowspan="4">
            									<p class="m-0 title">
            										Bill To:
            									</p>
            								</td>
            								<td class="col-40 align-top" colspan="2" rowspan="4">
            									<p class="content align-top" id="bill_to_field">
            										<?php echo nl2br($invoice_data[0]->bill_to); ?>
            									</p>
            								</td>
            								<td class="col-20 align-top">
            									<p class="title align-top right">
            										Invoice Num
            									</p>
            								</td>
            								<td class="col-20 align-top" colspan="2">
            									<p class="content align-top right">										
            										<span id="time">
            											{{ $invoice_data[0]->invoice_num }}
            										</span>
            									</p>
            								</td>
            							</tr>
            							<tr>
            								<td class="col-20 align-top">
            									<p class="title align-top right">
            										Date
            									</p>
            								</td>
            								<td class="col-20 align-top" colspan="2">
            									<p class="content align-top right" id="date_field">
            										{{date('m/d/Y',strtotime($invoice_data[0]->invoice_date))}}
            									</p>
            								</td>
            							</tr>
            							<tr>
            								<td class="col-20 align-top">
            									<p class="title align-top right">
            										Due Date
            									</p>
            								</td>
            								<td class="col-20 align-top" colspan="2">
            									<p class="content align-top right" id="due_date_field">
            										{{date('m/d/Y',strtotime($invoice_data[0]->due_date))}}
            									</p>
            								</td>
            							</tr>
                                        @if($invoice_data[0]->recurring_on)
            							<tr>
            								<td class="col-20 align-top">
            									<p class="title align-top right">
            										Recurring Date
            									</p>
            								</td>
            								<td class="col-20 align-top" colspan="2">
            									<p class="content align-top right" id="recurring_date_field">
                                                    @if($invoice_data[0]->recurring_date and $invoice_data[0]->recurring_date!="0000-00-00 00:00:00")
            										{{date('m/d/Y',strtotime($invoice_data[0]->recurring_date))}}
                                                    @endif
            									</p>
            								</td>
            							</tr>
                                       @endif 
            						</tbody>
            					</table>
            				</div>
            				<!--header title area-->
            				<div class="invoice-title-section">
            					<table class="col-100">
            						<tbody>
                                       <?php $firstcolm='col-30'; ?>
            							<tr>
                                           @if($invoice_data[0]->recurring_on and $invoice_data[0]->has_recurred!=1)
                                           <td class="col-10 align-top">
                                             <p class="title align-top left">                                                   
                                                      <i title="Add New Item" class="fa fa-plus add_item_recurring" ></i>                                                      
                                       			</p>
                                           </td>
                                           <?php $firstcolm='col-20'; ?>
                                           @endif 
            								<td class="{{$firstcolm}} align-top"  >
            									<p class="title align-top left">                                                   
            										Item
            									</p>
            								</td>
                                            <td class="col-25 align-top">
            									<p class="title align-top left">
            										Description
            									</p>
            								</td>
            								<td class="col-15 align-top">
            									<p class="title align-top right">
            										Quantity
            									</p>
            								</td>
            								<td class="col-15 align-top">
            									<p class="title align-top right">
            										Rate
            									</p>
            								</td>
            								<td class="col-15 align-top">
            									<p class="title align-top right">
            										Amount
            									</p>
            								</td>
            							</tr>
            						</tbody>
            					</table>
            				</div>
            				<!--header content area-->
            				<div class="invoice-details-section">
            					<div class="item-details-section">
            						<table class="col-100" style="position: relative;">
            							<tbody class="append_items">
                                            
            								@foreach($invoice_data as $value)
            								<tr class="border-b">
                                               @if($invoice_data[0]->recurring_on and $invoice_data[0]->has_recurred!=1)
                                               <td class="col-10 align-top" >                                                   
            										<p class="content align-top left">                                                      
                                                      <i class="fa fa-times delete" title="Remove Item" ></i>
                                                      <i title="Edit Item" class="fa fa-pencil edit-recurring"></i>
                                                      <i title="Copy Paste Item" class="fa fa-database copy_paste"></i>                                                                                           
            										</p>
            									</td>
                                                @endif
                                                <td class="{{$firstcolm}} align-top" >                                                   
            										<p class="content align-top left">                                                      
                                                      <input type="text" name="item[]" class="form-control no-display recurring_item" value="{{$value->item}}" />
            										   <span class="recurring_item_static editable">{{$value->item}}</span>                                                       
            										</p>
            									</td> 
            									<td class="col-25 align-top" >
            										<p class="content align-top left">
                                                        <textarea  name="description[]" class="form-control no-display recurring_description" >{{$value->description}}</textarea>
            											<span class="recurring_description_static editable"><?php echo nl2br($value->description); ?></span>                                                        
            										</p>
            									</td>
            									<td class="col-15 align-top">
            										<p class="content align-top right">
                                                        <input type="number" name="qty[]" class="form-control no-display recurring_qty" value="{{$value->qty}}" step="0.01" />
            											<span class="recurring_qty_static editable">{{money_format1($value->qty)}}</span>
            										</p>
            									</td>
            									<td class="col-15 align-top">
            										<p class="content align-top right">
                                                       <span class="no-display">$</span>
                                                       <input type="number" name="rate[]" class="form-control no-display recurring_rate" value="{{$value->rate}}" step="0.01" />
            											<span class="recurring_rate_static editable">${{money_format1($value->rate)}}</span>
            										</p>
            									</td>
            									<td class="col-15 align-top">
                                                    <input type="hidden" name="amount[]" value="{{$value->amount}}" class="recurring_amount" />
            										<p class="content align-top right recurring_amount_static">                                                        
            											${{money_format1($value->amount)}}
            										</p>
            									</td>
            								</tr>
            								@endforeach
            							</tbody>
            						</table>
            					</div>
            					<table class="col-100">
            						<tbody>
            							<!--bill section-->
            							<tr>
            								<td class="col-60 align-top" colspan="3">
            								</td>
            								<td class="col-20 align-top">                                                
            									<p class="title align-top right">
            										Subtotal
            									</p>
            								</td>
            								<td class="col-20 align-top">
                                                <input type="hidden" name="sub_total" value="{{ $invoice_data[0]->sub_total }}" />
            									<p class="title align-top right calculation_sub_total">
            										${{ money_format1($invoice_data[0]->sub_total) }}
            									</p>
            								</td>
            							</tr>
            							<tr>
            								<td class="col-60 align-top" colspan="3">
            								</td>
            								<td class="col-20 align-top">
                                               <input type="hidden" name="tax_perc" value="{{$invoice_data[0]->tax_perc}}"  />
            									<p class="title align-top right">
            										Tax(
            										<span class="calculation_tax_perc" id="tax_field">
            											{{money_format1($invoice_data[0]->tax_perc)}}
            										</span>
            										%)
            									</p>
            								</td>
            								<td class="col-20 align-top">
                                                <input type="hidden" name="tax_value" value="{{$invoice_data[0]->tax}}"  />
            									<p class="title align-top right calculation_tax">
            										${{money_format1($invoice_data[0]->tax)}}
            									</p>
            								</td>
            							</tr>
            							<tr>
            								<td class="col-60 align-top" colspan="3">
            								</td>
            								<td class="col-20 align-top">                                               
            									<p class="title align-top right">
            										Total
            									</p>
            								</td>
            								<td class="col-20 align-top">
                                                <input type="hidden" name="total" value="{{$invoice_data[0]->total}}" />
            									<p class="title align-top right calculation_total">
            										${{money_format1($invoice_data[0]->total)}}
            									</p>
                                                <p  class="calculation_paid invisible "></p>
                                                <p style="font-size:1px" class="calculation_balance invisible "></p>  
            								</td>
            							</tr>
            							<!--<tr>
            								<td class="col-60 align-top" colspan="3">
            								</td>
            								<td class="col-20 align-top">
            									<p class="title align-top right">
            										Paid
            									</p>
            								</td>
            								<td class="col-20 align-top">
                                            -->
                                                <input type="hidden" name="paid" value="{{$invoice_data[0]->paid}}" />
                                                                                              
            								<!--	<p class="title align-top right calculation_paid">
            										${{money_format1($invoice_data[0]->paid)}}
            									</p>
            								</td>
            							</tr> 
                                        -->  
                                        <!--<tr>
            								<td class="col-60 align-top" colspan="3">
            								</td>
            								<td class="col-20 align-top">
            									<p class="title align-top right">
            										Balance Due
            									</p>
            								</td>
            								<td class="col-20 align-top">
                                           -->    <input type="hidden" name="balance" value="{{$invoice_data[0]->balance}}" />
            								<!--	<p class="title align-top right calculation_balance">
            										${{money_format1($invoice_data[0]->balance)}}
            									</p>
            								</td>
            							</tr>-->
                                                 							
                                        @if($invoice_data[0]->creditcardfee_perc and $invoice_data[0]->creditcardfee_amount)
                                        
                                        <tr>
            								<td class="col-60 align-top" colspan="3">
            								</td>
            								<td class="col-20 align-top">
            									<p class="title align-top right">
            										Transactional Fee ({{$invoice_data[0]->creditcardfee_perc}} %)
            									</p>
            								</td>
            								<td class="col-20 align-top">
            									<p class="title align-top right">
            										${{money_format1($invoice_data[0]->creditcardfee_amount)}}
            									</p>
            								</td>
            							</tr>
                                        
                                        @endif
                                        <tr>
            								<td class="col-60 align-top" colspan="3">
            								</td>
            								<td class="col-20 align-top final-due">
            									<p class="title align-top right">
            										Total Paid
            									</p>
            								</td>
            								<td class="col-20 align-top final-due">
            									<p class="title align-top right">
            										${{money_format1($invoice_data[0]->total_amount)}}
            									</p>
            								</td>
            							</tr>
                                        
                                        
                                                                               
            						</tbody>
            					</table>
            					
            				</div>
            			</div>
            		</section>                     
            	 
            	</div>
            </div>
            <br />
            @if($invoice_data[0]->recurring_on and $invoice_data[0]->has_recurred!=1)             
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <button title="Update Invoice For Next Recurring Date" class="btn btn-success pownder_green submit_changes_btn pull-right no-display" type="submit">Update Invoice</button>
              
              </div>
            </div>
            <br />
           @endif  
  
@endif		
					    
         </div>
        
        </div>
		
	  </div>	
	</div>
  </form>    
</section>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header gred_2">
				<button type="button" class="close" data-dismiss="modal">
					&times;
				</button>
				<h4 class="modal-title" style="color:#fff;">
					Item List
				</h4>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table item-list-table">
						<thead>
							<tr>
								<td class="title">
									Item
								</td>
                                <td class="title">
									Description
								</td>
                                <td class="title">
                                    Qty
                                </td>
                                <td class="title">
                                    Rate
                                </td>
                                <td class="title" colspan="2">
                                    
                                </td>
							</tr>
						</thead>
						<tbody>
                            @foreach($saved_items as $item)
							<tr>                                
                                <td class="i_item">
									{{$item->item}}
								</td> 
								<td class="i_des">
									<?php echo nl2br($item->description); ?>                                    
								</td>  
                                <td class="i_qty">
                                   1
                                </td>
                                <td class="i_rate">
                                  $0.00
                                </td>                              
								<td>
									<button title="Add Item" type="button" class="btn btn-sm bg-warning add-item-db-recurring i_add_button i_add pull-right">
										<i class="fa fa-plus"></i>
									</button>
                                    <input type="hidden" class="saved_item_id" value="{{$item->id}}" />
								</td>
                                <td>
									<button title="Remove Item" type="button" class="btn btn-sm bg-danger remove-saved-item i_add_button i_add pull-right">
										<i class="fa fa-close"></i>
									</button>
								</td>
							</tr>
                            @endforeach
							
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" style=" background: #ffbe18; color:#fff;">
					Close
				</button>
			</div>
		</div>
	</div>
</div>

<!-- /.content -->
@stop 

{{-- page level scripts --}} 
@section('footer_scripts')
<script type="text/javascript" src="{{asset('assets/js/custom_js/invoice.js')}}"></script>
@stop