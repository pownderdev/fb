@extends('layouts/default')

{{-- Page title --}} 
@section('title') 
Bot Report @parent 
@stop 

{{-- page level styles --}} 
@section('header_styles')
<!--start page level css -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" type="text/css"  href="assets/css/custom_css/chartjs-charts.css" >
<link rel="stylesheet" type="text/css" href="assets/vendors/bootstrap-table/css/bootstrap-table.min.css">
<link rel="stylesheet" type="text/css" href="assets/css/custom_css/bootstrap_tables.css">
<!--end page level css-->
@stop 
{{-- Page content --}}
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
        <li>
			<a href="{{ route('manage-bot') }}">
				Manage Bot
			</a>
		</li>
		<li class="active">
			<a href="{{ route('bot-report') }}">
				Bot Report
			</a>
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content p-l-r-15">
        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" /> 
      <div class="row">
			<div class="col-lg-12">
				<div class="panel panel-success filterable">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-fw fa-th-large">
							</i>
							Chat Report - <span class="filter_date_range">Lifetime</span>
						</h3>
					</div>
					<div class="panel-body">                   
                       
						<div class="row">
                           <div class="col-md-12">
                              <div class="table-responsive">
                                  <table id="questions" class="table table-bordered"  >
                                    <thead>
        								<tr>
                                            <th>Total Messages</th> 
                                            <th>Total User Messages</th>
                                            <th>Total Bot Messages</th>
                                            <th>Get Started</th>
        									<th>Answered By Bot</th>
                                            <th>Unanswered Queries</th>
                                            <th>Questions Asked By Bot</th>
        								</tr>
        							</thead>
                                    <tbody id="total_chat_report">
                                       <tr>                                          
                                          <td>{{$total_msg}}</td>
                                          <td>{{$total_user_msg}}</td>
                                          <td>{{$total_bot_msg}}</td>
                                          <td>{{$total_get_started_msg}}</td>
                                          <td>{{$total_anwered_by_bot}}</td>
                                          <td>{{$total_unanwered_queries}}</td>
                                          <td>{{$total_bot_question_asked}}</td>
                                       </tr>
        							</tbody>
        						 </table>
                              </div>
                           </div>
                        </div>
                        
					</div>
				</div>
			</div>
		</div>
        <div class="row">
			<div class="col-lg-12">
				<div class="panel panel-success filterable">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-fw fa-th-large">
							</i>
							Keywords (User Says) Report - <span class="filter_date_range">Lifetime</span>
						</h3>
					</div>
					<div class="panel-body">                   
                       
						<div class="row">
                           <div class="col-md-12">
                              <div class="table-responsive">
                                  <table id="questions" class="table table-bordered" data-toggle="table"  data-search="true" data-show-columns="true" data-pagination="true" data-page-list="[10, 20,40,ALL]" >
                                    <thead>
        								<tr>                                    
        								  <th>User Says</th>
                                          <!--<th data-visible="false">Bot Replies</th>-->
                                          <th>No. of time Asked </th>
                                          <th>Percentage</th>
        								</tr>
        							</thead>
                                    <tbody id="keywords_matching_table">
                                         @foreach($user_says_report as $keyword)
                                            <tr>
                                              <td>{{$keyword->user_says}}</td>
                                              <!--<td>
                                              <?php //echo nl2br(str_replace('*@*',"<br/>--------<br/>",$keyword->bot_reply)); ?>
                                              </td>-->
                                              <td>{{$keyword->no_of_time_asked}}</td>
                                              <td>{{$keyword->Percentage}} %</td>
                                            </tr>
                                          @endforeach                                      
        							</tbody>
        						 </table>
                              </div>
                           </div>
                        </div>
                        
					</div>
				</div>
			</div>
		</div>	
</section>


<!-- /.content -->
@stop 

{{-- page level scripts --}} 
@section('footer_scripts')
<!-- begining of page level js -->
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- date-range-picker -->
<script type="text/javascript" src="assets/vendors/daterangepicker/js/daterangepicker.js"></script>
<!-- begining of page level js -->
<script src="assets/vendors/chartjs/js/invoice_report_charts.js"></script>
<!-- begining of page level js -->
<script type="text/javascript" src="assets/vendors/bootstrap-table/js/bootstrap-table.min.js"></script>
<script type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bot_report.js')}}"></script>
@stop