@extends('layouts/default')

{{-- Page title --}} 
@section('title') 
Answers @parent 
@stop 

{{-- page level styles --}} 
@section('header_styles')
<!--start page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" />
<style>	
    .bootstrap-tagsinput input[type="text"] , .bootstrap-tagsinput .tag
    {
        font-size: 13.5px !important;
    }
</style>
<!--end page level css-->
@stop 
{{-- Page content --}}
@section('content')
<?php 
	function showErrorMsg($error)
	{
		if(count($error)>0)
		{
			$msg='';
			foreach($error as $val)
			{
				$msg.='<small class="help-block animated fadeInUp text-danger" style="color: #FB8678;">'.$val.'</small>';    
			}
			return $msg;        
		}
	}
	//Money Format
	function money_format1($number)
	{
		setlocale(LC_MONETARY, 'en_US'); 
		return money_format('%!.2i',$number); 
	}
?>
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
        <li>
			<a href="{{ route('manage-bot') }}">
				Manage Bot
			</a>
		</li>
		<li class="active">
			<a href="{{ route('answers') }}">
				Manage Answers
			</a>
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content p-l-r-15">
    <?php $url='add-answer'; if(Request::has('edit')) {$url='edit-answer';} ?> 
	<form action="{{route($url)}}" method="post" class="form-horizontal form_answers">
		<input type="hidden" name="_token" id="_token" value="{{csrf_token()}}" />
                
        @if(Request::has('edit'))
        <input type="hidden" name="question_id" id="question_id" value="{{$edit_question->id}}" />
        @endif
        
		@include('panel.includes.status')
        
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-success filterable">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-fw fa-th-large">
							</i>
							@if(Request::has('edit')) Edit @else Add @endif Answer
						</h3>
					</div>
					<div class="panel-body">
                        <?php echo showErrorMsg($errors->all()); ?>
						<div class="form-group">
							<label class="control-label col-sm-3">
								Question
							</label>
							<div class="col-sm-9">
								<input type="text" name="question" id="question_tags" required="" class="form-control" value="@if(Request::has('edit')){{$edit_question->question}}@else{{$unanswered_queries}}@endif" placeholder="User Says" data-role="tagsinput"/>
                                <small class="help-block">Write question and press Tab to enter it.</small>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3">
								Answer
							</label>
							<div class="col-sm-9">
                                 <div class="col-md-12 answers_container">
                                 <?php if(Request::has('edit')){ $arr=explode('$%^',$edit_question->answer); 
                                 for($i=0;$i<count($arr)-1;$i++)
                                 {
                                    echo '<div class="row"><div style="padding:0px;margin:0" class="col-md-10" class="col-md-10"><input type="hidden" name="answer[]" value="'.$arr[$i].'" /><p class="answer_text p-2 help-block text-white bg-default">'.nl2br($arr[$i]).'</p></div><div style="padding-top: 5px;" class="col-md-2"><div class="btn-group btn-group-xs"><button type="button" class="btn btn-primary remove-answer"><i class="fa fa-trash" title="Remove"></i></button><button type="button" class="btn btn-info edit-answer"> <i class="fa fa-pencil" title="Edit"></i> </button></div></div></div>';
                                 } 
                                 }
                                 ?>
                                 </div> 
								<textarea name="answer[]" id="answers_box"  required="" class="form-control" placeholder="Bot Reply">@if(Request::has('edit')){{$arr[count($arr)-1]}}@endif</textarea>
                                <br />
                                <button type="button" class="btn btn-warning btn-sm pownder_yellow add_another_answer" title="Add Another Answer"><i class="fa fa-plus-circle"></i></button>
							</div>                            
						</div>
                        
                        <div class="form-group">
							<label class="control-label col-sm-3">								
							</label>
							<div class="col-sm-9">
								<button type="submit" class="btn btn-success">@if(Request::has('edit')) Update @else Add @endif Answer</button>
							</div>
						</div>
                        <div class="form-group">
							<small class="col-sm-3 text-danger">
								{First_Name} = Person's First Name
							</small>
                        </div>    
                        
					</div>
				</div>
			</div>
		</div>
   </form>     
        <div class="row">
			<div class="col-lg-12">
				<div class="panel panel-success filterable">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-fw fa-th-large">
							</i>
							All Answers
						</h3>
					</div>
					<div class="panel-body">
						<table id="questions" data-toggle="table"  data-search="true" data-show-columns="true" 
						data-pagination="true" data-page-list="[10, 20,40,ALL]">
                            <thead>
								<tr>
									<th>User Says</th>
									<th>Bot Reply</th>
                                    <th>Enabled</th>
									<th>Date</th> 
									<th class="text-center">Action</th>
								</tr>
							</thead>
                            <tbody>
                                <?php $i=1; ?>
								@foreach($questions as $question) 
                                <?php $arr=explode(',',$question->question); $l=0; ?>                               
								<tr>
                                    <td>
                                    	  @foreach($arr as $quest)
                                            <?php
                                            switch($l)
                                            {
                                                //case "0": $class="label-default";break;
                                                case "0": $class="label-primary";break;
                                                case "1": $class="label-success";break;
                                                //case "3": $class="label-warning";break;
                                                case "2": $class="label-info";break;
                                                //case "5": $class="label-danger";break;
                                                case "3": $class="bg-default";break;
                                            }                                             
                                            if($l==3){$l=0;}
                                            $l++;  
                                            ?>                                     
                                    		<span class="label {{$class}} text-15 m-3 display-block">
                                    			{{$quest}}
                                    		</span>
                                          @endforeach                                      		
                                    	
                                    </td>									
									<td><?php echo nl2br(str_replace('$%^',"<br/>---------<br/>",$question->answer)); ?></td>
                                    <td>
                                      <label class="switch ib switch-custom m-t-5">
        									<input type="checkbox" @if($question->status) checked="" @endif class="status_answer" id="status_answer{{$i}}" data-id="{{$question->id}}" data-status="{{$question->status}}"   />
        									<label for="status_answer{{$i++}}" data-on="Yes" data-off="No">
        									</label>
 								      </label>
                                    </td>                                    
									<td>{{date('m/d/Y H:i:s',strtotime($question->created_at))}}</td>
									<td>
										<a href="#" data-id="{{$question->id}}" class="delete-answer"><i class="fa fa-trash" title="Delete"></i></a>
										<a href="?edit={{$question->id}}"><i class="fa fa-edit" title="Edit"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>	
</section>


<!-- /.content -->
@stop 

{{-- page level scripts --}} 
@section('footer_scripts')
<script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.20/angular.min.js"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-tagsinput/bootstrap-tagsinput-angular.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/custom_questions.js')}}"></script>
@stop