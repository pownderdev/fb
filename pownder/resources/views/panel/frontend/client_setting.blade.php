@extends('layouts/default')

{{-- Page title --}}
@section('title')
Setting
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/tab.css')}}">
<link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/js/sumoselect/sumoselect.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendors/toolbar/css/jquery.toolbar.css')}}">
<style>
	.faq-cat-content .panel-heading:hover {
    background-color: #1dd4cb;
	} 
	
	/* ============================================================
	COMMON
	============================================================ */
	.cmn-toggle {
	position: absolute;
	margin-left: -9999px;
	visibility: hidden;
	}
	.cmn-toggle + label {
	/*display: block;*/
	position: relative;
	cursor: pointer;
	outline: none;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	}
	
	/* ============================================================
	SWITCH 1 - ROUND
	============================================================ */
	input.cmn-toggle-round + label {
	padding: 2px;
	width: 40px;
	height: 20px;
	background-color: #dddddd;
	-webkit-border-radius: 60px;
	-moz-border-radius: 60px;
	-ms-border-radius: 60px;
	-o-border-radius: 60px;
	border-radius: 60px;
	}
	input.cmn-toggle-round + label:before, input.cmn-toggle-round + label:after {
	display: block;
	position: absolute;
	top: 1px;
	left: 1px;
	bottom: 1px;
	content: "";
	}
	input.cmn-toggle-round + label:before {
	right: 1px;
	background-color: #f1f1f1;
	-webkit-border-radius: 60px;
	-moz-border-radius: 60px;
	-ms-border-radius: 60px;
	-o-border-radius: 60px;
	border-radius: 60px;
	-webkit-transition: background 0.4s;
	-moz-transition: background 0.4s;
	-o-transition: background 0.4s;
	transition: background 0.4s;
	}
	input.cmn-toggle-round + label:after {
	width: 19px;
	background-color: #fff;
	-webkit-border-radius: 100%;
	-moz-border-radius: 100%;
	-ms-border-radius: 100%;
	-o-border-radius: 100%;
	border-radius: 100%;
	-webkit-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
	box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
	-webkit-transition: margin 0.4s;
	-moz-transition: margin 0.4s;
	-o-transition: margin 0.4s;
	transition: margin 0.4s;
	}
	input.cmn-toggle-round:checked + label:before {
	background-color: #8ce196;
	}
	input.cmn-toggle-round:checked + label:after {
	margin-left: 20px;
	}
</style>
<!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">Setting</li>
	</ol>
</section>

<!-- Main content -->
<section class="content p-l-r-15">
    @include('panel.includes.status')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<!--tab starts-->
			<!-- Nav tabs category -->
			<ul class="nav nav-tabs" style="margin-top:0px">
				<li class="active"><a href="#faq-cat-1" id="Email-Notification-Tab" data-toggle="tab">Email Notification</a></li>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content faq-cat-content">
				<div class="tab-pane active in fade" id="faq-cat-1">
					<div class="panel-group" id="accordion-cat-1">
						<div class="panel panel-success panel-faq">
							<div class="panel-heading">
								<h4 class="panel-title" style="color: #fff;">
									<i class="fa fa-fw fa-th-large"></i> Email Notification
								</h4>
							</div>
							<div id="faq-cat-1-sub-1" class="panel-collapse collapse in">
								<div class="panel-body">
									<div class="row">
										<div class="col-md-12">
											<table data-toggle="table">
												<thead>
													<tr>
														<th>S. No.</th>
														<th>User Name</th>
														<th>Appointments Notifier <div class="switch11" style="display: inline-flex;" title="Switch to select all"><input id="cmn-toggle-10" type="checkbox" value="All" class="cmn-toggle cmn-toggle-round"><label for="cmn-toggle-10"></label></div></th>
														<th>Lead Notifier <div class="switch22" style="display: inline-flex;" title="Switch to select all"><input id="cmn-toggle-20" type="checkbox" value="All" class="cmn-toggle cmn-toggle-round"><label for="cmn-toggle-20"></label></div></th>
													</tr>
												</thead>
												<tbody id="BodyContent">
													@php 
													$i=0; 
													$appointment_notifier = '';
													$lead_notifier = '';
													@endphp
													
													@foreach($Managers as $Manager)
													<tr>
														<td>{{ ++$i }}.</td>
														<td>{{ $Manager->first_name }} {{ $Manager->last_name }}</td>
														@if($Manager->appointment_notifier == 'On')
														<td>
															<div class="switch1" style="display: inline-flex;"><input  id="cmn-toggle-1{{ $i }}" type="checkbox" value="{{ $Manager->id }}" class="cmn-toggle appointment_notifier cmn-toggle-round abcdef" checked><label for="cmn-toggle-1{{ $i }}"></label>
															</div>
														</td>
														@else
														<td>
															<div class="switch1" style="display: inline-flex;"><input id="cmn-toggle-1{{ $i }}" type="checkbox" value="{{ $Manager->id }}" class="cmn-toggle appointment_notifier cmn-toggle-round abcdef" ><label for="cmn-toggle-1{{ $i }}"></label></div>
														</td>
														@php $appointment_notifier = 'yes'; @endphp
														@endif
														
														@if($Manager->lead_notifier == 'On')
														<td>
															<div class="switch2" style="display: inline-flex;"><input  id="cmn-toggle-2{{ $i }}" type="checkbox" value="{{ $Manager->id }}" class="cmn-toggle lead_notifier cmn-toggle-round ghijkl" checked><label for="cmn-toggle-2{{ $i }}"></label></div>
														</td>
														@else
														<td>
															<div class="switch2" style="display: inline-flex;"><input id="cmn-toggle-2{{ $i }}" type="checkbox" value="{{ $Manager->id }}" class="cmn-toggle lead_notifier cmn-toggle-round ghijkl" ><label for="cmn-toggle-2{{ $i }}"></label></div>
														</td>
														
														@php $lead_notifier = 'yes'; @endphp
														@endif
													</tr>
													@endforeach
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- /.content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script>
	$(document).ready(function(){		
		@if($appointment_notifier != 'yes') 
		$('#cmn-toggle-10').prop('checked', true);
		@else
		$('#cmn-toggle-10').prop('checked', false);
		@endif
		
		@if ($lead_notifier != 'yes') 
		$('#cmn-toggle-20').prop('checked', true);
		@else
		$('#cmn-toggle-20').prop('checked', false);
		@endif
		
		$(".switch1 input[type='checkbox']").on('change', function (event, state) {
			var manager_id = $(this).val();
			var $section = $(this);
			$('#date-loader').show();
			if($(this).is(':checked')) 
			{
				$.get("{{ route('user-appointment-notifier') }}", { 'id':manager_id, 'appointment_notifier':'On' }, function(data) {
					//alert('On');
					$('#date-loader').hide();
				});			
			} 
			else 
			{
				$.get("{{ route('user-appointment-notifier') }}", { 'id':manager_id, 'appointment_notifier':'Off' }, function(data) {
					//alert('Off');
					$('#date-loader').hide();
				});			
			}
			
			if ($('.abcdef:checked').length == $('.abcdef').length) 
			{
				$('#cmn-toggle-10').prop('checked', true);
			}
			else
			{
				$('#cmn-toggle-10').prop('checked', false);
			}
		});
		
		$(".switch2 input[type='checkbox']").on('change', function (event, state) {
			var manager_id = $(this).val();
			var $section = $(this);
			$('#date-loader').show();
			if($(this).is(':checked')) 
			{
				$.get("{{ route('user-lead-notifier') }}", { 'id':manager_id, 'lead_notifier':'On' }, function(data) {
					//alert('On');
					$('#date-loader').hide();
				});			
			} 
			else 
			{
				$.get("{{ route('user-lead-notifier') }}", { 'id':manager_id, 'lead_notifier':'Off' }, function(data) {
					//alert('Off');
					$('#date-loader').hide();
				});			
			}
			
			if ($('.ghijkl:checked').length == $('.ghijkl').length) 
			{
				$('#cmn-toggle-20').prop('checked', true);
			}
			else
			{
				$('#cmn-toggle-20').prop('checked', false);
			}
		});
		
		$(".switch11 input[type='checkbox']").on('change', function (event, state) {
			var checked = $(this).prop('checked');
			$('#BodyContent').find('.appointment_notifier').prop('checked', checked);
			$('#date-loader').show();
			if($(this).is(':checked')) 
			{
				$.get("{{ route('all-user-appointment-notifier') }}", { 'client_id':{{ Session::get('client_id') }}, 'appointment_notifier':'On' }, function(data) {
					$('#date-loader').hide();
				});			
			} 
			else 
			{
				$.get("{{ route('all-user-appointment-notifier') }}", { 'client_id':{{ Session::get('client_id') }}, 'appointment_notifier':'Off' }, function(data) {
					$('#date-loader').hide();
				});			
			}
		});
		
		$(".switch22 input[type='checkbox']").on('change', function (event, state) {
			var checked = $(this).prop('checked');
			$('#BodyContent').find('.lead_notifier').prop('checked', checked);
			$('#date-loader').show();
			if($(this).is(':checked')) 
			{
				$.get("{{ route('all-user-lead-notifier') }}", { 'client_id':{{ Session::get('client_id') }}, 'lead_notifier':'On' }, function(data) {
					$('#date-loader').hide();
				});			
			} 
			else 
			{
				$.get("{{ route('all-user-lead-notifier') }}", { 'client_id':{{ Session::get('client_id') }}, 'lead_notifier':'Off' }, function(data) {
					$('#date-loader').hide();
				});			
			}
		});
	});
</script>
@stop																																									