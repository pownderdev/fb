@extends('layouts/default')

{{-- Page title --}}
@section('title')
Call Track
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/bootstrap-table/css/bootstrap-table.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_css/bootstrap_tables.css')}}">
<!--page level css -->
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/daterangepicker/css/daterangepicker.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/jquerydaterangepicker/css/daterangepicker.min.css')}}">
<style>
	button[name=paginationSwitch] {display:none;}
	button[name=toggle] {display:none;}
	
	.export.btn-group {*display:none!important;}
	.fixed-table-toolbar .search {width: Calc( 100% - 108px );}
	.table-summary {display:inline-block; border: 1px solid #ccc; margin-top: 15px;}
	
	.table-summary .wrapp {float:left; text-align:center; width:100px;}
	.table-summary .wrapp .title{padding: 5px 0; font-weight: 800; border-bottom: 1px solid #ccc; border-left: 1px solid #ccc;}
	.table-summary .wrapp .details{padding: 5px; border-left: 1px solid #ccc;}
	.nopadding{padding: 0;}
	
	@media(max-width:500px) {
	.content {padding: 0;}
	.panel-body {padding: 6px;}
	}
	
	.fixed-table-container {height: 500px !important; }
</style>
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>
		&nbsp;
	</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">            
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		<li class="active">
			<a href="#"> Call Track</a>
		</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<!--fourth table start-->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i>  Call Track
					</h3>
				</div>
				<div class="panel-body">
					
					<div class="col-md-12 nopadding" style="" align="right">
						
						<div class="wrapp" >
							<a href="#"><button class="btn btn-default" type="button"  >Generate New #</button></a>
							<a href="manage-numbers"><button class="btn btn-default" type="button"  >Manager Numbers</button></a>
						</div>       
						
					</div>
					<div class="col-md-12 nopadding" style="" align="right">
						
						<div class="wrapp table-summary" >         
							<div class="wrapp" >       
								<div class="title" >Total #'s</div>         
								<div class="details" >12</div>       
							</div>  
							<div class="wrapp" >       
								<div class="title" >Total Calls</div>       
								<div class="details" >55</div>       
							</div> 
							<div class="wrapp" >       
								<div class="title" >Total Minutes</div>       
								<div class="details" >278</div>       
							</div> 	
						</div> 	
					</div>
					
					<table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false" data-show-toggle="true" data-show-columns="true" data-show-export="true" data-detail-view="true" data-detail-formatter="detailFormatter" data-minimum-count-columns="2" data-show-pagination-switch="true" data-pagination="true" data-id-field="id" data-page-list="[10, 20, 40, ALL]" data-show-footer="false">
						<thead>
                            <tr>
                                <th data-field="Create Time" data-sortable="true">Date</th>
                                <th data-field="Campaign" data-sortable="true">From</th>
                                <th data-field="Ad Set Name" data-sortable="true">To</th>
                                <th data-field="Ad Name" data-sortable="true">Type</th>
                                <th data-field="Full Name" data-sortable="true">Status</th>
                                <th data-field="Email" data-sortable="true">Recording</th>
                                <th data-field="Phone #" data-sortable="true">Duration</th>
							</tr>
						</thead>
						<tbody>
                            <tr>
                                <td>04-26-2017  10:10:00 AM</td>
                                <td>(562) 334-1119</td>
                                <td>Facebook Profile</td>
                                <td>Phone</td>
                                <td>Completed</td>
								<td >-</td>
                                <td>8 secs</td>
							</tr>
							<tr>
                                <td>04-26-2017  12:10:00 PM</td>
                                <td>(562) 311-5479</td>
                                <td>English Ad</td>
                                <td>Phone</td>
                                <td>Completed</td>
								<td >-</td>
                                <td>15 secs</td>
							</tr>
							<tr>
                                <td>04-26-2017  1:10:00 PM</td>
                                <td>John, Smith</td>
                                <td>Mix Ads</td>
                                <td>Call</td>
                                <td>No Answer</td>
								<td>-</td>
                                <td>9 secs</td>
							</tr>
							<tr>
                                <td>04-26-2017  12:10:00 AM</td>
                                <td>(732) 983-1149</td>
                                <td>Store Number </td>
                                <td>SMS</td>
                                <td>Completed</td>
								<td >-</td>
                                <td>3 min 15 secs</td>
							</tr>
							
							<tr>
                                <td>04-26-2017  12:10:00 AM</td>
                                <td>(832) 343-1619</td>
                                <td>Store Number </td>
                                <td>Phone</td>
                                <td>Received</td>
								<td >-</td>
                                <td>3 min 15 secs</td>
							</tr>
							
							<tr>
                                <td>04-26-2017  12:10:00 AM</td>
                                <td>(212) 351-1949</td>
                                <td>Client Name  </td>
                                <td>Call</td>
                                <td>Completed</td>
								<td >-</td>
                                <td>1 secs</td>
							</tr>
							
							<tr>
                                <td>04-26-2017  12:10:00 AM</td>
                                <td>Sean, Johnson </td>
                                <td>Client Name  </td>
                                <td>Phone</td>
                                <td>Received</td>
								<td >-</td>
                                <td>3 secs</td>
							</tr>
							
							<tr>
                                <td>04-26-2017  12:10:00 AM</td>
                                <td>(432) 343-1619</td>
                                <td>Campaign Name </td>
                                <td>Phone</td>
                                <td>Completed</td>
								<td >-</td>
                                <td>1 secs</td>
							</tr>
							
							<tr>
                                <td>04-26-2017  12:10:00 AM</td>
                                <td>(832) 343-1619</td>
                                <td>Store Number </td>
                                <td>Phone</td>
                                <td>Received</td>
								<td >-</td>
                                <td>3 min 15 secs</td>
							</tr>
							
							<tr>
                                <td>04-26-2017  12:10:00 AM</td>
                                <td>(212) 351-1949</td>
                                <td>Client Name  </td>
                                <td>Call</td>
                                <td>Completed</td>
								<td >-</td>
                                <td>1 secs</td>
							</tr>
							
							<tr>
                                <td>04-26-2017  12:10:00 AM</td>
                                <td>Sean, Johnson </td>
                                <td>Client Name  </td>
                                <td>Phone</td>
                                <td>Received</td>
								<td >-</td>
                                <td>3 secs</td>
							</tr>
							
							<tr>
                                <td>04-26-2017  12:10:00 AM</td>
                                <td>(432) 343-1619</td>
                                <td>Campaign Name </td>
                                <td>Phone</td>
                                <td>Completed</td>
								<td >-</td>
                                <td>1 secs</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!--fourth table end-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>

<script>
	$( document ).ready(function() {
	    var extra_features = '<button class="btn btn-default" type="button" name="mail" title="Mail" ><i class="fa fa-fw fa-envelope"></i></button><button class="btn btn-default" type="button" name="print" title="Print" ><i class="fa fa-fw fa-print"></i></button>';
		//$(".fixed-table-toolbar .columns").prepend(extra_features);
	});		
</script>

<!-- bootstrap time picker -->
<!-- InputMask -->
<script  type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}" ></script>
<!-- bootstrap time picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/jquerydaterangepicker/js/jquery.daterangepicker.min.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}" ></script>
<script  type="text/javascript" src="{{asset('assets/vendors/timedropper/js/timedropper.js')}}" ></script>
<!-- end of page level js -->
@stop