@extends('layouts/default')

{{-- Page title --}}
@section('title')
Appointments
@parent
@stop

{{-- Page content --}}
@section('content') 


 <!-- global css -->
    
    <link type="text/css" href="http://big.pownder.com/assets/css/app.css" rel="stylesheet"/>
    <!-- end of global css -->
    <!--page level css -->
    <link href="http://big.pownder.com/assets/vendors/bootstrap-calendar/css/calendar.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="http://big.pownder.com/assets/vendors/jquerydaterangepicker/css/daterangepicker.min.css">
   
    <link href="http://big.pownder.com/assets/vendors/iCheck/css/all.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="http://big.pownder.com/assets/css/custom.css">

    <link href="http://big.pownder.com/assets/css/calendar_custom2.css" rel="stylesheet" type="text/css"/>
    <!--end of page level css-->
	
<style>
  .right-side {
    background: #ffffff;
  }

</style>	

<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="#">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		
		<li class="active">
			Appointments
		</li>
	</ol>
</section>

   

<!-- Main content -->
<section class="content p-l-r-15">
   

            <div class="page-header">
                <div class="pull-right form-inline">
                    <div class="btn-group m-t-10">
                        <button class="btn btn-primary" data-calendar-nav="prev">
                            <i class="fa fa-fw fa-angle-double-left" aria-hidden="true"></i> Prev
                        </button>
                        <button class="btn btn-default" data-calendar-nav="today">Today</button>
                        <button class="btn btn-primary" data-calendar-nav="next">Next <i
                                class="fa fa-fw fa-angle-double-right" aria-hidden="true"></i>
                        </button>
                    </div>  
                    <div class="btn-group m-t-10">
                        <button class="btn btn-warning" data-calendar-view="year">Year</button>
                        <button class="btn btn-warning" data-calendar-view="month">Month</button>
                        <button class="btn btn-warning" data-calendar-view="week">Week</button>
                        <button class="btn btn-warning" data-calendar-view="day">Day</button>
                    </div>
                </div>
                <h3></h3>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div id="calendar" class="m-b-25"></div>
                </div>
				
				
				
                <div class="col-md-3">
				   
                    <div class="">
                        <h4>Events List</h4>
                    </div>
                    <ul id="eventlist" class="nav nav-list"></ul>
                    <div class="row m-t-10">
                        <div class="col-md-12">
                            <a href="#" class="btn btn-success btn-block" data-toggle="modal" data-target="#myModal">Create
                                event</a>
                        </div>
                    </div>
					
				    <hr/>
				
                    <div class="row m-t-25">
                        <div class="col-md-12">
                            <H4>Calendar Settings</H4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <label for="first_day">First day of week </label>
                                        <select id="first_day" class="form-control">
                                            <option value="" selected="selected">language-dependant</option>
                                            <option value="2">Sunday</option>
                                            <option value="1">Monday</option>
                                        </select>
                                    </div>
                                    <div class="input-group">
                                        <label for="language">Select Language</label>
                                        <select id="language" class="form-control">
                                            <option value="">default: English-US</option>
                                            <option value="bg-BG">Bulgarian</option>
                                            <option value="nl-NL">Dutch</option>
                                            <option value="fr-FR">French</option>
                                            <option value="de-DE">German</option>
                                            <option value="el-GR">Greek</option>
                                            <option value="hu-HU">Hungarian</option>
                                            <option value="id-ID">Bahasa Indonesia</option>
                                            <option value="it-IT">Italian</option>
                                            <option value="pl-PL">Polish</option>
                                            <option value="pt-BR">Portuguese (Brazil)</option>
                                            <option value="ro-RO">Romania</option>
                                            <option value="es-CO">Spanish (Colombia)</option>
                                            <option value="es-MX">Spanish (Mexico)</option>
                                            <option value="es-ES">Spanish (Spain)</option>
                                            <option value="ru-RU">Russian</option>
                                            <option value="sk-SR">Slovak</option>
                                            <option value="sv-SE">Swedish</option>
                                            <option value="zh-CN">简体中文</option>
                                            <option value="zh-TW">繁體中文</option>
                                            <option value="ko-KR">한국어</option>
                                            <option value="th-TH">Thai (Thailand)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <!--<div class="col-md-12 m-t-10">
                                    <label class="checkbox">
                                        <input type="checkbox" value="#events-modal" id="events-in-modal"> Open events
                                        in modal window
                                    </label>
                                </div>-->
                                <div class="col-md-12 m-t-10">
                                    <label class="checkbox">
                                        <input type="checkbox" id="format-12-hours"> 12 Hour format
                                    </label>
                                </div>
                                <div class="col-md-12 m-t-10">
                                    <label class="checkbox">
                                        <input type="checkbox" id="show_wb" checked> Show week box
                                    </label>
                                </div>
                                <div class="col-md-12 m-t-10">
                                    <label class="checkbox">
                                        <input type="checkbox" id="show_wbn" checked> Show week box number
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
					
					
					
					
                    
                </div>
            </div>
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">
                                <i class="fa fa-plus"></i> Create Event
                            </h4>
                        </div>
                        <form role="form" name="eventform" id="eventform">
                            <div class="modal-body">
                                <div class="input-group">
                                    <label for="new-event">Event Name</label>
                                    <input type="text" id="new-event" class="form-control" placeholder="Event">
                                </div>
                                <div class="input-group">
                                    <label for="eventclass">Event Class</label>
                                    <select name="eventclass" id="eventclass" class="form-control">
                                        <option value="event-important" selected>Important</option>
                                        <option value="event-success">Success</option>
                                        <option value="event-primary">primary</option>
                                        <option value="event-default">Default</option>
                                        <option value="event-info">Info</option>
                                        <option value="event-warning">Warning</option>
                                    </select>
                                </div>
                                <label for="event_url">Event URL</label>
                                <div class="input-group">
                                    <span class="input-group-addon">HTTP://</span>
                                    <input type="text" class="form-control pull-right" id="event_url"
                                           placeholder="Enter The URL related to event"/>
                                </div>
                                <div class="input-group">
                                    <label for="date-range0">Date and Time</label>
                                    <!--<input type="text" class="form-control pull-right" id="date-range0"
                                           placeholder="Select Date Range For Event"/>-->
                                </div>
								<div class='input-group date' id='datetimepicker1'>
									<input type='text' class="form-control" id="date-time0" placeholder="Select Date and timr For Event" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
                                <!-- /input-group -->
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger pull-right" id="close_calendar_event"
                                        data-dismiss="modal">
                                    Close
                                    <i class="fa fa-times"></i>
                                </button>
                                <button type="button" class="btn btn-success pull-left" id="add-new-event">
                                    <i class="fa fa-plus"></i> Add
                                </button>
                                <button type="reset" class="hidden">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="events-modal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3>Event</h3>
                        </div>
                        <div class="modal-body" style="height: 400px">
                        </div>
                        <div class="modal-footer">
                            <a href="#" data-dismiss="modal" class="btn btn-danger">Close</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="right">
                <div id="slim2">
                    <div class="rightsidebar-right">
                        <div class="rightsidebar-right-content">
                            <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                                <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                            </h5>
                            <ul class="list-unstyled margin-none">
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="img/authors/avatar1.jpg" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-primary"></i> Alanis
                                    </a>
                                </li>
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="img/authors/avatar.jpg" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-primary"></i> Rolando
                                    </a>
                                </li>
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="img/authors/avatar2.jpg" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-primary"></i> Marlee
                                    </a>
                                </li>
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="img/authors/avatar3.jpg" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-warning"></i> Marlee
                                    </a>
                                </li>
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="img/authors/avatar4.jpg" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                                    </a>
                                </li>
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="img/authors/avatar5.jpg" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-muted"></i> Cielo
                                    </a>
                                </li>
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="img/authors/avatar7.jpg" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-muted"></i> Charlene
                                    </a>
                                </li>
                            </ul>
                            <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                <i class="fa fa-fw fa-tasks"></i> Tasks
                            </h5>
                            <ul class="list-unstyled m-t-25">
                                <li>
                                    <div>
                                        <p>
                                            <strong>Task 1</strong>
                                            <small class="pull-right text-muted">40% Complete</small>
                                        </p>
                                        <div class="progress progress-xs progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar"
                                                 aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                                 style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <p>
                                            <strong>Task 2</strong>
                                            <small class="pull-right text-muted">20% Complete</small>
                                        </p>
                                        <div class="progress progress-xs progress-striped active">
                                            <div class="progress-bar progress-bar-info" role="progressbar"
                                                 aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                                 style="width: 20%">
                                                <span class="sr-only">20% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <p>
                                            <strong>Task 3</strong>
                                            <small class="pull-right text-muted">60% Complete</small>
                                        </p>
                                        <div class="progress progress-xs progress-striped active">
                                            <div class="progress-bar progress-bar-warning" role="progressbar"
                                                 aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                                 style="width: 60%">
                                                <span class="sr-only">60% Complete (warning)</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <p>
                                            <strong>Task 4</strong>
                                            <small class="pull-right text-muted">80% Complete</small>
                                        </p>
                                        <div class="progress progress-xs progress-striped active">
                                            <div class="progress-bar progress-bar-danger" role="progressbar"
                                                 aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                                 style="width: 80%">
                                                <span class="sr-only">80% Complete (danger)</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                <i class="fa fa-fw fa-group"></i> Recent Activities
                            </h5>
                            <div>
                                <ul class="list-unstyled">
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
     
</section>
<!-- /.content -->




	

<!-- global js -->
<!--<script src="http://big.pownder.com/assets/js/app.js" type="text/javascript"></script>-->
<!-- end of global js -->
<!-- begining of page level js -->
<script src="http://big.pownder.com/assets/vendors/moment/js/moment.min.js" type="text/javascript"></script>
<script src="http://big.pownder.com/assets/vendors/jquerydaterangepicker/js/jquery.daterangepicker.min.js" type="text/javascript"></script>
<script src="http://big.pownder.com/assets/vendors/underscore/js/underscore-min.js" type="text/javascript"></script>
<script src="http://big.pownder.com/assets/vendors/bootstrap-calendar/js/calendar.min.js" type="text/javascript"></script>
<script src="http://big.pownder.com/assets/vendors/iCheck/js/icheck.js" type="text/javascript"></script>



    <!--<link rel="stylesheet" type="text/css" media="screen" href="http://big.pownder.com/assets/date_time/bootstrap.min.css" />-->
	<link href="http://big.pownder.com/assets/date_time/bootstrap-datetimepicker.css" rel="stylesheet">
	<!--<script type="text/javascript" src="http://big.pownder.com/assets/date_time/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="http://big.pownder.com/assets/date_time/bootstrap.min.js"></script>-->
	<script src="http://big.pownder.com/assets/date_time/moment-with-locales.js"></script>
	<script src="http://big.pownder.com/assets/date_time/bootstrap-datetimepicker.js"></script>

    <script type="text/javascript">
    $(function () {
        $('#datetimepicker1').datetimepicker();
	});
    </script>

<script src="http://big.pownder.com/assets/js/custom_js/calendar_custom2.js" type="text/javascript"></script>




@stop