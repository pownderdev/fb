@extends('layouts/default')

{{-- Page title --}}
@section('title')
Invoice
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--start page level css -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
<style>
	optgroup {
    background-color: #fb9f98;
    color: #000;
	}
	.invoicheading {
	font-size:40px; color:#42A5F5;
	}
	.invoiceparagraph{
	font-size: 13px;
	}
	
</style>
<!--end page level css-->
@stop

{{-- Page content --}}
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header gred_2">
	<h1>&nbsp;</h1>
	<ol class="breadcrumb" style="margin-top: -10px;">
		<li>
			<a href="{{ route('/') }}">
				<i class="fa fa-fw fa-home"></i> Dashboard
			</a>
		</li>
		
		<li class="active">
			Invoice
		</li>
	</ol>
</section>

<!-- Main content -->
<section class="content p-l-r-15">
<!--
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-success filterable">
				<div class="panel-heading">
					<h3 class="panel-title">
						<i class="fa fa-fw fa-th-large"></i> Add Invoice
					</h3>
				</div>
				<div class="panel-body">
					<form role="form" id="add_invoice_form">
						{{ csrf_field() }}
						<div class="form-body">
							
							<div class="row">							
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="client_name">Client Name</label>
										<select id="client_name" name="client_name" class="form-control">
											
											<option value="">Select Client Name</option>
											<optgroup label="Own Client"></optgroup>
											@foreach($Clients as $Client)
											<option value="{{ $Client->id }}">{{ $Client->name }}</option>
											@endforeach
											@foreach($Vendors as $Vendor)
											<optgroup label="{{ $Vendor->name }} Client"></optgroup>
											@foreach(VendorHelper::VendorClients($Vendor->id) as $Client)
											<option value="{{ $Client->id }}">{{ $Client->name }}</option>
											@endforeach
											@endforeach
										</select>
										<small class="text-danger animated client_name fadeInUp invoice_add_error"></small>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label class="control-label" for="due_date">Due Date</label>
										<input type="text" name="due_date" id="due_date" class="form-control" placeholder="MM-DD-YYYY"/>
										<small class="text-danger animated due_date fadeInUp invoice_add_error"></small>
									</div>
								</div>
								
							</div>		
							<div class="row">
								<table class="table table-responsive">
									<thead>
										<tr>
											<th>
												Description
											</th>
											<th>
												Quantity
											</th>
											<th>
												Rate
											</th>
											<th>
												Amount
											</th>
											<th>
												
											</th>
										</tr>
									</thead>
									<tr>
										<td>
											<div class="form-group">										
												<textarea class="form-control" rows="3" name="description[]" id="description" placeholder="Description"></textarea>
												<small class="text-danger animated description fadeInUp invoice_add_error"></small>
											</div>
										</td>
										<td>
											<div class="form-group">										
												<input type="text" name="quantity[]" id="quantity" class="form-control" placeholder="Quantity"/>
												<small class="text-danger animated quantity fadeInUp invoice_add_error"></small>
											</div>	
										</td>
										<td>
											<div class="form-group">										
												<input type="text" name="rate[]" id="rate" class="form-control" placeholder="Rate"/>
												<small class="text-danger rate  fadeInUp invoice_add_error"></small>
											</div>	
										</td>
										<td>
											<div class="form-group">										
												<input type="text" name="amount[]" id="amount" class="form-control" placeholder="Amount"/>
												<small class="text-danger animated amount fadeInUp invoice_add_error"></small>
											</div>	
										</td>
										<td>
											<span class='btn btn-success glyphicon glyphicon-plus btnAdd'></span>
											
										</td>
									</tr>
								</table>
								<div class="form-group form-actions">
									<center>
										<button type="submit" class="btn btn-primary addButton" style="color:#fff; background-color: #ffbe18!important;
										border-color: #e4a70c!important;">Submit</button>
										<span id="printpreview" class="btn btn-primary" >Invoice Preview</span>
									</center>
								</div>
							</div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="hidden" id="date" value="<?php echo date("Y-m-d");?>"/>
	<div class="modal fade" id="myModal" role="dialog">

    <div class="col-md-12">
	<div class="col-md-2"></div>
	<div class="col-md-8">

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Invoice</h4>
        </div>
        <div class="modal-body">
		 <div class="row">
		 <div class="col-md-12">
		 <div class="col-md-8">
          <img src="{{asset('img/invoice.png')}}" alt="logo" /><br/>
		  <b class="invoicheading">INVOICE </b><br/>
		  <b style="font-size:28px;">Perfect Enterprises, Inc</b>
		  </div>
		  <div class="col-md-4">
		  <br/><br/>
		  <p class="pull-right">
		  146 W Cypress Unit 102 <br/>
		  Burbank CA 91502
		  </p>
		    <p class="pull-right">
			  (818) 578-9995<br/>
			  www.pownder.com<br/>
			  info@pownder.com
		    </p>
		  </div>
		  </div>
		  </div>
		  <div class="row">
		  <div class="col-md-12" style="margin-top:30px">
		  <div class="col-md-2">
		  <b style="font-size:20px;">Bill To</b>
		  </div>
		  <div class="col-md-3">
		  <p id="client_details"></p>
		  </div>
		  <div class="col-md-5">
		  <p>
		  <b class="pull-right invoiceparagraph">Invoice Number<b><br/>
		  <b class="pull-right invoiceparagraph">Date</b><br/>
		  <b class="pull-right invoiceparagraph">Due Date</b><br/>
		  <b class="pull-right invoiceparagraph">Terms</b>
		  </p>
		  </div>
		  <div class="col-md-2">
		  <p id="invoice_info">
		  </p>
		  </div>
		  </div>
		
		  
        </div>
     
      </div>
      
    </div>
	</div>
	<div class="col-md-2"></div>
	</div>
  </div>-->
  
  <style>
  .invoice-table { font-family: 'Open Sans', sans-serif; }
	 .invoice-table {
	   *max-width:900px;
	   *margin:0 auto;
	   width: 730px;
       float: left;
       
	   
	 background-color:#fff;
     float: left;
     padding: 15px;
     box-shadow: 0px 2px 10px rgba(177, 171, 171, 0.88);
	   
	   
	   
	 }
	 
	 .invoice-option {
		 width: calc(100% - 730px);
		 display: inline-block;
		 float: left;
		 min-height: 50px;
		 padding-right: 25px;
	 }
	  .option-wrapper {
	     padding: 15px;
         background: #fff;
		 margin-bottom: 15px;
	  }
	  
	 .col-100{
	  width:100%;
	 }
	 .col-50{
	  width:50%;
	 }
	 .col-25{
	  width:25%;
	 }
	 .col-33{
	  width:33.33%;
	 }
	 .col-66{
	  width:66.66%;
	 }
	 
	 .col-10{
	  width:10%;
	 }
	 
	 .col-20{
	  width:20%;
	 }
	 .col-15{
	  width:15%;
	 }
	 .col-30{
	  width:30%;
	 }
	 .col-40{
	  width:40%;
	 }
	 
	 .center {text-align:center}
	 .right {text-align:right}
	
	 p{
	   margin:0;
	 }
	 
	 .title {
	     font-weight: 800;
		 }
	table .content {
	     font-weight: normal;
		     padding: 0px;
		 }
     .align-top {
	       vertical-align: top;
	 }
	 .align-bottom {
	       vertical-align: bottom;
	 }
	 
	 
	 .m-t-0 { margin-top:0px;}
	 .m-b-0 { margin-bottom:0px;}
	 .m-l-0 { margin-left:0px;}
	 .m-r-0 { margin-right:0px;}
	 .m-0 { margin : 0; }
	 
	 
	 .p-t-0 { padding-top:0px;}
	 .p-b-0 { padding-bottom:0px;}
	 .p-l-0 { padding-left:0px;}
	 .p-r-0 { padding-right:0px;}
	 .p-0 { padding : 0px; }
	 .p-2 { padding : 2px; }
	 
	 .border-none { 
	   border:none;
	 }
	 .border-left-none { 
	   border-left:0px;
	 }
	 .border-right-none { 
	   border-right:0px;
	 }
	 .border-top-none { 
	   border-top:0px;
	 }
	 .border-bottm-none { 
	   border-bottom:0px;
	 }
	 
	 
	 
	 table {
			*border-collapse: collapse;
		}

	table, td, th {
		   *border: 1px solid black;
	    }
	td, th {
		   *padding: 4px 7px;
		   padding: 0px 5px;
	    }
		
	.one-line {white-space: nowrap;}
  
  
    .panel-heading {    
	   color: #fff;
       background-color: #1dd4cb;
       border-color: #00c9be;
	   margin-bottom: 20px;
	}
	.invoice-logo {
		    max-width: 70px;
	}
	
	.invoice-header-bottom {
		    padding: 12px 15px;
            background: #f5f5f5;
            margin: 0 -15px;
	}
	.invoice-title-section {
		    padding: 5px 15px;
            background: #008fac;
            margin: 15px -15px;
			color:#fff;
	}
	
	.main-title {
		color: #008fac;
	}
	.sub-title {
		
	}
	.invoice-details-section td {
		padding-top:5px;
		padding-bottom:5px;
	}
	.invoice-details-section tr.border-b {
		border-bottom:1px solid #efefef;
	}
	.item-details-section {
		min-height:120px;
	}
	.final-due {
		    background: #ea9c18;
            color: #fff;
	}
	.add-btn {
		background: #ffb900;
        color: #fff!important;
	}  
	.items-section {
		min-height : 150px;
	}
	.add_existing {
		float: right;
		border: 1px solid #f9f9f9;
		background: #ffb900;
		color: #fff!important;
		padding: 0;
		width: 25px;
		height: 25px;
		margin-top: 5px;
		font-size: 20px;
		line-height: 0;
		box-shadow: 0 0 7px rgba(35, 34, 34, 0.48);
	}
	  /*
	.item-details-section .edit:before , .item-details-section .delete:after {
            font: normal normal normal 14px/1 FontAwesome;
            position: absolute;
            width: 21px;
            height: 21px;
			content: "";
			padding: 4px 6px;
           font-size: 12px;
           cursor: pointer;
		   color:#fff;
		   z-index: 9999;
	}
	
	.item-details-section .delete:after {
		    content: "\f00d";
            background: #ea9c18;
			left: 21px;
	}
	.item-details-section .edit:before {
	        content: "\f040";
            background: #008fac;
	}
	*/
	
	.item-details-section .delete {
            background: #ea9c18;
			width: 21px;
            height: 21px;
			padding: 4px 6px;
           font-size: 12px;
           cursor: pointer;
		   color:#fff;
			
	}
	.item-details-section .edit {
	        
            background: #008fac;
			width: 21px;
            height: 21px;
			padding: 4px 6px;
           font-size: 12px;
           cursor: pointer;
		   color:#fff;
	}
	
	.i_add_button{
		padding: 4px 12px;
       background: #ea9c18;
      color: #fff;
	  margin-top: 3px;
	}
	
	.item-list-table thead{
		background: #008fac;
		color: #fff;
	}
	.item-list-table td{
		padding: 3px 8px;
	}
	.item-list-table tr + tr {
	  border-top: 1px solid #efefef;
	}
	.modal-backdrop.in {
    opacity: .5;
	}
	
	@media(max-width:1300px){
		
	 .invoice-table {
	   width: 650px;
	 }
	 .invoice-option {
		 width: calc(100% - 650px);
	 }
	 .invoice-option {    padding-right: 5px;  }
	 
    }
	
	@media(max-width:991px){
		
	 .invoice-table {
	   width: 650px;
	 }
	 .invoice-option {
		 width: 100%;
	 }
	 .invoice-option {    padding-right: 0px;  }
	 .main_wrapper {
				display: inline-block;
				width: 100%;
	 }
	 
	 .invoice-table {
         float: initial;
		 margin: 0 auto;
		}
	 
    }
	
	@media(max-width:767px){
		.main_wrapper {
				overflow-x: scroll;
		}
		
	}
  </style>
    <div class="row">
	        <div class="panel-heading">
			    <h3 class="panel-title">
					<i class="fa fa-fw fa-th-large"></i> Add Invoice
				</h3>
		    </div>
	
	    <section class="invoice-option">
			<div class="option-wrapper">
			                        <!--
									<div class="form-group">
										<label class="control-label" for="client_name">Invoice Logo</label>
										<input type="file" name="" id="invoice-logo">
										<small class="text-danger animated client_name fadeInUp invoice_add_error"></small>
									</div>
									
									<div class="form-group">
										<label class="control-label" for="client_name">Bill TO:</label>
										<select id="client_name" name="client_name" class="form-control">
											
											<option value="">Select Client Name</option>
											<option value="">Client Name1</option>
											<option value="">Client Name2</option>
											<option value="">Client Name3</option>
											<option value="">Client Name4</option>
											
										</select>
										<small class="text-danger animated client_name fadeInUp invoice_add_error"></small>
									</div>
									-->
								
									
									<div class="panel-heading  gred_2" style="padding: 8px 15px;">
										<h3 class="panel-title">
										   Invoice Details
										</h3>
									</div>
									
									<div class="form-group">
										<label class="control-label" for="bill_to">Bill To:</label>
										<textarea type="text" name="" class="form-control" id="bill_to"></textarea>
										<small class="text-danger animated bill_to fadeInUp invoice_add_error"></small>
									</div>
									
									<div class="form-group">
										<label class="control-label" for="due_date">Due Date</label>
										<input type="text" name="due_date" id="due_date" class="form-control" placeholder="MM-DD-YYYY"/>
										<small class="text-danger animated due_date fadeInUp invoice_add_error"></small>
									</div>
									<div class="form-group">
										<label class="control-label" for="recurring_date">Recurring Date</label>
										<input type="text" name="recurring_date" id="recurring_date" class="form-control" placeholder="MM-DD-YYYY"/>
										<small class="text-danger animated recurring_date fadeInUp invoice_add_error"></small>
									</div>
									<div class="form-group">
										<label class="control-label" for="tax">Tax(%)</label>
										<input type="number" name="tax" id="tax" class="form-control" placeholder="%" min="0" />
										<small class="text-danger animated tax fadeInUp invoice_add_error"></small>
									</div>
			
			
			    <hr>	
			
			    <div class="panel-heading  gred_2" style="padding: 0px 15px;">
				  <h4 style="float:left; margin: 9px 0;">Add Items</h4> <input type="submit" name="" value="+" class="btn form-control add_existing" id="add_existing" data-toggle="modal" data-target="#myModal">
				  <div class="clear" style=" clear: both;"></div>
				</div>
				
								
			    
				<div class="row">
				    <div class="col-md-12">
					  <div class="form-group">
					    <label class="control-label" for="bill_to">Description</label>
					     <textarea type="text" name="" class="form-control a_des" id="a_des_"></textarea>
					  </div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
							<label class="control-label" for="bill_to">Rate</label>
							<input type="number" name="" class="form-control a_price" id="a_price_" min="1">
						</div>
					</div>
					<div class="col-md-5 col-sm-8 col-xs-7">
						<div class="form-group">
							<label class="control-label" for="bill_to">Qnt.</label>
							<input type="number" name="" class="form-control a_quantity" id="a_quantity_" min="1">
						</div>
					</div>
					<div class="col-md-2 col-sm-4 col-xs-5">
						<div class="form-group">
							<label class="control-label" for="bill_to"><br></label>
							<input type="submit" name="" value=">" class="btn form-control add-btn" id="addto" data_id="">
						</div>
					</div>
                </div>					
			</div>
			
			
			
		</section>  
	
		<section class="main_wrapper ">
			<!--header area-->
			<div class="invoice-table">
			<table class="col-100">
				<thead>
				</thead>
				<tbody>
				  
				   <tr>
					  <td class="col-50" colspan="2">
						<img class="invoice-logo" src="http://big.pownder.com/assets/img/favicon.png">
						<h2 class="title m-t-10 m-b-10 main-title">Invoice</h2>
						<h4 class="title m-t-10 m-b-10 sub-title">Pownder<sup>TM<sup></h4>
					  </td>
					  <td class="col-10" >
						
					  </td>
					  <td class="col-40 align-bottom" colspan="2">
					    <p class="m-0 right " style="min-height: 50px">
						Permanent Address
						</p>
					  
					  </td>
				   </tr>
				</tbody>
			</table>
			<!--header bottom area-->
			<div class="invoice-header-bottom">
			<table class="col-100">
				<thead>
				</thead>
				<tbody>
				  <tr>
					  <td class="col-10 align-top" colspan="1" rowspan="4" >
						<p class="m-0 title">
						Bill To:
						</p>
					  </td>
					  <td class="col-40 align-top" colspan="2" rowspan="4">
						<p class="content align-top" id="bill_to_field">
						Name , Address
						</p>
					  </td>
					  
					  <td class="col-20 align-top" >
						<p class="title align-top right">Invoice Num</p>
					  </td>
					  <td class="col-20 align-top" colspan="2">
						<p class="content align-top right">PTMI_<span id="time"></span></p>
					  </td>
				   </tr>
				   
				   <tr>
					  <td class="col-20 align-top" >
						<p class="title align-top right">Date</p>
					  </td>
					  <td class="col-20 align-top" colspan="2">
						<p class="content align-top right" id="date_field">01/05/2017</p>
					  </td>
				   </tr>
				    <tr>
					  <td class="col-20 align-top" >
						<p class="title align-top right" >Due Date</p>
					  </td>
					  <td class="col-20 align-top" colspan="2">
						<p class="content align-top right" id="due_date_field" ></p>
					  </td>
				   </tr>
				    <tr>
					  <td class="col-20 align-top">
						<p class="title align-top right">Recurring Date</p>
					  </td>
					  <td class="col-20 align-top" colspan="2">
						<p class="content align-top right" id="recurring_date_field"></p>
					  </td>
				   </tr>
				   
				</tbody>
			</table>
			</div>
			
			<!--header title area-->
			<div class="invoice-title-section">
			<table class="col-100">
				<tbody>
				   <tr>
					  <td class="col-50 align-top" colspan="2">
						<p class="title align-top left">Description</p>
					  </td>
					  <td class="col-15 align-top" >
						<p class="title align-top right">Quantity</p>
					  </td>
					  <td class="col-15 align-top" >
						<p class="title align-top right">Rate</p>
					  </td>
					  <td class="col-20 align-top" >
						<p class="title align-top right">Amount</p>
					  </td>
				   </tr>
			    </tbody>
			</table>
			</div>
			
			<!--header content area-->
			<div class="invoice-details-section">
			<div class="item-details-section">
				<table class="col-100" style="position: relative;">
					<tbody class="append_items">
					
					  <!--
					   <tr class="border-b">
						  <td class="col-50 align-top" colspan="2">
							<p class="content align-top left">R K Transport & Constructions Ltd., </p>
						  </td>
						  <td class="col-15 align-top" >
							<p class="content align-top right">1</p>
						  </td>
						  <td class="col-15 align-top" >
							<p class="content align-top right">$600</p>
						  </td>
						  <td class="col-20 align-top" >
							<p class="content align-top right">$600</p>
						  </td>
					   </tr>
					 
					   -->
					   
				
					</tbody>
				</table>
			</div>
            <table class="col-100">
				<tbody>			
				   <!--bill section-->
				   <tr>
					  <td class="col-60 align-top" colspan="3">
					  </td>
					  <td class="col-20 align-top">
						<p class="title align-top right">Subtotal </p>
					  </td>
					  <td class="col-20 align-top">
						<p class="title align-top right">$740 </p>
					  </td>
				   </tr>
				   <tr>
					  <td class="col-60 align-top" colspan="3">
					  </td>
					  <td class="col-20 align-top">
						<p class="title align-top right">Tax(<span id="tax_field"></span>%) </p>
					  </td>
					  <td class="col-20 align-top">
						<p class="title align-top right">$5.92</p>
					  </td>
				   </tr>
				    <tr>
					  <td class="col-60 align-top" colspan="3">
					  </td>
					  <td class="col-20 align-top">
						<p class="title align-top right">Total </p>
					  </td>
					  <td class="col-20 align-top">
						<p class="title align-top right">$745.92</p>
					  </td>
				   </tr>
				   <tr>
					  <td class="col-60 align-top" colspan="3">
					  </td>
					  <td class="col-20 align-top">
						<p class="title align-top right">Paid </p>
					  </td>
					  <td class="col-20 align-top">
						<p class="title align-top right">$0</p>
					  </td>
				   </tr>
				   <tr>
					  <td class="col-60 align-top" colspan="3">
					  </td>
					  <td class="col-20 align-top final-due">
						<p class="title align-top right">Balance Due </p>
					  </td>
					  <td class="col-20 align-top final-due">
						<p class="title align-top right">$745.92</p>
					  </td>
				   </tr>
				   
				   
			    </tbody>
			</table>
			
			<br><br>
			<br><br>
			
			</div>
			
			
			</div>
	    </section>
		
		
		
		
    </div>
  
</section>


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header gred_2">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:#fff;">Item List</h4>
      </div>
      <div class="modal-body">
         <div class="table-responsive">
		     <table class="item-list-table col-100">
			    <thead>
					<tr>
						<td class="col-50 title">
						  Description
						</td>
						<td class="col-20 title">
						  Price
						</td>
						<td class="col-20 title">
						  Quantity
						</td>
						<td class="col-10">
						</td>
					</tr>
				</thead>
			    <tbody>
					<tr class="" id="item1">
						<td class="i_des">saefcsa scdsc</td>
						<td class="i_qnt">10</td>
						<td>$<span class="i_rate">1000</span></td>
						<td><button class="btn i_add_button i_add" id="">Add</button></td>
					</tr>
					<tr class="" id="item2">
						<td class="i_des">saefcsa </td>
						<td class="i_qnt">10</td>
						<td>$<span class="i_rate">1000</span></td>
						<td><button class="btn i_add_button i_add" id="">Add</button></td>
					</tr>
				</tbody>
			 </table>
	     </div>
      </div>
      <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal" style=" background: #ea9c18; color:#fff;">Close</button>
      </div>
    </div>

  </div>
</div>





<!-- /.content -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')

<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/tableExport.jquery.plugin/tableExport.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
 <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<!-- date-range-picker -->
<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}"></script>

<script>
	$(document).ready(function(){
		
		
	   
	   
		$('table').on('click', '.btnAdd', function(){
			
			var itemIndex = $(this).closest('tr').index();    
			var tr = "<tr><td><textarea class='form-control' rows='3' name='description[]' id=description_"+itemIndex+" placeholder='Description'></textarea></td><td><input type='text' name='quantity[]' id='quantity' class='form-control' placeholder='Quantity'></input></td><td><input type='text' name='rate[]' id='rate' class='form-control' placeholder='Rate'></input></td><td><input type='text' name='amount[]' id='amount' class='form-control' placeholder='Amount'></input></td><td><span class='btn btn-success glyphicon glyphicon-plus btnAdd' ></span></td></tr>";
			
			$(this).closest('table').append(tr);
			//$(this).attr('value', 'Delete');
			$(this).toggleClass('btnDelete btn btn-danger glyphicon glyphicon-minus').toggleClass('btn btn-success glyphicon glyphicon-plus btnAdd');
			}).on('click', '.btnDelete', function(){
			//alert($(this).closest('tr').index());
			$(this).closest('tr').remove();
		});
		
		$("#add_invoice_form").on("submit", function( event ) { 
		event.preventDefault();
			$(".addButton").prop('disabled', true);
			$('.invoice_add_error').html('');
			var data=$(this).serialize();
			$.ajax({
				url: "{{ route('invoice_add') }}",
				type:'POST',
				data: data,
				success: function(data) {
					if(data=='success')
					{
						swal('Success!', 'Invoice added successfully.', 'success');
						
					}
					else
					{
						swal('Error!', data, 'error');
						$(".addButton").prop('disabled', false);
					}
					
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					$.each( XMLHttpRequest.responseJSON, function( key, value ) {
						$("."+key).html(value);
					});
					$(".addButton").prop('disabled', false);
				}       
			});
		
		});
		
		$('#client_name').on('change', function() {
			var cleint_id=$(this).val();
			
			$.get("{{ route('client_info') }}",
			{
				cleint_id: cleint_id
			},
			function(data){
			
				if(data.length==1)
				{
					$('#client_details').html(data[0].name +",<br/>"+ data[0].address +", "+ data[0].zip);
					
				}
				
			});
		});
		$('#printpreview').click(function(){
		   
		    var client_id = $('#client_name').val();
			var date = $('#date').val();
			var due_date = $('#due_date').val();
            
			$('#invoice_info').html(1+"<br/>"+date+"<br/>"+due_date);
			if(due_date == ''){
			  alert('Please Enter Due Date First');
			  return false;
				}
				
			if(client_id == ''){
				alert('Please Select Client First');
				return false;
				}
				if(client_id != '' && due_date != ''){
					
			     $('#myModal').modal('toggle');
				}
			});
			
			
			
			
			
			
			
		/*****************invoice section*****************/	
		
		
		$( "#due_date" ).datepicker({
		  dateFormat: 'mm-dd-yy',
          changeMonth: true,
          changeYear: true,
		  
        });
	   	$( "#recurring_date" ).datepicker({
		  dateFormat: 'mm-dd-yy',
          changeMonth: true,
          changeYear: true,
		  
        });
	   	  
		$( "#due_date" ).blur(function() {
			setTimeout(function(){ 
			  var due_date = $("#due_date").val()
              //alert(due_date);
			  document.getElementById("due_date_field").innerHTML = due_date;
			}, 500);
        });
		$( "#recurring_date" ).blur(function() {
            setTimeout(function(){ 
			  var recurring_date = $("#recurring_date").val()
              //alert(recurring_date);
			  document.getElementById("recurring_date_field").innerHTML = recurring_date;
			}, 500);
			
        });
		
		$( "#bill_to" ).blur(function() {
			setTimeout(function(){ 
			  var bill_to = $("#bill_to").val()
              //alert(due_date);
			  document.getElementById("bill_to_field").innerHTML = bill_to;
			}, 500);
        });
		$( "#tax" ).blur(function() {
			setTimeout(function(){ 
			  var tax = $("#tax").val()
              //alert(tax);
			  document.getElementById("tax_field").innerHTML = tax;
			}, 500);
        });
		
		var d = new Date();
        var day = d.getDate();
		if( day < 10 ){ var  dd = '0' + day; }else { var dd = day; }
		var month = d.getMonth() + 1;
		if( month < 10 ){ var  mm = '0' + month; }else { var mm = month; }
		var yy = d.getFullYear();
		document.getElementById("date_field").innerHTML = mm +'-'+ dd +'-'+ yy;	
		  
		var d = new Date();
        var id = d.getTime();  
		document.getElementById("time").innerHTML = id;	
		
		/**************add item**************/
		
		$('#addto').click(function(){
		   
		    var a_des = $('.a_des').val();
			var a_rate = $('.a_price').val();
			var a_quantity = $('.a_quantity').val();
			
		    var count = $('.append_items').children().length + 1;
            
			//alert(count);
			//alert(a_des);
			//alert(a_rate);
			//alert(a_quantity);
			   //$('#invoice_info').html(1+"<br/>"+date+"<br/>"+due_date);
			   
			   if(a_des !="" && a_rate !="" && a_quantity !="" ){
			       //alert('ok');
				   var item = '<tr class="border-b" id="'+ count +'"><td class="col-10 align-top item-option" colspan=""><i class="fa fa-times delete" ></i><i class="fa fa-pencil edit"></i></td><td class="col-60 align-top" colspan="2"><p class="content align-top left des"> '+ a_des +' </p></td><td class="col-10 align-top"><p class="content align-top right qnt">'+ a_quantity +'</p></td><td class="col-15 align-top"><p class="content align-top right "> $<span class="rate">'+ a_rate +'</span></p></td><td class="col-20 align-top"><p class="content align-top right item_total">$<span>'+ a_rate * a_quantity +'</span></p></td></tr>';
				   $('.append_items').append(item);
				   
				   var a_des = $('.a_des').val("");
			       var a_rate = $('.a_price').val("");
			       var a_quantity = $('.a_quantity').val("");
				   
				}else{
					if( a_des =="" ) {
						alert('Please Enter Description');
					}
					if( a_rate =="" ) {
						alert('Please Enter Rate');
					}
					if(a_quantity =="" ) {
						alert('Please Enter Quantity');
					}
				}
				
			    
		});
		
		
		
		
		//  edit form invoice
		$(document).on('click', '.edit', function(){
			var e = $(this);
			var e_id = e.parent().parent().attr("id");
			
			var get_id = "#"+e_id;
			var get_des = $(get_id).find(".des").text();
			var get_qnt = $(get_id).find(".qnt").text();
			var get_rate = $(get_id).find(".rate").text();
			//alert(get_des);
			
			$(".a_des").val(get_des);
			$(".a_quantity").val(get_qnt);
			$(".a_price").val(get_rate);
			$("#addto").attr("data_id",e_id);
			
			e.parent().parent().hide();
			
		});
		
		// delete item from invoice
		$(document).on('click', '.delete', function(){
			var e = $(this);
			var e_id = e.parent().parent().hide();
		});
		
		//add item from database
		$(document).on('click', '.i_add', function(){
			var e = $(this);
			var e_id = e.parent().parent().attr("id");
			//alert(e_id);
			var get_id = "#"+e_id;
			var get_des = $(get_id).find(".i_des").text();
			var get_qnt = $(get_id).find(".i_qnt").text();
			var get_rate = $(get_id).find(".i_rate").text();
			//alert(get_des);
			
			
			$(".a_des").val(get_des);
			$(".a_quantity").val(get_qnt);
			$(".a_price").val(get_rate);
			$("#addto").attr("data_id",e_id);
		});
		
	});
</script>
@stop