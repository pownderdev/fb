@if(Session::has('alert_danger'))
<div class="row">
    <div class="col-lg-12"><br />
		<div class="alert alert-danger alert-dismissible fade in">
			<strong><i class="fa fa-times-circle"></i></strong> <?php echo  Session::pull('alert_danger') ?> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
		</div>
	</div>
</div>
@elseif(isset($alert_danger))
<div class="row">
    <div class="col-lg-12"><br />    
		<div class="alert alert-danger alert-dismissible fade in">
			<strong><i class="fa fa-times-circle"></i></strong> <?php echo  $alert_danger ?> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
		</div>
	</div>
</div>
@elseif(Session::has('alert_success'))
<div class="row">
    <div class="col-lg-12"><br />
		<div class="alert alert-success alert-dismissible fade in">
			<strong><i class="fa fa-check-circle"></i></strong> <?php echo  Session::pull('alert_success') ?> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
		</div>
	</div>
</div>
@elseif(isset($alert_success))
<div class="row">
    <div class="col-lg-12"><br />    
		<div class="alert alert-success alert-dismissible fade in">
			<strong><i class="fa fa-check-circle"></i></strong> <?php echo  $alert_success ?> <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
		</div>
	</div>
</div>      
@endif