<!DOCTYPE html>
<html>
	
	<head>
		<title>::User Reset Password::</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}"/>
		<!-- Bootstrap -->
		<!-- global level css -->
		<link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
		<link href="{{('assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css"/>
		<!-- end of global css-->
		<!-- page level styles-->
		<link href="{{asset('assets/vendors/iCheck/css/all.css')}}" rel="stylesheet">
		<link href="{{asset('assets/vendors/bootstrapvalidator/css/bootstrapValidator.min.css')}}" rel="stylesheet"/>
		<link href="{{asset('assets/css/forgot_password.css')}}" rel="stylesheet">
		<!-- end of page level styles-->
		<style>
			body {
			background: url(img/lbg-11.jpg);
			background-size:cover;
			background-position:center center;
			}
		</style>
	</head>
	
	<body>
		<div class="preloader">
			<div class="loader_img"><img src="{{asset('assets/img/loader.gif')}}" alt="loading..." height="64" width="64"></div>
		</div>
		<?php
			$errorarr=$errors;   
			function showerrormsg($key,$errors)
			{    
				$msg='';
				if(count($errors->get($key))>0)
				{ 
					$msg.='';
					foreach($errors->get($key) as $error)
					{
						if($key=="alert_success")
						{
							$msg.=$error;
						}
						else
						{
							$msg.='<small class="help-block text-danger animated fadeInUp"  >'.$error.'</small>' ; 
						}
					}
				} 
				return $msg;     
			}
		?>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 box animated fadeInUp">
					<div class="text-center"><img src="{{asset('img/logo-s-b.png')}}" alt="coreplus logo" style="width:200px;"></div>
					<h3 class="text-center">Reset Password
					</h3>
					<p class="text-center">
						<small>Enter your new Password</small>
					</p>            
					
					<form action="{{route('password_reset')}}" method="POST" id="reset_password" >
						{{ csrf_field() }}
						<input type="hidden" name="email" value="{{ $email }}" />
						<input type="hidden" name="token" value="{{ $token }}" /> 
						<div class="form-group">
							<input value="{{ old('password') }}" type="password" class="form-control" name="password"  placeholder="Password" >
						</div>
						<div class="form-group">
							<br />
							<input type="password" class="form-control" name="password_confirmation"  placeholder="Confirm Password">
							<?php echo showerrormsg('password',$errorarr); ?>
							<?php echo showerrormsg('loginfailed',$errorarr); ?>
						</div>
						<button type="submit"  class="btn btn-primary btn-block submit-btn">
							Reset Your Password 
						</button>
					</form>
					
				</div>
			</div>
		</div>
		<!-- page level js -->
		<script src="{{asset('assets/js/jquery.min.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>
		<script type="text/javascript" src="{{asset('assets/vendors/iCheck/js/icheck.js')}}"></script>
		<script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>
		<script src="{{asset('assets/js/custom_js/forgot_password.js')}}" type="text/javascript"></script>
		<!-- end of page level js -->
	</body>
	
</html>
