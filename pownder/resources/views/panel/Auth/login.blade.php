<!DOCTYPE html>
<html>
	<head>
		<title>::Admin Login::</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}" />
		<!-- Bootstrap -->
		<!-- global css -->
		<link href="{{asset('assets/css/app.css')}}" rel="stylesheet">
		<!-- end of global css -->
		<!--page level css -->
		<link href="{{asset('assets/css/theme.css')}}" rel="stylesheet">
		<link href="{{asset('assets/css/admin-forms.css')}}" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom.css')}}">
		<link href="{{asset('assets/css/login2.css')}}" rel="stylesheet">
		<!--end page level css-->
		<style>
			.help-block {
			color: #fa5a46  !important;
			}
		</style>
		<style>
            .date-loader{
            z-index: 99999999;
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            width: 100%;
            height: 100%;
            background: rgba(238, 238, 238, 0.45);
            overflow: hidden;
            text-align: center;
            display: none;
			} 
			.date-preloader {
            display: inline-block;
            position: relative;
            width: 50px;
            height: 50px;
            -ms-transform: skew(20deg); /* IE 9 */
            -webkit-transform: skew(20deg); /* Safari */
            transform: skew(20deg);
			}
			
		</style>
	</head>
	
	<body class="external-page external-alt sb-l-c sb-r-c onload-check bg-slider">
		<div class="preloader">
			<div class="loader_img"><img src="{{asset('assets/img/loader.gif')}}" alt="loading..." height="64" width="64"></div>
		</div>
		<div class="date-loader" id="date-loader">
			<div style="position: relative;top: calc(50% - 30px);">
				<div class="date-preloader">
					<i class="fa fa-spinner fa-spin fa-pulse fa-4x" style="color: #428BCA;"></i>
				</div>
				<p id="loading-msg" style="font-size: 13px;margin-top: 10px;font-weight: bold;color: #444;">please wait..</p>
			</div>
		</div>
		<?php
			$errorarr=$errors;   
			function showerrormsg($key,$errors)
			{    
				$msg='';
				if(count($errors->get($key))>0)
				{   
					$msg.='';
					foreach($errors->get($key) as $error)
					{
						if($key=="alert_success")
						{
							$msg.='<div class="alert alert-success alert-dismissable fade in"><i class="fa fa-check-square" style="padding-right: 10px;"></i> '.$error.' <i data-dismiss="alert" class="fa fa-close close pull-right"></i> </div>';
						}
						else
						{
							$msg.='<div class="alert alert-danger alert-dismissable fade in" ><i class="fa fa-exclamation-triangle" style="padding-right: 10px;"></i> '.$error.' <i data-dismiss="alert" class="fa fa-close close pull-right"></i> </div>';
						}
					}
				}           
				return $msg;     
			}
		?>
		<div class="container">
			<div class="row " id="form-login">
				<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 login-content">
					<div class="row">
						<div class="col-md-12">
							<div class="header">
								<h2 class="text-center">
									<img src="{{asset('img/logo-f.png')}}" alt="logo" style="max-width:250px;">
								</h2>
							</div>
						</div>
					</div>
					<div id="main" class="animated fadeIn" style="margin-bottom: 15px;">
						<section id="content_wrapper">
							<form class="form-horizontal" action="{{route('submit-login')}}" id="authentication" method="post">
								<!-- CSRF Token -->
								{{ csrf_field() }}
								<div class="loading login_validation" style="display:none">Loading&#8230;</div>
								<section id="content" class="mn p0 pn">
									<div class="admin-form theme-info" id="login" style="margin-top:-4%">
										<div class="panel mt20 mb20">
											<div class="panel-body bg-white p25 pb15">
												<div class="section row">
													<div class="col-md-12">
														<a href="https://www.facebook.com/v2.9/dialog/oauth?client_id=246735105809032&state=481439cd8a84aebd6236c6fb0d4128c1&response_type=code&sdk=php-sdk-5.5.0&redirect_uri=http%3A%2F%2Fbig.pownder.com%2Ffblogin%2FuserCallback.php&scope=email" class="button btn-social facebook span-left btn-block text-center" style="color: #fff;width: 100%;">
															<span><i class="fa fa-facebook"></i></span>
															Login with Facebook
														</a>
													</div>
												</div>
												
												<div class="section-divider mv30">
													<span>OR</span>
												</div>
												
												<div class="section">
													<label for="username" class="field-label text-muted fs18 mb10">Email</label>
													<label for="username" class="field prepend-icon">
														<input type="text" name="username" id="username" class="gui-input" placeholder="Type your email" value="{{ Cookie::get('username') }}">
														<label for="username" class="field-icon">
															<i class="fa fa-user"></i>
														</label>
													</label>
													<small class="text-danger animated username fadeInUp login_validate"></small>
													<?php echo showerrormsg('username',$errorarr); ?>
												</div>
												<div class="section">
													<label for="loginPassword" class="field-label text-muted fs18 mb10">Password</label>
													<label for="password" class="field prepend-icon">
														<input type="password" name="password" id="password" class="gui-input" placeholder="Type your password"  value="{{ Cookie::get('password') }}">
														<label for="password" class="field-icon">
															<i class="fa fa-lock"></i>
														</label>
													</label>
													<small class="text-danger animated password fadeInUp login_validate"></small>
													<?php echo showerrormsg('password',$errorarr); ?>
												</div>
											</div>
											<div class="panel-footer clearfix bg-white">
												<button type="submit" onClick="return validate();" class="button btn-primary mr10 pull-right">Login</button>
												<button type="submit" class="button btn-primary mr10 pull-right addSubmit" style="display:none">Login</button>
												<label class="switch ib switch-primary mt10">
													<input type="checkbox" name="remember_me" id="remember_me" @if(Cookie::get('password') != '') {{ 'checked' }} @endif>
													<label for="remember_me" data-on="YES" data-off="NO"></label>
													<span>Remember me</span>
												</label>
												<?php echo showerrormsg('loginfailed',$errorarr); ?>
												<?php echo showerrormsg('alert_success',$errorarr); ?>
											</div>
										</div>
									</div>
								</section>
							</form>
						</section>
						<div class="login-links">
							<p>
								Lost password? <a href="{{route('forgot-password')}}" class="active">Click here</a>.                    
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- global js -->
	<script src="{{asset('assets/js/jquery.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/js/bootstrap.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/js/backstretch.js')}}"></script>
	<!-- end of global js -->
	<!-- page level js -->
	<!--<script src="{{asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js')}}" type="text/javascript"></script>-->
	<script type="text/javascript" src="{{asset('assets/js/custom_js/login.js')}}"></script>
	<!-- end of page level js -->
	<script>
		function ValidateEmail(email) {
			var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			return expr.test(email);
		};
		
		function validate()
		{
			$('.login_validate').html('');
			var username = $('#username').val();
			var password = $('#password').val();
			var validate = 'Pass';
			
			if(username=='')
			{
				validate = 'Fail';
				$('.username').html('The email field is required');
				//return false;
			}
			else
			{
				if (!ValidateEmail(username)) 
				{
					validate = 'Fail';
					$('.username').html('Please enter valid email format');
					//return false;
				}
			}
			
			if(password=='')
			{
				validate = 'Fail';
				$('.password').html('The password field is required');
				//return false;
			}
			
			if(validate == 'Fail')
			{
				return false;
			}
		}
	</script>
</body>
</html>