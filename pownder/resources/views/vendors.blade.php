@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Vendors list
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/bootstrap-table/css/bootstrap-table.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_css/bootstrap_tables.css')}}">
	
	
	<!--page level css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/daterangepicker/css/daterangepicker.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/jquerydaterangepicker/css/daterangepicker.min.css')}}">
    <link rel="stylesheet" type="text/css" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
	
	
	<!--<link href="{{asset('assets/css/menubarfold.css')}}" rel="stylesheet">-->
	
	<style>
	  button[name=paginationSwitch] {display:none;}
	  button[name=toggle] {display:none;}
	  
	  .export.btn-group {
		  display:none!important;
	  }
	  .date_range {
		         margin: 1% 0;
                 margin-top: 10px;
                 float: right;
                 width: 40%;
	  }
	  .fixed-table-toolbar {width:60%;}
      .fixed-table-toolbar .search {width: Calc( 100% - 187px );}
	  
	  
	  @media screen and (max-width: 767px) {
		  .date_range {
				float: left;
				width: 98%;
			}
			.fixed-table-toolbar {
				width: 100%;
			}
			.fixed-table-toolbar {width:100%;}
            .fixed-table-toolbar .search {width: Calc( 100% );}
       }
	</style>
	
    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <!--<h1>
            Vendors list
        </h1>-->
        <ol class="breadcrumb">
            <li>
                <a href="index">
                    <i class="fa fa-fw fa-home"></i> Dashboard
                </a>
            </li>
            <li class="active">
                Vendors list
            </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        
		
        <!--fourth table start-->
        <div class="row">
		
		                        
		
		
            <div class="col-lg-12">
                <div class="panel panel-success filterable">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-fw fa-th-large"></i> Vendors List
                        </h3>
                    </div>
                    <div class="panel-body">
					
					    <div class="form-group date_range" style="">
                                    
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-fw fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right " id="date-range0"
                                               placeholder="MM-DD-YYYY to MM-DD-YYYY"  />
                                    </div>
                                    
                        </div>
					
					
					
                        <table id="table4" data-toolbar="#toolbar" data-search="true" data-show-refresh="false"
                               data-show-toggle="true" data-show-columns="true" data-show-export="true"
                               data-detail-view="true" data-detail-formatter="detailFormatter"
                               data-minimum-count-columns="2" data-show-pagination-switch="true"
                               data-pagination="true" data-id-field="id" data-page-list="[10, 20,40,ALL]"
                               data-show-footer="false" data-height="503">
                            <thead>
                            <tr>
                                <th data-field="ID" data-sortable="true">ID</th>
                                <th data-field="Date" data-sortable="true">Date</th>
                                <th data-field="Status" data-sortable="true">Status</th>
                                <th data-field="Vendor" data-sortable="true">Vendor</th>
                                <th data-field="Package" data-sortable="true">Package</th>
                                <th data-field="Ads" data-sortable="true">Ads</th>
                                <th data-field="CPs" data-sortable="true">CPs</th>
                                <th data-field="LDs" data-sortable="true">LDs</th>
                                <th data-field="login" data-sortable="true">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>101</td>
                                <td>04/14/2017</td>
                                <td>Active</td>
                                <td>Alhambra Nissan</td>
                                <td>Platinum</td>
                                <td>8</td>
                                <td>52</td>
                                <td>0</td>
                                <td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
                            <tr>
                                <td>102</td>
                                <td>04/15/2017</td>
                                <td>Active</td>
                                <td>Honda World </td>
                                <td>Platinum</td>
                                <td>1</td>
                                <td>12</td>
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
                            <tr>
                                <td>103</td>
                                <td>04/16/2017</td>
                                <td>Active</td>
                                <td>Downey Nissan </td>
                                <td>Platinum</td>
                                <td>28</td>
                                <td>2</td>
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
                            <tr>
                                <td>104</td>
                                <td>04/17/2017</td>
                                <td>Active</td>
                                <td>Nissan of Long Beach </td>
                                <td>Gold</td>
                                <td>48</td>
                                <td>5</td>
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
                            <tr>
                                <td>105</td>
                                <td>04/18/2017</td>
                                <td>Active</td>
                                <td>Westcovina Nissan </td>
                                <td>Gold</td>
                                <td>0</td>
                                <td>9</td>
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
                            <tr>
                                <td>106</td>
                                <td>04/19/2017</td>
                                <td>Active</td>
                                <td>Carson Chevrolet </td>
                                <td>Silver</td>
                                 <td>8</td>
                                <td>22</td>
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
							<tr>
                                <td>107</td>
                                <td>04/20/2017</td>
                                <td>Active</td>
                                <td>Car Pros Kia</td>
                                <td>Silver</td>
                                <td>98</td>
                                <td>32</td>
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
							<tr>
                                <td>108</td>
                                <td>04/21/2017</td>
                                <td>Active</td>
                                <td>Glendale Toyota</td>
                                <td>Silver</td>
                                <td>38</td>
                                <td>22</td>  
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
							<tr>
                                <td>107</td>  
                                <td>04/20/2017</td>
                                <td>Active</td>
                                <td>Car Pros Kia</td>
                                <td>Silver</td>
                                <td>98</td>
                                <td>32</td>
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
							<tr>
                                <td>108</td>
                                <td>04/21/2017</td>
                                <td>Active</td>
                                <td>Glendale Toyota</td>
                                <td>Silver</td>
                                <td>38</td>
                                <td>22</td>
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
							<tr>
                                <td>107</td>
                                <td>04/20/2017</td>
                                <td>Active</td>
                                <td>Alhambra Nissan</td>
                                <td>Silver</td>
                                <td>98</td>
                                <td>32</td>
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
							<tr>
                                <td>108</td>
                                <td>04/21/2017</td>
                                <td>Active</td>
                                <td>Honda World</td>
                                <td>Silver</td>
                                <td>38</td>
                                <td>22</td>
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
							<tr>
                                <td>107</td>
                                <td>04/20/2017</td>
                                <td>Active</td>
                                <td>Downey Nissan</td>
                                <td>Silver</td>
                                <td>98</td>
                                <td>32</td>
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
							<tr>
                                <td>108</td>
                                <td>04/21/2017</td>
                                <td>Active</td>
                                <td>Westcovina Nissan </td>
                                <td>Silver</td>
                                <td>38</td>
                                <td>22</td>
                                <td>0</td>
								<td><a href="edit_user.html"><i class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a href="#" data-toggle="modal" data-target="#delete"><i class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
                                    </a><a href="user_profile.html"><i class="fa fa-fw fa-star text-success actions_icon" title="View User"></i></a></td>
                            </tr>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--fourth table end-->
        <!--@include('layouts.right_sidebar')  -->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- begining of page level js -->
    <script type="text/javascript" src="{{asset('assets/vendors/editable-table/js/mindmup-editabletable.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/bootstrap-table/js/bootstrap-table.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/tableExport/tableExport.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/custom_js/bootstrap_tables.js')}}"></script>
	   
	<script>
	
	 
	$( document ).ready(function() {
	    var extra_features = '<button class="btn btn-default" type="button" name="mail" title="Create" ><i class="fa fa-user-plus"></i></button><button class="btn btn-default" type="button" name="mail" title="Mail" ><i class="fa fa-fw fa-envelope"></i></button><button class="btn btn-default" type="button" name="print" title="Print" ><i class="fa fa-fw fa-print"></i></button>';
		$(".fixed-table-toolbar .columns").prepend(extra_features);
	});	
	</script>
	   
	   <!--
	   <div class="input-group"><div class="input-group-addon"><i class="fa fa-fw fa-calendar"></i></div><input type="text" class="form-control pull-right" id="date-range0" placeholder="YYYY-MM-DD to YYYY-MM-DD"/></div>-->
	   
	   
	   <!-- bootstrap time picker -->
	   
	<!-- InputMask -->
	<script  type="text/javascript" src="{{asset('assets/vendors/moment/js/moment.min.js')}}"></script>
	<!-- date-range-picker -->
	<script  type="text/javascript" src="{{asset('assets/vendors/daterangepicker/js/daterangepicker.js')}}" ></script>
	<!-- bootstrap time picker -->
	<script  type="text/javascript" src="{{asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" ></script>

	<script  type="text/javascript" src="{{asset('assets/vendors/jquerydaterangepicker/js/jquery.daterangepicker.min.js')}}" ></script>
	<script  type="text/javascript" src="{{asset('assets/vendors/datedropper/datedropper.js')}}" ></script>
	<script  type="text/javascript" src="{{asset('assets/vendors/timedropper/js/timedropper.js')}}" ></script>
	<!--<script  type="text/javascript" src="{{asset('assets/js/custom_js/datepickers.js')}}" ></script> -->
	   
	   
	<script>
	$('#date-range0').dateRangePicker({
		autoClose: true,
		format: 'MM-DD-YYYY',
        getValue: function () {
            var range = $(this).val();
			//alert(range);
        }
    });
	
	 
	</script>
		
	
	
    <!-- end of page level js -->
@stop
