<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Hi {!! $name !!},</h3>
		<p>Below is the lead details:</p>
		<p>Lead Type: Pownder™ Lead</p>
		<p>Name: {!! $full_name !!}</p>
		<p>Email: {!! $email !!}</p>
		<p>Phone#: {!! $phone !!}</p>
		<p>City: {!! $city !!}</p>
		<p style="margin-top: 25px;">Remember to contact the lead.</p>
		<p style="margin-top: 25px;">Thanks & Regards</p>
		<table>
			<tr>
				<td style="border-right: 3px solid #00c8bf; padding: 0px 10px;">
					<img src='http://big.pownder.com/img/LOGO_pownder.png' alt='logo' style='max-width:150px; height:100px'>
				</td>
				<td style="padding: 0px 10px;">
					<h3 style="margin: 0px 0px 10px 0px">Pownder™ Team</h3>
					<span style="font-size: 18px;">
						818-578-9995 <br />
						info@pownder.com <br /> 
						big.pownder.com
					</span>
					<br />
					<a href="https://www.facebook.com/pownders/"><img src='http://big.pownder.com/img/facebook.png' style='max-width:32px; height:32px'></a>
					<a href="https://www.instagram.com/pownder_marketing/"><img src='http://big.pownder.com/img/instagram.png' style='max-width:32px; height:32px'></a>
					<a href="https://in.linkedin.com/"><img src='http://big.pownder.com/img/linkedin.png' style='max-width:32px; height:32px'></a>
					<a href="https://plus.google.com/"><img src='http://big.pownder.com/img/google-plus.png' style='max-width:32px; height:32px'></a>
				</td>
			</tr>
		</table>
	</body>
</html>