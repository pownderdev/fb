<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<link rel='stylesheet' type='text/css' href='http://fontawesome.io/assets/font-awesome/css/font-awesome.css'>
	</head>
	<body>
		@for($i = 0; $i<count($data); $i++)
		<h3>Hi {!! $data[$i]['name'] !!},</h3>
		<p>Below is the appointment details with lead:</p>
		<p>Appointment Date and Time: {!! $data[$i]['appointment_date'] !!} {!! $data[$i]['appointment_time'] !!}</p>
		<h4>Lead Details:</h4>
		@if($data[$i]['category'] == 'admin' || $data[$i]['category'] == 'vendor')
		<p>Client: {!! $data[$i]['client_name'] !!}</p>
		@endif
		@if($data[$i]['manager_name'] != '')
		<p>User: {!! $data[$i]['manager_name'] !!}</p>
		@endif
		<p>Lead Type: {!! $data[$i]['leadType'] !!}</p>
		@if($data[$i]['leadAddedBy'] != '')
		<p>Added By: {!! $data[$i]['leadAddedBy'] !!}</p>
		@endif
		<p>Name: {!! $data[$i]['leadName'] !!}</p>
		<p>Email: {!! $data[$i]['leadEmail'] !!}</p>
		<p>Phone#: {!! $data[$i]['leadPhone'] !!}</p>
		<p>City: {!! $data[$i]['leadCity'] !!}</p>
		@endfor
		<p style="margin-top: 25px;">Thanks & Regards</p>
		<table>
			<tr>
				<td style="border-right: 3px solid #00c8bf; padding: 0px 10px;"><img src='http://big.pownder.com/img/LOGO_pownder.png' alt='logo' style='max-width:150px; height:100px'></td>
				<td style="padding: 0px 10px;">
					<h3 style="margin: 0px 0px 10px 0px">Pownder™ Team</h3>
					<span style="font-size: 18px;">
						818-578-9995 <br />
						info@pownder.com <br /> 
						big.pownder.com
					</span>
					<br />
					<a href="https://www.facebook.com/pownders/"><img src='http://big.pownder.com/img/facebook.png' style='max-width:32px; height:32px'></a>
					<a href="https://www.instagram.com/pownder_marketing/"><img src='http://big.pownder.com/img/instagram.png' style='max-width:32px; height:32px'></a>
					<a href="https://in.linkedin.com/"><img src='http://big.pownder.com/img/linkedin.png' style='max-width:32px; height:32px'></a>
					<a href="https://plus.google.com/"><img src='http://big.pownder.com/img/google-plus.png' style='max-width:32px; height:32px'></a>
				</td>
			</tr>
		</table>
	</body>
</html>