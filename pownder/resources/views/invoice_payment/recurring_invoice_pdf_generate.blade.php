<html>
	<head>
		<meta charset="UTF-8">
		<style>
		
			label {
			font-weight:bold;
			}

			.with-border-b {
			border-bottom:1px solid #ccc;
			}
            

			.text-blue {
			color:blue;
			}

			.text-muted {
			color:#666;
			}

			.m-l-5 {
			margin-left:5px;
			}

			.m-t-0 {
			margin-top:0;
			}

			.m-b-0 {
			margin-bottom:0;
			}
             .text-center
             {
                text-align: center;
             }
			.m-l-0 {
			margin-bottom:0;
			}

			.m-t-5 {
			margin-top:5px;
			}

			.m-b-5 {
			margin-bottom:5px;
			}

			.m-t-10 {
			margin-top:10px;
			}

			.m-t-19 {
			margin-top:20px;
			}

			.m-l-10 {
			margin-left:10px;
			}

			.m-l-20 {
			margin-left:20px;
			}

			.p-t-0 {
			padding-top:0;
			}

			hr {
			margin-top:10px;
			margin-bottom:10px;
			}

			.text-green {
			color:green;
			}

			.cursor-pointer {
			cursor:pointer;
			}

			.border-left-right-0 {
			border-left:none;
			border-right:none;
			}

			.invoice-table {
			font-family: "Open Sans ",sans-serif;
			}

			.invoice-table {
			*max-width:900px;
			*margin:auto;
			width:100%;
			
			background-color:#fff;
			float:left;
			padding:15px;
			box-shadow:0px 2px 10px rgba(177,171,171,0.88);
			}

			.invoice-option {
			width:calc(100% - 730px);
			display:inline-block;
			float:left;
			min-height:50px;
			padding-right:25px;
			}

			.option-wrapper {
			padding:15px;
			background:#fff;
			margin-bottom:15px;
			}

			.col-100 {
			width:100%;
			}

			.col-50 {
			width:50%;
			}

			.col-25 {
			width:25%;
			}

			.col-33 {
			width:33.33%;
			}

			.col-66 {
			width:66.66%;
			}

			.col-10 {
			width:10%;
			}

			.col-20 {
			width:20%;
			}

			.col-15 {
			width:15%;
			}

			.col-30 {
			width:30%;
			}

			.col-40 {
			width:40%;
			}

			.center {
			text-align:center
			}

			.right {
			text-align:right
			}

			p {
			margin:0;
			}

			.title {
			font-weight:800;
			}

			table .content {
			font-weight:normal;
			padding:0px;
			}

			.align-top {
			vertical-align:top;
			}

			.align-bottom {
			vertical-align:bottom;
			}

			.m-t-0 {
			margin-top:0px;
			}

			.m-b-0 {
			margin-bottom:0px;
			}

			.m-l-0 {
			margin-left:0px;
			}

			.m-r-0 {
			margin-right:0px;
			}

			.m-0 {
			margin:0;
			}

			.p-t-0 {
			padding-top:0px;
			}

			.p-b-0 {
			padding-bottom:0px;
			}

			.p-l-0 {
			padding-left:0px;
			}

			.p-r-0 {
			padding-right:0px;
			}

			.p-0 {
			padding:0px;
			}

			.p-2 {
			padding:2px;
			}

			.border-none {
			border:none;
			}

			.border-left-none {
			border-left:0px;
			}

			.border-right-none {
			border-right:0px;
			}

			.border-top-none {
			border-top:0px;
			}

			.border-bottm-none {
			border-bottom:0px;
			}

			table {
			*border-collapse:collapse;
			}

			table,td,th {
			*border:1px solid black;
			}

			td,th {
			*padding:4px 7px;
			padding:0px 5px;
			}

			.one-line {
			white-space:nowrap;
			}

			
			.invoice-logo {
			max-width:70px;
			}

			.invoice-header-bottom {
			padding:12px 15px;
			background:#f5f5f5;
			margin:0 -15px;
			}

			.invoice-title-section {
			padding:5px 15px;
			background:#00c3bc;
			margin:15px -15px;
			color:#fff;
			}

			.main-title {
			color:#008fac;
			}

			.sub-title {
			}

			.invoice-details-section td {
			padding-top:5px;
			padding-bottom:5px;
			}

			.invoice-details-section tr.border-b {
			border-bottom:1px solid #efefef;
			}

			.item-details-section {
			min-height:120px;
			}

			.final-due {
			background:#ffbe18;
			color:#fff;
			}

			.add-btn {
			background:#ffb900;
			color:#fff!important;
			}

			.items-section {
			min-height:150px;
			}

			.add_existing {
			float:right;
			border:1px solid #f9f9f9;
			background:#ffb900;
			color:#fff!important;
			padding:0;
			width:25px;
			height:25px;
			margin-top:5px;
			font-size:20px;
			line-height:0;
			box-shadow:0 0 7px rgba(35,34,34,0.48);
			}
		
			.item-details-section .delete {
			background:#ea9c18;
			width:21px;
			height:21px;
			padding:4px 6px;
			font-size:12px;
			cursor:pointer;
			color:#fff;
			}

			.item-details-section .edit {
			background:#008fac;
			width:21px;
			height:21px;
			padding:4px 6px;
			font-size:12px;
			cursor:pointer;
			color:#fff;
			}

			.i_add_button {
			padding:4px 12px;
			background:#ea9c18;
			color:#fff;
			margin-top:3px;
			}

			.item-list-table thead {
			background:#008fac;
			color:#fff;
			}

			.item-list-table td {
			padding:3px 8px;
			}

			.item-list-table tr + tr {
			border-top:1px solid #efefef;
			}

			.modal-backdrop.in {
			opacity:.5;
			}
			.m-t-8 {
			margin-top:8px;
			}

			.btn-success {
			color:#fff;
			background-color:#00c3bc;
			border-color:#00c3bc;
			}

			.btn {
			display:inline-block;
			margin-bottom:0;
			font-weight:normal;
			text-align:center;
			vertical-align:middle;
			-ms-touch-action:manipulation;
			touch-action:manipulation;
			cursor:pointer;
			background-image:none;
			border:1px solid transparent;
			white-space:nowrap;
			padding:6px 12px;
			font-size:14px;
			line-height:1.428571429;
			border-radius:4px;
			-webkit-user-select:none;
			-moz-user-select:none;
			-ms-user-select:none;
			user-select:none;
			}
             small 
            {
                font-size: 11px;
            }            
            .invoice_tnc_con ul
            {               
                list-style-type: disc;
            }
            .invoice_tnc_con 
            {   
                margin: 0;
                padding: 0;
            }
            @media print
            {
                .no-print
                {
                    display: none;
                }
            }
		</style>
	</head>
	<body>
      <?php 
        //Money Format
         function money_format1($number)
        {
          setlocale(LC_MONETARY, 'en_US'); 
          return money_format('%!.2i',$number); 
        }
      ?>
		<div class="wrapper row-offcanvas row-offcanvas-left">
			<section class="content">			
				<section class="main_wrapper">
					<div class="invoice-table">
						<table class="col-100">
							<thead>
							</thead>
							<tbody>
								<tr>
									<td class="col-50" colspan="2">
										<img class="invoice-logo" src="pownder/storage/app/uploads/invoice/company_logo/{{$company_logo}}"/>
										<h2 class="title m-t-10 m-b-10 main-title">
											Invoice
										</h2>
										<h4 class="title m-t-10 m-b-10 sub-title">
											{{ $request->company_name }}
										</h4>
									</td>
									<td class="col-10">
									</td>
									<td class="col-40" colspan="2">
										<p class="m-0 right " style="min-height: 50px">
											<?php echo nl2br($request->company_address); ?>
										</p>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="invoice-header-bottom">
							<table class="col-100">
								<thead>
								</thead>
								<tbody>
									<tr>
										<td class="col-10 align-top" colspan="1" rowspan="4">
											<p class="m-0 title">
												Bill To:
											</p>
										</td>
										<td class="col-40 align-top" colspan="2" rowspan="4">
											<p class="content align-top" id="bill_to_field">
											<?php echo nl2br($request->bill_to); ?>
											</p>
										</td>
										<td class="col-20 align-top">
											<p class="title align-top right">
												Invoice Num
											</p>
										</td>
										<td class="col-20 align-top" colspan="2">
											<p class="content align-top right">
												<span id="time">
													{{ $invoice_num }}
												</span>
											</p>
										</td>
									</tr>
									<tr>
										<td class="col-20 align-top">
											<p class="title align-top right">
												Date
											</p>
										</td>
										<td class="col-20 align-top" colspan="2">
											<p class="content align-top right" id="date_field">
												{{ date('m/d/Y',strtotime($request->recurring_date)) }}
											</p>
										</td>
									</tr>
									<tr>
										<td class="col-20 align-top">
											<p class="title align-top right">
												Due Date
											</p>
										</td>
										<td class="col-20 align-top" colspan="2">
											<p class="content align-top right" id="due_date_field">
												{{ date('m/d/Y',strtotime('+'.$request->due_on_receipt.' days',strtotime($request->recurring_date))) }}
											</p>
										</td>
									</tr>
                                    @if($request->recurring_on)
									<tr>
										<td class="col-20 align-top">
											<p class="title align-top right">
												Recurring Date
											</p>
										</td>
										<td class="col-20 align-top" colspan="2">
											<p class="content align-top right" id="recurring_date_field">
												{{ date('m/d/Y',strtotime($recurr_date)) }}
											</p>
										</td>
									</tr>
                                    @endif
								</tbody>
							</table>
						</div>
						<div class="invoice-title-section">
							<table class="col-100">
								<tbody>
									<tr>
										<td class="col-20 align-top">
											<p class="title align-top left">
												Item
											</p>
										</td>
										<td class="col-30 align-top">
											<p class="title align-top left">
												Description
											</p>
										</td>
										<td class="col-15 align-top">
											<p class="title align-top right">
												Quantity
											</p>
										</td>
										<td class="col-15 align-top">
											<p class="title align-top right">
												Rate
											</p>
										</td>
										<td class="col-20 align-top">
											<p class="title align-top right">
												Amount
											</p>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="invoice-details-section">
							<div class="item-details-section">
								<table class="col-100" style="position: relative;">
									<tbody class="append_items">
										@for($i=0;$i<count($items);$i++) 
                                        <tr class="border-b">
											<td class="col-20 align-top">
												<p class="content align-top left">
													{{ $items[$i] }}
												</p>
											</td>
											<td class="col-30 align-top">
												<p class="content align-top left">
												<?php echo nl2br($description[$i]); ?>
												</p>
											</td>
											<td class="col-15 align-top">
												<p class="content align-top right">
													{{ money_format1($qty[$i]) }}
												</p>
											</td>
											<td class="col-15 align-top">
												<p class="content align-top right">
													${{ money_format1($rate[$i]) }}
												</p>
											</td>
											<td class="col-20 align-top">
												<p class="content align-top right">
													${{money_format1($amount[$i])}}
												</p>
											</td>
											</tr>
											@endfor
									</tbody>
								</table>
							</div>
							<table class="col-100">
								<tbody>
									<!--bill section-->
									<tr>
										<td class="col-60 align-top" colspan="3">
										</td>
										<td class="col-20 align-top">
											<p class="title align-top right">
												Subtotal
											</p>
										</td>
										<td class="col-20 align-top">
											<p class="title align-top right">
												${{money_format1($request->sub_total)}}
											</p>
										</td>
									</tr>
									<tr>
										<td class="col-60 align-top" colspan="3">
										</td>
										<td class="col-20 align-top">
											<p class="title align-top right">
												Tax(
												<span id="tax_field">
													{{ money_format1($request->tax_perc) }}
												</span>
												%)
											</p>
										</td>
										<td class="col-20 align-top">
											<p class="title align-top right">
												${{ money_format1($request->tax) }}
											</p>
										</td>
									</tr>
									<tr>
										<td class="col-60 align-top" colspan="3">
										</td>
										<td class="col-20 align-top">
											<p class="title align-top right">
												Total
											</p>
										</td>
										<td class="col-20 align-top">
											<p class="title align-top right">
												${{ money_format1($request->total) }}
											</p>
										</td>
									</tr>
									<tr>
										<td class="col-60 align-top" colspan="3">
										</td>
										<td class="col-20 align-top">
											<p class="title align-top right">
												Paid
											</p>
										</td>
										<td class="col-20 align-top">
											<p class="title align-top right">
												${{ money_format1($request->paid) }}
											</p>
										</td>
									</tr>
									<tr>
										<td class="col-60 align-top" colspan="3">
										</td>
										<td class="col-20 align-top final-due">
											<p class="title align-top right">
												Balance Due
											</p>
										</td>
										<td class="col-20 align-top final-due">
											<p class="title align-top right">
												${{ money_format1($request->balance) }}
											</p>
										</td>
									</tr>
									<tr class="no-print">
										<td class="col-100 align-top text-center" colspan="5" style="text-align:center">
										    <a href="http://big.pownder.com/confirm-invoice/{{$invoice_num}}" style="text-decoration: none;" class="btn btn-success right">
											Pay Now
											</a>
										</td>
									</tr>
								</tbody>
							</table>
		                    @if($INVOICETNC)
                            <br />
                            <hr style="margin-top: -5px;"/>  
                              <div class="col-md-12 invoice_tnc_con">
                              
                                <?php echo $INVOICETNC; ?> 
                                
                              </div>
                            
                           
                                                                                        
                            @endif	  
						</div>
					
					</div>
				</section>
			</section>
		</div>
	</body>

</html>