@extends('layouts/make_payment')

{{-- Page title --}}
@section('title')
Confirm Invoice
@parent
@stop

<?php 
//Money Format
 function money_format1($number)
{
  setlocale(LC_MONETARY, 'en_US'); 
  return money_format('%!.2i',$number); 
}
?>

{{-- Page content --}}
@section('content')

  @if(count($invoice_data)>0)
  <div class="row">
	<div class="col-md-6 col-md-offset-3">
		<br />
		<div class="panel-heading header no-print">
			<img src="{{asset('img/logo-f.png')}}" alt="logo" />
		</div>
		<section class="main_wrapper">
			<!--header area-->
			<div class="invoice-table table-responsive">
				<table class="col-100">
					<tbody>
						<tr>
							<td class="col-50" colspan="2">
								<img class="invoice-logo" src="{{asset('pownder/storage/app/uploads/invoice/company_logo/'.$invoice_data[0]->company_logo)}}"/>
								<h2 class="title m-t-10 m-b-10 main-title">
									Invoice
								</h2>
								<h4 class="title m-t-10 m-b-10 sub-title">
									{{$invoice_data[0]->company_name}}
								</h4>
							</td>
							<td class="col-10">
							</td>
							<td class="col-40 align-bottom" colspan="2">
								<p class="m-0 right " style="min-height: 50px">
									<?php echo nl2br($invoice_data[0]->company_address); ?>
								</p>
							</td>
						</tr>
					</tbody>
				</table>
				<!--header bottom area-->
				<div class="invoice-header-bottom table-responsive">
					<table class="col-100">
						<thead>
						</thead>
						<tbody>
							<tr>
								<td class="col-10 align-top" colspan="1" rowspan="4">
									<p class="m-0 title">
										Bill To:
									</p>
								</td>
								<td class="col-40 align-top" colspan="2" rowspan="4">
									<p class="content align-top" id="bill_to_field">
										<?php echo nl2br($invoice_data[0]->bill_to);?>
									</p>
								</td>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Invoice Num
									</p>
								</td>
								<td class="col-20 align-top" colspan="2">
									<p class="content align-top right">
										<span id="time">
											{{ $invoice_data[0]->invoice_num }}
										</span>
									</p>
								</td>
							</tr>
							<tr>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Date
									</p>
								</td>
								<td class="col-20 align-top" colspan="2">
									<p class="content align-top right" id="date_field">
										{{date('m/d/Y',strtotime($invoice_data[0]->invoice_date))}}
									</p>
								</td>
							</tr>
							<tr>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Due Date
									</p>
								</td>
								<td class="col-20 align-top" colspan="2">
									<p class="content align-top right" id="due_date_field">
										{{date('m/d/Y',strtotime($invoice_data[0]->due_date))}}
									</p>
								</td>
							</tr>
                            @if($invoice_data[0]->recurring_on)
							<tr>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Recurring Date
									</p>
								</td>
								<td class="col-20 align-top" colspan="2">
									<p class="content align-top right" id="recurring_date_field">
										@if($invoice_data[0]->recurring_date and $invoice_data[0]->recurring_date!="0000-00-00 00:00:00") {{date('m/d/Y',strtotime($invoice_data[0]->recurring_date))}} @endif
									</p>
								</td>
							</tr>
                            @endif
						</tbody>
					</table>
				</div>
				<!--header title area-->
				<div class="invoice-title-section">
					<table class="col-100">
						<tbody>
							<tr>
								<td class="col-30 align-top">
									<p class="title align-top left">
										Item
									</p>
								</td>
								<td class="col-20 align-top">
									<p class="title align-top left">
										Description
									</p>
								</td>
								<td class="col-15 align-top">
									<p class="title align-top right">
										Quantity
									</p>
								</td>
								<td class="col-15 align-top">
									<p class="title align-top right">
										Rate
									</p>
								</td>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Amount
									</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--header content area-->
				<div class="invoice-details-section">
					<div class="item-details-section">
						<table class="col-100" style="position: relative;">
							<tbody class="append_items">
								@foreach($invoice_data as $value)
								<tr class="border-b">
									<td class="col-20 align-top">
										<p class="content align-top left">
											{{$value->item}}
										</p>
									</td>
									<td class="col-20 align-top">
										<p class="content align-top left">
											<?php echo nl2br($value->description); ?>
										</p>
									</td>
									<td class="col-15 align-top">
										<p class="content align-top right">
											{{money_format1($value->qty)}}
										</p>
									</td>
									<td class="col-15 align-top">
										<p class="content align-top right">
											${{money_format1($value->rate)}}
										</p>
									</td>
									<td class="col-20 align-top">
										<p class="content align-top right">
											${{money_format1($value->amount)}}
										</p>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<table class="col-100">
						<tbody>
							<!--bill section-->
							<tr>
								<td class="col-60 align-top" colspan="3">
								</td>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Subtotal
									</p>
								</td>
								<td class="col-20 align-top">
									<p class="title align-top right">
										${{ money_format1($invoice_data[0]->sub_total) }}
									</p>
								</td>
							</tr>
							<tr>
								<td class="col-60 align-top" colspan="3">
								</td>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Tax(
										<span id="tax_field">
											{{money_format1($invoice_data[0]->tax_perc)}}
										</span>
										%)
									</p>
								</td>
								<td class="col-20 align-top">
									<p class="title align-top right">
										${{money_format1($invoice_data[0]->tax)}}
									</p>
								</td>
							</tr>
							<tr>
								<td class="col-60 align-top" colspan="3">
								</td>
								<td class="col-20 align-top">
									<p class="title align-top right">
										Total
									</p>
								</td>
								<td class="col-20 align-top">
									<p class="title align-top right">
										${{money_format1($invoice_data[0]->total)}}
									</p>
								</td>
							</tr>
							<tr>
								<td class="col-60 align-top" colspan="3">
								</td>
								<td class=" align-top">
									<p class="title align-top right">
										Paid
									</p>
								</td>
								<td class=" align-top">
									<p class="title align-top right">
										${{money_format1($invoice_data[0]->paid)}}
									</p>
								</td>
							</tr>
							<tr>
								<td class="col-60 align-top" colspan="3">
								</td>
								<td class="col-20 align-top final-due">
									<p class="title align-top right">
										Balance Due
									</p>
								</td>
								<td class="col-20 align-top final-due">
									<p class="title align-top right">
										${{money_format1($invoice_data[0]->balance)}}
									</p>
								</td>
							</tr>
							<tr class="no-print">
								<td class=" align-top" colspan="4">
									<p>
										<a href="#" id="printwindow">                                                 
										<button class="btn btn-default left" type="button"><i class="fa fa-print"></i> Print Invoice</button>
										</a>
									</p>
								</td>
								<td class=" align-top">
									<p class="title align-top right">
										<a href="{{route('invoice-details',['id'=>$invoice_data[0]->invoice_num])}}">
										<button class="btn btn-success right green_btn" type="button">Pay Now</button>
										</a>
									</p>
								</td>
							</tr>
						</tbody>
					</table>
                    
                    @if($INVOICETNC)
                    <hr/>  
                    <div class="row">
                      <div class="col-md-12 invoice_tnc_con">
                      
                        <?php echo $INVOICETNC; ?> 
                        
                      </div>
                    
                    </div>
                                                                                
                    @endif	
					
				</div>
			</div>
		</section>
        <br />
		<br />
	</div>
</div>
@endif
<!-- /.content -->
@stop 
{{-- page level scripts --}} 
@section('footer_scripts') 
@stop