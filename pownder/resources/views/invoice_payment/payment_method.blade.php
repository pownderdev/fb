@extends('layouts/make_payment')

{{-- Page title --}}
@section('title')
Payment Method
@endsection

@section('header_styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <style>
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }
  .custom-combobox-input {
    margin: 0;
    border-radius: 0;
    width:calc(100% - 26px);
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.528571429;
    color: #555555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px 0px 0px 4px;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;        
  }
  .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active
  {
    background-color: #fff;
  }
  .ui-menu
  {
   max-height: 200px;
   overflow-y: scroll;
  }
  .no-margin { margin: 0 !important;} 
  .no-padding {padding: 0 !important;}
  .state {  !important; }
  </style>
@endsection

@section('content')
<?php 
function showRedBorder($error)
{
    if(count($error)>0)
    {
        return ' has-error '  ;      
    }
}
function showErrorMsg($error)
{
    if(count($error)>0)
    {
        $msg='';
        foreach($error as $val)
        {
         $msg.='<small class="help-block animated fadeInUp">'.$val.'</small>';    
        }
        return $msg;        
    }
}
?>
    <br />
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
         <br />
        <div class="panel-heading header">
			     <img src="{{asset('img/logo-f.png')}}" alt="logo" />
	     </div>
         <div class="panel">
             <div class="panel-heading gred_2" style="padding: 8px 15px;">
                <h3 class="panel-title">
                   Payment Method
                </h3>
             </div>
             <div class="panel-body">               
                            <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                                <li class="active">
                                    <a href="#card" data-toggle="tab">
                                    <i class="fa fa-credit-card text-blue"></i> <span class="text-blue">Card Accounts</span><br />
                                    <small class="m-l-20">Card Details</small>
                                    </a>
                                </li>
                                <li>
                                    <a href="#bank" data-toggle="tab"  >
                                    <i class="fa fa-money text-blue"></i> <span class="text-blue">Bank Accounts</span><br />
                                    <small class="m-l-20">Bank Account Details</small>
                                    </a>
                                </li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <div class="tab-pane fade active in" id="card">                                    
                                    <h5><strong>CARD ACCOUNT</strong></h5>
                                    <br />
                                    <form  class="payment_method" method="post" autocomplete="off">
                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
                                    <div class="row">
                                   <div class="col-md-6">
                                   <label class="text-muted"><small>Card Info</small></label>
                                   <div class="form-group <?php echo showRedBorder($errors->get('name_on_card')); ?>">
                                      <label for="name_on_card">Full Name on Card</label>
                                      <input type="text" class="form-control name_on_card" value="{{old('name_on_card')}}" placeholder="Full Name on Card" name="name_on_card" id="name_on_card" />
                                      <?php echo showErrorMsg($errors->get('name_on_card')); ?>
                                   </div>
                                   <div class="form-group <?php echo showRedBorder($errors->get('card_number')); ?> <?php echo showRedBorder($errors->get('cvv')); ?> ">
                                      <label class="m-t-19" for="card_number">
                                       Card Number
                                       </label>
                                       <img class="pull-right m-t-10 m-b-5 m-l-5" src="{{asset('assets/img/payment_icons/discover.png')}}" />
                                       <img class="pull-right m-t-10 m-b-5 m-l-5" src="{{asset('assets/img/payment_icons/american-express.png')}}" />                                      
                                       <img class="pull-right m-t-10 m-b-5 m-l-5" src="{{asset('assets/img/payment_icons/visa.png')}}" />                                       
                                       <img  class="pull-right m-t-10 m-b-5" src="{{asset('assets/img/payment_icons/mastercard.png')}}" />
                                       <div class="row">
                                          <div class="col-md-8">
                                            <div class="col-md-3 no-margin no-padding">
                                            <input type="text" class="form-control card_number1" maxlength="4"  placeholder="xxxx" name="card_number1" id="card_number1" />
                                            </div>
                                            <div class="col-md-3 no-margin no-padding">
                                            <input type="text" class="form-control card_number2" maxlength="4"  placeholder="xxxx" name="card_number2" id="card_number2" />
                                            </div>
                                            <div class="col-md-3 no-margin no-padding">
                                            <input type="text" class="form-control card_number3" maxlength="4"  placeholder="xxxx" name="card_number3" id="card_number3" />
                                            </div>
                                            <div class="col-md-3 no-margin no-padding">
                                            <input type="text" class="form-control card_number4" maxlength="4"  placeholder="xxxx" name="card_number4" id="card_number4" />
                                            </div>
                                            <input type="hidden" class="form-control card_number" value="{{old('card_number')}}" name="card_number" id="card_number" />
                                            <?php echo showErrorMsg($errors->get('card_number')); ?> 
                                          </div>
                                          <div class="col-md-4">
                                            <input type="number" class="form-control cvv" value="{{old('cvv')}}" placeholder="cvv" name="cvv" id="cvv" />
                                            <?php echo showErrorMsg($errors->get('cvv')); ?> 
                                          </div>
                                       </div>
                                       
                                       
                                       
                                   </div>
                                   <div class="form-group <?php echo showRedBorder($errors->get('month')); ?> <?php echo showRedBorder($errors->get('year')); ?>">
                                      <label for="month">
                                       Expiration Date
                                       </label>
                                       <?php $year=date('Y');$to_year=date('Y',strtotime('+10 years'));$month=date('m'); ?>
                                       <div class="row">
                                          <div class="col-md-6">
                                            <select name="month" id="month" class="form-control month">                                             
                                             <option value="" selected="" disabled="">Month</option>
                                             <option <?php if(old('month')=="01"){echo 'selected';} ?>  value="1">January</option>
                                             <option <?php if(old('month')=="02"){echo 'selected';} ?>  value="2">February</option>
                                             <option <?php if(old('month')=="03"){echo 'selected';} ?> value="3">March</option>
                                             <option <?php if(old('month')=="04"){echo 'selected';} ?> value="4">April</option>
                                             <option <?php if(old('month')=="05"){echo 'selected';} ?> value="5">May</option>
                                             <option <?php if(old('month')=="06"){echo 'selected';} ?> value="6">June</option>
                                             <option <?php if(old('month')=="07"){echo 'selected';} ?> value="7">July</option>
                                             <option <?php if(old('month')=="08"){echo 'selected';} ?> value="8">August</option>
                                             <option <?php if(old('month')=="09"){echo 'selected';} ?> value="9">September</option>
                                             <option <?php if(old('month')=="10"){echo 'selected';} ?> value="10">October</option>
                                             <option <?php if(old('month')=="11"){echo 'selected';} ?> value="11">November</option>
                                             <option <?php if(old('month')=="12"){echo 'selected';} ?> value="12">December</option>                                           
                                            </select>
                                            <?php echo showErrorMsg($errors->get('month')); ?>
                                          </div>
                                          <div class="col-md-6">
                                              <select name="year" id="year" class="form-control year">
                                               <option value="" selected="" disabled="">Year</option>  
                                               <?php                                           
                                                   for($i=$year;$i<=$to_year;$i++)
                                                   {
                                                      echo '<option ';if(old('year')==$i){echo 'selected';} echo ' value="'.$i.'">'.$i.'</option>';
                                                   }
                                                ?>
                                               </select>
                                               <?php echo showErrorMsg($errors->get('year')); ?>
                                          </div>
                                       </div>
                                       
                                       
                                   </div>
                                   </div>
                                   <div class="col-md-6">
                                   <label class="text-muted"><small>Card Billing Address</small></label>
                                   <div class="row">
                                          <div class="col-md-6">
                                             <div class="form-group <?php echo showRedBorder($errors->get('address')); ?>">
                                              <label for="address">Street Address</label>
                                              <input type="text" class="form-control address" value="{{old('address')}}" placeholder="Street Address"  name="address" id="address" />
                                              <?php echo showErrorMsg($errors->get('address')); ?>
                                           </div>
                                          </div>
                                          <div class="col-md-6">
                                             <div class="form-group <?php echo showRedBorder($errors->get('address2')); ?>">
                                              <label for="address2">Street Address Line 2</label>
                                              <input type="text" class="form-control address2" value="{{old('address2')}}" placeholder="(optional)"  name="address2" id="address2" />
                                              <?php echo showErrorMsg($errors->get('address2')); ?>
                                           </div>
                                          </div>
                                          
                                   </div>
                                   <div class="row">
                                      <input type="hidden" value="USA" class="country" name="country" id="country" />
                                      <?php /*
                                       <div class="col-md-6">                                                                              
                                       <div class="form-group <?php echo showRedBorder($errors->get('country')); ?>">
                                          <label for="country">Country</label>
                                          <select class="form-control country" name="country" id="country">
                                           <option value="" selected="" disabled="">country</option>
                                           @foreach($countries as $val)
                                             <option <?php if(old('country')==$val->name) {echo 'selected';} else if(!old('country') && $val->name=="USA") {echo 'selected'; } ?>>{{$val->name}}</option>
                                           @endforeach
                                          </select>
                                          <?php echo showErrorMsg($errors->get('country')); ?>
                                       </div>
                                       </div>
                                       <div class="col-md-6">
                                            <div class="form-group <?php echo showRedBorder($errors->get('state')); ?>">
                                              <label for="state">State</label>
                                              <input type="text" class="form-control state" value="{{old('state')}}" placeholder="State" name="state" id="state" />
                                              <?php echo showErrorMsg($errors->get('state')); ?>
                                           </div>
                                       </div>
                                       */ ?>
                                       
                                       <div class="col-md-6">
                                         <div class="form-group <?php echo showRedBorder($errors->get('city')); ?>">
                                          <label for="city">City</label>
                                          <input type="text" class="form-control city" value="{{old('city')}}" placeholder="City" name="city" id="city" />
                                          <?php echo showErrorMsg($errors->get('city')); ?>
                                         </div>
                                       </div>
                                       
                                       <div class="col-md-6">                                                                              
                                       <div class="form-group ui-widget <?php echo showRedBorder($errors->get('state')); ?>">
                                          <label for="state">State</label>
                                          <select class="form-control state" name="state" id="state">
                                           <option value="" selected="">State</option>
                                           @foreach($states as $val)
                                             <option value="{{$val->name}}" <?php if(old('state')==$val->name) {echo 'selected';}  ?>>{{$val->name.' '.$val->shortname}}</option>
                                           @endforeach
                                          </select>
                                          <?php echo showErrorMsg($errors->get('state')); ?>
                                       </div>
                                       </div>
                                   </div>
                                   
                                   <div class="row">
                                          
                                          <div class="col-md-6">
                                            <div class="form-group <?php echo showRedBorder($errors->get('zip')); ?>">
                                              <label for="zip">Zip Code</label>
                                              <input type="text" class="form-control zip" value="{{old('zip')}}" placeholder="Zip Code" name="zip" id="zip"  />
                                              <?php echo showErrorMsg($errors->get('zip')); ?>
                                           </div>
                                          </div>
                                   </div>
                                       <div class="row">
                                          <div class="col-md-12 text-justify text-muted small">
                                            <p>
                                            <b>
                                            By selecting "Agree & Pay",you authorize the information you've provided on the above account to be used 
                                            for creation of a charge to the account listed above. You also affirm that the information
                                            you provided is correct, that you are a signer on the account above and there are available funds
                                            to cover the amount of any transactions that you authorize.
                                            </b>
                                            </p>
                                            <div class="form-group">
                                              <div class="checkbox">
                                                <label><input type="checkbox" required="" name="terms" id="terms" class="terms" /> Agree & Pay </label>
                                              </div>
                                            </div>
                                          </div>
                                       </div>
                                   </div>
                                   
                                   </div>
                                   <div class="row">
                                      <div class="col-md-12 text-right">
                                       <a href="#" id="cancel_payment" class="btn btn-default cancel_payment">Cancel</a>
                                       <button type="submit" class="btn btn-success green_btn">Pay</button>
                                      </div>                    
                                   </div>
                                   </form>
                                </div>
                                <div class="tab-pane fade" id="bank">                                    
                                    <h5><strong>BANK ACCOUNT</strong></h5>
                                    <br />
                                    <form  class="payment_method_bank" method="post" autocomplete="off">
                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
                                    <input type="hidden" value="US" class="country_1" name="country" id="country_1" />
                                    <input type="hidden" value="USD" class="currency" name="currency" id="currency" />
                                                                       
                                    <div class="row">
                                   <div class="col-md-6">
                                   <label class="text-muted"><small>Account Info</small></label>
                                   <div class="form-group ">
                                      <label for="routing_no">Routing Number</label>
                                      <input type="text" class="form-control routing_no"  placeholder="Routing Number" name="routing_no" id="routing_no" />                                      
                                   </div>
                                   <div class="form-group ">
                                      <label for="account_number">Account Number</label>
                                      <input type="text" class="form-control account_number"  placeholder="Account Number" name="account_number" id="account_number" />                                      
                                   </div>
                                   
                                   <div class="form-group has-error"><p class="help-block bank_error"></p></div>
                                   
                                   </div>
                                   <div class="col-md-6">
                                   <label class="text-muted"><small>&nbsp;</small></label>
                                   <div class="form-group">
                                      <label for="account_holder">Account Holder Name</label>
                                      <input type="text" class="form-control account_holder" placeholder="Account Holder Name" name="account_holder" id="account_holder" />                                      
                                   </div>
                                   
                                   <div class="form-group">
                                      <label for="account_holder_type">Account Holder Type</label>
                                      <select class="form-control account_holder_type" name="account_holder_type" id="account_holder_type" >
                                       <option value="individual">Individual</option>
                                       <option value="company">Company</option>
                                      </select>                                      
                                   </div>
                                  
                                   
                                   
                                       <div class="row">
                                          <div class="col-md-12 text-justify text-muted small">
                                            <p>
                                            <b>
                                            By selecting "Agree & Pay",you authorize the information you've provided on the above account to be used 
                                            for creation of a charge to the account listed above. You also affirm that the information
                                            you provided is correct, that you are a signer on the account above and there are available funds
                                            to cover the amount of any transactions that you authorize.
                                            </b>
                                            </p>
                                            <div class="form-group">
                                              <div class="checkbox">
                                                <label><input type="checkbox" required="" name="terms" id="terms" class="terms" /> Agree & Pay </label>
                                              </div>
                                            </div>
                                          </div>
                                       </div>
                                   </div>
                                   
                                   </div>
                                   <div class="row">
                                      <div class="col-md-12 text-right">
                                       <a href="#" id="cancel_payment" class="btn btn-default cancel_payment">Cancel</a>
                                       <button type="submit" class="btn btn-success green_btn">Pay</button>
                                      </div>                    
                                   </div>
                                   </form>
                                </div>
                                <input type="hidden" id="invoice_num" value="{{$invoiceno}}" />
                                
                                
                            </div>
                      
             </div>
         </div>
      </div>
    </div>
@stop
    
@section('footer_scripts')
<!--<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
-->
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<script src="{{ asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{ asset('assets/js/custom_js/custom_payment_method.js')}}" type="text/javascript"></script>
@endsection

								