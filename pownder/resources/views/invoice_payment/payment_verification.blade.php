@extends('layouts/make_payment')

{{-- Page title --}}
@section('title')
Verify Payment
@endsection

@section('header_styles')
@endsection

@section('content')
    <br />
    <?php 
    //Money Format
     function money_format1($number)
    {
      setlocale(LC_MONETARY, 'en_US'); 
      return money_format('%!.2i',$number); 
    }
    ?>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
         <br />
         <div class="panel-heading header">
			     <img src="{{asset('img/logo-f.png')}}" alt="logo" />
	     </div>
         <div class="panel">
             <div class="panel-heading gred_2" style="padding: 8px 15px;">
                <h3 class="panel-title">
                    Verify Payment
                </h3>
             </div>
             <div class="panel-body">
               <div class="row">
                  <div class="col-md-6">
                     <div class="well">       
                       <div class="row">
                          <div class="col-md-12">
                             <h5><b><i class="fa fa-money"></i> PAYMENT SUMMARY</b></h5>
                          </div>
                          <div class="col-md-6">          
                               <ul class="list-unstyled m-t-10">
                               <li>1 Invoice        <span class="pull-right"><b>${{money_format1(session('data2')['invoice_details']['balance'])}}</b></span></li>
                               <?php $cc_charge=0; ?>
                               @if(session('data2')['pay_method_type']=="card" and session('data2')["card_type"]=="credit")
                                <?php $cc_charge=round(session('data2')['invoice_details']['balance']*session('data2')["credit_card_processing_fee"]/100,2); ?>
                                <li>Transactional Fee ({{session('data2')["credit_card_processing_fee"]}}%) <span class="pull-right">${{money_format1($cc_charge)}}</span></li>                                
                                <li>Total Payment    <span class="pull-right"><b>${{money_format1(session('data2')['invoice_details']['balance']+$cc_charge)}}</b></span></li>
                               @else
                               <li>Total Payment    <span class="pull-right"><b>${{money_format1(session('data2')['invoice_details']['balance'])}}</b></span></li> 
                               @endif 
                               </ul>
                          </div>
                          <div class="col-md-6 p-t-0">                             
                              <!--
                                <a href="#" class="m-t-0" >View Profile Information</a>
                                                              <hr />
                              -->
                              <div class="form-group form-group-sm">
                              <label><small>Payment Method</small></label>
                              <!--<a href="{{ route('invoice-details',['id'=>1]) }}"><i class="fa fa-plus-circle"></i> Add New</a>-->
                              <select class="form-control">
                                @if(session('data2')['pay_method_type']=="card")
                                <option>{{session('data2')['card_brand']}} ************{{session('data2')['cvc_last4']}}</option>
                                @elseif(session('data2')['pay_method_type']=="bank")
                                 <option>{{session('data2')['bank_name']}} ***{{session('data2')['cvc_last4']}}</option>
                                @else
                                  <option>No payment method selected</option> 
                                @endif
                              </select>
                              </div>
                              <div class="form-group form-group-sm">
                                  <label><small>Payment Date</small></label>
                                  <div class="input-group input-group-sm">
                                  <input type="date" class="form-control"  value="{{date('Y-m-d')}}" readonly="" style="background-color: #fff;"  />
                                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                  </div>
                              </div>
                          </div>
                          <div class="col-md-12">
                             <p class="small">
                               <strong>Payments confirmed before {{date('l, F d, Y')}} 5:00 PM will be posted on {{date('l, F d, Y')}}.</strong>
                             </p>
                             <p class="small">
                               <strong>Payments confirmed after {{date('l, F d, Y')}} 5:00 PM will be posted on {{date('l, F d, Y',strtotime('+1 day'))}}.</strong>
                             </p>
                          </div> 
                       </div>
                           
                     </div>
                  </div>
                  <div class="col-md-6">
                     @if($payment_terms)
                     <div class="well m-b-5">
                       <h6><b>Payment Terms & Conditions</b></h6>
                       <p class="small">
                       <?php echo substr(nl2br($payment_terms),0,1000);   ?>
                       <a href="javascript:event.preventDefault()" data-toggle="modal" data-target="#payment_terms" >Read More</a>                       
                       </p>                                              
                     </div>
                     <a href="javascript:event.preventDefault()" data-toggle="modal" data-target="#payment_terms" ><i class="fa fa-print"> Print Terms & Conditions</i></a> 
                     @endif
                  </div>
               </div>
               <form  method="post" class="make_payment">
               <div class="row">
                <br />
                  
                 <div class="col-xs-8"> 
                        <div class="col-xs-12" >
                        <label class="checkbox-inline"><input type="checkbox" required="" /> <small>By clicking this box you agree to the terms and conditions stated above.</small></label>
                        </div>
                        <?php /*    
                        <div class="col-xs-11">                        
                        <p class="small text-left">
                                            
                        By clicking this box you agree to the terms and conditions stated above.<br />
                        By clicking the <strong>Make Payment</strong> button I <strong>{{session('data2')['client_name']}} ,</strong> confirm
                        that today {{date('l, F d, Y')}}, I am authorizing a one-time debit from my {{session('data2')['card_brand']}} account ending
                        in ************{{session('data2')['cvc_last4']}} in the amount of ${{session('data2')['invoice_details']['balance']}} USD to be remitted to {{session('data2')['sender_company_name']}}. This debit will occur on or after
                        {{date('l, F d, Y',strtotime('+1 day'))}}.  
                                          
                        </p>
                        <!--<p class="small">If you have any questions regarding this transaction request, please call XXX-XXX-XXXX.</p>-->
                     
                        </div>
                         */?>     
                 </div>
                 <div class="col-xs-4">
                    {{ csrf_field() }}
                    <a href="#" id="cancel_payment" class="btn btn-default" role="button">Cancel</a>
                    <button type="submit" class="btn btn-success green_btn">Make Payment</button>
                    
                 </div>
               </div>
               </form>
               <div class="row">
                <div class="col-md-12">
                   <h6 class="text-uppercase">
                   <b>Payment Details</b>                   
                   </h6>
                    <hr />
                   <div class="table-responsive">
                     <table class="table table-striped">
                       <thead>
                         <tr>
                            <th>Invoice Date</th>
                            <th>Due Date</th>
                            <th>Invoice Number</th>
                            <th>Balance Due</th>
                            <th>Payment Amount</th>
                         </tr>
                         
                       </thead>
                       <tbody>
                        <!--
                        <tr>
                           <th></th>
                           <th align="left" colspan="6">CUSTOMER NUMBER 1327437 </th>
                         </tr>
                        -->
                         <tr>
                           <!--<td><i class="fa fa-check-circle text-green"></i></td>-->
                           <td>{{date('m/d/Y',strtotime(session('data2')['invoice_details']['invoice_date']))}}</td>                           
                           <td>{{date('m/d/Y',strtotime(session('data2')['invoice_details']['due_date']))}}</td>
                           <td>{{session('data2')['invoice_details']['invoice_num']}}</td>
                           <td>${{money_format1(session('data2')['invoice_details']['balance'])}}</td>
                           <td>${{money_format1(session('data2')['invoice_details']['balance']+$cc_charge)}}</td>                           
                         </tr>
                       </tbody>
                     </table>
                   </div>
                  
               
           </div>
        </div>
             </div>
                 
         </div>
      </div>
    </div>
    <div class="modal fade animated" id="payment_terms" role="dialog">
      <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" data-dismiss="modal" class="close no-print">&times;</button>
             <h4 class="modal-title">Payment Terms & Conditions</h4>
           </div> 
           <div class="modal-body">
             <p class="small">
               <?php echo nl2br($payment_terms);   ?>
             </p>
           </div>
           <div class="modal-footer no-print">
            <button type="button"  class="btn btn-danger print-tnc"><i class="fa fa-print"></i> Print</button>
            <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
           </div>
         </div>
      </div>
    </div>
    
@stop

@section('footer_scripts')
<script type="text/javascript">
 $('.print-tnc').click(function(){
   var newwin=window.open();
   newwin.document.write("<style>@media only print{.no-print{display:none; }.invoice-logo { max-height: 100px;}} </style>"+$('#payment_terms').html());
   newwin.print();
   newwin.close();
 });
</script>
@endsection


								