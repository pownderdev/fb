@extends('layouts/make_payment')

{{-- Page title --}}
@section('title')
Additional Information 
@endsection

@section('header_styles')
@endsection

@section('content')
<?php 
function showRedBorder($error)
{
    if(count($error)>0)
    {
        return ' has-error '  ;      
    }
}
function showErrorMsg($error)
{
    if(count($error)>0)
    {
        $msg='';
        foreach($error as $val)
        {
         $msg.='<small class="help-block animated fadeInUp">'.$val.'</small>';    
        }
        return $msg;        
    }
}
?>
    <br />
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <br />
        <div class="panel-heading header">
			     <img src="{{asset('img/logo-f.png')}}" alt="logo" />
	     </div>
         <div class="panel">             
             <div class="panel-heading gred_2" style="padding: 8px 15px;">
                <h3 class="panel-title">
                    Additional Information 
                </h3>
             </div>
             <div class="panel-body">
                <form   class="invoice_details" method="post" role="form">
                {{ csrf_field() }}
                 <div class="row">
                   <div class="col-md-6">
                   <div class="form-group <?php echo showRedBorder($errors->get('company_name')); ?>">
                      <label for="company_name">Company Name</label>
                      <input type="text" class="form-control company_name" value="@if(old('company_name')){{old('company_name')}}@else{{session('invoice_data')->name}}@endif" placeholder="Company Name" name="company_name" id="company_name" />
                      <?php echo showErrorMsg($errors->get('company_name')); ?>
                   </div>
                   </div>
                   <div class="col-md-6">
                   <div class="form-group <?php echo showRedBorder($errors->get('telephone')); ?> <?php echo showRedBorder($errors->get('telephone_type')); ?>">
                      <label for="telephone">Primary Telephone Number</label>
                        <input type="text" class="form-control inline telephone" value="@if(old('telephone')){{old('telephone')}}@else{{session('invoice_data')->mobile}}@endif" placeholder="(XXX) XXX-XXXX" name="telephone" id="telephone" />
                   
                         <select class="form-control inline telephone_type" name="telephone_type" id="telephone_type">
                            <option  selected="" value="mobile">Mobile</option>
                            <option <?php if(old('telephone_type')=="work"){echo 'selected';} ?> value="work">Work</option>
                            <option <?php if(old('telephone_type')=="personal"){echo 'selected';} ?> value="personal">Personal</option>
                         </select>
                         <?php echo showErrorMsg($errors->get('telephone')); ?>
                         <?php echo showErrorMsg($errors->get('telephone_type')); ?>
                    
                   </div>
                   </div>
                   </div>
                   <?php $first_name=$last_name=''; if(trim(session('invoice_data')->name)) { $full_name=explode(' ',session('invoice_data')->name);$first_name=implode(' ',array_slice($full_name,0,-1));$last_name=array_slice($full_name,-1,1)[0]; } ?>
                   <div class="row">
                   <div class="col-md-6">
                   <div class="form-group <?php echo showRedBorder($errors->get('first_name')); ?>">
                      <label for="first_name">First Name</label>
                      <input type="text" class="form-control first_name" value="@if(old('first_name')){{old('first_name')}}@else{{$first_name}}@endif" placeholder="First Name" name="first_name" id="first_name" />
                      <?php echo showErrorMsg($errors->get('first_name')); ?> 
                   </div>
                   </div>
                   <div class="col-md-6">
                   <div class="form-group <?php echo showRedBorder($errors->get('email')); ?>">
                      <label for="email">Primary Email Address</label>
                      <input type="email" class="form-control email" value="@if(old('email')){{old('email')}}@else{{session('invoice_data')->email}}@endif" placeholder="Email Address" name="email" id="email" />
                      <?php echo showErrorMsg($errors->get('email')); ?>
                   </div>
                   </div>
                   </div>
                   <div class="row">
                   <div class="col-md-6">
                   <div class="form-group <?php echo showRedBorder($errors->get('middle_name')); ?>">
                      <label for="middle_name">Middle Name</label>
                      <input type="text" class="form-control middle_name" value="{{old('middle_name')}}" placeholder="(optional)" name="middle_name" id="middle_name" />
                      <?php echo showErrorMsg($errors->get('middle_name')); ?>
                   </div>
                   </div>
                   </div>
                   <div class="row">
                   <div class="col-md-6">
                   <div class="form-group <?php echo showRedBorder($errors->get('last_name')); ?>">
                      <label for="last_name">Last Name</label>
                      <input type="text" class="form-control last_name" placeholder="Last Name" value="@if(old('last_name')){{old('last_name')}}@else{{$last_name}}@endif" name="last_name" id="last_name" />
                      <?php echo showErrorMsg($errors->get('last_name')); ?>
                   </div>
                   </div>
                   </div>
                   <div class="row">
                   <div class="col-md-6">
                   <div class="form-group <?php echo showRedBorder($errors->get('country')); ?>">
                      <label for="country">Country</label>
                      <select class="form-control country" name="country" id="">
                       <option value="" selected="" disabled="">select country</option>
                       @foreach($countries as $val)
                         <option <?php if(old('country')==$val->name) {echo 'selected';} else if(!old('country') && $val->name=="USA") {echo 'selected'; } ?>>{{$val->name}}</option>
                       @endforeach
                      </select>
                      <?php echo showErrorMsg($errors->get('country')); ?>
                   </div>
                   </div>
                   </div>
                   <div class="row">
                   <div class="col-md-12">
                   <div class="form-group <?php echo showRedBorder($errors->get('address')); ?>">
                      <label for="address">Address</label>
                      <input type="text" class="form-control address" value="@if(old('address')){{old('address')}}@else{{session('invoice_data')->address}}@endif" placeholder="Address"  name="address" id="address" />
                      <?php echo showErrorMsg($errors->get('address')); ?>
                   </div>
                   </div>
                   </div>
                   <div class="row">
                   <div class="col-md-12">
                   <div class="form-group <?php echo showRedBorder($errors->get('address2')); ?>">
                      <label for="address2">Address Line 2</label>
                      <input type="text" class="form-control address2" value="{{old('address2')}}" placeholder="(optional)"  name="address2" id="address2" />
                      <?php echo showErrorMsg($errors->get('address2')); ?>
                   </div>
                   </div>
                   </div>
                   <div class="row">
                   <div class="col-md-6">
                   <div class="form-group <?php echo showRedBorder($errors->get('city')); ?>">
                      <label for="city">City</label>
                      <input type="text" class="form-control city" value="@if(old('city')){{old('city')}}@else{{session('invoice_data')->city}}@endif" placeholder="City" name="city" id="city" />
                      <?php echo showErrorMsg($errors->get('city')); ?>
                   </div>
                   </div>
                   <div class="col-md-6">
                   <div class="form-group <?php echo showRedBorder($errors->get('state')); ?>">
                      <label for="state">State</label>
                      <input type="text" class="form-control state" value="@if(old('state')){{old('state')}}@else{{session('invoice_data')->state}}@endif" placeholder="State" name="state" id="state" />
                      <?php echo showErrorMsg($errors->get('state')); ?>
                   </div>
                   </div>
                   </div>
                   <div class="row">
                   <div class="col-md-6">
                   <div class="form-group <?php echo showRedBorder($errors->get('zip')); ?>">
                      <label for="zip">Zip Code</label>
                      <input type="text" class="form-control zip" value="@if(old('zip')){{old('zip')}}@else{{session('invoice_data')->zip}}@endif" placeholder="Zip Code" name="zip" id="zip" />
                      <?php echo showErrorMsg($errors->get('zip')); ?>
                   </div>
                   </div>                   
                   </div>
                   <div class="row">
                      <div class="col-md-12 text-right">
                       <a href="#" id="cancel_payment" class="btn btn-default" role="button">Cancel</a>
                       <button type="submit" class="btn btn-success green_btn">Continue</button>
                      </div>                    
                   </div>
                </form>
             </div>
         </div>
      </div>
    </div>
@stop

@section('footer_scripts')
<script src="{{ asset('assets/vendors/inputmask/inputmask/inputmask.js')}}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/inputmask/inputmask/jquery.inputmask.js')}}" type="text/javascript"></script>
<script>$("#telephone").inputmask('(999) 999-9999');$("#zip").inputmask('99999');</script>
@endsection

								