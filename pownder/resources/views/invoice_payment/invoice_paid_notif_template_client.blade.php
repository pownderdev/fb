<html>
	<head>
		<meta charset="UTF-8">
		<style>
			.btn-success {
			color:#fff;
			background-color:#00c3bc;
			border-color:#00c3bc;
			}

			.btn {
			display:inline-block;
			margin-bottom:0;
			font-weight:normal;
			font-size:13px;
			text-align:center;
			vertical-align:middle;
			-ms-touch-action:manipulation;
			touch-action:manipulation;
			cursor:pointer;
			background-image:none;
			border:1px solid transparent;
			white-space:nowrap;
			padding:6px 12px;
			font-size:14px;
			line-height:1.428571429;
			border-radius:4px;
			-webkit-user-select:none;
			-moz-user-select:none;
			-ms-user-select:none;
			user-select:none;
			}
            table
            {
                margin-top: 30px;
            }
		</style>
	</head>
	<body>
		<h3>
			Dear {{$client_name}},
		</h3>
		<p>
		   Your	payment against invoice number {{$invoice_num}} has been done successfully.
		</p>		
		
		<table>
			<tr>
				<th align="left">
					Thanks and Regards
				</th>
			</tr>
			<tr>
				<td>
					{{$company_name}}
				</td>
			</tr>
		</table>
	</body>

</html>