@extends('layouts/make_payment')

{{-- Page title --}}
@section('title')
Confirmation
@endsection

@section('header_styles')
@endsection

@section('content')
  <?php
  //Money Format
     function money_format1($number)
    {
      setlocale(LC_MONETARY, 'en_US'); 
      return money_format('%!.2i',$number); 
    }
  ?>
    <br />
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
         <br />
         <div class="panel-heading header">
			     <img src="{{asset('img/logo-f.png')}}" alt="logo" />
	     </div>
         <div class="panel">
             <div class="panel-heading gred_2" style="padding: 8px 15px;">
                <h3 class="panel-title">
                    Confirmation
                </h3>
             </div>
             <div class="panel-body">
                <div class="alert alert-success">
                 <i class="fa fa-check-square-o"></i> <strong>Thank You!</strong> Your payment is done successfully.
                </div>
                <div class="row">
                   <div class="col-md-4">
                       <!--
                       <div class="well">
                          <a href="#"><i class="fa fa-print"></i> Print Confirmation Page</a>
                       </div>
                       -->
                       <h5><b>{{session('data2')['invoice_details']['company_name']}}</b></h5>
                       <address>
                       <?php  echo wordwrap(session('data2')['invoice_details']['company_address'],25,"<br />\n"); ?>
                       </address>
                   </div>
                   <div class="col-md-8">
                      <ul class="list-group">
                        <li class="list-group-item border-left-right-0"><b>Payment Date</b> <span class="pull-right">{{date('m/d/Y')}}</span></li>
                        <li class="list-group-item border-left-right-0"><b>Payment Method</b>
                         @if(session('data2')['pay_method_type']=="card")
                         <span class="pull-right">{{session('data2')['card_brand']}} ************{{session('data2')['cvc_last4']}}</span>
                         @elseif(session('data2')['pay_method_type']=="bank")
                         <span class="pull-right">{{session('data2')['bank_name']}}  ***{{session('data2')['cvc_last4']}}</span>
                         @endif
                         </li>
                         <?php $cc_charge=0; ?>
                         @if(session('data2')['pay_method_type']=="card" and session('data2')["card_type"]=="credit")
                         <?php $cc_charge=round(session('data2')['invoice_details']['balance']*session('data2')["credit_card_processing_fee"]/100,2); ?>
                         <li class="list-group-item border-left-right-0"><b>Total Balance</b> <span class="pull-right">${{money_format1(session('data2')['invoice_details']['balance'])}}</span></li>
                         <li class="list-group-item border-left-right-0"><b>Transactional Fee</b> ({{session('data2')["credit_card_processing_fee"]}}%) <span class="pull-right">${{money_format1($cc_charge)}}</span></li>                                
                         <li class="list-group-item border-left-right-0"><b>Total Payment</b> <span class="pull-right">${{money_format1(session('data2')['invoice_details']['balance']+$cc_charge)}}</span></li>
                         @else
                         <li class="list-group-item border-left-right-0"><b>Total Payment</b> <span class="pull-right">${{money_format1(session('data2')['invoice_details']['balance'])}}</span></li>
                         @endif
                      </ul>
                   </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                     <p>
                       You have been provided a confirmation number. Please save this page for your records.
                     </p>
                     <p>
                       Payments confirmed before {{date('l, F d, Y')}} 5:00 PM will be posted on {{date('l, F d, Y')}}.
                     </p>
                     <p>
                       Payments confirmed after {{date('l, F d, Y')}} 5:00 PM will be posted on {{date('l, F d, Y',strtotime('+1 day'))}}.
                     </p>
                     <p>
                       If you have any further questions about payments to {{session('data2')['invoice_details']['company_name']}}, please contact out office at XXX-XXX-XXXX.
                     </p>
                  </div>
                </div>
                <br />
                <div class="row">
                   <div class="col-md-12">
                      <div class="table-responsive">
                         <table class="table table-condensed">
                           <thead>
                             <tr>                               
                               <th>Confirmation Number</th>
                               <th>Payment Amount</th>
                               <th>Invoices</th>
                             </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td>{{sprintf('CNF%05d',session('data2')['confirmation_no'])}}</td>
                                 <td>
                                 @if(session('data2')['pay_method_type']=="card" and session('data2')["card_type"]=="credit")
                                 ${{money_format1(session('data2')['invoice_details']['balance']+$cc_charge)}}
                                 @else
                                 ${{money_format1(session('data2')['invoice_details']['balance'])}}
                                 @endif 
                                 </td>
                                 <td>1</td>
                              </tr>
                           </tbody>
                         </table>
                      </div>
                   </div>
                </div>
                {{session()->forget('invoice_data')}}{{session()->forget('data')}}{{session()->forget('data2')}}
                <div class="row no-print">
                   <div class="col-md-12 text-center">
                      <a href="#" id="printwindow">                                                 
						<button type="button" class="btn btn-danger"><i class="fa fa-print"></i> Print Confirmation Page</button>
				      </a>                      
                      <a href="{{url('http://pownder.com')}}">
                         <button type="button" class="btn btn-primary">Go To Home Page</button>
                      </a>
                   </div>
                </div>
                   
             </div>
         </div>
      </div>
    </div>
@stop

@section('footer_scripts')

@endsection

								