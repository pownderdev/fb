<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>
			@section('title')
            | Big Pownder™ 
			@show
		</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}"/>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<!-- global css -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/app.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom.css')}}">
		
		<!--page level css -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/bootstrap-multiselect/css/bootstrap-multiselect.css')}}" >
		
		<!-- daterange picker -->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/daterangepicker/css/daterangepicker.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/jquerydaterangepicker/css/daterangepicker.min.css')}}">
		
		<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/toastr/css/toastr.min.css')}}" />
		<!--weathericons-->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/weathericon/css/weather-icons.min.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/c3/c3.min.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/nvd3/css/nv.d3.min.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/hover/css/hover-min.css')}}" />
		
		<!--Bootstrap Tables-->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/bootstrap-table/css/bootstrap-table.min.css')}}">
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_css/bootstrap_tables.css')}}">
		
		<!--Sweet Alert-->
		<link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/sweetalert2/css/sweetalert2.min.css')}}"/>
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_css/sweet_alert2.css')}}">
		
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_css/dashboard1.css')}}" />
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/custom_css/dashboard2.css')}}" />
		
		<link rel="stylesheet" type="text/css" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">
		
		<link rel="stylesheet" type="text/css" href="{{asset('assets/css/buttons_sass.css')}}" />
        <style>
            .date-loader{
            z-index: 99999999;
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            width: 100%;
            height: 100%;
            background: rgba(238, 238, 238, 0.45);
            overflow: hidden;
            text-align: center;
            display: none;
			} 
			.date-preloader {
            display: inline-block;
            position: relative;
            width: 50px;
            height: 50px;
            -ms-transform: skew(20deg); /* IE 9 */
            -webkit-transform: skew(20deg); /* Safari */
            transform: skew(20deg);
			}
            .fixed-table-container {max-height: 700px !important; }
		</style>
		
		@yield('header_styles')
		<!-- end of global css -->
		<!-- global js -->
		<script src="{{asset('assets/js/app.js')}}" type="text/javascript"></script>
	</head>
	<body class="skin-coreplus">
		<div class="preloader">
			<div class="loader_img"><img src="{{asset('assets/img/loader.gif')}}" alt="loading..." height="64" width="64"></div>
		</div>
        <div class="date-loader" id="date-loader">
			<div style="position: relative;top: calc(50% - 30px);">
				<div class="date-preloader">
					<i class="fa fa-spinner fa-spin fa-pulse fa-4x" style="color: #428BCA;"></i>
				</div>
				<p id="loading-msg" style="font-size: 13px;margin-top: 10px;font-weight: bold;color: #444;">please wait..</p>
			</div>
		</div>
		<!-- header logo: style can be found in header-->
		<header class="header">
			<nav class="navbar navbar-static-top" role="navigation">
				<a href="dashboard" class="logo">
					<!-- Add the class icon to your logo image or logo icon to add the margining -->
					<!--<img src="{{asset('assets/img/logo.png')}}" alt="logo"/>-->
					<img src="{{asset('img/logo-f.png')}}" alt="logo" style="max-width: 90%;" />
				</a>
				<!-- Header Navbar: style can be found in header-->
				<!-- Sidebar toggle button-->
				<div>
					<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button"> 
						<i class="fa fa-fw fa-bars"></i>
					</a>
				</div>
				<!-- /Sidebar toggle button-->
				
				<div class="navbar-right">
					<ul class="nav navbar-nav">
						@if(Session::get('user_category')=='admin' || Session::get('user_category')=='vendor')
						<li class="dropdown messages-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Facebook Ads Account List"> <i
								class="fa fa-fw fa-facebook black"></i>
								<!--<span class="label label-success">2</span>-->
							</a>
							<div class="dropdown-menu account-list" style="border-radius: 5px;">
								<ul class="dropdown-messages table-striped scroll-style">
									<li class="dropdown-title" style="font-size:18px">Select Facebook Account</li>
									@php $ij=0; @endphp
									@foreach(Helper::FacebookAccounts() as $FacebookAccount)
									<li class="dropdown-title" style="padding: 5px 0px 5px 15px;font-size: 16px;"> 
										<img class="message-image img-circle" src="@if($FacebookAccount->picture!=''){{ $FacebookAccount->picture }}@else{{asset('assets/img/authors/avatar.jpg')}}@endif" alt="avatar-image" /> 
										{{ $FacebookAccount->name }} 
										<button class="btn btn-danger" style="float:right;margin-top: 8px;
										margin-right: 5px;" title="Remove Account" onClick="removeFacebookAccount('{{ $FacebookAccount->id }}','{{ $FacebookAccount->name }}')"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></button>
									</li>
									@foreach(Helper::FacebookAdsAccounts($FacebookAccount->id) as $FacebookAdsAccount)
									<!--<li class="active">-->
									<li>
										<a href="javascript:(void);" class="message @if($ij%2==0){{ 'striped-col' }}@endif">
											<img class="message-image img-circle" src="@if($FacebookAdsAccount->facebook_account_picture!=''){{ $FacebookAdsAccount->facebook_account_picture }}@else{{asset('assets/img/authors/avatar.jpg')}}@endif" alt="avatar-image" />
											
											<div class="message-body">
												<strong>{{ $FacebookAdsAccount->facebook_account_name }}</strong>
												<br>
												{{ $FacebookAdsAccount->facebook_account_name }} ({{ $FacebookAdsAccount->facebook_id }})
												<br>
												<!--<small>Just Now</small>-->
												<span class="label label-success label-mini msg-lable active-account"><i class="fa fa-check" style="font-size: 17px;"></i></span>
											</div>
										</a>
									</li>
									@php $ij++; @endphp
									@endforeach
									@endforeach
								</ul>
								<div class="add_acc_btn">
									<a href="https://www.facebook.com/v2.10/dialog/oauth?client_id=246735105809032&state=45c2c5f62d6c955c813aa2e987072db6&response_type=code&sdk=php-sdk-5.5.0&redirect_uri=http%3A%2F%2Fbig.pownder.com%2Ffblogin%2Fcallback.php&scope=email%2Cpages_show_list%2Cmanage_pages%2Cads_read%2Cads_management%2Cuser_likes%2Cpublish_actions%2Cpublish_pages%2Cpages_messaging" class="button button-rounded"  style="background-color: #3b5998; font-weight: 600; padding:0px 20px 0px 0px; color:#FFF;margin: 0px 50px;"> 
										<span class="button" style="background-color: #3b5998; padding:0px 10px;">
										<i class="fa fa-fw fa-facebook fa-2" aria-hidden="true" style="font-size: 16px; color:#FFF"></i></span> Add another account
									</a>
								</div>
							</div>
						</li>
						@endif
						
						@if(Session::get('user_category')=='admin' || ManagerHelper::ManagerCategory()=='admin' || Session::get('user_category')=='vendor' || ManagerHelper::ManagerCategory()=='vendor')
						<li class="dropdown messages-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Client Filter"> <i
								class="fa fa-fw fa-suitcase black"></i>
								<!--<span class="label label-success">2</span>-->
							</a>
							<div class="dropdown-menu client-list" style="border-radius: 5px;">
								<ul class="dropdown-messages table-striped scroll-style" style="max-height:310px; overflow: scroll;padding: 0px;border-top: 4px solid #1dd4cb;">
									@php $ij=0; @endphp
									@foreach(Helper::GroupCategories() as $GroupCategory)
									<li class="group_heading">{{ $GroupCategory->name }}</li>
									@foreach(Helper::GroupCategoryClients($GroupCategory->id) as $Client)
									@if(app('request')->exists('client_id') || Session::get('client_id')>0)
									@if(app('request')->client_id==$Client->id || Session::get('client_id')==$Client->id)
									<li user_client_no="{{ $Client->client_no }}" user_client_id="{{ $Client->id }}" class="active">
										<div class="message @if($ij%2==0){{ 'striped-col' }}@endif">
											<div class="message-body">
												<strong style="font-size: 12px;">{{ $Client->name }}</strong>
												<span style="display:none" class="user_client_id">{{ $Client->id }}</span>
												<span class="label label-success label-mini msg-lable active-account" style="margin-top: 0px;">
													<i class="fa fa-check" style="font-size: 17px;"></i>
												</span>
											</div>
										</div>
									</li>
									@else
									<li user_client_no="{{ $Client->client_no }}" user_client_id="{{ $Client->id }}">
										<div class="message @if($ij%2==0){{ 'striped-col' }}@endif">
											<div class="message-body">
												<strong style="font-size: 12px;">{{ $Client->name }}</strong>
												<span style="display:none" class="user_client_id">{{ $Client->id }}</span>
												<span class="label label-success label-mini msg-lable active-account" style="margin-top: 0px;">
													<i class="fa fa-check" style="font-size: 17px;"></i>
												</span>
											</div>
										</div>
									</li>
									@endif
									@else
									<li user_client_no="{{ $Client->client_no }}" user_client_id="{{ $Client->id }}">
										<div class="message @if($ij%2==0){{ 'striped-col' }}@endif">
											<div class="message-body">
												<strong style="font-size: 12px;">{{ $Client->name }}</strong>
												<span style="display:none" class="user_client_id">{{ $Client->id }}</span>
												<span class="label label-success label-mini msg-lable active-account" style="margin-top: 0px;">
													<i class="fa fa-check" style="font-size: 17px;"></i>
												</span>
											</div>
										</div>
									</li>
									@endif
									@php $ij++; @endphp
									@endforeach
									@endforeach
								</ul>
								<div class="add_acc_btn">
									<center>
										<button class="btn btn-primary reset_lead btn-lg" style="padding: 5px 50px;">Reset</button>
									</center>
								</div>
							</div>
						</li>
						@endif
						<!--tasks-->
						<li class="dropdown messages-menu">
							<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" id="open_date_filter">
								<i  class="fa fa-fw fa-calendar black"></i>
							</a>
						</li>
						
						@if(count(Helper::resync_error()) != 0)
						<li class="dropdown tasks-menu hidden-xs">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-fw fa-edit black"></i>
								<span class="label label-primary resyncCount">{{ count(Helper::resync_error()) }}</span>
							</a>
							<ul class="dropdown-menu dropdown-messages scroll-style" style="max-height: 280px; overflow: scroll;">
								<li class="dropdown-title">You Have <span id="resyncCount">{{ count(Helper::resync_error()) }}</span> Re-Sync Notification</li>
								@php $jk = 0; @endphp
								@foreach(Helper::resync_error() as $resync_error)
								<li data-resync="{{ $resync_error->id }}">
									<a href="#" class="message @if($jk%2==1) striped-col @endif">
										{{ title_case($resync_error->lead_name) }} 
										@if($resync_error->client_name == '') 
											({{ title_case($resync_error->form_name) }})
											@else
											({{ title_case($resync_error->client_name) }}) 
										@endif
										<small class="pull-right"> <i class="fa fa-trash-o fa-2x resync_error text-danger" data-value="{{ $resync_error->id }}" aria-hidden="true" title="Remove"></i></small>
										<div class="message-body">
											{{ $resync_error->reason }}
										</div>
									</a>
								</li>
								@php $jk++; @endphp
								@endforeach
								<!--<li class="dropdown-footer"><a href="#"> View All Tasks</a></li>-->
							</ul>
						</li>
						@endif
						
						<!--<li>
							<a href="#" class="dropdown-toggle toggle-right">
								<i class="fa fa-fw fa-comments-o black"></i>
								<span class="label label-danger">9</span>
							</a>
						</li>-->
						
						@php $notification=0; @endphp
						@if(Session::get('user_category')=='admin'  || ManagerHelper::ManagerCategory()=='admin' || Session::get('user_category')=='vendor'  || ManagerHelper::ManagerCategory()=='vendor')
						@if(count(Helper::UnassignLeadForm())>0)
						@php $notification++; @endphp
						@endif
						@if(count(Helper::UnassignLeadFormGroup())>0)
						@php $notification++; @endphp
						@endif
						@if(count(Helper::UnassignLeadFormClient())>0)
						@php $notification++; @endphp
						@endif
                        
                        @php $notification_recurring=Boot_Notifications::GetRecurringNotifications(); @endphp 
                        @if(count($notification_recurring)>0)
						@php $notification++; @endphp
						@endif                       
                        
						<!-- Notifications: style can be found in dropdown-->
						<li class="dropdown notifications-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-fw fa-bell-o black"></i>
								<span class="label label-warning">{{ $notification }}</span>
							</a>
							<ul class="dropdown-menu dropdown-messages scroll-style" style="max-height:295px; overflow: scroll;padding: 0px;border-top: 4px solid #1dd4cb;">
								@if(count(Helper::UnassignLeadForm())>0)
								<li class="dropdown-title" style="padding: 5px 0px 5px 15px;">You have {{ count(Helper::UnassignLeadForm()) }} Lead forms not assigned</li>
								@php $jk=1; @endphp
								@foreach(Helper::UnassignLeadForm() as $leadForm)
								<li>
									<a href="{{ route('clients') }}" class="message icon-not @if($jk%2==0){{ 'striped-col' }}@endif">
										<div class="message-body" style="font-size:12px">
											<strong>{{ $jk++ }}. {{ $leadForm->name }}</strong>
										</div>
									</a>
								</li>
								@endforeach
								@endif
								
								@if(count(Helper::UnassignLeadFormGroup())>0)
								<li class="dropdown-title" style="padding: 5px 0px 5px 15px;">You have {{ count(Helper::UnassignLeadFormGroup()) }} Group not assigned Lead forms</li>
								
								@php $jk=1; @endphp
								@foreach(Helper::UnassignLeadFormGroup() as $leadGroup)
								<li>
									<a href="settings?id={{ $leadGroup->id }}" class="message icon-not @if($jk%2==0){{ 'striped-col' }}@endif">
										<div class="message-body" style="font-size:12px">
											<strong>{{ $jk++ }}. {{ $leadGroup->name }}</strong>
										</div>
									</a>
								</li>
								@endforeach
								@endif
								
								@if(count(Helper::UnassignLeadFormClient())>0)
								<li class="dropdown-title" style="padding: 5px 0px 5px 15px;">You have {{ count(Helper::UnassignLeadFormClient()) }} Client not assigned Lead forms</li>
								
								@php $jk=1; @endphp
								@foreach(Helper::UnassignLeadFormClient() as $leadClient)
								<li>
									<a href="clients?id={{ $leadClient->id }}" class="message icon-not @if($jk%2==0){{ 'striped-col' }}@endif">
										<div class="message-body" style="font-size:12px">
											<strong>{{ $jk++ }}. {{ $leadClient->name }}</strong>
										</div>
									</a>
								</li>
								@endforeach
								@endif
								
                                @if(count($notification_recurring)>0)
                                <li class="dropdown-title" style="padding: 5px 0px 5px 15px;">You Have <span>{{ count($notification_recurring) }}</span> Recurring Invoice  Notification</li>
                                @endif
								@php $jk=1; @endphp
                                @foreach($notification_recurring as $notify)
                                <li>
									<a href="{{route('mark_read_notification',['type'=>'recurring_invoice_sent','id'=>$notify->invoice_id])}}" class="message icon-not @if($jk%2==0){{ 'striped-col' }}@endif">
										<div class="message-body" style="font-size:12px">
										<strong>{{ $jk++ }}.</strong>	New recurring invoice 	<strong>{{ $notify->invoice_num }}</strong> sent.
										</div>
									</a>
								</li>
                                @endforeach
								<!--<li class="dropdown-footer"><a href="#"> View All Notifications</a></li>-->
							</ul>
						</li>
						@endif
						
						<!-- User Account: style can be found in dropdown-->
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle padding-user" data-toggle="dropdown">
								@if(Session::get('photo')!='')
								@if(Session::get('user_category')=='admin')
								<img src="http://big.pownder.com/pownder/storage/app/uploads/admins/{{Session::get('photo')}}" width="35" class="img-circle img-responsive pull-left" height="35" alt="User Image">
								@elseif(Session::get('user_category')=='vendor')
								<img src="http://big.pownder.com/pownder/storage/app/uploads/vendors/{{Session::get('photo')}}" width="35" class="img-circle img-responsive pull-left" height="35" alt="User Image">
								@elseif(Session::get('user_category')=='client')
								<img src="http://big.pownder.com/pownder/storage/app/uploads/clients/{{Session::get('photo')}}" width="35" class="img-circle img-responsive pull-left" height="35" alt="User Image">
								@elseif(Session::get('user_category')=='user')
								<img src="http://big.pownder.com/pownder/storage/app/uploads/users/{{Session::get('photo')}}" width="35" class="img-circle img-responsive pull-left" height="35" alt="User Image">
								@endif
								@else
								<img src="http://big.pownder.com/pownder/storage/app/uploads/default.png" width="35"
								class="img-circle img-responsive pull-left" height="35" alt="User Image"">
								@endif
								
								<div class="riot">
								<div>
									{{ Session::get('user_name') }} 
									<span>
										<i class="caret"></i>
									</span>
								</div>
							</div>
						</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header">
								@if(Session::get('photo')!='')
								@if(Session::get('user_category')=='admin')
								<img src="http://big.pownder.com/pownder/storage/app/uploads/admins/{{Session::get('photo')}}" class="img-circle" alt="User Image">
								@elseif(Session::get('user_category')=='vendor')
								<img src="http://big.pownder.com/pownder/storage/app/uploads/vendors/{{Session::get('photo')}}" class="img-circle" alt="User Image">
								@elseif(Session::get('user_category')=='client')
								<img src="http://big.pownder.com/pownder/storage/app/uploads/clients/{{Session::get('photo')}}" class="img-circle" alt="User Image">
								@elseif(Session::get('user_category')=='user')
								<img src="http://big.pownder.com/pownder/storage/app/uploads/users/{{Session::get('photo')}}" class="img-circle" alt="User Image">								
								@endif
								@else
								<img src="http://big.pownder.com/pownder/storage/app/uploads/default.png" class="img-circle" alt="User Image">
								@endif
								<p> {{ Session::get('user_name') }} </p>
							</li>
							<!-- Menu Body -->
							<!--<li class="p-t-3">
								<a href="#"> <i class="fa fa-fw fa-user"></i> My Profile </a>
								</li>
							<li role="presentation"></li>-->
							<li class="p-t-3">
								@if(Session::get('user_category')=='admin')
								<a href="{{ route('edit-admin') }}"> <i class="fa fa-fw fa-user"></i> My Profile</a>
								@elseif(Session::get('user_category')=='vendor')
								<a href="{{ route('edit-vendor') }}"> <i class="fa fa-fw fa-user"></i> My Profile</a>
								@elseif(Session::get('user_category')=='client')
								<a href="{{ route('edit-client') }}"> <i class="fa fa-fw fa-user"></i> My Profile</a>
								@elseif(Session::get('user_category')=='user')
								<a href="{{ route('edit-user') }}"> <i class="fa fa-fw fa-user"></i> My Profile</a>
								@endif
							</li>
							<li role="presentation" class="divider"></li>
							<!-- Menu Footer-->
							<li class="user-footer">
								<div class="pull-left">
									<a href="#">
										<i class="fa fa-fw fa-lock"></i>
										Lock
									</a>
								</div>
								<div class="pull-right">
									<a href="{{ route('logout') }}">
										<i class="fa fa-fw fa-sign-out"></i>
										Logout
									</a>
								</div>
							</li>
						</ul>
					</li>
				</ul>
			</div>
			
			<!------filter sec ------>
			<div id="date-filter-section" class="date-filter-section">
				<div class="date-filter-wrapp">
					<div class="date-filter">
						<div class="date-filter-left">
							<div class="filter-with-day">
								<ul class="list">
									<li class="select-life-time">Lifetime</li>
									<li class="active select-today">Today</li>
									<li class="select-yesterday">Yesterday</li>
									<li class="select-7days">Last 7 Day</li>
									<li class="select-14days">Last 14 Day</li>
									<li class="select-30days">Last 30 Day</li>
									<li class="select-week">This Week</li>
									<li class="select-last-week">Last Week</li>
									<li class="select-this-month">This Month</li>
									<li class="select-last-month">Last Month</li>
									<li class="button-clear">Custom</li>
								</ul>
							</div>
						</div>  
						<div class="date-filter-right" >
							<div class="filter-by-daterange" id="filter-by-daterange">
								@if(app('request')->exists('date_range'))
								<input id="date-range-header" type="text" value="{{ app('request')->date_range }}">
								@else
								<input id="date-range-header" type="text">
								@endif
								<div id="date-range-header-container" style=""></div>
							</div>
						</div>
						<div class="button-section">
							<div>
								<button class="btn button-select">Select</button>
								<button class="btn button-clear">Clear</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-------filter sec ------>
		</nav>
	</header>
	<!-- For horizontal menu -->
	@yield('horizontal_header')
	<!-- horizontal menu ends -->
	<div class="wrapper row-offcanvas row-offcanvas-left">
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="left-side sidebar-offcanvas ">
			<!-- sidebar: style can be found in sidebar-->
			<section class="sidebar">
				<div id="menu" role="navigation">
					
					<!--
						<div class="nav_profile fold">
						<div class="media profile-left">
						<a class="pull-left profile-thumb" href="#">
						<img src="{{asset('assets/img/authors/avatar1.jpg')}}" class="img-circle" alt="User Image">
						</a>
						</div>
						</div>
					-->
					
					<div class="nav_profile">
						<div class="media profile-left">
							<a class="pull-left profile-thumb" href="#">
								@if(Session::get('photo')!='')
								@if(Session::get('user_category')=='admin')
								<img src="http://big.pownder.com/pownder/storage/app/uploads/admins/{{Session::get('photo')}}" class="img-circle" alt="User Image">
								@elseif(Session::get('user_category')=='vendor')
								<img src="http://big.pownder.com/pownder/storage/app/uploads/vendors/{{Session::get('photo')}}" class="img-circle" alt="User Image">
								@elseif(Session::get('user_category')=='client')
								<img src="http://big.pownder.com/pownder/storage/app/uploads/clients/{{Session::get('photo')}}" class="img-circle" alt="User Image">
								@elseif(Session::get('user_category')=='user')
								<img src="http://big.pownder.com/pownder/storage/app/uploads/users/{{Session::get('photo')}}" class="img-circle" alt="User Image">
								@endif
								@else
								<img src="http://big.pownder.com/pownder/storage/app/uploads/default.png" class="img-circle" alt="User Image">
								@endif
							</a>
							<div class="content-profile">
								<h4 class="media-heading">
									{{ Session::get('user_name') }} 
								</h4>
								<ul class="icon-list">
									<li>
										<a href="#">
											<i class="fa fa-fw fa-user"></i>
										</a>
									</li>
									<li>
										<a href="#">
											<i class="fa fa-fw fa-lock"></i>
										</a>
									</li>
									<li>
										<a href="#">
											<i class="fa fa-fw fa-gear"></i>
										</a>
									</li>
									<li>
										<a href="{{ route('logout') }}">
											<i class="fa fa-fw fa-sign-out"></i>                                    
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					@if(Session::get('user_category')=='user')
					<ul class="navigation">
						@if(ManagerHelper::ManagerMenuVisible('Dashboard')=='Yes')
						@if(ManagerHelper::ManagerCategory()=='admin')
						<li {!! (Request::is('dashboard')? 'class="active"': '') !!}>
							<a href="{{ route('dashboard') }}">
								<i class="menu-icon fa fa-fw fa-tachometer"></i>
								<span class="mm-text"> Dashboard </span>
							</a>
						</li>
						@elseif(ManagerHelper::ManagerCategory()=='vendor')
						<li {!! (Request::is('vdash')? 'class="active"': '') !!}>
							<a href="{{ route('vdash') }}">
								<i class="menu-icon fa fa-fw fa-tachometer"></i>
								<span class="mm-text"> Dashboard </span>
							</a>
						</li>
						@elseif(ManagerHelper::ManagerCategory()=='client')
						<li {!! (Request::is('cdash')? 'class="active"': '') !!}>
							<a href="{{ route('cdash') }}">
								<i class="menu-icon fa fa-fw fa-tachometer"></i>
								<span class="mm-text"> Dashboard </span>
							</a>
						</li>
						@endif
						@endif
						
						@if(ManagerHelper::ManagerMenuVisible('Campaigns')=='Yes')
						<li {!! (Request::is('campaigns')? 'class="active"': '') !!}>
							<a href="{{ URL::to('campaigns') }} ">
								<i class="menu-icon fa fa-fw fa-sitemap"></i>
								<span class="mm-text">Campaigns</span>
							</a>
						</li>
						@endif
						
						@if(ManagerHelper::ManagerMenuVisible('Leads')=='Yes')
						@if(ManagerHelper::ManagerCategory()=='client')
						<li {!! (Request::is('cleads')? 'class="active"': '') !!}>
							<a href="{{ URL::to('cleads') }} ">
								<i class="menu-icon fa fa-fw fa-list"></i>
								<span class="mm-text">Leads</span>
							</a>
						</li>
						@else
						<li {!! (Request::is('leads')? 'class="active"': '') !!}>
							<a href="{{ URL::to('leads') }} ">
								<i class="menu-icon fa fa-fw fa-list"></i>
								<span class="mm-text">Leads</span>
							</a>
						</li>
						@endif
						@endif
                        
                        @if(ManagerHelper::ManagerMenuVisible('Closed Deals')=='Yes')
						<li {!! (Request::is('closed-deals')? 'class="active"': '') !!}>
							<a href="{{ URL::to('closed-deals') }} ">
								<i class="menu-icon fa fa-fw fa-file-excel-o"></i>
								<span class="mm-text">Closed Deals</span>
							</a>
						</li>
						@endif
						
						@if(ManagerHelper::ManagerMenuVisible('Call Track')=='Yes')
						<li {!! (Request::is('calltrack')? 'class="active"': '') !!}>
							<a href="{{ URL::to('calltrack') }} ">
								<i class="menu-icon fa fa-fw fa-phone"></i>
								<span class="mm-text">Call Track</span>
							</a>
						</li>
						@endif
						
						@if(ManagerHelper::ManagerMenuVisible('FB Messenger')=='Yes')
						<li {!! (Request::is('index2')? 'class="active"': '') !!}>
							<a href="{{ URL::to('#') }} ">
								<i class="menu-icon fa fa-fw fa-facebook black"></i>
								<span class="mm-text">FB Messenger</span>
							</a>
						</li>
						@endif
						
						@if(ManagerHelper::ManagerMenuVisible('Reports')=='Yes')
						<li {!! (Request::is('reports') ? 'class="active"' : '') !!}>
							<a href="{{ URL::to('reports') }}">
								<i class="menu-icon fa fa-fw fa-bar-chart-o"></i>
								<span class="mm-text">Reports</span> 
							</a>
						</li>
						@endif
						
						@if(ManagerHelper::ManagerMenuVisible('Clients')=='Yes')
						<li {!! (Request::is('clients') ? 'class="active"' : '') !!}>
							<a href="{{ URL::to('clients') }}">
								<i class="menu-icon fa fa-fw fa-suitcase"></i>
								<span class="mm-text">Clients</span> 
							</a>
						</li> 
						@endif
						
						@if(ManagerHelper::ManagerMenuVisible('Vendors')=='Yes')
						<li {!! (Request::is('vendors') ? 'class="active"' : '') !!}>
							<a href="{{ URL::to('vendors') }}">
								<i class="menu-icon fa fa-fw fa-group"></i>
								<span class="mm-text">Vendors</span> 
							</a>
						</li> 
						@endif						
						
                        @if(ManagerHelper::ManagerMenuVisible('Invoice')=='Yes')
                        <li {!! ((Request::is('invoice') || Request::is('invoice-report') || Request::is('manage-invoice') || Request::is('view-invoice/*/*'))? 'class="active"': '') !!}>
							<a href="{{ route('invoice') }}">
								<i class="menu-icon fa fa-fw fa-list-alt"></i>
								<span class="mm-text">Invoice</span>
							</a>
						</li>
						@endif
						
						@if(ManagerHelper::ManagerMenuVisible('Expense')=='Yes')
						<li {!! (Request::is('expenses')? 'class="active"': '') !!}>
							<a href="{{ route('expenses') }}">
								<i class="menu-icon fa fa-fw fa-money"></i>
								<span class="mm-text">Expense</span>
							</a>
						</li>
						@endif
						
						@if(ManagerHelper::ManagerMenuVisible('Setting')=='Yes')
						<li {!! (Request::is('settings')? 'class="active"': '') !!}>
							<a href="{{ route('settings') }}">
								<i class="menu-icon fa fa-fw fa-cogs"></i>
								<span class="mm-text">Settings</span>
							</a>
						</li>
						@endif
					</ul>
					@else
					<ul class="navigation">
						@if(Session::get('user_category')=='admin')
						<li {!! (Request::is('dashboard')? 'class="active"': '') !!}>
							<a href="{{ route('dashboard') }}">
								<i class="menu-icon fa fa-fw fa-tachometer"></i>
								<span class="mm-text"> Dashboard </span>
							</a>
						</li>
						@elseif(Session::get('user_category')=='vendor')
						<li {!! (Request::is('vdash')? 'class="active"': '') !!}>
							<a href="{{ route('vdash') }}">
								<i class="menu-icon fa fa-fw fa-tachometer"></i>
								<span class="mm-text"> Dashboard </span>
							</a>
						</li>
						@elseif(Session::get('user_category')=='client')
						<li {!! (Request::is('cdash')? 'class="active"': '') !!}>
							<a href="{{ route('cdash') }}">
								<i class="menu-icon fa fa-fw fa-tachometer"></i>
								<span class="mm-text"> Dashboard </span>
							</a>
						</li>
						@endif
						@if(Session::get('user_category')=='vendor' || Session::get('user_category')=='admin')
						<li {!! (Request::is('campaigns')? 'class="active"': '') !!}>
							<a href="{{ URL::to('campaigns') }} ">
								<i class="menu-icon fa fa-fw fa-sitemap"></i>
								<span class="mm-text">Campaigns</span>
							</a>
						</li>
						@endif
						@if(Session::get('user_category')=='client')
						<li {!! (Request::is('cleads')? 'class="active"': '') !!}>
							<a href="{{ URL::to('cleads') }} ">
								<i class="menu-icon fa fa-fw fa-list"></i>
								<span class="mm-text ">Leads</span>
							</a>
						</li>
						@else
						<li {!! (Request::is('leads')? 'class="active"': '') !!}>
							<a href="{{ URL::to('leads') }} ">
								<i class="menu-icon fa fa-fw fa-list"></i>
								<span class="mm-text">Leads</span>
							</a>
						</li>
						<li {!! (Request::is('dispute-lead-list')? 'class="active"': '') !!}>
							<a href="{{ URL::to('dispute-lead-list') }} ">
								<i class="menu-icon fa fa-fw fa-thumbs-o-down"></i>
								<span class="mm-text">Dispute Leads</span>
							</a>
						</li>
                        @endif
						
						@if(Session::get('user_category')=='admin')
						<li {!! (Request::is('appointments')? 'class="active"': '') !!}>
							<a href="{{ URL::to('appointments') }} ">
								<i class="menu-icon fa fa-fw fa-calendar"></i>
								<span class="mm-text">Appointment</span>
							</a>
						</li>
						@endif
						
                        <li {!! (Request::is('closed-deals')? 'class="active"': '') !!}>
							<a href="{{ URL::to('closed-deals') }} ">
								<i class="menu-icon fa fa-fw fa-file-excel-o"></i>
								<span class="mm-text">Closed Deals</span>
							</a>
						</li>                                                
						
                        <li {!! (Request::is('calltrack')? 'class="active"': '') !!}>
							<a href="{{ URL::to('calltrack') }} ">
								<i class="menu-icon fa fa-fw fa-phone"></i>
								<span class="mm-text">Call Track</span>
							</a>
						</li>
						
						<li {!! (Request::is('index2')? 'class="active"': '') !!}>
							<a href="{{ URL::to('#') }} ">
								<i class="menu-icon fa fa-fw fa-facebook black"></i>
								<span class="mm-text">FB Messenger</span>
							</a>
						</li>
						
						<li {!! (Request::is('reports') ? 'class="active"' : '') !!}>
							<a href="{{ URL::to('reports') }}">
								<i class="menu-icon fa fa-fw fa-bar-chart-o"></i><span class="mm-text ">Reports</span> 
							</a>
						</li>
						
						@if(Session::get('user_category')=='admin')
						<li {!! (Request::is('clients') ? 'class="active"' : '') !!}>
							<a href="{{ URL::to('clients') }}">
								<i class="menu-icon fa fa-fw fa-suitcase"></i>
								<span class="mm-text">Clients</span> 
							</a>
						</li>
						@elseif(Session::get('user_category')=='vendor')
						<li {!! (Request::is('clients') ? 'class="active"' : '') !!}>
							<a href="{{ URL::to('clients') }}">
								<i class="menu-icon fa fa-fw fa-suitcase"></i>
								<span class="mm-text">Clients</span> 
							</a>
						</li>
						@endif
						
						@if(Session::get('user_category')=='admin')
						<li {!! (Request::is('vendors') ? 'class="active"' : '') !!}>
							<a href="{{ URL::to('vendors') }}">
								<i class="menu-icon fa fa-fw fa-group"></i><span class="mm-text ">Vendors</span> 
							</a>
						</li> 
						@endif
						
                        @if(Session::get('user_category')=='admin' || Session::get('user_category')=='vendor')
						<li {!! ((Request::is('invoice') || Request::is('invoice-report') || Request::is('manage-invoice') || Request::is('view-invoice/*/*'))? 'class="active"': '') !!}>
							<a href="{{ route('invoice') }}">
								<i class="menu-icon fa fa-fw fa-list-alt"></i>
								<span class="mm-text">Invoice</span>
							</a>
						</li>
						<li {!! (Request::is('expenses')? 'class="active"': '') !!}>
							<a href="{{ route('expenses') }}">
								<i class="menu-icon fa fa-fw fa-money"></i>
								<span class="mm-text">Expense</span>
							</a>
						</li>
						@endif
						
						@if(Session::get('user_category')=='admin')
                        
                        <li {!! (Request::is('manage-bot')? 'class="active"': '') !!}>
							<a href="{{ route('manage-bot') }}">
								<i class="menu-icon fa fa-fw fa-android"></i>
								<span class="mm-text"> Manage Bot</span>
							</a>
						</li>
                        
						<li {!! (Request::is('user')? 'class="active"': '') !!}>
							<a href="{{ route('user') }}">
								<i class="menu-icon fa fa-fw fa-user-plus"></i>
								<span class="mm-text"> User</span>
							</a>
						</li>
						@elseif(Session::get('user_category')=='vendor')
						<li {!! (Request::is('vuser')? 'class="active"': '') !!}>
							<a href="{{ route('vuser') }}">
								<i class="menu-icon fa fa-fw fa-user-plus"></i>
								<span class="mm-text"> User</span>
							</a>
						</li>
						@elseif(Session::get('user_category')=='client')
						<li {!! (Request::is('cuser')? 'class="active"': '') !!}>
							<a href="{{ route('cuser') }}">
								<i class="menu-icon fa fa-fw fa-user-plus"></i>
								<span class="mm-text"> User</span>
							</a>
						</li>
						@endif
						@if(Session::get('user_category')=='admin' || Session::get('user_category')=='vendor')
						@if(Session::get('vendor_id') != '13')
						<li {!! (Request::is('ftp-account')? 'class="active"': '') !!}>
							<a href="{{ route('ftp-account') }}">
								<i class="menu-icon fa fa-fw fa-file-text-o"></i>
								<span class="mm-text">FTP Connect</span>
							</a>
						</li>
						@endif
						@endif
						@if(Session::get('user_category')=='admin' || Session::get('user_category')=='vendor')
						<li {!! (Request::is('settings')? 'class="active"': '') !!}>
							<a href="{{ route('settings') }}">
								<i class="menu-icon fa fa-fw fa-cogs"></i>
								<span class="mm-text">Settings</span>
							</a>
						</li>
						@else
						<li {!! (Request::is('csettings')? 'class="active"': '') !!}>
							<a href="{{ route('csettings') }}">
								<i class="menu-icon fa fa-fw fa-cogs"></i>
								<span class="mm-text">Settings</span>
							</a>
						</li>
						@endif
					</ul>
					@endif
					<!-- / .navigation -->
				</div>
				<!-- menu -->
			</section>
			<!-- /.sidebar -->
		</aside>
		<aside class="right-side">
			<!-- Content -->
			@yield('content')
		</aside>
		<!-- page wrapper-->
	</div>
	<!-- wrapper-->
	
	<!-- begining of page level js -->
	<script src="{{asset('assets/js/backstretch.js')}}"></script>
	<script src="{{asset('assets/vendors/countupcircle/js/jquery.countupcircle.js')}}" type="text/javascript"></script>	
	<!--Sparkline Chart-->
	<script src="{{asset('assets/js/custom_js/sparkline/jquery.flot.spline.js')}}"></script>
	<!--c3 and d3 chart-->
	<script type="text/javascript" src="{{asset('assets/vendors/c3/c3.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/vendors/d3/d3.min.js')}}"></script>
	<!--nvd3 charts-->
	<script type="text/javascript" src="{{asset('assets/vendors/nvd3/js/nv.d3.min.js')}}"></script>
	<!--advanced news ticker-->
	<script type="text/javascript" src="{{asset('assets/vendors/advanced_newsTicker/js/newsTicker.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/dashboard2.js')}}" ></script>
	
	<!--Sweet Alert-->
	<script type="text/javascript" src="{{asset('assets/vendors/sweetalert2/js/sweetalert2.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/js/custom_js/sweetalert.js')}}"></script>
	
	<!--maps-->
	<script src="{{asset('assets/vendors/bower-jvectormap/js/jquery-jvectormap-1.2.2.min.js')}}"></script>
	<script src="{{asset('assets/vendors/bower-jvectormap/js/jquery-jvectormap-world-mill-en.js')}}"></script>
	<!-- end of maps -->
	
	<!-- date range piker -->
	<script src="{{asset('assets/vendors/moment/js/moment.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('assets/vendors/moment/js/moment-timezone-with-data.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/jquerydaterangepicker/js/jquery.daterangepicker.min.js')}}"></script>
	<!--<script  type="text/javascript" src="{{asset('assets/js/custom_js/datepickers.js')}}" ></script> -->
	
	<script>
		//moment.tz.setDefault("America/Los_Angeles");
		//var moment = require('moment-timezone');
		moment().tz("America/Los_Angeles").format();
	    $('body').click(function(e) {
			$('#date-filter-section').removeClass("open");
		});
		
		$('#open_date_filter').click(function(e) {
			$(".messages-menu").removeClass("open");
		    e.stopPropagation();
		});
		
		$('#date-filter-section').click(function(e) {
		    e.stopPropagation();
		});
		
	    jQuery("filter-by-daterange").click(function(c) {
			var target = c.target.id; 
		    alert(target);
			switch(target) {
				case "filter-by-daterange" :
				$(".filter-with-day .list li").removeClass("active");
				$(".filter-with-day .list li.button-clear").addClass("active");
				break;
				default:
				$(".filter-with-day .list li").removeClass("active");
				$(".filter-with-day .list li.button-clear").addClass("active");
			}
		});		
	    
		$(".filter-with-day .list li").click(function(){
		    $(".filter-with-day .list li").removeClass("active");
			$(this).addClass("active");
		}); 
		
		$("#open_date_filter").click(function(){
			$(this).find("")
		    $("#date-filter-section").toggleClass("open");
		});
		
		$('.button-select').click(function(evt){
			$('#date-filter-section').removeClass("open");
		});
		
		
		$('#date-range-header').dateRangePicker({
			format: 'YYYY-MM-DD',
			inline:true,
			//timeZone: 'America/Los_Angeles',
	        container: '#date-range-header-container',
	        alwaysOpen:true,
			getValue: function () {
				var range = $(this).val();
				//alert(range);
			}
		});
		
		$('.button-clear').click(function(evt){
			evt.stopPropagation();
			$('#date-range-header').data('dateRangePicker').clear();
		});
		
		$('.select-life-time').click(function(evt){
		    evt.stopPropagation();
			var start = new Date('0000','00','01');
			var end = moment().toDate();
			$('#date-range-header').data('dateRangePicker').setDateRange(start,end);
		});
		$('.select-today').click(function(evt){
		    evt.stopPropagation();
			var start = moment().toDate();
			var end = moment().toDate();
			$('#date-range-header').data('dateRangePicker').setDateRange(start,end);
		});
		$('.select-yesterday').click(function(evt){
		    evt.stopPropagation();
			var today = moment().toDate();
			var newdate = new Date(today);
            newdate.setDate(newdate.getDate() - 1);
			var start = new Date(newdate);
			var end = new Date(newdate);
			$('#date-range-header').data('dateRangePicker').setDateRange(start,end);
		});
		$('.select-7days').click(function(evt){
		    evt.stopPropagation();
			/*--last 7day---*/
			var end = moment().toDate();
			var newdate = new Date(end);
            newdate.setDate(newdate.getDate() - 6);
			var start = new Date(newdate);
			/*prev 7day*/
			/*
				var today = moment().toDate();
				var newdate1 = new Date(today);
				newdate1.setDate(newdate1.getDate() - 1);
				var start = new Date(newdate1);
				var newdate = new Date(today);
				newdate.setDate(newdate.getDate() - 7);
				var end = new Date(newdate);
			*/
			$('#date-range-header').data('dateRangePicker').setDateRange(start,end);
		});
		$('.select-14days').click(function(evt){
		    evt.stopPropagation();
			var end = moment().toDate();
			var newdate = new Date(end);
            newdate.setDate(newdate.getDate() - 13);
			var start = new Date(newdate);
			$('#date-range-header').data('dateRangePicker').setDateRange(start,end);
		});
		$('.select-30days').click(function(evt){
		    evt.stopPropagation();
			var end = moment().toDate();
			var newdate = new Date(end);
            newdate.setDate(newdate.getDate() - 29);
			var start = new Date(newdate);
			$('#date-range-header').data('dateRangePicker').setDateRange(start,end);
		});
		$('.select-week').click(function(evt){
			evt.stopPropagation();
			var start = moment().day(0).toDate();
			var end = moment().day(6).toDate();
	        $('#date-range-header').data('dateRangePicker').setDateRange(start,end);
		});	
		$('.select-last-week').click(function(evt){
			evt.stopPropagation();
			var start = moment().day(-7).toDate();
			var end = moment().day(-1).toDate();
	        $('#date-range-header').data('dateRangePicker').setDateRange(start,end);
		});	
		$('.select-this-month').click(function(evt){
			var date = new Date(), y = date.getFullYear(), m = date.getMonth();
            var start = new Date(y, m, 1);
            var end = new Date(y, m + 1, 0);
	        $('#date-range-header').data('dateRangePicker').setDateRange(start,end);
		});
		$('.select-last-month').click(function(evt){
			var date = new Date(), y = date.getFullYear(), m = date.getMonth();
            var start = new Date(y, m - 1, 1);
            var end = new Date(y, m, 0);
	        $('#date-range-header').data('dateRangePicker').setDateRange(start,end);
		});
	</script>
	
	<script>
		$(function(){
			$(".account-list ul li").click(function() {
				//alert("ok");
				//var  a_acc = $(this);
				//$(".account-list ul li").removeClass("active");
				//$(a_acc).addClass("active");
			});
			
			$(".client-list ul li").click(function() {
				//alert("ok");
				var  a_acc = $(this);
				$(".client-list ul li").removeClass("active");
				$(a_acc).addClass("active");
				var client_id = $(this).attr('user_client_id');
				$.get("{{ route('client-header-filter') }}",{client_id: client_id},function(data){ 
					//
				});
			});
			
            $('.reset_lead').click(function(){
                $(".client-list ul li").removeClass("active");
				$.get("{{ route('client-header-filter') }}",{client_id: ''},function(data){ 
					//
				});
			});
			
			$(".resync_error").click(function(){
				var id = $(this).data('value');
				$('li').filter('[data-resync="'+id+'"]').hide();
				var resyncCount = $('#resyncCount').html();
				$('#resyncCount').html(resyncCount-1);
				$('.resyncCount').html(resyncCount-1);
				$.get("{{ route('remove-re-sync-error') }}",{id: id},function(data){ 
					//action section
				});
			});
			
			@if(Session::get('client_id')>0)
			$('.reset_lead').removeClass('btn-primary');
			$('.reset_lead').addClass('btn-warning');
			@else
			$('.reset_lead').addClass('btn-primary');
			$('.reset_lead').removeClass('btn-warning');
			@endif
		});
	</script>
	
	<script>
		function removeFacebookAccount(facebook_account_id,facebook_account_name)
		{
			swal({
				title: 'Are you sure?',
				text: "You want to remove "+facebook_account_name+" facebook account?",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#22D69D',
				cancelButtonColor: '#FB8678',
				confirmButtonText: 'Yes, remove it!',
				cancelButtonText: 'No, cancel!',
				confirmButtonClass: 'btn',
				cancelButtonClass: 'btn'
				}).then(function () {
				$.get("{{ route('remove_facebook_account') }}",{'facebook_account_id':facebook_account_id}).then(function(response){
					swal('Removed!', facebook_account_name+' facebook account removed successfully.', 'success');
					window.location.reload(true);
				});
				}, function (dismiss) {
				// dismiss can be 'cancel', 'overlay',
				// 'close', and 'timer'
				if (dismiss === 'cancel') {
					swal('Cancelled', 'Your '+facebook_account_name+' facebook account is safe.', 'error');
				}
			});
		}
	</script>
	@yield('footer_scripts')
	<!-- end page level js -->
</body>
</html>