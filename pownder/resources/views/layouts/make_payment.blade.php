<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>
			@section('title')
            | Big Pownder� 
			@show
		</title>
		<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		<link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}"/>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<!-- global css -->
		<link rel="stylesheet" type="text/css" media="all"  href="{{asset('assets/css/app.css')}}" />
        <link rel="stylesheet" type="text/css" media="all"  href="{{asset('assets/css/custom.css')}}"  />
		<link rel="stylesheet" type="text/css" media="all"  href="{{asset('assets/css/custom_css/make_payment.css')}}" />
       	<!--Sweet Alert-->	
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/sweet-alert/sweetalert.css')}}"/>
	    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/iCheck/css/all.css')}}" />
       <link rel="stylesheet" type="text/css" href="http://fontawesome.io/assets/font-awesome/css/font-awesome.css">		
		
		@yield('header_styles')
		<!-- end of global css -->
	</head>
	<body class="skin-coreplus">
		<div class="preloader">
			<div class="loader_img"><img src="{{asset('assets/img/loader.gif')}}" alt="loading..." height="64" width="64"></div>
		</div>
		<!-- header logo: style can be found in header-->
		
	<!-- For horizontal menu -->
	@yield('horizontal_header')
	<!-- horizontal menu ends -->
	<div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Main content -->
        <section class="content">           
            @include('panel.includes.status')
            @yield('content')
        </section>        
                
	</div>
	<!-- wrapper-->
	<!-- global js -->
	<script src="{{asset('assets/js/app.js')}}" type="text/javascript"></script>	
	<!-- begining of page level js -->
	<!--Sweet Alert-->	
    <script type="text/javascript" src="{{asset('assets/css/sweet-alert/sweetalert.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/vendors/iCheck/js/icheck.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/custom_js/make_payment.js')}}"></script> 
    <!-- end of global js -->
	@yield('footer_scripts')
	<!-- end page level js -->
</body>
</html>