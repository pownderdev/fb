@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Users List
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/datatables/css/dataTables.bootstrap.css')}}"/>
    <!--end of page level css-->
@stop

{{-- Page content --}}
@section('content')
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Vendors List</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="index ">
                        <i class="fa fa-fw fa-home"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="#"> Vendor</a>
                </li>
                <li class="active">
                     Vendors  List
                </li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content p-l-r-15">
            <!--<div class="row" style=" margin-bottom: 15px;">
			     <div class="col-md-6 col-md-offset-6">
				     <div class="col-md-3">
					   <button class="btn btn-info">Mail </button>
		             </div>
					 <div class="col-md-9">
					    <div class="col-md-6">
						  
						  <input type="text" placeholder="From"  style="width:100%;padding:7px;">
						</div>  
						<div class="col-md-6">
						 
					      <input type="text" placeholder="To" style="width:100%;padding:7px;">
						</div>  
		             </div>
                 </div>
				 <br>
            </div>--->
			<div class="row">
                <div class="panel panel-primary ">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <i class="fa fa-fw fa-users"></i> Vendors List
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="table">
                                <thead>
                                 <tr class="filters">
                                    <th>Id</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Vendors</th>
                                    <th>Package</th>
                                    <th>Ads</th>
                                    <th>CP's</th>
                                    <th>LD's</th>
                                    <th>Login(icons)</th>
                                </tr>
                                </thead>
                                <tbody>
									<tr> 
										<td>101</td>
										<td>05/04/17</td>
										<td>Active</td>
										<td>Vendor 1</td>
										<td>Silver</td>
										<td>1</td>
										<td>15</td>
										<td>0</td>
										<td><a href="edit_user "><i
												class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a
												href="#"
												data-toggle="modal"
												data-target="#delete"><i
												class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
										</a><a
												href="user_profile "><i
												class="fa fa-fw fa-star text-success actions_icon"
												title="View User"></i></a></td>
									</tr>
									<tr> 
										<td>102</td>
										<td>07/04/17</td>
										<td>Active</td>
										<td>Vendor 2</td>
										<td>Silver</td>
										<td>1</td>
										<td>15</td>
										<td>0</td>
										<td><a href="edit_user "><i
												class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a
												href="#"
												data-toggle="modal"
												data-target="#delete"><i
												class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
										</a><a
												href="user_profile "><i
												class="fa fa-fw fa-star text-success actions_icon"
												title="View User"></i></a></td>
									</tr>
									<tr> 
										<td>103</td>
										<td>12/04/17</td>
										<td>Active</td>
										<td>Vendor 3</td>
										<td>Silver</td>
										<td>1</td>
										<td>15</td>
										<td>0</td>
										<td><a href="edit_user "><i
												class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a
												href="#"
												data-toggle="modal"
												data-target="#delete"><i
												class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
										</a><a
												href="user_profile "><i
												class="fa fa-fw fa-star text-success actions_icon"
												title="View User"></i></a></td>
									</tr>
									<tr> 
										<td>104</td>
										<td>22/04/17</td>
										<td>Active</td>
										<td>Vendor 4</td>
										<td>Silver</td>
										<td>1</td>
										<td>15</td>
										<td>0</td>
										<td><a href="edit_user "><i
												class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a
												href="#"
												data-toggle="modal"
												data-target="#delete"><i
												class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
										</a><a
												href="user_profile "><i
												class="fa fa-fw fa-star text-success actions_icon"
												title="View User"></i></a></td>
									</tr>
									<tr> 
										<td>105</td>
										<td>25/04/17</td>
										<td>Active</td>
										<td>Vendor 5</td>
										<td>Silver</td>
										<td>1</td>
										<td>15</td>
										<td>0</td>
										<td><a href="edit_user "><i
												class="fa fa-fw fa-pencil text-primary actions_icon" title="Edit User"></i></a><a
												href="#"
												data-toggle="modal"
												data-target="#delete"><i
												class="fa fa-fw fa-times text-danger actions_icon" title="Delete User"></i>
										</a><a
												href="user_profile "><i
												class="fa fa-fw fa-star text-success actions_icon"
												title="View User"></i></a></td>
									</tr>
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="Heading"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title custom_align" id="Heading">Delete User</h4>
                            </div>
                            <div class="modal-body">
                                <div class="alert alert-warning">
                                    <span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to
                                    delete this Account?
                                </div>
                            </div>
                            <div class="modal-footer ">
                                <a href="deleted_users " class="btn btn-danger">
                                    <span class="glyphicon glyphicon-ok-sign"></span> Yes
                                </a>
                                <button type="button" class="btn btn-success" data-dismiss="modal">
                                    <span class="glyphicon glyphicon-remove"></span> No
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- row-->
            @include('layouts.right_sidebar')
        </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/jquery.dataTables.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/vendors/datatables/js/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/js/custom_js/users_custom.js')}}"></script>
<!-- end of page level js -->
@stop