<?php
	include('vendor/autoload.php');
	
	$fb = new Facebook\Facebook([
	'app_id' => '246735105809032',
	'app_secret' => '629464d21ed809347c652afcdd014b9f',
	'default_graph_version' => 'v2.10',
	]);
	
	$adset_id = $_GET['adset_id'];
	$s_date = $_GET['s_date'];
	$access_token = $_GET['access_token'];
	
	$fb->setDefaultAccessToken($access_token);
	
	// Example : Get page impression
	$impression = $fb->request('GET', $adset_id.'/insights?fields=impressions&time_range[since]='.$s_date.'&time_range[until]='.$s_date);
	
	// Example : Get Organic page impressions
	$ctr = $fb->request('GET', $adset_id.'/insights?fields=ctr&time_range[since]='.$s_date.'&time_range[until]='.$s_date);
	$cpa = $fb->request('GET', $adset_id.'/insights?fields=cost_per_action_type&time_range[since]='.$s_date.'&time_range[until]='.$s_date);
	$click= $fb->request('GET', $adset_id.'/insights?fields=clicks&time_range[since]='.$s_date.'&time_range[until]='.$s_date);
	$spend = $fb->request('GET', $adset_id.'/insights?fields=spend&time_range[since]='.$s_date.'&time_range[until]='.$s_date);
	$frequency = $fb->request('GET', $adset_id.'/insights?fields=frequency&time_range[since]='.$s_date.'&time_range[until]='.$s_date);
	$reach = $fb->request('GET', $adset_id.'/insights?fields=reach&time_range[since]='.$s_date.'&time_range[until]='.$s_date);
	
	
	
	// Add more variable and API req, according to your list
	// $requestPageStories = $fb->request('GET','....');
	// etc...
	
	$batch = [
	'impression' => $impression,
	'ctr' => $ctr, 
	'cpa' => $cpa, 
	'click' => $click, 
	'spend' => $spend, 
	'frequency'=> $frequency,
	'reach'=>$reach,	
	// add more for your list
	];
	
	try {
		$responses = $fb->sendBatchRequest($batch);
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		// When Graph returns an error
		echo 'Graph returned an error: ' . $e->getMessage();
		exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		// When validation fails or other local issues
		echo 'Facebook SDK returned an error: ' . $e->getMessage();
		exit;
	}
	
	$data = array();
	foreach ($responses as $key => $response) {
		if ($response->isError()) {
			$e = $response->getThrownException();
			var_dump($e->getResponse());
			} else {
			//echo "<p>(" . $key . ") HTTP status code: " . $response->getHttpStatusCode() . "<br />\n";
			//echo "Response: " . $response->getBody() . "</p>\n\n";
			//echo "<hr />\n\n";
			
			$data[] = json_decode($response->getBody(), true);
		}
	}	
	echo json_encode($data);
	//print_r($data);
return json_encode($data);