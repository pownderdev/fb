<?php
	/* In this page we are making authorization of facebook account by using app_id, app_secret and redirect url. We get app id and app secret from developer site of facebook.  And there we also have to mention redirect url for authorization.*/
	
	session_start();
	include('vendor/autoload.php');
	
	$app_id = '246735105809032';//'644278432432729';//'459063747761965';
	$app_secret = '629464d21ed809347c652afcdd014b9f';//'c23b2c6fa48da08594ce64e5ae9eb521';//'98f9f19801d59a343b29200ea4255890';
	$redirect_url='http://big.pownder.com/fblogin/callback.php';
	
	$fb = new Facebook\Facebook([
	'app_id' => $app_id, 
	'app_secret' => $app_secret,
	'default_graph_version' => 'v2.10',
	]);
	
	$helper = $fb->getRedirectLoginHelper();
	
	// Optional permissions
	$permissions = ['email','pages_show_list','manage_pages','ads_read','ads_management','user_likes','publish_actions','publish_pages','pages_messaging']; 
	$loginUrl = $helper->getLoginUrl($redirect_url, $permissions);
	
	echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
?>