<?php
	session_start();
	
	include('vendor/autoload.php');
	
	use Facebook\Facebook;
	use Facebook\Exceptions\FacebookResponseException;
	use Facebook\Exceptions\FacebookSDKException;
	
	
	$fb = new Facebook([
	'app_id' => '459063747761965',
	'app_secret' => '98f9f19801d59a343b29200ea4255890',
	]);
	
	$helper = $fb->getRedirectLoginHelper();
	
	if (!isset($_SESSION['facebook_access_token'])) 
	{
		$_SESSION['facebook_access_token'] = null;
	}
	
	if (isset($_GET['state']))
	{
		//$helper->getPersistentDataHandler()->set('state', $_GET['state']);
	}
	
	if (!$_SESSION['facebook_access_token']) 
	{
		try 
		{
			$_SESSION['facebook_access_token'] = (string) $helper->getAccessToken();
		} 
		catch(FacebookResponseException $e) 
		{
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage();
			exit;
			} 
		catch(FacebookSDKException $e) 
		{
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
			exit;
		}
	}
	
	if ($_SESSION['facebook_access_token']) 
	{
		//echo "You are logged in!";
		header('location: http://big.pownder.com/facebook_redirect');
	} 
	else 
	{
		$permissions = ['public_profile', 'email','manage_pages','ads_read', 'ads_management'];
		$loginUrl = $helper->getLoginUrl('http://big.pownder.com/fblogin/test.php', $permissions);
		echo '<a href="' . $loginUrl . '">Log in with Facebook</a>';
		//header('location: http://big.pownder.com/facebook_redirect');
	} 
?>	