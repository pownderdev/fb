<?php

$lead=$conn->query('select full_name,city,email,phone_number from facebook_ads_lead_users where id="'.$facebook_ads_lead_user_id.'"');

if($lead->num_rows>0)
{
$lead_data=$lead->fetch_assoc();    
$admin = $conn->query('select receive_notification,name,new_lead_email from admins where id=1 and new_lead_email!="" and new_lead_option="Yes" and is_deleted=0 order by id desc limit 1');

if ($admin->num_rows>0) {
    $admin_data=$admin->fetch_assoc();
    
    $email=$lead_data['email'];
    $phone_number=$lead_data['phone_number'];
    $city=$lead_data['city'];
    $full_name=$lead_data['full_name'];
    
    if ($admin_data['receive_notification'] == 'Everytime') {
        
       $name=$admin_data->name; $phone = emailPhoneFormat($phone_number);
        
       $msg= file_get_contents(__DIR__.'/../../pownder/resources/views/emails/cron_new_lead.blade.php');
       
       /** Send Email */
           
            $urls = 'https://api.sendgrid.com/';
            $users = SENDGRID_USER;
            $passs = SENDGRID_PASS;
            $subject='NEW FACEBOOK LEAD';            
            $from='noreply@pownder.com';
                    
            $paramss = array(
            'api_user' => $users,
            'api_key' => $passs,            
            'subject' => $subject,
            'html' => $msg,
            'from' => $from,
            'fromname' => "Pownder�",
            'to'=>'priya.constacloud2@gmail.com',
           // 'to'=>$admin_data->new_lead_email            
            );
                        
            $requests = $urls.'api/mail.send.json';
            $sessions = curl_init($requests);
            curl_setopt ($sessions, CURLOPT_POST, true);
            curl_setopt ($sessions, CURLOPT_POSTFIELDS, $paramss);
            curl_setopt($sessions, CURLOPT_HEADER, false);
            curl_setopt($sessions, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
            curl_setopt($sessions, CURLOPT_RETURNTRANSFER, true);
            $responses = curl_exec($sessions);
            $info=curl_getinfo($sessions);
            curl_close($sessions);
       /** Send Email */
    
    } else {
        
        $already_notified=$conn->query('select id from admin_emails where facebook_ads_lead_user_id="'.$facebook_ads_lead_user_id.'" and leadType="Pownder� Messenger"');
        if(!$already_notified->num_rows)
        {
        $conn->query('insert into admin_emails set 
                    facebook_ads_lead_user_id="'.$facebook_ads_lead_user_id.'",
                    subject="NEW FACEBOOK LEAD",
                    client_name="",
                    leadType="Pownder� Messenger",
                    leadEmail = "'.$email.'",
                    leadPhone = "'.emailPhoneFormat($phone_number).'", 
                    leadCity = "'.$city.'",
                    first_name = "'.$full_name.'",
                    created_at ="'.$now.'",
                    is_deleted=1
                    ');
       }
       else
       {
         $conn->query('update admin_emails set 
                    leadEmail = "'.$email.'",
                    leadPhone = "'.emailPhoneFormat($phone_number).'", 
                    leadCity = "'.$city.'",
                    first_name = "'.$full_name.'",
                    updated_at ="'.$now.'",
                    is_deleted=1
                    where facebook_ads_lead_user_id="'.$facebook_ads_lead_user_id.'" and leadType="Pownder� Messenger"
                    ');
       }             
    }
}

}
?>