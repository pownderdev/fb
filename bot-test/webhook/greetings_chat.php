<?php
function CheckIfUserQuestionMatchedLIKE($question,$first_name,$conn)
{
//$check_answer=$conn->query('select answer,id from bot_answers where LOWER(REPLACE(question,"{First_Name}","'.$first_name.'")) LIKE "%'.$question.'%" and status=1');
$check_answer=$conn->query('select answer,id from bot_answers where FIND_IN_SET("'.$question.'",LOWER(REPLACE(question,"{First_Name}","'.$first_name.'"))) >0  and status=1');
//file_put_contents('priya.txt','select answer,id from bot_answers where FIND_IN_SET("'.$question.'",LOWER(REPLACE(question,"{First_Name}","'.$first_name.'"))) >0  and status=1');
if($check_answer->num_rows>0)
{
    $result_answer=$check_answer->fetch_assoc();
    $answer=$result_answer['answer'];$answer_id=$result_answer['id'];
    $answer=str_replace("{First_Name}",$first_name,$answer);
    $response=json_encode(["code"=>1,"answer"=>utf8_encode($answer),"answer_id"=>$answer_id]);
    $response=str_replace(["\r\n","\n"],'\n', $response);
    return $response;
}
return '{"code":0}';
}
function remove_garbage($key,$text)
{
   $key=trim(strtolower($key)); $text=trim(strtolower($text));
   if(is_numeric(strpos($text,$key)) && preg_match_all('/\s+'.$key.'[\s|\W|\d]+/',' '.$text.' '))
   {
     return strtolower($key);
   }  
  else if(is_numeric(strpos($key,$text)) && preg_match_all('/\s+'.$text.'[\s|\W|\d]+/',' '.$key.' ') )
   {
     return strtolower($key);
   }  
   return "**NA**";
}

function CheckIfUserQuestionMatchedByKeyword($question,$first_name,$conn,$default_lang='en',$fb_page_id,$fb_sender_id,$now)
{
    $answers=[];$answers_id_arr=[]; $matched_keyword='';  
    /** First Simply find Answer */
     $response=json_decode(CheckIfUserQuestionMatchedLIKE($question,$first_name,$conn)); 
     if($response->code)
     {        
        $rotate_answer=0;
        $answers_arr=explode('$%^',$response->answer);
        $matched_keyword=$question;
        /** Rotate Answer */
        $prev_answer=$conn->query('select id,answer_no from bot_rotate_answers where fb_page_id="'.$fb_page_id.'" and fb_sender_id="'.$fb_sender_id.'" and answer_id="'.$response->answer_id.'"'); 
               
        if($prev_answer->num_rows>0)
        {
          $prev_answer_row=$prev_answer->fetch_assoc();
          $prev_answer_no=$prev_answer_row['answer_no']; $prev_answer_id=$prev_answer_row['id']; 
          if(count($answers_arr)>$prev_answer_no+1)
          {
            $rotate_answer=$prev_answer_no+1;
          }
          
        }
        $response->answer=$answers_arr[$rotate_answer];
         
        if(!in_array($response->answer_id,$answers_id_arr))
        {
              $answers[]=$response->answer; 
                  $answers_id_arr[]=$response->answer_id;
                  /** Save  Answer Sequence i.e Rotaing Answers */
                  if($prev_answer->num_rows>0)
                  {
                  $conn->query('update bot_rotate_answers set answer_no="'.$rotate_answer.'",updated_at="'.$now.'" where id="'.$prev_answer_id.'"');
                  }
                  else
                  {
                  $conn->query('insert into bot_rotate_answers set fb_page_id="'.$fb_page_id.'",fb_sender_id="'.$fb_sender_id.'",answer_id="'.$response->answer_id.'",answer_no="'.$rotate_answer.'",created_at="'.$now.'"');
                  }                              
        
        }
     } 
     else
     {   
    
            $keywords=$conn->query('select group_concat(question) as keywords from bot_answers where status=1');
            if($keywords->num_rows>0)
            {
                $result_keywords=$keywords->fetch_assoc();
                $keywords_list=$result_keywords['keywords'];
                $keywords_list=explode(',',$keywords_list);
                foreach($keywords_list as $key=>$value)
                {
                $keywords_list[$key]=remove_garbage($value,$question);
                }
                $keywords_list=array_unique($keywords_list);
                //$keywords_list=check_exact($keywords_list,$question);
                $keywords_list=array_values($keywords_list);
               
                foreach($keywords_list as $key)
                {
                 if($key!="**NA**")
                 {   
                 $response=json_decode(CheckIfUserQuestionMatchedLIKE($key,$first_name,$conn)); 
                 if($response->code)
                 {
                    $matched_keyword=(!$matched_keyword) ? $key : $matched_keyword;
                    $rotate_answer=0;
                    $answers_arr=explode('$%^',$response->answer);
                    /** Rotate Answer */
                    $prev_answer=$conn->query('select id,answer_no from bot_rotate_answers where fb_page_id="'.$fb_page_id.'" and fb_sender_id="'.$fb_sender_id.'" and answer_id="'.$response->answer_id.'"'); 
                           
                    if($prev_answer->num_rows>0)
                    {
                      $prev_answer_row=$prev_answer->fetch_assoc();
                      $prev_answer_no=$prev_answer_row['answer_no']; $prev_answer_id=$prev_answer_row['id']; 
                      if(count($answers_arr)>$prev_answer_no+1)
                      {
                        $rotate_answer=$prev_answer_no+1;
                      }
                      
                    }
                    $response->answer=$answers_arr[$rotate_answer];
                     
                    if(!in_array($response->answer_id,$answers_id_arr))
                    {
                          $answers[]=$response->answer; 
                              $answers_id_arr[]=$response->answer_id;
                              /** Save  Answer Sequence i.e Rotaing Answers */
                              if($prev_answer->num_rows>0)
                              {
                              $conn->query('update bot_rotate_answers set answer_no="'.$rotate_answer.'",updated_at="'.$now.'" where id="'.$prev_answer_id.'"');
                              }
                              else
                              {
                              $conn->query('insert into bot_rotate_answers set fb_page_id="'.$fb_page_id.'",fb_sender_id="'.$fb_sender_id.'",answer_id="'.$response->answer_id.'",answer_no="'.$rotate_answer.'",created_at="'.$now.'"');
                              }
                    }
                 }
                 }  
                }
    
    
}
}
if(count($answers)>0)
{
      $answers=implode("\r\n",$answers);
      $response='{"code":1,"answer":"'.$answers.'","answer_id":"'.current($answers_id_arr).'","answer_no":"'.$rotate_answer.'","matched_keyword":"'.utf8_encode($matched_keyword).'"}'; 
      $response=str_replace(["\r\n","\n"],'\n', $response); 
      return $response;
}
    
return '{"code":0}';
}
?>