<?php 
header('content-type: application/json; charset=UTF-8');

/** API Credentials **/

define('DETECT_LANG_KEY','66d50e8b24fd288cc616130d7cb3626a');
define('SENDGRID_USER','help@constacloud.in'); //Sendgrid user name to send emails
define('ENDGRID_PASS','main1234');            //Sendgrid password to send emails
          
$default_lang='en';$question_lang='question_en';

require_once("dbconnect.php");

date_default_timezone_set('America/Los_Angeles');
$now=date('Y-m-d H:i:s');

function processMessage($update,$conn,$accessToken,$now,$question_lang='question_en') { 
    
       date_default_timezone_set('America/Los_Angeles');
    
       $count_questions=0;   
       $default_lang='en';
    
        $result=$conn->query("select id from questions  where status=1 and is_deleted=0 order by sequence asc");
        
        $count_questions=$result->num_rows;
        
        $query_parameters=$update["result"]["parameters"];
        $text=$update["result"]["resolvedQuery"];
        $nlp_entities=$update['originalRequest']['data']['message']['nlp']['entities'];
        
    
       if($update["result"]["action"] == "welcome"){
     
        if(array_key_exists("postback",$update["originalRequest"]["data"]) && $update["originalRequest"]["data"]["postback"]["payload"]=="FACEBOOK_WELCOME")
        {
        $user_details=getUserDetails($update["originalRequest"]["data"]["sender"]["id"],$accessToken);
        $first_name=$last_name=$gender=$profile_pic=$locale=$timezone='';
        if(isset($user_details->first_name))
        {
        $first_name=$user_details->first_name;$last_name=$user_details->last_name;$gender=$user_details->gender;$profile_pic=$user_details->profile_pic;
        $locale=$user_details->locale;$timezone=$user_details->timezone;
        }    
        
        if($count_questions)
        {   
         $question_result=$conn->query("select * from questions  where status=1 and is_deleted=0 order by sequence asc limit 1");
         $row_question=$question_result->fetch_assoc();        
         $question=str_replace("{First_Name}",$first_name,$row_question["question_en"]);  
         $question_id=$row_question["id"];         
         /** Close previously opened chat conversation */
         $conn->query('update messenger_answers  set chat_closed=1 where  senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'" and recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'"  and chat_closed=0 ');        
         $conn->query('delete from bot_rotate_answers  where  fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'" and fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'" ');        
         $conn->query('insert into messenger_answers  set chat_id="'.$update["id"].'", sessionId="'.$update["sessionId"].'", senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'",recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'", question_id="'.$question_id.'", first_name="'.$first_name.'", last_name="'.$last_name.'", profile_pic="'.$profile_pic.'" , locale="'.$locale.'" ,timezone="'.$timezone.'" ,gender="'.$gender.'" , created_at="'.$now.'"');            
         sendReply($update["originalRequest"]["data"]["sender"]["id"],$question,$accessToken);         
         $msg='';
         if(!$row_question['required'])
         {            
            $msg="If you don't want to answer, skip this just say skip";
            sendReply($update["originalRequest"]["data"]["sender"]["id"],$msg,$accessToken);
         }         
        //Save Chat Record
        $conn->query('insert into bot_chat_records set chat_id="'.$update["id"].'",sessionId="'.$update["sessionId"].'", fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'",fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'", text="'.$update["originalRequest"]["data"]["postback"]["title"].'" ,msg_by="user" , created_at="'.$now.'"');
        //Save Chat Record
        $conn->query('insert into bot_chat_records set chat_id="'.$update["id"].'",sessionId="'.$update["sessionId"].'", fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'",fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'", text="'.$question.'\r\n'.$msg.'" ,msg_by="bot" ,question_id="'.$question_id.'", created_at="'.$now.'"');
       
        }
        }
        else
        {            
          
            $result1=$conn->query('select A.*,B.input_format,B.required,B.sequence from messenger_answers A inner join questions B on A.question_id=B.id where sessionId="'.$update["sessionId"].'" and senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'" and recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'" and is_answered=0 and chat_closed=0 order by A.id desc limit 1 ');
            $count_unanswered=$result1->num_rows;
            
            $total_asked=$conn->query('select question_id from messenger_answers where sessionId="'.$update["sessionId"].'" and senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'" and recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'" and chat_closed =0 ');            
            $count_total_asked=$total_asked->num_rows;
            
            if($count_total_asked==0 && $count_unanswered==0)
            {
            $result_check_previous_opened_quest=$conn->query('select sessionId from messenger_answers A  where senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'" and recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'" and is_answered=0 and chat_closed=0 order by A.id desc limit 1 ');
            if($result_check_previous_opened_quest->num_rows>0)
            {
            $expired_sessionId=$result_check_previous_opened_quest->fetch_assoc();                
            $conn->query('update messenger_answers set sessionId="'.$update["sessionId"].'" where sessionId="'.$expired_sessionId["sessionId"].'" and senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'" and recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'" and chat_closed =0 ');
            $conn->query('update facebook_ads_lead_users set messenger_lead_id="'.$update["sessionId"].'" where messenger_lead_id="'.$expired_sessionId["sessionId"].'" ');           
            }
                        
            // Now Find Again
            $result1=$conn->query('select A.*,B.input_format,B.required,B.sequence from messenger_answers A inner join questions B on A.question_id=B.id where sessionId="'.$update["sessionId"].'" and senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'" and recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'" and is_answered=0 and chat_closed=0 order by A.id desc limit 1 ');
            $count_unanswered=$result1->num_rows;
            
            $total_asked=$conn->query('select question_id from messenger_answers where sessionId="'.$update["sessionId"].'" and senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'" and recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'" and chat_closed =0 ');            
            $count_total_asked=$total_asked->num_rows;
            
            }
            
            if($text!="skip" || $text!='omitir')
            {
            $detection_response=detectLanguage($text,DETECT_LANG_KEY);                
            $detection_response=json_decode($detection_response);            
            if($detection_response->code==200)
            {
                if(count($detection_response->data->detections)>0)
                {                
                $default_lang=$detection_response->data->detections[0]->language;
                }                               
            }    
            }
            
            $default_lang=($text=="omitir") ? "es" : $default_lang;
            
            //Save Chat Record
            $conn->query('insert into bot_chat_records set chat_id="'.$update["id"].'",sessionId="'.$update["sessionId"].'", fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'",fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'", text="'.$text.'" ,msg_by="user" ,lang="'.$default_lang.'", created_at="'.$now.'"');
            $last_insert_id=$conn->insert_id;
            
             /** Check delay in hours between last msg and this msg to send welcome back msg */
            $last_msged=$conn->query('select A.created_at,B.first_name,B.last_name from bot_chat_records A inner join messenger_answers B on A.fb_sender_id=B.senderId and A.fb_page_id=B.recipientId where A.fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'" and A.fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'" group by A.id  order by A.id desc limit 1,1');  
            $last_msged_row=$last_msged->fetch_assoc();
            $lastmsg_created_at=$last_msged_row['created_at'];
            $user_name=$last_msged_row['first_name'];
            
            $last_msged_diff=date_diff(date_create($lastmsg_created_at),date_create(date('Y-m-d H:i:s')));            
            
            if($last_msged_diff->format('%h')>=8 || $last_msged_diff->format('%a')>0)
            {
              $msg=($default_lang=='es') ? 'Bienvenido de vuelta '.$user_name."\r\n�Qu� puedo hacer por ti esta vez?" :'Welcome back '.$user_name."\r\nWhat can i do for you this time?";
              sendReply($update["originalRequest"]["data"]["sender"]["id"],$msg,$accessToken); 
              //Save Chat Record
              $conn->query('insert into bot_chat_records set chat_id="'.$update["id"].'",sessionId="'.$update["sessionId"].'", fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'",fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'", text="'.$msg.'" ,msg_by="bot" ,lang="'.$default_lang.'", created_at="'.$now.'"');
               
            }
             /** Check delay in hours between last msg and this msg to send welcome back msg */  
          
            
            /** Check if it is greeting message and reply */
            require_once('greetings_chat.php');
            if($count_questions && $count_unanswered>0 && $count_total_asked>0)
            {             
            $row1=$result1->fetch_assoc();
            
            $first_name=$last_name=$gender=$profile_pic=$locale=$timezone='';        
            $first_name=$row1["first_name"];$last_name=$row1["last_name"];$gender=$row1["gender"];$profile_pic=$row1["profile_pic"];$locale=$row1["locale"];$timezone=$row1["timezone"];                         
            $sequence=$row1['sequence'];
            
            $total_asked_ids=[];
            
            while($row_total_asked=$total_asked->fetch_assoc())
            {
                $total_asked_ids[]=$row_total_asked['question_id'];
            }
            $total_asked_ids=implode(',',$total_asked_ids); 
              
            $input_format=$row1['input_format']; $required=$row1['required'];
            $matched_text='';
            }
            else
            {
            $user_details=getUserDetails($update["originalRequest"]["data"]["sender"]["id"],$accessToken);            
            $first_name=$user_details->first_name;
            }                        
            $check_answer=CheckIfUserQuestionMatchedByKeyword($text,$first_name,$conn,$default_lang,$update["originalRequest"]["data"]["recipient"]["id"],$update["originalRequest"]["data"]["sender"]["id"],$now); 
            $check_answer =json_decode($check_answer);
            $greeting_reply=0;
            if($check_answer->code)
            {               
               sendReply($update["originalRequest"]["data"]["sender"]["id"],utf8_decode($check_answer->answer),$accessToken);
               
               $conn->query('update bot_chat_records set answer_id="'.$check_answer->answer_id.'",answer_no="'.$check_answer->answer_no.'",matched_keyword="'.utf8_decode($check_answer->matched_keyword).'" where id="'.$last_insert_id.'"');
              
               //Save Chat Record
               $conn->query('insert into bot_chat_records set chat_id="'.$update["id"].'",sessionId="'.$update["sessionId"].'", fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'",fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'", text="'.utf8_decode($check_answer->answer).'",answer_id="'.$check_answer->answer_id.'",answer_no="'.$check_answer->answer_no.'",matched_keyword="'.utf8_decode($check_answer->matched_keyword).'",msg_by="bot" ,lang="'.$default_lang.'", created_at="'.$now.'"');
            
               $greeting_reply=1; 
               if(!$count_unanswered)
               { 
               exit;
               }
            }
            
            if($count_questions && $count_unanswered>0 && $count_total_asked>0)
            {             
                           
            /** If language changed update default language */
            if($default_lang!=$row1['lang'] && $text!="skip")
            {
            $conn->query('update messenger_answers set lang="'.$default_lang.'" where id="'.$row1["id"].'"  '); 
            }
            /** if couldn't detect language use previously detected language */
            if($text=="skip" || count($detection_response->data->detections)==0 )
            {
             $default_lang=$row1['lang'];    
            }
            $question_lang=($default_lang=="es") ? 'question_sp' : $question_lang;
               
            if($text=="skip" && ! $required || $text=="omitir" && ! $required || $sequence==1) 
            {
                $msg=($default_lang=="es") ? "Bueno. Eso est� salteado." :"Okay. That's skipped.";   
                $msg=(($sequence==1 && $default_lang=="es") ? "Bueno!" : ( ($sequence==1) ? "Okay!" : $msg  ) );
                $conn->query('update messenger_answers set chat_id="'.$update["id"].'", entities="'.$input_format.'",entity_value="'.$text.'",text="'.$text.'",lang="'.$default_lang.'",is_answered=1,updated_at="'.$now.'" where id="'.$row1["id"].'" ');
                if(!$greeting_reply)
                {
                sendReply($update["originalRequest"]["data"]["sender"]["id"],$msg,$accessToken);
                
                $answer_id=($sequence==1 && !$greeting_reply) ? -1 : 0 ;
                
                //Save Chat Record
                $conn->query('insert into bot_chat_records set chat_id="'.$update["id"].'",sessionId="'.$update["sessionId"].'", fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'",fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'", text="'.$msg.'",answer_id="'.$answer_id.'",msg_by="bot" ,lang="'.$default_lang.'", created_at="'.$now.'"');
        
              
                }   
                if($sequence==1 && !$greeting_reply)
                {
                $conn->query('insert into bot_unanswered_query  set chat_id="'.$update["id"].'", sessionId="'.$update["sessionId"].'", senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'",recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'",   text="'.$text.'" ,lang="'.$default_lang.'" , created_at="'.$now.'"') or 'Duplicate Ignored';            
                }         
                $question_result1=$conn->query("select ".$question_lang.",id,required from questions  where status=1 and is_deleted=0 and id NOT IN ( ".$total_asked_ids.")  order by sequence asc limit 1");           
                if($question_result1->num_rows>0)
                {                     
                $row_question1=$question_result1->fetch_assoc();                
                $question=str_replace("{First_Name}",$first_name,$row_question1[$question_lang]);                
                $question_id=$row_question1["id"];            
                $conn->query('insert into messenger_answers  set chat_id="'.$update["id"].'", sessionId="'.$update["sessionId"].'",senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'",recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'",lang="'.$default_lang.'",question_id="'.$question_id.'",first_name="'.$first_name.'",last_name="'.$last_name.'" , profile_pic="'.$profile_pic.'" , locale="'.$locale.'" ,timezone="'.$timezone.'" ,gender="'.$gender.'",created_at="'.$now.'"');  
                sendReply($update["originalRequest"]["data"]["sender"]["id"],$question,$accessToken);
                $msg='';
                if(!$row_question1['required'])
                {                    
                    $msg=($default_lang=="es") ? "Si no quiere responder, omita esto solo diga omitir" :"If you don't want to answer, skip this just say skip";
                    sendReply($update["originalRequest"]["data"]["sender"]["id"],$msg,$accessToken);
                }
                //Save Chat Record
                $conn->query('insert into bot_chat_records set chat_id="'.$update["id"].'",sessionId="'.$update["sessionId"].'", fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'",fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'", text="'.$question.'\r\n'.$msg.'" ,question_id="'.$question_id.'", msg_by ="bot" ,lang="'.$default_lang.'", created_at="'.$now.'"');
        
                exit;                
                }
                else
                {
                   $msg=($default_lang=="es") ? "�Bonito! Todo listo." : "Nice! All Done.";
                   sendReply($update["originalRequest"]["data"]["sender"]["id"],$msg,$accessToken); 
                   
                   //Save Chat Record
                   $conn->query('insert into bot_chat_records set chat_id="'.$update["id"].'",sessionId="'.$update["sessionId"].'", fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'",fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'", text="'.$msg.'" ,msg_by ="bot" ,lang="'.$default_lang.'", created_at="'.$now.'"');
        
                                             
                   /** Close chat conversation in current session */
                   $conn->query('update messenger_answers  set chat_closed=1 where sessionId="'.$update["sessionId"].'" and senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'" and recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'"  and chat_closed =0'); 
                   exit;
                }            
             }
            if($greeting_reply)
            {
                exit;
            }    
                  
            if($query_parameters[$input_format] && $text!="skip" && $text!="omitir" ||  $input_format=="address" && count($nlp_entities)>0 && array_key_exists('location',$nlp_entities) ||  count($nlp_entities)>0 && array_key_exists($input_format,$nlp_entities) && $text!="skip" && $text!="omitir")
            {
            if($input_format=="any")
            {                
                $matched_text= $text;
            } 
            else if($input_format=="address")
            {
              $matched_text= (count($nlp_entities)>0 && array_key_exists('location',$nlp_entities)) ? $nlp_entities['location'][0]['value'] : $query_parameters[$input_format]; 
            }
            else
            {
                $matched_text=(count($nlp_entities)>0 && array_key_exists($input_format,$nlp_entities)) ? $nlp_entities[$input_format][0]['value'] : $query_parameters[$input_format] ;
            }  
            
            /** if couldn't detect language use previously detected language */
            if($matched_text==$text && $input_format!="any"  )
            {
             $default_lang=$row1['lang'];    
            }
            else if($input_format=="any" && count($nlp_entities)>0 && $nlp_entities[array_keys($nlp_entities)[0]][0]['value']==$text  )
            {
            $default_lang=$row1['lang'];    
            }            
            
            $msg=($default_lang=="es") ? "(y) Lo tengo. Gracias." : "(y) Got It. Thank You.";  
            $question_lang=($default_lang=="es") ? 'question_sp' : $question_lang;
            
            $conn->query('update messenger_answers set chat_id="'.$update["id"].'", entities="'.$input_format.'",entity_value="'.$matched_text.'",text="'.$text.'",lang="'.$default_lang.'",is_answered=1,updated_at="'.$now.'" where id="'.$row1["id"].'" ');            
            
            sendReply($update["originalRequest"]["data"]["sender"]["id"],$msg,$accessToken);
            
            //Save Chat Record
            $conn->query('insert into bot_chat_records set chat_id="'.$update["id"].'",sessionId="'.$update["sessionId"].'", fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'",fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'", text="'.$msg.'" ,msg_by="bot" ,lang="'.$default_lang.'", created_at="'.$now.'"');
         
            
             /** Save  Lead */  
             if($input_format=="phone_number" || $input_format=="email" || $input_format=="address" || $input_format=="zip-code")
             { 
                $column=( ($input_format=="address") ? "street_address" : (($input_format=="zip-code") ? "zip_code" : $input_format ) ); 
                $latlong_insert='';
                /** Find Lat Long */
                if($input_format=="address" || $input_format=="zip-code")
                {
                    $latlong=json_decode(FindLatLong($matched_text));
                    
                    if($latlong->code==200)
                    {
                    $latlong_insert=' , latitude="'.$latlong->lat.'" , longitude="'.$latlong->long.'" , city="'.$latlong->city.'" , state="'.$latlong->state.'" , country="'.$latlong->country.'" , post_code="'.$latlong->zipcode.'" ';
                    }
                }
                $matched_text=($input_format=="phone_number") ? PhoneNumber($matched_text) : $matched_text;
                 
                
                $check_lead_exists=$conn->query('select id,latitude,longitude from facebook_ads_lead_users where lead_type="Pownder� Messenger"  and facebook_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'" and first_name="'.$first_name.'" and last_name="'.$last_name.'" and full_name="'.$first_name.' '.$last_name.'" and  gender="'.$gender.'"  and messenger_lead_id="'.$update["sessionId"].'" and '.$column.' IS NULL order by id desc limit 1 ');
                $facebook_ads_lead_user_id=0;
                if($check_lead_exists->num_rows==0)
                {
                $conn->query('insert into facebook_ads_lead_users set lead_type="Pownder� Messenger" , facebook_account_id="'.$update["originalRequest"]["data"]["recipient"]["facebook_account_id"].'" , facebook_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'" , first_name="'.$first_name.'",last_name="'.$last_name.'",full_name="'.$first_name.' '.$last_name.'", '.$column.'="'.$matched_text.'"  '.$latlong_insert.' ,gender="'.$gender.'" , created_time= "'.$now.'" , created_user_id="'.$update["originalRequest"]["data"]["recipient"]["fbpage_updated_user_id"].'" , created_at="'.$now.'" , updated_user_id="'.$update["originalRequest"]["data"]["recipient"]["fbpage_updated_user_id"].'" , updated_at="'.$now.'" ,messenger_lead_id="'.$update["sessionId"].'" , campaign_name="'.$update["originalRequest"]["data"]["recipient"]["fb_page_name"].'"');
                $facebook_ads_lead_user_id=$conn->insert_id;
                }
                else
                {
                 $row_lead=$check_lead_exists->fetch_assoc();
                 
                 //$latlong_insert=($row_lead['latitude'] && $row_lead['longitude']) ? '' : $latlong_insert;  
                 
                 $conn->query('update facebook_ads_lead_users set  '.$column.'="'.$matched_text.'" '.$latlong_insert.' , updated_at="'.$now.'" where  id="'.$row_lead["id"].'" ');                    
                
                 $facebook_ads_lead_user_id=$row_lead["id"]; 
                }
               
                /** Notify Admin */
                 require_once 'notify_admin_new_lead.php';
                /** Notify Admin */
                
                
             }                       
             /** Save  Lead */ 
            
            $question_result1=$conn->query("select ".$question_lang.",id,required from questions  where status=1 and is_deleted=0 and id NOT IN ( ".$total_asked_ids.")  order by sequence asc limit 1");
            if($question_result1->num_rows>0)
            {                 
            $row_question1=$question_result1->fetch_assoc();            
            $question=str_replace("{First_Name}",$first_name,$row_question1[$question_lang]);            
            $question_id=$row_question1["id"];        
            $conn->query('insert into messenger_answers  set chat_id="'.$update["id"].'", sessionId="'.$update["sessionId"].'",senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'",recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'",lang="'.$default_lang.'",question_id="'.$question_id.'",first_name="'.$first_name.'",last_name="'.$last_name.'" , profile_pic="'.$profile_pic.'" , locale="'.$locale.'" ,timezone="'.$timezone.'" ,gender="'.$gender.'",created_at="'.$now.'"');  
            sendReply($update["originalRequest"]["data"]["sender"]["id"],$question,$accessToken);
            
            $msg='';
            if(!$row_question1['required'])
            {                
                $msg=($default_lang=="es") ? "Si no quiere responder, omita esto solo diga omitir" :"If you don't want to answer, skip this just say skip";
                sendReply($update["originalRequest"]["data"]["sender"]["id"],$msg,$accessToken);
            } 
            
             //Save Chat Record
            $conn->query('insert into bot_chat_records set chat_id="'.$update["id"].'",sessionId="'.$update["sessionId"].'", fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'",fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'", text="'.$question.'\r\n'.$msg.'",question_id="'.$question_id.'" ,msg_by="bot" ,lang="'.$default_lang.'", created_at="'.$now.'"');
                   
            }
            else
            {
               $msg=($default_lang=="es") ? "�Bonito! Todo listo." : "Nice! All Done.";
               sendReply($update["originalRequest"]["data"]["sender"]["id"],$msg,$accessToken);               
               /** Close chat conversation in current session */
               $conn->query('update messenger_answers  set chat_closed=1 where sessionId="'.$update["sessionId"].'" and senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'" and recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'"  and chat_closed =0 ');
               exit;
            }          
            }
            else  
            {            
            $msg=($default_lang=="es") ? "0:) No entend� eso. �Por favor, puedes decirlo otra vez?" : "0:) I didn't get that. Can you say it again please?"; 
            $msg= ( ($text=="omitir") ? "lo siento esta vez no puedes saltear" : (($text=="skip") ? "sorry this time you can't skip" : $msg ) )  ; 
            $unanswered=( ($text=="omitir") ? 0 : (($text=="skip") ? 0 : 1 ) );
            sendReply($update["originalRequest"]["data"]["sender"]["id"],$msg,$accessToken);
           
            $answer_id=($unanswered) ? -1 : 0 ;
            
            //Save Chat Record
            $conn->query('insert into bot_chat_records set chat_id="'.$update["id"].'",sessionId="'.$update["sessionId"].'", fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'",fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'", text="'.$msg.'" ,msg_by="bot" ,answer_id="'.$answer_id.'" ,lang="'.$default_lang.'", created_at="'.$now.'"');
            
            if($unanswered)
            {
             $conn->query('insert into bot_unanswered_query  set chat_id="'.$update["id"].'", sessionId="'.$update["sessionId"].'", senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'",recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'",   text="'.$text.'" ,lang="'.$default_lang.'" , created_at="'.$now.'"') or 'Duplicate Ignored';            
            }
            exit;
            } 
            
            }
            else
            {
            if($text!="skip" && $text!='omitir' && count($detection_response->data->detections)==0)
            {    
            $result_check_previous_opened_quest=$conn->query('select lang from messenger_answers A  where 
            senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'"
            and recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'"
            order by A.id desc limit 1
            ');
            if($result_check_previous_opened_quest->num_rows>0)
            {
            $expired_sessionId=$result_check_previous_opened_quest->fetch_assoc();
            $default_lang=$expired_sessionId['lang'];
            }
            }
            $msg=($default_lang=="es") ? "0:) No entend� eso. �Por favor, puedes decirlo otra vez?" : "0:) I didn't get that. Can you say it again please?"; 
            
           // $msg=($default_lang=="es") ? "No mas preguntas. :|" : "No more questions. :|";
            $unanswered=1;    
            if(count($nlp_entities)>0)
            {
                $msg1=[];
                
                if(array_key_exists('greetings',$nlp_entities))
                {
                    $msg1[]=($default_lang=="es") ? "Eso es bueno"  : "It's good";
                    $unanswered=0;
                }                
                if(array_key_exists('thanks',$nlp_entities))
                {
                    $msg1[]=($default_lang=="es") ? "Eres bienvenido" : "You're most welcome";
                    $unanswered=0;
                }
                if(is_numeric(strpos($text,'bye')) && strpos($text,'bye')>=0)
                {
                    $msg1[]=($default_lang=="es") ? "Adi�s :)" : "Bye Bye :)";
                    $unanswered=0;
                }
                if(count($msg1)>0)
                {
                $msg=implode("\r\n",$msg1);
                }
                
            }   
            sendReply($update["originalRequest"]["data"]["sender"]["id"],$msg,$accessToken);
            
            $answer_id=($unanswered) ? -1 : 0 ;
            
            //Save Chat Record
            $conn->query('insert into bot_chat_records set chat_id="'.$update["id"].'",sessionId="'.$update["sessionId"].'", fb_sender_id="'.$update["originalRequest"]["data"]["sender"]["id"].'",fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'", text="'.$msg.'" ,msg_by="bot",answer_id="'.$answer_id.'" ,lang="'.$default_lang.'", created_at="'.$now.'"');
            
            
            if($unanswered)
            {
             $conn->query('insert into bot_unanswered_query  set chat_id="'.$update["id"].'", sessionId="'.$update["sessionId"].'", senderId="'.$update["originalRequest"]["data"]["sender"]["id"].'",recipientId="'.$update["originalRequest"]["data"]["recipient"]["id"].'",   text="'.$text.'" ,lang="'.$default_lang.'" , created_at="'.$now.'"') or 'Duplicate Ignored';            
            }
            exit;
            }    
        }
       
    }
}

/** 1. Return response Dialogflow **/

function sendMessage($parameters) {
    echo json_encode($parameters);
}

/** 2. Send Message **/
function sendReply($recipientId,$msg,$accessToken) 
{

$data = [
    'messaging_type' =>'RESPONSE',
    'recipient' => [ 'id' => $recipientId ],
    'message' => [ 'text' => utf8_encode($msg) ],
   // 'sender_action'=>'typing_on'
];
$ch = curl_init('https://graph.facebook.com/v2.6/me/messages?access_token='.$accessToken);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json; charset=utf-8']);
$response=curl_exec($ch);
$http_code=curl_getinfo($ch,CURLINFO_HTTP_CODE);
curl_close($ch);
if($http_code==200)
{
    $response=json_decode($response);
    $response->code=200;
    return json_encode($response);
}
return '{"code":400}';


}

/** 3. Find user's name */
function getUserDetails($senderId,$accessToken)
{
$curl=curl_init("https://graph.facebook.com/v2.6/$senderId?fields=first_name,last_name,profile_pic,locale,timezone,gender&access_token=$accessToken");
curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HEADER, false);
$response=curl_exec($curl);
$http_code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
curl_close($curl);
if($http_code==200)
{
    $response=json_decode($response);
    return $response;
}   
return '';
} 

/** 4. Detect language **/
function detectLanguage($text,$key='66d50e8b24fd288cc616130d7cb3626a')
{
    $curl=curl_init('http://ws.detectlanguage.com/0.2/detect');
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl,CURLOPT_HEADER, false);
    curl_setopt($curl, CURLOPT_POST , true);
    curl_setopt($curl,CURLOPT_POSTFIELDS, ["q"=>$text,"key"=>$key]);
    $response=curl_exec($curl);
    $http_code=curl_getinfo($curl,CURLINFO_HTTP_CODE);
    curl_close($curl);
    if($http_code==200)
    {
        $response=json_decode($response);
        $response->code=200;
        return json_encode($response);
    }
    return '{"code":400}';
}

/** 5. Find Lat Long */
function FindLatLong($address)
{
$user_latitude=$user_longitude=$city = $state = $country =  $zipcode = '';

$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false";
$result_string = file_get_contents($url);
$result = json_decode($result_string, true);
if(count($result['results'])>0)
{
$val = $result['results'][0]['geometry']['location'];
$address_components = $result['results'][0]['address_components'];
$user_latitude=$val['lat'];
$user_longitude=$val['lng'];

foreach($address_components as $addressPart) {
	if ((in_array('locality', $addressPart['types'])) && (in_array('political', $addressPart['types'])))
	$city = $addressPart['long_name'];
	else if ((in_array('administrative_area_level_1', $addressPart['types'])) && (in_array('political', $addressPart['types'])))
	$state = $addressPart['long_name'];
	else if ((in_array('country', $addressPart['types'])) && (in_array('political', $addressPart['types'])))
	$country = $addressPart['long_name'];
    else if ((in_array('postal_code', $addressPart['types'])))
	$zipcode = $addressPart['long_name'];
}
			
return '{"code":200,"lat":"'.$user_latitude.'","long":"'.$user_longitude.'","city":"'.$city.'","state":"'.$state.'","country":"'.$country.'","zipcode":"'.$zipcode.'"}';
}
return '{"code":404}';
}

/** 6. Convert Phone Number Format */
function PhoneNumber($phone_number)
{
		$matches = null;
		preg_match_all('!\d+!', $phone_number, $matches);
		
		$string='';
		$number=$matches[0];
		for($i=0;$i<count($number);$i++)
		{
			$string.=$number[$i];
		}
		
		$newstring = substr($string, -10);
		
		if(strlen($newstring)==10)
		{
			$first=substr($newstring,0,3);
			$second=substr($newstring,3,3);
			$third=substr($newstring,6,10);
			
			$new_phone_number = '('.$first.') '.$second.'-'.$third;
			
			return $new_phone_number;
		}
		else
		{
			return $phone_number;
		}	
}
/** 7.Convert To Phone Number Format For Email */
function emailPhoneFormat($Phone)
{
			$Phone = str_replace(') ', '-', $Phone);
			$Phone = str_replace('(', '', $Phone);
			return $Phone;
}
		
$update_response = file_get_contents("php://input");
$update = json_decode($update_response, true);
file_put_contents('webhook.txt',print_r($update,true)); 
if (isset($update["result"]["action"])) {
    
    $result_token=$conn->query('select A.fb_page_token,A.status,B.is_deleted,B.is_blocked,B.facebook_account_id,B.name as fb_page_name,B.updated_user_id  as fbpage_updated_user_id  from bot_activation A inner join facebook_pages B on A.fb_page_id=B.id  where A.fb_page_id="'.$update["originalRequest"]["data"]["recipient"]["id"].'" ');
    if($result_token->num_rows>0)
    {       
    $token_row=$result_token->fetch_assoc(); 
    $accessToken=$token_row['fb_page_token'];
    if($token_row['is_deleted']==1 || $token_row['is_blocked']==1)
    {
      $msg=":'( Sorry i'm unable to respond";
    //  sendReply($update["originalRequest"]["data"]["sender"]["id"],$msg,$accessToken);
      file_put_contents('response.txt',$msg);    
    }
    else if($token_row['status']==1)
    {         
    $update["originalRequest"]["data"]["recipient"]["facebook_account_id"]=$token_row['facebook_account_id'];
    $update["originalRequest"]["data"]["recipient"]["fb_page_name"]=$token_row['fb_page_name'];
    $update["originalRequest"]["data"]["recipient"]["fbpage_updated_user_id"]=$token_row['fbpage_updated_user_id'];
    processMessage($update,$conn,$accessToken,$now);
    }
    else
    {
      $msg=":'( Sorry i'm unable to respond"; 
    //  sendReply($update["originalRequest"]["data"]["sender"]["id"],$msg,$accessToken);
      file_put_contents('response.txt',$msg);    
    }
    }
    else
    {
      $msg=":'( Sorry i'm unable to respond"; 
      file_put_contents('error.txt',$msg); 
    }           
}


//sendMessage(array(
//            "source" => $update["result"]["source"],
//            "speech" => utf8_encode($msg),
//            "displayText" => utf8_encode($msg),
//            "contextOut" => array()
//            ) );
//
?>