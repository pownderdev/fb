<?php 
if(isset($_POST['stripeToken']))
{
	$token=$_POST['stripeToken'];
	$ch = curl_init();
	$post = [
		'amount' => '1500',
		'currency' => 'usd',
		'source'   => $token, /* posted from payment.php */
		'description'   => 'testing',
	];


	curl_setopt($ch, CURLOPT_URL,"https://api.stripe.com/v1/charges");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($post));
	curl_setopt($ch, CURLOPT_USERPWD, "sk_test_o487qfpPDQxrZC1bUpJqC02t:");  /* use secret key */
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$server_output = curl_exec ($ch);
	$info = curl_getinfo($ch);
	curl_close ($ch);
	echo "<pre>";
	print_r($server_output);
	echo "</pre>";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Payments using Stripe</title>	
</head>
<body>
<p>Price: 15.00$</p>
<p>Payment Using Debit Card</p>
<form action="" method="POST">
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_KuHfbf3wkOtkAZabUU07Myyj" // your publishable keys
    data-name="Stripe Payment"
    data-description="Payment Using Debit Card"
    data-amount="1500">
  </script>
</form>
</body>
</html>