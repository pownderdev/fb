<?php 
function get_pay($key,$card_number,$exp_month,$exp_year,$cvc,$amount,$email)
{
	$token_request_body = array('card' => array(
    'number' => $card_number, /* for credit card */
    'exp_month' => $exp_month,
    'exp_year' => $exp_year,
    'cvc' => $cvc,
    'currency' => 'USD'
  ));
  /* step 1 create token by card details */
  $req = curl_init('https://api.stripe.com/v1/tokens');
  curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($req, CURLOPT_USERPWD, "$key:");
  curl_setopt($req, CURLOPT_POST, true );
  curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
  $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
  $resp = json_decode(curl_exec($req), true);
  $token=$resp['card']['id'];
  echo '<pre>';print_r($resp);
  /* step 2 create source */
  $ch = curl_init();
	$post = [
		'type' => 'bitcoin',
		'amount' => $amount,
		'currency' => 'usd',
		'owner' => ['email' => $email]
	];

	curl_setopt($ch, CURLOPT_URL,"https://api.stripe.com/v1/sources");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($post));
	curl_setopt($ch, CURLOPT_USERPWD, "$key:");  /* use secret key */
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$resp = json_decode(curl_exec($ch), true);
    print_r($resp);
	$source=$resp['id'];
	return $source;
}
function charge($key,$amount,$source,$description)
{
	/* step 3 create charge or pay */
	$ch = curl_init();
	$post = [
		'amount' => $amount,
		'currency' => 'usd',
		'source'   => $source, 
		'description'   => $description
	];
	curl_setopt($ch, CURLOPT_URL,"https://api.stripe.com/v1/charges");
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($post));
	curl_setopt($ch, CURLOPT_USERPWD, "$key:");  /* use secret key */
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$resp = json_decode(curl_exec($ch), true);
    $respCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	return $resp;
}



/* all required credentials */

$key='sk_test_gi4rxjXLkbqZ4zyNk2ZvS0gj';
$card_number='4242424242424242'; /* for credit card test */
//$card_number='6521-6036-9000-1308'; /* for debit card test */
$exp_month='08';
$exp_year='21';
$cvc='269';
$amount=1000; /* it should be on cent eg; $10=1000 */
$email='test@gmail.com';
$description= 'testing'; 

$source=get_pay($key,$card_number,$exp_month,$exp_year,$cvc,$amount,$email); /* call for cretae source using card details */
sleep(5);
$resp=charge($key,$amount,$source,$description); /* create charge using source */

echo "<pre>";
print_r($resp);
echo "</pre>";
?>