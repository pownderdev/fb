<?php 
    function get_exchange($client_id,$secret,$token)
	{
		/* step1:- exchange public token */

		$token_request_body = array(
			'client_id' => $client_id,
			'secret' => $secret,
			'public_token' => $token
				);
			$url='https://sandbox.plaid.com/item/public_token/exchange';
			$req = curl_init();
		  
			curl_setopt($req, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
				) );
			curl_setopt($req, CURLOPT_URL, $url);
			curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($req, CURLOPT_POST, true ); 
			curl_setopt($req, CURLOPT_POSTFIELDS, json_encode($token_request_body));
			curl_setopt($req, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($req, CURLOPT_SSL_VERIFYPEER, 0); 
			$respCode = curl_getinfo($req);
			$auth=curl_exec($req);
			$resp = json_decode($auth, true);
			$access_token=$resp['access_token'];
            print_r( '<step1>');
            print_r($resp);
			return $access_token;
	}
	function get_stripe_tok($client_id,$secret,$access_token,$account) /* get stripe bank account token */
	{
		/* step2:- generate stripe token */
		$token_request_body = array(
		'client_id' => $client_id,
		'secret' => $secret,
		'access_token' => $access_token,
		'account_id' => $account,
		);
		$url='https://sandbox.plaid.com/processor/stripe/bank_account_token/create';
		$req = curl_init();
	  
		curl_setopt($req, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
			) );
		curl_setopt($req, CURLOPT_URL, $url);
		curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($req, CURLOPT_POST, true ); 
		curl_setopt($req, CURLOPT_POSTFIELDS, json_encode($token_request_body));
		curl_setopt($req, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($req, CURLOPT_SSL_VERIFYPEER, 0); 
		$respCode = curl_getinfo($req);
		$auth=curl_exec($req);
		$resp = json_decode($auth, true);
        print_r( '<step2>');
            print_r($resp);
		$stripe_bank_account_token=$resp['stripe_bank_account_token'];
		return $stripe_bank_account_token;
	}
	function create_cust($stripe_bank_account_token,$description,$email,$stripe_key)
	{
		/* step 3:-  create external account */
			
				$token_request_body = array('source' => $stripe_bank_account_token,'description' => $description,'email' => $email);
				$req = curl_init('https://api.stripe.com/v1/customers');
				curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($req, CURLOPT_USERPWD, $stripe_key.":");
				curl_setopt($req, CURLOPT_POST, true );
				curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
				$respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
				$resp = json_decode(curl_exec($req), true);
				print_r( '<step3>');
                print_r($resp);
				return $resp;
	}
	
	function get_pay($amount,$cust_id,$stripe_key)
	{
		/* step4:- Make payment */	
				$token_request_body = array('amount' => $amount * 100,
								'currency' => 'usd',
								'customer' => $cust_id);
			$req = curl_init('https://api.stripe.com/v1/charges');
			curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($req, CURLOPT_USERPWD, $stripe_key.":");
			curl_setopt($req, CURLOPT_POST, true );
			curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
			$respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
			$resp = json_decode(curl_exec($req), true);
            print_r( '<step4_final>');
            print_r($resp);
			return $resp;
	}
?>