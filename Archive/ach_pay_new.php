<html>
<head></head>
<script type="text/javascript" src="https://js.stripe.com/v3/"></script>
<body>
CREATE TOKEN
  <form method="POST">
    <input type="hidden" name="token" />
    <div class="form-row">
      <label for="country">Country</label>
      <select id="country">
        <option value="US">United States</option>
        <option value="CA">Canada</option>
        <option value="GB">United Kingdom</option>
        <option value="DE">Germany</option>
        <option value="FR">France</option>
        <option value="ES">Spain</option>
      </select>
    </div>
    <div class="form-row">
      <label for="currency">Currency</label>
      <select id="currency">
        <option value="USD">US Dollar</option>
        <option value="CAD">Canadian Dollar</option>
        <option value="GBP">Pound</option>
        <option value="EUR">Euro</option>
      </select>
    </div>
    <div class="form-row">
      <label for="routing-number">Routing Number</label>
      <input type="text" id="routing-number" value="110000000" />
    </div>
    <div class="form-row">
      <label for="account-number">Account Number</label>
      <input type="text" id="account-number" value="000123456789" />
    </div>
    <div class="form-row">
      <label for="account-holder-name">Account Holder Name</label>
      <input type="text" id="account-holder-name" value="Jane Austen" />
    </div>
    <div class="form-row">
      <label for="account-holder-type">Account Holder Type</label>
      <select id="account-holder-type">
        <option value="individual">Individual</option>
        <option value="company">Company</option>
      </select>
    </div>
    <button type="submit">Submit</button>
    <div class="outcome">
      <div class="error"></div>
      <div class="success">
        Success! Your Stripe token is <span class="token"></span>
      </div>
    </div>
  </form>
   <form method="POST" class="main">
    <input type="hidden" name="token" />
  </form>  
</body>
<script type="text/javascript">

var stripe = Stripe('pk_test_asEFIgfe18JoKmkee0MfXowh');

function setOutcome(result) {
    console.log(result);
  var successElement = document.querySelector('.success');
  var errorElement = document.querySelector('.error');
  successElement.classList.remove('visible');
  errorElement.classList.remove('visible');

  if (result.token) {
    // In this example, we're simply displaying the token
    successElement.querySelector('.token').textContent = result.token.id;
    successElement.classList.add('visible');

    // In a real integration, you'd submit the form with the token to your backend server
    var form = document.querySelector('form.main');
    form.querySelector('input[name="token"]').setAttribute('value', result.token.id);
    form.submit();
  } else {
    errorElement.textContent = result.error.message;
    errorElement.classList.add('visible');
  }
}

document.querySelector('form').addEventListener('submit', function(e) {
  e.preventDefault();

  var bankAccountParams = {
    country: document.getElementById('country').value,
    currency: document.getElementById('currency').value,
    account_number: document.getElementById('account-number').value,
    account_holder_name: document.getElementById('account-holder-name').value,
    account_holder_type: document.getElementById('account-holder-type').value,
  }
  if (document.getElementById('routing-number').value != '') {
    bankAccountParams['routing_number'] = document.getElementById('routing-number').value;
  }

	stripe.createToken('bank_account', bankAccountParams).then(setOutcome);
});

</script>
</html>

Create Customer
Api documentation link:-http://constacloud.in/loogita/stripe/create_customer.php

<?php
if(isset($_POST))
{
$token=$_POST['token'];
$secret_key='sk_test_gi4rxjXLkbqZ4zyNk2ZvS0gj';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/customers");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "description=\"Customer&source=".$token);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_USERPWD, $secret_key . ":" . "");

$headers = array();
$headers[] = "Content-Type: application/x-www-form-urlencoded";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close ($ch);
echo "<pre>";
print_r(json_decode($result,true));
?>

Verify token
Api documentation link-http://constacloud.in/loogita/stripe/verify_token.php

<?php
$customet_id='cus_Bgjrw29vlLMp22';
$bank_id='ba_1BJMMHFcV9C197VEgh1NHB0t';
$secret_key='sk_test_gi4rxjXLkbqZ4zyNk2ZvS0gj';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/customers/$customet_id/sources/$bank_id/verify");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "amounts[]=32&amounts[]=45");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_USERPWD, $secret_key . ":" . "");

$headers = array();
$headers[] = "Content-Type: application/x-www-form-urlencoded";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close ($ch);
echo "<pre>";
print_r(json_decode($result,true));
?>

Create charge

Api documention link-http://constacloud.in/loogita/stripe/create_ach.php


<?php


$customer_id='cus_Bgjrw29vlLMp22';
$secret_key='sk_test_gi4rxjXLkbqZ4zyNk2ZvS0gj';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://api.stripe.com/v1/charges");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, "amount=150&currency=usd&customer=cus_Bgjrw29vlLMp22");
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_USERPWD, $secret_key . ":" . "");

$headers = array();
$headers[] = "Content-Type: application/x-www-form-urlencoded";
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

$result = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Error:' . curl_error($ch);
}
curl_close ($ch);
echo "<pre>";
print_r(json_decode($result,true));


}
?>

